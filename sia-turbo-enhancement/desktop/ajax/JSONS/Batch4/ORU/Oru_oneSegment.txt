{  
"locale": "pt_BR",
   "bookingSummary":{
      "scenario": "ORU",  
      "adultCount":1,
      "childCount":0,
      "infantCount":0,
      "precision":0,
      "milesAllocated":0,
      "totalMilesForORB":0,
      "totalToBePaidMiles":0,
      "milesToBeRefunded":0,
      "oldItinerary":{  
         "currency":"GBP",
         "fareTotal":2906.70,
         "taxTotal":706.70,
         "totalMilesForORB":0
      },
      "flight":[  
         {  
            "totalTravelTime":"17hrs 45mins",
            "origin":"Singapore",
            "destination":"San Francisco",
            "flightSegments":[  
               {  
                  "carrierCode":"SQ",
                  "airCraftType":"Boeing 777-300ER",
                  "flightNumber":"2",
                  "cabinClassDesc":"BUSINESS",
                  "deparure":{  
                     "time":"18:30",
                     "date":"24 Sep (Thu)",
                     "airportCode":"SIN",
                     "airportName":"Changi Intl",
                     "cityCode":"SIN",
                     "cityName":"Singapore",
                     "terminal":"3"
                  },
                  "arrival":{  
                     "time":"21:15",
                     "date":"24 Sep (Thu)",
                     "airportCode":"SFO",
                     "airportName":"San Francisco Intl",
                     "cityCode":"SFO",
                     "cityName":"San Francisco",
                     "terminal":"I"
                  }
               }
            ]
         }
      ],
      "paxDetails":[  
         {  
            "paxType":"Adult",
            "id":1,
            "paxName":"TWO TESTA",
            "detailsPerFlight":[  
               {  
                  "flightNo":"SQ2",
                  "from":"SIN",
                  "to":"SFO",
                  "departureDate":"Thu Sep 24 23:30:00 IST 2015"
               },
               {  
                  "flightNo":"SQ1",
                  "from":"SFO",
                  "to":"SIN",
                  "departureDate":"Thu Oct 01 08:00:00 IST 2015"
               }
            ]
         }
      ]
   },
   "oruUpgradeJsonVO":{  
      "totMilesForORU":"76500"
   }
}