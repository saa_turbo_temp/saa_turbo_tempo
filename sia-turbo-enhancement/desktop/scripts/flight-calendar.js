/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */

SIA.flightCalendar = function(){
	var global = SIA.global;
	var win = global.vars.win;

	if(!$('.fare-calendar-page').length){
		return;
	}

	var $calendar = $('.calendar__table'),
			$calendarRowColumn = $('td', $calendar),
			$calendarRowRadio = $('.custom-radio', $calendarRowColumn);
	var controls = $('.control-horizontal, .control-vertical');
	var nextControl = $('.next', controls);
	var prevControl = $('.prev', controls);
	var btnNext = $('.flight__calendar .btn-1');
	var dataChecked = $calendar.data('checked');
	var seatDepart;
	var seatReturn;
	var seatAmount;
	var monthsListAbbr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
	var monthsList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var daysListAbbr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
	var daysList = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

	//var isSeatLocked = false;
	var disableNextBtn = function(e) {
		e.preventDefault();
		return;
	};
	var getPositionInTable = function(el) {
		var cellIndex = el.cellIndex,
				rowIndex = el.parentNode.rowIndex;
		return rowIndex + ',' + cellIndex;
	};
	var resetCalendarTable = function() {
		$calendarRowColumn
			.removeClass('active')
			.attr('tabindex', 0);
		$('th', $calendar).removeClass('active');
		$('td input[type="radio"]', $calendar).prop('checked', false);
		$calendar.removeData('checked');
		btnNext.addClass('disabled').on('click.disabled', disableNextBtn);
	};
	var calcCellDisplay = function(calendarTable) {
		var totalCellDisplay = calendarTable.find('tr:eq(0) th:visible').length - 1;
		controls.find('.number').each(function(){
			$(this).html(totalCellDisplay);
		});
		return totalCellDisplay;
	};
	var changeUrlControl = function() {
		if(window.innerWidth >= global.config.tablet) {
			nextControl.attr('href', nextControl.data('url-desktop'));
			prevControl.attr('href', prevControl.data('url-desktop'));
		} else {
			nextControl.attr('href', nextControl.data('url-tablet'));
			prevControl.attr('href', prevControl.data('url-tablet'));
		}
	};
	var checkNextBtn = function(data, isTrigger) {
		if(data) {
			var position = data.split(','),
					rowIndex = parseInt(position[0]),
					cellIndex = parseInt(position[1]) - 1,
					checkedTd = $calendar.find('tr:eq(' + rowIndex + ') td:eq(' + cellIndex + ')');

			if(checkedTd.is(':hidden')) {
				resetCalendarTable();
			} else {
				if(isTrigger) {
					checkedTd.trigger('click.activeHead');
				}
				if(btnNext.hasClass('disabled')) {
					btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
				}
			}
		} else {
			resetCalendarTable();
		}
	};

	$calendarRowColumn.off('click.activeHead').on('click.activeHead',function(){
		var col = $(this).parent().children().index($(this)),
				$headerColumn = $calendar.find('tr').eq(0).find('th');

		resetCalendarTable();
		$(this).addClass('active');
		$(this).parent().find('th').addClass('active');
		$headerColumn.eq(col).addClass('active');

		// event checked radio button and set data table
		$(this).find('input[type="radio"]').prop('checked', true);
		$calendar.data('checked', getPositionInTable(this));
		if(!$(this).hasClass('not-available')){
			if(btnNext.hasClass('disabled')) {
				btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
			}
		}

	});

	var appendAriaLabel = function(){
		$calendarRowRadio.each(function(i){
			var self = $(this);
			var seatIndex = self.parent().index();
			var seatDepartCon;
			var seatReturnCon;
			var notice = (i !== 0)? L10n.fareCalendar.seat : L10n.fareCalendar.firstSeat + ' ' + L10n.fareCalendar.seat;
			seatAmount = self.find('label').text();
			seatDepart = self.parents('tr').children().eq(0).text();
			seatReturn = $calendar.find('tbody tr:first-child').children('th').eq(seatIndex).text();

			for( var i = 0; i < monthsListAbbr.length; i++ ){
				if( seatDepart.indexOf(monthsListAbbr[i]) >= 0 ){
					seatDepartCon = seatDepart.replace(monthsListAbbr[i], monthsList[i]);
				}
				if( seatReturn.indexOf(monthsListAbbr[i]) >= 0 ){
					seatReturnCon = seatReturn.replace(monthsListAbbr[i], monthsList[i]);
				}
			}

			for( var i = 0; i < daysListAbbr.length; i++ ){
				if( seatDepartCon.indexOf(daysListAbbr[i]) >= 0 ){
					seatDepartCon = seatDepartCon.replace(daysListAbbr[i], daysList[i]);
				}
				if( seatReturnCon.indexOf(daysListAbbr[i]) >= 0 ){
					seatReturnCon = seatReturnCon.replace(daysListAbbr[i], daysList[i]);
				}
			}

			var saysText = '<span class="says">' + notice.replace('{seatDepart}', seatDepartCon).replace('{seatReturn}', seatReturnCon).replace('{seatAmount}', seatAmount) + '</span>';

			self.find('label').html( '<span aria-hidden="true">' +seatAmount+ '</span>' + saysText );
			self.attr('aria-label', self.find('label').find('.says').text() );

		});
	};

	var keyboardNavigation = function(){

		/** Onload Move focus to logo **/
		$('.logo').focus();

		$('.flight__calendar').attr('tabindex', 5);

		var calendarTable = $('.calendar__table');
		var th = $('th', calendarTable);

		//TOOLS List
		$('.tools-list').children().each(function(){
			if($('em', $(this)).hasClass('ico-mail')){
				$('a', $(this)).attr({'aria-label':'Email','tabindex': 0});
			}
			if($('em', $(this)).hasClass('ico-print')){
				$('a', $(this)).attr({'aria-label':'Print','tabindex': 0});
			}
		});

		th.attr({
			'aria-hidden':true,
			'tabindex': -1
		});

		$('.calendar__table td').each(function(){
			var self = $(this);
			var radio = $('input', self);

			calendarTable.attr({
				'role':'application'
			});

			radio.attr({
				'aria-hidden':true,
				'tabindex':-1
			});

			self.attr({
				'role':'link'
			});

			self.focus(function(){
				/** Remove default key events **/
				self.trigger('click');
				overrideKeys();
			});

		});

		var allowKeyCode = [9, 37, 38, 39, 40, 32, 13];

		var moveRight = function(seat){
			if( !seat.is(':last-child') ){
				seat.next().trigger('click').focus();
			}else{
				seat.parent().next().children().eq(1).trigger('click').focus();
			}
		};
		var moveleft = function(seat){
			if( !seat.prev().is(':first-child') ){
				seat.prev().trigger('click').focus();
			}else{
				seat.parent().prev().children().last().trigger('click').focus();
			}
		};
		var moveTop = function(seat){
			if( !seat.parent().is(':first-child') ){
				seat.parent().prev().children().eq(seat.index()).trigger('click').focus();
			}
		};
		var moveDown = function(seat){
			if( ! seat.parent().is(':last-child') ){
				seat.parent().next().children().eq(seat.index()).trigger('click').focus();
			}
		};

		var confirmSeat = function(){
			if(!$('*:focus').hasClass('not-available')){
				btnNext.trigger('click');
			}
		};

		var overrideKeys = function(){

			$(document).off('keydown').on('keydown', function(e){

				var keyCode = e.keyCode || e.which || e.charCode;
				var activeSeat = $('td.active', $calendar);

				if( $.inArray(keyCode, allowKeyCode) >= 0 ){
					if(activeSeat.length){
						switch(keyCode){
							case 39:
								moveRight(activeSeat);
								break;
							case 37:
								moveleft(activeSeat);
								break;
							case 38:
								moveTop(activeSeat);
								break;
							case 40:
								moveDown(activeSeat);
								break;
							case 13:
								confirmSeat(seatDepart, seatReturn, seatAmount);
								break;
							default:
								//console.log('Default');
								break;
						}

					}else{
						$calendar.find('tr:nth-child(2) th + td').trigger('click');
					}
					e.preventDefault();
				}

			});
		};

	};

	keyboardNavigation();

	$calendar.data('currentCellDisplay', calcCellDisplay($calendar));
	checkNextBtn(dataChecked, true);
	changeUrlControl();
	appendAriaLabel();
	win.off('resize.calculateCellDisplay').on('resize.calculateCellDisplay', function(){
		var currentCellDisplay = calcCellDisplay($calendar);
		if($calendar.data('currentCellDisplay') !== currentCellDisplay) {
			checkNextBtn($calendar.data('checked'), false);
			changeUrlControl();
			$calendar.data('currentCellDisplay', currentCellDisplay);
		}
	});
};
