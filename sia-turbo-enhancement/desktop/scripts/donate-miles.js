/**
 * @name SIA
 * @description Define global donatemiles functions
 * @version 1.0
 */
SIA.donateMiles = function() {
  var global = SIA.global,
      formValidate = $('[data-donate-miles-form]'),
      directPage = $('[data-url]', formValidate);

  if(formValidate.length) {
    formValidate.validate({
      focusInvalid: true,
      errorPlacement: global.vars.validateErrorPlacement,
      success: global.vars.validateSuccess
    });
  }

  directPage.off('click.changeurlForm').on('click.changeurlForm', function(){
    if(directPage) {
      window.location.href = directPage.data('url');
    } else {
      window.history.back();
    }
  });
};
