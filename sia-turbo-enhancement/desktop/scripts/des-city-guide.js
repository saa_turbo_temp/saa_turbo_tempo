/**
 * @name SIA
 * @description
 * @version 1.0
 */
SIA.DESCityGuide = function() {
	var global = SIA.global,
			config = global.config,
			win = global.vars.win,
			blockSlider = $('#where-to-stay-slider'),
			wrapperSlider = blockSlider.parent(),
			imgSliderLength = blockSlider.find('img').length - 1,
			totalDesktopSlide = 2,
			totalTabletSlide = 3,
			slideMarginRight = 20;

	var wcagConfig = {
		headings: {
			whereToStaySlide: {
				el: $('.where-to-stay .place-1__title > p'),
				level: 4
			}
		}
	};

	var ratingToText = function(ratingBlock, textBlock) {

		var $ratingBlock = $(ratingBlock);
		var stars = $ratingBlock.find('.rated');
		var halfStar = $ratingBlock.find('.half-rated');
		var label = stars.length + ( halfStar.length ? L10n.desCityGuide.rating.starAndAHalf : '') + ( stars.length > 1 ? L10n.desCityGuide.rating.starPlural : L10n.desCityGuide.rating.starSingle );

		/* Fails in FF/NVDA
		$ratingBlock.attr({
			'role': 'text',
			'aria-label': label
		});
		*/

		$(textBlock)
			.append('<p class="ui-helper-hidden-accessible">' + label + '</p>');
	};

	var loadBackgroundSlider = function(self, parentSelf, idx) {
		if(global.vars.detectDevice.isTablet()) {
			parentSelf.css({
				'background-image': 'url(' + self.attr('src') + ')'
			});
			self.attr('src', config.imgSrc.transparent);
		}

		if(idx === imgSliderLength) {
			blockSlider.width(wrapperSlider.width() + slideMarginRight);
			blockSlider.css('visibility', 'visible');
			blockSlider.find('.slides')
				.on('init', function() {

					var ratingBlocks = $(this).find('.rating-block');
					var textBlock = $(this).find('.place-1__text');

					ratingBlocks.each( function(i) {
						ratingToText( ratingBlocks[i], textBlock[i] );
					});
				})
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: totalDesktopSlide,
					slidesToScroll: totalDesktopSlide,
					useCSS: global.vars.isNewIE() ? false : true,
					// start wcag opts
					accessibility: true,
					assistiveTechnology: true,
					customPaging: function(slider, i) {
						var index = (i+1);
						return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.whereToStay.customPagingTextPre + index + L10n.sliders.whereToStay.customPagingTextPost);
					},
					pauseOnFocus: true,
					focusOnSelect: true,
					pauseOnHover: true,
					// end wcag opts
					responsive: [
						{
							breakpoint: config.tablet,
							settings: {
								slidesToShow: totalTabletSlide,
								slidesToScroll: totalTabletSlide
							}
						}
					]
				});

			win.off('resize.blockSlider').on('resize.blockSlider',function() {
				blockSlider.width(wrapperSlider.width() + slideMarginRight);
			}).trigger('resize.blockSlider');
		}
	};

	blockSlider.find('img').each(function(idx) {
		var self = $(this),
				parentSelf = self.parent(),
				newImg = new Image();

		newImg.onload = function() {
			loadBackgroundSlider(self, parentSelf, idx);
		};
		newImg.src = self.attr('src');
	});
	var assignHeadings = function(){
		if (!$('body').hasClass('city-guide-page')){
			return;
		}
		$.each(wcagConfig.headings, function(){
			// every label requires a label
			if(!this.el || !this.level){
				return;
			}
			this.el.attr({
				'role': 'heading',
				'aria-level': this.level
			});
		});
	};
	assignHeadings();
};
