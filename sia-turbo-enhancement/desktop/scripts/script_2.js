'use strict';
/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
var SIA = SIA || {};

SIA.global = (function() {

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	var paramUrl = sURLVariables[1];
	var sName = paramUrl ? paramUrl.split('=')[1] : '';
	var getURLParams = function(sParam) {
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1];
			}
		}
	};


	// configuration
	var config = {
		width: {
			docScroll: window.Modernizr.touch || window.navigator.msMaxTouchPoints ? 0 : 20,
			datepicker: 14
		},
		mobile: 768,
		tablet: 988,
		formatDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		template: {
			overlay: '<div class="overlay"></div>',
			error: '<em class="ico-error">error</em>',
			// labelError: '<span class="text-error">Lorum ipsum lorum ipsum</span>',
			labelError: '<p class="text-error"><span>Lorum ipsum lorum ipsum</span></p>',
			success: '<em class="ico-success">success</em>',
			flags: '<img src="images/transparent.png" alt="" class="flags {0}">',
			addEmail: '<div data-validate-row="true" class="table-row"><div data-validate-col="true" class="table-col table-col-1"><span>{0}.</span></div><div data-validate-col="true" class="table-col table-col-2"><div class="table-inner"><span class="input-1"><input type="email" name="email-address-{0}" id="email-address-{0}" placeholder="Email address" value="" data-rule-email="true" data-msg-email="{1}"></span></div></div></div>',
			addEmailConfirm: '<div class="table-row"><div class="table-col table-col-1"><span>{0}.</span></div><div class="table-col table-col-2"><div class="table-inner"><span class="input-1 disabled"><input type="email" name="email-confirm-input-01" value="{1}" id="email-confirm-input-{0}" readonly="readonly"></span></div></div></div>',
			AddSMSSuccessfullySent: '<div class="table-row"> <div class="table-col table-col-1"><span>{0}.</span></div> <div class="table-col table-col-2"> <div class="table-inner"> <span class="input-1 disabled"> <input type="text" name="country-name-{0}" value="{1}" id="country-name-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-3"> <div class="table-inner"> <span class="input-1 disabled"> <input type="tel" name="area-code-{0}" value="{2}" id="area-code-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-4"> <div class="table-inner"> <span class="input-1 disabled"> <input type="text" name="phone-number-{0}" value="{3}" id="phone-number-{0}" readonly="readonly"></span> </div> </div> </div>',
			loadingStatus: '<div class="loading-block"><img src="images/ajax-loader-ie.gif" alt="loading" longdesc="img-desc.html"></div>',
			loadingMedium: '<div class="loading loading--medium">Loading</div>',
			loadingSmall: '<div class="loading loading--small">Loading</div>'
		},
		zIndex: {
			datepicker: 1004,
			ppLanguage: 100,
			tabContentOverlay: 14,
			overlayPopup: 14
		},
		duration: {
			overlay: 400,
			menu: 400,
			languagePopup: 400,
			searchPopup: 400,
			newsTicker: {
				auto: 2000,
				animate: 1000
			},
			clearTimeout: 100,
			bookingWidget: 400,
			popupSearch: 400,
			flightStatus: 400,
			popupGesture: 400,
			popup: 400
		},
		imgSrc: {
			transparent: 'images/transparent.png'
		},
		url: {
			jsonHotelsResponse : 'ajax/Hotels_response_JSON.json',
			paxFakeData: 'ajax/pax-fake-data.json',
			simpleStickyTemplate: 'template/simple-sticky.tpl',
			stickyTemplate: 'template/sticky.tpl',
			barTemplate: 'template/bar.tpl',
			flightHistoryTemplate: 'template/flight-history.tpl',
			addOnMbTemplate: 'template/add-on-mb.tpl',
			addOnMbTemplateSales: 'template/add-on-mb-sales.tpl',
			addOnMbHotelTemplateSales: 'template/add-on-mb-hotel-sales.tpl',
			addOnAddedTemplateSales: 'template/add-on-added-sales.tpl',
			jsonAutocomplete: 'ajax/destinations-data.json',
			fareDealsJSON: 'ajax/fare-deals.json',
			templateFareDeal: 'template/fare-deals.tpl',
			// templateFareDeal2: 'template/fare-deals-v2.tpl',
			cabinSelectJSON: 'ajax/cabin-data.json',
			flightStatusJSON: 'ajax/ice-flight-status.json',
			flightStatusTemplate: 'template/flight-status.tpl',
			flightScheduleJSON: 'ajax/flight-schedule.json',
			flightScheduleTemplate: 'template/flight-schedule.tpl',
			promotionPageKrisflyerTemplate: 'template/promotion-krisflyer-list.tpl',
			promotionPageKrisflyerJSON: 'ajax/promotion-krisflyer-list.json',
			promotionPageTemplate: 'template/promotion-list.tpl',
			promotionEnhancePageTemplate: 'template/promotion-enhance-list.tpl',
			promotionPageJSON: 'ajax/promotion-fare-list.json',
			promotionPageJSONSeaMore: 'ajax/promotion-fare-list-1.json',
			promotionPageJSONSearch: 'ajax/promotion-fare-list-search.json',
			languageJSON: 'ajax/autocomplete-language.json',
			social: {
				facebookSharing: 'https://www.facebook.com/sharer.php?u=',
				twitterSharing: 'https://twitter.com/share?url=',
				gplusSharing: 'https://plus.google.com/share?url='
			},
			templateCibFareCalendar: 'template/cib-fare-calendar-multi-city.tpl',
			CIBFlightSearch1Template: 'template/cib-flight-search.tpl',
			CIBFlightSearch1UpgradeTemplate: 'template/flight-upgrade.tpl',
			flightSearchFare1JSON: 'ajax/cib-flight-search-fare-1.json',
			flightSearchFare2JSON: 'ajax/cib-flight-search-fare-2.json',
			flightSearchFareFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-search-fare-flight-info.json',
			flightSearchFare1UpgradeJSON: 'ajax/cib-flight-search-fare-1-upgrade.json',
			flightSearchFare2UpgradeJSON: 'ajax/cib-flight-search-fare-2-upgrade.json',
			kfTableTemplate: 'template/how-to-earn-kf-table.tpl',
			kfTableJSON: 'ajax/how-to-earn-kf-table.json',
			kfAtAGlanceTemplate: 'template/kf-at-a-glance.tpl',
			kfAtAGlanceTemplate01: 'template/kf-at-a-glance-01.tpl',
			kfAtAGlanceTemplateButton: 'template/kf-at-a-glance-button.tpl',
			kfAtAGlanceTemplateSf: 'template/kf-at-a-glance-sf.tpl',
			// kfAtAGlanceJSON: 'ajax/kf-at-a-glance.json',
			kfUseTableTemplate: 'template/how-to-use-kf-table.tpl',
			kfUseTableJSON: 'ajax/how-to-use-kf-table.json',
			kfMessageMarkJSON: 'ajax/kf-message-mark.json',
			kfMessageDeleteJSON: 'ajax/kf-message-delete.json',
			kfMessageItemsJSON: 'ajax/kf-message-items.json',
			kfMessageItemsTabletJSON: 'ajax/kf-message-items-tablet.json',
			kfMessageItemTemplate: 'template/kf-message-item.tpl',
			kfBookingUpcomingJSON: 'ajax/kf-booking-upcoming-flights.json',
			kfBookingUpcomingTemplate: 'template/kf-booking-upcoming-flights.tpl',
			kfFlightHistoryJSON: 'ajax/kf-booking-flight-history.json',
			kfFlightHistoryTemplate: 'template/kf-booking-flight-history.tpl',
			kfPastFlightHistoryTemplate: 'template/kf-booking-past-flight.tpl',
			kfCheckInJSON: 'ajax/kf-check-in.json',
			kfCheckInTemplate: 'template/kf-check-in.tpl',
			langToolbarTemplate: 'template/language-toolbar.tpl',
			orbFlightResultTemplate: 'template/orb-flight-result.tpl',
			orbFlightResultJSON: 'ajax/orb-flight-result.json',
			orbFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-search-flight-info.json',
			orbWaitlistedFlightJSON: 'ajax/orb-waitlisted-flight.json',
			cibFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select.json',
			cibFlightSelectSf: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select-sf.json',
			cibFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select-on-change.json',
			orbFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select.json',
			orbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select-on-change.json',
			mbFlightSelect: $('body').hasClass('mb-payments-excess-baggage-page') ? 'ajax/mb-flight-select-excess-baggage.json' : getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select.json',
			mbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select-on-change.json',
			kfNumberNomineeJSON: getURLParams('nominne') ? ('ajax/JSONS/' + sName) : 'ajax/kf-number-nominne.json',
			mb: {
				selectMeal: 'template/mb-select-meals.tpl',
				selectMealSidebar: 'template/mb-select-meals-sidebar.tpl',
				selectFlightMeal: 'template/mb-flight-select-meals.tpl',
				selectFlightMealSidebar: 'template/mb-flight-select-meals-sidebar.tpl',
				selectFlightMealPopupMenu: 'template/mb-flight-select-meals-popup.tpl'
			},
			cibBookingSummaryDetailsPopupTemplate: 'template/booking-summary-details-popup.tpl',
			cibBookingSummaryDetailsPopupTemplateAddOn: 'template/booking-summary-details-popup-addon.tpl',
			cibBookingSummarySfDetailsPopupTemplate: 'template/booking-summary-details-popup-sf.tpl',
			orbBookingSummaryDetailsPopupTemplate: 'template/orb-booking-summary-details-popup.tpl',
			bookingFlightConfirm: 'ajax/booking-flight-confirm.json',
			kfMilesTemplate: 'template/kf-miles.tpl',
			kfFavorite: 'template/kf-favourite.tpl',
			kfStatement: 'template/kf-statement.tpl',
			preferSeatContent: 'template/prefer-seat-content.tpl',
			kfSubBookingUpcomingFlights: 'template/kf-booking-upcoming-flights-01.tpl',
			kfExtendMileJSON: 'ajax/kf-extend-miles.json',
			orbFlightSchedule: 'template/orb-flight-schedule.tpl',
			sqcUpcomingFlightTemplate: 'template/sqc-upcoming-flight.tpl',
			sqcUpcomingFlightDetailTemplate: 'template/sqc-upcoming-flight-detail.tpl',
			sqcUserDelete: 'ajax/sqc-user-delete.json',
			sqcSalePointDelete: 'ajax/sqc-sale-point-delete.json',
			sqcCheckPIN: 'ajax/sqc-check-PIN.json',
			sqc: {
				atGlanceTemplate: 'template/sqc-at-a-glance.tpl',
				atGlanceDetailTemplate: 'template/sqc-at-a-glance-detail.tpl',
				atGlanceJSON: 'ajax/sqc-at-a-glance.json'
			},
			sqcSavedTripsTemplate: 'template/sqc-saved-trips.tpl',
			ssh: {
				hotel: {
					json: 'ajax/ssh-hotel.json',
					template: 'template/ssh-hotel.tpl'
				}
			},
			success: 'ajax/success.json',
			bookingDetailTemplate: 'template/booking-detail.tpl',
			bookingDetailMealTemplate: 'template/booking-detail-meal.tpl',
			bookingDetailBagTemplate: 'template/booking-detail-baggage.tpl',
			excessBaggageTemplate: 'template/booking-detail-excess-baggage.tpl',
			mealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-meal.json',
			mealFlightJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-meal_2.json',
			mealFlightPopupJSON: getURLParams('mealMenu') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-popup-meal.json',
			manageBookingMealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/manage-booking-meal.json',
			excessMealJSON: 'ajax/booking-detail-excess-meal.json',
			baggageJSON: getURLParams('baggage') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-baggage.json',
			excessBaggageJSON: 'ajax/booking-detail-excess-baggage.json',
			desEntryTemplate: 'template/des-entry.tpl',
			desEntryJSON: 'ajax/des-entry.json',
			staticMoviesTemplate: 'template/static-content-movies.tpl',
			staticMoviesJSON: 'ajax/static-content-movies.json',
			staticMusicTemplate: 'template/static-content-music.tpl',
			staticMusicJSON: 'ajax/static-content-music.json',
			kfVoucherRedemptionJSON: 'ajax/kf-voucher-redemption.json',
			kfVoucherSummaryJSON: 'ajax/how-to-use-kf-voucher.json',
			kfVoucherRedemptionTemplate: 'template/kf-voucher-redemption.tpl',
			kfVoucherRedemptionReviewTemplate: 'template/kf-voucher-redemption-review.tpl',
			videoLightbox: {
				youtube: 'template/youtube-template-lightbox.tpl',
				flowplayer: 'template/flowplayer-template-lightbox.tpl'
			},
			addBaggagePopup: {
				template: 'template/add-baggage-popup.tpl',
				json: 'ajax/add-baggage-popup.json'
			},
			promotionsPackages: {
				json: 'ajax/pormotions-packages.json',
				template: 'template/pormotions-packages.tpl'
			},
			countryCityAutocomplete: 'ajax/country-city.json',
			faredealPromotion: {
				// faredealMultiPriceJson: 'ajax/faredeal-multiple-price.json',
				// faredealMultiPriceSeemoreJson: 'ajax/faredeal-multiple-price-seemore.json',
				// pricePointsJson: 'ajax/pormotions-price-points.json',
				bookingWidgetJson: 'ajax/booking-widget.json'
			},
			'hotelAvailabilityJson': 'ajax/Hotel_Availability_Response.json',
			addons: {
				hotel: {
					json: 'ajax/Hotel_Availability_Response.json',
					jsonmb: 'ajax/Hotel_Availability_Response.json',
					template: 'template/add-on-hotel-list.tpl'
				}
			}
		},
		highlightSlider: {
			desktop: 4,
			tablet: 3,
			mobile: 1
		},
		datepicker: {
			numberOfMonths: ((window.Modernizr.touch || window.navigator.msMaxTouchPoints) && ($(window).width() < 768)) ? 1 : 2
		},
		seatMap: {
			template: {
				cabin: '<div class="seatmap-cabin"></div>',
				space: '<div class="seatmap-cabin-separate"></div>',
				sLabelWrapper: '<div class="seatmap-cabin-row seatmap-toprow"></div>',
				seatWrapper: '<div class="seatmap-cabin-wrapper"></div>',
				sLabel: '<div data-sia-rowblock="{0}" class="seatmap-columnletter">{1}</div>',
				blk: '<div class="seatmap-row-block"></div>',
				aisle: '<div class="seat-aisle"></div>',
				seatRow: '<div class="seatmap-cabin-row"><span class="seatmap-rownum left">{0}</span><span class="seatmap-rownum right">{0}</span></div>',
				seatColumn: '<div data-sia-rowblock="{0}" class="seatmap-row-block"></div>',
				inforSeat: '<span class="passenger-info__seat"></span>'
			},
			flightClass: {
				economy: 'Economy Class',
				business: 'Business Class'
			},
			seat: {
				available: 'ico-seat-available',
				empty: 'ico-seat-empty',
				occupied: 'ico-seat-occupied',
				bassinet: 'ico-1-sm-bassinet',
				blocked: 'ico-seat-blocked'
			},
			url: {
				seatMap: 'template/seatmap-foundation.html',
				seatMapJSON: 'ajax/seatmap_sample.json',
				seatMapJSON1: 'ajax/seatmap_sample-1.json'
			},
			passenger: 2
		},
		ajaxMethod: 'get',
		map: {
			googlemap: {
				apiKey: 'AIzaSyA8UdAEjReJYw6GTXdTqBsIxHwRA738xTo',
				lib: 'places',
				zoom: 14,
				radius: 1000
			},
			baidumap: {
				apiKey: '11ipY4UjrfPkwsHPhpqWzeHvaWkzGyAL',
				zoom: 14,
				radius: 5000
			}
		}
	};

	if (!String.prototype.format) {
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) {
				return typeof args[number] !== 'undefined' ? args[number] : match;
			});
		};
	}
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		};
	}

	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) {
				if (this[i] === obj) {
					return i;
				}
			}
			return -1;
		};
	}


	// global variable
	var vars = {};
	var doc = $(document);
	var win = $(window);
	var body = $(document.body);
	var html = $('html');
	var menuHolder = $();
	var container = $('#container');
	var header = $('.header');
	var menuT = $('.menu');
	var menuBar = $('.menu-bar');
	var mainMenu = $('.menu-main');
	var popupSearch = $('.popup--search');
	var popupPromo = $('.popup--promo');
	var popupLogin = $('.popup--login');
	var ppLanguage = $('.popup--language');
	var loggedProfileSubmenu = $('.popup--logged-profile');
	var popupLoggedProfile = $();

	var languageJSON = {
		'data': [{
			'value': 'Global',
			'flag': 'global',
			'language': 'en_UK, zh_CN, fr_FR, pt_BR, de_DE, zh_TW, ja_JP, ko_KR, ru_RU, es_ES',
			'order': 1
		}, {
			'value': 'Australia',
			'flag': 'australia',
			'language': 'en_UK',
			'order': 2
		}, {
			'value': 'Austria',
			'flag': 'austria',
			'language': 'en_UK',
			'order': 3
		}, {
			'value': 'Bangladesh',
			'flag': 'bangladesh',
			'language': 'en_UK',
			'order': 4
		}, {
			'value': 'Belgium',
			'flag': 'belgium',
			'language': 'en_UK, fr_FR',
			'order': 5
		}, {
			'value': 'Brazil',
			'flag': 'brazil',
			'language': 'pt_BR, en_UK',
			'order': 6
		}, {
			'value': 'Brunei',
			'flag': 'brunei',
			'language': 'en_UK',
			'order': 7
		}, {
			'value': 'Cambodia',
			'flag': 'cambodia',
			'language': 'en_UK',
			'order': 8
		}, {
			'value': 'Canada',
			'flag': 'canada',
			'language': 'en_UK',
			'order': 9
		}, {
			'value': 'People\'s Republic Of China',
			'flag': 'people_republic_of_china',
			'language': 'zh_CN, en_UK',
			'order': 10
		}, {
			'value': 'Denmark',
			'flag': 'denmark',
			'language': 'en_UK',
			'order': 11
		}, {
			'value': 'Egypt',
			'flag': 'egypt',
			'language': 'en_UK',
			'order': 12
		}, {
			'value': 'France',
			'flag': 'france',
			'language': 'fr_FR, en_UK',
			'order': 13
		}, {
			'value': 'Germany',
			'flag': 'germany',
			'language': 'de_DE, en_UK',
			'order': 14
		}, {
			'value': 'Greece',
			'flag': 'greece',
			'language': 'en_UK',
			'order': 15
		}, {
			'value': 'Hong Kong',
			'flag': 'hong_kong',
			'language': 'en_UK, zh_TW',
			'order': 16
		}, {
			'value': 'India',
			'flag': 'india',
			'language': 'en_UK',
			'order': 17
		}, {
			'value': 'Indonesia',
			'flag': 'indonesia',
			'language': 'en_UK',
			'order': 18
		}, {
			'value': 'Ireland',
			'flag': 'ireland',
			'language': 'en_UK',
			'order': 19
		}, {
			'value': 'Italy',
			'flag': 'italy',
			'language': 'en_UK',
			'order': 20
		}, {
			'value': 'Japan',
			'flag': 'japan',
			'language': 'ja_JP, en_UK',
			'order': 21
		}, {
			'value': 'Kuwait',
			'flag': 'kuwait',
			'language': 'en_UK',
			'order': 22
		}, {
			'value': 'Laos',
			'flag': 'laos',
			'language': 'en_UK',
			'order': 23
		},{
			'value': 'Luxembourg',
			'flag': 'luxembourg',
			'language': 'en_UK',
			'order': 24
		}, {
			'value': 'Malaysia',
			'flag': 'malaysia',
			'language': 'en_UK',
			'order': 25
		}, {
			'value': 'Maldives',
			'flag': 'maldives',
			'language': 'en_UK',
			'order': 26
		}, {
			'value': 'Nepal',
			'flag': 'nepal',
			'language': 'en_UK',
			'order': 27
		}, {
			'value': 'Netherlands',
			'flag': 'netherlands',
			'language': 'en_UK',
			'order': 28
		}, {
			'value': 'New Zealand',
			'flag': 'new_zealand',
			'language': 'en_UK',
			'order': 29
		}, {
			'value': 'Norway',
			'flag': 'norway',
			'language': 'en_UK',
			'order': 30
		}, {
			'value': 'Philippines',
			'flag': 'philippines',
			'language': 'en_UK',
			'order': 31
		}, {
			'value': 'Republic of Korea',
			'flag': 'south_korea',
			'language': 'ko_KR, en_UK',
			'order': 32
		}, {
			'value': 'Russia',
			'flag': 'russia',
			'language': 'ru_RU, en_UK',
			'order': 33
		}, {
			'value': 'Saudi Arabia',
			'flag': 'saudia_arabia',
			'language': 'en_UK',
			'order': 34
		}, {
			'value': 'Singapore',
			'flag': 'singapore',
			'language': 'en_UK, zh_CN',
			'order': 35
		}, {
			'value': 'South Africa',
			'flag': 'south_africa',
			'language': 'en_UK',
			'order': 36
		}, {
			'value': 'Spain',
			'flag': 'spain',
			'language': 'es_ES, en_UK',
			'order': 37
		}, {
			'value': 'Sri Lanka',
			'flag': 'sri_lanka',
			'language': 'en_UK',
			'order': 38
		}, {
			'value': 'Sweden',
			'flag': 'sweden',
			'language': 'en_UK',
			'order': 39
		}, {
			'value': 'Switzerland',
			'flag': 'switzerland',
			'language': 'en_UK, de_DE',
			'order': 40
		}, {
			'value': 'Taiwan',
			'flag': 'taiwan',
			'language': 'zh_TW, en_UK',
			'order': 41
		}, {
			'value': 'Thailand',
			'flag': 'thailand',
			'language': 'en_UK',
			'order': 42
		}, {
			'value': 'Turkey',
			'flag': 'turkey',
			'language': 'en_UK',
			'order': 43
		}, {
			'value': 'United Arab Emirates',
			'flag': 'united_arab_emirates',
			'language': 'en_UK',
			'order': 44
		}, {
			'value': 'United Kingdom',
			'flag': 'united_kingdom',
			'language': 'en_UK',
			'order': 45
		}, {
			'value': 'United States',
			'flag': 'united_states',
			'language': 'en_UK',
			'order': 46
		}, {
			'value': 'Vietnam',
			'flag': 'vietnam',
			'language': 'en_UK',
			'order': 47
		}]
	};

	// var isIpad = /iPad/i.test(window.navigator.userAgent);
	vars = {
		doc: doc,
		win: win,
		body: body,
		html: html,
		menuT: menuT,
		mainMenu: mainMenu,
		menuBar: menuBar,
		container: container,
		header: header,
		popupPromo: popupPromo,
		ppLanguage: ppLanguage,
		languageJSON: languageJSON,
		popupLoggedProfile: popupLoggedProfile,
		loggedProfileSubmenu: loggedProfileSubmenu,
		validationRuleByGroup: [
			'validateDate',
			'required_issue_place',
			'checkofadult',
			'checkofchild',
			'checkofinfant',
			'checkCurrentDate',
			'checkpassport',
			'validateAtLeastOne',
			'notEqualTo'
		]
	};

	if (body.is('#template')) {
		return;
	}

	vars.isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};

	vars.isIETouch = function() {
		var nav = window.navigator;
		return (nav.msPointerEnabled && nav.msMaxTouchPoints > 1) ||
			(nav.pointerEnabled && nav.maxTouchPoints > 1);
	};

	vars.isNewIE = function() {
		var isGreaterIE9 = false;
		if (window.navigator.msPointerEnabled) {
			isGreaterIE9 = true;
		}
		return isGreaterIE9;
	};

	vars.ieVersion = function() {
		// var rv = -1; // Return value assumes failure.
		// if (navigator.appName === 'Microsoft Internet Explorer') {
		// 	var ua = navigator.userAgent;
		// 	var re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})');
		// 	if (re.exec(ua) !== null) {
		// 		rv = parseFloat(RegExp.$1);
		// 	}
		// }
		// return rv;

		var rv = -1;
		if (navigator.appName === 'Microsoft Internet Explorer') {
			var ua = navigator.userAgent;
			var re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})');
			if (re.exec(ua) !== null) {
				rv = parseFloat(RegExp.$1);
			}
		} else if (navigator.appName === 'Netscape') {
			var ua = navigator.userAgent;
			var re = new RegExp('Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})');
			if (re.exec(ua) !== null) {
				rv = parseFloat(RegExp.$1);
			}
		}
		return rv;
	};

	vars.detectDevice = {
		isMobile: (function() {
			return function() {
				return ($(window).width() < 768);
			};
		})(),
		isTablet: (function() {
			return function() {
				return ((win.width() <= config.tablet && win.width() >= config.mobile));
			};
		})()
	};

	vars.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

	vars.addClassOnIE = function(els, con, n) {
		switch (con) {
			case 'both':
				els.each(function(idx) {
					if (idx) {
						$(this).addClass('nth-child-' + (idx + 1));
					}
					if ($(this).is(':last')) {
						$(this).addClass('last-child');
					}
				});
				break;
			case 'last':
				els.each(function() {
					if ($(this).is(':last')) {
						$(this).addClass('last-child');
					}
				});
				break;
			case '2':
				els.each(function(idx) {
					if (idx === (con - 1)) {
						$(this).addClass('nth-child-' + con);
					}
				});
				break;
			case 'option':
				els.each(function(idx) {
					if (idx < n) {
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
				break;
			case 'all':
				els.each(function(idx) {
					$(this).addClass('nth-child-' + (idx + 1));
				});
				break;
			default:
				els.each(function(idx) {
					if (idx) {
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
		}
	};

	var addClassForSafari = function() {
		// if(isSafari && (!window.Modernizr.touch && !window.navigator.msMaxTouchPoints)){
		if (vars.isSafari) {
			body.addClass('safari');
		}
	};

	var centerPopup = function(el, visible) {
		el.css({
			top: Math.max(0, (win.height() - el.innerHeight()) / 2) + win.scrollTop(),
			display: (visible ? 'block' : 'none'),
			//left: detectDevice.isMobile ? 0 : Math.max(0, (win.width() - el.innerWidth()) / 2)
			left: Math.max(0, (win.width() - el.innerWidth()) / 2)
		});
	};

	var detectHidePopup = function(nothide) {
		if (menuHolder.length) {
			if (menuHolder.is('.popup--language')) {
				if (!vars.isIE()) {
					menuHolder.find('.custom-select input').blur();
				} else {
					// doc.trigger('click.hideAutocompleteCity');
					menuHolder.find('.custom-select input').closest('.custom-select').removeClass('focus');
					menuHolder.find('.custom-select input').autocomplete('close');
				}
				menuHolder.data('triggerLanguage').trigger('click.showLanguage');
				win.off('resize.popupLanguage');
			} else if (!menuHolder.is('.popup--language')) {
				if (!nothide) {
					menuHolder.data('menu').children('a').trigger('click.showSubMenu');
				}
			}
		}
	};
	vars.detectHidePopup = detectHidePopup;

	doc.off('scroll.hideOverlay mousewheel.hideOverlay').on('scroll.hideOverlay mousewheel.hideOverlay', function() {
		detectHidePopup(true);
		hidePopupSearch();
	});

	var hideLoggedProfile = function() {
		var popupLoggedProfile = vars.popupLoggedProfile;
		if (popupLoggedProfile.length) {
			if (!popupLoggedProfile.hasClass('hidden')) {
				popupLoggedProfile.triggerPopup.removeClass('active');
				if (window.Modernizr.cssanimations) {
					popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
					popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
						popupLoggedProfile.overlay.remove();
						popupLoggedProfile.overlay = $();
						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				} else {
					popupLoggedProfile.fadeOut(config.duration.popupSearch, function() {
						popupLoggedProfile.addClass('hidden');
					});
					popupLoggedProfile.overlay.fadeOut(config.duration.overlay, function() {
						popupLoggedProfile.overlay.remove();
						popupLoggedProfile.overlay = $();
					});
				}
				win.off('resize.LoggedProfile');
			}
		}
	};
	var hidePopupSearch = function(disableHideProfile) {
		if (!popupSearch.length) {
			return;
		}
		if (!popupSearch.hasClass('hidden')) {
			popupSearch.find('input:text').data('ui-autocomplete').menu.element.hide();
			popupSearch.triggerPopup.removeClass('active');
			if (window.Modernizr.cssanimations) {
				popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
				popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
					popupSearch.removeClass('animated fadeOut').addClass('hidden');
					popupSearch.overlay.remove();
					popupSearch.overlay = $();
					popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
			} else {
				popupSearch.fadeOut(config.duration.popupSearch, function() {
					popupSearch.addClass('hidden');
				});
				popupSearch.overlay.fadeOut(config.duration.overlay, function() {
					popupSearch.overlay.remove();
					popupSearch.overlay = $();
				});
			}
			win.off('resize.inputSearch');
		}
		if (!disableHideProfile) {
			hideLoggedProfile();
		}
	};
	vars.hidePopupSearch = hidePopupSearch;

	// create a gesture for popup on M and T
	var popupGesture = function(popup, trigger, pClose, pContent, notTablet, callback) {
		var scrollTopHolder = 0;
		var overlayPopup = null;
		var openOnTablet = false;
		var openOnMobile = false;
		var timerPopupGesture = null;
		var winW = win.width();
		var popupOnTablet = function() {
			centerPopup(popup, true, 0);
			popup.addClass('animated fadeIn');
			container.css({
				'marginTop': -scrollTopHolder,
				'height': win.height() + scrollTopHolder,
				'overflow': 'hidden'
			});
			popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				if (popup.outerHeight(true) > win.height()) {
					popup.find(pContent).removeAttr('style').css({
						height: win.height() - parseInt(popup.find(pContent).css('padding-top')),
						'overflow-y': 'auto'
					});
					// win.scrollTop(0);
				}
				openOnTablet = true;
				popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		};
		// var popupOnMobile = function(){
		// 	popup.show();
		// 	if(popup.outerHeight(true) > win.height()){
		// 		// popup.find(pContent).css({
		// 		// 	height: win.height() - parseInt(popup.find(pContent).css('padding')),
		// 		// 	'overflow-y': 'auto'
		// 		// });
		// 		container.css('height', popup.outerHeight(true));
		// 	}
		// 	else{
		// 		container.css('height', win.height());
		// 		popup.height(win.height());
		// 	}
		// 	// centerPopup(popup, true, 0);
		// 	popup.addClass('animated slideInRight');
		// 	popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
		// 		container.css('overflow', 'hidden');
		// 		popup.css('top', '');
		// 		openOnMobile = true;
		// 		win.scrollTop(0);
		// 		popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
		// 	});
		// };

		var onResize = function() {
			if (winW === win.width() && (window.navigator.msMaxTouchPoints || window.Modernizr.touch)) {
				return;
			}
			winW = win.width();
			popup.css('height', '');
			popup.find(pContent).css('height', '');
			// container.css('height', '');
			if (popup.outerHeight(true) > win.height()) {
				if (openOnMobile && win.width() < config.mobile) {
					container.css('height', popup.outerHeight(true));
					container.css('overflow', 'hidden');
				}
				if (openOnTablet && win.width() > config.mobile) {
					popup.find(pContent).removeAttr('style').css({
						height: win.height() - parseInt(popup.find(pContent).css('padding-top')),
						'overflow-y': 'auto'
					});
				}
			} else {
				if (openOnMobile && win.width() < config.mobile) {
					container.css('height', win.height());
					popup.height(win.height());
					container.css('overflow', 'hidden');
				}
				if (openOnTablet && win.width() > config.mobile) {
					popup.find(pContent).removeAttr('style');
				}
			}
			if (win.width() >= config.tablet - config.width.docScroll) {
				overlayPopup.remove();
				popup.removeClass('animated fadeIn').removeAttr('style');
				container.removeAttr('style');
				win.scrollTop(scrollTopHolder);
				scrollTopHolder = 0;
				win.off('resize.popupGesture');
				return;
			}
			popup.show();
			if (openOnMobile && win.width() > config.mobile) {
				popup.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
				popup.find(pContent).css('height', '');
				container.removeAttr('style');
				win.scrollTop(scrollTopHolder);
				if (notTablet) {
					overlayPopup.remove();
					popup.hide();
					win.off('resize.popupGesture');
				} else {
					popupOnTablet();
				}
				openOnMobile = false;
				openOnTablet = true;
			}
			// if(openOnTablet && win.width() < config.mobile){
			// 	popup.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
			// 	popup.find(pContent).css('height', '');
			// 	container.removeAttr('style');
			// 	win.scrollTop(scrollTopHolder);
			// 	popup.css('left', '');
			// 	popup.css('top', '');
			// 	popupOnMobile();
			// 	openOnTablet = false;
			// 	openOnMobile = true;
			// }
		};
		trigger.off('click.showPopup').on('click.showPopup', function(e) {
			e.preventDefault();
			winW = win.width();
			if (callback) {
				callback($(this));
			}
			if (win.width() < config.tablet - config.width.docScroll) {
				if (window.Modernizr.csstransitions) {
					scrollTopHolder = win.scrollTop();
					popup.show();
					overlayPopup = $(config.template.overlay).appendTo(body).show().css({
						'zIndex': config.zIndex.overlayPopup
					}).addClass('animated fadeInOverlay');
					// if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
					// 	popupOnMobile();
					// }
					// else{ // tablet
					// 	popupOnTablet();
					// 	if(isIpad){
					// 		overlayPopup.css('height', win.height());
					// 	}
					// }
					popupOnTablet();
				} else {
					scrollTopHolder = win.scrollTop();
					overlayPopup = $(config.template.overlay).appendTo(body).hide().css({
						'zIndex': config.zIndex.overlayPopup
					}).fadeIn(config.duration.overlay).addClass('animated fadeInOverlay');
					centerPopup(popup, true, 0);
					popup.addClass('animated fadeIn');
					container.css({
						'marginTop': -scrollTopHolder,
						'height': win.height() + scrollTopHolder,
						'overflow': 'hidden'
					});
					popup.fadeIn(config.duration.popup, function() {
						if (popup.outerHeight(true) > win.height()) {
							popup.find(pContent).removeAttr('style').css({
								height: win.height() - parseInt(popup.find(pContent).css('padding-top')),
								'overflow-y': 'auto'
							});
							// win.scrollTop(0);
						}
						openOnTablet = true;
					});
				}
				win.off('resize.popupGesture').on('resize.popupGesture', function() {
					clearTimeout(timerPopupGesture);
					timerPopupGesture = setTimeout(function() {
						onResize();
					}, 150);
				});
				overlayPopup.off('click.closePopupTrigger').on('click.closePopupTrigger', function(e) {
					e.preventDefault();
					popup.find(pClose).eq(0).trigger('click.closePopup');
				});
			}
		});
		popup.find(pClose).off('click.closePopup').on('click.closePopup', function(e) {
			e.preventDefault();
			// var langToolbarEl = $('.toolbar--language');
			win.off('resize.popupGesture');
			if (win.width() < config.tablet - config.width.docScroll) {
				if (window.Modernizr.csstransitions) {
					// if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
					// 	overlayPopup.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					// 	popup.addClass('animated slideOutRight');
					// 	popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					// 		container.removeAttr('style');
					// 		win.scrollTop(scrollTopHolder);
					// 		popup.removeClass('animated slideOutRight slideInRight');
					// 		overlayPopup.off('click.closePopup').remove();
					// 		scrollTopHolder = 0;
					// 		popup.removeAttr('style');
					// 		popup.find(pContent).removeAttr('style');
					// 		popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					// 	});
					// }
					// else{
					// 	overlayPopup.addClass('animated fadeOutOverlay');
					// 	popup.addClass('animated fadeOut');
					// 	win.scrollTop(scrollTopHolder);
					// 	scrollTopHolder = 0;
					// 	popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					// 		container.removeAttr('style');
					// 		overlayPopup.off('click.closePopup').remove();
					// 		popup.removeClass('animated fadeOut fadeIn');
					// 		popup.removeAttr('style');
					// 		popup.find(pContent).removeAttr('style');
					// 		popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					// 	});
					// }
					overlayPopup.addClass('animated fadeOutOverlay');
					popup.addClass('animated fadeOut');
					popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						container.removeAttr('style');
						// if(langToolbarEl.length) {
						// 	container.css('paddingTop', langToolbarEl.height());
						// }
						win.scrollTop(scrollTopHolder);
						scrollTopHolder = 0;
						overlayPopup.off('click.closePopup').remove();
						popup.removeClass('animated fadeOut fadeIn');
						popup.removeAttr('style');
						popup.find(pContent).removeAttr('style');
						popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				} else {
					container.removeAttr('style');
					// if(langToolbarEl.length) {
					// 	container.css('paddingTop', langToolbarEl.height());
					// }
					win.scrollTop(scrollTopHolder);
					scrollTopHolder = 0;
					popup.fadeOut(config.duration.popup, function() {
						popup.removeClass('animated fadeOut fadeIn');
						popup.removeAttr('style');
						popup.find(pContent).removeAttr('style');
					});
					overlayPopup.fadeOut(config.duration.overlay, function() {
						overlayPopup.off('click.closePopup').remove();
					});
				}
			}
		});
	};
	//public popupGesture
	vars.popupGesture = popupGesture;

	// click document to hide popup
	container.off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
		if (!$(e.target).closest('.menu').length) {
			if (typeof window.resetMenuT !== 'undefined') {
				window.resetMenuT();
			}
		}
		if (!$(e.target).closest('.popup--search').length) {
			hidePopupSearch();
		}
	});

	var animationNavigationOnDesktop = function() {
		var menu = mainMenu.find('.menu-item');
		var menuUl = mainMenu.find('>ul');
		var menuUlInner = mainMenu.find('ul ul');
		var menuFirstLevelLink = mainMenu.find('>ul>li>a');
		mainMenu.parent().attr({
			'role':'application'
		});
		mainMenu.attr({
			'role':'navigation'
		});
		menuUl.attr({
			'role':'menubar'
		});
		menuUlInner.attr({
			'role':'menu'
		});
		menuFirstLevelLink.attr({
			'aria-haspopup':true
		});
		mainMenu.preventClick = false;
		menu.each(function() {
			var self = $(this);
			var realSub = self.children('.menu-sub'); // real sub
			var sub = self.children('.menu-sub'); // fake sub
			var timerResetMenu = null;

			if (sub.length) {

				if(!realSub.find('.sub-item').length && !realSub.children('.menu-sub__join').length) {
					self.children('a').find('.ico-point-r').addClass('hidden');
					return;
				}

				sub = sub.clone().addClass('menu-clone').removeClass('hidden').appendTo(body); // fake sub is created
				sub.data('menu', self);

				if (!window.Modernizr.cssanimations) {
					sub.hide();
				} else {
					sub.addClass('hidden');
				}

				var openNav = function(keyDown){
					mainMenu.preventClick = true;
					if(!keyDown){
						keyDown = false;
					}
					if (window.innerWidth >= config.tablet) {
						if(self.overlay){
							sub.addClass('hidden').removeClass('animated fadeOut');
							self.overlay.remove();
							sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
							mainMenu.preventClick = false;
						}
					}
					self.overlay = $(config.template.overlay).height(doc.height()).appendTo(body);
					self.overlay.off('click.hideOverlay').on('click.hideOverlay', function() {
						closeNav();
					});
					if (window.Modernizr.cssanimations) {
						self.overlay.hide();
						sub.removeClass('hidden').removeClass('fadeOut').addClass('animated fadeIn');
						self.overlay.show().addClass('animated fadeInOverlay');
						sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							menuHolder = sub;
							mainMenu.preventClick = false;
							sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
						focusInSubMenu(sub, keyDown);
					} else {
						sub.removeClass('hidden').hide().fadeIn(config.duration.menu).addClass('fadeIn');
						if (vars.isIE() && vars.isIE() < 8) {
							self.overlay.css({
								top: header.height()
							});
						}
						self.overlay.fadeIn(config.duration.overlay, function() {
							menuHolder = sub;
							mainMenu.preventClick = false;
						});
					}
					self.addClass('active');
					header.addClass('active');
					win.off('resize.resetSub').on('resize.resetSub', function() {
						clearTimeout(timerResetMenu);
						timerResetMenu = setTimeout(function() {
							if (win.width() < config.tablet - config.width.docScroll) {
								self.overlay.remove();
								self.removeClass('active');
								header.removeClass('active');
								sub.addClass('hidden').removeClass('animated fadeOut fadeIn');
								menuHolder = $();
								win.off('resize.resetSub');
							}
						}, config.duration.clearTimeout);
					});
				};

				var closeNav = function() {
					if (window.Modernizr.cssanimations) {
						sub.removeClass('fadeIn').addClass('fadeOut');
						mainMenu.preventClick = true;
						sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							sub.addClass('hidden').removeClass('animated fadeOut');
							self.overlay.remove();
							sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
							mainMenu.preventClick = false;
						});
						self.overlay.removeClass('fadeInOverlay').addClass('fadeOutOverlay');
					} else {
						mainMenu.preventClick = true;
						sub.fadeOut(config.duration.menu).removeClass('fadeIn');
						self.overlay.fadeOut(config.duration.overlay, function() {
							$(this).remove();
							mainMenu.preventClick = false;
						});
					}
					self.removeClass('active');
					menuHolder = $();
					header.removeClass('active');
					win.off('resize.resetSub');
				};

				var beforeShowHideMenu = function(){
					if (menuHolder.length) {
						if (menuHolder.is('.popup--language')) {
							menuHolder.data('triggerLanguage').trigger('click.showLanguage');
						}
					}
					if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
						vars.popupPromo.Popup('hide');
					}
					if (vars.popupPromo.length && popupLogin.length && popupLogin.data('Popup').element.isShow) {
						popupLogin.Popup('hide');
					}
					// hide menu if there is a its siblings opened
					if ($('.menu-sub.menu-clone.fadeIn').length && !$('.menu-sub.menu-clone.fadeIn').is(sub)) {
						if (window.Modernizr.cssanimations) {
							var activeMenu = $('.menu-sub.menu-clone.fadeIn');
							activeMenu.addClass('fadeOut').data('menu').overlay.removeClass('fadeInOverlay').addClass('fadeOutOverlay');
							activeMenu.data('menu').removeClass('active');
							// mainMenu.preventClick = true;
							activeMenu.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
								activeMenu.addClass('hidden').removeClass('animated fadeOut');
								activeMenu.data('menu').overlay.remove();
								activeMenu.removeClass('fadeIn animated fadeOut').addClass('hidden');
								activeMenu.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
								// mainMenu.preventClick = false;
							});
						} else {
							$('.menu-sub.menu-clone.fadeIn').fadeOut(config.duration.menu).removeClass('fadeIn').data('menu').removeClass('active').overlay.fadeOut(config.duration.overlay, function() {
								$(this).remove();
							});
						}
					}
				};

				var menuAnimation = function() {
					// if (window.innerWidth >= config.tablet /* - config.width.docScroll*/ ) {
						// Desk and Tablet
						// hide language
						// if (menuHolder.length) {
						// 	if (menuHolder.is('.popup--language')) {
						// 		menuHolder.data('triggerLanguage').trigger('click.showLanguage');
						// 	}
						// }
						// if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
						// 	vars.popupPromo.Popup('hide');
						// }
						// if (vars.popupPromo.length && popupLogin.length && popupLogin.data('Popup').element.isShow) {
						// 	popupLogin.Popup('hide');
						// }
						// // hide menu if there is a its siblings opened
						// if ($('.menu-sub.menu-clone.fadeIn').length && !$('.menu-sub.menu-clone.fadeIn').is(sub)) {
						// 	if (window.Modernizr.cssanimations) {
						// 		var activeMenu = $('.menu-sub.menu-clone.fadeIn');
						// 		activeMenu.addClass('fadeOut').data('menu').overlay.removeClass('fadeInOverlay').addClass('fadeOutOverlay');
						// 		activeMenu.data('menu').removeClass('active');
						// 		// mainMenu.preventClick = true;
						// 		activeMenu.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						// 			activeMenu.addClass('hidden').removeClass('animated fadeOut');
						// 			activeMenu.data('menu').overlay.remove();
						// 			activeMenu.removeClass('fadeIn animated fadeOut').addClass('hidden');
						// 			activeMenu.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						// 			// mainMenu.preventClick = false;
						// 		});
						// 	} else {
						// 		$('.menu-sub.menu-clone.fadeIn').fadeOut(config.duration.menu).removeClass('fadeIn').data('menu').removeClass('active').overlay.fadeOut(config.duration.overlay, function() {
						// 			$(this).remove();
						// 		});
						// 	}
						// }

						// // open and close sub
						// if (!mainMenu.preventClick) {
						// 	if (!sub.hasClass('fadeIn')) {
						// 		openNav();
						// 	} else {
						// 		closeNav();
						// 	}
						// }
					// } else {
					// 	// Mobile
					// }
					var currentX; //use percent as unit
					var targetX;
					var spaceToMove;
					var interval;
					var animateInterval;
					if (realSub.hasClass('hidden')) {
						if (window.Modernizr.cssanimations) {
							menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
							realSub.removeClass('hidden');
							mainMenu.addClass('active');
							mainMenu.parent().scrollTop(0);
							if (mainMenu.height() < realSub.height()) {
								mainMenu.height(realSub.height());
							}
							menuBar.addClass('active');
						} else {
							//No CSS3 animation
							currentX = 0; //use percent as unit
							targetX = -109;
							spaceToMove = 10;
							interval = 20;
							animateInterval = window.setInterval(function() {
								if (currentX <= targetX) {
									window.clearInterval(animateInterval);
									//
									mainMenu.children('ul').add(menuBar).css({
										'transform': '',
										'-moz-transform': '',
										'-webkite-transform': '',
										'-ms-transform': '',
										'-o-transform': ''
									});
									menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
									realSub.removeClass('hidden');
									mainMenu.addClass('active');
									menuBar.addClass('active');
								} else {
									currentX -= spaceToMove;
									mainMenu.children('ul').add(menuBar).css({
										'transform': 'translateX(' + currentX + '%)',
										'-moz-transform': 'translateX(' + currentX + '%)',
										'-webkite-transform': 'translateX(' + currentX + '%)',
										'-ms-transform': 'translateX(' + currentX + '%)',
										'-o-transform': 'translateX(' + currentX + '%)'
									});
								}
							}, interval);
						}
					} else {
						realSub.addClass('hidden');
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
					}
				};

				// self.children('a').off('click.showSubMenu').on('click.showSubMenu', function(e) {
				// 	e.preventDefault();
				// 	var popupSeatSelect = $('[data-infomations-1]'),
				// 		popupSeatChange = $('[data-infomations-2]');
				// 	if (popupSeatSelect.length) {
				// 		popupSeatSelect.find('.tooltip__close').trigger('click');
				// 	}
				// 	if (popupSeatChange.length) {
				// 		popupSeatChange.find('.tooltip__close').trigger('click');
				// 	}
				// 	if (mainMenu.preventClick) {
				// 		return;
				// 	}
				// 	sub.css('top', mainMenu.offset().top + self.height());
				// 	menuAnimation();
				// });

				var callCloseTimer;

				self.children('a').off('mouseover.showSubMenu').on('mouseover.showSubMenu', function() {
					if(window.innerWidth >= config.tablet){
						sub.css('top', mainMenu.offset().top + self.height());
						beforeShowHideMenu();
						if (!sub.hasClass('fadeIn')) {
							openNav();
						}
						if (callCloseTimer) {
							clearTimeout(callCloseTimer);
						}
					}
				}).off('mouseout.showSubMenu').on('mouseout.showSubMenu', function() {
					if(window.innerWidth >= config.tablet){
						beforeShowHideMenu();
						callCloseTimer = setTimeout(function(){
							closeNav();
						}, 100);
					}
				});

				/*
				* WCAG STARTS
				* Toggle Submenu using Arrow Keys
				*/
				self.children('a').each(function(){
					$(this).attr({
						'aria-controls':'wcag-nav-' + $(this).parent().index()
					});
					sub.attr({
						'id': 'wcag-nav-' + $(this).parent().index()
					});
				});

				self.children('a').off('keydown').on('keydown', function(e) {
					var keyCode = e.keyCode || e.which || e.charCode;

					if( keyCode === 40 ){
						if(window.innerWidth >= config.tablet){
							sub.css('top', mainMenu.offset().top + self.height());
							beforeShowHideMenu();
							if (!sub.hasClass('fadeIn')) {
								openNav(true);
							}
							if (callCloseTimer) {
								clearTimeout(callCloseTimer);
							}
						}
						e.preventDefault();
					}

					if(keyCode === 38){
						if(window.innerWidth >= config.tablet){
							beforeShowHideMenu();
							callCloseTimer = setTimeout(function(){
								closeNav();
							}, 100);
						}
					}
				});
				/** WCAG ENDS **/

				var focusInSubMenu = function(sub, key){

					if( key ){
						sub.find('a:first').focus();
					}


					if( !sub.find('.menu-sub-outer').hasClass('hidden-dt') ){
						var anchorLength = sub.find('.menu-sub-outer a').length;
						sub.find('.menu-sub-outer a').each(function(i){
							if((i+1) === anchorLength){
								$(this).addClass('last');
							}
							if((i) === 0){
								$(this).addClass('first');
							}
						});
					}else{
						var anchorLength = sub.find('> ul a').length;
						sub.find('> ul a').each(function(i){
							if((i+1) === anchorLength){
								$(this).addClass('last');
							}
							if((i) === 0){
								$(this).addClass('first');
							}
						});
					}

					sub.find('a').off('keydown').on('keydown', function(e) {
						var keyCode = e.keyCode || e.which || e.charCode;

						if( keyCode === 9 && e.shiftKey ){
							if($(this).hasClass('first')){
								$('.menu-item.active').next().find('a').focus();
								$('.menu-item.active a').trigger('mouseout');
							}
						}

						if( keyCode === 27 ){
							$('.menu-item.active').next().find('a').focus();
							$('.menu-item.active a').trigger('mouseout');
						}

						if( keyCode === 9 ){

							if($(this).hasClass('last')){
								if($('.menu-item.active').is(':last-child')){
									$('.menu-main').next().find('a:first').parent().attr('tabindex', -1).focus();
									// console.log($('.menu-main').next().find('a:first-child'));
									// console.log($('.menu-main').next().find('a:first'));
									// $('.menu-bar li').eq(0).find('a').focus();
									// $('a.flag').attr('tabindex', -1).focus();

								}else{
									$('ul[role="menubar"]').children().eq($('.menu-item.active').index()).find('a').focus();
								}
								$('.menu-item.active a').trigger('mouseout');
							}


						}

					});

				};

				var siteLogo = $('.logo');
				var skipToContent = $('.skip-to-content');
				var skipToContentLink = $('a', skipToContent);
				var siteMainInner = $('.main-inner');
				var menuSub = $('.menu-sub');
				var travelWidget = $('#travel-widget');

				siteMainInner.attr('id', 'main-inner');

				/** Logo **/
				siteLogo.off('keydown').on('keydown', function(e) {
					var keyCode = e.keyCode || e.which || e.charCode;

					if( keyCode === 9 && e.shiftKey ){
						if(skipToContent.length>0){
							skipToContent.removeClass('hidden').find('a').focus();
							e.preventDefault();
						}

					}

				});

				if(travelWidget.length>0){
					skipToContentLink.attr('href', '#travel-widget');
				}else{
					skipToContentLink.attr('href', '#main-inner');
				}

				skipToContentLink.focus(function(){
					skipToContent.css({
						'position':'static',
						'right':'auto'
					}).removeClass('ui-helper-hidden-accessible');
				});

				var flagAndSearch = function(){
					var flag = $('.flag');
					var search = $('.search');
					flag.attr('aria-label', $('.popup--language .popup__heading').text());
					search.attr('aria-label', 'Search');
				};

				flagAndSearch();

				// skipToContentLink.off('keydown').on('keydown', function(e) {
				// 	var keyCode = e.keyCode || e.which || e.charCode;
				// 	if( keyCode === 13  ){
				// 		if(travelWidget.length>0){
				// 			travelWidget.attr('tabindex', -1).focus();
				// 		}else{
				// 			siteMainInner.attr('tabindex', -1).focus();
				// 		}
				//
				// 	}
				// });

				menuSub.find('*').each(function(){
					var self = $(this);
					self.off('keydown').on('keydown', function(e) {
						var keyCode = e.keyCode || e.which || e.charCode;
						if( keyCode === 27  ){
							$('.menu-item.active a').focus();
							if(window.innerWidth >= config.tablet){
								beforeShowHideMenu();
								callCloseTimer = setTimeout(function(){
									closeNav();
								}, 100);
							}

						}
					});
				});

				siteMainInner.blur(function(){
					$(this).removeAttr('tabindex');
				});

				skipToContentLink.blur(function(){
					$(this).parents('.skip-to-content').addClass('ui-helper-hidden-accessible');
				});

				sub.off('mouseover.showSubMenu').on('mouseover.showSubMenu', function() {
					if(window.innerWidth >= config.tablet){
						if (callCloseTimer) {
							clearTimeout(callCloseTimer);
						}
					}
				}).off('mouseout.showSubMenu').on('mouseout.showSubMenu', function() {
					if(window.innerWidth >= config.tablet){
						beforeShowHideMenu();
						callCloseTimer = setTimeout(function(){
							closeNav();
						}, 100);
					}
				});

				self.find('> a em.ico-point-r').off('click.showSubMenu').on('click.showSubMenu', function(e){
					e.preventDefault();
					e.stopPropagation();
					var popupSeatSelect = $('[data-infomations-1]'),
						popupSeatChange = $('[data-infomations-2]');
					if (popupSeatSelect.length) {
						popupSeatSelect.find('.tooltip__close').trigger('click');
					}
					if (popupSeatChange.length) {
						popupSeatChange.find('.tooltip__close').trigger('click');
					}
					if (mainMenu.preventClick) {
						return;
					}
					sub.css('top', mainMenu.offset().top + self.height());
					menuAnimation();
				});

				/*sub.find('.ico-close-rounded').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
					e.preventDefault();
					if(window.Modernizr.touch){
						self.children('a').trigger('click.showSubMenu');
					}
					else{
						closeNav();
					}
				});*/
				var closeBtn = sub.find('.menu-sub__close').detach();
				closeBtn.off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
					e.preventDefault();
					if (window.Modernizr.touch) {
						self.children('a').trigger('click.showSubMenu');
					} else {
						closeNav();
					}
				});
				sub.append(closeBtn);
				realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
					e.preventDefault();
					if (window.Modernizr.cssanimations) {
						setTimeout(function() {
							realSub.addClass('hidden');
							realSub.closest('.menu-item').removeClass('active');
						}, 500);
						mainMenu.css('height', '').removeClass('active');
						menuBar.removeClass('active');
					} else {
						var currentX = -109; //use percent as unit
						var targetX = 0;
						var spaceToMove = 10;
						var interval = 20;
						var animateInterval = window.setInterval(function() {
							if (currentX >= targetX) {
								window.clearInterval(animateInterval);
								//
								mainMenu.children('ul').add(menuBar).css({
									'transform': '',
									'-moz-transform': '',
									'-webkite-transform': '',
									'-ms-transform': '',
									'-o-transform': ''
								});
								//setTimeout(function(){
								realSub.addClass('hidden');
								realSub.closest('.menu-item').removeClass('active');
								//},500);
								mainMenu.removeClass('active');
								menuBar.removeClass('active');
							} else {
								currentX += spaceToMove;
								mainMenu.children('ul').add(menuBar).css({
									'transform': 'translateX(' + currentX + '%)',
									'-moz-transform': 'translateX(' + currentX + '%)',
									'-webkite-transform': 'translateX(' + currentX + '%)',
									'-ms-transform': 'translateX(' + currentX + '%)',
									'-o-transform': 'translateX(' + currentX + '%)'
								});
							}
						}, interval);
					}
				});
			}
		});
	};

	var selectLanguage = function() {
		var triggerLanguage = menuBar.find('ul > li:first > a');
		var timeoutppLanguage = null;
		var openOnMobile = false;
		var openOnTablet = false;
		var wW = win.width();
		ppLanguage = ppLanguage.appendTo(body).css('zIndex', config.zIndex.ppLanguage);
		var innerPopup = ppLanguage.find('.popup__inner');

		ppLanguage.data('triggerLanguage', triggerLanguage);
		triggerLanguage.off('click.showLanguage').on('click.showLanguage', function(e) {
			e.preventDefault();
			// hide menu
			if (menuHolder.length) {
				if (!menuHolder.is('.popup--language')) {
					menuHolder.data('menu').children('a').trigger('click.showSubMenu');
				}
			}
			// hide promotion popup
			if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
				vars.popupPromo.Popup('hide');
			}
			// hide login popup
			if (popupLogin.length && popupLogin.data('Popup').element.isShow) {
				popupLogin.Popup('hide');
			}

			var langToolbarEl = $('.toolbar--language');

			// open and close popup language
			if (ppLanguage.hasClass('hidden')) {
				var popupOnTablet = function() {
					/*ppLanguage.removeClass('hidden').css({
						left: triggerLanguage.offset().left - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
						top: triggerLanguage.offset().top + triggerLanguage.height() + 25
					}).addClass('animated fadeIn');*/
					ppLanguage.css('position', 'fixed').removeClass('hidden').addClass('animated fadeIn');

					if (window.innerWidth < config.tablet) {
						container.css({
							'height': win.height() - (langToolbarEl.length ? langToolbarEl.height() : 0),
							'overflow': 'hidden'
						});
					}
					menuHolder = ppLanguage;
					triggerLanguage.addClass('active');
					triggerLanguage.overlay.show().addClass('animated fadeInOverlay').css('zIndex', (window.innerWidth < config.tablet) ? (config.zIndex.ppLanguage - 1) : '');

					openOnTablet = true;

				};

				var setPosition = function() {
					if (window.innerWidth >= config.tablet) {
						ppLanguage.removeClass('hidden');
						/*var leftPpLanguage = Math.max(0,triggerLanguage.offset().left) - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2;
						if(leftPpLanguage + ppLanguage.outerWidth() > win.width()){
							leftPpLanguage = leftPpLanguage - (leftPpLanguage + ppLanguage.outerWidth() - win.width());
						}
						ppLanguage.css({
							left: leftPpLanguage,
							top: Math.max(0,triggerLanguage.offset().top + triggerLanguage.height() + 40)
						});
						ppLanguage.find('.popup__arrow').css({
							left : triggerLanguage.offset().left - leftPpLanguage + 10
						});*/
						var widthInnerPopup = innerPopup.outerWidth(),
							leftPpLanguage = Math.max(0, triggerLanguage.offset().left) - widthInnerPopup / 2 + triggerLanguage.outerWidth(true) / 2;

						if (leftPpLanguage + widthInnerPopup > win.width()) {
							leftPpLanguage = win.width() - innerPopup.outerWidth();
						}
						ppLanguage.css('position', 'fixed');
						innerPopup.css({
							position: 'absolute',
							left: leftPpLanguage,
							top: Math.max(0, triggerLanguage.parent().offset().top + triggerLanguage.parent().height() + 30 - win.scrollTop())
						});
						ppLanguage.find('.popup__arrow').css({
							left: triggerLanguage.offset().left - leftPpLanguage + 21
						});
					} else {
						/*if(vars.isIE() && vars.ieVersion() < 9){
							ppLanguage.removeClass('hidden').css({
								left: Math.max(0,triggerLanguage.offset().left) - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
								top: Math.max(0,triggerLanguage.offset().top + triggerLanguage.height() + 25)
							});
						} else {
							ppLanguage.removeClass('hidden').css({
								left: Math.max(0,(win.width() - ppLanguage.outerWidth(true))/2),
								top: Math.max(0, (win.height() - ppLanguage.outerHeight(true))/2)
							});
						}*/
						ppLanguage.css('position', 'fixed').removeClass('hidden');
						innerPopup.css('position', 'absolute');
						innerPopup.css({
							left: Math.max(0, (win.width() - innerPopup.outerWidth(true)) / 2),
							top: Math.max(0, (win.height() - innerPopup.outerHeight(true)) / 2)
						});
					}

					triggerLanguage.overlay.css({
						'zIndex': (container.css('overflow') === 'hidden') ? (config.zIndex.ppLanguage - 1) : (win.width() < config.tablet - config.width.docScroll) ? (config.zIndex.ppLanguage - 1) : ''
					});
				};
				triggerLanguage.overlay = $(config.template.overlay).appendTo(body);
				triggerLanguage.overlay.hide();
				triggerLanguage.overlay.off('click.hideOverlay').on('click.hideOverlay', function() {
					triggerLanguage.trigger('click.showLanguage');
				});

				// Desktop and Tablet
				if (window.Modernizr.cssanimations) {
					popupOnTablet();
				} else {
					/*ppLanguage.removeClass('hidden').css({
						left: triggerLanguage.offset().left - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
						display: 'none',
						top: triggerLanguage.offset().top + triggerLanguage.height() + 25
					}).fadeIn(config.duration.languagePopup);*/
					ppLanguage.removeClass('hidden').hide().fadeIn(config.duration.languagePopup);
					menuHolder = ppLanguage;
					triggerLanguage.addClass('active');
					if (vars.isIE() && vars.isIE() < 8) {
						triggerLanguage.overlay.css({
							top: header.height()
						});
					}
					triggerLanguage.overlay.fadeIn(config.duration.overlay).css('zIndex', (win.width() < config.tablet - config.width.docScroll) ? (config.zIndex.ppLanguage - 1) : '');
				}
				setPosition();
				// if(window.innerWidth >= config.tablet || (window.innerWidth < config.tablet && window.innerWidth >= config.mobile)){
				// }
				win.off('resize.popupLanguage').on('resize.popupLanguage', function() {
					clearTimeout(timeoutppLanguage);
					timeoutppLanguage = setTimeout(function() {
						if (openOnMobile) {
							if (wW !== win.width()) {
								if (ppLanguage.find('.ui-autocomplete-input').autocomplete('widget').is(':visible')) {
									ppLanguage.find('.ui-autocomplete-input').blur();
								}
								wW = win.width();
							}
						}
						ppLanguage.css('height', '');
						ppLanguage.find('.popup__content').css('height', '');
						if (window.innerWidth < config.tablet) {
							container.css({
								'height': win.height(),
								'overflow': 'hidden'
							});
						} else if (window.innerWidth >= config.tablet) {
							container.removeAttr('style');
						}
						if (ppLanguage.outerHeight(true) > win.height()) {
							if (win.width() > config.mobile) {
								ppLanguage.find('.popup__content').css({
									height: win.height() - parseInt(ppLanguage.find('.popup__content').css('padding-top')),
									'overflow-y': 'auto'
								});
							}
						}
						setPosition();
					}, 100);
				});

				SIA.WcagGlobal.assignAttributes();

			} else { // close
				win.off('resize.popupLanguage');
				if (window.Modernizr.cssanimations) {
					ppLanguage.addClass('fadeOut');
					triggerLanguage.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay').css('zIndex', '');
					triggerLanguage.removeClass('active');
					ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(e) {
						e.preventDefault();
						menuHolder = $();
						triggerLanguage.overlay.remove();
						ppLanguage.removeClass('animated fadeOut').addClass('hidden');
						container.removeAttr('style');
						// if(langToolbarEl.length) {
						// 	container.css('padding-top', langToolbarEl.height());
						// }
						ppLanguage.css('right', '');
						ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						win.trigger('resize.openMenuT');
					});
				} else {
					ppLanguage.fadeOut(config.duration.languagePopup, function() {
						ppLanguage.addClass('hidden');
					});
					triggerLanguage.removeClass('active');
					menuHolder = $();
					triggerLanguage.overlay.fadeOut(config.duration.overlay, function() {
						triggerLanguage.overlay.remove();
					}).css('zIndex', '');
				}
			}
		});

		// close popup language
		ppLanguage.find('.popup__close').off('click.hideLanguage').on('click.hideLanguage', function(e) {
			e.preventDefault();
			//e.stopPropagation()
			triggerLanguage.trigger('click.showLanguage');
		});

		ppLanguage.off('click.hideLanguage').on('click.hideLanguage', function() {
			triggerLanguage.trigger('click.showLanguage');
		});
		innerPopup.off('click.hideLanguage').on('click.hideLanguage', function(e) {
			e.stopPropagation();
		});
	};

	var loginPopup = function() {
		var triggerLoginPoup = menuBar.find('ul a.login');
		var flyingFocus = $('#flying-focus');

		// init login popup
		popupLogin.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close',
			afterShow: function() {
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
			},
			beforeHide: function() {
				if (popupLogin.find('[data-tooltip]').length) {
					popupLogin.find('[data-tooltip]').data('kTooltip').closeTooltip();
				}
			},
			afterHide: function() {
				win.trigger('resize.openMenuT');
				win.trigger('resize.resetTabMenu');
				container.css('padding-right', '');
				body.removeAttr('style');
			}
		});
		triggerLoginPoup.off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			// hide sub
			if (menuHolder.length) {
				if ($('.menu-sub.menu-clone.fadeIn').length) {
					$('.menu-sub.menu-clone.fadeIn .menu-sub__close').trigger('click.hideSubMenu');
					/*if(window.Modernizr.cssanimations){
						var activeMenu = $('.menu-sub.menu-clone.fadeIn');
						activeMenu.removeClass('fadeIn').addClass('fadeOut').data('menu').overlay.removeClass('fadeInOverlay').addClass('fadeOutOverlay');
						activeMenu.data('menu').removeClass('active');
						activeMenu.removeClass('animated fadeOut').addClass('hidden');
						// activeMenu.data('menu').overlay.removeClass('animated fadeOutOverlay').hide();
						activeMenu.data('menu').overlay.remove();
						// setTimeout(function(){
						// }, config.duration.menu);
					}
					else{
						// $('.menu-sub.menu-clone.fadeIn').animate({
						// 	top: - $('.menu-sub.menu-clone.fadeIn').height() - header.height() + header.offset().top
						// }, config.duration.menu).removeClass('fadeIn').data('menu').removeClass('active').overlay.fadeOut(config.duration.overlay);
						$('.menu-sub.menu-clone.fadeIn').fadeOut(config.duration.menu).removeClass('fadeIn').data('menu').removeClass('active').overlay.fadeOut(config.duration.overlay, function(){
							$(this).remove();
						});
					}*/
				}

				/*if(!menuHolder.is('.popup--language')){
					// hide menu
					if(window.Modernizr.cssanimations){
						menuHolder.removeClass('fadeIn').addClass('fadeOut');
						menuHolder.data('menu').removeClass('active');
						menuHolder.data('menu').overlay.removeClass('fadeInOverlay').addClass('fadeOutOverlay').remove();
						menuHolder = $();
						header.removeClass('active');
					}
					else{
						menuHolder.animate({
							top: - menuHolder.height() - header.height() + header.offset().top
						}, config.duration.menu).removeClass('fadeIn');
						menuHolder.data('menu').removeClass('active');
						menuHolder.data('menu').overlay.fadeOut(config.duration.overlay, function(){
							$(this);
						});
						menuHolder = $();
						header.removeClass('active');
					}
				}

				if(menuHolder.is('.popup--language')){
					// hide language
					if(window.Modernizr.cssanimations){
						menuHolder.removeClass('fadeIn').addClass('fadeOut');
						menuHolder.addClass('hidden').removeClass('animated fadeOut');
						win.off('resize.popupLanguage');
						menuHolder.data('triggerLanguage').removeClass('active');
						menuHolder.data('triggerLanguage').overlay.remove();
						menuHolder = $();
					}
					else{
						menuHolder.fadeOut(config.duration.overlay, function(){
							menuHolder.addClass('hidden');
							menuHolder = $();
						});
						win.off('resize.popupLanguage');
						menuHolder.data('triggerLanguage').removeClass('active');
						menuHolder.data('triggerLanguage').overlay.fadeOut(config.duration.overlay, function(){
							$(this);
						});
					}
				}*/
			}
			// hide promotion popup
			if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
				vars.popupPromo.Popup('hide');
			}

			// hide search popup
			hidePopupSearch();

			popupLogin.Popup('show');
		});

		// highlight input
		popupLogin.find('input:text, input:password').off('focus.highlight').on('focus.highlight', function(e) {
			e.stopPropagation();
			$(this).closest('.input-1').addClass('focus');
		});
		popupLogin.find('input:text, input:password').off('blur.highlight').on('blur.highlight', function() {
			$(this).closest('.input-1').removeClass('focus');
		});
	};

	var menuTablet = function() {
		var navbar = $('.ico-nav');
		menuT = $('.menu');
		var menuBarItems = menuBar.find('li');
		var login = $();
		// var login = menuBarItems.eq(2).clone().addClass('login-item').prependTo(mainMenu.children('ul'));
		var search = menuBarItems.eq(1).clone(true).addClass('search-item').prependTo(mainMenu.children('ul'));
		var resetMenuTimer = null;
		var isAnimate = false;
		var timerPopupSearchResize = null;

		search.find('.popup--search').removeClass('hidden');
		login.hide();
		search.hide();

		var resetMenuT = function() {
			if (container.hasClass('show-menu')) {
				var langToolbarEl = $('.toolbar--language');
				container.removeClass('show-menu');
				// reset menu
				// do not use event transitionEnd because this event always trigger when container's children do css animation
				if (window.Modernizr.cssanimations) {
					menuT.addClass('fadeOutLeft animated');
					menuT.menuOverlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					menuT.find('.menu-inner').removeAttr('style');
					menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						container.removeAttr('style');
						if (langToolbarEl.length) {
							// container.css('padding-top', langToolbarEl.height());
							menuT.removeAttr('style');
						}
						win.off('resize.openMenuT');
						menuBarItems.eq(1).show();
						// menuBarItems.eq(2).show();
						login.hide();
						search.hide();
						menuT.removeClass('fadeOutLeft animated active').css('display', '').css('height', '');
						isAnimate = false;
						menuT.menuOverlay.remove();
						mainMenu.removeClass('active').removeAttr('style');
						menuBar.removeClass('active');
						mainMenu.find('.menu-item').removeClass('active');
						mainMenu.find('.menu-sub').addClass('hidden');
						loggedProfileSubmenu.addClass('hidden');
						menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				} else {
					var spaceToMove = Math.abs((-290) - (-20)); //290 = original translateX, 20 = animated translateX;
					var currentX = -20;
					var interval = 20;
					var totalTime = 0;
					var animateDuration = 500;
					var spaceToMoveEveryInterval = spaceToMove / (animateDuration / interval);
					var intervalAnimation = window.setInterval(function() {
						if (totalTime === animateDuration) {
							window.clearInterval(intervalAnimation);
							menuT.removeClass('active');
							menuT.css({
								'transform': '',
								'-webkit-transform': '',
								'-moz-transform': '',
								'-ms-transform': '',
								'-o-transform': ''
							});
							isAnimate = false;
						} else {
							currentX -= spaceToMoveEveryInterval;
							menuT.css({
								'transform': 'translateX(' + currentX + 'px)',
								'-webkit-transform': 'translateX(' + currentX + 'px)',
								'-moz-transform': 'translateX(' + currentX + 'px)',
								'-ms-transform': 'translateX(' + currentX + 'px)',
								'-o-transform': 'translateX(' + currentX + 'px)'
							});
							totalTime += interval;
						}
					}, interval);
					menuT.menuOverlay.fadeOut(config.duration.overlay, function() {
						menuT.menuOverlay.remove();
						html.removeAttr('style');
						body.removeAttr('style');
						win.off('resize.openMenuT');
						menuBarItems.eq(1).show();
						// menuBarItems.eq(2).show();
						login.hide();
						search.hide();
						menuT.css('display', '').css('height', '');
						//isAnimate = false;
						menuT.menuOverlay.remove();
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
						mainMenu.find('.menu-item').removeClass('active');
						mainMenu.find('.menu-sub').addClass('hidden');
						loggedProfileSubmenu.addClass('hidden');
					});
				}
			}
			mainMenu.removeClass('hidden');
		};
		// freeze body to slide
		var freezeBody = function() {
			if (window.Modernizr.cssanimations) {
				var getMaxHeight = win.height();
				var langToolbarEl = $('.toolbar--language'),
					containerH = getMaxHeight;
				var menuInnerEl = menuT.find('.menu-inner');
				if (langToolbarEl.length) {
					containerH -= langToolbarEl.height();
				}
				if (menuT.hasClass('active')) {
					// menuT.find('.menu-inner').height(getMaxHeight - parseInt($('.menu-inner').css('padding-top')));
					menuInnerEl.height(containerH - parseInt(menuInnerEl.css('padding-top')) - parseInt(menuInnerEl.css('padding-bottom')));
					// container.css('height', '');
					container.css({
						'overflow': 'hidden',
						'height': win.height()
					});
					return;
				}
				menuT.menuOverlay = $(config.template.overlay).appendTo(body).show().addClass('animated fadeInOverlay');
				// menuT.css('height', '');
				menuT.addClass('fadeInLeft animated active').show();
				// menuT.find('.menu-inner').height(getMaxHeight - parseInt($('.menu-inner').css('padding-top')));
				menuInnerEl.height(containerH - parseInt(menuInnerEl.css('padding-top')) - parseInt(menuInnerEl.css('padding-bottom')));
				menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
					menuT.removeClass('fadeInLeft animated');
					isAnimate = false;
					container.css({
						'overflow': 'hidden',
						'height': containerH
					});
					menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
			} else {
				var spaceToMove = Math.abs((-290) - (-20)); //290 = original translateX, 20 = animated translateX;
				var currentX = -290;
				var interval = 20;
				var totalTime = 0;
				var animateDuration = 500;
				var spaceToMoveEveryInterval = spaceToMove / (animateDuration / interval);
				menuT.addClass('active').css({
					'transform': 'translateX(-290px)',
					'-webkit-transform': 'translateX(-290px)',
					'-moz-transform': 'translateX(-290px)',
					'-ms-transform': 'translateX(-290px)',
					'-o-transform': 'translateX(-290px)'
				});
				var intervalAnimation = window.setInterval(function() {
					if (totalTime === animateDuration) {
						window.clearInterval(intervalAnimation);
						menuT.css({
							'transform': '',
							'-webkit-transform': '',
							'-moz-transform': '',
							'-ms-transform': '',
							'-o-transform': ''
						});
						isAnimate = false;
					} else {
						currentX += spaceToMoveEveryInterval;
						menuT.css({
							'transform': 'translateX(' + currentX + 'px)',
							'-webkit-transform': 'translateX(' + currentX + 'px)',
							'-moz-transform': 'translateX(' + currentX + 'px)',
							'-ms-transform': 'translateX(' + currentX + 'px)',
							'-o-transform': 'translateX(' + currentX + 'px)'
						});
						totalTime += interval;
					}
				}, interval);
				menuT.menuOverlay = $(config.template.overlay).appendTo(body).fadeIn(config.duration.overlay);
			}
		};

		var openMenuT = function() {
			//container.width(win.width());
			var langToolbarEl = $('.toolbar--language'),
				containerH = win.height();
			if (langToolbarEl.length) {
				containerH -= langToolbarEl.height();
			}

			container.addClass('show-menu');
			freezeBody();
			// menuT.show().height(win.height());
			menuT.menuOverlay.off('click.hideMenu').on('click.hideMenu', function(e) {
				e.preventDefault();
				resetMenuT();
			});
			menuBarItems.eq(1).hide();
			// menuBarItems.eq(2).hide();
			login.show();
			search.show();
			// loggedProfileSubmenu.css('top', -mainMenu.height());
			win.off('resize.openMenuT').on('resize.openMenuT', function() {
				if (win.width() >= config.tablet /* - config.width.docScroll*/ ) {
					mainMenu.removeClass('active');
					menuBar.removeClass('active');
				}
				if (win.width() >= config.tablet /* - config.width.docScroll*/ ) {
					if (menuT.find('.menu-sub').length) {
						menuT.removeAttr('style').find('.menu-sub').addClass('hidden').parent().removeClass('active');
						menuT.find('.menu-inner').removeAttr('style');
						menuT.find('.menu-main').removeAttr('style');
						container.removeAttr('style');
					}
					container.removeClass('show-menu');
					menuT.removeClass('fadeInLeft animated active').css('display', '').css('height', '');
					menuT.menuOverlay.remove();
					menuBarItems.eq(1).show();
					// menuBarItems.eq(2).show();
					login.hide();
					search.hide();
					win.off('resize.openMenuT');
					clearTimeout(resetMenuTimer);
					return;
				}
				clearTimeout(resetMenuTimer);
				resetMenuTimer = setTimeout(function() {
					container.css({
						'overflow': 'hidden',
						'height': containerH
					});
					menuT.show().height(win.height());
					responsiveMenubar();
					loggedProfileSubmenu.css('top', -mainMenu.height());
				}, config.duration.clearTimeout);
			});

		};
		window.resetMenuT = resetMenuT;
		var responsiveMenubar = function() {
			if (win.width() < config.tablet /* - config.width.docScroll*/ ) {
				menuBarItems.eq(1).hide();
				// menuBarItems.eq(2).hide();
				login.show();
				search.show();
			} else {
				menuBarItems.eq(1).show();
				// menuBarItems.eq(2).show();
				login.hide();
				search.hide();
			}
		};

		menuBarItems.eq(1).children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			e.stopPropagation();
			detectHidePopup();
			hideLoggedProfile();
			//$('[data-customselect]').customSelect('hide');
			popupSearch.find('[data-customselect]').customSelect('hide');
			var self = $(this);
			if (popupSearch.hasClass('hidden')) {
				self.addClass('active');
				var popupSearchPosition = function() {
					popupSearch.removeClass('hidden');
					/*var leftPopupSearch = menuBarItems.eq(1).offset().left - popupSearch.outerWidth()/2 + menuBarItems.eq(1).outerWidth()/2 - 30;
					if(leftPopupSearch + popupSearch.outerWidth() > win.width()){
						leftPopupSearch = leftPopupSearch - (leftPopupSearch + popupSearch.outerWidth() - win.width());
					}
					popupSearch.css({
						left: leftPopupSearch,
						top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + (vars.isIE() ? 26 : 21)
					});
					popupSearch.find('.popup__arrow').css({
						left : menuBarItems.eq(1).offset().left - leftPopupSearch + 10
					});*/
					var innerPopup = popupSearch.find('.popup__inner'),
						widthInnerPopup = innerPopup.outerWidth(),
						leftPopupSearch = menuBarItems.eq(1).offset().left - widthInnerPopup / 2 + menuBarItems.eq(1).outerWidth() / 2 - 30;
					if (leftPopupSearch + widthInnerPopup > win.width()) {
						leftPopupSearch = win.width() - innerPopup.outerWidth();
					}
					popupSearch.css('position', 'fixed');
					innerPopup.css({
						position: 'absolute',
						left: leftPopupSearch,
						top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + 30 - win.scrollTop()
					});
					popupSearch.find('.popup__arrow').css({
						left: menuBarItems.eq(1).offset().left - leftPopupSearch + 12
					});
				};
				popupSearchPosition();
				if (window.Modernizr.cssanimations) {
					popupSearch.addClass('animated fadeIn');
					popupSearch.overlay = $(config.template.overlay).appendTo(body).addClass('animated fadeInOverlay').show();
					popupSearch.triggerPopup = self;
				} else {
					popupSearch.overlay = $(config.template.overlay).appendTo(body);
					popupSearch.overlay.fadeIn(config.duration.overlay);
					popupSearch.triggerPopup = self;
					popupSearch.hide().fadeIn(config.duration.popupSearch);
				}
				win.off('resize.inputSearch').on('resize.inputSearch', function() {
					clearTimeout(timerPopupSearchResize);
					timerPopupSearchResize = setTimeout(function() {
						popupSearchPosition();
						if (win.width() < config.tablet - config.width.docScroll) {
							self.removeClass('active');
							popupSearch.addClass('hidden');
							popupSearch.overlay.remove();
							win.off('resize.inputSearch');
						}
					}, 100);
				});
				popupSearch.find('.popup__close').off('click.closeSearch').on('click.closeSearch', function(e) {
					e.preventDefault();
					e.stopPropagation();
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.overlay.off('click.closeSearch').on('click.closeSearch', function() {
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.off('click.closeSearch').on('click.closeSearch', function() {
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.find('.popup__inner').off('click.closeSearch').on('click.closeSearch', function(e) {
					e.stopPropagation();
				});
			} else {
				self.removeClass('active');
				if (window.Modernizr.cssanimations) {
					popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
					popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						popupSearch.removeClass('animated fadeOut').addClass('hidden');
						popupSearch.overlay.remove();
						popupSearch.overlay = $();
						popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				} else {
					popupSearch.fadeOut(config.duration.popupSearch, function() {
						popupSearch.addClass('hidden');
					});
					popupSearch.overlay.fadeOut(config.duration.overlay, function() {
						popupSearch.overlay.remove();
						popupSearch.overlay = $();
					});
				}
				win.off('resize.inputSearch');
			}
			SIA.WcagGlobal.assignAttributes();
			SIA.WcagGlobal.ppMoveFocus();
		});

		login.children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			menuBarItems.eq(2).children('a').trigger('click.showLoginPopup');
		});

		navbar.off('click.showMenuT').on('click.showMenuT', function(e) {
			e.preventDefault();
			e.stopPropagation();
			if (window.innerWidth < SIA.global.config.tablet) {
				var popupSeatSelect = $('[data-infomations-1]'),
					popupSeatChange = $('[data-infomations-2]');
				if (popupSeatSelect.length) {
					popupSeatSelect.find('.tooltip__close').trigger('click');
				}
				if (popupSeatChange.length) {
					popupSeatChange.find('.tooltip__close').trigger('click');
				}
			}
			// if(win.width() < config.tablet - config.width.docScroll){
			if (win.width() < config.tablet) {
				if (isAnimate) {
					return;
				}
				var langToolbarEl = $('.toolbar--language');
				isAnimate = true;
				if (menuT.hasClass('active')) {
					resetMenuT();
				} else {
					// if(langToolbarEl.length) {
					// 	menuT.css('top', langToolbarEl.height());
					// }

					if (langToolbarEl.length) {
						menuT.css({
							// 'top', langToolbarEl.height()}
							height: win.height() - langToolbarEl.height()
						});
					} else {
						menuT.css({
							height: win.height()
						});
					}

					openMenuT();
				}
			}
		});
	};


	// var loggedProfile = function(){
	// 	popupLoggedProfile = loggedProfileSubmenu.clone().appendTo(document.body);
	// 	loggedProfileSubmenu.removeClass().addClass('menu-sub hidden');
	// 	popupLoggedProfile.removeClass('menu-sub');
	// 	if(popupLoggedProfile.length){
	// 		var triggerLoggedProfile = menuBar.find('ul a.status');
	// 		var timerLoggedProfileResize = null;
	// 		var realSub = loggedProfileSubmenu;
	// 		var popupLoggedProfilePosition = function(){
	// 			popupLoggedProfile.removeClass('hidden');
	// 			var leftPopupLoggedProfile = triggerLoggedProfile.offset().left - popupLoggedProfile.outerWidth()/2 + triggerLoggedProfile.outerWidth()/2 - 30;
	// 			if(leftPopupLoggedProfile + popupLoggedProfile.outerWidth() > win.width()){
	// 				leftPopupLoggedProfile = leftPopupLoggedProfile - (leftPopupLoggedProfile + popupLoggedProfile.outerWidth() - win.width());
	// 			}
	// 			popupLoggedProfile.css({
	// 				left: leftPopupLoggedProfile,
	// 				top: triggerLoggedProfile.offset().top + triggerLoggedProfile.children().innerHeight() + (vars.isIE() ? 33 : 27)
	// 			});
	// 			popupLoggedProfile.find('.popup__arrow').css({
	// 				left : Math.round(triggerLoggedProfile.offset().left - leftPopupLoggedProfile + 21)
	// 			});
	// 		};

	// 		triggerLoggedProfile.off('click.showLoggedProfile').on('click.showLoggedProfile', function(e){
	// 			e.preventDefault();
	// 			e.stopPropagation();
	// 			var winInnerW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	// 			//hide all custom-select
	// 			$('[data-customselect]').customSelect('hide');
	// 			if(winInnerW >= config.tablet/* - config.width.docScroll*/){
	// 				detectHidePopup();
	// 				hidePopupSearch(true);
	// 				var self = $(this);
	// 				if(popupLoggedProfile.hasClass('hidden')){
	// 					self.addClass('active');
	// 					popupLoggedProfilePosition();
	// 					if(window.Modernizr.cssanimations){
	// 						popupLoggedProfile.addClass('animated fadeIn');
	// 						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body).addClass('animated fadeInOverlay').show();
	// 						popupLoggedProfile.triggerPopup = self;
	// 					}
	// 					else{
	// 						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body);
	// 						popupLoggedProfile.overlay.fadeIn(config.duration.overlay);
	// 						popupLoggedProfile.triggerPopup = self;
	// 						popupLoggedProfile.hide().fadeIn(config.duration.popupSearch);
	// 					}
	// 					win.off('resize.LoggedProfile').on('resize.LoggedProfile', function(){
	// 						clearTimeout(timerLoggedProfileResize);
	// 						timerLoggedProfileResize = setTimeout(function(){
	// 							popupLoggedProfilePosition();
	// 							if(win.width() < config.tablet - config.width.docScroll){
	// 								self.removeClass('active');
	// 								popupLoggedProfile.addClass('hidden');
	// 								popupLoggedProfile.overlay.remove();
	// 								win.off('resize.LoggedProfile');
	// 							}
	// 						}, 100);
	// 					}).trigger('resize.LoggedProfile');
	// 					popupLoggedProfile.overlay.off('click.closeLoggedProfile').on('click.closeLoggedProfile', function(e){
	// 						e.preventDefault();
	// 						e.stopPropagation();
	// 						triggerLoggedProfile.trigger('click.showLoggedProfile');
	// 					});
	// 				}
	// 				else{
	// 					self.removeClass('active');
	// 					if(window.Modernizr.cssanimations){
	// 						popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
	// 						popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
	// 						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
	// 							popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
	// 							popupLoggedProfile.overlay.remove();
	// 							popupLoggedProfile.overlay = $();
	// 							popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
	// 						});
	// 					}
	// 					else{
	// 						popupLoggedProfile.fadeOut(config.duration.popupSearch, function(){
	// 							popupLoggedProfile.addClass('hidden');
	// 						});
	// 						popupLoggedProfile.overlay.fadeOut(config.duration.overlay, function(){
	// 							popupLoggedProfile.overlay.remove();
	// 							popupLoggedProfile.overlay = $();
	// 						});
	// 					}
	// 					win.off('resize.LoggedProfile');
	// 				}
	// 			}
	// 			else {
	// 				// Mobile
	// 				var currentX; //use percent as unit
	// 				var targetX;
	// 				var spaceToMove;
	// 				var interval;
	// 				var animateInterval;
	// 				var menuSub = menuT.find('.logged-in .menu-sub');
	// 				var langToolbarEl = $('.toolbar--language'),
	// 						langToolbarH = 0;
	// 				if(langToolbarEl.length) {
	// 					langToolbarH = langToolbarEl.height();
	// 				}
	// 				if(!menuSub.data('onceIncreaseTop')) {
	// 					menuSub.data('onceIncreaseTop', true).css('top', -menuBar.find('.logged-in').offset().top);
	// 				}
	// 				menuT.find('.menu-inner').css('overflowY', 'hidden');
	// 				if(realSub.hasClass('hidden')){
	// 					if(window.Modernizr.cssanimations) {
	// 						realSub.removeClass('hidden');
	// 						mainMenu.addClass('active');
	// 						mainMenu.parent().scrollTop(0);
	// 						// realSub.css('top', - mainMenu.height());
	// 						menuBar.addClass('active');
	// 					}
	// 					else {
	// 						//No CSS3 animation
	// 						currentX = 0; //use percent as unit
	// 						targetX = -109;
	// 						spaceToMove = 10;
	// 						interval = 20;
	// 						animateInterval = window.setInterval(function() {
	// 							if(currentX <= targetX) {
	// 								window.clearInterval(animateInterval);
	// 								//
	// 								mainMenu.children('ul').add(menuBar).css({
	// 									'transform': '',
	// 									'-moz-transform': '',
	// 									'-webkite-transform': '',
	// 									'-ms-transform': '',
	// 									'-o-transform': ''
	// 								});
	// 								//menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
	// 								realSub.removeClass('hidden');
	// 								mainMenu.addClass('active');
	// 								menuBar.addClass('active');
	// 							}
	// 							else {
	// 								currentX -= spaceToMove;
	// 								mainMenu.children('ul').add(menuBar).css({
	// 									'transform': 'translateX(' + currentX + '%)',
	// 									'-moz-transform': 'translateX(' + currentX + '%)',
	// 									'-webkite-transform': 'translateX(' + currentX + '%)',
	// 									'-ms-transform': 'translateX(' + currentX + '%)',
	// 									'-o-transform': 'translateX(' + currentX + '%)'
	// 								});
	// 							}
	// 						}, interval);
	// 					}
	// 					menuSub.css('top', 18 + langToolbarH - menuBar.find('.logged-in').offset().top);
	// 				}
	// 				else{
	// 					realSub.addClass('hidden');
	// 					mainMenu.removeClass('active').removeClass('hidden');
	// 					menuBar.removeClass('active');
	// 				}
	// 			}
	// 		});

	// 		realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
	// 			e.preventDefault();
	// 			menuT.find('.menu-inner').css('overflowY', 'auto');
	// 			if (window.Modernizr.cssanimations) {
	// 				setTimeout(function(){
	// 					realSub.addClass('hidden');
	// 					realSub.closest('.menu-item').removeClass('active');
	// 				},500);
	// 				mainMenu.removeClass('active');
	// 				menuBar.removeClass('active');
	// 			}
	// 			else {
	// 				var currentX = -109; //use percent as unit
	// 				var targetX = 0;
	// 				var spaceToMove = 10;
	// 				var interval = 20;
	// 				var animateInterval = window.setInterval(function() {
	// 					if(currentX >= targetX) {
	// 						window.clearInterval(animateInterval);
	// 						//
	// 						mainMenu.children('ul').add(menuBar).css({
	// 							'transform': '',
	// 							'-moz-transform': '',
	// 							'-webkite-transform': '',
	// 							'-ms-transform': '',
	// 							'-o-transform': ''
	// 						});
	// 						//setTimeout(function(){
	// 						realSub.addClass('hidden');
	// 						realSub.closest('.menu-item').removeClass('active');
	// 						//},500);
	// 						mainMenu.removeClass('active');
	// 						menuBar.removeClass('active');
	// 					}
	// 					else {
	// 						currentX += spaceToMove;
	// 						mainMenu.children('ul').add(menuBar).css({
	// 							'transform': 'translateX(' + currentX + '%)',
	// 							'-moz-transform': 'translateX(' + currentX + '%)',
	// 							'-webkite-transform': 'translateX(' + currentX + '%)',
	// 							'-ms-transform': 'translateX(' + currentX + '%)',
	// 							'-o-transform': 'translateX(' + currentX + '%)'
	// 						});
	// 					}
	// 				}, interval);
	// 			}
	// 		});
	// 	}
	// };

	// checkAllList
	var checkAllList = function(trigger, wrapper, callback) {
		var checkAll = function() {

			var checkall = true;

			if (callback) {
				callback(wrapper.find(':checkbox:checked').length);
			}

			wrapper.find(':checkbox').not('[disabled]').each(function() {
				if (!$(this).is(':checked')) {
					checkall = false;
					return;
				}
			});

			if (!wrapper.find(':checkbox').not('[disabled]').length) {
				checkall = false;
			}

			return checkall;
		};

		trigger.off('change.checkAllList').on('change.checkAllList', function() {

			if ($(this).is(':checked')) {

				wrapper.find(':checkbox').not('[disabled]').each(function() {
					$(this).prop('checked', true);
				});

			} else {

				wrapper.find(':checkbox').not('[disabled]').each(function() {
					$(this).prop('checked', false);
				});

			}

			if (callback) {
				callback($(this).is(':checked'));
			}

		});

		wrapper.on('change.checkAllList', ':checkbox', function() {
			if (checkAll()) {
				trigger.prop('checked', true);
			} else {
				trigger.prop('checked', false);
			}
		});

		// wrapper.find(':checkbox').each(function() {
		// 	var self = $(this);
		// 	self.off('change.checkAllList').on('change.checkAllList', function() {
		// 		if (checkAll()) {
		// 			trigger.prop('checked', true);
		// 		} else {
		// 			trigger.prop('checked', false);
		// 		}
		// 	});
		// });

	};

	//publick checkAllList function
	vars.checkAllList = checkAllList;

	// var loader = function(){
	// 	var imgs = $('body img').not('.img-loading'),
	// 		totalImage = imgs.length,
	// 		count = 0;
	// 	imgs.each(function(){
	// 		var img = new Image();
	// 		img.onload = function(){
	// 			count++;
	// 			if(count >= totalImage){
	// 				SIA.preloader.hide();
	// 			}
	// 		};
	// 		img.src = this.src;
	// 	});
	// };

	var initTooltip = function() {
		if ($('[data-tooltip]')) {
			$('[data-tooltip]').each(function() {
				if (!$(this).data('kTooltip')) {
					$(this).kTooltip();
				}
			});
		}
	};

	// var addClassForWindowPhone = function(){
	// 	if(window.navigator.msMaxTouchPoints){
	// 		body.addClass('windows-phone');
	// 	}
	// };

	var initClearText = function() {
		$('input:text, input[type="tel"]').not('[data-start-date], [data-return-date], [data-oneway], [readonly]').addClear();
	};

	var triggerValidateByGroup = function() {
		// $('[data-rule-require_from_group]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group');
		// 	var elements = $(data[1]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });
		// $('[data-rule-require_from_group_if_check]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group_if_check');
		// 	var elements = $(data[3]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });

		// $('form').on('submit.validatedOnce', function() {
		// 	$(this).data('validatedOnce', true);
		// });

		$('[data-rule-validatedate]').each(function() {
			var self = $(this);
			var data = self.data('rule-validatedate');
			var elements = $(data.join(','));
			elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
				var el = $(this);
				var validator = el.closest('form').data('validator');
				// var validatedOnce = el.closest('form').data('validatedOnce');
				if (validator) {
					// validator.element(self);
					if (!el.is(':hidden') && !el.hasClass('ignore-validate')) {
						validator.element(el);
					}
				}
			});
		});
		$('[data-rule-requiredignorable]').each(function(i, it) {
			var self = $(it);
			var form = self.closest('form');
			var dataRule = self.data('rule-requiredignorable');
			var ignoreCheckbox = dataRule[1].replace(':checked', '');
			$(ignoreCheckbox).off('change.ignore-' + i).on('change.ignore-' + i, function() {
				// if (form.data('validator') && form.data('validatedOnce')) {
				if (form.data('validator')) {
					form.data('validator').element(self);
				}
			});
		});

		var vldDate = $('[data-rule-validatedate]');
		if (vldDate.length) {
			vldDate.each(function() {
				var self = $(this);
				var ar = self.data('rule-validatedate');
				// var formValidated = self.closest('form');
				$(ar[0]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[1]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[1]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[1]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
			});
		}
		var vldPp = $('[data-rule-checkpassport]');
		if (vldPp.length) {
			vldPp.each(function() {
				var self = $(this);
				var ar = self.data('rule-checkpassport')[1].split(',');
				// var formValidated = self.closest('form');
				$(ar[1]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[1]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
			});
		}
	};

	var triggerAutocomplete = function() {
		$('[data-autocomplete] input').not(':submit').off('click.autocomplete').on('click.autocomplete', function() {
			if ($(this).is(':focus')) {
				$(this).autocomplete('search', '');
			}
		});
	};

	var initTriggerPopup = function() {
		var triggerPopup = $('[data-trigger-popup]');
		var flyingFocus = $('#flying-focus');

		triggerPopup.each(function() {
			var self = $(this);
			var passengerPage = $('.passenger-details-page');

			if (typeof self.data('trigger-popup') === 'boolean') {
				return;
			}

			var popup = $(self.data('trigger-popup'));
			var measureScrollbar = (function(){
				var a = document.createElement('div');
				a.className = 'modal-scrollbar-measure';
				body.append(a);
				var b = a.offsetWidth - a.clientWidth;
				return $(a).remove(), b;
			})();

			if (!popup.data('Popup')) {
				popup.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close], .cancel', // .btn-1 will have class cancel
					beforeShow: function(that){
						if(passengerPage.length){
							$('.booking-summary.active').addClass('reset-zindex');
						}

						that.modal.find('.popup__content').attr({
							'aria-hidden' : false
						});
					},
					afterShow: function(that) {
						flyingFocus = $('#flying-focus');
						if (flyingFocus.length) {
							flyingFocus.remove();
						}

						that.modal.attr({
							'open' : 'true',
							'role' : 'dialog'
						});

						var help = 'Beginning of dialog window. Press escape to cancel and close this window.'; // TODO l10n.prop
						that.modal.find('#popup--help').remove();
						that.modal.find('.popup__content').removeAttr('role');
						that.modal.prepend('<p id="popup--help" class="says">' + help + '</p>');

						that.modal.find('.popup__content').attr({
							'aria-describedby' : 'popup--help',
							'aria-hidden' : false
						});

						that.modal.find('.popup__content').find('[tabindex]').first().attr('aria-hidden', false).focus();
						var pEl =that.modal.find('.popup__content').find('p[tabindex]');
						pEl.each(function(i, v){
              $(v).attr({'aria-label': $(v).text() });
            });
					},
					afterHide: function(that){
						container.css('padding-right', '');
						body.css('overflow', '');
						if(vars.isSafari){
							body.attr('style', function(i, s) { return s.replace('overflow:hidden !important;','');});
						}
						if(passengerPage.length){
							$('.booking-summary.active').removeClass('reset-zindex');
						}

						that.modal.find('.popup__content').attr({
							'aria-hidden' : true
						});

					}
				});
			}
			self.off('click.showPopup').on('click.showPopup', function(e) {
				e.preventDefault();
				if (!self.hasClass('disabled')) {
					container.css('padding-right', measureScrollbar);
					body.attr('style', function(i, s) { return s ? s + 'overflow:hidden !important;' : 'overflow:hidden !important;'; });
					popup.Popup('show');
				}
			});
		});
	};

	var loginFormValidation = function() {
		$('.form--login').each(function(){
			$(this).validate({
				focusInvalid: true,
				errorPlacement: vars.validateErrorPlacement,
				success: vars.validateSuccess
			});
		});
	};

	var globalFormValidation = function() {
		$('[data-global-form-validate]').validate({
			focusInvalid: true,
			errorPlacement: vars.validateErrorPlacement,
			success: vars.validateSuccess,
			onfocusout: vars.validateOnfocusout,
			invalidHandler: vars.invalidHandler
		});
	};

	var initDisabledLink = function() {
		var disabledLink = $('[data-disabled-link]');
		if (disabledLink.length) {
			disabledLink.off().on('click', function(e) {
				if ($(this).hasClass('disabled')) {
					e.preventDefault();
					e.stopImmediatePropagation();
				}
			});
		}
	};

	var addClassIE10 = function() {
		if (vars.ieVersion() === 10) {
			html.addClass('ie10');
		}
	};

	// var initAutocompleteSearch = function() {
	// 	var global = SIA.global;
	// 	var vars = global.vars;
	// 	var timer = null;
	// 	var term = '';

	// 	var getInternetExplorerVersion = function() {
	// 		var rv = -1;
	// 		if (navigator.appName === 'Microsoft Internet Explorer') {
	// 			var ua = navigator.userAgent;
	// 			var re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})');
	// 			if (re.exec(ua) !== null) {
	// 				rv = parseFloat(RegExp.$1);
	// 			}
	// 		} else if (navigator.appName === 'Netscape') {
	// 			var ua = navigator.userAgent;
	// 			var re = new RegExp('Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})');
	// 			if (re.exec(ua) !== null) {
	// 				rv = parseFloat(RegExp.$1);
	// 			}
	// 		}
	// 		return rv;
	// 	};

	// 	if (getInternetExplorerVersion() === 11) {
	// 		vars.html.addClass('ie11');
	// 	}

	// 	$('.form-search').each(function() {
	// 		var field = $(this).find('input:text');
	// 		var searchAutoComplete = field.autocomplete({
	// 			open: function() {
	// 				$(this).autocomplete('widget').jScrollPane({
	// 					scrollPagePercent: 10
	// 				}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
	// 					e.preventDefault();
	// 					e.stopPropagation();
	// 				});
	// 			},
	// 			source: function(request, response) {
	// 				term = request.term;
	// 				$.get('ajax/autocomplete-search.json', function(data) {
	// 					var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
	// 					var source = $.grep(data.searchSuggests, function(item){
	// 						return matcher.test(item);
	// 					});
	// 					response(source);
	// 				});
	// 			},
	// 			search: function() {
	// 				var self = $(this);
	// 				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
	// 			},
	// 			close: function() {
	// 				var self = $(this);
	// 				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
	// 			}
	// 		}).data('ui-autocomplete');

	// 		searchAutoComplete._renderItem = function(ul, item) {
	// 			if (!ul.hasClass('auto-suggest')) {
	// 				ul.addClass('auto-suggest');
	// 			}
	// 			var termLength = term.length;
	// 			var label = item.label;
	// 			var liContent = '<span class="text-suggest">' + label.substr(0, termLength) +
	// 				'</span>' + label.substr(termLength, label.length - 1);
	// 			return $('<li class="autocomplete-item">')
	// 				.attr('data-value', item.value)
	// 				.append($('<a href="javascript:void(0);">').html(liContent))
	// 				.appendTo(ul);
	// 		};

	// 		searchAutoComplete._move = function(direction) {
	// 			var item, previousItem,
	// 				last = false,
	// 				api = this.menu.element.data('jsp'),
	// 				li = $(),
	// 				minus = null,
	// 				currentPosition = api.getContentPositionY();

	// 			switch(direction){
	// 				case 'next':
	// 					if(this.element.val() === ''){
	// 						api.scrollToY(0);
	// 						li = this.menu.element.find('li:first');
	// 						item = li.addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					else{
	// 						previousItem = this.menu.element.find('li.active').removeClass('active');
	// 						li = previousItem.next();
	// 						item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					if(!item){
	// 						last = true;
	// 						li = this.menu.element.find('li').removeClass('active').first();
	// 						item = li.addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					this.term = item.value;
	// 					this.element.val(this.term);
	// 					if(last){
	// 						api.scrollToY(0);
	// 						last = false;
	// 					}
	// 					else{
	// 						currentPosition = api.getContentPositionY();
	// 						minus = li.position().top + li.innerHeight();
	// 						if(minus - this.menu.element.height() > currentPosition){
	// 							api.scrollToY(Math.max(0, minus - this.menu.element.height()));
	// 						}
	// 					}
	// 					break;
	// 				case 'previous':
	// 					if(this.element.val() === ''){
	// 						last = true;
	// 						item = this.menu.element.find('li:last').addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					else{
	// 						previousItem = this.menu.element.find('li.active').removeClass('active');
	// 						li = previousItem.prev();
	// 						item = li.removeClass('active').addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					if(!item){
	// 						last = true;
	// 						item = this.menu.element.find('li').removeClass('active').last().addClass('active').data('ui-autocomplete-item');
	// 					}
	// 					this.term = item.value;
	// 					this.element.val(this.term);
	// 					if(last){
	// 						api.scrollToY(this.menu.element.find('.jspPane').height());
	// 						last = false;
	// 					}
	// 					else{
	// 						currentPosition = api.getContentPositionY();
	// 						if(li.position().top <= currentPosition){
	// 							api.scrollToY(li.position().top);
	// 						}
	// 					}
	// 					break;
	// 			}
	// 		};

	// 		field.autocomplete('widget').addClass('autocomplete-menu');

	// 		field.off('blur.autocomplete');
	// 		field.off('focus.highlight').on('focus.highlight', function (e) {
	// 			e.stopPropagation();
	// 			var self = $(this);
	// 			self.closest('.custom-select').addClass('focus');

	// 			if(global.vars.isIE()){
	// 				doc.off('mousedown.hideAutocompleteLanguage').on('mousedown.hideAutocompleteLanguage', function(e){
	// 					if(!$(e.target).closest('.ui-autocomplete').length){
	// 						field.closest('.custom-select').removeClass('focus');
	// 						field.autocomplete('close');
	// 					}
	// 				});
	// 			}
	// 		});

	// 		if(!global.vars.isIE()){
	// 			field.off('blur.highlight').on('blur.highlight', function(){
	// 				timer = setTimeout(function(){
	// 					field.closest('.custom-select').removeClass('focus');
	// 					field.autocomplete('close');
	// 				}, 200);
	// 				if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
	// 					win.off('resize.reposition');
	// 				}
	// 			});
	// 			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
	// 				clearTimeout(timer);
	// 			});
	// 		}

	// 		field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
	// 			e.stopPropagation();
	// 		});

	// 		field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
	// 			e.stopPropagation();
	// 		});

	// 		field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
	// 			if(e.which === 13){
	// 				e.preventDefault();
	// 				if(field.autocomplete('widget').find('li').length === 1){
	// 					field.autocomplete('widget').find('li').trigger('click');
	// 					return;
	// 				}
	// 				field.autocomplete('widget').find('li.active').trigger('click');
	// 			}
	// 		});
	// 	});
	// };

	var globalFun = function() {
		animationNavigationOnDesktop();
		selectLanguage();
		loginPopup();
		// loggedProfile();
		menuTablet();
		// loader();
		initTooltip();
		addClassForSafari();
		initClearText();
		triggerValidateByGroup();
		// addClassForWindowPhone();
		triggerAutocomplete();
		initTriggerPopup();
		loginFormValidation();
		globalFormValidation();
		initDisabledLink();
		addClassIE10();

		// initAutocompleteSearch();
	};

	// it is used on mobile
	var backgroundImage = function() {
		var prImgs = $('#banner-slider .slide-item');
		if (!(window.darkSite && vars.ieVersion() && vars.ieVersion() >= 10)) {
			prImgs = prImgs.add($('.full-banner--img'));
		}

		prImgs.each(function(idx) {
			var prImg = $(this);
			var img = prImg.find('img');

			if (!body.hasClass('home-page')) {
				var nImage = new Image();
				nImage.onload = function() {
					var nWidth = nImage.width;
					var nHeight = nImage.height;
					var rate = nHeight/nWidth;

					img.data('img-rate', rate);

					if (win.width() > 1280) {
						if(!body.hasClass('promotion-components')){
							var settedHeightEls = img.closest('.flexslider').length ?
							img.closest('.flexslider').add(img.closest('.slide-item')) : img;
							settedHeightEls.height(win.width() * rate);
						}
					}
				};

				nImage.src = img.attr('src');
			}

			prImg.css({
				'background-image': 'url(' + img.attr('src') + ')'
			});

			img.attr('src', config.imgSrc.transparent);

			if (prImg.is('.full-banner--img')) {
				prImg.removeClass('visibility-hidden');
			}

			var resizeFullBanner = function(){
				var winW = window.innerWidth;

				if(vars.detectDevice.isTablet() && prImg.data('tablet-bg')){
					prImg.css({
						'background-position': prImg.data('tablet-bg')
					});
				}
				else if(winW > config.tablet){
					if(prImg.data('desktop')){
						prImg.css('background-position', prImg.data('desktop'));
					}
					else{
						prImg.css('background-position', '');
					}

					if (!body.hasClass('home-page')) {
						var settedHeightEls = img.closest('.flexslider').length ?
							img.closest('.flexslider').add(img.closest('.slide-item')) : img;

						if (winW > 1280) {
							if(!body.hasClass('promotion-components')){
								var rate = parseFloat(img.data('img-rate'));
								settedHeightEls.height(win.width() * rate);
							}
						}
						else {
							settedHeightEls.css('height', '');
						}
					}
				}
			};

			win.off('resize.fullBannerImg' + idx).on('resize.fullBannerImg' + idx, resizeFullBanner).trigger('resize.fullBannerImg' + idx);
		});
	};

	$.fn.resetForm = function() {
		return this.each(function() {
			// $(this).data('validatedOnce', null);
			$(this).find('.error:input').parents('.error').removeClass('error');
			$(this).find('.text-error').remove();
		});
	};
	// vars.validationRuleByGroup = [];
	vars.validateErrorPlacement = function(error, element) {
		// if (!$(element).closest('form').data('validatedOnce')) {
		// 	return;
		// }
		if (element.is(':disabled')) { return; }
		// if (!$(element).closest('form').data('validatedOnce')) {
		// 	return;
		// }
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		var inputsInGroup = group.find(':input:visible').filter(function() {
			return (jQuery.isEmptyObject($(this).rules()) === false);
		});
		var validator = $(element).closest('form').data('validator');
		var validationRuleByGroup = vars.validationRuleByGroup;
		var errorList = validator.errorList;
		var errorId = $(element).attr('id') + '-error';

		var appendError = function(container) {
			container.addClass('error');

			// fix issue: elements are not required validation in form-group
			var ignoreEl = container.find('[data-ignore]');
			if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
				ignoreEl.closest('.grid-col').addClass('ignore');
			}

			if (!container.children('.text-error').length && error.text()) {
				$(config.template.labelError)
					.appendTo(container)
					.attr( 'id', errorId )
					.find('span')
					.text(error.text());

				SIA.WcagGlobal.assignDescriptionAttributes({
					descriptionSelector: '#' + errorId,
					descriptionCategory: 'validationError',
					uniqueHostSelector: '#' + $(element).attr('id')
				});
				SIA.WcagGlobal.attachDescription('#' + errorId);
				$(element).data('errorMessage', container.children('.text-error'));
			}else{
				if(error.text()){
					container.children('.text-error').find('span').text(error.text());
				}
				else{
					SIA.WcagGlobal.detachDescription('#' + errorId);
					container.children('.text-error').remove();
				}
			}
		};

		var theRestIsOk = function() {
			return inputsInGroup.not($(element)).filter(function() {
				return (validator.check($(this)) === false);
			}).length === 0;
		};

		//If it's not validating by group
		if (!isValidateByGroup) {
			appendError(gridInner);

			//check if the current rule is a rule-by-group
			var isRuleByGroup = false;
			if (validationRuleByGroup) {
				for (var i = errorList.length - 1; i >= 0; i--) {
					if ($(element).is(errorList[i].element) && validationRuleByGroup.indexOf(errorList[i].method) >= 0) {
						isRuleByGroup = true;
						break;
					}
				}
			}

			//if current rule is a rule-by-group and it's fail
			if (isRuleByGroup) {
				//and the rest input in group are ok
				if (theRestIsOk()) {
					//remove old error message in group if exists
					if (group.children('.text-error').length) {
						group.children('.text-error').remove();
					}
					gridInner.removeClass('error');
					//append new error message to group
					group.append($(element).data('errorMessage')).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}
				} else {
					//if the rest input are not ok
					//just show separate error and temporarily hide group error
					gridInner.removeClass('error').children('.text-error').remove();
				}
			} else {
				//if it's another separate error
				if (error.text() !== '') {
					//remove group error
					if (inputsInGroup.length > 1) {
						group.removeClass('error').children('.text-error').remove();

						// fix issue: elements are not required validation in form-group
						var ignoreEl = group.find('[data-ignore]');
						if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
							ignoreEl.closest('.grid-col').removeClass('ignore');
						}
					}
				} else {
					//if it's ok, need to detect it's ok for rule-group or rule-element
					for (var z = validationRuleByGroup.length - 1; z >= 0; z--) {
						if ($(element).rules()[validationRuleByGroup[z]]) {
							group.removeClass('error').children('.text-error').remove();

							// fix issue: elements are not required validation in form-group
							var ignoreEl = group.find('[data-ignore]');
							if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
								ignoreEl.closest('.grid-col').removeClass('ignore');
							}

							break;
						}
					}
				}
			}
		} else {
			//validating by group
			appendError(gridInner);
			if (inputsInGroup.index($(element)) === 0) {
				if (!$(element).data('elementValidator')) {
					$(element).data('elementValidator', $.extend({}, validator));
				}
			}
			if (!$(element).data('being-validated')) {
				inputsInGroup.data('being-validated', true);
				inputsInGroup.each(function() {
					inputsInGroup.eq(0).data('elementValidator') && inputsInGroup.eq(0).data('elementValidator').element($(this));
				});
				inputsInGroup.data('being-validated', false);
				group.children('.text-error').remove();
				var firstError = group.find('.text-error').filter(function() {
					return ($(this).find('span').text() !== '');
				}).first();

				if (firstError.length) {
					group.append(firstError).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}

					group.find('.text-error').not(firstError).remove();
				} else {
					group.removeClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').removeClass('ignore');
					}

					group.find('.error:not(:input)').removeClass('error');
				}
			}
		}
	};

	vars.validateSuccess = function(label, element) {
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		// var inputsInGroup = group.find(':input');
		// var validator = $(element).closest('form').data('validator');

		var removeMessage = function(container) {
			container.removeClass('error');
		};
		//If it's not validating by group
		if (!isValidateByGroup) {
			removeMessage(gridInner);
		}
	};

	vars.validateOnfocusout = function(element) {
		var formEl = $(element).closest('form'),
			validator = formEl.data('validator');
		var container = $(element).closest('.error');
		if (container.data('validate-by-group')) {
			if (container.find('.error:input').not($(element)).length) {
				return;
			}
		}

		if (vars.validationRuleByGroup) {
			var group = $(element).closest('.form-group, [data-validate-row]'),
				strRules = '';
			for (var i = vars.validationRuleByGroup.length - 1; i >= 0; i--) {
				strRules += '[data-rule-' + vars.validationRuleByGroup[i] + ']' + (i !== 0 ? ',' : '');
			}
			if (strRules) {
				var ruleEl = group.find(strRules);
				if (ruleEl.length) {
					// ruleEl.trigger('change');
					validator.element(element);
					validator.element(ruleEl);
					return;
				}
			}
		}

		if (this.element(element) && !this.checkable(element)) {
			$(element)
				.closest('.error').removeClass('error').addClass('success')
				.children('.text-error').remove();
		}
		if (this.element(element) && !this.checkable(element)) {
			$(element)
				.closest('.error').removeClass('error').addClass('success')
				.children('.text-error').remove();
		}
	};

	vars.invalidHandler = function(form, validator) {
		var errors = validator.numberOfInvalids();
		if (errors) {
			var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
			if (errorColEl.length && !errorColEl.parents('.popup__content').length) {
				win.scrollTop(errorColEl.offset().top - 40);
			}
		}
	};

	return {
		vars: vars,
		config: config,
		globalFun: globalFun,
		backgroundImage: backgroundImage
	};
})();

SIA.preloader = (function() {
	var preload = $('.overlay-loading');
	var show = function(callback) {
		preload.removeClass('hidden');
		if (typeof(callback) === 'function') {
			callback();
		}
	};

	var hide = function(callback) {
		if (!preload.data('never-stop')) {
			if(preload.data('planloading') !== undefined) {
				SIA.planLoading.goEnd();
			} else {
				preload.addClass('hidden');
			}
			preload.trigger('afterHide.ready');
		}
		if (typeof(callback) === 'function') {
			callback();
		}
	};

	var isVisible = function() {
		return preload.is(':visible');
	};

	return {
		show: show,
		hide: hide,
		isVisible: isVisible
	};
})();

SIA.planLoading = (function() {

	var interBlock = $('.interstitial-block'),
			interEl = $('.interstitial-2'),
	    startPoint = interEl.data('start'),
	    endPoint = interEl.data('end'),
	    listCircle = $('.rotate-circle'),
	    staDeviation = 9,
	    cungDeg = 1.5,
	    originalDeg = (180 - (staDeviation * 2)) / 5,
	    startDeg = 113 + (originalDeg * startPoint),
	    endDeg = 90 + staDeviation + (originalDeg * endPoint ) - 2.5,
	    endDeg1 = endDeg - 0.2 * (endDeg - startDeg),
	    win = $(window);

	var activeCircle = function(index, callback) {

		listCircle.eq(index).addClass('active').css('z-index', 2);

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var init = function(callback) {


		listCircle.each(function(index){
			  var _self = $(this),
			      _selfDeg = 90 + staDeviation + (_self.data('step')*originalDeg) + cungDeg;
			  _self.css({
			  	'transform' : 'rotate('+_selfDeg+'deg)',
				});

				_self.attr('data-degree', _selfDeg);

				if(endPoint - startPoint < 2) {
				  if(index < startPoint) {
				  	_self.addClass('active');
				  }
				}
		})

		if(endPoint - startPoint < 2) {

			interEl.css({'transform' : 'rotate('+startDeg+'deg)'});

			setTimeout(function(){
			  interEl.css({
			  'transition': 'transform 1s linear'
			  });
			},1);

			setTimeout(function(){
			  interEl.css({
			  	'transform' : 'rotate('+ endDeg1 +'deg)'});
			},2);

		} else {
			listCircle.css('z-index', 0);
		  interEl.animate({
		  	deg: startDeg
	  	}).animate({
	    	deg: endDeg1
	  	}, {
		  duration: 3000,
		  easing: "swing",
	  	step: function(now){
	  		interEl.css({
          'transform':'rotate('+now+'deg)'
        });

        if(now > $('[data-step="1"]').data('degree') + 13) {
        	activeCircle(0);
        }
        if(now > $('[data-step="2"]').data('degree') + 13) {
        	activeCircle(1);
        }
        if(now > $('[data-step="3"]').data('degree') + 13) {
        	activeCircle(2);
        }
        if(now > $('[data-step="4"]').data('degree') + 13) {
        	activeCircle(3);
        }
	  	 }
		 })
		}

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var goEnd = function(callback) {

		if(endPoint - startPoint < 2) {
			if (endPoint === 1) {
				endDeg -= 0.5;
			}
			interEl.css({'transform' : 'rotate('+endDeg+'deg)'});
			setTimeout(function(){
				 	$('.overlay-loading').addClass('hidden');
			},1000);
		} else {
			interEl.animate({
			  	deg: endDeg1
		  	}).animate({
		    	deg: endDeg
		  	}, {
			  duration: 1000,
			  easing: "swing",
		  	step: function(now){
		  		interEl.css({
	          'transform':'rotate('+now+'deg)'
	        });
	        if(now > $('[data-step="1"]').data('degree') + 13) {
	        	activeCircle(0);
	        }
	        if(now > $('[data-step="2"]').data('degree') + 13) {
	        	activeCircle(1);
	        }
	        if(now > $('[data-step="3"]').data('degree') + 13) {
	        	activeCircle(2);
	        }
	        if(now > $('[data-step="4"]').data('degree') + 13) {
	        	activeCircle(3);
	        }
		  	 },
		  	 complete: function(){
		  	 	$('.overlay-loading').addClass('hidden');
		  	 }
			 })
		}

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var rePosition = function(callback) {

		if (typeof(callback) === 'function') {
			callback();
		};

	}

	return {
		init: init,
		goEnd: goEnd,
		rePosition: rePosition
	};

})();

(function($) {
	SIA.planLoading.init();

	$(document).ready(function() {
		// SIA.global = global();
		// SIA.preloader = preloader();
		var body = $(document.body);

		var loadScript = function(url, callback, location) {
			if (!url || (typeof url !== 'string')) {
				return;
			}
			var script = document.createElement('script');
			//if this is IE8 and below, handle onload differently
			if (typeof document.attachEvent === 'object') {
				script.onreadystatechange = function() {
					//once the script is loaded, run the callback
					if (script.readyState === 'loaded' || script.readyState === 'complete') {
						if (callback) {
							callback();
						}
					}
				};
			} else {
				//this is not IE8 and below, so we can actually use onload
				script.onload = function() {
					//once the script is loaded, run the callback
					if (callback) {
						callback();
					}
				};
			}
			//create the script and add it to the DOM
			script.src = url;
			document.getElementsByTagName(location ? location : 'head')[0].appendChild(script);
		};

		var urlScript = {
			scripts: {
				'underscore': 'scripts/underscore.js',
				'jqueryui': 'scripts/jquery-ui-1.10.4.custom.js',
				'validate': 'scripts/jquery.validate.js',
				'slick': 'scripts/slick.js',
				'mousewheel': 'scripts/jquery.mousewheel.js',
				'jscrollpane': 'scripts/jquery.jscrollpane.js',
				'inputmask': 'scripts/jquery.inputmask.js',
				'grayscale': 'scripts/grayscale.js',
				'accounting': 'scripts/accounting.min.js',
				'placeholder': 'scripts/jquery.placeholder.js',
				'cleartext': 'scripts/clear-text.js',
				'seatmap': 'scripts/seat-map.js',
				'chart': 'scripts/chart.js',
				'highcharts': 'scripts/highcharts.js',
				'isotope': 'scripts/isotope.js',
				'shuffle': 'scripts/jquery.shuffle.js',
				'youtubeAPI': 'https://www.youtube.com/iframe_api',
				'flowplayerAPI': '//releases.flowplayer.org/6.0.1/flowplayer.min.js',
				'tiff': 'scripts/tiff.min.js',
				'recaptcha': 'https://www.google.com/recaptcha/api.js'
			},
			modules: {
				'flightTableBorder': 'scripts/flight-table-border.js',
				'forceInput': 'scripts/force-input.js',
				'multipleSubmit': 'scripts/multiple-submit.js',
				'contact': 'scripts/contact.js',
				'simpleSticky': 'scripts/simple-sticky.js',
				'coachBeta': 'scripts/coach-beta.js',
				'sticky': 'scripts/sticky.js',
				'bar': 'scripts/bar.js',
				'filterEntertainment': 'scripts/filter-entertainment.js',
				'blockScroll': 'scripts/block-scroll.js',
				'accordion': 'scripts/accordion.js',
				'addon': 'scripts/add-on.js',
				'atAGlance': 'scripts/at-a-glance.js',
				'initAutocompleteCity': 'scripts/autocomplete-city.js',
				'autocompleteAirport': 'scripts/autocomplete-airport.js',
				'bookingSummnary': 'scripts/booking-summary.js',
				'CIBBookingSummary': 'scripts/cib-booking-summary.js',
				'cibConfirmation': 'scripts/cib-confirmation.js',
				'KFClaimMissingMiles': 'scripts/claim-missing-miles.js',
				'KFMileExpiring': 'scripts/expiring-miles.js',
				'KFFavourite': 'scripts/favourites.js',
				'flightCalendar': 'scripts/flight-calendar.js',
				'flightSchedule': 'scripts/flight-schedule.js',
				'flightSearchCalendar': 'scripts/flight-search-calendar.js',
				'flightSelect': 'scripts/flight-select.js',
				'flightStatus': 'scripts/flight-status.js',
				'home': 'scripts/home.js',
				'darkSiteHome': 'scripts/dark-site-home.js',
				'KFMileHowToEarn': 'scripts/how-to-earn.js',
				'KFMileHowToUse': 'scripts/how-to-use.js',
				'flightScheduleTrip': 'scripts/init-flight-schedule-trip.js',
				'initPersonTitles': 'scripts/init-personTitles.js',
				'initTabMenu': 'scripts/init-tabMenu.js',
				'KFCheckIns': 'scripts/kf-check-ins.js',
				'KFFlightHistory': 'scripts/kf-flight-history.js',
				'KFMessageForward': 'scripts/kf-message-forward.js',
				'KFMessage': 'scripts/kf-message.js',
				'KFPartnerProgramme': 'scripts/kf-partner-programme.js',
				'KFPersonalDetail': 'scripts/kf-personal-detail.js',
				'KFRedemptionNominee': 'scripts/kf-redemption-nominee-add.js',
				'KFTicketReceipt': 'scripts/kf-tickets-and-receipts.js',
				'KFUpcomingFlights': 'scripts/kf-upcoming-flights.js',
				'multicity': 'scripts/multicity.js',
				'newsTickerContent': 'scripts/newsTickerContent.js',
				'orbConfirmation': 'scripts/orb-confirmation.js',
				'ORBFlightSelect': 'scripts/orb-flight-search.js',
				'passengerDetail': 'scripts/passenger-detail.js',
				'payment': 'scripts/payment.js',
				'promotionKrisflyer': 'scripts/promotion-krisflyer.js',
				'promotion': 'scripts/promotion.js',
				'KFRedeemMiles': 'scripts/redeem-miles.js',
				'roundTrip': 'scripts/round-trip.js',
				'selectCabin': 'scripts/select-cabin.js',
				'selectReturnDate': 'scripts/select-return-date.js',
				'KFStatement': 'scripts/statements.js',
				'darkSiteStatements': 'scripts/dark-site-statements.js',
				'stickySidebar': 'scripts/sticky-sidebar.js',
				// 'FormCheckChange' : 'scripts/form-check-change.js',
				'ORBBookingSummary': 'scripts/orb-booking-summary.js',
				'initAutoFilledPassenger': 'scripts/init-auto-filled-passenger.js',
				'initCorrectDate': 'scripts/init-corect-date.js',
				'kfRegistration': 'scripts/kf-registration.js',
				'sqcRegistration': 'scripts/sqc-registration.js',
				'specialAssistance': 'scripts/special-assistance.js',
				'manageBooking': 'scripts/manage-booking.js',
				'excessBaggage': 'scripts/excess-baggage.js',
				'mbConfirmation': 'scripts/mb-confirmation.js',
				'mbSelectMeal': 'scripts/mb-select-meals.js',
				'MBBookingSummary': 'scripts/mb-booking-summary.js',
				'SQCExpenditure': 'scripts/sqc-expenditure.js',
				'sqcUpcomingFlight': 'scripts/sqc-upcoming-flight.js',
				'SQCUser': 'scripts/sqc-user.js',
				'SQCAddUser': 'scripts/sqc-add-user.js',
				'sqcAtAGlance': 'scripts/sqc-at-a-glance.js',
				'SQCFlightHistory': 'scripts/sqc-flight-history.js',
				'sqcSavedTrips': 'scripts/sqc-saved-trips.js',
				'desWhereTo': 'scripts/des-where-to-stay.js',
				'desEntry': 'scripts/des-entry.js',
				'DESCityGuide': 'scripts/des-city-guide.js',
				'staticContentKrisflyer': 'scripts/static-content-krisflyer.js',
				'sshHotel': 'scripts/ssh-hotel.js',
				'staticContentMusic': 'scripts/static-content-music.js',
				'sshSelection': 'scripts/ssh-selection.js',
				'sshAdditional': 'scripts/ssh-additional.js',
				'mbChangeFlight': 'scripts/mb-change-flight.js',
				'staticContentComponents': 'scripts/static-content-components.js',
				'multiTabsWithLongText': 'scripts/multitabs-with-long-text.js',
				'staticContentGeneric': 'scripts/static-content-generic.js',
				'staticContentHeritage': 'scripts/static-content-heritage.js',
				'moreInTheSection': 'scripts/more-in-the-section.js',
				'feedBack': 'scripts/feed-back.js',
				'kfUnsubscribe': 'scripts/kf-unsubscribe.js',
				'kfVoucherRedemption': 'scripts/kf-voucher-redemption.js',
				'youtubeApp': 'scripts/youtube-app.js',
				'flowPlayerApp': 'scripts/flowplayer-app.js',
				'landingSearchFlight': 'scripts/landing-search-flight.js',
				'tab': 'scripts/tab.js',
				'grayscaleModule': 'scripts/grayscale-module.js',
				'autocompleteSearch': 'scripts/autocomplete-search.js',
				'chatWidget': 'scripts/chat-widget.js',
				'faqs': 'scripts/faqs.js',
				'plusOrMinusNumber': 'scripts/plus-or-minus-number.js',
				'radioTabsChange': 'scripts/radio-tabs-change.js',
				'countCharsLeft': 'scripts/count-chars-left.js',
				'addBaggage': 'scripts/add-baggage.js',
				'promotionsPackages': 'scripts/promotions-packages.js',
				'autocompleteCountryCity': 'scripts/autocomplete-country-city.js',
				'hotelListing': 'scripts/hotel-listing.js',
				'disableValue': 'scripts/disable-value.js',
				'togglePrivacy': 'scripts/toggle-privacy.js',
				'carouselFade': 'scripts/carousel-fade.js',
				'carouselSlide': 'scripts/carousel-slide.js',
				'pricePoints': 'scripts/price-points.js',
				'checkImage': 'scripts/check-image.js',
				'promotionComponent': 'scripts/promotion-component.js',
				'bookingWidget': 'scripts/booking-widget.js',
				'sortTableContent': 'scripts/sort-table-content.js',
				'redemptionNominee': 'scripts/redemption-nominee.js',
				'bookingMileClaim': 'scripts/kf-retroactive-mile-claim.js',
				'googleMap': 'scripts/googlemap.js',
				'baiduMap': 'scripts/baidumap.js',
				'selectMeals': 'scripts/mb-flight-select-meal.js',
				'donateMiles': 'scripts/donate-miles.js',
			},
			getScript: function(script) {
				return this.scripts[script];
			}
		};

		var comment = [
			SIA.global.globalFun,
			SIA.footer,
			SIA.cookiesUse,
			SIA.autoCompleteLanguage,
			SIA.highlightInput,
			SIA.fixPlaceholder,
			SIA.initCustomSelect,

			// set backgroundImage
			SIA.global.backgroundImage,
			// social
			SIA.socialNetworkAction,
			// accessibility
			SIA.initSkipTabIndex,
			// sub menu on device for KF
			SIA.initSubMenu,
			// language toolbar
			SIA.initLangToolbar,
			// checkbox confirm
			SIA.initConfirmCheckbox,
			SIA.initToggleButtonValue,
			// SIA.LoggedProfilePopup
			SIA.initAlly
		];

		var initFunction = comment;
		var requireScript = ['jqueryui', 'mousewheel', 'validate', 'jscrollpane', 'cleartext', 'placeholder'];
		var concatInitFunctionDone = false;
		var totalLoadedModules = 0;
		var loadedModules = 0;
		var modulesToLoad = [];
		var dashboard = {
			page: {
				'flightTableBorder': [
					'flightTableBorder'
				],
				'forceInput': [
					'forceInput'
				],
				'multipleSubmit': [
					'multipleSubmit'
				],
				'contact': [
					'contact',
					'initAutocompleteCity'
				],
				'simpleSticky': [
					'simpleSticky'
				],
				'coachBeta': [
					'coachBeta'
				],
				'sticky': [
					'sticky'
				],
				'bar': [
					'bar'
				],
				'grayscaleModule': [
					'grayscaleModule'/*,
					SIA.global.backgroundImage*/
				],
				'darkSiteStatements': [
					'darkSiteStatements'
				],
				'home': [
					'home',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					'autocompleteAirport',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip'
				],
				'darkSiteHome': [
					'darkSiteHome',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip'
				],
				'passengerDetail': [
					'initPersonTitles',
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initAutocompleteCity',
					'CIBBookingSummary',
					'initAutoFilledPassenger'
				],
				'orbPassengerDetail': [
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initPersonTitles',
					'initAutocompleteCity',
					'ORBBookingSummary',
					'initAutoFilledPassenger'
				],
				'seatMap': [
					'stickySidebar',
					// 'initTabMenu',
					'CIBBookingSummary'
				],
				'flightStatus': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'flightStatus'
					// 'initTabMenu'
				],
				'flightSchedule': [
					'flightSchedule',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'initAutocompleteCity',
					'selectCabin'
					// 'initTabMenu'
				],
				'flightCalendar': [
					'flightCalendar',
					'CIBBookingSummary'
				],
				'promotion': [
					'promotion'
				],
				'faresDetailsPage': [
					'roundTrip',
					'selectCabin',
					'promotion'
				],
				'promotionKrisflyer': [
					'promotionKrisflyer'
				],
				'bookingSummnary': [
					'bookingSummnary',
					'initAutocompleteCity'
					// 'initTabMenu'
				],
				'flightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'flightSelect',
					'CIBBookingSummary'
				],
				'orbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'ORBBookingSummary'
				],
				'mbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'MBBookingSummary'
				],
				'mbFlightSelectSf': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'CIBBookingSummary'
				],
				'flightSearchCalendar': [
					'flightSearchCalendar'
				],
				'multicity': [
					'initAutocompleteCity',
					'selectReturnDate',
					'selectCabin',
					'multicity'
				],
				'payment': [
					'payment',
					'initAutocompleteCity',
					'CIBBookingSummary'
				],
				'orbPayment': [
					'payment',
					'initAutocompleteCity',
					'ORBBookingSummary'
				],
				'mbPayment': [
					'payment'
				],
				'addon': [
					'addon',
					'CIBBookingSummary',
					'googleMap',
					'baiduMap',
					'multiTabsWithLongText',
					'initTabMenu',
					'plusOrMinusNumber',
					'roundTrip'
				],
				'orbAddOns': [
					'addon',
					'initTabMenu',
					'multiTabsWithLongText',
					'plusOrMinusNumber',
					'googleMap',
					'ORBBookingSummary'
				],
				'cibConfirmation': [
					'cibConfirmation',
					'CIBBookingSummary'
				],
				'atAGlance': [
					'atAGlance'
				],
				'accordion': [
					'accordion'
				],
				'countCharsLeft': [
					'countCharsLeft'
				],
				'howToEarn': [
					'initAutocompleteCity',
					'KFMileHowToEarn'
				],
				'howToUse': [
					'initAutocompleteCity',
					'KFMileHowToUse',
					'kfVoucherRedemption'
				],
				'redeemMiles': [
					'initAutocompleteCity',
					'KFRedeemMiles'
				],
				'kfMessage': [
					'KFMessage'
				],
				'kfMessageForward': [
					'KFMessageForward'
				],
				'kfPersonalDetail': [
					'KFPersonalDetail',
					'initAutocompleteCity',
					'initPersonTitles'
				],
				'kfRedemptionNominee': [
					'KFRedemptionNominee',
					'initAutocompleteCity',
					// 'initCorectDate'
					'initPersonTitles'
				],
				'kfPartnerProgramme': [
					'KFPartnerProgramme'
				],
				'kfClaimMissingMiles': [
					'KFClaimMissingMiles',
					'initAutocompleteCity'
				],
				'kfExpiringMiles': [
					'KFMileExpiring'
				],
				'kfUpcomingFlights': [
					'KFUpcomingFlights'
				],
				'kfStatement': [
					'KFStatement'
				],
				'KFFavourite': [
					'initAutocompleteCity',
					'KFFavourite'
				],
				'kfFlightHistory': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'KFFlightHistory'
				],
				'KFTicketReceipt': [
					'KFTicketReceipt'
				],
				'KFCheckIns': [
					'KFCheckIns'
				],
				'ORBConfirmation': [
					'orbConfirmation',
					'ORBBookingSummary'
				],
				// 'FormCheckChange': [
				// 	'FormCheckChange'
				// ],
				'ORBFlightSchedule': [
					'flightSearchCalendar'
				],
				// 'KFCreateNewPin': [
				// 	'initCorectDate'
				// ],
				'KFRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'kfRegistration'
				],
				'SQCRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'sqcRegistration'
				],
				'specialAssistance': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'specialAssistance'
				],
				'cancelWaitlistBooking': [
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'ManageBooking': [
					'manageBooking'
				],
				'ExcessBaggage': [
					'excessBaggage'
				],
				'MBConfirmation': [
					'mbConfirmation',
					'manageBooking'
				],
				'MBReview': [
					'manageBooking',
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'MBBookingSummary'
				],
				'MBSelectMeal': [
					'mbSelectMeal',
					'stickySidebar'
				],
				'MBSelectMealLand': [
					'stickySidebar',
					'accordion',
					'selectMeals',
					'multiTabsWithLongText',
					'initTabMenu'
				],
				'SQCExpenditure': [
					'SQCExpenditure'
				],
				'sqcUpcomingFlight': [
					'sqcUpcomingFlight'
				],
				'SQCUser': [
					'SQCUser'
				],
				'SQCAddUser': [
					'SQCAddUser',
					'initAutocompleteCity',
					'initPersonTitles'
					// 'initCorectDate'
				],
				'SQCAtAGlance': [
					'sqcAtAGlance'
				],
				'SQCFlightHistory': [
					'initAutocompleteCity',
					'SQCFlightHistory'
				],
				'SQCSavedTrips': [
					'sqcSavedTrips'
				],
				'initCorrectDate': [
					'initCorrectDate'
				],
				'desWhereTo': [
					'desWhereTo',
					'bookingWidget',
					'roundTrip',
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'desEntry': [
					'desEntry',
					'initAutocompleteCity'
				],
				'DESCityGuide': [
					'DESCityGuide',
					'roundTrip',
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'staticContentKrisflyer': [
					'staticContentKrisflyer'
				],
				'staticContentMusic': [
					'staticContentMusic'
				],
				'sshSelection': [
					'CIBBookingSummary',
					'roundTrip'
					// 'sshSelection'
				],
				'sshHotel': [
					'sshHotel',
					'CIBBookingSummary',
					'roundTrip'
					// 'sshSelection'
				],
				'sshAdditional': [
					'CIBBookingSummary',
					'sshAdditional'
				],
				'mbChangeFlight': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'mbChangeFlight'
				],
				'staticContentComponents': [
					// 'initTabMenu'
				],
				'multiTabsWithLongText': [
					'multiTabsWithLongText'
				],
				'staticContentMeals': [
					// 'initTabMenu'
				],
				'staticContentSpecMeals': [
					// 'initTabMenu'
				],
				'staticContentGeneric': [
					'staticContentGeneric'
				],
				'staticContentHeritage': [
					'staticContentHeritage'
				],
				'initTabMenu': [
					'initTabMenu'
				],
				'moreInTheSection': [
					'moreInTheSection'
				],
				'feedBack': [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack'
				],
				'bestFare' : [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack',
					'selectReturnDate',
					'roundTrip'
				],
				'kfUnsubscribe': [
					'kfUnsubscribe'
				],
				'checkRoom': [
					'sshSelection'
				],
				'youtubeApp': [
					'youtubeApp'
				],
				'flowPlayerApp': [
					'flowPlayerApp'
				],
				'landingSearchFlight': [
					'initAutocompleteCity',
					'landingSearchFlight',
					'roundTrip',
					'selectCabin',
					'selectReturnDate',
					'flightScheduleTrip'
				],
				'tab': [
					'tab'
				],
				'blockScroll': [
					'blockScroll'
				],
				'filterEntertainment': [
					'filterEntertainment'
				],
				'autocompleteSearch': [
					'autocompleteSearch'
				],
				'chatWidget': [
					'chatWidget'
				],
				'faqs': [
					'faqs'
				],
				'enewsSubscribe': [
					'initPersonTitles'
				],
				'contactUs': [
					'initAutocompleteCity'
				],
				'plusOrMinusNumber': [
					'plusOrMinusNumber'
				],
				'addBaggage': [
					'addBaggage'
				],
				'autocompleteCountryCity': [
					'autocompleteCountryCity'
				],
				'promotionsPackages': [
					'promotionsPackages',
					'initAutocompleteCity'
				],
				'radioTabsChange': [
					'radioTabsChange'
				],
				'hotelListing': [
					'hotelListing'
				],
				'disableValue': [
					'disableValue'
				],
				'togglePrivacy': [
					'togglePrivacy'
				],
				'carouselFade': [
					'carouselFade'
				],
				'carouselSlide': [
					'carouselSlide'
				],
				'promotionComponent': [
					'promotionComponent'
				],
				'pricePoints': [
					'pricePoints'
				],
				'checkImage': [
					'checkImage'
				],
				'bookingWidget': [
					'bookingWidget',
					'initAutocompleteCity',
					'roundTrip',
					'selectReturnDate',
					'selectCabin'
				],
				'sortTableContent': [
					'sortTableContent'
				],
				'redemptionNominee': [
					'initAutocompleteCity',
					'redemptionNominee'
				],
				'bookingMileClaim': [
					'bookingMileClaim'
				],
				'donateMiles': [
					'donateMiles'
				]
			},
			getPage: function(page) {
				//return this.page[page];
				var functions = this.page[page];
				totalLoadedModules += functions.length;
				$.each(functions, function(index) {
					modulesToLoad.push(functions[index]);
					loadScript(urlScript.modules[functions[index]], function() {
						var moduleIndex = modulesToLoad.indexOf(functions[index]);
						modulesToLoad[moduleIndex] = SIA[functions[index]];
						loadedModules++;
						if (totalLoadedModules === loadedModules) {
							initFunction = initFunction.concat(modulesToLoad);
							concatInitFunctionDone = true;
						}
					}, 'body');
				});
			}
		};

		if($('[data-flight]').length){
			dashboard.getPage('flightTableBorder');
		}

		if($('[data-rule-onlycharacter]').length || $('[data-rule-digits]').length){
			dashboard.getPage('forceInput');
		}

		if($('[data-multiple-submit]').length){
			dashboard.getPage('multipleSubmit');
		}

		if ($('[data-social]').length) {
			dashboard.getPage('contact');
		}

		if (window.simpleSticky) {
			dashboard.getPage('simpleSticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-sticky]').length) {
			dashboard.getPage('sticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-bar]').length) {
			dashboard.getPage('bar');
			requireScript = requireScript.concat(['underscore']);
		}

		if($('[data-coach-beta]').length){
			dashboard.getPage('coachBeta');
		}

		if ($('[data-autocomplete-search]').length) {
			dashboard.getPage('autocompleteSearch');
		}

		if ($('[data-tab]').length) {
			dashboard.getPage('initTabMenu');
		}

		if ($('[data-check-room]').length) {
			dashboard.getPage('checkRoom');
		}

		if ($('[data-carousel-fade]').length) {
			dashboard.getPage('carouselFade');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-carousel-slide]').length) {
			dashboard.getPage('carouselSlide');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-price-points]').length) {
			dashboard.getPage('pricePoints');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-check-image]').length) {
			dashboard.getPage('checkImage');
		}

		if (body.hasClass('statement-dark-site-page')) {
			dashboard.getPage('darkSiteStatements');
			requireScript = requireScript.concat(['slick']);
			if (!SIA.global.vars.isIETouch()) {
				requireScript = requireScript.concat(['youtubeAPI']);
			}
		}

		if(window.darkSite){
			dashboard.getPage('grayscaleModule');
		}

		if (body.hasClass('home-page') && !window.darkSite) {
			dashboard.getPage('home');
			requireScript = requireScript.concat(['underscore', 'slick', 'inputmask', 'accounting']);
		}

		if (body.hasClass('home-page') && window.darkSite) {
			dashboard.getPage('darkSiteHome');
			requireScript = requireScript.concat(['underscore', 'slick', 'inputmask', 'grayscale', 'accounting']);
		}

		if (body.hasClass('passenger-details-page')) {
			if (body.hasClass('orb-passenger-details-page')) {
				dashboard.getPage('orbPassengerDetail');
			} else {
				dashboard.getPage('passengerDetail');
			}
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}

		if (body.hasClass('mb-main-page')) {
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('seatsmap-page')) {
			dashboard.getPage('seatMap');
			requireScript = requireScript.concat(['underscore', 'accounting', 'seatmap']);
		}

		if (body.hasClass('flight-status-page')) {
			dashboard.getPage('flightStatus');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('flight-schedules-page')) {
			dashboard.getPage('flightSchedule');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}

		if (body.hasClass('fare-calendar-page')) {
			dashboard.getPage('flightCalendar');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}

		if (body.hasClass('fares-list-page')) {
			dashboard.getPage('promotion');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('fares-details-page')) {
			dashboard.getPage('faresDetailsPage');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('krisflyer-list-page')) {
			dashboard.getPage('promotionKrisflyer');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('booking-sumary-page') || body.hasClass('checkin-complete') || body.hasClass('relaunch-page')) {
			dashboard.getPage('bookingSummnary');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('flight-select-page')) {
			if (body.hasClass('orb-flight-select-page')) {
				dashboard.getPage('orbFlightSelect');
			} else if (body.hasClass('mb-flight-select-page')) {
				if (body.hasClass('mb-flight-select-page-sf')) {
					dashboard.getPage('mbFlightSelectSf');
				} else {
					dashboard.getPage('mbFlightSelect');
				}
			} else {
				dashboard.getPage('flightSelect');
			}
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}

		if (body.hasClass('flight-search-calendar-page')) {
			dashboard.getPage('flightSearchCalendar');
		}

		if (body.hasClass('multi-city-page')) {
			dashboard.getPage('multicity');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('payments-page')) {
			if (body.hasClass('orb-payments-page')) {
				dashboard.getPage('orbPayment');
			}
			else if (body.hasClass('mb-payments-page')) {
				dashboard.getPage('mbPayment');
			}
			else {
				dashboard.getPage('payment');
			}
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		// if (body.hasClass('mb-payments-page')) {
		// 	dashboard.getPage('mbPayment');
		// }
		if (body.hasClass('add-ons-page')) {
			if (body.hasClass('orb-add-ons')) {
				dashboard.getPage('orbAddOns');
			} else {
				dashboard.getPage('addon');
			}
			requireScript = requireScript.concat(['inputmask', 'slick', 'accounting', 'underscore']);
		}
		if (body.hasClass('cib-confirmation-page')) {
			dashboard.getPage('cibConfirmation');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if (body.hasClass('at-a-glance-page')) {
			dashboard.getPage('atAGlance');
			requireScript = requireScript.concat(['accounting', 'chart', 'underscore']);
		}
		if (body.hasClass('how-to-earn-page')) {
			dashboard.getPage('howToEarn');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('how-to-use-page')) {
			dashboard.getPage('howToUse');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if (body.hasClass('redeem-miles')) {
			dashboard.getPage('redeemMiles');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('messages-inbox-page')) {
			dashboard.getPage('kfMessage');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('message-forward-page')) {
			dashboard.getPage('kfMessageForward');
		}
		if (body.hasClass('personal-details-page') || body.hasClass('kf-profile-security') || body.hasClass('kf-preferences') /*|| body.hasClass('redemption-nominee-page')*/) {
			dashboard.getPage('kfPersonalDetail');
		}
		if (body.hasClass('redemption-nominee-page')) {
			dashboard.getPage('redemptionNominee');
		}

		if (body.hasClass('add-redemption-nominee-page')) {
			dashboard.getPage('kfRedemptionNominee');
		}
		if (body.hasClass('partner-programme-page')) {
			dashboard.getPage('kfPartnerProgramme');
		}
		if (body.hasClass('claim-missing-miles-page')) {
			dashboard.getPage('kfClaimMissingMiles');
		}
		if (body.hasClass('expiring-miles-page')) {
			dashboard.getPage('kfExpiringMiles');
		}
		if (body.hasClass('booking-upcoming-flights-page')) {
			dashboard.getPage('kfUpcomingFlights');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('statements-page')) {
			dashboard.getPage('kfStatement');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('booking-page')) {
			dashboard.getPage('kfFlightHistory');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}
		if (body.hasClass('ticket-receipt-page')) {
			dashboard.getPage('KFTicketReceipt');
		}
		if (body.hasClass('favourites-page')) {
			dashboard.getPage('KFFavourite');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('booking-check-ins-page')) {
			dashboard.getPage('KFCheckIns');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('orb-confirmation-page')) {
			dashboard.getPage('ORBConfirmation');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('orb-flight-schedule')) {
			dashboard.getPage('ORBFlightSchedule');
			requireScript = requireScript.concat(['underscore']);
		}

		// if(body.hasClass('create-new-pin-page')) {
		// 	dashboard.getPage('KFCreateNewPin');
		// }

		if (body.hasClass('registration-page')) {
			dashboard.getPage('KFRegistration');
		}

		if (body.hasClass('sqc-registration-page')) {
			dashboard.getPage('SQCRegistration');
		}

		if (body.hasClass('special-assistance-page')) {
			dashboard.getPage('specialAssistance');
			requireScript = requireScript.concat(['tiff']);
		}

		if (body.hasClass('cancel-waitlist-booking-page')) {
			dashboard.getPage('cancelWaitlistBooking');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('mb-main-page')) {
			dashboard.getPage('ManageBooking');
		}

		if (body.hasClass('excess-baggage-page')) {
			dashboard.getPage('ExcessBaggage');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('mb-confirmation-page') || body.hasClass('mb-change-booking-confirmation-page')) {
			dashboard.getPage('MBConfirmation');
		}
		if (body.hasClass('review-cancellation-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('mb-waitlisted-flight-orc-reserved-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('mb-select-flights-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('review-page') || body.hasClass('review-booking-insuff-page')) {
			dashboard.getPage('MBReview');
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}
		if (body.hasClass('select-meals-page')) {
			dashboard.getPage('MBSelectMeal');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('select-meals-land-page')) {
			dashboard.getPage('MBSelectMealLand');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('expenditure-page')) {
			dashboard.getPage('SQCExpenditure');
		}
		if (body.hasClass('sqc-upcoming-flights-page')) {
			dashboard.getPage('sqcUpcomingFlight');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('sqc-user')) {
			dashboard.getPage('SQCUser');
		}
		if (body.hasClass('sqc-add-user')) {
			dashboard.getPage('SQCAddUser');
		}
		if (body.hasClass('sqc-at-a-glance-page')) {
			dashboard.getPage('SQCAtAGlance');
			requireScript = requireScript.concat(['highcharts', 'chart', 'underscore', 'accounting']);
		}
		if (body.hasClass('change-flight-page')) {
			dashboard.getPage('mbChangeFlight');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if ($('[data-accordion-wrapper]').length) {
			dashboard.getPage('accordion');
		}

		if ($('[data-count-chars-left]').length) {
			dashboard.getPage('countCharsLeft');
			requireScript = requireScript.concat(['accounting']);
		}

		if ($('[data-rule-validatedate]').length) {
			dashboard.getPage('initCorrectDate');
		}
		if (body.hasClass('sqc-bookings-flight-history-page')) {
			dashboard.getPage('SQCFlightHistory');
			requireScript = requireScript.concat(['jqueryui', 'validate', 'underscore']);
		}
		if (body.hasClass('sqc-saved-trips-page')) {
			dashboard.getPage('SQCSavedTrips');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('utilities-page')) {
			dashboard.getPage('desWhereTo');
			// requireScript = requireScript.concat(['isotope']);
			requireScript = requireScript.concat(['shuffle', 'underscore', 'inputmask', 'validate']);
		}
		if (body.hasClass('destination-list-page')) {
			dashboard.getPage('desEntry');
			requireScript = requireScript.concat(['shuffle']);
		}

		if (body.hasClass('city-guide-page')) {
			dashboard.getPage('DESCityGuide');
			requireScript = requireScript.concat(['slick', 'inputmask']);
		}

		if (body.hasClass('static-content-music')) {
			dashboard.getPage('staticContentMusic');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('des-krisFlyer-1-page')) {
			dashboard.getPage('staticContentKrisflyer');
			requireScript = requireScript.concat(['slick']);
		}

		if (body.hasClass('ssh-selection-page')) {
			dashboard.getPage('sshSelection');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if (body.hasClass('hotel-page')) {
			dashboard.getPage('sshHotel');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if (body.hasClass('ssh-additional-page')) {
			dashboard.getPage('sshAdditional');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('static-content-components')) {
			dashboard.getPage('staticContentComponents');
		}

		if ($('[data-multi-tab]').length) {
			dashboard.getPage('multiTabsWithLongText');
		}

		if (body.hasClass('static-content-meals-page')) {
			dashboard.getPage('staticContentMeals');
		}

		if (body.hasClass('static-content-spec-meal-page')) {
			dashboard.getPage('staticContentSpecMeals');
		}

		if (body.hasClass('static-content-generic-page')) {
			dashboard.getPage('staticContentGeneric');
			requireScript = requireScript.concat(['slick']);
		}

		if (body.hasClass('static-content-heritage')) {
			dashboard.getPage('staticContentHeritage');
		}

		if (body.hasClass('contact-us-page')) {
			dashboard.getPage('contactUs');
		}

		if ($('[data-disable-value]').length) {
			dashboard.getPage('disableValue');
		}

		if ($('[data-toggle-privacy]').length) {
			dashboard.getPage('togglePrivacy');
		}

		if (body.hasClass('feedback-concern-baggage-delayed-page')) {
			dashboard.getPage('feedBack');
			requireScript = requireScript.concat(['accounting', 'tiff', 'recaptcha']);
		}

		if (body.hasClass('static-bestfare')) {
			dashboard.getPage('bestFare');
			requireScript = requireScript.concat(['accounting', 'inputmask', 'tiff', 'recaptcha']);
		}

		if ($('[data-tablet-slider]').length) {
			dashboard.getPage('moreInTheSection');
			if (requireScript.indexOf('slick') === -1) {
				requireScript = requireScript.concat(['slick']);
			}
		}
		if ($('[data-youtube-url]').length) {
			dashboard.getPage('youtubeApp');
			if (!SIA.global.vars.isIETouch()) {
				requireScript = requireScript.concat(['youtubeAPI']);
			}
		}
		if ($('[data-flow-url]').length) {
			dashboard.getPage('flowPlayerApp');
			requireScript = requireScript.concat(['flowplayerAPI']);
		}
		if ($('[data-block-scroll]').length) {
			dashboard.getPage('blockScroll');
		}
		if ($('[data-entertaiment]').length) {
			dashboard.getPage('filterEntertainment');
		}
		if ($('[data-radio-tab]').length) {
			dashboard.getPage('tab');
		}
		if (body.hasClass('landing-search-flights-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}
		if (body.hasClass('landing-fl-status-page')) {
			dashboard.getPage('landingSearchFlight');
		}
		if (body.hasClass('landing-manage-booking-page')) {
			dashboard.getPage('landingSearchFlight');
		}
		if (body.hasClass('landing-checkin-page')) {
			dashboard.getPage('landingSearchFlight');
		}
		if (body.hasClass('landing-flight-schedules-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}
		if (body.hasClass('unsubscribe-page')) {
			dashboard.getPage('kfUnsubscribe');
		}
		// if($('[data-check-change]').length){
		// dashboard.getPage('FormCheckChange');
		// }
		if ($('[data-chat-now]').length) {
			dashboard.getPage('chatWidget');
		}

		if ($('[data-open-accordion]').length) {
			dashboard.getPage('faqs');
		}

		if ($('[data-plus-or-minus-number]').length) {
			dashboard.getPage('plusOrMinusNumber');
			requireScript = requireScript.concat(['inputmask']);
		}

		if ($('[data-radio-tabs-change]').length) {
			dashboard.getPage('radioTabsChange');
		}

		if (body.hasClass('add-baggage-page')) {
			dashboard.getPage('addBaggage');
		}

		if ($('[data-component-country-city]').length) {
			dashboard.getPage('autocompleteCountryCity');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('enews-subscribe-page')) {
			dashboard.getPage('enewsSubscribe');
		}

		if (body.hasClass('promotions-packages-page')) {
			dashboard.getPage('promotionsPackages');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('hotel-listing-page')) {
			dashboard.getPage('hotelListing');
			requireScript = requireScript.concat(['shuffle']);
		}

		if (body.hasClass('booking-mile-claim-page')) {
			dashboard.getPage('bookingMileClaim');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}

		if ($('[data-promotion-component]').length) {
			dashboard.getPage('promotionComponent');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-booking-widget]').length) {
			dashboard.getPage('bookingWidget');
			requireScript = requireScript.concat(['inputmask']);
		}

		if ($('[data-sort-content]').length) {
			dashboard.getPage('sortTableContent');
		}

		if (body.hasClass('donate-miles-page')) {
			dashboard.getPage('donateMiles');
		}

		if (totalLoadedModules === 0) {
			concatInitFunctionDone = true;
		}

		var countLoadScript = 0;
		var loadSIAScript = function() {
			var loadFunctionDuration = 0;
			var count = 0;
			var loadFunction = function(c) {
				setTimeout(function() {
					if (initFunction[c]) {
						initFunction[c]();
						count++;
						if (count !== initFunction.length) {
							loadFunction(count);
						} else {
							var unloadTimer = window.noJsonHandler ? 700 : 0;
							setTimeout(function() {
								SIA.preloader.hide();
							}, unloadTimer);
						}
					} else {
						SIA.preloader.hide();
					}
				}, loadFunctionDuration);
			};
			var waitInitFunction = setInterval(function() {
				if (concatInitFunctionDone) {
					loadFunction(count);
					clearInterval(waitInitFunction);
				}
			}, 100);
			if (typeof triggerModal !== 'undefined') {
				var setCookie = function(cname, cvalue, exdays) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
					var expires = 'expires=' + d.toGMTString();
					document.cookie = cname + '=' + cvalue + ';' + expires;
				};

				var getCookie = function(cname) {
					var name = cname + '=';
					var ca = document.cookie.split(';');
					for (var i = 0; i < ca.length; i++) {
						var c = ca[i].trim();
						if (c.indexOf(name) === 0) {
							return c.substring(name.length, c.length);
						}
					}
					return '';
				};


				var initPopupTriggerModal = function(triggerModal, introLightboxEl) {
					// var trM = $(triggerModal);
					var trM = $();

					if(typeof introLightboxEl !== 'undefined'){
						trM = introLightboxEl;
					}else{
						trM = $(triggerModal);
					}

					var openLogin = trM.find('[data-open-popup-login]');
					var isOpenLogin = false;
					trM.Popup({
						overlayBGTemplate: SIA.global.config.template.overlay,
						modalShowClass: '',
						triggerCloseModal: '.popup__close, [data-close]',
						afterHide: function(popup) {
							if ($(popup).find('[data-remember-cookie] input').is(':checked')) {
								setCookie(triggerModal, true, 7);
							}
							if(openLogin.length && isOpenLogin){
								$(openLogin.data('open-popup-login')).Popup('show');
								isOpenLogin = false;
							}
						}
					});

					if(openLogin.length){
						openLogin.off('click.openLogin').on('click.openLogin', function(){
							isOpenLogin = true;
							trM.Popup('hide');
						});
					}
					if (!getCookie(triggerModal)) {
						trM.Popup('show');
					}
				};

				if (/\//.test(triggerModal)) {
					var getLbTemplate = function() {
						return $.ajax({
							url: triggerModal
						});
					};
					getLbTemplate().done(function(data) {
						var introLightboxEl = $(data);
						var totalImg = introLightboxEl.find('img');
						var countImage = 0;
						body.append(introLightboxEl);
						if (totalImg.length) {
							totalImg.each(function() {
								var that = $(this);
								var img = new Image();
								img.onload = function() {
									countImage++;
									if (countImage === totalImg.length) {
										// initPopupTriggerModal(introLightboxEl);
										initPopupTriggerModal(triggerModal, introLightboxEl);
									}
								};
								img.src = that.attr('src');
							});
						} else {
							// initPopupTriggerModal(introLightboxEl);
							initPopupTriggerModal(triggerModal, introLightboxEl);
						}
					});
				} else {
					initPopupTriggerModal(triggerModal);
				}
			}
		};
		var excuteScript = function(url) {
			loadScript(url, function() {
				countLoadScript++;
				if (countLoadScript === requireScript.length) {
					loadSIAScript();
				}
			});
		};

		var initLoadPage = function() {
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			var paramUrl = sURLVariables[1];
			var getURLParams = function(sParam) {
				for (var i = 0; i < sURLVariables.length; i++) {
					var sParameterName = sURLVariables[i].split('=');
					if (sParameterName[0] === sParam) {
						return sParameterName[1];
					}
				}
			};
			var run = function() {
				if (requireScript.length) {
					for (var i = 0; i < requireScript.length; i++) {
						excuteScript(urlScript.getScript(requireScript[i]));
					}
				} else {
					loadSIAScript();
				}
			};

			if (getURLParams('debug') && $.parseJSON(getURLParams('debug'))) {
				var sName = paramUrl.split('=');
				$.get('./ajax/JSONS/' + sName[1], function(res) {
					globalJson[sName[0]] = res;
					run();
				});
			} else {
				run();
			}
		};

		initLoadPage();


		var preloadImages = function() {
			if (typeof imageSrcs !== 'undefined' && imageSrcs.desktop.length) {
				$.each(imageSrcs.desktop, function(i, src){
					var img = new Image();
					img.src = src;
				});
			}
		};

		preloadImages();

	});

		$(window).off('resize.updatePosLoading').on('resize.updatePosLoading', function(){
			SIA.planLoading.rePosition();
		});

})(jQuery);

SIA.footer = function() {
	var footer = $('footer'),
		config = SIA.global.config,
		win = $(window),
		winW = win.width(),
		timerResize;

	var calcHeightFooter = function(currentW) {
		footer.find('.footer-block').height('auto');
		if (currentW >= config.mobile) {
			if (currentW < config.tablet) {
				footer.find('.footer-block').each(function(index) {
					if (index % 2) {
						var firstEl = $('.footer-block:nth-child(' + index + ')'),
							secondEl = $('.footer-block:nth-child(' + (index + 1) + ')'),
							maxHeight = Math.max(firstEl.height(), secondEl.height());
						firstEl.add(secondEl).height(maxHeight);
					}
				});
			}
		}
	};

	var onResize = function() {
		var currentW = win.width();
		if (winW === currentW) {
			return;
		}
		calcHeightFooter(currentW);
		winW = currentW;
	};

	win.off('resize.calcHeightFooter').on('resize.calcHeightFooter', function() {
		clearTimeout(timerResize);
		timerResize = setTimeout(function() {
			onResize();
		}, 150);
	});

	calcHeightFooter(winW);
};

/**
 *  @name accordion
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'accordion';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}


	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			that.isOpenAll = false;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);

			that.wrapper.each(function(){
				var self = $(this);
				var triggerAccordion = self.find(that.options.triggerAccordion);
				var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');
				self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;

				triggerAccordion.each(function(idx){
					var trigger = $(this);
					var preventDefault = false;

					if(trigger.hasClass('active')){
						trigger.attr('aria-expanded', true);
					}else{
						trigger.attr('aria-expanded', false);
					}


					if(supportCss){
						trigger.data('height', contentAccordion.eq(idx).height());
					}
					if(!trigger.hasClass(that.options.activeClass)){
						if(supportCss){
							contentAccordion.eq(idx).css('height', 0);
						}else{
							contentAccordion.eq(idx).hide();
						}
					}
					trigger.off('click.accordion').on('click.accordion', function(e) {
						var expandedState = trigger.attr('aria-expanded') === 'false' ? true : false;

						trigger.attr('aria-expanded', expandedState);
						if($(e.target).is('label') || $(e.target).is(':checkbox')){
							return;
						}
						e.preventDefault();
						if(trigger.closest('.disable').length || preventDefault){
							return;
						}
						if(that.isOpenAll){
							that.isOpenAll = false;
							triggerAccordion.not(trigger).trigger('click.accordion');
							trigger.trigger('stayAcc');
							return;
						}
						preventDefault = true;

						if($.isFunction(that.options.beforeAccordion)){
							that.options.beforeAccordion.call(self, trigger, contentAccordion.eq(idx));
						}
						trigger.trigger('beforeAccordion');

						if(trigger.hasClass(that.options.activeClass)){
							if(supportCss){
								contentAccordion.eq(idx).css('height', 0);
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideUp(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.removeClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
							self.activeAccordion = $();
						}
						else{

							// if(self.activeAccordion.length || ){
							// 	self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							// 	self.activeAccordion.trigger('click.accordion');
							// 	self.activeAccordion = $();
							// }

							var curActiveAccor = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							if (curActiveAccor && curActiveAccor.length) {
								curActiveAccor.trigger('click.accordion');
							}

							self.activeAccordion = trigger;


							if(supportCss){
								contentAccordion.eq(idx).css('height', trigger.data('height'));
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideDown(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.addClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
						}
					});
				});

			});
		},
		refresh: function(){
			this.init();
		},
		openAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(!that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', triggerAccordion.data('height'));
					}else{
						contentAccordion.slideDown(that.options.duration);
					}
					triggerAccordion.addClass(that.options.activeClass);
					that.trigger('openAll');
					that.isOpenAll = true;
				});
			}
		},
		closeAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', 0);
					}else{
						contentAccordion.slideUp(that.options.duration);
					}
					triggerAccordion.removeClass(that.options.activeClass);
					that.trigger('closeAll');
					that.isOpenAll = false;
				});
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		wrapper: '',
		triggerAccordion: '',
		contentAccordion: '',
		activeClass: 'active',
		duration: 400,
		css3: false,
		afterAccordion: function() {},
		beforeAccordion: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global ally functions
 * @version 1.0
 */
SIA.initAlly = function(){
  /*! ally.js - v1.3.0 - http://allyjs.io/ - MIT License */
  !function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.ally=e()}}(function(){var e;return function t(e,n,r){function i(a,u){if(!n[a]){if(!e[a]){var s="function"==typeof require&&require;if(!u&&s)return s(a,!0);if(o)return o(a,!0);var l=new Error("Cannot find module '"+a+"'");throw l.code="MODULE_NOT_FOUND",l}var c=n[a]={exports:{}};e[a][0].call(c.exports,function(t){var n=e[a][1][t];return i(n?n:t)},c,c.exports,t,e,n,r)}return n[a].exports}for(var o="function"==typeof require&&require,a=0;a<r.length;a++)i(r[a]);return i}({1:[function(e,t){"use strict";function n(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function r(e){return e&&"object"==typeof e&&"default"in e?e["default"]:e}function i(e){if(!e)return[];if(Array.isArray(e))return e;if(void 0!==e.nodeType)return[e];if("string"==typeof e&&(e=document.querySelectorAll(e)),void 0!==e.length)return[].slice.call(e,0);throw new TypeError("unexpected input "+String(e))}function o(e){var t=e.context,n=e.label,r=void 0===n?"context-to-element":n,o=e.resolveDocument,a=e.defaultToDocument,u=i(t)[0];if(o&&u&&u.nodeType===Node.DOCUMENT_NODE&&(u=u.documentElement),!u&&a)return document.documentElement;if(!u)throw new TypeError(r+" requires valid options.context");if(u.nodeType!==Node.ELEMENT_NODE&&u.nodeType!==Node.DOCUMENT_FRAGMENT_NODE)throw new TypeError(r+" requires options.context to be an Element");return u}function a(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=o({label:"get/shadow-host",context:t}),r=null;n;)r=n,n=n.parentNode;return r.nodeType===r.DOCUMENT_FRAGMENT_NODE&&r.host?r.host:null}function u(e){return e?e.nodeType===Node.DOCUMENT_NODE?e:e.ownerDocument||document:document}function s(e){var t=o({label:"is/active-element",resolveDocument:!0,context:e}),n=u(t);if(n.activeElement===t)return!0;var r=a({context:t});return r&&r.shadowRoot.activeElement===t?!0:!1}function l(e){var t=u(e);return t.defaultView||window}function c(e){var t=o({label:"element/blur",context:e});if(!s(t))return null;var n=t.nodeName.toLowerCase();if("body"===n)return null;if(t.blur)return t.blur(),document.activeElement;var r=l(t);try{return r.HTMLElement.prototype.blur.call(t),document.activeElement}catch(i){return null}}function d(){var e={activeElement:document.activeElement,windowScrollTop:window.scrollTop,windowScrollLeft:window.scrollLeft,bodyScrollTop:document.body.scrollTop,bodyScrollLeft:document.body.scrollLeft},t=document.createElement("iframe");t.setAttribute("style","position:absolute; position:fixed; top:0; left:-2px; width:1px; height:1px; overflow:hidden;"),t.setAttribute("aria-live","off"),t.setAttribute("aria-busy","true"),t.setAttribute("aria-hidden","true"),document.body.appendChild(t);var n=t.contentWindow,r=n.document;r.open(),r.close();var i=r.createElement("div");return r.body.appendChild(i),e.iframe=t,e.wrapper=i,e.window=n,e.document=r,e}function f(e,t){e.wrapper.innerHTML="";var n="string"==typeof t.element?e.document.createElement(t.element):t.element(e.wrapper,e.document),r=t.mutate&&t.mutate(n,e.wrapper,e.document);return r||r===!1||(r=n),!n.parentNode&&e.wrapper.appendChild(n),r&&r.focus&&r.focus(),t.validate?t.validate(n,e.document):e.document.activeElement===r}function m(e){e.activeElement===document.body?(document.activeElement&&document.activeElement.blur&&document.activeElement.blur(),ln.is.IE10&&document.body.focus()):e.activeElement&&e.activeElement.focus&&e.activeElement.focus(),document.body.removeChild(e.iframe),window.scrollTop=e.windowScrollTop,window.scrollLeft=e.windowScrollLeft,document.body.scrollTop=e.bodyScrollTop,document.body.scrollLeft=e.bodyScrollLeft}function b(e){var t=d(),n={};return Object.keys(e).map(function(r){n[r]=f(t,e[r])}),m(t),n}function v(e){var t=void 0;try{t=window.localStorage&&window.localStorage.getItem(e),t=t?JSON.parse(t):{}}catch(n){t={}}return t}function h(e,t){if(document.hasFocus())try{window.localStorage&&window.localStorage.setItem(e,JSON.stringify(t))}catch(n){}else try{window.localStorage&&window.localStorage.removeItem(e)}catch(n){}}function g(){var e=void 0;try{document.querySelector("html >>> :first-child"),e=">>>"}catch(t){try{document.querySelector("html /deep/ :first-child"),e="/deep/"}catch(n){e=""}}return e}function p(){return jn}function x(){return $n}function y(){return ar}function w(){var e=b(sr);return Object.keys(ur).forEach(function(t){e[t]=ur[t]()}),e}function E(){return lr?lr:(lr=An.get(),lr.time||(An.set(w()),lr=An.get()),lr)}function S(e){cr||(cr=E());var t=cr.focusTabindexTrailingCharacters?fr:dr,n=o({label:"is/valid-tabindex",resolveDocument:!0,context:e});if(!n.hasAttribute("tabindex"))return!1;if(void 0===n.tabIndex)return!1;if(cr.focusInvalidTabindex)return!0;var r=n.getAttribute("tabindex");return"-32768"===r?!1:Boolean(r&&t.test(r))}function T(e){if(!S(e))return null;var t=parseInt(e.getAttribute("tabindex"),10);return isNaN(t)?-1:t}function A(e){mr||(mr=E(),mr.focusFieldsetDisabled&&delete vr.fieldset,mr.focusFormDisabled&&delete vr.form,br=new RegExp("^("+Object.keys(vr).join("|")+")$"));var t=o({label:"is/native-disabled-supported",context:e}),n=t.nodeName.toLowerCase();return Boolean(br.test(n))}function O(e){var t=e.element,n=e.attribute,r="data-cached-"+n,i=t.getAttribute(r);if(null===i){var o=t.getAttribute(n);if(null===o)return;t.setAttribute(r,o||""),t.removeAttribute(n)}else{var o=t.getAttribute(r);t.removeAttribute(r),t.setAttribute(n,o)}}function I(e){var t=e.element,n=e.attribute,r=e.temporaryValue,i=e.saveValue,o="data-cached-"+n;if(void 0!==r){var a=i||t.getAttribute(n);t.setAttribute(o,a||""),t.setAttribute(n,r)}else{var a=t.getAttribute(o);t.removeAttribute(o),""===a?t.removeAttribute(n):t.setAttribute(n,a)}}function C(){pr.warn("trying to focus inert element",this)}function L(e,t){if(t){var n=T(e);I({element:e,attribute:"tabindex",temporaryValue:"-1",saveValue:null!==n?n:""})}else I({element:e,attribute:"tabindex"})}function N(e,t){O({element:e,attribute:"controls",remove:t})}function k(e,t){I({element:e,attribute:"focusable",temporaryValue:t?"false":void 0})}function M(e,t){O({element:e,attribute:"xlink:href",remove:t})}function _(e,t){I({element:e,attribute:"aria-disabled",temporaryValue:t?"true":void 0})}function P(e,t){t?e.focus=C:delete e.focus}function F(e,t){if(t){var n=e.style.pointerEvents||"";e.setAttribute("data-inert-pointer-events",n),e.style.pointerEvents="none"}else{var n=e.getAttribute("data-inert-pointer-events");e.removeAttribute("data-inert-pointer-events"),e.style.pointerEvents=n}}function R(e,t){_(e,t),L(e,t),P(e,t),F(e,t);var n=e.nodeName.toLowerCase();("video"===n||"audio"===n)&&N(e,t),("svg"===n||e.ownerSVGElement)&&(xr.focusSvgFocusableAttribute?k(e,t):xr.focusSvgTabindexAttribute||"a"!==n||M(e,t)),t?e.setAttribute("data-ally-disabled","true"):e.removeAttribute("data-ally-disabled")}function B(e,t){xr||(xr=E());var n=o({label:"element/disabled",context:e});t=Boolean(t);var r=n.hasAttribute("data-ally-disabled"),i=1===arguments.length;return A(n)?i?n.disabled:(n.disabled=t,n):i?r:r===t?n:(R(n,t),n)}function D(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=[],r=o({label:"get/parents",context:t});r;)n.push(r),r=r.parentNode,r&&r.nodeType!==Node.ELEMENT_NODE&&(r=null);return n}function W(e){yr.some(function(t){return e[t]?(wr=t,!0):!1})}function H(e,t){return wr||W(e),e[wr](t)}function j(e){var t=e.webkitUserModify||"";return Boolean(t&&-1!==t.indexOf("write"))}function q(e){return[e.getPropertyValue("overflow"),e.getPropertyValue("overflow-x"),e.getPropertyValue("overflow-y")].some(function(e){return"auto"===e||"scroll"===e})}function K(e){return e.display.indexOf("flex")>-1}function G(e,t,n,r){return"div"!==t&&"span"!==t?!1:n&&"div"!==n&&"span"!==n&&!q(r)?!1:e.offsetHeight<e.scrollHeight||e.offsetWidth<e.scrollWidth}function Z(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{flexbox:!1,scrollable:!1,shadow:!1}:n;Er||(Er=E());var i=o({label:"is/focus-relevant",resolveDocument:!0,context:t});if(!r.shadow&&i.shadowRoot)return!0;var a=i.nodeName.toLowerCase();if("input"===a&&"hidden"===i.type)return!1;if("input"===a||"select"===a||"button"===a||"textarea"===a)return!0;if("legend"===a&&Er.focusRedirectLegend)return!0;if("label"===a)return!0;if("area"===a)return!0;if("a"===a&&i.hasAttribute("href"))return!0;if("object"===a&&i.hasAttribute("usemap"))return!1;if("object"===a){var u=i.getAttribute("type");if(!Er.focusObjectSvg&&"image/svg+xml"===u)return!1;if(!Er.focusObjectSwf&&"application/x-shockwave-flash"===u)return!1}if("iframe"===a||"object"===a)return!0;if("embed"===a||"keygen"===a)return!0;if(i.hasAttribute("contenteditable"))return!0;if("audio"===a&&(Er.focusAudioWithoutControls||i.hasAttribute("controls")))return!0;if("video"===a&&(Er.focusVideoWithoutControls||i.hasAttribute("controls")))return!0;if(Er.focusSummary&&"summary"===a)return!0;var s=S(i);if("img"===a&&i.hasAttribute("usemap"))return s&&Er.focusImgUsemapTabindex||Er.focusRedirectImgUsemap;if(Er.focusTable&&("table"===a||"td"===a))return!0;if(Er.focusFieldset&&"fieldset"===a)return!0;var l=i.getAttribute("focusable");if("svg"===a)return s||Er.focusSvg||Boolean(Er.focusSvgFocusableAttribute&&l&&"true"===l);if(H(i,"svg a")&&i.hasAttribute("xlink:href"))return!0;if(Er.focusSvgFocusableAttribute&&i.ownerSVGElement)return Boolean(l&&"true"===l);if(s)return!0;var c=window.getComputedStyle(i,null);if(j(c))return!0;if(Er.focusImgIsmap&&"img"===a&&i.hasAttribute("ismap")){var d=D({context:i}).some(function(e){return"a"===e.nodeName.toLowerCase()&&e.hasAttribute("href")});if(d)return!0}if(!r.scrollable&&Er.focusScrollContainer)if(Er.focusScrollContainerWithoutOverflow){if(G(i,a))return!0}else if(q(c))return!0;if(!r.flexbox&&Er.focusFlexboxContainer&&K(c))return!0;var f=i.parentElement;if(!r.scrollable&&f){var m=f.nodeName.toLowerCase(),b=window.getComputedStyle(f,null);if(Er.focusScrollBody&&G(f,a,m,b))return!0;if(Er.focusChildrenOfFocusableFlexbox&&K(b))return!0}return!1}function V(e){try{return e.contentDocument||e.contentWindow&&e.contentWindow.document||e.getSVGDocument&&e.getSVGDocument()||null}catch(t){return null}}function $(e){if("string"!=typeof Tr){var t=g();t&&(Tr=", html "+t+" ")}return Tr?e+Tr+e.replace(/\s*,\s*/g,",").split(",").join(Tr):e}function U(e){if(Ar||(Ar=$("object, iframe")),void 0!==e._frameElement)return e._frameElement;e._frameElement=null;var t=e.parent.document.querySelectorAll(Ar);return[].some.call(t,function(t){var n=V(t);return n!==e.document?!1:(e._frameElement=t,!0)}),e._frameElement}function X(e){var t=l(e);if(!t.parent||t.parent===t)return null;try{return t.frameElement||U(t)}catch(n){return null}}function z(e,t){return window.getComputedStyle(e,null).getPropertyValue(t)}function J(e){return e.some(function(e){return"none"===z(e,"display")})}function Q(e){var t=e.findIndex(function(e){var t=z(e,"visibility");return"hidden"===t||"collapse"===t});if(-1===t)return!1;var n=e.findIndex(function(e){return"visible"===z(e,"visibility")});return-1===n?!0:n>t?!0:!1}function Y(e){var t=1;return"summary"===e[0].nodeName.toLowerCase()&&(t=2),e.slice(t).some(function(e){return"details"===e.nodeName.toLowerCase()&&e.open===!1})}function ee(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{notRendered:!1,cssDisplay:!1,cssVisibility:!1,detailsElement:!1,browsingContext:!1}:n,i=o({label:"is/visible",resolveDocument:!0,context:t}),a=i.nodeName.toLowerCase();if(!r.notRendered&&Or.test(a))return!0;var u=D({context:i}),s="audio"===a&&!i.hasAttribute("controls");if(!r.cssDisplay&&J(s?u.slice(1):u))return!1;if(!r.cssVisibility&&Q(u))return!1;if(!r.detailsElement&&Y(u))return!1;if(!r.browsingContext){var l=X(i),c=ee.except(r);if(l&&!c(l))return!1}return!0}function te(e,t){var n=t.querySelector('map[name="'+sn(e)+'"]');return n||null}function ne(e){var t=e.getAttribute("usemap");if(!t)return null;var n=u(e);return te(t.slice(1),n)}function re(e){var t=e.parentElement;if(!t.name||"map"!==t.nodeName.toLowerCase())return null;var n=u(e);return n.querySelector('img[usemap="#'+sn(t.name)+'"]')||null}function ie(e){Cr||(Cr=E());var t=o({label:"is/valid-area",context:e}),n=t.nodeName.toLowerCase();if("area"!==n)return!1;var r=t.hasAttribute("tabindex");if(!Cr.focusAreaTabindex&&r)return!1;var i=re(t);if(!i||!Ir(i))return!1;if(!Cr.focusBrokenImageMap&&(!i.complete||!i.naturalHeight||i.offsetWidth<=0||i.offsetHeight<=0))return!1;if(!Cr.focusAreaWithoutHref&&!t.href)return Cr.focusAreaTabindex&&r||Cr.focusAreaImgTabindex&&i.hasAttribute("tabindex");var a=D({context:i}).slice(1).some(function(e){var t=e.nodeName.toLowerCase();return"button"===t||"a"===t});return a?!1:!0}function oe(e){var t=e.nodeName.toLowerCase();return"fieldset"===t&&e.disabled}function ae(e){var t=e.nodeName.toLowerCase();return"form"===t&&e.disabled}function ue(e){Lr||(Lr=E());var t=o({label:"is/disabled",context:e});if(t.hasAttribute("data-ally-disabled"))return!0;if(!A(t))return!1;if(t.disabled)return!0;var n=D({context:t});return n.some(oe)?!0:!Lr.focusFormDisabled&&n.some(ae)?!0:!1}function se(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{onlyFocusableBrowsingContext:!1,visible:!1}:n,i=o({label:"is/only-tabbable",resolveDocument:!0,context:t});if(!r.visible&&!Ir(i))return!1;if(!r.onlyFocusableBrowsingContext&&(ln.is.GECKO||ln.is.TRIDENT)){var a=X(i);if(a&&T(a)<0)return!1}var u=i.nodeName.toLowerCase(),s=T(i);if("label"===u&&ln.is.GECKO)return null!==s&&s>=0;if("svg"===u&&ln.is.TRIDENT)return"false"!==i.getAttribute("focusable");var c=l(i);if(i instanceof c.SVGElement){if("a"===u&&i.hasAttribute("xlink:href")){if(ln.is.GECKO)return!0;if(ln.is.TRIDENT)return"false"!==i.getAttribute("focusable")}if(ln.is.TRIDENT)return"true"===i.getAttribute("focusable")}return!1}function le(e){var t=e.nodeName.toLowerCase();if("embed"===t||"keygen"===t)return!0;var n=T(e);if(e.shadowRoot&&null===n)return!0;if("label"===t)return!kr.focusLabelTabindex||null===n;if("legend"===t)return null===n;if(kr.focusSvgFocusableAttribute&&(e.ownerSVGElement||"svg"===t)){var r=e.getAttribute("focusable");return r&&"false"===r}return"img"===t&&e.hasAttribute("usemap")?null===n||!kr.focusImgUsemapTabindex:"area"===t?!ie(e):!1}function ce(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{disabled:!1,visible:!1,onlyTabbable:!1}:n;kr||(kr=E());var i=Nr.rules.except({onlyFocusableBrowsingContext:!0,visible:r.visible}),a=o({label:"is/focusable",resolveDocument:!0,context:t}),u=Sr.rules({context:a,except:r});if(!u||le(a))return!1;if(!r.disabled&&ue(a))return!1;if(!r.onlyTabbable&&i(a))return!1;if(!r.visible){var s={context:a,except:{}};if(kr.focusInHiddenIframe&&(s.except.browsingContext=!0),kr.focusObjectSvgHidden){var l=a.nodeName.toLowerCase();"object"===l&&(s.except.cssVisibility=!0)}if(!Ir.rules(s))return!1}var c=X(a);if(c){var d=c.nodeName.toLowerCase();if(!("object"!==d||kr.focusInZeroDimensionObject||c.offsetWidth&&c.offsetHeight))return!1}return!0}function de(e){var t=function(t){return t.shadowRoot?NodeFilter.FILTER_ACCEPT:e(t)?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_SKIP};return t.acceptNode=t,t}function fe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy;t||(t=document.documentElement);for(var o=Mr.rules.except({onlyTabbable:r}),a=u(t),s=a.createTreeWalker(t,NodeFilter.SHOW_ELEMENT,"all"===i?_r:de(o),!1),l=[];s.nextNode();)s.currentNode.shadowRoot?(o(s.currentNode)&&l.push(s.currentNode),l=l.concat(fe({context:s.currentNode.shadowRoot,includeOnlyTabbable:r,strategy:i}))):l.push(s.currentNode);return n&&("all"===i?Sr(t)&&l.unshift(t):o(t)&&l.unshift(t)),l}function me(){return Pr||(Pr=E()),"string"==typeof Fr?Fr:(Fr=""+(Pr.focusTable?"table, td,":"")+(Pr.focusFieldset?"fieldset,":"")+"svg a,a[href],area[href],input, select, textarea, button,iframe, object, embed,keygen,"+(Pr.focusAudioWithoutControls?"audio,":"audio[controls],")+(Pr.focusVideoWithoutControls?"video,":"video[controls],")+(Pr.focusSummary?"summary,":"")+"[tabindex],[contenteditable]",Fr=$(Fr))}function be(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=me(),o=t.querySelectorAll(i),a=Mr.rules.except({onlyTabbable:r}),u=[].filter.call(o,a);return n&&a(t)&&u.unshift(t),u}function ve(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy,a=void 0===i?"quick":i,u=o({label:"query/focusable",resolveDocument:!0,defaultToDocument:!0,context:t}),s={context:u,includeContext:n,includeOnlyTabbable:r,strategy:a};if("quick"===a)return be(s);if("strict"===a||"all"===a)return fe(s);throw new TypeError('query/focusable requires option.strategy to be one of ["quick", "strict", "all"]')}function he(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{flexbox:!1,scrollable:!1,shadow:!1,visible:!1,onlyTabbable:!1}:n,i=o({label:"is/tabbable",resolveDocument:!0,context:t});if(ln.is.BLINK&&ln.is.ANDROID&&ln.majorVersion>42)return!1;var a=X(i);if(a){if(ln.is.WEBKIT&&ln.is.IOS&&ln.majorVersion<10)return!1;if(T(a)<0)return!1;if(!r.visible&&(ln.is.BLINK||ln.is.WEBKIT)&&!Ir(a))return!1;var u=a.nodeName.toLowerCase();if("object"===u&&(ln.is.BLINK||ln.is.WEBKIT))return!1}var s=i.nodeName.toLowerCase(),l=T(i),c=null===l?null:l>=0,d=c!==!1,f=null!==l&&l>=0;if(i.hasAttribute("contenteditable"))return d;if(Rr.test(s)&&c!==!0)return!1;if(void 0===i.tabIndex)return Boolean(r.onlyTabbable);if("audio"===s){if(!i.hasAttribute("controls"))return!1;if(ln.is.BLINK)return!0}if("video"===s)if(i.hasAttribute("controls")){if(ln.is.BLINK||ln.is.GECKO)return!0}else if(ln.is.TRIDENT)return!1;if("object"===s&&(ln.is.BLINK||ln.is.WEBKIT))return!1;if("iframe"===s)return!1;if(ln.is.WEBKIT&&ln.is.IOS){var m="input"===s&&"text"===i.type||"password"===i.type||"select"===s||"textarea"===s||i.hasAttribute("contenteditable");if(!m){var b=window.getComputedStyle(i,null);m=j(b)}if(!m)return!1}if(!r.scrollable&&ln.is.GECKO){var v=window.getComputedStyle(i,null);if(q(v))return d}if(ln.is.TRIDENT){if("area"===s){var h=re(i);if(h&&T(h)<0)return!1}var g=window.getComputedStyle(i,null);if(j(g))return i.tabIndex>=0;if(!r.flexbox&&K(g))return null!==l?f:Br(i)&&Dr(i);if(G(i,s))return!1;var p=i.parentElement;if(p){var x=p.nodeName.toLowerCase(),y=window.getComputedStyle(p,null);if(G(p,s,x,y))return!1;if(K(y))return f}}return i.tabIndex>=0}function ge(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy,o=Wr.rules.except({onlyTabbable:r});return ve({context:t,includeContext:n,includeOnlyTabbable:r,strategy:i}).filter(o)}function pe(e,t){return e.compareDocumentPosition(t)&Node.DOCUMENT_POSITION_FOLLOWING?-1:1}function xe(e){return e.sort(pe)}function ye(e,t){return e.findIndex(function(e){return t.compareDocumentPosition(e)&Node.DOCUMENT_POSITION_FOLLOWING})}function we(e,t,n){var r=[];return t.forEach(function(t){var o=!0,a=e.indexOf(t);-1===a&&(a=ye(e,t),o=!1),-1===a&&(a=e.length);var u=i(n?n(t):t);u.length&&r.push({offset:a,replace:o,elements:u})}),r}function Ee(e,t){var n=0;t.sort(function(e,t){return e.offset-t.offset}),t.forEach(function(t){var r=t.replace?1:0,i=[t.offset+n,r].concat(t.elements);e.splice.apply(e,i),n+=t.elements.length-r})}function Se(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.list,n=e.elements,r=e.resolveElement,o=t.slice(0),a=i(n).slice(0);xe(a);var u=we(o,a,r);return Ee(o,u),o}function Te(e){var t=e.nodeName.toLowerCase();return"input"===t||"textarea"===t||"select"===t||"button"===t}function Ae(e,t){var n=e.getAttribute("for");return n?t.getElementById(n):e.querySelector("input, select, textarea")}function Oe(e){var t=e.parentNode,n=ve({context:t,strategy:"strict"});return n.filter(Te)[0]||null}function Ie(e,t){var n=ge({context:t.body,strategy:"strict"});if(!n.length)return null;var r=Se({list:n,elements:[e]}),i=r.indexOf(e);return i===r.length-1?null:r[i+1]}function Ce(e,t){if(!Hr.focusRedirectLegend)return null;var n=e.parentNode;return"fieldset"!==n.nodeName.toLowerCase()?null:"tabbable"===Hr.focusRedirectLegend?Ie(e,t):Oe(e,t)}function Le(e){if(!Hr.focusRedirectImgUsemap)return null;var t=ne(e);return t&&t.querySelector("area")||null}function Ne(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.skipFocusable;Hr||(Hr=E());var r=o({label:"get/focus-redirect-target",context:t});if(!n&&Mr(r))return null;var i=r.nodeName.toLowerCase(),a=u(r);return"label"===i?Ae(r,a):"legend"===i?Ce(r,a):"img"===i?Le(r,a):null}function ke(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=o({label:"get/focus-target",context:t}),i=null,a=function(e){var t=Mr.rules({context:e,except:n});return t?(i=e,!0):(i=Ne({context:e,skipFocusable:!0}),Boolean(i))};return a(r)?i:(D({context:r}).slice(1).some(a),i)}function Me(e){var t=D({context:e}),n=t.slice(1).map(function(e){return{element:e,scrollTop:e.scrollTop,scrollLeft:e.scrollLeft}});return function(){n.forEach(function(e){e.element.scrollTop=e.scrollTop,e.element.scrollLeft=e.scrollLeft})}}function _e(e){if(e.focus)return e.focus(),s(e)?e:null;var t=l(e);try{return t.HTMLElement.prototype.focus.call(e),e}catch(n){return null}}function Pe(e){var t=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],n=t.defaultToAncestor,r=t.undoScrolling,i=o({label:"element/focus",context:e}),a=Mr.rules({context:i,except:jr});if(!n&&!a)return null;var u=ke({context:i,except:jr});if(!u)return null;if(s(u))return u;var l=void 0;r&&(l=Me(u));var c=_e(u);return l&&l(),c}function Fe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.force;t?this.instances=0:this.instances--,this.instances||(this.disengage(),this._result=null)}function Re(){return this.instances?(this.instances++,this._result):(this.instances++,this._result=this.engage()||{},this._result.disengage=Fe.bind(this),this._result)}function Be(){}function De(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.engage,n=e.disengage,r={engage:t||Be,disengage:n||Be,instances:0,_result:null};return Re.bind(r)}function We(){if(document.activeElement){if(document.activeElement!==Zr){var e=new Gr("active-element",{bubbles:!1,cancelable:!1,detail:{focus:document.activeElement,blur:Zr}});document.dispatchEvent(e),Zr=document.activeElement}}else document.body.focus();Vr!==!1&&(Vr=requestAnimationFrame(We))}function He(){Vr=!0,Zr=document.activeElement,We()}function je(){cancelAnimationFrame(Vr),Vr=!1}function qe(e){var t=o({label:"is/shadowed",resolveDocument:!0,context:e});return Boolean(a({context:t}))}function Ke(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=[],r=o({label:"get/shadow-host-parents",context:t});r&&(r=a({context:r}));)n.push(r);return n}function Ge(){for(var e=[document.activeElement];e[0]&&e[0].shadowRoot;)e.unshift(e[0].shadowRoot.activeElement);return e}function Ze(){var e=Ke({context:document.activeElement});return[document.activeElement].concat(e)}function Ve(){return null===document.activeElement&&document.body.focus(),qe(document.activeElement)?Ze():Ge()}function $e(){this.context&&(this.context.forEach(this.disengage),this.context=null,this.engage=null,this.disengage=null)}function Ue(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context;return this.context=i(t||document),this.context.forEach(this.engage),{disengage:$e.bind(this)}}function Xe(){}function ze(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.engage,n=e.disengage,r={engage:t||Xe,disengage:n||Xe,context:null};return Ue.bind(r)}function Je(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=u(t),r=void 0;try{r=n.activeElement}catch(i){}return r&&r.nodeType||(r=n.body||n.documentElement),r}function Qe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.parent,n=e.element,r=e.includeSelf;if(t)return function(e){return Boolean(r&&e===t||t.compareDocumentPosition(e)&Node.DOCUMENT_POSITION_CONTAINED_BY)};if(n)return function(e){return Boolean(r&&n===e||e.compareDocumentPosition(n)&Node.DOCUMENT_POSITION_CONTAINED_BY)};throw new TypeError("util/compare-position#getParentComparator required either options.parent or options.element")}function Ye(e){var t=e.context,n=e.filter,r=function(e){var t=Qe({parent:e});return n.some(t)},i=[],o=function(e){return n.some(function(t){return e===t})?NodeFilter.FILTER_REJECT:r(e)?NodeFilter.FILTER_ACCEPT:(i.push(e),NodeFilter.FILTER_REJECT)};o.acceptNode=o;for(var a=u(t),s=a.createTreeWalker(t,NodeFilter.SHOW_ELEMENT,o,!1);s.nextNode(););return i}function et(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter;if(t=o({label:"get/insignificant-branches",defaultToDocument:!0,context:t}),n=i(n),!n.length)throw new TypeError("get/insignificant-branches requires valid options.filter");return Ye({context:t,filter:n})}function tt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=o({label:"query/shadow-hosts",resolveDocument:!0,defaultToDocument:!0,context:t}),r=u(t),i=r.createTreeWalker(n,NodeFilter.SHOW_ELEMENT,mi,!1),a=[];for(n.shadowRoot&&(a.push(n),a=a.concat(tt({context:n.shadowRoot})));i.nextNode();)a.push(i.currentNode),a=a.concat(tt({context:i.currentNode.shadowRoot}));return a}function nt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.config;if("function"!=typeof n)throw new TypeError("observe/shadow-mutations requires options.callback to be a function");if("object"!=typeof r)throw new TypeError("observe/shadow-mutations requires options.config to be an object");if(!window.MutationObserver)return{disengage:function(){}};var i=o({label:"observe/shadow-mutations",resolveDocument:!0,defaultToDocument:!0,context:t}),a=new vi({context:i,callback:n,config:r});return{disengage:a.disengage}}function rt(e){return B(e,!0)}function it(e){return B(e,!1)}function ot(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter,r=new gi({context:t,filter:n});return{disengage:r.disengage}}function at(e){I({element:e,attribute:"aria-hidden",temporaryValue:"true"})}function ut(e){I({element:e,attribute:"aria-hidden"})}function st(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter,r=new xi({context:t,filter:n});return{disengage:r.disengage}}function lt(e,t){var n=t.querySelectorAll("img[usemap]"),r=new yi(t),i=r.extractAreasFromList(e);return n.length?Se({list:i,elements:n,resolveElement:function(e){var t=e.getAttribute("usemap").slice(1);return r.getAreasFor(t)}}):i}function ct(e,t,n){var r=new wi(t,n),i=r.extractElements(e);return i.length===e.length?n(e):r.sort(i)}function dt(e){var t={},n=[],r=e.filter(function(e){return e.tabIndex<=0||void 0===e.tabIndex?!0:(t[e.tabIndex]||(t[e.tabIndex]=[],n.push(e.tabIndex)),t[e.tabIndex].push(e),!1)}),i=n.sort().map(function(e){return t[e]}).reduceRight(function(e,t){return t.concat(e)},r);return i}function ft(e,t){var n=e.indexOf(t);if(n>0){var r=e.splice(n,1);return r.concat(e)}return e}function mt(e,t){return Ei.tabsequenceAreaAtImgPosition&&(e=lt(e,t)),e=dt(e)}function bt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,o=e.strategy;Ei||(Ei=E());var a=i(t)[0]||document.documentElement,u=ge({context:a,includeContext:n,includeOnlyTabbable:r,strategy:o});return u=document.body.createShadowRoot&&ln.is.BLINK?ct(u,a,mt):mt(u,a),n&&(u=ft(u,a)),u}function vt(e){var t=e?null:!1;return{altKey:t,ctrlKey:t,metaKey:t,shiftKey:t}}function ht(e){var t=-1!==e.indexOf("*"),n=vt(t);return e.forEach(function(e){if("*"!==e){var t=!0,r=e.slice(0,1);"?"===r?t=null:"!"===r&&(t=!1),t!==!0&&(e=e.slice(1));var i=Ci[e];if(!i)throw new TypeError('Unknown modifier "'+e+'"');n[i]=t}}),n}function gt(e){var t=Si[e]||parseInt(e,10);if(!t||"number"!=typeof t||isNaN(t))throw new TypeError('Unknown key "'+e+'"');return[t].concat(Si._alias[t]||[])}function pt(e,t){return!Li.some(function(n){return"boolean"==typeof e[n]&&Boolean(t[n])!==e[n]})}function xt(e){return e.split(/\s+/).map(function(e){var t=e.split("+"),n=ht(t.slice(0,-1)),r=gt(t.slice(-1));return{keyCodes:r,modifiers:n,matchModifiers:pt.bind(null,n)}})}function yt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t={},n=i(e.context)[0]||document.documentElement;delete e.context;var r=i(e.filter);delete e.filter;var o=Object.keys(e);if(!o.length)throw new TypeError("when/key requires at least one option key");var a=function(e){e.keyCodes.forEach(function(n){t[n]||(t[n]=[]),t[n].push(e)})};o.forEach(function(t){if("function"!=typeof e[t])throw new TypeError('when/key requires option["'+t+'"] to be a function');var n=function(n){return n.callback=e[t],n};xt(t).map(n).forEach(a)});var u=function(e){if(!e.defaultPrevented){if(r.length){var i=Qe({element:e.target,includeSelf:!0});if(r.some(i))return}var o=e.keyCode||e.which;t[o]&&t[o].forEach(function(t){t.matchModifiers(e)&&t.callback.call(n,e,s)})}};n.addEventListener("keydown",u,!1);var s=function(){n.removeEventListener("keydown",u,!1)};return{disengage:s}}function wt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context;return t||(t=document.documentElement),bt(),yt({"?alt+?shift+tab":function(e){e.preventDefault();var n=bt({context:t}),r=e.shiftKey,i=n[0],o=n[n.length-1],a=r?i:o,u=r?o:i;if(s(a))return void u.focus();var l=void 0,c=n.some(function(e,t){return s(e)?(l=t,!0):!1});if(!c)return void i.focus();var d=r?-1:1;n[l+d].focus()}})}function Et(){_i=0,Pi=0}function St(e){e.isPrimary!==!1&&_i++}function Tt(e){return e.isPrimary!==!1?e.touches?void(_i=e.touches.length):void(window.setImmediate||window.setTimeout)(function(){_i=Math.max(_i-1,0)}):void 0}function At(e){switch(e.keyCode||e.which){case 16:case 17:case 18:case 91:case 93:return}Pi++}function Ot(e){switch(e.keyCode||e.which){case 16:case 17:case 18:case 91:case 93:return}(window.setImmediate||window.setTimeout)(function(){Pi=Math.max(Pi-1,0)})}function It(){return{pointer:Boolean(_i),key:Boolean(Pi)}}function Ct(){_i=Pi=0,window.removeEventListener("blur",Et,!1),document.documentElement.removeEventListener("keydown",At,!0),document.documentElement.removeEventListener("keyup",Ot,!0),Fi.forEach(function(e){document.documentElement.removeEventListener(e,St,!0)}),Ri.forEach(function(e){document.documentElement.removeEventListener(e,Tt,!0)})}function Lt(){return window.addEventListener("blur",Et,!1),document.documentElement.addEventListener("keydown",At,!0),document.documentElement.addEventListener("keyup",Ot,!0),Fi.forEach(function(e){document.documentElement.addEventListener(e,St,!0)}),Ri.forEach(function(e){document.documentElement.addEventListener(e,Tt,!0)}),{get:It}}function Nt(e){return e.hasAttribute("autofocus")}function kt(e){return e.tabIndex<=0}function Mt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.sequence,r=e.strategy,o=e.ignoreAutofocus,a=e.defaultToContext,u=e.includeOnlyTabbable,s=-1;n||(t=i(t||document.body)[0],n=ge({context:t,includeOnlyTabbable:u,strategy:r})),n.length&&!o&&(s=n.findIndex(Nt)),n.length&&-1===s&&(s=n.findIndex(kt));var l=Mr.rules.except({onlyTabbable:u});return-1===s&&a&&t&&l(t)?t:n[s]||null}function _t(e){var t=e.getAttribute&&e.getAttribute("class")||"";return""===t?[]:t.split(" ")}function Pt(e,t,n){var r=_t(e),i=r.indexOf(t),o=-1!==i,a=void 0!==n?n:!o;

  a!==o&&(a||r.splice(i,1),a&&r.push(t),e.setAttribute("class",r.join(" ")))}function Ft(e,t){return Pt(e,t,!1)}function Rt(e,t){return Pt(e,t,!0)}function Bt(e){var t="";if(e.type===Hi||"shadow-focus"===e.type){var n=qi.get();t=Zi||n.pointer&&"pointer"||n.key&&"key"||"script"}else"initial"===e.type&&(t="initial");document.documentElement.setAttribute("data-focus-source",t),e.type!==ji&&(Vi[t]||Rt(document.documentElement,"focus-source-"+t),Vi[t]=!0,Gi=t)}function Dt(){return Gi}function Wt(e){return Vi[e]}function Ht(e){Zi=e}function jt(){Zi=!1}function qt(){Bt({type:ji}),Gi=Zi=null,Object.keys(Vi).forEach(function(e){Ft(document.documentElement,"focus-source-"+e),Vi[e]=!1}),qi.disengage(),Ki&&Ki.disengage(),document.removeEventListener("shadow-focus",Bt,!0),document.documentElement.removeEventListener(Hi,Bt,!0),document.documentElement.removeEventListener(ji,Bt,!0),document.documentElement.removeAttribute("data-focus-source")}function Kt(){return Ki=zr(),document.addEventListener("shadow-focus",Bt,!0),document.documentElement.addEventListener(Hi,Bt,!0),document.documentElement.addEventListener(ji,Bt,!0),qi=Bi(),Bt({type:"initial"}),{used:Wt,current:Dt,lock:Ht,unlock:jt}}function Gt(e){var t=e||Ve();Ui.cssShadowPiercingDeepCombinator||(t=t.slice(-1));var n=[].slice.call(document.querySelectorAll(Qi),0),r=t.map(function(e){return D({context:e})}).reduce(function(e,t){return t.concat(e)},[]);n.forEach(function(e){-1===r.indexOf(e)&&Ft(e,Ji)}),r.forEach(function(e){-1===n.indexOf(e)&&Rt(e,Ji)})}function Zt(){Yi=(window.setImmediate||window.setTimeout)(function(){Gt()})}function Vt(){(window.clearImmediate||window.clearTimeout)(Yi),Gt()}function $t(e){Gt(e.detail.elements)}function Ut(){eo&&eo.disengage(),(window.clearImmediate||window.clearTimeout)(Yi),document.removeEventListener(zi,Zt,!0),document.removeEventListener(Xi,Vt,!0),document.removeEventListener("shadow-focus",$t,!0),[].forEach.call(document.querySelectorAll(Qi),function(e){Ft(e,Ji)})}function Xt(){Ui||(Ui=E(),Qi=$("."+Ji)),eo=zr(),document.addEventListener(zi,Zt,!0),document.addEventListener(Xi,Vt,!0),document.addEventListener("shadow-focus",$t,!0),Gt()}function zt(e,t){var n=Math.max(e.top,t.top),r=Math.max(e.left,t.left),i=Math.max(Math.min(e.right,t.right),r),o=Math.max(Math.min(e.bottom,t.bottom),n);return{top:n,right:i,bottom:o,left:r,width:i-r,height:o-n}}function Jt(){var e=window.innerWidth||document.documentElement.clientWidth,t=window.innerHeight||document.documentElement.clientHeight;return{top:0,right:e,bottom:t,left:0,width:e,height:t}}function Qt(e){var t=e.getBoundingClientRect(),n=e.offsetWidth-e.clientWidth,r=e.offsetHeight-e.clientHeight,i={top:t.top,left:t.left,right:t.right-n,bottom:t.bottom-r,width:t.width-n,height:t.height-r,area:0};return i.area=i.width*i.height,i}function Yt(e){var t=window.getComputedStyle(e,null),n="visible";return t.getPropertyValue("overflow-x")!==n&&t.getPropertyValue("overflow-y")!==n}function en(e){return Yt(e)?e.offsetHeight<e.scrollHeight||e.offsetWidth<e.scrollWidth:!1}function tn(e){var t=D({context:e}).slice(1).filter(en);return t.length?t.reduce(function(e,t){var n=Qt(t),r=zt(n,e);return r.area=Math.min(n.area,e.area),r},Qt(t[0])):null}function nn(e){var t=e.getBoundingClientRect(),n=Jt();n.area=n.width*n.height;var r=n,i=tn(e);if(i){if(!i.width||!i.height)return 0;r=zt(i,n),r.area=i.area}var o=zt(t,r);if(!o.width||!o.height)return 0;var a=t.width*t.height,u=Math.min(a,r.area),s=Math.round(o.width)*Math.round(o.height)/u,l=1e4,c=Math.round(s*l)/l;return Math.min(c,1)}function rn(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.area;if("function"!=typeof n)throw new TypeError("when/visible-area requires options.callback to be a function");"number"!=typeof r&&(r=1);var i=o({label:"when/visible-area",context:t}),a=void 0,u=null,s=function(){a&&cancelAnimationFrame(a)},l=function(){return!Ir(i)||nn(i)<r||n(i)===!1},c=function(){return l()?void u():void s()};return u=function(){a=requestAnimationFrame(c)},u(),{disengage:s}}function on(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.area;if("function"!=typeof n)throw new TypeError("when/focusable requires options.callback to be a function");var i=o({label:"when/focusable",context:t}),a=function(e){return Mr(e)?n(e):!1},s=u(i),l=rn({context:i,callback:a,area:r}),c=function d(){s.removeEventListener("focus",d,!0),l&&l.disengage()};return s.addEventListener("focus",c,!0),{disengage:c}}var an=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}(),un=r(e("platform")),sn=(e("array.prototype.findindex"),r(e("css.escape"))),ln=JSON.parse(JSON.stringify(un)),cn=ln.os.family||"",dn="Android"===cn,fn="Windows"===cn.slice(0,7),mn="OS X"===cn,bn="iOS"===cn,vn="Blink"===ln.layout,hn="Gecko"===ln.layout,gn="Trident"===ln.layout,pn="WebKit"===ln.layout,xn=parseFloat(ln.version),yn=Math.floor(xn);ln.majorVersion=yn,ln.is={ANDROID:dn,WINDOWS:fn,OSX:mn,IOS:bn,BLINK:vn,GECKO:hn,TRIDENT:gn,WEBKIT:pn,IE9:gn&&9===yn,IE10:gn&&10===yn,IE11:gn&&11===yn};var wn="1.3.0",En="undefined"!=typeof window&&window.navigator.userAgent||"",Sn="ally-supports-cache",Tn=v(Sn);(Tn.userAgent!==En||Tn.version!==wn)&&(Tn={}),Tn.userAgent=En,Tn.version=wn;var An={get:function(){return Tn},set:function(e){Object.keys(e).forEach(function(t){Tn[t]=e[t]}),Tn.time=(new Date).toISOString(),h(Sn,Tn)}},On="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",In={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" tabindex="-1" alt="" src="'+On+'">',e.querySelector("area")}},Cn={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area href="#void" tabindex="-1" shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" alt="" src="'+On+'">',!1},validate:function(e,t){if(ln.is.GECKO)return!0;var n=e.querySelector("area");return n.focus(),t.activeElement===n}},Ln={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-area-href-test"><area shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-area-href-test" alt="" src="'+On+'">',e.querySelector("area")},validate:function(e,t){if(ln.is.GECKO)return!0;var n=e.querySelector("area");return t.activeElement===n}},Nn={name:"can-focus-audio-without-controls",element:"audio",mutate:function(e){try{e.setAttribute("src",On)}catch(t){}}},kn="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ",Mn={element:"div",mutate:function(e){return e.innerHTML='<map name="broken-image-map-test"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#broken-image-map-test" alt="" src="'+kn+'">',e.querySelector("area")}},_n={element:"div",mutate:function(e){return e.setAttribute("tabindex","-1"),e.setAttribute("style","display: -webkit-flex; display: -ms-flexbox; display: flex;"),e.innerHTML='<span style="display: block;">hello</span>',e.querySelector("span")}},Pn={element:"fieldset",mutate:function(e){e.setAttribute("tabindex",0),e.setAttribute("disabled","disabled")}},Fn={element:"fieldset",mutate:function(e){e.innerHTML="<legend>legend</legend><p>content</p>"}},Rn={element:"span",mutate:function(e){e.setAttribute("style","display: -webkit-flex; display: -ms-flexbox; display: flex;"),e.innerHTML='<span style="display: block;">hello</span>'}},Bn={element:"form",mutate:function(e){e.setAttribute("tabindex",0),e.setAttribute("disabled","disabled")}},Dn={element:"a",mutate:function(e){return e.href="#void",e.innerHTML='<img ismap src="'+On+'" alt="">',e.querySelector("img")}},Wn={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" tabindex="-1" alt="" src="'+On+'">',e.querySelector("img")}},Hn={element:function(e,t){var n=t.createElement("iframe");e.appendChild(n);var r=n.contentWindow.document;return r.open(),r.close(),n},mutate:function(e){e.style.visibility="hidden";var t=e.contentWindow.document,n=t.createElement("input");return t.body.appendChild(n),n},validate:function(e){var t=e.contentWindow.document,n=t.querySelector("input");return t.activeElement===n}},jn=!ln.is.WEBKIT,qn={element:"div",mutate:function(e){e.setAttribute("tabindex","invalid-value")}},Kn={element:"label",mutate:function(e){e.setAttribute("tabindex","-1")},validate:function(e,t){e.offsetHeight;return e.focus(),t.activeElement===e}},Gn="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBpZD0ic3ZnIj48dGV4dCB4PSIxMCIgeT0iMjAiIGlkPSJzdmctbGluay10ZXh0Ij50ZXh0PC90ZXh0Pjwvc3ZnPg==",Zn={element:"object",mutate:function(e){e.setAttribute("type","image/svg+xml"),e.setAttribute("data",Gn),e.setAttribute("width","200"),e.setAttribute("height","50"),e.style.visibility="hidden"}},Vn={name:"can-focus-object-svg",element:"object",mutate:function(e){e.setAttribute("type","image/svg+xml"),e.setAttribute("data",Gn),e.setAttribute("width","200"),e.setAttribute("height","50")},validate:function(e,t){return ln.is.GECKO?!0:t.activeElement===e}},$n=!ln.is.TRIDENT||!ln.is.IE9,Un={element:"div",mutate:function(e){return e.innerHTML='<map name="focus-redirect-img-usemap"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#focus-redirect-img-usemap" alt="" src="'+On+'">',e.querySelector("img")},validate:function(e,t){var n=e.querySelector("area");return t.activeElement===n}},Xn={element:"fieldset",mutate:function(e){return e.innerHTML='<legend>legend</legend><input tabindex="-1"><input tabindex="0">',!1},validate:function(e,t){var n=e.querySelector('input[tabindex="-1"]'),r=e.querySelector('input[tabindex="0"]');return e.focus(),e.querySelector("legend").focus(),t.activeElement===n&&"focusable"||t.activeElement===r&&"tabbable"||""}},zn={element:"div",mutate:function(e){return e.setAttribute("style","width: 100px; height: 50px; overflow: auto;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>',e.querySelector("div")}},Jn={element:"div",mutate:function(e){e.setAttribute("style","width: 100px; height: 50px;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>'}},Qn={element:"div",mutate:function(e){e.setAttribute("style","width: 100px; height: 50px; overflow: auto;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>'}},Yn={element:"details",mutate:function(e){return e.innerHTML="<summary>foo</summary><p>content</p>",e.firstElementChild}},er={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><text focusable="true">a</text></svg>',e.querySelector("text")},validate:function(e,t){var n=e.querySelector("text");return ln.is.TRIDENT?!0:t.activeElement===n}},tr={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><text tabindex="-1">a</text></svg>',e.querySelector("text")}},nr={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>',e.firstChild},validate:function(e,t){var n=e.firstChild;return ln.is.TRIDENT?!0:t.activeElement===n}},rr={element:"div",mutate:function(e){e.setAttribute("tabindex","3x")}},ir={element:"table",mutate:function(e,t,n){var r=n.createDocumentFragment();r.innerHTML="<tr><td>cell</td></tr>",e.appendChild(r)}},or={element:"video",mutate:function(e){try{e.setAttribute("src",On)}catch(t){}}},ar=ln.is.GECKO||ln.is.TRIDENT,ur={cssShadowPiercingDeepCombinator:g,focusInZeroDimensionObject:p,focusObjectSwf:x,tabsequenceAreaAtImgPosition:y},sr={focusAreaImgTabindex:In,focusAreaTabindex:Cn,focusAreaWithoutHref:Ln,focusAudioWithoutControls:Nn,focusBrokenImageMap:Mn,focusChildrenOfFocusableFlexbox:_n,focusFieldsetDisabled:Pn,focusFieldset:Fn,focusFlexboxContainer:Rn,focusFormDisabled:Bn,focusImgIsmap:Dn,focusImgUsemapTabindex:Wn,focusInHiddenIframe:Hn,focusInvalidTabindex:qn,focusLabelTabindex:Kn,focusObjectSvg:Vn,focusObjectSvgHidden:Zn,focusRedirectImgUsemap:Un,focusRedirectLegend:Xn,focusScrollBody:zn,focusScrollContainerWithoutOverflow:Jn,focusScrollContainer:Qn,focusSummary:Yn,focusSvgFocusableAttribute:er,focusSvgTabindexAttribute:tr,focusSvg:nr,focusTabindexTrailingCharacters:rr,focusTable:ir,focusVideoWithoutControls:or},lr=null,cr=void 0,dr=/^\s*(-|\+)?[0-9]+\s*$/,fr=/^\s*(-|\+)?[0-9]+.*$/,mr=void 0,br=void 0,vr={input:!0,select:!0,textarea:!0,button:!0,fieldset:!0,form:!0},hr=function(){},gr={log:hr,debug:hr,info:hr,warn:hr,error:hr},pr="undefined"!=typeof console?console:gr,xr=void 0,yr=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector"],wr=null,Er=void 0;Z.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return Z({context:t,except:e})};return t.rules=Z,t};var Sr=Z.except({}),Tr=void 0,Ar=void 0,Or=/^(area)$/;ee.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return ee({context:t,except:e})};return t.rules=ee,t};var Ir=ee.except({}),Cr=void 0,Lr=void 0;se.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return se({context:t,except:e})};return t.rules=se,t};var Nr=se.except({}),kr=void 0;ce.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return ce({context:t,except:e})};return t.rules=ce,t};var Mr=ce.except({}),_r=de(Sr),Pr=void 0,Fr=void 0,Rr=/^(fieldset|table|td|body)$/;he.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return he({context:t,except:e})};return t.rules=he,t};var Br=Sr.rules.except({flexbox:!0}),Dr=he.except({flexbox:!0}),Wr=he.except({}),Hr=void 0,jr={flexbox:!0,scrollable:!0,onlyTabbable:!0},qr={blur:c,disabled:B,focus:Pe};"undefined"!=typeof window&&function(){for(var e=0,t=["ms","moz","webkit","o"],n="",r="",i=0,o=t.length;o>i;++i)n=window[t[i]+"RequestAnimationFrame"],r=window[t[i]+"CancelAnimationFrame"]||window[t[i]+"CancelRequestAnimationFrame"];"function"!=typeof window.requestAnimationFrame&&(window.requestAnimationFrame=window[n]||function(t){var n=(new Date).getTime(),r=Math.max(0,16-(n-e)),i=window.setTimeout(function(){t(n+r)},r);return e=n+r,i}),"function"!=typeof window.cancelAnimationFrame&&(window.cancelAnimationFrame=window[r]||function(e){clearTimeout(e)})}();var Kr="undefined"!=typeof window&&window.CustomEvent||function(){};"function"!=typeof Kr&&(Kr=function(e,t){var n=document.createEvent("CustomEvent");return!t&&(t={bubbles:!1,cancelable:!1,detail:void 0}),n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),n},Kr.prototype=window.Event.prototype);var Gr=Kr,Zr=void 0,Vr=void 0,$r=De({engage:He,disengage:je}),Ur=void 0,Xr=void 0;"undefined"!=typeof document&&document.documentElement.createShadowRoot?!function(){var e=void 0,t=void 0,n=function(){i(),(window.clearImmediate||window.clearTimeout)(e),e=(window.setImmediate||window.setTimeout)(function(){o()})},r=function(e){e.addEventListener("blur",n,!0),t=e},i=function(){t&&t.removeEventListener("blur",n,!0),t=null},o=function(){var e=Ve();if(1===e.length)return void i();r(e[0]);var t=new CustomEvent("shadow-focus",{bubbles:!1,cancelable:!1,detail:{elements:e,active:e[0],hosts:e.slice(1)}});document.dispatchEvent(t)},a=function(){(window.clearImmediate||window.clearTimeout)(e),o()};Ur=function(){document.addEventListener("focus",a,!0)},Xr=function(){(window.clearImmediate||window.clearTimeout)(e),t&&t.removeEventListener("blur",n,!0),document.removeEventListener("focus",a,!0)}}():Ur=Xr=function(){};var zr=De({engage:Ur,disengage:Xr}),Jr={activeElement:$r,shadowFocus:zr},Qr=void 0,Yr=void 0,ei=ln.is.TRIDENT&&(ln.is.IE10||ln.is.IE11);ei?!function(){var e=function(e){var t=ke({context:e.target,except:{flexbox:!0,scrollable:!0}});if(t&&t!==e.target){window.setImmediate(function(){t.focus()});var n=[].map.call(t.children,function(e){var t=e.style.visibility||"",n=e.style.transition||"";return e.style.visibility="hidden",e.style.transition="none",[e,t,n]});window.setImmediate(function(){n.forEach(function(e){e[0].style.visibility=e[1],e[0].style.transition=e[2]})})}};Qr=function(t){t.addEventListener("mousedown",e,!0)},Yr=function(t){t.removeEventListener("mousedown",e,!0)}}():Qr=function(){};var ti=ze({engage:Qr,disengage:Yr}),ni=void 0,ri=void 0,ii=ln.is.OSX&&(ln.is.GECKO||ln.is.WEBKIT);ii?!function(){var e=function(e){if(!e.defaultPrevented&&H(e.target,"input, button, button *")){var t=ke({context:e.target});(window.setImmediate||window.setTimeout)(function(){t.focus()})}},t=function(e){if(!e.defaultPrevented&&H(e.target,"label, label *")){var t=ke({context:e.target});t&&t.focus()}};ni=function(n){n.addEventListener("mousedown",e,!1),n.addEventListener("mouseup",t,!1)},ri=function(n){n.removeEventListener("mousedown",e,!1),n.removeEventListener("mouseup",t,!1)}}():ni=function(){};var oi=ze({engage:ni,disengage:ri}),ai=void 0,ui=void 0,si=ln.is.WEBKIT;si?!function(){var e=function(e){var t=ke({context:e.target});!t||t.hasAttribute("tabindex")&&S(t)||(t.setAttribute("tabindex",0),(window.setImmediate||window.setTimeout)(function(){t.removeAttribute("tabindex")},0))};ai=function(t){t.addEventListener("mousedown",e,!0),t.addEventListener("touchstart",e,!0)},ui=function(t){t.removeEventListener("mousedown",e,!0),t.removeEventListener("touchstart",e,!0)}}():ai=function(){};var li=ze({engage:ai,disengage:ui}),ci={pointerFocusChildren:ti,pointerFocusInput:oi,pointerFocusParent:li},di={activeElement:Je,activeElements:Ve,focusRedirectTarget:Ne,focusTarget:ke,insignificantBranches:et,parents:D,shadowHostParents:Ke,shadowHost:a},fi={activeElement:s,disabled:ue,focusRelevant:Sr,focusable:Mr,onlyTabbable:Nr,shadowed:qe,tabbable:Wr,validArea:ie,validTabindex:S,visible:Ir},mi=function(e){return e.shadowRoot?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_SKIP};mi.acceptNode=mi;for(var bi={childList:!0,subtree:!0},vi=function(){function e(){var t=this,r=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],i=r.context,o=r.callback,a=r.config;n(this,e),this.config=a,this.disengage=this.disengage.bind(this),this.clientObserver=new MutationObserver(o),this.hostObserver=new MutationObserver(function(e){return e.forEach(t.handleHostMutation,t)}),this.observeContext(i),this.observeShadowHosts(i)}return an(e,[{key:"disengage",value:function(){this.clientObserver&&this.clientObserver.disconnect(),this.clientObserver=null,this.hostObserver&&this.hostObserver.disconnect(),this.hostObserver=null}},{key:"observeShadowHosts",value:function(e){var t=this,n=tt({context:e});n.forEach(function(e){return t.observeContext(e.shadowRoot)})}},{key:"observeContext",value:function(e){this.clientObserver.observe(e,this.config),this.hostObserver.observe(e,bi)}},{key:"handleHostMutation",value:function(e){if("childList"===e.type){var t=i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE});t.forEach(this.observeShadowHosts,this)}}}]),e}(),hi={attributes:!0,childList:!0,subtree:!0,attributeFilter:["tabindex","disabled","data-ally-disabled"]},gi=function(){function e(){var t=this,r=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],o=r.context,a=r.filter;n(this,e),this._context=i(o||document.documentElement)[0],this._filter=i(a),this._inertElementCache=[],this.disengage=this.disengage.bind(this),this.handleMutation=this.handleMutation.bind(this),this.renderInert=this.renderInert.bind(this),this.filterElements=this.filterElements.bind(this),this.filterParentElements=this.filterParentElements.bind(this);var u=ve({context:this._context,includeContext:!0,strategy:"all"});this.renderInert(u),this.shadowObserver=nt({context:this._context,config:hi,callback:function(e){return e.forEach(t.handleMutation)}})}return an(e,[{key:"disengage",value:function(){this._context&&(it(this._context),this._inertElementCache.forEach(function(e){return it(e)}),this._inertElementCache=null,this._filter=null,this._context=null,this.shadowObserver&&this.shadowObserver.disengage(),this.shadowObserver=null)}},{key:"listQueryFocusable",value:function(e){return e.map(function(e){return ve({context:e,includeContext:!0,strategy:"all"})}).reduce(function(e,t){return e.concat(t)},[])}},{key:"renderInert",value:function(e){var t=this,n=function(e){t._inertElementCache.push(e),rt(e)};e.filter(this.filterElements).filter(this.filterParentElements).filter(function(e){return!B(e)}).forEach(n)}},{key:"filterElements",value:function(e){var t=Qe({element:e,includeSelf:!0});return!this._filter.some(t)}},{key:"filterParentElements",value:function(e){var t=Qe({parent:e});return!this._filter.some(t)}},{key:"handleMutation",value:function(e){if("childList"===e.type){var t=i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE});if(!t.length)return;var n=this.listQueryFocusable(t);this.renderInert(n)}else"attributes"===e.type&&this.renderInert([e.target])}}]),e}(),pi={attributes:!1,childList:!0,subtree:!0},xi=function(){function e(){var t=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],r=t.context,o=t.filter;n(this,e),this._context=i(r||document.documentElement)[0],this._filter=i(o),this.disengage=this.disengage.bind(this),this.handleMutation=this.handleMutation.bind(this),this.isInsignificantBranch=this.isInsignificantBranch.bind(this);var a=et({context:this._context,filter:this._filter});a.forEach(at),this.startObserver()}return an(e,[{key:"disengage",value:function(){this._context&&([].forEach.call(this._context.querySelectorAll("[data-cached-aria-hidden]"),ut),this._context=null,this._filter=null,this._observer&&this._observer.disconnect(),this._observer=null)}},{key:"startObserver",value:function(){var e=this;window.MutationObserver&&(this._observer=new MutationObserver(function(t){return t.forEach(e.handleMutation)}),this._observer.observe(this._context,pi))}},{key:"handleMutation",value:function(e){"childList"===e.type&&i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE}).filter(this.isInsignificantBranch).forEach(at)}},{key:"isInsignificantBranch",value:function(e){var t=D({context:e});if(t.some(function(e){return"true"===e.getAttribute("aria-hidden")}))return!1;var n=Qe({element:e});return this._filter.some(n)?!1:!0}}]),e}(),yi=function(){function e(t){n(this,e),this._document=u(t),this.maps={}}return an(e,[{key:"getAreasFor",value:function(e){return this.maps[e]||this.addMapByName(e),this.maps[e]}},{key:"addMapByName",value:function(e){var t=te(e,this._document);t&&(this.maps[t.name]=ge({context:t}))}},{key:"extractAreasFromList",value:function(e){return e.filter(function(e){var t=e.nodeName.toLowerCase();if("area"!==t)return!0;var n=e.parentNode;return this.maps[n.name]||(this.maps[n.name]=[]),this.maps[n.name].push(e),!1},this)}}]),e}(),wi=function(){function e(t,r){n(this,e),this.context=t,this.sortElements=r,this.hostCounter=1,this.inHost={},this.inDocument=[],this.hosts={},this.elements={}}return an(e,[{key:"_registerHost",value:function(e){if(!e._sortingId){e._sortingId="shadow-"+this.hostCounter++,this.hosts[e._sortingId]=e;var t=a({context:e});t?(this._registerHost(t),this._registerHostParent(e,t)):this.inDocument.push(e)}}},{key:"_registerHostParent",value:function(e,t){this.inHost[t._sortingId]||(this.inHost[t._sortingId]=[]),this.inHost[t._sortingId].push(e)}},{key:"_registerElement",value:function(e,t){this.elements[t._sortingId]||(this.elements[t._sortingId]=[]),this.elements[t._sortingId].push(e)}},{key:"extractElements",value:function(e){return e.filter(function(e){var t=a({context:e});return t?(this._registerHost(t),this._registerElement(e,t),!1):!0},this)}},{key:"sort",value:function(e){var t=this._injectHosts(e);return t=this._replaceHosts(t),this._cleanup(),t}},{key:"_injectHosts",value:function(e){return Object.keys(this.hosts).forEach(function(e){var t=this.elements[e],n=this.inHost[e],r=this.hosts[e].shadowRoot;this.elements[e]=this._merge(t,n,r)},this),this._merge(e,this.inDocument,this.context)}},{key:"_merge",value:function(e,t,n){var r=Se({list:e,elements:t});return this.sortElements(r,n)}},{key:"_replaceHosts",value:function(e){return Se({list:e,elements:this.inDocument,resolveElement:this._resolveHostElement.bind(this)})}},{key:"_resolveHostElement",value:function(e){var t=Se({list:this.elements[e._sortingId],elements:this.inHost[e._sortingId],resolveElement:this._resolveHostElement.bind(this)}),n=T(e);return null!==n&&n>-1?[e].concat(t):t}},{key:"_cleanup",value:function(){Object.keys(this.hosts).forEach(function(e){delete this.hosts[e]._sortingId},this)}}]),e}(),Ei=void 0,Si={tab:9,left:37,up:38,right:39,down:40,pageUp:33,"page-up":33,pageDown:34,"page-down":34,end:35,home:36,enter:13,escape:27,space:32,shift:16,capsLock:20,"caps-lock":20,ctrl:17,alt:18,meta:91,pause:19,insert:45,"delete":46,backspace:8,_alias:{91:[92,93,224]}},Ti=1;26>Ti;Ti++)Si["f"+Ti]=Ti+111;for(var Ti=0;10>Ti;Ti++){var Ai=Ti+48,Oi=Ti+96;Si[Ti]=Ai,Si["num-"+Ti]=Oi,Si._alias[Ai]=[Oi]}for(var Ti=0;26>Ti;Ti++){var Ai=Ti+65,Ii=String.fromCharCode(Ai).toLowerCase();Si[Ii]=Ai}var Ci={alt:"altKey",ctrl:"ctrlKey",meta:"metaKey",shift:"shiftKey"},Li=Object.keys(Ci).map(function(e){return Ci[e]}),Ni={disabled:ot,hidden:st,tabFocus:wt},ki={"aria-busy":{"default":"false",values:["true","false"]},"aria-checked":{"default":void 0,values:["true","false","mixed",void 0]},"aria-disabled":{"default":"false",values:["true","false"]},"aria-expanded":{"default":void 0,values:["true","false",void 0]},"aria-grabbed":{"default":void 0,values:["true","false",void 0]},"aria-hidden":{"default":"false",values:["true","false"]},"aria-invalid":{"default":"false",values:["true","false","grammar","spelling"]},"aria-pressed":{"default":void 0,values:["true","false","mixed",void 0]},"aria-selected":{"default":void 0,values:["true","false",void 0]},"aria-atomic":{"default":"false",values:["true","false"]},"aria-autocomplete":{"default":"none",values:["inline","list","both","none"]},"aria-dropeffect":{"default":"none",multiple:!0,values:["copy","move","link","execute","popup","none"]},"aria-haspopup":{"default":"false",values:["true","false"]},"aria-live":{"default":"off",values:["off","polite","assertive"]},"aria-multiline":{"default":"false",values:["true","false"]},"aria-multiselectable":{"default":"false",values:["true","false"]},"aria-orientation":{"default":"horizontal",values:["vertical","horizontal"]},"aria-readonly":{"default":"false",values:["true","false"]},"aria-relevant":{"default":"additions text",multiple:!0,values:["additions","removals","text","all"]},"aria-required":{"default":"false",values:["true","false"]},"aria-sort":{"default":"none",other:!0,values:["ascending","descending","none"]}},Mi={attribute:ki,keycode:Si},_i=0,Pi=0,Fi=["touchstart","pointerdown","MSPointerDown","mousedown"],Ri=["touchend","touchcancel","pointerup","MSPointerUp","pointercancel","MSPointerCancel","mouseup"],Bi=De({engage:Lt,disengage:Ct}),Di={interactionType:Bi,shadowMutations:nt},Wi={firstTabbable:Mt,focusable:ve,shadowHosts:tt,tabbable:ge,tabsequence:bt},Hi="undefined"!=typeof document&&("onfocusin"in document?"focusin":"focus"),ji="undefined"!=typeof document&&("onfocusin"in document?"focusout":"blur"),qi=void 0,Ki=void 0,Gi=null,Zi=null,Vi={pointer:!1,key:!1,script:!1,initial:!1},$i=De({engage:Kt,disengage:qt}),Ui=void 0,Xi="undefined"!=typeof document&&("onfocusin"in document?"focusin":"focus"),zi="undefined"!=typeof document&&("onfocusin"in document?"focusout":"blur"),Ji="ally-focus-within",Qi=void 0,Yi=void 0,eo=void 0,to=De({engage:Xt,disengage:Ut}),no={focusSource:$i,focusWithin:to},ro={focusable:on,key:yt,visibleArea:rn},io="undefined"!=typeof window&&window.ally,oo={element:qr,event:Jr,fix:ci,get:di,is:fi,maintain:Ni,map:Mi,observe:Di,query:Wi,style:no,when:ro,version:wn,noConflict:function(){return"undefined"!=typeof window&&window.ally===this&&(window.ally=io),this}};t.exports=oo},{"array.prototype.findindex":2,"css.escape":3,platform:4}],2:[function(){!function(){if(!Array.prototype.findIndex){var e=function(e){var t=Object(this),n=Math.max(0,t.length)>>>0;if(0===n)return-1;if("function"!=typeof e||"[object Function]"!==Object.prototype.toString.call(e))throw new TypeError("Array#findIndex: predicate must be a function");for(var r=arguments.length>1?arguments[1]:void 0,i=0;n>i;i++)if(e.call(r,t[i],i,t))return i;return-1};if(Object.defineProperty)try{Object.defineProperty(Array.prototype,"findIndex",{value:e,configurable:!0,writable:!0})}catch(t){}Array.prototype.findIndex||(Array.prototype.findIndex=e)}}(this)},{}],3:[function(t,n,r){(function(t){!function(t,i){"object"==typeof r?n.exports=i(t):"function"==typeof e&&e.amd?e([],i.bind(t,t)):i(t)}("undefined"!=typeof t?t:this,function(e){if(e.CSS&&e.CSS.escape)return e.CSS.escape;var t=function(e){if(0==arguments.length)throw new TypeError("`CSS.escape` requires an argument.");for(var t,n=String(e),r=n.length,i=-1,o="",a=n.charCodeAt(0);++i<r;)t=n.charCodeAt(i),o+=0!=t?t>=1&&31>=t||127==t||0==i&&t>=48&&57>=t||1==i&&t>=48&&57>=t&&45==a?"\\"+t.toString(16)+" ":(0!=i||1!=r||45!=t)&&(t>=128||45==t||95==t||t>=48&&57>=t||t>=65&&90>=t||t>=97&&122>=t)?n.charAt(i):"\\"+n.charAt(i):"�";return o};return e.CSS||(e.CSS={}),e.CSS.escape=t,t})}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],4:[function(t,n,r){(function(t){(function(){"use strict";function i(e){return e=String(e),e.charAt(0).toUpperCase()+e.slice(1)}function o(e,t,n){var r={6.4:"10",6.3:"8.1",6.2:"8",6.1:"Server 2008 R2 / 7","6.0":"Server 2008 / Vista",5.2:"Server 2003 / XP 64-bit",5.1:"XP",5.01:"2000 SP1","5.0":"2000","4.0":"NT","4.90":"ME"};return t&&n&&/^Win/i.test(e)&&(r=r[/[\d.]+$/.exec(e)])&&(e="Windows "+r),e=String(e),t&&n&&(e=e.replace(RegExp(t,"i"),n)),e=u(e.replace(/ ce$/i," CE").replace(/\bhpw/i,"web").replace(/\bMacintosh\b/,"Mac OS").replace(/_PowerPC\b/i," OS").replace(/\b(OS X) [^ \d]+/i,"$1").replace(/\bMac (OS X)\b/,"$1").replace(/\/(\d)/," $1").replace(/_/g,".").replace(/(?: BePC|[ .]*fc[ \d.]+)$/i,"").replace(/\bx86\.64\b/gi,"x86_64").replace(/\b(Windows Phone) OS\b/,"$1").split(" on ")[0])}function a(e,t){var n=-1,r=e?e.length:0;if("number"==typeof r&&r>-1&&w>=r)for(;++n<r;)t(e[n],n,e);else s(e,t)}function u(e){return e=m(e),/^(?:webOS|i(?:OS|P))/.test(e)?e:i(e)}function s(e,t){for(var n in e)A.call(e,n)&&t(e[n],n,e)}function l(e){return null==e?i(e):O.call(e).slice(8,-1)}function c(e,t){var n=null!=e?typeof e[t]:"number";return!/^(?:boolean|number|string|undefined)$/.test(n)&&("object"==n?!!e[t]:!0)}function d(e){return String(e).replace(/([ -])(?!$)/g,"$1?")}function f(e,t){var n=null;return a(e,function(r,i){n=t(n,r,i,e)}),n}function m(e){return String(e).replace(/^ +| +$/g,"")}function b(e){function t(t){return f(t,function(t,n){return t||RegExp("\\b"+(n.pattern||d(n))+"\\b","i").exec(e)&&(n.label||n)})}function n(t){return f(t,function(t,n,r){return t||(n[X]||n[/^[a-z]+(?: +[a-z]+\b)*/i.exec(X)]||RegExp("\\b"+d(r)+"(?:\\b|\\w*\\d)","i").exec(e))&&r})}function r(t){return f(t,function(t,n){return t||RegExp("\\b"+(n.pattern||d(n))+"\\b","i").exec(e)&&(n.label||n)})}function i(t){return f(t,function(t,n){var r=n.pattern||d(n);return!t&&(t=RegExp("\\b"+r+"(?:/[\\d.]+|[ \\w.]*)","i").exec(e))&&(t=o(t,r,n.label||n)),t})}function a(t){return f(t,function(t,n){var r=n.pattern||d(n);return!t&&(t=RegExp("\\b"+r+" *\\d+[.\\w_]*","i").exec(e)||RegExp("\\b"+r+"(?:; *(?:[a-z]+[_-])?[a-z]+\\d+|[^ ();-]*)","i").exec(e))&&((t=String(n.label&&!RegExp(r,"i").test(n.label)?n.label:t).split("/"))[1]&&!/[\d.]+/.test(t[0])&&(t[0]+=" "+t[1]),
  n=n.label||n,t=u(t[0].replace(RegExp(r,"i"),n).replace(RegExp("; *(?:"+n+"[_-])?","i")," ").replace(RegExp("("+n+")[-_.]?(\\w)","i"),"$1 $2"))),t})}function v(t){return f(t,function(t,n){return t||(RegExp(n+"(?:-[\\d.]+/|(?: for [\\w-]+)?[ /-])([\\d.]+[^ ();/_-]*)","i").exec(e)||0)[1]||null})}function p(){return this.description||""}var x=h,y=e&&"object"==typeof e&&"String"!=l(e);y&&(x=e,e=null);var w=x.navigator||{},T=w.userAgent||"";e||(e=T);var A,I,C=y||S==g,L=y?!!w.likeChrome:/\bChrome\b/.test(e)&&!/internal|\n/i.test(O.toString()),N="Object",k=y?N:"ScriptBridgingProxyObject",M=y?N:"Environment",_=y&&x.java?"JavaPackage":l(x.java),P=y?N:"RuntimeObject",F=/\bJava/.test(_)&&x.java,R=F&&l(x.environment)==M,B=F?"a":"α",D=F?"b":"β",W=x.document||{},H=x.operamini||x.opera,j=E.test(j=y&&H?H["[[Class]]"]:l(H))?j:H=null,q=e,K=[],G=null,Z=e==T,V=Z&&H&&"function"==typeof H.version&&H.version(),$=t(["Trident",{label:"WebKit",pattern:"AppleWebKit"},"iCab","Presto","NetFront","Tasman","KHTML","Gecko"]),U=r(["Adobe AIR","Arora","Avant Browser","Breach","Camino","Epiphany","Fennec","Flock","Galeon","GreenBrowser","iCab","Iceweasel",{label:"SRWare Iron",pattern:"Iron"},"K-Meleon","Konqueror","Lunascape","Maxthon","Midori","Nook Browser","PhantomJS","Raven","Rekonq","RockMelt","SeaMonkey",{label:"Silk",pattern:"(?:Cloud9|Silk-Accelerated)"},"Sleipnir","SlimBrowser","Sunrise","Swiftfox","WebPositive","Opera Mini",{label:"Opera Mini",pattern:"OPiOS"},"Opera",{label:"Opera",pattern:"OPR"},"Chrome",{label:"Chrome Mobile",pattern:"(?:CriOS|CrMo)"},{label:"Firefox",pattern:"(?:Firefox|Minefield)"},{label:"IE",pattern:"IEMobile"},{label:"IE",pattern:"MSIE"},"Safari"]),X=a([{label:"BlackBerry",pattern:"BB10"},"BlackBerry",{label:"Galaxy S",pattern:"GT-I9000"},{label:"Galaxy S2",pattern:"GT-I9100"},{label:"Galaxy S3",pattern:"GT-I9300"},{label:"Galaxy S4",pattern:"GT-I9500"},"Google TV","Lumia","iPad","iPod","iPhone","Kindle",{label:"Kindle Fire",pattern:"(?:Cloud9|Silk-Accelerated)"},"Nook","PlayBook","PlayStation 4","PlayStation 3","PlayStation Vita","TouchPad","Transformer",{label:"Wii U",pattern:"WiiU"},"Wii","Xbox One",{label:"Xbox 360",pattern:"Xbox"},"Xoom"]),z=n({Apple:{iPad:1,iPhone:1,iPod:1},Amazon:{Kindle:1,"Kindle Fire":1},Asus:{Transformer:1},"Barnes & Noble":{Nook:1},BlackBerry:{PlayBook:1},Google:{"Google TV":1},HP:{TouchPad:1},HTC:{},LG:{},Microsoft:{Xbox:1,"Xbox One":1},Motorola:{Xoom:1},Nintendo:{"Wii U":1,Wii:1},Nokia:{Lumia:1},Samsung:{"Galaxy S":1,"Galaxy S2":1,"Galaxy S3":1,"Galaxy S4":1},Sony:{"PlayStation 4":1,"PlayStation 3":1,"PlayStation Vita":1}}),J=i(["Windows Phone ","Android","CentOS","Debian","Fedora","FreeBSD","Gentoo","Haiku","Kubuntu","Linux Mint","Red Hat","SuSE","Ubuntu","Xubuntu","Cygwin","Symbian OS","hpwOS","webOS ","webOS","Tablet OS","Linux","Mac OS X","Macintosh","Mac","Windows 98;","Windows "]);if($&&($=[$]),z&&!X&&(X=a([z])),(A=/\bGoogle TV\b/.exec(X))&&(X=A[0]),/\bSimulator\b/i.test(e)&&(X=(X?X+" ":"")+"Simulator"),"Opera Mini"==U&&/\bOPiOS\b/.test(e)&&K.push("running in Turbo/Uncompressed mode"),/^iP/.test(X)?(U||(U="Safari"),J="iOS"+((A=/ OS ([\d_]+)/i.exec(e))?" "+A[1].replace(/_/g,"."):"")):"Konqueror"!=U||/buntu/i.test(J)?z&&"Google"!=z&&(/Chrome/.test(U)&&!/\bMobile Safari\b/i.test(e)||/\bVita\b/.test(X))?(U="Android Browser",J=/\bAndroid\b/.test(J)?J:"Android"):(!U||(A=!/\bMinefield\b|\(Android;/i.test(e)&&/\b(?:Firefox|Safari)\b/.exec(U)))&&(U&&!X&&/[\/,]|^[^(]+?\)/.test(e.slice(e.indexOf(A+"/")+8))&&(U=null),(A=X||z||J)&&(X||z||/\b(?:Android|Symbian OS|Tablet OS|webOS)\b/.test(J))&&(U=/[a-z]+(?: Hat)?/i.exec(/\bAndroid\b/.test(J)?J:A)+" Browser")):J="Kubuntu",(A=/\((Mobile|Tablet).*?Firefox\b/i.exec(e))&&A[1]&&(J="Firefox OS",X||(X=A[1])),V||(V=v(["(?:Cloud9|CriOS|CrMo|IEMobile|Iron|Opera ?Mini|OPiOS|OPR|Raven|Silk(?!/[\\d.]+$))","Version",d(U),"(?:Firefox|Minefield|NetFront)"])),"iCab"==$&&parseFloat(V)>3?$=["WebKit"]:"Trident"!=$&&(A=/\bOpera\b/.test(U)&&(/\bOPR\b/.test(e)?"Blink":"Presto")||/\b(?:Midori|Nook|Safari)\b/i.test(e)&&"WebKit"||!$&&/\bMSIE\b/i.test(e)&&("Mac OS"==J?"Tasman":"Trident"))?$=[A]:/\bPlayStation\b(?! Vita\b)/i.test(U)&&"WebKit"==$&&($=["NetFront"]),"IE"==U&&(A=(/; *(?:XBLWP|ZuneWP)(\d+)/i.exec(e)||0)[1])?(U+=" Mobile",J="Windows Phone "+(/\+$/.test(A)?A:A+".x"),K.unshift("desktop mode")):/\bWPDesktop\b/i.test(e)?(U="IE Mobile",J="Windows Phone 8+",K.unshift("desktop mode"),V||(V=(/\brv:([\d.]+)/.exec(e)||0)[1])):"IE"!=U&&"Trident"==$&&(A=/\brv:([\d.]+)/.exec(e))?(/\bWPDesktop\b/i.test(e)||(U&&K.push("identifying as "+U+(V?" "+V:"")),U="IE"),V=A[1]):"Chrome"!=U&&"IE"==U||!(A=/\bEdge\/([\d.]+)/.exec(e))||(U="Microsoft Edge",V=A[1],$=["Trident"]),Z){if(c(x,"global"))if(F&&(A=F.lang.System,q=A.getProperty("os.arch"),J=J||A.getProperty("os.name")+" "+A.getProperty("os.version")),C&&c(x,"system")&&(A=[x.system])[0]){J||(J=A[0].os||null);try{A[1]=x.require("ringo/engine").version,V=A[1].join("."),U="RingoJS"}catch(Q){A[0].global.system==x.system&&(U="Narwhal")}}else"object"==typeof x.process&&(A=x.process)?(U="Node.js",q=A.arch,J=A.platform,V=/[\d.]+/.exec(A.version)[0]):R&&(U="Rhino");else l(A=x.runtime)==k?(U="Adobe AIR",J=A.flash.system.Capabilities.os):l(A=x.phantom)==P?(U="PhantomJS",V=(A=A.version||null)&&A.major+"."+A.minor+"."+A.patch):"number"==typeof W.documentMode&&(A=/\bTrident\/(\d+)/i.exec(e))&&(V=[V,W.documentMode],(A=+A[1]+4)!=V[1]&&(K.push("IE "+V[1]+" mode"),$&&($[1]=""),V[1]=A),V="IE"==U?String(V[1].toFixed(1)):V[0]);J=J&&u(J)}V&&(A=/(?:[ab]|dp|pre|[ab]\d+pre)(?:\d+\+?)?$/i.exec(V)||/(?:alpha|beta)(?: ?\d)?/i.exec(e+";"+(Z&&w.appMinorVersion))||/\bMinefield\b/i.test(e)&&"a")&&(G=/b/i.test(A)?"beta":"alpha",V=V.replace(RegExp(A+"\\+?$"),"")+("beta"==G?D:B)+(/\d+\+?/.exec(A)||"")),"Fennec"==U||"Firefox"==U&&/\b(?:Android|Firefox OS)\b/.test(J)?U="Firefox Mobile":"Maxthon"==U&&V?V=V.replace(/\.[\d.]+/,".x"):"Silk"==U?(/\bMobi/i.test(e)||(J="Android",K.unshift("desktop mode")),/Accelerated *= *true/i.test(e)&&K.unshift("accelerated")):/\bXbox\b/i.test(X)?(J=null,"Xbox 360"==X&&/\bIEMobile\b/.test(e)&&K.unshift("mobile mode")):!/^(?:Chrome|IE|Opera)$/.test(U)&&(!U||X||/Browser|Mobi/.test(U))||"Windows CE"!=J&&!/Mobi/i.test(e)?"IE"==U&&Z&&null===x.external?K.unshift("platform preview"):(/\bBlackBerry\b/.test(X)||/\bBB10\b/.test(e))&&(A=(RegExp(X.replace(/ +/g," *")+"/([.\\d]+)","i").exec(e)||0)[1]||V)?(A=[A,/BB10/.test(e)],J=(A[1]?(X=null,z="BlackBerry"):"Device Software")+" "+A[0],V=null):this!=s&&"Wii"!=X&&(Z&&H||/Opera/.test(U)&&/\b(?:MSIE|Firefox)\b/i.test(e)||"Firefox"==U&&/\bOS X (?:\d+\.){2,}/.test(J)||"IE"==U&&(J&&!/^Win/.test(J)&&V>5.5||/\bWindows XP\b/.test(J)&&V>8||8==V&&!/\bTrident\b/.test(e)))&&!E.test(A=b.call(s,e.replace(E,"")+";"))&&A.name&&(A="ing as "+A.name+((A=A.version)?" "+A:""),E.test(U)?(/\bIE\b/.test(A)&&"Mac OS"==J&&(J=null),A="identify"+A):(A="mask"+A,U=j?u(j.replace(/([a-z])([A-Z])/g,"$1 $2")):"Opera",/\bIE\b/.test(A)&&(J=null),Z||(V=null)),$=["Presto"],K.push(A)):U+=" Mobile",(A=(/\bAppleWebKit\/([\d.]+\+?)/i.exec(e)||0)[1])&&(A=[parseFloat(A.replace(/\.(\d)$/,".0$1")),A],"Safari"==U&&"+"==A[1].slice(-1)?(U="WebKit Nightly",G="alpha",V=A[1].slice(0,-1)):(V==A[1]||V==(A[2]=(/\bSafari\/([\d.]+\+?)/i.exec(e)||0)[1]))&&(V=null),A[1]=(/\bChrome\/([\d.]+)/i.exec(e)||0)[1],537.36==A[0]&&537.36==A[2]&&parseFloat(A[1])>=28&&"IE"!=U&&"Microsoft Edge"!=U&&($=["Blink"]),Z&&(L||A[1])?($&&($[1]="like Chrome"),A=A[1]||(A=A[0],530>A?1:532>A?2:532.05>A?3:533>A?4:534.03>A?5:534.07>A?6:534.1>A?7:534.13>A?8:534.16>A?9:534.24>A?10:534.3>A?11:535.01>A?12:535.02>A?"13+":535.07>A?15:535.11>A?16:535.19>A?17:536.05>A?18:536.1>A?19:537.01>A?20:537.11>A?"21+":537.13>A?23:537.18>A?24:537.24>A?25:537.36>A?26:"Blink"!=$?"27":"28")):($&&($[1]="like Safari"),A=A[0],A=400>A?1:500>A?2:526>A?3:533>A?4:534>A?"4+":535>A?5:537>A?6:538>A?7:601>A?8:"8"),$&&($[1]+=" "+(A+="number"==typeof A?".x":/[.+]/.test(A)?"":"+")),"Safari"==U&&(!V||parseInt(V)>45)&&(V=A)),"Opera"==U&&(A=/\bzbov|zvav$/.exec(J))?(U+=" ",K.unshift("desktop mode"),"zvav"==A?(U+="Mini",V=null):U+="Mobile",J=J.replace(RegExp(" *"+A+"$"),"")):"Safari"==U&&/\bChrome\b/.exec($&&$[1])&&(K.unshift("desktop mode"),U="Chrome Mobile",V=null,/\bOS X\b/.test(J)?(z="Apple",J="iOS 4.3+"):J=null),V&&0==V.indexOf(A=/[\d.]+$/.exec(J))&&e.indexOf("/"+A+"-")>-1&&(J=m(J.replace(A,""))),$&&!/\b(?:Avant|Nook)\b/.test(U)&&(/Browser|Lunascape|Maxthon/.test(U)||/^(?:Adobe|Arora|Breach|Midori|Opera|Phantom|Rekonq|Rock|Sleipnir|Web)/.test(U)&&$[1])&&(A=$[$.length-1])&&K.push(A),K.length&&(K=["("+K.join("; ")+")"]),z&&X&&X.indexOf(z)<0&&K.push("on "+z),X&&K.push((/^on /.test(K[K.length-1])?"":"on ")+X),J&&(A=/ ([\d.+]+)$/.exec(J),I=A&&"/"==J.charAt(J.length-A[0].length-1),J={architecture:32,family:A&&!I?J.replace(A[0],""):J,version:A?A[1]:null,toString:function(){var e=this.version;return this.family+(e&&!I?" "+e:"")+(64==this.architecture?" 64-bit":"")}}),(A=/\b(?:AMD|IA|Win|WOW|x86_|x)64\b/i.exec(q))&&!/\bi686\b/i.test(q)&&(J&&(J.architecture=64,J.family=J.family.replace(RegExp(" *"+A),"")),U&&(/\bWOW64\b/i.test(e)||Z&&/\w(?:86|32)$/.test(w.cpuClass||w.platform)&&!/\bWin64; x64\b/i.test(e))&&K.unshift("32-bit")),e||(e=null);var Y={};return Y.description=e,Y.layout=$&&$[0],Y.manufacturer=z,Y.name=U,Y.prerelease=G,Y.product=X,Y.ua=e,Y.version=U&&V,Y.os=J||{architecture:null,family:null,version:null,toString:function(){return"null"}},Y.parse=b,Y.toString=p,Y.version&&K.unshift(V),Y.name&&K.unshift(U),J&&U&&(J!=String(J).split(" ")[0]||J!=U.split(" ")[0]&&!X)&&K.push(X?"("+J+")":"on "+J),K.length&&(Y.description=K.join(" ")),Y}var v={"function":!0,object:!0},h=v[typeof window]&&window||this,g=h,p=v[typeof r]&&r,x=v[typeof n]&&n&&!n.nodeType&&n,y=p&&x&&"object"==typeof t&&t;!y||y.global!==y&&y.window!==y&&y.self!==y||(h=y);var w=Math.pow(2,53)-1,E=/\bOpera/,S=this,T=Object.prototype,A=T.hasOwnProperty,O=T.toString;"function"==typeof e&&"object"==typeof e.amd&&e.amd?e(function(){return b()}):p&&x?s(b(),function(e,t){p[t]=e}):h.platform=b()}).call(this)}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}]},{},[1])(1)});

};
//# sourceMappingURL=ally.min.js.map

/**
 * @name SIA
 * @description Define global autoCompleteLanguage functions
 * @version 1.0
 */

SIA.autoCompleteLanguage = function(){
	var global = SIA.global;
	var doc = global.vars.doc;
	var win = global.vars.win;
	var	config = global.config;
	var body = global.vars.body;
	var languageJSON = global.vars.languageJSON;


	var autoCompleteCustom = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.airportData = that.options.airportData;
		that.timer = null;
		that.timerResize = null;

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('div.custom-select');
			var bookingAutoComplete = field.autocomplete({
					minLength : 0,
					open: that.options.open,
					change: that.options.change,
					select: that.options.select,
					close: that.options.close,
					response: that.options.response,
					search: that.options.search,
					// source: that.airportData,
					source: function(request, response) {
						// create a regex from ui.autocomplete for 'safe returns'
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
						// match the user's request against each destination's keys,

						var match = $.grep(that.airportData, function(airport) {
							var flag = airport.flag;
							var value = airport.value;

							return (matcher.test(value) || matcher.test(flag)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
						});

						// ... return if ANY of the keys are matched
						response(match);
					},
					appendTo : that.options.autoCompleteAppendTo
				}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function (ul, item) {
				// customising our suggestion dropdowns here
				return $('<li class="autocomplete-item">')
				/*.attr('data-value', item.order)
				.attr('data-language', item.language)
				.attr('data-flag', item.flag)*/
				.attr({'data-value': item.order, 'data-language': item.language, 'data-flag': item.flag})
				.append('<a class="autocomplete-link" href="javascript:void(0);"><img src="images/transparent.png" alt="" class="flags '+ item.flag +'">'+ item.value + '</a>')
				.appendTo(ul);
			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			bookingAutoComplete._move = function( direction ) {
				var item, previousItem,
				last = false,
				api = this.menu.element.data('jsp'),
				li = $(),
				minus = null;
				if (!api) {
					if (!this.menu.element.is(':visible')) {
						this.search(null, event);
						return;
					}
					if (this.menu.isFirstItem() && /^previous/.test(direction) ||
							this.menu.isLastItem() && /^next/.test(direction) ) {
						this._value( this.term );
						this.menu.blur();
						return;
					}
					this.menu[direction](event);
				}
				else {
					var currentPosition = api.getContentPositionY();
					switch(direction){
						case 'next':
							if(this.element.val() === ''){
								api.scrollToY(0);
								li = this.menu.element.find('li:first');
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.next();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
								// console.log(currentPosition, previousItem.position().top);
							}
							if(!item){
								last = true;
								li = this.menu.element.find('li').removeClass('active').first();
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(0);
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								minus = li.position().top + li.innerHeight();
								if(minus - this.menu.element.height() > currentPosition){
									api.scrollToY(Math.max(0, minus - this.menu.element.height()));
								}
							}
							$('#wcag-custom-select').html(item.value);
							break;
						case 'previous':
							if(this.element.val() === ''){
								last = true;
								item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.prev();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
							}
							if(!item){
								last = true;
								item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(this.menu.element.find('.jspPane').height());
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								if(li.position().top <= currentPosition){
									api.scrollToY(li.position().top);
								}
							}
							break;
					}
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');
			// if(window.Modernizr.touch || window.navigator.msPointerEnabled){
			// 	win.off('resize.blur'+index).on('resize.blur'+index, function(){
			// 		clearTimeout(that.timerResize);
			// 		// that.timerResize = setTimeout(function(){
			// 		// 	field.blur();
			// 		// }, 200);
			// 	});
			// }
			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				var self = $(this);
				self.closest('.custom-select').addClass('focus');
				// return false;
				if(global.vars.isIE()){
					doc.off('mousedown.hideAutocompleteLanguage').on('mousedown.hideAutocompleteLanguage', function(e){
						if(!$(e.target).closest('.ui-autocomplete').length){
							field.closest('.custom-select').removeClass('focus');
							field.autocomplete('close');
						}
					});
				}

				SIA.WcagGlobal.customSelectAriaLive(field.closest('.custom-select .select__text'));

				// var dataAutocomplete = self.data('uiAutocomplete');
				// if(dataAutocomplete && self.val()) {
				// 	if(!dataAutocomplete.selectedItem) {
				// 		clearTimeout(timerTriggerSearch);
				// 		timerTriggerSearch = setTimeout(function() {
				// 			dataAutocomplete.search();
				// 		}, 100);
				// 	}
				// }
			});
			if(!global.vars.isIE()){
				field.off('blur.highlight').on('blur.highlight', function(){
					that.timer = setTimeout(function(){
						field.closest('.custom-select').removeClass('focus');
						field.autocomplete('close');
					}, 200);

					// if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
					// 	win.off('resize.reposition');
					// }

				});
				field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
					clearTimeout(that.timer);
				});
			}

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					e.preventDefault();
					if(field.autocomplete('widget').find('li').length === 1){
						field.autocomplete('widget').find('li').trigger('click');
						return;
					}
					field.autocomplete('widget').find('li.active').trigger('click');
				}
			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
			// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
	};

	var autoCompleteLanguage = function(){
		var ppLanguage = global.vars.ppLanguage;
		if (!ppLanguage.length) {
			return;
		}
		var optionLanguage = ppLanguage.find('.custom-radio');
		var parentOptionLanguage = optionLanguage.parent();
		var defaultFlag = ppLanguage.find('.custom-select img').attr('class').split(' ')[1];
		parentOptionLanguage.children().not(':eq(0)').remove();
		// optionLanguage.not(':eq(0)').hide();
		var dataLanguage = ['en_UK', 'zh_CN', 'fr_FR', 'pt_BR', 'de_DE', 'zh_TW', 'ja_JP', 'ko_KR', 'ru_RU', 'es_ES'];
		var detectLanguge = function(lng){
			var lngs = lng.split(',');
			var isLng = [];

			for(var i = 0; i < lngs.length; i ++){
				for(var ii = 0; ii < dataLanguage.length; ii ++){
					if($.trim(lngs[i]) === $.trim(dataLanguage[ii])){
						isLng.push(ii);
					}
				}
			}

			return isLng;
		};

		var _selectLanguage = function(arr){
			parentOptionLanguage.empty();
			optionLanguage.find(':radio').removeAttr('checked');
			for(var i = 0; i< arr.length; i ++){
				var t = optionLanguage.eq(arr[i]).clone().appendTo(parentOptionLanguage);
				if(i === 0){
					t.find(':radio').prop('checked', true);
				}
				else{
					t.find(':radio').prop('checked', false);
				}
			}
		};
		var timerAutocompleteLang = null;
		var timerAutocompleteLangOpen = null;

		var getFlags = function(value){
			var a = {
				idx : 0,
				flag : false
			};
			for(var i = 0; i < languageJSON.data.length; i ++){
				if(languageJSON.data[i].value === value){
					a = {
						idx : i,
						flag : true
					};
				}
			}
			return a;
		};

		autoCompleteCustom({
			containerAutocomplete : ppLanguage,
			autocompleteFields : 'input#text-country',
			autoCompleteAppendTo: body,
			airportData : languageJSON.data,
			open: function(){
				var self = $(this);
				self.autocomplete('widget').hide();
				clearTimeout(timerAutocompleteLangOpen);
				timerAutocompleteLangOpen = setTimeout(function(){
					self.autocomplete('widget').show().css({
						'left': self.closest('.custom-select--2').offset().left,
						'top': self.closest('.custom-select--2').offset().top + self.closest('.custom-select--2').outerHeight(true)
					});
					self.autocomplete('widget')
					.jScrollPane({
						scrollPagePercent				: 10
					}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
				}, 100);
			},
			select: function(event, ui){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				flag.removeClass().addClass('flags ' + ui.item.flag);
				_selectLanguage(detectLanguge(ui.item.language));
				if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){
					win.trigger('resize.popupLanguage');
				}
				setTimeout(function(){
					self.blur();
				}, 100);
			},
			response: function(event, ui){
				if(ui.content.length ===1){
					// $(this).val(ui.content[0].value);
					// $(this).select();
				}
			},
			search: function(){
				var self = $(this);
				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				clearTimeout(timerAutocompleteLang);
				timerAutocompleteLang = setTimeout(function(){
					// if(self.autocomplete('widget').find('li').length === 1){
					// 	self.autocomplete('widget').find('li').addClass('active');
					// }
				}, 100);
			},
			close: function(){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				var item = getFlags(self.val());
				if(item.flag){
					flag.removeClass().addClass('flags ' + languageJSON.data[item.idx].flag);
					_selectLanguage(detectLanguge(languageJSON.data[item.idx].language));
				}
				$('#wcag-custom-select').html(self.val());
				self.prev().focus();
				if(!$.trim(self.val())){
					flag.removeClass().addClass('flags ' + defaultFlag);
					var defaultLanguage = 'en_UK, zh_CN';
					_selectLanguage(detectLanguge($.trim(defaultLanguage)));
				}
				$(this).autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				doc.off('mousedown.hideAutocompleteLanguage');
			},
			setWidth: 0
		});
	};
	// init
	autoCompleteLanguage();
};

/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'checkboxAll';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var that = this;
			var chbMaster = that.element.find('[data-checkbox-master]').first();
			var chbItems = that.element.find('[data-checkbox-item]');


			chbMaster.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checked = $(this).is(':checked');
				chbItems.filter(':visible').prop('checked', checked);
			});

			chbItems.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checkedAll = (chbItems.filter(':visible').length === chbItems.filter(':checked').length);
				chbMaster.prop('checked', checkedAll);
			});

		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
	};

	// $(function() {
	// 	$('[data-checkbox-all]')[pluginName]();
	// });

}(jQuery, window));

/**
 * @name SIA
 * @description Define global cookiesUse functions
 * @version 1.0
 */
SIA.cookiesUse = function(){
	var popupCookies = $('.popup--cookie').appendTo(SIA.global.vars.container);
	// cookies

	var setCookie = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+d.toGMTString();
		document.cookie = cname + '=' + cvalue + ';' + expires;
	};

	var getCookie = function(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i].trim();
			if (c.indexOf(name) === 0){
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	var checkCookie = function () {
		var user = getCookie('seen');

		if (user !== '') {
			popupCookies.addClass('hidden');
		} else {
			popupCookies.removeClass('hidden');
		}
	};
	// end cookies
	checkCookie();
	popupCookies.find('.popup__close').off('click.closeCookie').on('click.closeCookie', function(e){
		e.preventDefault();
		setCookie('seen', true, 7);
		popupCookies.addClass('hidden');
	});
};

/**
 *  @name customSelect
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */

// .active is used to determine the equivalent of option:selected
// .focus is used for ?
// .keyboardfocus is used for the equivalent of :hover


(function($, window, undefined) {
	var pluginName = 'customSelect';
	var openedSelect = null;
	var getUID = (function(){
		var id = 0;
		return function(){
			return pluginName + '-' + id++;
		};
	})();
	var win = $(window);
	var doc = $(document);
	var body = $(document.body);
	var isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};
	var key = {
		DOWN: 40,
		UP: 38,
		ENTER: 13,
		ESC: 27,
		SPACE: 32,
		TAB: 9
	};


	var returnFalse = false;

	function scrollWithADistance(that, top) {
		that.scrollBarBtn.css({
			'top' : Math.min(top, that.scrollContainer.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : Math.max(-top * that.ratioOfWrapperToScroll + top * that.ratioOfContenToScroll, that.scrollContainer.height() - that.wrapperOption.height())
		});
	}

	function scrollWithY(that, Y) {
		var y = Y;
		that.scrollBarBtn.css({
			'top' : Math.min(y/(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll), that.scrollBarWrapper.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : -Math.min(that.itemsLength*that.options.heightItem - that.scrollContainer.height(), y)
		});
	}

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.idPlugin = getUID();
			that.options = plugin.options;
			that.curItem = $();
			// that.hasDefaultClass = that.hasClass('default');
			plugin.inPopup = that.closest('.popup');
			that.selectEl = that.find(plugin.options.selectEl);
			if(!that.selectEl.is('select')){
				return;
			}
			that.customText = that.find(plugin.options.customText);
			that.customIcon = that.find('.ico-dropdown');
			that.arrow = that.find(plugin.options.arrow);
			that.selectLabel = that.find('.select__label');

			that.customText.attr('aria-hidden', true);
			that.customIcon.attr('aria-hidden', true);

			that.simulatorListbox = $('<ul/>')
				.attr({
					'role': 'listbox',
					'id': that.idPlugin + '-listbox',
					'aria-multiselectable': false
				});

			that.scroll = $('<div/>')
				.addClass('custom-scroll custom-dropdown')
				.append( $('<div/>')
					.addClass('scroll-container')
					.append( that.simulatorListbox )
					.append( $('<div/>' )
						.addClass('scroll-bar')
						.attr('role', 'presentation')
						.html('<span>&nbsp;</span>')
					)
				);

			that.scrollContainer = that.scroll.find('.scroll-container');
			that.wrapperOption = that.scrollContainer.find('ul');
			that.scrollBarWrapper = that.scroll.find('.scroll-bar');
			that.scrollBarBtn = that.scrollBarWrapper.children();

			plugin._createTemplate();
			plugin.refresh();
			var closeSelect = function(){
				if(openedSelect){
					openedSelect.hide();
					// openedSelect = null;
				}
			};

			// if(that.data('filter')){
			// 	that.find('[data-trigger]').off('click.show').on('click.show', function(e){
			// 		if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
			// 		that.trigger('beforeSelect');
			// 		e.stopPropagation();
			// 		if(!that.hasClass('active')){
			// 			plugin.show();
			// 			closeSelect();
			// 			openedSelect = plugin;
			// 		}
			// 		else{
			// 			// openedSelect = null;
			// 			plugin.hide();
			// 		}
			// 	});
			// }else{
				that.off('click.show').on('click.show', function(e){
					clearTimeout(that.simulatorTimer);
					if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
					that.trigger('beforeSelect');
					e.stopPropagation();
					if(that.hasClass('active')){
						// openedSelect = null;
						plugin.hide();
					}
					else{
						$(document).trigger('click');
						plugin.show();
						closeSelect();
						openedSelect = plugin;
					}
				});
				that.find('.select__tips').on('click', function(e){
					e.stopPropagation();
				});
				// reroute the label from the hidden select to the visible combobox
				that.selectLabel.attr({
					'for': that.idPlugin + '-combobox'
				});

				that.simulatorCombobox = $('<input/>')
					.addClass('input-overlay')
					.attr({
						'id': that.idPlugin + '-combobox',
						'name': that.idPlugin + '-combobox',
						'aria-expanded': false,
						'aria-autocomplete': 'none',
						'aria-owns': that.idPlugin + '-listbox', // semantic sugar for NVDA, in combination with aria-selected
						'role': 'combobox',
						'readonly': 'readonly' // requires VO users to navigate with VO + arrow keys, and select with VO + space
					});

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {

					if ( SIA.WcagGlobal.isIE() ) {
						that.attr('role', 'document'); // ensures that combobox is announced and selection option and description are available
					}

				}

				that.simulatorCombobox
					.val( $('#' + that.idPlugin + '-listbox li.active').text() ) // this communicates the selected option to VO and JAWS
					.insertAfter( that.selectEl );

				that.append( $('<p></p>')
					.addClass('select__tips ui-helper-hidden-accessible')
					.attr({
						'id': that.idPlugin + '-customSelectUsage'
					})
				);
				if(typeof L10n !== 'undefined'){
					if(navigator.platform === 'MacIntel'){
						$('#' + that.idPlugin + '-customSelectUsage').text( L10n.tips.customSelectUsage.osx );
					}else{
						$('#' + that.idPlugin + '-customSelectUsage').text( L10n.tips.customSelectUsage.windows );
					}
				}

				if(typeof SIA.WcagGlobal !== 'undefined'){
					SIA.WcagGlobal.assignDescriptionAttributes({
						descriptionSelector:'#'+that.idPlugin+'-customSelectUsage',
						descriptionCategory:'customSelectUsage',
						uniqueHostSelector:'#'+that.idPlugin+'-combobox'
					});
				}

				that.simulatorCombobox
					.off('click.preventDefault').on('click.preventDefault', function(e){
						e.preventDefault();
					})
					.off('focusin.focusHighlighting').on('focusin.focusHighlighting', function(e) {
						$(that).addClass('focus'); // .focus.active breaks SPACE listener
						e.preventDefault();
					})
					.off('focusout.focusHighlighting').on('focusout.focusHighlighting', function(e) {
						$(that).removeClass('focus'); // .focus.active breaks SPACE listener
						e.preventDefault();
					})
					.off('click.show').on('click.show', function() {
						if(that.selectEl.prop('disabled') || that.hasClass('disabled')){
							return;
						}
						that.simulatorTimer = setTimeout(function(){
							if(!that.hasClass('active')){
								plugin.show();
								closeSelect();
								openedSelect = plugin;
							}
						}, 200);
					})
					.off('keypress.show').on('keypress.show', function(event) {
						if(that.selectEl.prop('disabled') || that.hasClass('disabled')){
							return;
						}

						// allow the spacebar to open the menu
						var code = event.keycode || event.which;
						switch(code) {
							case key.SPACE:
								event.preventDefault();
								that.simulatorTimer = setTimeout(function(){
									if(!that.hasClass('active')){
										plugin.show();
										closeSelect();
										openedSelect = plugin;
									}
								}, 200);
								break;
						}
					})
					.off('keydown.preventSubmit').on('keydown.preventSubmit', function(event) {

						// prevent form submit when the custom select is focussed but closed
						var code = event.keycode || event.which;
						switch(code) {
							case key.ENTER:
								event.preventDefault();
								break;
						}
					});
					/*
					.off('click.show').on('click.show', function() {
						// Removed in WCAG update as this opens then closes the menu on click, presumably because the focus moves directly off the combobox
						that.focusoutTimer = setTimeout(function(){
							plugin.hide();
						}, 200);
					});
					*/
			// }

			that.selectEl.css('visibility', 'hidden');
			//that.selectEl.attr('aria-hidden', true); // test for JAWS, did't fix anything

			that.arrow.off('click.show').on('click.show', function(){
				that.trigger('focusin.show');
				return false;
			});

			// the original select, which is replaced by the custom select
			that.selectEl.off('keypress.select').on('keypress.select', function(event) {
				var code = event.keycode || event.which;
				switch(code) {
					case key.ENTER:
						event.preventDefault();
						plugin.show();
						returnFalse = true;
						break;
				}
			});

			if(plugin.inPopup.length) {
				plugin.inPopup.off('scroll.inPopup' + that.idPlugin).on('scroll.inPopup' + that.idPlugin, function() {
					plugin.hide();
				});
			}
		},
		_createTemplate: function() {
			var plugin = this,
				that = plugin.element;
			that.items = that.selectEl.children();
			that.itemsLength = that.items.length;
			that.wrapperOption.empty().css('margin-top', '');
			that.scrollContainer.css('height', 0);
			that.scrollBarWrapper.css('height', 0);
			that.scrollBarBtn.css('top', '');
			var items  = [];
			if(!that.items.length){
				that.customText.text('');
			}
			that.items.each(function(i) {
				var self = $(this),
						optionClass = self.attr('class') || '';
				var item = '<li role="option" data-value="' + self.val() + '" id="' + that.idPlugin + '-option-' + i + '" class="' + (self.is(':selected') ? 'active' + optionClass : optionClass) + '">' + self.text() + '</li>';
				items.push(item);
				if(self.is(':selected')){
					that.customText.text(self.text());
				}
				// item.outerHeight(plugin.options.heightItem);
				// item.off('click.select').on('click.select', function(){
				// 	that.curItem = item;
				// 	that.customText.text(self.text());
				// 	that.selectEl.prop('selectedIndex', idx);
				// 	item.siblings('.active').removeClass('active');
				// 	item.addClass('active');
				// 	if(plugin.options.afterSelect){
				// 		plugin.options.afterSelect.call(that, that.selectEl, self, idx);
				// 	}
				// 	that.trigger('afterSelect');
				// });
			});
			if(that.items.length <= plugin.options.itemsShow){
				that.scrollContainer.height(that.items.length*plugin.options.heightItem);
				that.scrollBarWrapper.height(that.items.length*plugin.options.heightItem);
			}
			else{
				that.scrollContainer.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
				that.scrollBarWrapper.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
			}
			// when a mouse user clicks a menu item
			that.wrapperOption.html(items.join(''));
			that.wrapperOption.undelegate('.select')
				.delegate('[data-value]','click.select', function(){
					// e.stopPropagation();
					that.curItem = $(this);
					if(that.curIndex === that.curItem.index()){
						return;
					}
					that.curIndex = that.curItem.index();
					that.customText.text(that.items.eq(that.curIndex).text());
					that.selectEl.prop('selectedIndex', that.curIndex);

					that.curItem.siblings('.active').removeClass('active').attr('aria-selected', false);
					that.curItem.addClass('active');
					plugin.setAriaSelected( that.curItem );

					if(plugin.options.afterSelect){
						plugin.options.afterSelect.call(that, that.selectEl, that.items.eq(that.curIndex), that.curIndex);
						if(void 0 !== that.selectEl.closest('form').data('validator')){
							that.selectEl.valid();
						}
					}
					that.trigger('afterSelect', that.curItem.data('value'));
					that.selectEl.trigger('change');
					that.selectEl.trigger('blur');
					// plugin.hide();
				})
				.delegate('[data-value]', 'mousedown.select', function() {
					setTimeout(function() {
						clearTimeout(that.focusoutTimer);
					}, 200);
				});
			that.scroll = that.scroll.appendTo(body);
			if(that.selectEl.closest('.popup').length){
				that.scroll.css({
					'z-index': '1004'
				});
			}
		},
		refresh: function () {
			var plugin = this,
				that = plugin.element;
			that.customText.text(that.selectEl.find(':selected').text());
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(that.selectEl.prop('selectedIndex')).addClass('active').attr('aria-selected', true);
			that.curIndex = that.selectEl.prop('selectedIndex');
			if($.trim(that.selectEl.find(':selected').val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		setFocus: function($element) {
			var plugin = this,
				that = plugin.element;

			// save the scroll position
			var y = $(window).scrollTop();

			if ( ! $element.is(':focusable') ) {
				$element.attr('tabindex', -1);
			}

			$element.addClass('keyboardfocus').focus();

			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				if ( $element.is( that.simulatorCombobox ) ) {
					$(that).addClass('focus');
				}
			}

			// revert the scroll position
			window.scrollTo( 0, y );
		},
		setAriaSelected: function($selected) {
			var plugin = this,
					that = plugin.element;

			// prevent errors when parts of this compound widget are used in other compound widgets
			if(typeof that.simulatorCombobox !== 'undefined'){
				that.simulatorCombobox.val($selected.text());
				$selected.attr('aria-selected', true);
			}
		},
		show: function(){
			var plugin = this,
					that = plugin.element;
			if(that.is(':hidden')){
				return;
			}
			that.setDimension = function(){
				var top,
						maxW = 0;

				that.scroll.show();

				// prevent errors when parts of this compound widget are used in other compound widgets
				if ( typeof that.simulatorCombobox !== 'undefined' ) {
					that.simulatorCombobox.attr('aria-expanded', true);
				}

				maxW = that.outerWidth();

				var $option = that.scrollContainer.find('[role="option"]');
				var $optionSelected = $option.filter('.active');
				// set keyboardfocus to the .active option, or failing that, the first option
				if ( $optionSelected.length ) {
					plugin.setFocus( $optionSelected );
				}
				else {
					plugin.setFocus( $option.eq(0) );
				}

				// fix issue page mb-select-meals: dropdown width > select width
				// that.scroll.find('li').each(function() {
				// 	maxW = Math.max(maxW, this.offsetWidth);
				// });

				that.scroll.width(maxW - plugin.options.padding);
				if(that.items.length <= plugin.options.itemsShow){
					that.scrollContainer.width(maxW);
				}
				else{
					that.scrollContainer.width(maxW - plugin.options.padding - that.scrollBarWrapper.outerWidth(true));
				}

				if(!isIE() && plugin.inPopup.length && plugin.inPopup.css('position') === 'fixed'){
					top = that.offset().top - win.scrollTop() + that.outerHeight();
					if(that.offset().top + that.scroll.outerHeight(true) + that.outerHeight() > win.scrollTop() + win.height()){
						top = that.offset().top - that.scroll.outerHeight(true) - win.scrollTop();
					}
					that.scroll.css('position', 'fixed');
				}
				else{
					top = that.offset().top + that.outerHeight();
					if(top + that.scroll.outerHeight(true) > win.scrollTop() + win.height()) {
						top = that.offset().top - that.scroll.outerHeight(true);
					}
					that.scroll.css('position', 'absolute');
				}

				that.scroll.css({
					'z-index': '1000',
					top: top,
					left: that.offset().left
				});
				if(that.selectEl.closest('.popup').length){
					that.scroll.css({
						'z-index': '1004'
					});
				}

				if(plugin.options.onOpen){
					plugin.options.onOpen(that.selectEl);
				}
			};
			var clearResize = null;
			that.setDimension();

			that.addClass('active focus');
			win.off('resize.kCustomSelect').on('resize.kCustomSelect', function(){
				clearTimeout(clearResize);
				clearResize = setTimeout(function(){
					that.setDimension();
				}, 300);
			});
			plugin._initCustomScroll();
			that.wrapperOptionHeight = that.wrapperOption.outerHeight(true);
			$(document).off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
				if (!plugin.options.preventClose) {
					plugin.hide();
				}
			});

			if(that.closest('.popup__inner').length) {
				that.closest('.popup__inner').off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
					plugin.hide();
				});
			}
		},
		hide: function(){
			var plugin = this,
				that = plugin.element;
			if(plugin.options.onClose){
				plugin.options.onClose(that.selectEl);
			}
			that.scroll.hide();

			var $option = that.scrollContainer.find('[role="option"]');
			if ( $option.length ) {
				$option.removeClass('keyboardfocus');
			}
			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.attr('aria-expanded', false);

				if ( that.hasClass('focus') ) {
					plugin.setFocus( that.simulatorCombobox );
				}
			}

			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.removeClass('active');
			}
			else {
				that.removeClass('active focus');
			}

			if(that.curItem.length){
				if(that.items.eq(that.curItem.index()).val()){
					that.removeClass('default');
				}
				else{
					that.addClass('default');
				}
			}

			win.off('resize.kCustomSelect');
			openedSelect = null;
			doc.off('mousemove.scrollBar'+ that.idPlugin);
			doc.off('mouseup.scrollBar'+ that.idPlugin);
			doc.off('keydown.scrollBar'+ that.idPlugin);
			doc.off('click.hide' + that.idPlugin);
			that.off('mousewheel.scrollBar');
			that.scroll.off('mousewheel.scrollBar');
		},
		_initCustomScroll: function(){
			var plugin = this,
				that = plugin.element;
			that.draggable = false;
			that.enableScrollBar = true;
			that.ratioOfContenToWrapper = that.scrollContainer.height()/that.wrapperOption.height();
			that.scrollBarBtn.height(that.scrollBarWrapper.height()*that.ratioOfContenToWrapper);
			that.ratioOfContenToScroll = that.scrollContainer.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.ratioOfWrapperToScroll = that.wrapperOption.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.firstY = 0;
			that.scrollBarBtnPos = 0;
			that.hiddenScroll = false;
			// that.scrollBarBtn.css('top', '');
			if (that.scrollContainer.height() >= that.wrapperOption.height() - 3) {
				that.scrollBarWrapper.hide();
				that.hiddenScroll = true;
			}
			else{
				if(that.scrollBarWrapper.is(':hidden')){
					that.scrollBarWrapper.show();
					that.hiddenScroll = false;
				}
			}

			var scroll = function (e, a) {
				//if(!that.enableScrollBar) return;
				var top = that.scrollBarBtn.position().top,
				preventDefault = true;
				if(e.keyCode === 40 || a < 0) {
					top += that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else if (e.keyCode === 38 || a > 0) {
					top -= that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else {
					preventDefault = true;
				}
				if (top <= 0){
					top = 0;
				}
				if (top + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.height()){
					top = that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, top);
				return preventDefault;
			};

			that.scrollBarBtn.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {
				that.draggable = true;
				that.firstY = e.pageY;
				that.scrollBarBtnPos = that.scrollBarBtn.position().top;
				return false;
			});

			that.scrollBarBtn.off('click.scrollBar').on('click.scrollBar', function (e) {
				e.stopPropagation();
			});

			that.scrollBarWrapper.off('click.scrollBar').on('click.scrollBar', function (e) {
				e.stopPropagation();
				clearTimeout(that.focusoutTimer);
				var a = (e.pageY - that.scrollBarWrapper.offset().top) - that.scrollBarBtn.outerHeight()/2;
				if (a <= 0){
					a = 0;
				}
				if (a + that.scrollBarBtn.outerHeight()/2 >= that.scrollBarWrapper.outerHeight()){
					a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, a);
			});

			// Fix bugs closing the custom select when holded mouse on the scrollbar.
			that.scrollBarWrapper.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {
				e.preventDefault();
			});

			that.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});

			that.scroll.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});

			doc.off('mousemove.scrollBar'+ that.idPlugin).on('mousemove.scrollBar'+ that.idPlugin, function(e){
				if (that.draggable) {
					var a = that.scrollBarBtnPos + (e.pageY - that.firstY);
					if (a <= 0){
						a = 0;
					}
					if (a + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.outerHeight()){
						a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
					}
					scrollWithADistance(that, a);
				}
				return false;
			});

			doc.off('mouseup.scrollBar'+ that.idPlugin).on('mouseup.scrollBar'+ that.idPlugin, function(){
				that.draggable = false;
				return false;
			});

			// this seems to be used when entering digts
			doc.off('keydown.scrollBar'+ that.idPlugin).on('keydown.scrollBar'+ that.idPlugin, function(e){
				var cur, next, prev, curPos, minus, character, isStartedWithSelectedCharacter, collection;

				// when the menu is open and the custom scrollbar is required, the scrollbar and list are navigable with the arrow keys
				if(!that.hiddenScroll){
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.keyboardfocus');
							next = cur.removeClass('keyboardfocus').removeAttr('tabindex').next();

							if(next.length){
								curPos = that.scrollBarBtn.position().top*(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll);
								minus = next.index()*next.outerHeight(true) + next.outerHeight(true) - that.scrollContainer.height() ;
								if(minus  > curPos){
									scrollWithY(that, Math.max(0, minus));
								}
							}
							else{
								next = that.scrollContainer.find('li:first');
								scrollWithY(that, next.index()*next.outerHeight(true), plugin.options.itemsShow);
							}

							plugin.setFocus( next );
							returnFalse = true;

							break;
						case key.UP:
							cur = that.scrollContainer.find('li.keyboardfocus');
							prev = cur.removeClass('keyboardfocus').removeAttr('tabindex').prev();

							if(prev.length){
								curPos = that.scrollBarBtn.position().top * (that.ratioOfWrapperToScroll - that.ratioOfContenToScroll) - that.scrollContainer.height();
								minus = prev.index()*prev.outerHeight(true) - that.scrollContainer.height();
								if(minus <= curPos){
									scrollWithY(that, prev.index()*prev.outerHeight(true));
								}
							}
							else{
								prev = that.scrollContainer.find('li:last');
								scrollWithY(that, prev.index()*prev.outerHeight(true), plugin.options.itemsShow);
							}

							plugin.setFocus( prev );
							returnFalse = true;

							break;
						case key.ENTER:
							e.preventDefault(); // don't submit the form

							cur = that.scrollContainer.find('li.keyboardfocus').addClass('active');

							plugin.setAriaSelected( cur );

							cur.trigger('click.select'); // save the .active value to the real select
							plugin.hide();

							returnFalse = true;

							break;
						case key.ESC:
						case key.TAB:
							// allow ESC to be used by screen reader to switch modes
							// allow TAB to move to next/previous focusable element
							plugin.hide();
							break;
						case key.SPACE:
							e.preventDefault(); // don't enter a space
							plugin.hide();
							break;
						default:
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.keyboardfocus');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}

								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return $(this).text()[0].toLowerCase() === character && $(this).data('value') !== '';
								});

								if(!collection.length) {
									break;
								}

								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('keyboardfocus');
									cur = collection.first();
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('keyboardfocus');
										cur = collection.eq(collection.index(cur) + 1);
									}
									else {
										cur.removeClass('keyboardfocus');
										cur = collection.first();
									}
								}

								scrollWithY(that, cur.index() * cur.outerHeight(true), plugin.options.itemsShow);

								plugin.setFocus( cur );
							}
							break;
					}
				}
				else{
					// the custom scrollbar is not required and therefore hidden
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.keyboardfocus');
							next = cur.removeClass('keyboardfocus').next();
							if(!next.length){
								next = that.scrollContainer.find('li:first');
							}

							plugin.setFocus( next );

							returnFalse = true;

							break;
						case key.UP:
							cur = that.scrollContainer.find('li.keyboardfocus'); // else we can only navigate one spot away
							prev = cur.removeClass('keyboardfocus').prev();

							if(!prev.length){
								prev = that.scrollContainer.find('li:last');
							}

							plugin.setFocus( prev );

							returnFalse = true;

							break;
						case key.ENTER:
							cur = that.scrollContainer.find('li.keyboardfocus').addClass('active');

							plugin.setAriaSelected( cur );

							cur.trigger('click.select');
							plugin.hide();
							returnFalse = true;

							break;
						case key.ESC:
						case key.TAB:
							// allow tab to have default action of moving to next/previous focusable element
							plugin.hide();

							break;
						case key.SPACE:
							e.preventDefault();

							plugin.hide();

							break;
						default:
							// jump to list option by typing first letter
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.keyboardfocus');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}
								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return ($(this).text()[0].toLowerCase() === character && $(this).data('value') !== '');
								});
								if(!collection.length) {
									break;
								}
								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('keyboardfocus');
									cur = collection.first();
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('keyboardfocus');
										cur = collection.eq(collection.index(cur) + 1);
									}
									else {
										cur.removeClass('keyboardfocus');
										cur = collection.first();
									}
								}

								plugin.setFocus( cur );
							}
							break;
					}
				}

				if(returnFalse){
					return false;
				}
				//return scroll(e);
			});

			that.setDimension();

			var curActive = that.scrollContainer.find('li.active');

			scrollWithY(that, Math.max(0, curActive.index()*curActive.outerHeight(true)));
			// doc.off('mousemove.scrollBar'+ that.idPlugin +' mouseup.scrollBar'+ that.idPlugin +' keydown.scrollBar'+ that.idPlugin).on({
			// 	'mousemove.scrollBar' : function (e) {
			// 	},
			// 	'mouseup.scrollBar' : function () {
			// 		that.draggable = false;
			// 		return false;
			// 	},
			// 	'keydown.scrollBar' : function (e) {
			// 	}
			// });
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		},
		enable: function() {
			var that = this.element;
			that.removeClass('disabled');

			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.removeAttr('aria-disabled');

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {
					SIA.WcagGlobal.attachDescription('#' + that.idPlugin + '-customSelectUsage');
				}
			}
		},
		disable: function() {
			var that = this.element;
			that.addClass('disabled');

			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.attr('aria-disabled', true);

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {
					SIA.WcagGlobal.detachDescription('#' + that.idPlugin + '-customSelectUsage');
				}
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				/* jshint -W030 */
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		selectEl: '> select',
		customText: '> span.select__text',
		arrow: '> a',
		itemsShow: 4,
		scrollWith : 2,
		heightItem: 43,
		padding: 3,
		preventClose: false,
		onOpen: function(){
			$('.custom-scroll:visible').attr({
				'open' : 'true' // proprietary
			});

			// legacy ARIA
			// $('.custom-scroll:visible').attr({
			// 	'role':'applicatiopn'
			// });
			// $('.custom-scroll:visible').find('ul').attr({
			// 	'role':'navigation',
			// 	'tabindex': '10'
			// });
			// $('.custom-scroll:visible').find('ul').children().attr({
			// 	'role':'button',
			// 	'tabindex': '-1'
			// });
			//$('.custom-scroll:visible').find('ul').children().eq(0).focus();

			var _scrollContainer = $('.custom-scroll:visible');

			if ( _scrollContainer.find('[role="option"].active').length ) {
				_scrollContainer.find('[role="option"].active').attr('tabindex', -1).focus();
			}

		},

		afterSelect: function(){},
		onClose: function(){}
	};

}(jQuery, window));

/**
 * @name SIA
 * @description Define global highlightInput functions
 * @version 1.0
 */
SIA.highlightInput = function(){
	var doc = SIA.global.vars.doc;
	var el = null;

	// fix issue autocomplete when clear the text and focus other input, the autocomplete doesn't close
	doc.off('focus.highlightInput').on('focus.highlightInput', 'input, select', function(e) {
		if (el && el.length && el.closest('[data-autocomplete]').length && el.get(0) !== e.target) {
			setTimeout(function() {
				if(el.data('uiAutocomplete')){
					el.autocomplete('close');
					el = $(e.target);
				}
			}, 500);
		}
		else {
			el = $(e.target);
		}
	});

	var ip = $('.input-3,.input-1,.textarea-1,.textarea-2').not('.disabled').filter(function(idx, el){
		if(!$(el).closest('[data-return-flight]').length){
			return el;
		}
	});

	ip.off('focusin.highlightInput').on('focusin.highlightInput', function(){
		var self = $(this);
		if (!self.hasClass('disabled')) {
			self.addClass('focus');
			self.removeClass('default');
		}
	}).off('focusout.highlightInput').on('focusout.highlightInput', function(){
		var self = $(this);
		var child = self.children();
		self.removeClass('focus');
		// fix issue for alphanumeric if there is no rule-required
		if(child.data('rule-alphanumeric') && !child.data('rule-required')){
			self.closest('.grid-col').removeClass('error').find('.text-error').remove();
		}
	}).find('input').off('change.removeClassDefault').on('change.removeClassDefault', function(){
		var self = $(this);
		if(!self.val() || self.val() === self.attr('placeholder')){
			self.addClass('default');
		}
		else{
			self.removeClass('default');
		}
	}).trigger('change.removeClassDefault');
};

'use strict';
/**
 * @name SIA
 * @description Define global initCustomSelect functions
 * @version 1.0
 */
SIA.initCustomSelect = function(){
	if($('.passenger-details-page').length){
		return;
	}
	var win = SIA.global.vars.win;
	$('[data-customselect]').each(function(){
		var self = $(this);
		if(!self.data('customSelect')){
			self.customSelect({
				itemsShow: 5,
				heightItem: 43,
				scrollWith: 2,
				afterSelect: function(el, item, idx){
					if(el.is('#economy-5')){
						var outerWraper = el.closest('.form-group');
						var byRoute = outerWraper.siblings('.by-route');
						var byFlightNo = outerWraper.siblings('.by-flight-no');
						if(el.prop('selectedIndex')){
							byRoute.removeClass('hidden');
							byFlightNo.addClass('hidden');
						}
						else{
							byRoute.addClass('hidden');
							byFlightNo.removeClass('hidden');
						}
						win.trigger('resize.resetTabMenu');
					}
					if(el.closest('[data-fare-deal]').length){
						$('#fare-deals').children('.manualFareDeals').hide().eq(idx).show();
					}
				}
			});
		}
	});
};

/**
 * @name SIA
 * @description Define global initConfirmCheckbox functions
 * @version 1.0
 */
SIA.initConfirmCheckbox = function() {
	var wrapperFieldConfirm = $('[data-confirm-tc]');
	var btnSubmit = $('[data-button-submit]');

	if(wrapperFieldConfirm.length) {
		var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

		confirmCheckbox.off('change.confirm').on('change.confirm', function(e) {
			e.preventDefault();
			var btn = btnSubmit.length ? btnSubmit : confirmCheckbox.closest('.form-group-full').next().find(':submit');
			if(btn.length) {
				if(confirmCheckbox.is(':checked')) {
					btn.removeClass('disabled').prop('disabled', false);
				}
				else {
					btn.addClass('disabled').prop('disabled', true);
				}
			}
		}).trigger('change.confirm');
	}
};

/**
 * @name SIA
 * @description Define global initLangToolbar functions
 * @version 1.0
 */
SIA.initLangToolbar = function() {
	var body = SIA.global.vars.body;

	if(typeof(Storage) !== 'undefined' && !body.hasClass('popup-window-login-page') && !body.hasClass('popup-window-logout-page')) {
		// if(JSON.parse(localStorage.getItem('translateTo'))) {
		// condition alway to return true value
		if(localStorage.getItem('translateTo')) {
			return;
		}

		var global = SIA.global;
		var body = global.vars.body;
		// var container = $('#container');

		var hideTbar = function(tpl) {
			tpl.css('margin-top', -tpl.height());
			// container.css('padding-top', 0);
			body.removeClass('translateOn').off('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd').on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function() {
				body.removeClass('transition-all');
				tpl.remove();
			});
		};

		var showTBar = function(tpl) {
			var delayTime = 200;
			var tBarHeight;
			var skipToContent = $('.skip-to-content');
			//body.prepend(tpl);
			if(skipToContent.length>0){
				$(tpl).insertAfter('.skip-to-content');
			}else{
				body.prepend(tpl);
			}
			tBarHeight = tpl.height();
			tpl.css('margin-top', -tBarHeight);
			if($('.at-a-glance-page').length){
				delayTime += 800;
			}

			setTimeout(function() {
				// container.css('padding-top', tBarHeight);
				body.addClass('transition-all');
				tpl.removeAttr('style');
				body.addClass('translateOn');
			}, delayTime);
		};

		if(!body.is('.interstitial-page')) {
			$.get(global.config.url.langToolbarTemplate, function(templateStr) {
				var tpl = $(templateStr);
				tpl.find('.toolbar__close').off('click.closeToolbar').on('click.closeToolbar', function(e) {
					e.preventDefault();
					hideTbar(tpl);
				});
				tpl.find('[data-lang-toolbar]').off('click.translateLang').on('click.translateLang', function(e) {
					e.stopPropagation();
					hideTbar(tpl);
					localStorage.setItem('translateTo', $(this).data('langToolbar'));
				});
				// global.vars.win.off('resize.langToolbar').on('resize.langToolbar', function() {
				// 	if(tpl.length) {
				// 		container.css('padding-top', tpl.height());
				// 	}
				// });
				showTBar(tpl);
			}, 'html');
		}
	}
};

/**
 * @name SIA
 * @description Define global fixPlaceholder functions
 * @version 1.0
 */
SIA.fixPlaceholder = function(){
	// creating a fake placeholder for not supporting placeholder
	var placeholderInput = $('[placeholder]');
	if(!window.Modernizr.input.placeholder){
		placeholderInput.placeholder();
	}
};

/**
 * @name SIA
 * @description Define global initSubMenu functions
 * @version 1.0
 */
SIA.initSubMenu = function() {
	var subMenu = $('[data-device-submenu]');

	if(subMenu.length) {
		subMenu.find('option[selected]').prop('selected', true);
		subMenu.off('change.submenu').on('change.submenu', function() {
			var url = $(this).find('option:selected').data('url');
			if(url) {
				window.location.href = url;
			}
		});
	}
};

/**
 * @name SIA
 * @description Define global initToggleButtonValue functions
 * @version 1.0
 */
SIA.initToggleButtonValue = function() {
	var buttonToggle = $('[data-toggle-value]');
	buttonToggle.off('click.toggleLabel').on('click.toggleLabel',function() {
		var toggleValue = $(this).data('toggle-value');
		var currentValue = $(this).is('input') ? $(this).val() : $(this).html();
		var slideItem = $(this).closest('.slide-item');
		var index = slideItem.attr('index');
		$(this).data('toggle-value', currentValue);
		if($(this).is('input')) {
			$(this).val(toggleValue);
		}
		else {
			$(this).html(toggleValue);
		}
		if(slideItem.length){
			slideItem.siblings('[index="'+ index +'"]').find('[data-toggle-value]').html(toggleValue);
		}
	});
};

/**
 * @name SIA
 * @description Define global LoggedProfilePopup functions
 * @version 1.0
 */
SIA.LoggedProfilePopup = (function(){
	var global = SIA.global;
	var vars = global.vars;
	var config = global.config;
	var win = vars.win;
	var body = vars.body;
	var mainMenu = vars.mainMenu;
	var menuT = vars.menuT;
	var menuBar = vars.menuBar;
	var popupLoggedProfile = vars.popupLoggedProfile;
	var loggedProfileSubmenu = vars.loggedProfileSubmenu;

	popupLoggedProfile = loggedProfileSubmenu.clone().appendTo(document.body);
	loggedProfileSubmenu.removeClass().addClass('menu-sub hidden');
	popupLoggedProfile.removeClass('menu-sub');
	if(popupLoggedProfile.length){
		var triggerLoggedProfile = menuBar.find('ul a.status');
		var timerLoggedProfileResize = null;
		var realSub = loggedProfileSubmenu;
		var popupLoggedProfilePosition = function(){
			popupLoggedProfile.removeClass('hidden');
			var leftPopupLoggedProfile = triggerLoggedProfile.offset().left - popupLoggedProfile.outerWidth()/2 + triggerLoggedProfile.outerWidth()/2 - 30;
			if(leftPopupLoggedProfile + popupLoggedProfile.outerWidth() > win.width()){
				leftPopupLoggedProfile = leftPopupLoggedProfile - (leftPopupLoggedProfile + popupLoggedProfile.outerWidth() - win.width());
			}
			popupLoggedProfile.css({
				left: leftPopupLoggedProfile,
				top: triggerLoggedProfile.offset().top + triggerLoggedProfile.children().innerHeight() + (vars.isIE() ? 33 : 27)
			});
			popupLoggedProfile.find('.popup__arrow').css({
				left : Math.round(triggerLoggedProfile.offset().left - leftPopupLoggedProfile + 21)
			});
		};

		triggerLoggedProfile.off('click.showLoggedProfile').on('click.showLoggedProfile', function(e){
			e.preventDefault();
			e.stopPropagation();
			var winInnerW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			//hide all custom-select
			$('[data-customselect]').customSelect('hide');
			if(winInnerW >= config.tablet/* - config.width.docScroll*/){
				vars.detectHidePopup();
				vars.hidePopupSearch(true);
				var self = $(this);
				if(popupLoggedProfile.hasClass('hidden')){
					self.addClass('active');
					popupLoggedProfilePosition();
					if(window.Modernizr.cssanimations){
						popupLoggedProfile.addClass('animated fadeIn');
						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body).addClass('animated fadeInOverlay').show();
						popupLoggedProfile.triggerPopup = self;
					}
					else{
						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body);
						popupLoggedProfile.overlay.fadeIn(config.duration.overlay);
						popupLoggedProfile.triggerPopup = self;
						popupLoggedProfile.hide().fadeIn(config.duration.popupSearch);
					}
					win.off('resize.LoggedProfile').on('resize.LoggedProfile', function(){
						clearTimeout(timerLoggedProfileResize);
						timerLoggedProfileResize = setTimeout(function(){
							popupLoggedProfilePosition();
							if(win.width() < config.tablet - config.width.docScroll){
								self.removeClass('active');
								popupLoggedProfile.addClass('hidden');
								popupLoggedProfile.overlay.remove();
								win.off('resize.LoggedProfile');
							}
						}, 100);
					}).trigger('resize.LoggedProfile');
					popupLoggedProfile.overlay.off('click.closeLoggedProfile').on('click.closeLoggedProfile', function(e){
						e.preventDefault();
						e.stopPropagation();
						triggerLoggedProfile.trigger('click.showLoggedProfile');
					});
				}
				else{
					self.removeClass('active');
					if(window.Modernizr.cssanimations){
						popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
						popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
							popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
							popupLoggedProfile.overlay.remove();
							popupLoggedProfile.overlay = $();
							popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{
						popupLoggedProfile.fadeOut(config.duration.popupSearch, function(){
							popupLoggedProfile.addClass('hidden');
						});
						popupLoggedProfile.overlay.fadeOut(config.duration.overlay, function(){
							popupLoggedProfile.overlay.remove();
							popupLoggedProfile.overlay = $();
						});
					}
					win.off('resize.LoggedProfile');
				}
			}
			else {
				// Mobile
				var currentX; //use percent as unit
				var targetX;
				var spaceToMove;
				var interval;
				var animateInterval;
				var menuSub = menuT.find('.logged-in .menu-sub');
				var langToolbarEl = $('.toolbar--language'),
						langToolbarH = 0;
				if(langToolbarEl.length) {
					langToolbarH = langToolbarEl.height();
				}
				if(!menuSub.data('onceIncreaseTop')) {
					menuSub.data('onceIncreaseTop', true).css('top', -menuBar.find('.logged-in').offset().top);
				}
				menuT.find('.menu-inner').css('overflowY', 'hidden');
				if(realSub.hasClass('hidden')){
					if(window.Modernizr.cssanimations) {
						realSub.removeClass('hidden');
						mainMenu.addClass('active');
						mainMenu.parent().scrollTop(0);
						// realSub.css('top', - mainMenu.height());
						menuBar.addClass('active');
					}
					else {
						//No CSS3 animation
						currentX = 0; //use percent as unit
						targetX = -109;
						spaceToMove = 10;
						interval = 20;
						animateInterval = window.setInterval(function() {
							if(currentX <= targetX) {
								window.clearInterval(animateInterval);
								//
								mainMenu.children('ul').add(menuBar).css({
									'transform': '',
									'-moz-transform': '',
									'-webkite-transform': '',
									'-ms-transform': '',
									'-o-transform': ''
								});
								//menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
								realSub.removeClass('hidden');
								mainMenu.addClass('active');
								menuBar.addClass('active');
							}
							else {
								currentX -= spaceToMove;
								mainMenu.children('ul').add(menuBar).css({
									'transform': 'translateX(' + currentX + '%)',
									'-moz-transform': 'translateX(' + currentX + '%)',
									'-webkite-transform': 'translateX(' + currentX + '%)',
									'-ms-transform': 'translateX(' + currentX + '%)',
									'-o-transform': 'translateX(' + currentX + '%)'
								});
							}
						}, interval);
					}
					menuSub.css('top', 18 + langToolbarH - menuBar.find('.logged-in').offset().top);
				}
				else{
					realSub.addClass('hidden');
					mainMenu.removeClass('active').removeClass('hidden');
					menuBar.removeClass('active');
				}
			}
			SIA.WcagGlobal.assignAttributes();
		});

		realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
			e.preventDefault();
			menuT.find('.menu-inner').css('overflowY', 'auto');
			if (window.Modernizr.cssanimations) {
				setTimeout(function(){
					realSub.addClass('hidden');
					realSub.closest('.menu-item').removeClass('active');
				},500);
				mainMenu.removeClass('active');
				menuBar.removeClass('active');
			}
			else {
				var currentX = -109; //use percent as unit
				var targetX = 0;
				var spaceToMove = 10;
				var interval = 20;
				var animateInterval = window.setInterval(function() {
					if(currentX >= targetX) {
						window.clearInterval(animateInterval);
						//
						mainMenu.children('ul').add(menuBar).css({
							'transform': '',
							'-moz-transform': '',
							'-webkite-transform': '',
							'-ms-transform': '',
							'-o-transform': ''
						});
						//setTimeout(function(){
						realSub.addClass('hidden');
						realSub.closest('.menu-item').removeClass('active');
						//},500);
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
					}
					else {
						currentX += spaceToMove;
						mainMenu.children('ul').add(menuBar).css({
							'transform': 'translateX(' + currentX + '%)',
							'-moz-transform': 'translateX(' + currentX + '%)',
							'-webkite-transform': 'translateX(' + currentX + '%)',
							'-ms-transform': 'translateX(' + currentX + '%)',
							'-o-transform': 'translateX(' + currentX + '%)'
						});
					}
				}, interval);
			}
		});

		vars.popupLoggedProfile = popupLoggedProfile;
	}
})();

/**
 *  @name Popup
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'Popup';
	var win = $(window);
	var body = $(document.body);
	var prevFocusingItem;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	function onIpad() {
		// var userAgent = navigator.userAgent.toLowerCase();
		// var isIOS = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || ('ontouchstart' in window);
		var mobile = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
		if (!mobile) {
			return false;
		} else {
			return true;
		}
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
					that = plugin.element;
			that.options = plugin.options;
			that.modal = that.attr('data-modal-popup') ? $(that.attr('data-modal-popup')) : that;
			that.modal = that.modal.appendTo(body);
			that.detectIpad = onIpad();
			that.isShow = false;
			that.isAnimate = false;
			that.timer = null;
			that.freezeBody = false;
			that.heightHolderContainer = 0;
			that.isTablet = false;
			that.isMobile = false;
			that.isIpad = /iPad/i.test(window.navigator.userAgent);
			that.scrollTopHolder = 0;

			that.modal.addClass('hidden');

		},
		freeze: function(){
			var plugin = this,
					that = plugin.element,
					h = win.height() + that.scrollTopHolder,
					langToolbarEl = $('.toolbar--language');

			if(langToolbarEl.length) {
				var langH = langToolbarEl.height();
				h -= langH;
				if(win.scrollTop() > langH) {
					// langToolbarEl.addClass('hidden');
					body.removeClass('transition-all').removeClass('translateOn');
					// langToolbarEl.css('marginTop', -langH);
				}
			}
			// detect not remove style for container when exits popup
			if(!that.data('parentContainerStyle')) {
				$(plugin.options.container).css({
					// 'marginTop': - that.scrollTopHolder,
					'height': h,
					// 'overflow': 'hidden'
				});
				body.addClass('no-flow');
			}
		},
		showTablet: function() {
			var plugin = this,
					that = plugin.element;
			plugin.reposition();
			plugin.freeze();
			that.modal.removeClass('hidden').addClass('animated fadeIn').css('zIndex', (that.options.zIndex + 1));
			that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
					win.scrollTop(0);
				}
				if(that.options.afterShow){
					that.options.afterShow(that);
				}
				plugin.freeze();
				that.trigger('afterShow');
				that.isAnimate = false;
				that.isTablet = true;
				plugin.addResize();
				that.modal.removeClass('fadeIn');
				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		},
		reset: function(){
			var plugin = this,
				that = plugin.element;
			if(that.isTablet){
				that.modal.css('height', '');
				that.modal.find('.popup__content').removeClass('popup__scrolling').css('height', '');
				if (that.modal/*.find('.popup__content')*/.outerHeight(true) > win.height()) {
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
				}
				else{
					that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
				}
				if(that.isShow){
					plugin.freeze();
				}
			}
		},
		show: function() {
			var plugin = this,
					that = plugin.element;
			if(that.isShow){
				return;
			}
			that.trigger('beforeShow');
			if(that.options.beforeShow){
				that.options.beforeShow(that);
			}
			prevFocusingItem = document.activeElement;
			that.isShow = true;
			that.isAnimate = true;
			that.overlay = $(that.options.overlayBGTemplate).appendTo(body);
			that.scrollTopHolder = win.scrollTop();
			if(window.Modernizr.csstransitions){
				that.overlay.show().css({
					'zIndex': that.options.zIndex,
					opacity: 0.8
				}).addClass('animated fadeInOverlay');
				that.modal.removeClass('hidden');
				plugin.showTablet();
			}
			else{
				plugin.reposition();
				that.isTablet = true;
				plugin.freeze();
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
				}
				// if(window.isIE() && window.isIE() < 9){
				// 	that.overlay.css({
				// 		'zIndex': that.options.zIndex,
				// 		opacity: 0.5
				// 	}).show();
				// 	that.modal.addClass(that.options.modalShowClass).hide().show().css('zIndex', (that.options.zIndex + 1));
				// 	plugin.freeze();
				// 	if(that.options.afterShow){
				// 		that.options.afterShow(that);
				// 	}
				// 	that.trigger('afterShow');
				// 	that.isAnimate = false;
				// 	plugin.addResize();
				// }
				// else{
				// }
				that.overlay.css({
					'zIndex': that.options.zIndex
				}).fadeIn(plugin.options.duration);
				that.modal.removeClass('hidden').addClass(that.options.modalShowClass).fadeIn(plugin.options.duration, function(){
					plugin.freeze();
					// plugin.reposition();
					if(that.options.afterShow){
						that.options.afterShow(that);
					}
					that.trigger('afterShow');
					that.isAnimate = false;
					plugin.addResize();
				}).css('zIndex', (that.options.zIndex + 1));
			}

			that.modal.find(that.options.triggerCloseModal).on({
				'click.hideModal': function(e) {
					e.preventDefault();
					if(!that.isAnimate){
						plugin.hide();
					}
					if(that.find('[data-customselect]').length){
						$(document).trigger('click');
					}
				}
			});
			that.modal.off('click.doNothing').on('click.doNothing', function(){
				// fix for lumina 820 and 1020
			});
			if(that.options.closeViaOverlay){
				that.overlay.on({
					'click.hideModal': function(e) {
						e.preventDefault();
						if(!that.isAnimate){
							plugin.hide();
						}
					}
				});

				that.modal.on({'click.hideModal': function(e) {
						if(!that.isAnimate && !$(e.target).closest('.popup__inner').length){
							// e.preventDefault();
							plugin.hide();
						}
					}
				});
				// that.modal.find('.popup__inner').off('click.hideModal').on('click.hideModal', function(e) {
				// 	e.stopPropagation();
				// });
			}

			SIA.WcagGlobal.assignAttributes();
			SIA.WcagGlobal.ppMoveFocus();

			that.isShow = true;

		},
		addResize: function(){
			var plugin = this,
				that = plugin.element;
			// resize
			win.on({
				'resize.reposition': function() {
					if(that.timer){
						clearTimeout(that.timer);
					}
					that.timer = setTimeout(function(){
						plugin.reset();
						if(that.isShow){
							plugin.reposition();
						}
					}, that.options.duration + 100);
				},
				'keyup.escape': function(e) {
					if (e.which === 27) {
						plugin.hide();
						if(that.find('[data-customselect]').length){
							$(document).trigger('click');
						}
					}
				},
				'orientationchange.hide': function() {
					plugin.hide();
				}
			});
		},
		hide: function() {
			var plugin = this,
					that = plugin.element,
					langToolbarEl = $('.toolbar--language');

			if(that.overlay){
				that.isShow = false;
				that.trigger('beforeHide');
				that.modal.find(that.options.triggerCloseModal).off('click.hideModal');
				that.overlay.off('click.hideModal');
				if(langToolbarEl.length) {
					// langToolbarEl.removeClass('hidden');
					langToolbarEl.removeAttr('style');
					body.addClass('transition-all translateOn');
				}
				if(that.options.beforeHide){
					that.options.beforeHide(that);
				}
				if(window.Modernizr.csstransitions){
					that.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					that.modal.addClass('animated fadeOut');
					if(!that.data('parentContainerStyle')) {
						// $(plugin.options.container).removeAttr('style');
						$(plugin.options.container).css('height', '');
						body.removeClass('no-flow');
						// if(langToolbarEl.length) {
						// 	$(plugin.options.container).css('padding-top', langToolbarEl.height());
						// }
					}
					win.scrollTop(that.scrollTopHolder);
					win.off('resize.reposition keyup.escape');
					that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						that.modal.removeAttr('style').addClass('hidden').removeClass('animated fadeOut fadeIn slideInRight');
						that.scrollTopHolder = 0;
						that.overlay.off('click.hideModal').remove();
						that.overlay = null;

						that.isAnimate = false;
						that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
						if(that.options.afterHide){
							that.options.afterHide(that);
						}
						that.trigger('afterHide');
						that.modal.css('right', '');
						that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				}
				else{
					if(!that.data('parentContainerStyle')) {
						$(plugin.options.container).removeAttr('style');
						// if(langToolbarEl.length) {
						// 	$(plugin.options.container).css('padding-top', langToolbarEl.height());
						// }
					}
					win.off('resize.reposition keyup.escape');
					win.scrollTop(that.scrollTopHolder);
					that.scrollTopHolder = 0;
					// if(window.isIE() && window.isIE() < 9){
					// 	that.modal.removeClass(that.options.modalShowClass).hide();
					// 	that.modal.removeAttr('style').hide();
					// 	that.isAnimate = false;
					// 	that.modal.find('.popup__content').removeAttr('style');
					// 	if(that.options.afterHide){
					// 		that.options.afterHide(that);
					// 	}
					// 	that.trigger('afterHide');
					// 	that.overlay.hide().off('click.hideModal').remove();
					// }
					// else{
					// }
					that.modal.removeClass(that.options.modalShowClass).fadeOut(plugin.options.duration, function(){
						that.modal.removeAttr('style').addClass('hidden');
						that.isAnimate = false;
						that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
						if(that.options.afterHide){
							that.options.afterHide(that);
						}
						that.trigger('afterHide');
					});
					that.overlay.fadeOut(plugin.options.duration, function(){
						that.overlay.off('click.hideModal').remove();
						that.overlay = null;
					});
				}
			}
			prevFocusingItem.focus();
		},
		reposition: function() {
			var plugin = this,
					that = plugin.element,
					popupInner = that.modal.find('.popup__inner');
			that.modal.removeClass('hidden').css({
				position: (!that.detectIpad) ? ((win.height() > popupInner.height()) ? 'fixed' : 'fixed') : (win.width() < plugin.options.mobile) ? 'absolute' : 'fixed'
			});
			popupInner.css({
				top: win.height() > popupInner.height() ? Math.max(0, (win.height() - popupInner.height()) / 2) : 0,
				left: Math.max(0, (win.width() - that.modal.width()) / 2)
			});

			// that.modal.show().css({
			// 	// top: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0, (win.height() - that.modal.height()) / 2),
			// 	top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  0,
			// 	// top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  Math.max(0, (win.height() - that.modal.height()) / 2) + win.scrollTop(),
			// 	position : (!that.detectIpad) ? ((win.height() > that.modal.height()) ? 'fixed' : 'absolute') : 'absolute',
			// 	// left: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// 	left: Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// });
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		overlayBGTemplate: '<div class="bg-modal radial" id="overlay"></div>',
		modalShowClass: 'modal-show',
		triggerCloseModal: '.modal_close, a.btn-gray, [data-close]',
		duration: 500,
		container: '#container',
		mobile: 768,
		tablet: 988,
		zIndex: 20,
		closeViaOverlay: true,
		beforeShow: function() {},
		beforeHide: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global initSkipTabIndex functions
 * This function is created for accessibility
 * @version 1.0
 */

SIA.initSkipTabIndex = function(elm){
	if(elm){
		elm.find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
	}else{
		$('[data-skip-tabindex]').each(function(){
			$(this).find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
			$(this).removeAttr('data-skip-tabindex');
		});
	}
};

/**
 * @name SIA
 * @description Define social network functions
 * @version 1.0
 */
SIA.socialNetworkAction = function(){
	var global = SIA.global;
	var config = global.config;
	var social = $('[data-social]');
	var cocialSharing = function (element, link) {
		$(element).attr('href', link);
		$(element).attr('target', '_blank');
	};
	social.each(function(){
		var fb = $('[data-facebook]', this);
		var tw = $('[data-twitter]', this);
		var gplus = $('[data-gplus]', this);
		cocialSharing(fb, config.url.social.facebookSharing + window.location.href);
		cocialSharing(tw, config.url.social.twitterSharing + window.location.href);
		cocialSharing(gplus, config.url.social.gplusSharing + window.location.href);
	});
};

(function($, window, undefined) {
  var pluginName = 'stickyWidget',
      win = $(window),
      doc = $(document);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          el = that.element,
          elPos = el.offset().top,
          openStickyEl = el.find('[data-sticky-open]'),
          stickyBlock = el.find('.booking-widget-block'),
          closeStickyEl = el.find('.sticky__close');
      win.off('scroll.' + pluginName).on('scroll.' + pluginName,
        function(){
        if(win.scrollTop() >= elPos) {
          el.addClass('sticky');
          if(stickyBlock.is('.hidden')) {
            openStickyEl.removeClass('hidden');
          }
          el.find('input').blur();
          $('.ui-datepicker').hide();
        } else {
          openStickyEl.addClass('hidden');
          stickyBlock.addClass('hidden');
          closeStickyEl.css('display','none');
          el.removeClass('sticky');
        }
      })
      openStickyEl.off('click.openSticky').on('click.openSticky', function(){
          stickyBlock.slideDown('fast').removeClass('hidden');
          openStickyEl.addClass('hidden');
          closeStickyEl.css('display', 'block');
          stickyBlock.parents('[data-booking-widget]').focus();
      });
      closeStickyEl.off('click.closeSticky').on('click.closeSticky', function(){
          $.when(stickyBlock.slideUp('fast')).done(function(){
              openStickyEl.removeClass('hidden');
          });
          closeStickyEl.css('display', 'none');
      });
      closeStickyEl.off('keydown.closeSticky').on('keydown.closeSticky', function(e){
          e.preventDefault();
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 9 || keyCode === 13) {
          $(this).trigger('click.closeSticky');
        }
      });

      // doc.off('keydown.closeSticky').on('keydown.closeSticky', function(e){
      //     e.preventDefault();
      //   // var keyCode = e.keyCode || e.which || e.charCode;

      //   // if(keyCode === 9 || keyCode === 13) {
      //   //   $(this).trigger('click.closeSticky');
      //   // }
      //   console.log($(this));
      // });

      this.checkRadio();
    },

    checkRadio: function() {
      var that = this,
          el   = that.element,
          radioEl = el.find('input:radio'),
          target = el.find('[data-target]');
      radioEl.each(function(idx){
        var self = $(this);
        if(self.is(':checked')){
          if(idx){
            target.eq(1).removeClass('hidden');
            target.eq(0).addClass('hidden');
          }
          else{
            target.eq(0).removeClass('hidden');
            target.eq(1).addClass('hidden');
          }
        }
        self.off('change.stickyWidget').on('change.stickyWidget', function(){
          if(self.is(':checked')){
            if(idx){
              target.eq(1).removeClass('hidden');
              target.eq(0).addClass('hidden');
            }
            else{
              target.eq(0).removeClass('hidden');
              target.eq(1).addClass('hidden');
            }
          }
        });
      });
    },

    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
  };

  $(function() {
   $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));

(function($, window, undefined) {
  var pluginName = 'tabMenu';
  var win = $(window);
  var body = $(document.body);
  var html = $('html');

  var centerPopup = function(el, visible, scrollTopHolder){
    el.css({
      top: Math.max(0, (win.height() - el.innerHeight()) / 2) + scrollTopHolder,
      display: (visible ? 'block' : 'none'),
      left: Math.max(0, (win.width() - el.innerWidth()) / 2)
    });
  };

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.selectOnMobile = that.find(that.options.selectOnMobile);
      that.freezeBody = false;
      that.isIpad = /iPad/i.test(window.navigator.userAgent);
      that.scrollTopHolder = 0;

      var container = $(plugin.options.container);
      var tabContentOverlay = $();
      var tabMenuTimer = null;
      var isOpen = false;
      var isTablet = false;
      var onResize = null;
      var listTabs = that.find('.tab > .tab-item');
      // var isIE = function() {
      //  var myNav = navigator.userAgent.toLowerCase();
      //  return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
      // };

      if(that.options.isPopup){
        onResize = function(parent, self){
          centerPopup(parent, true, 0);
          parent.css('height', '');
          that.tabContent.eq(that.tab.index(self)).css('height', '');
          // container.css('height', '');
          if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) > win.height()){
            parent.find('.tab-content.active').css({
              height: win.height() - parseInt(that.tabContent.eq(that.tab.index(self)).css('padding-top')),
              'overflow-y': 'auto'
            });
            container.css({
              'marginTop': - that.scrollTopHolder,
              'height': win.height() + that.scrollTopHolder,
              'overflow': 'hidden'
            });
          }
          else{
            that.tabContent.eq(that.tab.index(self)).removeAttr('style');
            container.css({
              'marginTop': - that.scrollTopHolder,
              'height': win.height() + that.scrollTopHolder,
              'overflow': 'hidden'
            });
          }
          if(win.width() >= that.options.tablet - that.options.docScroll){
            tabContentOverlay.remove();
            parent.removeClass('animated fadeIn').removeAttr('style');
            that.tabContent.eq(that.tab.index(self)).css('height', '');
            if(that.freezeBody){
              body.removeAttr('style');
              html.removeAttr('style');
              that.freezeBody = false;
            }
            $(plugin.options.container).removeAttr('style');
            win.scrollTop(that.scrollTopHolder);
            that.scrollTopHolder = 0;
            isOpen = false;
            win.off('resize.resetTabMenu');
            return;
          }
          parent.show();
        };
      }

      //that.tabContent.removeClass('hidden').not(':eq(' + that.index + ')').hide();

      that.selectOnMobile.prop('selectedIndex', that.index);
      that.selectOnMobile.off('change.triggerTab').on('change.triggerTab', function(){
        if(that.data('tab')){
          that.tab.eq(that.selectOnMobile.prop('selectedIndex')).trigger('click.show');
        }
        else{
          if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).is('a')){
            window.location.href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).attr('href');
          }
          else{
            if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').length){
              var href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').attr('href');
              if(!href.match(':void')){
                window.location.href = href;
              }
            }
          }
        }
      }).triggerHandler('blur.resetValue');

      var changeTab = function(self){
        // if(!self.hasClass('hidden')) {
        that.tab/*.eq(that.index)*/.removeClass(that.options.activeClass);
        // }
        if(that.options.animation){
          that.tabContent.eq(that.index).fadeOut(that.options.duration);
        }
        else{
          that.tabContent.eq(that.index).removeClass(that.options.activeClass);
        }

        if(!self.hasClass('hidden')) {
          self.addClass(that.options.activeClass);
        }

        that.index = that.tab.index(self);

        if(that.options.animation){
          that.tabContent.eq(that.index).css({
            'position': 'absolute',
            top: 80
          }).fadeIn(that.options.duration, function(){
            $(this).css('position', '');
          });
        }
        else{
          that.tabContent
            .removeClass(that.options.activeClass)
            .eq(that.index).addClass(that.options.activeClass);
          that.selectOnMobile.prop('selectedIndex', that.index);
        }
      };

      if(!that.data('tab')){
        return;
      }
      that.tab.off('click.show').on('click.show', function(e) {
        e.preventDefault();
        if($.isFunction(that.options.beforeChange)){
          var resultBeforeChange = that.options.beforeChange.call(that, that.tabContent, $(this));
          if(false === resultBeforeChange) {
            return false;
          }
        }

        // show as popup for tabMenu

        if(that.options.isPopup){
          var parent = that.tabContent.parent();
          var self = $(this);
          isOpen = true;

          if(window.innerWidth < that.options.tablet){
            changeTab(self);
            if(window.Modernizr.csstransitions){ // support css3
              that.scrollTopHolder = win.scrollTop();
              parent.show();
              tabContentOverlay = $(that.options.templateOverlay).appendTo(body).show().css({'opacity': 0.5, 'zIndex': that.options.zIndex}).addClass('animated fadeInOverlay');
              parent.css('display', 'block');
              centerPopup(parent, true, 0);
              parent.css('position', 'fixed');
              parent.addClass('animated fadeIn');
              $(plugin.options.container).css({
                'marginTop': - that.scrollTopHolder,
                'height': win.height() + that.scrollTopHolder,
                'overflow': 'hidden'
              });
              parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
                if(parent.outerHeight(true) > win.height()){
                  parent.find('.tab-content.active').removeAttr('style').css({
                    height: win.height() - parseInt(parent.find('.tab-content.active').css('padding-top')),
                    'overflow-y': 'auto'
                  });
                }
                centerPopup(parent, true, 0);
                isOpen = false;
                isTablet = true;
                win.scrollTop(0);
                parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
                if($.isFunction(that.options.afterChange)){
                  that.options.afterChange.call(that, that.tabContent, $(this));
                }
                that.trigger('afterChange');
              });
            }
            else{ // does not support css3
              if(window.innerWidth >= that.options.mobile){
                parent.css('display', 'block');
                centerPopup(parent, true, that.scrollTopHolder);
                parent.css('opacity', 0);
                that.scrollTopHolder = win.scrollTop();
                centerPopup(parent, true, that.scrollTopHolder);
                parent.animate({
                  opacity: 1
                }, that.options.duration);
                isOpen = false;
                tabContentOverlay = $(that.options.templateOverlay).appendTo(body).css({'opacity': 0.5, 'zIndex': that.options.zIndex}).fadeIn(that.options.duration);
              }
            }
            tabContentOverlay.off('click.closeTabContent').on('click.closeTabContent', function(e){
              e.preventDefault();
              that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
            });
            win.off('resize.resetTabMenu').on('resize.resetTabMenu', function(){
              clearTimeout(tabMenuTimer);
              tabMenuTimer = setTimeout(function(){
                onResize(parent, self);
              }, 10);
            });
            return;
          }
        }
        // if($(this).hasClass(that.options.activeClass)){
        //  return;
        // }

        changeTab($(this));

        if($.isFunction(that.options.afterChange)){
          that.options.afterChange.call(that, that.tabContent, $(this));
        }
        that.trigger('afterChange');
      });

      if(that.options.isPopup){
        var closeOnTablet = function(parent){
          // var langToolbarEl = $('.toolbar--language');
          tabContentOverlay.addClass('animated fadeOutOverlay');
          parent.removeClass('fadeIn').addClass('animated fadeOut');
          parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
            $(that.options.container).removeAttr('style');
            // if(langToolbarEl.length) {
            //  $(that.options.container).css('padding-top', langToolbarEl.height());
            // }
            win.scrollTop(that.scrollTopHolder);
            tabContentOverlay.off('click.closeTabContent').remove();
            parent.removeClass('animated fadeOut');
            that.scrollTopHolder = 0;
            isOpen = false;
            parent.removeAttr('style');
            parent.find('.tab-content.active').removeAttr('style');
            parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
          });
        };
        that.find(that.options.triggerClosePopup).off('click.hideTabPopup').on('click.hideTabPopup', function(e){
          e.preventDefault();
          var parent = $(this).closest('.tab-wrapper');
          if(isOpen){
            return;
          }
          isOpen = true;
          if(window.innerWidth < that.options.tablet){
            if(window.Modernizr.csstransitions){ // support css3
              closeOnTablet(parent);
            }
            else{ // does not support css3
              parent.fadeOut(that.options.duration);
              tabContentOverlay.fadeOut(that.options.duration, function(){
                tabContentOverlay.remove();
                if(that.freezeBody){
                  body.removeAttr('style');
                  that.freezeBody = false;
                }
                win.scrollTop(that.scrollTopHolder);
                that.scrollTopHolder = 0;
                isOpen = false;
                parent.removeAttr('style');
              }).off('click.closeTabContent');
            }
            win.off('resize.resetTabMenu');
          }
          if($.isFunction(that.options.beforeClosePopup)){
            that.options.beforeClosePopup.call(that, that.tabContent, $(this));
          }
        });
      }

      that.on('keydown', function(e){
        var currActive = $(ally.get.activeElement());
        function moveTab(dir){
          var currTab = $(e.target).parent();
          var currTabInx;
          if(dir === 'next'){
              currTabInx = listTabs.index(currTab) + 1;
              if(currTabInx >= listTabs.length){
                currTabInx = 0;
              }
          }else if(dir === 'prev'){
              currTabInx = listTabs.index(currTab) - 1;
              if(currTabInx < 0 ){
                currTabInx = listTabs.length - 1;
              }
          }
          var currentActiveTab = listTabs.eq(currTabInx);

          if(currentActiveTab && !currActive.is('input')) {
            ally.element.focus(currentActiveTab.children()[0]);
          }

          currentActiveTab.children().attr('tabindex', 0);
          currentActiveTab.siblings().children().attr('tabindex', -1);
          currentActiveTab.attr({
            'aria-selected': true
          });
          currentActiveTab.siblings().attr({
            'aria-selected': false
          });
          changeTab(currentActiveTab);
          currentActiveTab.trigger('click');

        }
        function moveContent(dir, tabContentSel){
          var currContent = $(e.target);
          var listContent = that.find(tabContentSel + '.active').find('[tabindex], button, a, input').not('.hidden').not('.main--tabs tab-item a');
          var currContentInx;

          if(dir === 'next'){
              currContentInx = listContent.index(currContent) + 1;
              if(currContentInx >= listContent.length){
                currContentInx = 0;
              }
          }else if(dir === 'prev'){
              currContentInx = listContent.index(currContent) - 1;
              if(currContentInx < 0 ){
                currContentInx = listContent.length - 1;
              }
          }
          var currentActiveContent = listContent.eq(currContentInx);
          if(!currContent.parents('.main--tabs').is('.main--tabs') && !currActive.is('input')) {
            ally.element.focus(currentActiveContent[0]);
          }
        }
        function handleKeyDownTab(tabContentSel) {
            switch(e.keyCode) {
                case 39:
                case 40:
                  if(currActive.parents(tabContentSel).length){
                    moveContent('next', tabContentSel);
                  }else{
                    moveTab('next');
                  }
                    break;
                case 37:
                case 38:
                  if(currActive.parents(tabContentSel).length){
                    moveContent('prev', tabContentSel);
                  }else{
                    moveTab('prev');
                  }
                    break;

                case 9:
                if(tabContentSel === '.tab-content-1') {
                  var fistTabindex = that.find(tabContentSel+'.active').find('[tabindex]').eq(0);

                  if(currActive.parents('.tab-content.tabs--4').find(tabContentSel+'.active').length){
                    if(currActive.is(fistTabindex) && e.shiftKey){
                      ally.element.focus(that.find('.main--tabs .tab-item.active').children()[0]);
                      e.stopPropagation();
                      e.preventDefault();
                      return false;
                    }
                    return true;
                  }else{
                    if(e.shiftKey){
                      return true;
                    }
                  }
                  if(fistTabindex.length > 0) {
                    ally.element.focus(fistTabindex[0]);
                  }
                  return false;
                } else {
                  var fistTabindex = that.find(tabContentSel+'.active').find('[aria-selected="true"]');

                  if(currActive.parents(tabContentSel).length){
                    if(currActive.is(fistTabindex) && e.shiftKey){
                      ally.element.focus(that.find('.tab-item.active').children()[0]);
                      e.stopPropagation();
                      e.preventDefault();
                      return false;
                    }
                    return true;
                  }else{
                    if(e.shiftKey){
                      return true;
                    }
                  }
                  if(fistTabindex.length > 0) {
                    ally.element.focus(fistTabindex[0]);
                  }
                  return false;
                }


              case 36:
                  if(currActive.parents(tabContentSel).length){
                    return true;
                  }
                  var targetEle = listTabs.eq(0).children();
                  if(targetEle.length > 0) {
                    ally.element.focus(targetEle[0]);
                  }

                  changeTab(targetEle);
                    targetEle.trigger('click');
                  return false;

              case 35:
                  if(currActive.parents(tabContentSel).length){
                    return true;
                  }
                  var targetEle = listTabs.eq(listTabs.length - 1).children();
                  if(targetEle.length > 0) {
                    ally.element.focus(targetEle[0]);
                  }

                  changeTab(targetEle);
                  targetEle.trigger('click');
                  return false;
          }
        }

        // if(that.is('.tab-content')){
        //   listTabs = that.find('.main--tabs .tab-item');
        //   handleKeyDownTab('.tab-content-1');
        // } else {
        //   listTabs = that.find('.tab .tab-item');
        //   handleKeyDownTab('.tab-content');
        // }
      });
    },
    update: function(){
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.freezeBody = false;
    },
    option: function(options) {
      this.element.options = $.extend({}, this.options, options);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    tab: '',
    tabContent: '',
    activeClass: 'active',
    animation: false,
    duration: 500,
    isPopup: false,
    tablet: 988,
    container: '#container',
    mobile: 768,
    zIndex: 14,
    templateOverlay: '<div class="overlay"></div>',
    triggerClosePopup: '.tab-wrapper .popup__close',
    selectOnMobile: '> select',
    docScroll: window.Modernizr.touch ? 0 : 20,
    beforeClosePopup: function(){},
    afterChange: function() {},
    beforeChange: function(){}
  };

}(jQuery, window));

/**
 *  @name tooltip
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'kTooltip';
	var win = $(window);
	var tooltipOnDevice = null;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
			that = plugin.element;
			that.options = plugin.options;
			that.isShow = false;
			that.timer = 0;
			that.tooltip = $();
			that.maxWidth = plugin.options.maxWidth;
			if(true){
				// this transform only applies to the fare table tooltips
				var oldContent = that.data('content');
				var newContent = oldContent.replace('class="title-conditions"', 'class="title-conditions" id="tooltip-title"');
				that.attr({
					'data-content': newContent,
					'aria-label': 'View more information'
				});

				that.bind('click.showTooltip', function(e){
					e.preventDefault();
					if(that.hasClass('disabled') && !that.data('open-tooltip')){
						return;
					}
					e.stopPropagation();
					if(tooltipOnDevice){
						tooltipOnDevice.tooltip.remove();
						tooltipOnDevice.tooltip = $();
						tooltipOnDevice.isShow = false;
						win.off('resize.hideTooltip');
						win.off('click.hideTooltip');
						win.off('keyup.escapeTooltip');
						$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
						tooltipOnDevice = null;
					}
					if(that.isShow){
						return;
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
			}
			else{
				that.off('click.showTooltip').on('click.showTooltip', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(that.isShow){
						return;
					}
					if(that.timer){
						clearTimeout(that.timer);
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
				that.off('mouseleave.hideTooltip').on('mouseleave.hideTooltip', function(){
					that.timer = setTimeout(function(){
							that.tooltip.remove();
							that.tooltip = $();
							that.isShow = false;
							win.off('resize.hideTooltip');
						}, 200);
				});
				// that.hover(
				// 	function(){
				// 		if(that.timer){
				// 			clearTimeout(that.timer);
				// 		}
				// 		plugin.generateTooltip();
				// 	},
				// 	function(){
				// 		that.timer = setTimeout(function(){
				// 			that.tooltip.remove();
				// 			that.tooltip = $();
				// 		}, 200);
				// 	}
				// );
			}
		},
		generateTooltip: function(){
			var plugin = this,
			that = plugin.element;
			tooltipOnDevice = that;
			if(that.tooltip.length){
				return;
			}
			// if(that.attr('data-type') === 2){
			// 	that.tooltip = $(that.options.template2).appendTo(document.body);
			// }
			// else if(that.attr('data-type') === 3){
			// 	that.tooltip = $(that.options.template3).appendTo(document.body);
			// }
			// else{
			// 	that.tooltip = $(that.options.template1).appendTo(document.body);
			// 	that.tooltip.find('a.popup_close').unbind('click.close').bind('click.close', function(e){
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		that.tooltip.remove();
			// 		that.tooltip = $();
			// 	});
			// }
			if(that.data('type') === 2){
				that.tooltip = $(that.options.template2).appendTo(document.body);
				that.tooltip.find('a.tooltip__close').off('click.close').on('click.close', function(e){
					e.preventDefault();
					e.stopPropagation();
					that.tooltip.remove();
					that.tooltip = $();
					that.isShow = false;
					win.off('resize.hideTooltip');
					$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
				});
			}
			else if(that.data('type') === 3){
				that.tooltip = $(that.options.template3).appendTo(document.body);
			}
			else{
				that.tooltip = $(that.options.template1).appendTo(document.body);
			}

			// that.tooltip = $(that.options.template1).appendTo(document.body);
			that.tooltip.show().find('.tooltip__content').html(that.attr('data-content'));

			// add aria-labels to content
			// IE11+JAWS doesn't read the aria-live
			that.tooltip.find('.summary-fare__conditions .ico-check-thick').after('<span class="says">You may</span>');
			that.tooltip.find('.summary-fare__conditions .ico-close').after('<span class="says">You may not</span>');

			that.tooltip.off('click.preventDefault').on('click.preventDefault', function(e){
				e.stopPropagation();
			}).find('a.tooltip__close').unbind('click.close').bind('click.close', function(e){
				e.preventDefault();
				/*e.stopPropagation();
				that.tooltip.remove();
				that.tooltip = $();*/
				plugin.closeTooltip();
			});

			var actualWidth = that.tooltip.outerWidth(true);
			if(that.data('max-width')){
				that.maxWidth = that.data('max-width');
			}
			if(actualWidth > that.maxWidth){
				actualWidth = that.maxWidth;
			}
			that.tooltip.innerWidth(that.maxWidth);
			that.tooltip.css({
				'max-width': that.tooltip.width()
			}).css('width', '');


			var left = Math.max(0, that.offset().left - actualWidth/2 + that.outerWidth(true)/2 + 5);

			var top = (that.data('type') === 3) ? that.offset().top  + that.innerHeight() : that.offset().top - that.tooltip.outerHeight() - 10;

			if((left + actualWidth) > $(window).width()){
				left = $(window).width() - actualWidth;
			}

			that.tooltip.css({
				position: 'absolute',
				'z-index': 999999,
				// top: that.offset().top - that.tooltip.outerHeight() - that.height()/2,
				top: top,
				left: left
			});
			that.tooltip.find('.tooltip__arrow').css({
				left: that.offset().left - left + that.width()/2
			});

			win.off('click.hideTooltip').on('click.hideTooltip', function(){
				plugin.closeTooltip();
			});
			win.off('keyup.escapeTooltip').on('keyup.escapeTooltip', function(e){
				if (e.which === 27) {
					plugin.closeTooltip();
				}
			});
			win.off('resize.hideTooltip').on('resize.hideTooltip', function(){
				plugin.closeTooltip();
			});
			// TODO: this is triggered when the document scrolls to reveal the popup
			//$(document).off('scroll.hideTooltip mousewheel.hideTooltip').on('scroll.hideTooltip mousewheel.hideTooltip', function(){
			//	console.log('closing due to scroll');
			//	plugin.closeTooltip();
			//});

			if($.isFunction(that.options.afterShow)){
				that.options.afterShow.call(that, that.tooltip);
			}
		},
		closeTooltip: function(){
			var plugin = this,
				that = plugin.element;

			if(that.tooltip.length){
				that.isShow = false;
				that.tooltip.remove();
				that.tooltip = $();
				win.unbind('click.hideTooltip');
				win.unbind('keyup.escapeTooltip');
				tooltipOnDevice = null;
			}
			win.off('resize.hideTooltip');
			//$(document).off('scroll.hideTooltip mousewheel.hideTooltip'); // see above

			if($.isFunction(that.options.afterClose)){
				that.options.afterClose.call(that, that.tooltip);
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		afterShow: function(){
			if ( this.data('type') === 2 || this.data('type') === 3 ) {

				this.addClass('current');

				this.tooltip.attr({
					'tabindex' : '0',
					'open' : 'true',
					'role' : 'dialog'
				});

				if(!this.hasClass('info-card')){
					this.tooltip.attr({
						'aria-describedby': (this.parents('.tooltip-wrapper').length>0)? this.parents('.tooltip-wrapper').attr('data-aria') : 'bsp-wcag-' + this.parent().attr('tabindex')
					});
				}else{
					this.tooltip.attr({
						'aria-describedby': 'bsp-wcag-current'
					});
				}


				var help = 'Beginning of dialog window. Press escape to cancel and close this window.'; // TODO l10n.prop

				this.tooltip.prepend('<p id="tooltip--help" class="says">' + help + '</p>');

				this.tooltip.find('.tooltip__content').attr({
					'role' : 'document', // NVDA doesn't read dialogs see https://github.com/nvaccess/nvda/issues/4493
					'tabindex' : '0',
					'aria-describedby' : 'tooltip--help'
				}).focus();
				this.tooltip.find('.tooltip__content .summary-fare').attr({
					'aria-hidden':true
				});
			}
		},
		afterClose: function(){
			if ( this.data('type') === 2 ) {
				if(this.parents('li').next().length>0){
					this.parents('li').next().focus();
				}else{
					$('[data-tooltip].current').attr('tabindex', -1).focus();
					this.removeClass('current');
				}

			}
			if ( this.data('type') === 3 ) {
				if($('[data-aria="'+this.parents('.tooltip-wrapper').data('aria')+'"]').length > 0 ){
					$('[data-aria="'+this.parents('.tooltip-wrapper').data('aria')+'"]').focus();
				}else{
					$('[data-tooltip].current').focus();
					this.removeClass('current');
				}

			}
		},
		maxWidth: 240,
		template1: '<div class="tooltip"><span class="tooltip__content"></span><em class="tooltip__arrow"></div>',
		template2: '<aside class="tooltip tooltip--info"><em class="tooltip__arrow"></em><div class="tooltip__content"></div><a href="#" class="tooltip__close" aria-label="Close button">&#xe60d;</a></aside>',
		template3: '<aside class="tooltip tooltip--conditions-1"><em class="tooltip__arrow type-top"></em><div class="tooltip__content"></div><a href="#" class="tooltip__close" aria-label="Close button">&#xe60d;</a></aside>'

	};

}(jQuery, window));

/**
 * @name SIA
 * @description Define global wcag language functions
 * @version 1.0
 */

SIA.WcagGlobal = (function(){

  var global = SIA.global;

  var onInitHandler = function(){

    closeButton();
    customRadioLabel();
    customCheckboxLabel();
    addAriaLabel();
    thePopupLanguage();
    dataSearchFlights();
    assignSiblingDescriptionAttributes({
      descriptionSelector: '[data-customselect] .select__tips:not([data-description-category="customSelectUsage"])',
      descriptionCategory: 'tip',
      genericSiblingHostSelector: '.input-overlay'
    });
    attachDescription('[data-customselect] .select__tips');
    disableSelects();
    externalLinks();
    rangeSlider();
    //removeEmptyElements();
    initKeyboardOutlines();
  };

  var addAriaLabel = function(){
    var textCountry = $('[name="text-country"]');
    textCountry.attr({
      'aria-label': 'Country / Region'
    });
  };

  /** Remove Empty Labels **/
  var removeEmptyElements = function(){
    /** List elements that are empty **/
    var emptyEls = 'label.hidden, h2, h3, h4, h5, h6';

    $(emptyEls).each(function(){
      var self = $(this);
      var text = $(this).text();
      if(!text.trim()){
        self.remove();
      }
    });

  };

  /** Close popup, ESC Key **/
  var closeButton = function(isModal){

    if(!isModal){
      isModal = false;
    }

    if(isModal){
      var popup = $('.popup:not(.hidden)');
      var popupContent = $('.popup__content', popup);
      var popupClose = $('.popup__close', popup);
      popupClose.attr({
        'aria-label':'Close button'
      });
      popupContent.append(popupClose);

    }else{
      var popup = $('.popup');
      popup.each(function(){
        var self = $(this);
        var popupContent = $('.popup__content', self);
        var popupClose = $('.popup__close', self);
        popupClose.attr({
          'aria-label':'Close button'
        });
        popupContent.append(popupClose);

      });
    }

  };

  var assignAttributes = function(){

    var popup = $('.popup:visible');

    var ppContent = $('.popup__content', popup);
    var textCountry = $('#text-country', popup);
    var ppInner = $('.popup__inner', popup);
    var ppUiComplete = $('.ui-autocomplete:visible', popup);
    var ppHeading = $('.popup__heading', popup);
    var menuSubInner = $('.menu-sub-inner', popup);

    popup.attr({
      'role':'dialog',
      'tabindex':-1
    });

    ppInner.attr({
      'tabindex':-1
    });

    if(!popup.hasClass('popup--logged-profile')){
      ppContent.attr({
        'role':'document',
        'tabindex':-1
      });
    }else{
      menuSubInner.attr({
        'role':'document',
        'tabindex':0
      });
    }


    ppHeading.attr({
      'tabindex': 0,
      'aria-hidden': false
    }).focus();

  };

  // var ppMoveFocus = function(){
  //   var popup = $('.popup:not(.hidden)');
  //   var ppHeading = popup.find('.popup__content').children(':not(.popup__close)').first();
  //   ppHeading.attr({
  //     'tabindex': 0
  //   }).focus();
  // };

  var ppMoveFocus = function(){
    //Popup will focus whole popup when show
    var popup = $('.popup:not(.hidden)');
    var ppHeading = popup.find('.popup__content');
    ppHeading.addClass('focus-outline');
    setTimeout(function(){
      ppHeading.attr({
        'tabindex': 0
      }).focus();
    },200);
  };

  var externalLinks = function(){
    var linkCheck = $('a.external');

    linkCheck.each(function(){
      var self = $(this);
      var linkText = self.text();
      self.attr({
        'aria-label': 'External Link. ' + linkText
      });
    });

  };

  var customRadioLabel = function(){
    var customRadio = $('.popup .custom-radio');
    customRadio.each(function(){
      var self = $(this);
      var customRadioInput = $('input[type="radio"]', self);
      var customRadioLabel = $('label[for]', self);

      customRadioInput.attr({
        'aria-label': customRadioLabel.text()
      });
    });
  };

  var customCheckboxLabel = function(){
    var customCheckbox = $('.custom-checkbox');
    customCheckbox.each(function(){
      var self = $(this);
      var customCheckboxInput = $('input[type="checkbox"]', self);
      var customCheckboxLabel = $('label[for]', self);

      customCheckboxInput.attr({
        'aria-label': customCheckboxLabel.text()
      });
    });
  };

  var customSelectAriaLive = function(selectText){

    if( selectText.find('#wcag-custom-select').length > 0 ){
      selectText.find('#wcag-custom-select').remove();
    }

    $( '<span>', {
				role: 'status',
				"aria-live": 'assertive',
        id: 'wcag-custom-select'
			})
			.addClass( 'ui-helper-hidden-accessible' )
			.appendTo( selectText );
  };

  var customSelectAriaLiveLang = function(search){

    if($(search).parent().find('#wcag-autocomplete-lang').length>0){
      $(search).parent().find('#wcag-autocomplete-lang').remove();
    }

    $( '<span>', {
				role: 'status',
				"aria-live": 'polite',
        id: 'wcag-autocomplete-lang'
			})
			.addClass( 'ui-helper-hidden-accessible' )
			.appendTo( $(search).parent() );

  };

  var thePopupLanguage = function(){
    var popup = $('.popup--language');
    var customSelect = $('.custom-select', popup);
    var focusable = ['.popup__heading', '.select__text'];

    popup.find('.popup__content *').attr({
      'tabindex': -1,
      'aria-hidden': true
    });

    $('.popup__heading').removeAttr('aria-label');

    popup.find('[aria-live].aria-live, form, fieldset, .form-group, .custom-select, .custom-radio, .custom-checkbox').attr({
      'aria-hidden': false
    });

    popup.find('input[name="language"], input[name="submit-2"], input[name="checkbox-2"], .popup__close').attr({
      'tabindex': 0,
      'aria-hidden': false
    });


    customSelect.each(function(){
      var self = $(this);
      var customSelectImage = self.find('img');
      var customSelectLabel = self.find('label[for]');
      var selectText = self.find('.select__text');
      var selfHeading = self.find('.popup__heading');
      var selfFlag = $('a.flag');

      // self.find('img, label[for], input, .ico-dropdown').attr({
      //   'aria-hidden': true,
      //   'tabindex': -1
      // });

      // self.find('[aria-live]').attr({
      //   'tabindex': -1
      // });
      //

      self.attr({
        'aria-hidden': false
      });

      selfHeading.attr({
        'tabindex': 0,
        'aria-hidden': false
      });

      selectText.attr({
        'aria-label':'Country / Region',
        'tabindex': 0,
        'aria-hidden': false,
        'role':'combobox'
      });
      //
      // selectText.off('keydown').on('keydown', function(e){
      //   var keyCode = e.keyCode || e.which || e.charCode;
      //   var selfSelectText = $(this);
      //   if(keyCode === 9){
      //     selectText.find('input').trigger('blur.highlight');
      //     selectText.find('input').autocomplete('close');
      //     selfSelectText.parents('.form-group').next().find('input:first').focus();
      //   }
      // });
      //
      self.find('.select__text').focus(function(){
        $(this).find('input').trigger('focus.highlight');
      });
      //
      // customSelect.find('*').focus(function(){
      //   console.log($(this));
      // });

    });
  };

  var dataSearchFlights = function(){
    var dataSearchFlights = $('[data-search-flights]');
    var dataSearchFlightsForm = $('[data-search-flights-form]');
    var formGroupTooltips = $('.form-group--tooltips');

    formGroupTooltips.each(function(){
      var self = $(this);

      /** Check if the radio is inside the form **/
      if(self.closest('form').length>0){
        return;
      }

      var selfChild = self.children();
      var selfParents = self.parents('.form-group--tooltips');
      var focusPos = 0;


      self.attr({
        'role':'radiogroup'
      });

      selfChild.each(function(){
        var child = $(this);
        var dataSearchFlightsValue = child.find('input').data('search-flights');
        var form = $('[data-search-flights-form="'+dataSearchFlightsValue+'"]');

        child.attr({
          'aria-describedby': 'wcag-form-' + dataSearchFlightsValue,
          'role': 'radio',
          'tabindex': -1,
          'aria-checked':false,
          'aria-label': child.find('label').text()
        });


        $('.radio-tooltips__text', form).attr('id', 'wcag-form-' + dataSearchFlightsValue);

        child.find('input').attr({
          'tabindex': -1
        });

        child.find('input').next().attr({
          'aria-hidden':true,
          'tabindex':-1
        });

        if(child.index() === 0){
          child.attr({
            'tabindex': 0,
            'aria-checked':true
          });
        }

      });

      selfChild.off('keyup').on('keyup', function(e){
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 39){
          if(focusPos !== selfChild.length-1){
            focusPos++;
            selfChild.eq(focusPos).focus();
          }else{
            selfChild.eq(0).focus();
            focusPos = 0;
          }
        }

        if(keyCode === 37){
          if(focusPos !== 0){
            focusPos--;
            selfChild.eq(focusPos).focus();
          }else{
            selfChild.eq(selfChild.length-1).focus();
            focusPos = selfChild.length-1;
          }
        }

      });

      selfChild.off('focus').on('focus', function(e){
        var selfFocus = $(this);
        selfFocus.find('input').trigger('click');
        selfFocus.attr({
          'aria-checked':true,
          'tabindex': 0
        });
        selfFocus.siblings().attr({
          'aria-checked':false,
          'tabindex': -1
        });
      });

    });
  };

  var assignDescriptionAttributes = function(options) {

    var defaultOptions = ({
      descriptionSelector: false,
      descriptionCategory: false,
      unique_host_selector: false
    });

    var _options = $.extend(defaultOptions, options);

    if ( !_options.descriptionSelector || !_options.descriptionCategory || !_options.unique_host_selector ) {
      return;
    }

    var descriptionSelector = _options.descriptionSelector;
    var descriptionCategory = _options.descriptionCategory;
    var unique_host_selector = _options.unique_host_selector;

    $(descriptionSelector).each( function(i, item) {
      var $description = $(item),
          $host = $(unique_host_selector),
           hostId = $host.attr('id') || 'description-host-' + i + '_js';

      $host.attr('id', hostId);

      $description.attr({
        'data-description-host': '#' + hostId,
        'data-description-category': descriptionCategory
      });
    });
  };

  var assignSiblingDescriptionAttributes = function(options) {

    var defaultOptions = ({
      descriptionSelector: false,
      descriptionCategory: false,
      genericSiblingHostSelector: false
    });

    var _options = $.extend(defaultOptions, options);

    if ( !_options.descriptionSelector || !_options.descriptionCategory || !_options.genericSiblingHostSelector ) {
      return;
    }

    var descriptionSelector = _options.descriptionSelector;
    var descriptionCategory = _options.descriptionCategory;
    var genericSiblingHostSelector = _options.genericSiblingHostSelector;

    $(descriptionSelector).each( function(i, item) {
      var $description = $(item),
          $host = $description.siblings(genericSiblingHostSelector).eq(0),
           hostId = $host.attr('id') || 'description-host-' + i + '_js';

      $host.attr('id', hostId);

      $description.attr({
        'data-description-host': '#' + hostId,
        'data-description-category': descriptionCategory
      });
    });
  };

  var attachDescription = function(descriptionSelector) {

    var $_descriptions = $(descriptionSelector),
        $_description,
        $_host,
         _id,
         _host_descriptions,
         _old_id_list = [],
         _new_id_list = [],
         _str = '';

    // a host may have mutiple descriptions
    $_descriptions.each( function(i, item) {

      // the current description
      $_description = $(item);

      // get host selector
      $_host = $( $_description.data('description-host') );

      // if the host canot be found, exit
      if ( ! $_host.length ) {
        return;
      }

      // get or create description id
      _id = $_description.attr('id') || 'description-' + i + '_js';
      $_description.attr('id', _id);

      // Inject a vocal pause between tips, by adding one to this description
      // But NVDA reads . as 'dot'
      // And IE/JAWS duplicates the description text into the span
      // if ( ! $_description.find('[data-wcag-pause]').length ) {
      //  $_description.append('<span class="ui-helper-hidden-accessible" data-wcag-pause>, </span>');
      // }

      // get the existing list of description ids
      _host_descriptions = $_host.attr('aria-describedby') || '';

      // ids are stored as a space separated list
      // convert into an array
      _old_id_list = _host_descriptions.split(' ');

      // reset the array and string for each description
      _new_id_list = [];
      _str = '';

      // add the new ID to the end of the old list
      // so that it is announced after the existing descriptions
      _old_id_list.push(_id);

      // filter out any duplicate IDs
      // http://stackoverflow.com/a/9229932/6850747
      // this prevents screen readers from reading an attached description multiple times
      $.each(_old_id_list, function(i, el){
        if( $.inArray(el, _new_id_list) === -1 ) {
          _new_id_list.push(el);
        }
      });

      // build a new space separated string
      $.each(_new_id_list, function(i, _id) {
        _str += _id + ' ';
      });

      // remove any leading or trailing spaces
      _str = $.trim( _str );

      // attach the description
      if ( _str.length ) {
        $_host.attr('aria-describedby', _str );
      }

      // reveal description to AT
      $_description.removeAttr('aria-hidden');
    });
  };

  var detachDescription = function(descriptionSelector) {

    if ( !descriptionSelector ) {
      return;
    }

    var $_descriptions = $(descriptionSelector),
        $_description,
        $_host,
         _id,
         _host_descriptions,
         _old_id_list = [],
         _old_id_index,
         _str = '';

    // a host may have mutiple descriptions
    $_descriptions.each( function(i, item) {

      // the current description
      $_description = $(item);

      // get host selector
      $_host = $( $_description.data('description-host') );

      // if the host canot be found, exit
      if ( ! $_host.length ) {
        return;
      }

      // get description id
      _id = $_description.attr('id');

      // get the existing list of description ids
      _host_descriptions = $_host.attr('aria-describedby') || '';

      // ids are stored as a space separated list
      // convert into an array
      _old_id_list = _host_descriptions.split(' ');

      // location of this description's id in the array
      _old_id_index = _old_id_list.indexOf(_id);

      // remove this description ID from the list
      if (_old_id_index !== -1) {
        _old_id_list.splice(_old_id_index, 1);
      }

      // if this description was the only one attached to this host
      // remove the attachment attribute
      if ( ! _old_id_list.length ) {
        $_host.removeAttr('aria-describedby');
      }
      else {
        // build a string from the array
        $.each(_old_id_list, function(i, id) {

          id = $.trim(id);

          if ( id.length ) {
            if ( i > 0 ) {
              _str += ' ';
            }
            _str += id;
          }
        });
      }

      _str = $.trim( _str );

      if ( _str.length ) {
        $_host.attr('aria-describedby', _str );
      }

      // hide the description from AT
      // as we're leaving it in the DOM for reattachment later
      $_description.attr('aria-hidden', true);
    });
  };

  var isIE = function() {

    // http://stackoverflow.com/a/25380644/6850747
    var msie = parseInt((/msie (\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);

    if (isNaN(msie)) {
      msie = parseInt((/trident\/.*; rv:(\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);
    }

    return msie ? true : false;
  };

  var disableSelects = function() {

    var $disabled = $('[data-customselect].disabled');

    $disabled.customSelect('disable');
  };

  var detectIE = function () {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      var rv = ua.indexOf('rv:');
      return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      return true;
    }

    return false;
  }

  var rangeSlider = function() {


    $('.ui-slider_title').attr({
      'tabindex': 0
    });
    $('.ui-slider-result').attr({
      'tabindex': 0
    });
    $('.ui-slider-handle').each(function(index){
      var fromEl = $('.ui-slider_from'),
          toEl = $('.ui-slider_to'),
          minPrice = fromEl.html(),
          maxPrice = toEl.html(),
          _self = $(this),
          oneButton = false;

      if(fromEl.length === 0 && toEl.length === 0) {
        oneButton = true;
        var priceSpec = $('[data-mile="true"]').html().split(" ")[0];
        minPrice = $('#slider-range').data('kris-min');
        maxPrice = $('#slider-range').data('kris-max');
      }

      if(detectIE() === true) {
        if(_self.find('.ui-helper-hidden-accessible').length  === 0) {
          _self.append('<span class="ui-helper-hidden-accessible" id="ui-slider-handle_label' + (index + 1)+ '"></span>');
        }
      }

      _self.attr({
        'role': 'slider',
        'aria-live': 'assertive',
        'aria-valuemin': minPrice,
        'aria-valuemax': maxPrice,
        'aria-labelledby': 'ui-slider-handle_label' + (index + 1),
        'aria-controls': 'ui-slider-handle_text' + (index + 1),
        'tabindex': 0
      });

      if(index === 0) {
        if(fromEl.length === 0 && toEl.length === 0) {
          _self.attr({
            'aria-valuenow': priceSpec,
            'aria-valuetext': priceSpec,
            'title': priceSpec
          });
          _self.find('#ui-slider-handle_label' + (index + 1)).text(priceSpec);
          _self.append('<span class="ui-helper-hidden-accessible">current</span>');
        } else {
          _self.attr({
          'aria-valuenow': minPrice,
          'aria-valuetext': minPrice,
          'title': minPrice
         });
        _self.find('#ui-slider-handle_label' + (index + 1)).text(minPrice);
         _self.append('<span class="ui-helper-hidden-accessible">minimum</span>');
        }
      } else {
        _self.attr({
          'aria-valuenow': maxPrice,
          'aria-valuetext': maxPrice,
          'title': maxPrice
        });
        _self.find('#ui-slider-handle_label' + (index + 1)).text(maxPrice);
        _self.append('<span class="ui-helper-hidden-accessible">maximum</span>');
      }

      _self.on('keyup', function(e){

        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 34 || keyCode === 33 || keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40 || keyCode === 35 || keyCode === 36) {

          var price, title;

          if(index === 0) {
            if(fromEl.length === 0 && toEl.length === 0) {
              price = $('[data-mile="true"]').html().split(" ")[0];
            } else {
              price = fromEl.html();
            }
          } else {
            price = toEl.html();
          }

          _self.find('.ui-helper-hidden-accessible').text(price);

          _self.attr({
            'aria-valuenow': price,
            'aria-valuetext': price,
            'title': price
          });
        }
      });
    });
  };

  var initKeyboardOutlines = function() {
    $('*').on('keydown.tab', function(e){
      /*
      TAB or Shift Tab, Aw.
      Add some more key code if you really want
      */
      if ( 9== e.which && this == e.target ){
        window.setTimeout( function(){
          $('.focus-outline').removeClass('focus-outline');
          $(document.activeElement).addClass('focus-outline');

          if(document.activeElement.type == 'radio') {
            initRadioButtonOutlines();
          } else {
            $(document).off('keydown.radioLogic');
          }
        }, 50 );
      }
    });
  }

  var initRadioButtonOutlines = function() {
    $(document).on('keydown.radioLogic', function(e) {
      if(e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
        window.setTimeout( function(){
          $('.focus-outline').removeClass('focus-outline');
          $(document.activeElement).addClass('focus-outline');
        }, 50 );
      }
    });
  }

  return {
    init: onInitHandler,
    addAriaLabel: addAriaLabel,
    closeButton: closeButton,
    assignAttributes: assignAttributes,
    ppMoveFocus: ppMoveFocus,
    externalLinks: externalLinks,
    customRadioLabel: customRadioLabel,
    customCheckboxLabel: customCheckboxLabel,
    customSelectAriaLive: customSelectAriaLive,
    customSelectAriaLiveLang: customSelectAriaLiveLang,
    thePopupLanguage: thePopupLanguage,
    dataSearchFlights: dataSearchFlights,
    assignDescriptionAttributes: assignDescriptionAttributes,
    assignSiblingDescriptionAttributes: assignSiblingDescriptionAttributes,
    attachDescription: attachDescription,
    detachDescription: detachDescription,
    isIE: isIE,
    disableSelects: disableSelects,
    rangeSlider: rangeSlider
  };

})();


setTimeout(function(){

  SIA.WcagGlobal.init();

  var currentClickedElement;

  $(document).on('click', 'a', function(){
    var linkSelf = $(this);
    setTimeout(function(){
      if($('.popup:visible').length>0){
        if(linkSelf.hasClass('active')){
          linkSelf = linkSelf.removeClass('active');
        }
        currentClickedElement = linkSelf;
      }
    }, 1000);

  });

  $(document).on('keydown', 'a', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;

    if(keyCode === 13){
      var linkSelf = $(this);
      setTimeout(function(){
        if($('.popup:visible').length>0){
          if(linkSelf.hasClass('active')){
            linkSelf = linkSelf.removeClass('active');
          }
          currentClickedElement = linkSelf;
        }
      }, 1000);
    }

  });

  $(document).on('keydown', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;
    var popup = $('.popup:visible');
    var popupClose = $('.popup__close');
    var overlay = $('.overlay');
    var self = $(':focus');

    if(keyCode === 27){
      if(overlay.length>0){
        if($(currentClickedElement).parent().hasClass('logged-in')){
          setTimeout(function(){
            $(currentClickedElement).parent().attr('tabindex', -1).focus();
          },600);
        }else{
          $(currentClickedElement).focus();
        }

        overlay.trigger('click');
      }
    }
    if(keyCode === 13){
      if(self.parent().hasClass('tab-item')){
        self.trigger('click');
        setTimeout(function(){
          var tabContentActive = $('.tab-content.active');
          var tabContentActiveHeading = $('.main-heading', tabContentActive);
          tabContentActive.find('input:first').attr('tabindex', 0).focus();
        }, 500);

      }
    }
    // if(keyCode === 9 && e.shiftKey) {
    //   if(self.attr('name') === 'language'){
    //     $('.popup.fadeIn .custom-select .select__text').focus();
    //   }
    // }
  });

  $('.popup__close').on('keydown', function(e){
    var close = $(this);
    var keyCode = e.keyCode || e.which || e.charCode;
    var overlay = $('.overlay');
    if(keyCode === 13 || keyCode === 9){
      if(overlay.length>0){
        close.trigger('click');
        /* Only set the last clicked element if one existed first */
        if(currentClickedElement) {
          $(currentClickedElement).focus();
        }
      }
    }
  });

  /** Prevent focus moving to skip to main content **/
  $('.popup--language .popup__heading').off('keydown').on('keydown', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;
    if(keyCode === 27){
      $('.flag').focus();
    }

  });


}, 5000);

/**
 *  @name accordion
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'accordion';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}


	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			that.isOpenAll = false;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);

			that.wrapper.each(function(){
				var self = $(this);
				var triggerAccordion = self.find(that.options.triggerAccordion);
				var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');
				self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;

				triggerAccordion.each(function(idx){
					var trigger = $(this);
					var preventDefault = false;

					if(trigger.hasClass('active')){
						trigger.attr('aria-expanded', true);
					}else{
						trigger.attr('aria-expanded', false);
					}
					

					if(supportCss){
						trigger.data('height', contentAccordion.eq(idx).height());
					}
					if(!trigger.hasClass(that.options.activeClass)){
						if(supportCss){
							contentAccordion.eq(idx).css('height', 0);
						}else{
							contentAccordion.eq(idx).hide();
						}
					}
					trigger.off('click.accordion').on('click.accordion', function(e) {
						var expandedState = trigger.attr('aria-expanded') === 'false' ? true : false;

						trigger.attr('aria-expanded', expandedState);
						if($(e.target).is('label') || $(e.target).is(':checkbox')){
							return;
						}
						e.preventDefault();
						if(trigger.closest('.disable').length || preventDefault){
							return;
						}
						if(that.isOpenAll){
							that.isOpenAll = false;
							triggerAccordion.not(trigger).trigger('click.accordion');
							trigger.trigger('stayAcc');
							return;
						}
						preventDefault = true;

						if($.isFunction(that.options.beforeAccordion)){
							that.options.beforeAccordion.call(self, trigger, contentAccordion.eq(idx));
						}
						trigger.trigger('beforeAccordion');

						if(trigger.hasClass(that.options.activeClass)){
							if(supportCss){
								contentAccordion.eq(idx).css('height', 0);
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideUp(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.removeClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
							self.activeAccordion = $();
						}
						else{

							// if(self.activeAccordion.length || ){
							// 	self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							// 	self.activeAccordion.trigger('click.accordion');
							// 	self.activeAccordion = $();
							// }

							var curActiveAccor = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							if (curActiveAccor && curActiveAccor.length) {
								curActiveAccor.trigger('click.accordion');
							}

							self.activeAccordion = trigger;


							if(supportCss){
								contentAccordion.eq(idx).css('height', trigger.data('height'));
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideDown(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.addClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
						}
					});
				});

			});
		},
		refresh: function(){
			this.init();
		},
		openAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(!that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', triggerAccordion.data('height'));
					}else{
						contentAccordion.slideDown(that.options.duration);
					}
					triggerAccordion.addClass(that.options.activeClass);
					that.trigger('openAll');
					that.isOpenAll = true;
				});
			}
		},
		closeAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', 0);
					}else{
						contentAccordion.slideUp(that.options.duration);
					}
					triggerAccordion.removeClass(that.options.activeClass);
					that.trigger('closeAll');
					that.isOpenAll = false;
				});
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		wrapper: '',
		triggerAccordion: '',
		contentAccordion: '',
		activeClass: 'active',
		duration: 400,
		css3: false,
		afterAccordion: function() {},
		beforeAccordion: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global ally functions
 * @version 1.0
 */
SIA.initAlly = function(){
  /*! ally.js - v1.3.0 - http://allyjs.io/ - MIT License */
  !function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{var t;t="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,t.ally=e()}}(function(){var e;return function t(e,n,r){function i(a,u){if(!n[a]){if(!e[a]){var s="function"==typeof require&&require;if(!u&&s)return s(a,!0);if(o)return o(a,!0);var l=new Error("Cannot find module '"+a+"'");throw l.code="MODULE_NOT_FOUND",l}var c=n[a]={exports:{}};e[a][0].call(c.exports,function(t){var n=e[a][1][t];return i(n?n:t)},c,c.exports,t,e,n,r)}return n[a].exports}for(var o="function"==typeof require&&require,a=0;a<r.length;a++)i(r[a]);return i}({1:[function(e,t){"use strict";function n(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function r(e){return e&&"object"==typeof e&&"default"in e?e["default"]:e}function i(e){if(!e)return[];if(Array.isArray(e))return e;if(void 0!==e.nodeType)return[e];if("string"==typeof e&&(e=document.querySelectorAll(e)),void 0!==e.length)return[].slice.call(e,0);throw new TypeError("unexpected input "+String(e))}function o(e){var t=e.context,n=e.label,r=void 0===n?"context-to-element":n,o=e.resolveDocument,a=e.defaultToDocument,u=i(t)[0];if(o&&u&&u.nodeType===Node.DOCUMENT_NODE&&(u=u.documentElement),!u&&a)return document.documentElement;if(!u)throw new TypeError(r+" requires valid options.context");if(u.nodeType!==Node.ELEMENT_NODE&&u.nodeType!==Node.DOCUMENT_FRAGMENT_NODE)throw new TypeError(r+" requires options.context to be an Element");return u}function a(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=o({label:"get/shadow-host",context:t}),r=null;n;)r=n,n=n.parentNode;return r.nodeType===r.DOCUMENT_FRAGMENT_NODE&&r.host?r.host:null}function u(e){return e?e.nodeType===Node.DOCUMENT_NODE?e:e.ownerDocument||document:document}function s(e){var t=o({label:"is/active-element",resolveDocument:!0,context:e}),n=u(t);if(n.activeElement===t)return!0;var r=a({context:t});return r&&r.shadowRoot.activeElement===t?!0:!1}function l(e){var t=u(e);return t.defaultView||window}function c(e){var t=o({label:"element/blur",context:e});if(!s(t))return null;var n=t.nodeName.toLowerCase();if("body"===n)return null;if(t.blur)return t.blur(),document.activeElement;var r=l(t);try{return r.HTMLElement.prototype.blur.call(t),document.activeElement}catch(i){return null}}function d(){var e={activeElement:document.activeElement,windowScrollTop:window.scrollTop,windowScrollLeft:window.scrollLeft,bodyScrollTop:document.body.scrollTop,bodyScrollLeft:document.body.scrollLeft},t=document.createElement("iframe");t.setAttribute("style","position:absolute; position:fixed; top:0; left:-2px; width:1px; height:1px; overflow:hidden;"),t.setAttribute("aria-live","off"),t.setAttribute("aria-busy","true"),t.setAttribute("aria-hidden","true"),document.body.appendChild(t);var n=t.contentWindow,r=n.document;r.open(),r.close();var i=r.createElement("div");return r.body.appendChild(i),e.iframe=t,e.wrapper=i,e.window=n,e.document=r,e}function f(e,t){e.wrapper.innerHTML="";var n="string"==typeof t.element?e.document.createElement(t.element):t.element(e.wrapper,e.document),r=t.mutate&&t.mutate(n,e.wrapper,e.document);return r||r===!1||(r=n),!n.parentNode&&e.wrapper.appendChild(n),r&&r.focus&&r.focus(),t.validate?t.validate(n,e.document):e.document.activeElement===r}function m(e){e.activeElement===document.body?(document.activeElement&&document.activeElement.blur&&document.activeElement.blur(),ln.is.IE10&&document.body.focus()):e.activeElement&&e.activeElement.focus&&e.activeElement.focus(),document.body.removeChild(e.iframe),window.scrollTop=e.windowScrollTop,window.scrollLeft=e.windowScrollLeft,document.body.scrollTop=e.bodyScrollTop,document.body.scrollLeft=e.bodyScrollLeft}function b(e){var t=d(),n={};return Object.keys(e).map(function(r){n[r]=f(t,e[r])}),m(t),n}function v(e){var t=void 0;try{t=window.localStorage&&window.localStorage.getItem(e),t=t?JSON.parse(t):{}}catch(n){t={}}return t}function h(e,t){if(document.hasFocus())try{window.localStorage&&window.localStorage.setItem(e,JSON.stringify(t))}catch(n){}else try{window.localStorage&&window.localStorage.removeItem(e)}catch(n){}}function g(){var e=void 0;try{document.querySelector("html >>> :first-child"),e=">>>"}catch(t){try{document.querySelector("html /deep/ :first-child"),e="/deep/"}catch(n){e=""}}return e}function p(){return jn}function x(){return $n}function y(){return ar}function w(){var e=b(sr);return Object.keys(ur).forEach(function(t){e[t]=ur[t]()}),e}function E(){return lr?lr:(lr=An.get(),lr.time||(An.set(w()),lr=An.get()),lr)}function S(e){cr||(cr=E());var t=cr.focusTabindexTrailingCharacters?fr:dr,n=o({label:"is/valid-tabindex",resolveDocument:!0,context:e});if(!n.hasAttribute("tabindex"))return!1;if(void 0===n.tabIndex)return!1;if(cr.focusInvalidTabindex)return!0;var r=n.getAttribute("tabindex");return"-32768"===r?!1:Boolean(r&&t.test(r))}function T(e){if(!S(e))return null;var t=parseInt(e.getAttribute("tabindex"),10);return isNaN(t)?-1:t}function A(e){mr||(mr=E(),mr.focusFieldsetDisabled&&delete vr.fieldset,mr.focusFormDisabled&&delete vr.form,br=new RegExp("^("+Object.keys(vr).join("|")+")$"));var t=o({label:"is/native-disabled-supported",context:e}),n=t.nodeName.toLowerCase();return Boolean(br.test(n))}function O(e){var t=e.element,n=e.attribute,r="data-cached-"+n,i=t.getAttribute(r);if(null===i){var o=t.getAttribute(n);if(null===o)return;t.setAttribute(r,o||""),t.removeAttribute(n)}else{var o=t.getAttribute(r);t.removeAttribute(r),t.setAttribute(n,o)}}function I(e){var t=e.element,n=e.attribute,r=e.temporaryValue,i=e.saveValue,o="data-cached-"+n;if(void 0!==r){var a=i||t.getAttribute(n);t.setAttribute(o,a||""),t.setAttribute(n,r)}else{var a=t.getAttribute(o);t.removeAttribute(o),""===a?t.removeAttribute(n):t.setAttribute(n,a)}}function C(){pr.warn("trying to focus inert element",this)}function L(e,t){if(t){var n=T(e);I({element:e,attribute:"tabindex",temporaryValue:"-1",saveValue:null!==n?n:""})}else I({element:e,attribute:"tabindex"})}function N(e,t){O({element:e,attribute:"controls",remove:t})}function k(e,t){I({element:e,attribute:"focusable",temporaryValue:t?"false":void 0})}function M(e,t){O({element:e,attribute:"xlink:href",remove:t})}function _(e,t){I({element:e,attribute:"aria-disabled",temporaryValue:t?"true":void 0})}function P(e,t){t?e.focus=C:delete e.focus}function F(e,t){if(t){var n=e.style.pointerEvents||"";e.setAttribute("data-inert-pointer-events",n),e.style.pointerEvents="none"}else{var n=e.getAttribute("data-inert-pointer-events");e.removeAttribute("data-inert-pointer-events"),e.style.pointerEvents=n}}function R(e,t){_(e,t),L(e,t),P(e,t),F(e,t);var n=e.nodeName.toLowerCase();("video"===n||"audio"===n)&&N(e,t),("svg"===n||e.ownerSVGElement)&&(xr.focusSvgFocusableAttribute?k(e,t):xr.focusSvgTabindexAttribute||"a"!==n||M(e,t)),t?e.setAttribute("data-ally-disabled","true"):e.removeAttribute("data-ally-disabled")}function B(e,t){xr||(xr=E());var n=o({label:"element/disabled",context:e});t=Boolean(t);var r=n.hasAttribute("data-ally-disabled"),i=1===arguments.length;return A(n)?i?n.disabled:(n.disabled=t,n):i?r:r===t?n:(R(n,t),n)}function D(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=[],r=o({label:"get/parents",context:t});r;)n.push(r),r=r.parentNode,r&&r.nodeType!==Node.ELEMENT_NODE&&(r=null);return n}function W(e){yr.some(function(t){return e[t]?(wr=t,!0):!1})}function H(e,t){return wr||W(e),e[wr](t)}function j(e){var t=e.webkitUserModify||"";return Boolean(t&&-1!==t.indexOf("write"))}function q(e){return[e.getPropertyValue("overflow"),e.getPropertyValue("overflow-x"),e.getPropertyValue("overflow-y")].some(function(e){return"auto"===e||"scroll"===e})}function K(e){return e.display.indexOf("flex")>-1}function G(e,t,n,r){return"div"!==t&&"span"!==t?!1:n&&"div"!==n&&"span"!==n&&!q(r)?!1:e.offsetHeight<e.scrollHeight||e.offsetWidth<e.scrollWidth}function Z(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{flexbox:!1,scrollable:!1,shadow:!1}:n;Er||(Er=E());var i=o({label:"is/focus-relevant",resolveDocument:!0,context:t});if(!r.shadow&&i.shadowRoot)return!0;var a=i.nodeName.toLowerCase();if("input"===a&&"hidden"===i.type)return!1;if("input"===a||"select"===a||"button"===a||"textarea"===a)return!0;if("legend"===a&&Er.focusRedirectLegend)return!0;if("label"===a)return!0;if("area"===a)return!0;if("a"===a&&i.hasAttribute("href"))return!0;if("object"===a&&i.hasAttribute("usemap"))return!1;if("object"===a){var u=i.getAttribute("type");if(!Er.focusObjectSvg&&"image/svg+xml"===u)return!1;if(!Er.focusObjectSwf&&"application/x-shockwave-flash"===u)return!1}if("iframe"===a||"object"===a)return!0;if("embed"===a||"keygen"===a)return!0;if(i.hasAttribute("contenteditable"))return!0;if("audio"===a&&(Er.focusAudioWithoutControls||i.hasAttribute("controls")))return!0;if("video"===a&&(Er.focusVideoWithoutControls||i.hasAttribute("controls")))return!0;if(Er.focusSummary&&"summary"===a)return!0;var s=S(i);if("img"===a&&i.hasAttribute("usemap"))return s&&Er.focusImgUsemapTabindex||Er.focusRedirectImgUsemap;if(Er.focusTable&&("table"===a||"td"===a))return!0;if(Er.focusFieldset&&"fieldset"===a)return!0;var l=i.getAttribute("focusable");if("svg"===a)return s||Er.focusSvg||Boolean(Er.focusSvgFocusableAttribute&&l&&"true"===l);if(H(i,"svg a")&&i.hasAttribute("xlink:href"))return!0;if(Er.focusSvgFocusableAttribute&&i.ownerSVGElement)return Boolean(l&&"true"===l);if(s)return!0;var c=window.getComputedStyle(i,null);if(j(c))return!0;if(Er.focusImgIsmap&&"img"===a&&i.hasAttribute("ismap")){var d=D({context:i}).some(function(e){return"a"===e.nodeName.toLowerCase()&&e.hasAttribute("href")});if(d)return!0}if(!r.scrollable&&Er.focusScrollContainer)if(Er.focusScrollContainerWithoutOverflow){if(G(i,a))return!0}else if(q(c))return!0;if(!r.flexbox&&Er.focusFlexboxContainer&&K(c))return!0;var f=i.parentElement;if(!r.scrollable&&f){var m=f.nodeName.toLowerCase(),b=window.getComputedStyle(f,null);if(Er.focusScrollBody&&G(f,a,m,b))return!0;if(Er.focusChildrenOfFocusableFlexbox&&K(b))return!0}return!1}function V(e){try{return e.contentDocument||e.contentWindow&&e.contentWindow.document||e.getSVGDocument&&e.getSVGDocument()||null}catch(t){return null}}function $(e){if("string"!=typeof Tr){var t=g();t&&(Tr=", html "+t+" ")}return Tr?e+Tr+e.replace(/\s*,\s*/g,",").split(",").join(Tr):e}function U(e){if(Ar||(Ar=$("object, iframe")),void 0!==e._frameElement)return e._frameElement;e._frameElement=null;var t=e.parent.document.querySelectorAll(Ar);return[].some.call(t,function(t){var n=V(t);return n!==e.document?!1:(e._frameElement=t,!0)}),e._frameElement}function X(e){var t=l(e);if(!t.parent||t.parent===t)return null;try{return t.frameElement||U(t)}catch(n){return null}}function z(e,t){return window.getComputedStyle(e,null).getPropertyValue(t)}function J(e){return e.some(function(e){return"none"===z(e,"display")})}function Q(e){var t=e.findIndex(function(e){var t=z(e,"visibility");return"hidden"===t||"collapse"===t});if(-1===t)return!1;var n=e.findIndex(function(e){return"visible"===z(e,"visibility")});return-1===n?!0:n>t?!0:!1}function Y(e){var t=1;return"summary"===e[0].nodeName.toLowerCase()&&(t=2),e.slice(t).some(function(e){return"details"===e.nodeName.toLowerCase()&&e.open===!1})}function ee(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{notRendered:!1,cssDisplay:!1,cssVisibility:!1,detailsElement:!1,browsingContext:!1}:n,i=o({label:"is/visible",resolveDocument:!0,context:t}),a=i.nodeName.toLowerCase();if(!r.notRendered&&Or.test(a))return!0;var u=D({context:i}),s="audio"===a&&!i.hasAttribute("controls");if(!r.cssDisplay&&J(s?u.slice(1):u))return!1;if(!r.cssVisibility&&Q(u))return!1;if(!r.detailsElement&&Y(u))return!1;if(!r.browsingContext){var l=X(i),c=ee.except(r);if(l&&!c(l))return!1}return!0}function te(e,t){var n=t.querySelector('map[name="'+sn(e)+'"]');return n||null}function ne(e){var t=e.getAttribute("usemap");if(!t)return null;var n=u(e);return te(t.slice(1),n)}function re(e){var t=e.parentElement;if(!t.name||"map"!==t.nodeName.toLowerCase())return null;var n=u(e);return n.querySelector('img[usemap="#'+sn(t.name)+'"]')||null}function ie(e){Cr||(Cr=E());var t=o({label:"is/valid-area",context:e}),n=t.nodeName.toLowerCase();if("area"!==n)return!1;var r=t.hasAttribute("tabindex");if(!Cr.focusAreaTabindex&&r)return!1;var i=re(t);if(!i||!Ir(i))return!1;if(!Cr.focusBrokenImageMap&&(!i.complete||!i.naturalHeight||i.offsetWidth<=0||i.offsetHeight<=0))return!1;if(!Cr.focusAreaWithoutHref&&!t.href)return Cr.focusAreaTabindex&&r||Cr.focusAreaImgTabindex&&i.hasAttribute("tabindex");var a=D({context:i}).slice(1).some(function(e){var t=e.nodeName.toLowerCase();return"button"===t||"a"===t});return a?!1:!0}function oe(e){var t=e.nodeName.toLowerCase();return"fieldset"===t&&e.disabled}function ae(e){var t=e.nodeName.toLowerCase();return"form"===t&&e.disabled}function ue(e){Lr||(Lr=E());var t=o({label:"is/disabled",context:e});if(t.hasAttribute("data-ally-disabled"))return!0;if(!A(t))return!1;if(t.disabled)return!0;var n=D({context:t});return n.some(oe)?!0:!Lr.focusFormDisabled&&n.some(ae)?!0:!1}function se(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{onlyFocusableBrowsingContext:!1,visible:!1}:n,i=o({label:"is/only-tabbable",resolveDocument:!0,context:t});if(!r.visible&&!Ir(i))return!1;if(!r.onlyFocusableBrowsingContext&&(ln.is.GECKO||ln.is.TRIDENT)){var a=X(i);if(a&&T(a)<0)return!1}var u=i.nodeName.toLowerCase(),s=T(i);if("label"===u&&ln.is.GECKO)return null!==s&&s>=0;if("svg"===u&&ln.is.TRIDENT)return"false"!==i.getAttribute("focusable");var c=l(i);if(i instanceof c.SVGElement){if("a"===u&&i.hasAttribute("xlink:href")){if(ln.is.GECKO)return!0;if(ln.is.TRIDENT)return"false"!==i.getAttribute("focusable")}if(ln.is.TRIDENT)return"true"===i.getAttribute("focusable")}return!1}function le(e){var t=e.nodeName.toLowerCase();if("embed"===t||"keygen"===t)return!0;var n=T(e);if(e.shadowRoot&&null===n)return!0;if("label"===t)return!kr.focusLabelTabindex||null===n;if("legend"===t)return null===n;if(kr.focusSvgFocusableAttribute&&(e.ownerSVGElement||"svg"===t)){var r=e.getAttribute("focusable");return r&&"false"===r}return"img"===t&&e.hasAttribute("usemap")?null===n||!kr.focusImgUsemapTabindex:"area"===t?!ie(e):!1}function ce(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{disabled:!1,visible:!1,onlyTabbable:!1}:n;kr||(kr=E());var i=Nr.rules.except({onlyFocusableBrowsingContext:!0,visible:r.visible}),a=o({label:"is/focusable",resolveDocument:!0,context:t}),u=Sr.rules({context:a,except:r});if(!u||le(a))return!1;if(!r.disabled&&ue(a))return!1;if(!r.onlyTabbable&&i(a))return!1;if(!r.visible){var s={context:a,except:{}};if(kr.focusInHiddenIframe&&(s.except.browsingContext=!0),kr.focusObjectSvgHidden){var l=a.nodeName.toLowerCase();"object"===l&&(s.except.cssVisibility=!0)}if(!Ir.rules(s))return!1}var c=X(a);if(c){var d=c.nodeName.toLowerCase();if(!("object"!==d||kr.focusInZeroDimensionObject||c.offsetWidth&&c.offsetHeight))return!1}return!0}function de(e){var t=function(t){return t.shadowRoot?NodeFilter.FILTER_ACCEPT:e(t)?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_SKIP};return t.acceptNode=t,t}function fe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy;t||(t=document.documentElement);for(var o=Mr.rules.except({onlyTabbable:r}),a=u(t),s=a.createTreeWalker(t,NodeFilter.SHOW_ELEMENT,"all"===i?_r:de(o),!1),l=[];s.nextNode();)s.currentNode.shadowRoot?(o(s.currentNode)&&l.push(s.currentNode),l=l.concat(fe({context:s.currentNode.shadowRoot,includeOnlyTabbable:r,strategy:i}))):l.push(s.currentNode);return n&&("all"===i?Sr(t)&&l.unshift(t):o(t)&&l.unshift(t)),l}function me(){return Pr||(Pr=E()),"string"==typeof Fr?Fr:(Fr=""+(Pr.focusTable?"table, td,":"")+(Pr.focusFieldset?"fieldset,":"")+"svg a,a[href],area[href],input, select, textarea, button,iframe, object, embed,keygen,"+(Pr.focusAudioWithoutControls?"audio,":"audio[controls],")+(Pr.focusVideoWithoutControls?"video,":"video[controls],")+(Pr.focusSummary?"summary,":"")+"[tabindex],[contenteditable]",Fr=$(Fr))}function be(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=me(),o=t.querySelectorAll(i),a=Mr.rules.except({onlyTabbable:r}),u=[].filter.call(o,a);return n&&a(t)&&u.unshift(t),u}function ve(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy,a=void 0===i?"quick":i,u=o({label:"query/focusable",resolveDocument:!0,defaultToDocument:!0,context:t}),s={context:u,includeContext:n,includeOnlyTabbable:r,strategy:a};if("quick"===a)return be(s);if("strict"===a||"all"===a)return fe(s);throw new TypeError('query/focusable requires option.strategy to be one of ["quick", "strict", "all"]')}function he(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=void 0===n?{flexbox:!1,scrollable:!1,shadow:!1,visible:!1,onlyTabbable:!1}:n,i=o({label:"is/tabbable",resolveDocument:!0,context:t});if(ln.is.BLINK&&ln.is.ANDROID&&ln.majorVersion>42)return!1;var a=X(i);if(a){if(ln.is.WEBKIT&&ln.is.IOS&&ln.majorVersion<10)return!1;if(T(a)<0)return!1;if(!r.visible&&(ln.is.BLINK||ln.is.WEBKIT)&&!Ir(a))return!1;var u=a.nodeName.toLowerCase();if("object"===u&&(ln.is.BLINK||ln.is.WEBKIT))return!1}var s=i.nodeName.toLowerCase(),l=T(i),c=null===l?null:l>=0,d=c!==!1,f=null!==l&&l>=0;if(i.hasAttribute("contenteditable"))return d;if(Rr.test(s)&&c!==!0)return!1;if(void 0===i.tabIndex)return Boolean(r.onlyTabbable);if("audio"===s){if(!i.hasAttribute("controls"))return!1;if(ln.is.BLINK)return!0}if("video"===s)if(i.hasAttribute("controls")){if(ln.is.BLINK||ln.is.GECKO)return!0}else if(ln.is.TRIDENT)return!1;if("object"===s&&(ln.is.BLINK||ln.is.WEBKIT))return!1;if("iframe"===s)return!1;if(ln.is.WEBKIT&&ln.is.IOS){var m="input"===s&&"text"===i.type||"password"===i.type||"select"===s||"textarea"===s||i.hasAttribute("contenteditable");if(!m){var b=window.getComputedStyle(i,null);m=j(b)}if(!m)return!1}if(!r.scrollable&&ln.is.GECKO){var v=window.getComputedStyle(i,null);if(q(v))return d}if(ln.is.TRIDENT){if("area"===s){var h=re(i);if(h&&T(h)<0)return!1}var g=window.getComputedStyle(i,null);if(j(g))return i.tabIndex>=0;if(!r.flexbox&&K(g))return null!==l?f:Br(i)&&Dr(i);if(G(i,s))return!1;var p=i.parentElement;if(p){var x=p.nodeName.toLowerCase(),y=window.getComputedStyle(p,null);if(G(p,s,x,y))return!1;if(K(y))return f}}return i.tabIndex>=0}function ge(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,i=e.strategy,o=Wr.rules.except({onlyTabbable:r});return ve({context:t,includeContext:n,includeOnlyTabbable:r,strategy:i}).filter(o)}function pe(e,t){return e.compareDocumentPosition(t)&Node.DOCUMENT_POSITION_FOLLOWING?-1:1}function xe(e){return e.sort(pe)}function ye(e,t){return e.findIndex(function(e){return t.compareDocumentPosition(e)&Node.DOCUMENT_POSITION_FOLLOWING})}function we(e,t,n){var r=[];return t.forEach(function(t){var o=!0,a=e.indexOf(t);-1===a&&(a=ye(e,t),o=!1),-1===a&&(a=e.length);var u=i(n?n(t):t);u.length&&r.push({offset:a,replace:o,elements:u})}),r}function Ee(e,t){var n=0;t.sort(function(e,t){return e.offset-t.offset}),t.forEach(function(t){var r=t.replace?1:0,i=[t.offset+n,r].concat(t.elements);e.splice.apply(e,i),n+=t.elements.length-r})}function Se(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.list,n=e.elements,r=e.resolveElement,o=t.slice(0),a=i(n).slice(0);xe(a);var u=we(o,a,r);return Ee(o,u),o}function Te(e){var t=e.nodeName.toLowerCase();return"input"===t||"textarea"===t||"select"===t||"button"===t}function Ae(e,t){var n=e.getAttribute("for");return n?t.getElementById(n):e.querySelector("input, select, textarea")}function Oe(e){var t=e.parentNode,n=ve({context:t,strategy:"strict"});return n.filter(Te)[0]||null}function Ie(e,t){var n=ge({context:t.body,strategy:"strict"});if(!n.length)return null;var r=Se({list:n,elements:[e]}),i=r.indexOf(e);return i===r.length-1?null:r[i+1]}function Ce(e,t){if(!Hr.focusRedirectLegend)return null;var n=e.parentNode;return"fieldset"!==n.nodeName.toLowerCase()?null:"tabbable"===Hr.focusRedirectLegend?Ie(e,t):Oe(e,t)}function Le(e){if(!Hr.focusRedirectImgUsemap)return null;var t=ne(e);return t&&t.querySelector("area")||null}function Ne(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.skipFocusable;Hr||(Hr=E());var r=o({label:"get/focus-redirect-target",context:t});if(!n&&Mr(r))return null;var i=r.nodeName.toLowerCase(),a=u(r);return"label"===i?Ae(r,a):"legend"===i?Ce(r,a):"img"===i?Le(r,a):null}function ke(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.except,r=o({label:"get/focus-target",context:t}),i=null,a=function(e){var t=Mr.rules({context:e,except:n});return t?(i=e,!0):(i=Ne({context:e,skipFocusable:!0}),Boolean(i))};return a(r)?i:(D({context:r}).slice(1).some(a),i)}function Me(e){var t=D({context:e}),n=t.slice(1).map(function(e){return{element:e,scrollTop:e.scrollTop,scrollLeft:e.scrollLeft}});return function(){n.forEach(function(e){e.element.scrollTop=e.scrollTop,e.element.scrollLeft=e.scrollLeft})}}function _e(e){if(e.focus)return e.focus(),s(e)?e:null;var t=l(e);try{return t.HTMLElement.prototype.focus.call(e),e}catch(n){return null}}function Pe(e){var t=arguments.length<=1||void 0===arguments[1]?{}:arguments[1],n=t.defaultToAncestor,r=t.undoScrolling,i=o({label:"element/focus",context:e}),a=Mr.rules({context:i,except:jr});if(!n&&!a)return null;var u=ke({context:i,except:jr});if(!u)return null;if(s(u))return u;var l=void 0;r&&(l=Me(u));var c=_e(u);return l&&l(),c}function Fe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.force;t?this.instances=0:this.instances--,this.instances||(this.disengage(),this._result=null)}function Re(){return this.instances?(this.instances++,this._result):(this.instances++,this._result=this.engage()||{},this._result.disengage=Fe.bind(this),this._result)}function Be(){}function De(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.engage,n=e.disengage,r={engage:t||Be,disengage:n||Be,instances:0,_result:null};return Re.bind(r)}function We(){if(document.activeElement){if(document.activeElement!==Zr){var e=new Gr("active-element",{bubbles:!1,cancelable:!1,detail:{focus:document.activeElement,blur:Zr}});document.dispatchEvent(e),Zr=document.activeElement}}else document.body.focus();Vr!==!1&&(Vr=requestAnimationFrame(We))}function He(){Vr=!0,Zr=document.activeElement,We()}function je(){cancelAnimationFrame(Vr),Vr=!1}function qe(e){var t=o({label:"is/shadowed",resolveDocument:!0,context:e});return Boolean(a({context:t}))}function Ke(){for(var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=[],r=o({label:"get/shadow-host-parents",context:t});r&&(r=a({context:r}));)n.push(r);return n}function Ge(){for(var e=[document.activeElement];e[0]&&e[0].shadowRoot;)e.unshift(e[0].shadowRoot.activeElement);return e}function Ze(){var e=Ke({context:document.activeElement});return[document.activeElement].concat(e)}function Ve(){return null===document.activeElement&&document.body.focus(),qe(document.activeElement)?Ze():Ge()}function $e(){this.context&&(this.context.forEach(this.disengage),this.context=null,this.engage=null,this.disengage=null)}function Ue(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context;return this.context=i(t||document),this.context.forEach(this.engage),{disengage:$e.bind(this)}}function Xe(){}function ze(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.engage,n=e.disengage,r={engage:t||Xe,disengage:n||Xe,context:null};return Ue.bind(r)}function Je(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=u(t),r=void 0;try{r=n.activeElement}catch(i){}return r&&r.nodeType||(r=n.body||n.documentElement),r}function Qe(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.parent,n=e.element,r=e.includeSelf;if(t)return function(e){return Boolean(r&&e===t||t.compareDocumentPosition(e)&Node.DOCUMENT_POSITION_CONTAINED_BY)};if(n)return function(e){return Boolean(r&&n===e||e.compareDocumentPosition(n)&Node.DOCUMENT_POSITION_CONTAINED_BY)};throw new TypeError("util/compare-position#getParentComparator required either options.parent or options.element")}function Ye(e){var t=e.context,n=e.filter,r=function(e){var t=Qe({parent:e});return n.some(t)},i=[],o=function(e){return n.some(function(t){return e===t})?NodeFilter.FILTER_REJECT:r(e)?NodeFilter.FILTER_ACCEPT:(i.push(e),NodeFilter.FILTER_REJECT)};o.acceptNode=o;for(var a=u(t),s=a.createTreeWalker(t,NodeFilter.SHOW_ELEMENT,o,!1);s.nextNode(););return i}function et(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter;if(t=o({label:"get/insignificant-branches",defaultToDocument:!0,context:t}),n=i(n),!n.length)throw new TypeError("get/insignificant-branches requires valid options.filter");return Ye({context:t,filter:n})}function tt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=o({label:"query/shadow-hosts",resolveDocument:!0,defaultToDocument:!0,context:t}),r=u(t),i=r.createTreeWalker(n,NodeFilter.SHOW_ELEMENT,mi,!1),a=[];for(n.shadowRoot&&(a.push(n),a=a.concat(tt({context:n.shadowRoot})));i.nextNode();)a.push(i.currentNode),a=a.concat(tt({context:i.currentNode.shadowRoot}));return a}function nt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.config;if("function"!=typeof n)throw new TypeError("observe/shadow-mutations requires options.callback to be a function");if("object"!=typeof r)throw new TypeError("observe/shadow-mutations requires options.config to be an object");if(!window.MutationObserver)return{disengage:function(){}};var i=o({label:"observe/shadow-mutations",resolveDocument:!0,defaultToDocument:!0,context:t}),a=new vi({context:i,callback:n,config:r});return{disengage:a.disengage}}function rt(e){return B(e,!0)}function it(e){return B(e,!1)}function ot(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter,r=new gi({context:t,filter:n});return{disengage:r.disengage}}function at(e){I({element:e,attribute:"aria-hidden",temporaryValue:"true"})}function ut(e){I({element:e,attribute:"aria-hidden"})}function st(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.filter,r=new xi({context:t,filter:n});return{disengage:r.disengage}}function lt(e,t){var n=t.querySelectorAll("img[usemap]"),r=new yi(t),i=r.extractAreasFromList(e);return n.length?Se({list:i,elements:n,resolveElement:function(e){var t=e.getAttribute("usemap").slice(1);return r.getAreasFor(t)}}):i}function ct(e,t,n){var r=new wi(t,n),i=r.extractElements(e);return i.length===e.length?n(e):r.sort(i)}function dt(e){var t={},n=[],r=e.filter(function(e){return e.tabIndex<=0||void 0===e.tabIndex?!0:(t[e.tabIndex]||(t[e.tabIndex]=[],n.push(e.tabIndex)),t[e.tabIndex].push(e),!1)}),i=n.sort().map(function(e){return t[e]}).reduceRight(function(e,t){return t.concat(e)},r);return i}function ft(e,t){var n=e.indexOf(t);if(n>0){var r=e.splice(n,1);return r.concat(e)}return e}function mt(e,t){return Ei.tabsequenceAreaAtImgPosition&&(e=lt(e,t)),e=dt(e)}function bt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.includeContext,r=e.includeOnlyTabbable,o=e.strategy;Ei||(Ei=E());var a=i(t)[0]||document.documentElement,u=ge({context:a,includeContext:n,includeOnlyTabbable:r,strategy:o});return u=document.body.createShadowRoot&&ln.is.BLINK?ct(u,a,mt):mt(u,a),n&&(u=ft(u,a)),u}function vt(e){var t=e?null:!1;return{altKey:t,ctrlKey:t,metaKey:t,shiftKey:t}}function ht(e){var t=-1!==e.indexOf("*"),n=vt(t);return e.forEach(function(e){if("*"!==e){var t=!0,r=e.slice(0,1);"?"===r?t=null:"!"===r&&(t=!1),t!==!0&&(e=e.slice(1));var i=Ci[e];if(!i)throw new TypeError('Unknown modifier "'+e+'"');n[i]=t}}),n}function gt(e){var t=Si[e]||parseInt(e,10);if(!t||"number"!=typeof t||isNaN(t))throw new TypeError('Unknown key "'+e+'"');return[t].concat(Si._alias[t]||[])}function pt(e,t){return!Li.some(function(n){return"boolean"==typeof e[n]&&Boolean(t[n])!==e[n]})}function xt(e){return e.split(/\s+/).map(function(e){var t=e.split("+"),n=ht(t.slice(0,-1)),r=gt(t.slice(-1));return{keyCodes:r,modifiers:n,matchModifiers:pt.bind(null,n)}})}function yt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t={},n=i(e.context)[0]||document.documentElement;delete e.context;var r=i(e.filter);delete e.filter;var o=Object.keys(e);if(!o.length)throw new TypeError("when/key requires at least one option key");var a=function(e){e.keyCodes.forEach(function(n){t[n]||(t[n]=[]),t[n].push(e)})};o.forEach(function(t){if("function"!=typeof e[t])throw new TypeError('when/key requires option["'+t+'"] to be a function');var n=function(n){return n.callback=e[t],n};xt(t).map(n).forEach(a)});var u=function(e){if(!e.defaultPrevented){if(r.length){var i=Qe({element:e.target,includeSelf:!0});if(r.some(i))return}var o=e.keyCode||e.which;t[o]&&t[o].forEach(function(t){t.matchModifiers(e)&&t.callback.call(n,e,s)})}};n.addEventListener("keydown",u,!1);var s=function(){n.removeEventListener("keydown",u,!1)};return{disengage:s}}function wt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context;return t||(t=document.documentElement),bt(),yt({"?alt+?shift+tab":function(e){e.preventDefault();var n=bt({context:t}),r=e.shiftKey,i=n[0],o=n[n.length-1],a=r?i:o,u=r?o:i;if(s(a))return void u.focus();var l=void 0,c=n.some(function(e,t){return s(e)?(l=t,!0):!1});if(!c)return void i.focus();var d=r?-1:1;n[l+d].focus()}})}function Et(){_i=0,Pi=0}function St(e){e.isPrimary!==!1&&_i++}function Tt(e){return e.isPrimary!==!1?e.touches?void(_i=e.touches.length):void(window.setImmediate||window.setTimeout)(function(){_i=Math.max(_i-1,0)}):void 0}function At(e){switch(e.keyCode||e.which){case 16:case 17:case 18:case 91:case 93:return}Pi++}function Ot(e){switch(e.keyCode||e.which){case 16:case 17:case 18:case 91:case 93:return}(window.setImmediate||window.setTimeout)(function(){Pi=Math.max(Pi-1,0)})}function It(){return{pointer:Boolean(_i),key:Boolean(Pi)}}function Ct(){_i=Pi=0,window.removeEventListener("blur",Et,!1),document.documentElement.removeEventListener("keydown",At,!0),document.documentElement.removeEventListener("keyup",Ot,!0),Fi.forEach(function(e){document.documentElement.removeEventListener(e,St,!0)}),Ri.forEach(function(e){document.documentElement.removeEventListener(e,Tt,!0)})}function Lt(){return window.addEventListener("blur",Et,!1),document.documentElement.addEventListener("keydown",At,!0),document.documentElement.addEventListener("keyup",Ot,!0),Fi.forEach(function(e){document.documentElement.addEventListener(e,St,!0)}),Ri.forEach(function(e){document.documentElement.addEventListener(e,Tt,!0)}),{get:It}}function Nt(e){return e.hasAttribute("autofocus")}function kt(e){return e.tabIndex<=0}function Mt(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.sequence,r=e.strategy,o=e.ignoreAutofocus,a=e.defaultToContext,u=e.includeOnlyTabbable,s=-1;n||(t=i(t||document.body)[0],n=ge({context:t,includeOnlyTabbable:u,strategy:r})),n.length&&!o&&(s=n.findIndex(Nt)),n.length&&-1===s&&(s=n.findIndex(kt));var l=Mr.rules.except({onlyTabbable:u});return-1===s&&a&&t&&l(t)?t:n[s]||null}function _t(e){var t=e.getAttribute&&e.getAttribute("class")||"";return""===t?[]:t.split(" ")}function Pt(e,t,n){var r=_t(e),i=r.indexOf(t),o=-1!==i,a=void 0!==n?n:!o;

  a!==o&&(a||r.splice(i,1),a&&r.push(t),e.setAttribute("class",r.join(" ")))}function Ft(e,t){return Pt(e,t,!1)}function Rt(e,t){return Pt(e,t,!0)}function Bt(e){var t="";if(e.type===Hi||"shadow-focus"===e.type){var n=qi.get();t=Zi||n.pointer&&"pointer"||n.key&&"key"||"script"}else"initial"===e.type&&(t="initial");document.documentElement.setAttribute("data-focus-source",t),e.type!==ji&&(Vi[t]||Rt(document.documentElement,"focus-source-"+t),Vi[t]=!0,Gi=t)}function Dt(){return Gi}function Wt(e){return Vi[e]}function Ht(e){Zi=e}function jt(){Zi=!1}function qt(){Bt({type:ji}),Gi=Zi=null,Object.keys(Vi).forEach(function(e){Ft(document.documentElement,"focus-source-"+e),Vi[e]=!1}),qi.disengage(),Ki&&Ki.disengage(),document.removeEventListener("shadow-focus",Bt,!0),document.documentElement.removeEventListener(Hi,Bt,!0),document.documentElement.removeEventListener(ji,Bt,!0),document.documentElement.removeAttribute("data-focus-source")}function Kt(){return Ki=zr(),document.addEventListener("shadow-focus",Bt,!0),document.documentElement.addEventListener(Hi,Bt,!0),document.documentElement.addEventListener(ji,Bt,!0),qi=Bi(),Bt({type:"initial"}),{used:Wt,current:Dt,lock:Ht,unlock:jt}}function Gt(e){var t=e||Ve();Ui.cssShadowPiercingDeepCombinator||(t=t.slice(-1));var n=[].slice.call(document.querySelectorAll(Qi),0),r=t.map(function(e){return D({context:e})}).reduce(function(e,t){return t.concat(e)},[]);n.forEach(function(e){-1===r.indexOf(e)&&Ft(e,Ji)}),r.forEach(function(e){-1===n.indexOf(e)&&Rt(e,Ji)})}function Zt(){Yi=(window.setImmediate||window.setTimeout)(function(){Gt()})}function Vt(){(window.clearImmediate||window.clearTimeout)(Yi),Gt()}function $t(e){Gt(e.detail.elements)}function Ut(){eo&&eo.disengage(),(window.clearImmediate||window.clearTimeout)(Yi),document.removeEventListener(zi,Zt,!0),document.removeEventListener(Xi,Vt,!0),document.removeEventListener("shadow-focus",$t,!0),[].forEach.call(document.querySelectorAll(Qi),function(e){Ft(e,Ji)})}function Xt(){Ui||(Ui=E(),Qi=$("."+Ji)),eo=zr(),document.addEventListener(zi,Zt,!0),document.addEventListener(Xi,Vt,!0),document.addEventListener("shadow-focus",$t,!0),Gt()}function zt(e,t){var n=Math.max(e.top,t.top),r=Math.max(e.left,t.left),i=Math.max(Math.min(e.right,t.right),r),o=Math.max(Math.min(e.bottom,t.bottom),n);return{top:n,right:i,bottom:o,left:r,width:i-r,height:o-n}}function Jt(){var e=window.innerWidth||document.documentElement.clientWidth,t=window.innerHeight||document.documentElement.clientHeight;return{top:0,right:e,bottom:t,left:0,width:e,height:t}}function Qt(e){var t=e.getBoundingClientRect(),n=e.offsetWidth-e.clientWidth,r=e.offsetHeight-e.clientHeight,i={top:t.top,left:t.left,right:t.right-n,bottom:t.bottom-r,width:t.width-n,height:t.height-r,area:0};return i.area=i.width*i.height,i}function Yt(e){var t=window.getComputedStyle(e,null),n="visible";return t.getPropertyValue("overflow-x")!==n&&t.getPropertyValue("overflow-y")!==n}function en(e){return Yt(e)?e.offsetHeight<e.scrollHeight||e.offsetWidth<e.scrollWidth:!1}function tn(e){var t=D({context:e}).slice(1).filter(en);return t.length?t.reduce(function(e,t){var n=Qt(t),r=zt(n,e);return r.area=Math.min(n.area,e.area),r},Qt(t[0])):null}function nn(e){var t=e.getBoundingClientRect(),n=Jt();n.area=n.width*n.height;var r=n,i=tn(e);if(i){if(!i.width||!i.height)return 0;r=zt(i,n),r.area=i.area}var o=zt(t,r);if(!o.width||!o.height)return 0;var a=t.width*t.height,u=Math.min(a,r.area),s=Math.round(o.width)*Math.round(o.height)/u,l=1e4,c=Math.round(s*l)/l;return Math.min(c,1)}function rn(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.area;if("function"!=typeof n)throw new TypeError("when/visible-area requires options.callback to be a function");"number"!=typeof r&&(r=1);var i=o({label:"when/visible-area",context:t}),a=void 0,u=null,s=function(){a&&cancelAnimationFrame(a)},l=function(){return!Ir(i)||nn(i)<r||n(i)===!1},c=function(){return l()?void u():void s()};return u=function(){a=requestAnimationFrame(c)},u(),{disengage:s}}function on(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=e.context,n=e.callback,r=e.area;if("function"!=typeof n)throw new TypeError("when/focusable requires options.callback to be a function");var i=o({label:"when/focusable",context:t}),a=function(e){return Mr(e)?n(e):!1},s=u(i),l=rn({context:i,callback:a,area:r}),c=function d(){s.removeEventListener("focus",d,!0),l&&l.disengage()};return s.addEventListener("focus",c,!0),{disengage:c}}var an=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}(),un=r(e("platform")),sn=(e("array.prototype.findindex"),r(e("css.escape"))),ln=JSON.parse(JSON.stringify(un)),cn=ln.os.family||"",dn="Android"===cn,fn="Windows"===cn.slice(0,7),mn="OS X"===cn,bn="iOS"===cn,vn="Blink"===ln.layout,hn="Gecko"===ln.layout,gn="Trident"===ln.layout,pn="WebKit"===ln.layout,xn=parseFloat(ln.version),yn=Math.floor(xn);ln.majorVersion=yn,ln.is={ANDROID:dn,WINDOWS:fn,OSX:mn,IOS:bn,BLINK:vn,GECKO:hn,TRIDENT:gn,WEBKIT:pn,IE9:gn&&9===yn,IE10:gn&&10===yn,IE11:gn&&11===yn};var wn="1.3.0",En="undefined"!=typeof window&&window.navigator.userAgent||"",Sn="ally-supports-cache",Tn=v(Sn);(Tn.userAgent!==En||Tn.version!==wn)&&(Tn={}),Tn.userAgent=En,Tn.version=wn;var An={get:function(){return Tn},set:function(e){Object.keys(e).forEach(function(t){Tn[t]=e[t]}),Tn.time=(new Date).toISOString(),h(Sn,Tn)}},On="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7",In={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" tabindex="-1" alt="" src="'+On+'">',e.querySelector("area")}},Cn={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area href="#void" tabindex="-1" shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" alt="" src="'+On+'">',!1},validate:function(e,t){if(ln.is.GECKO)return!0;var n=e.querySelector("area");return n.focus(),t.activeElement===n}},Ln={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-area-href-test"><area shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-area-href-test" alt="" src="'+On+'">',e.querySelector("area")},validate:function(e,t){if(ln.is.GECKO)return!0;var n=e.querySelector("area");return t.activeElement===n}},Nn={name:"can-focus-audio-without-controls",element:"audio",mutate:function(e){try{e.setAttribute("src",On)}catch(t){}}},kn="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ",Mn={element:"div",mutate:function(e){return e.innerHTML='<map name="broken-image-map-test"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#broken-image-map-test" alt="" src="'+kn+'">',e.querySelector("area")}},_n={element:"div",mutate:function(e){return e.setAttribute("tabindex","-1"),e.setAttribute("style","display: -webkit-flex; display: -ms-flexbox; display: flex;"),e.innerHTML='<span style="display: block;">hello</span>',e.querySelector("span")}},Pn={element:"fieldset",mutate:function(e){e.setAttribute("tabindex",0),e.setAttribute("disabled","disabled")}},Fn={element:"fieldset",mutate:function(e){e.innerHTML="<legend>legend</legend><p>content</p>"}},Rn={element:"span",mutate:function(e){e.setAttribute("style","display: -webkit-flex; display: -ms-flexbox; display: flex;"),e.innerHTML='<span style="display: block;">hello</span>'}},Bn={element:"form",mutate:function(e){e.setAttribute("tabindex",0),e.setAttribute("disabled","disabled")}},Dn={element:"a",mutate:function(e){return e.href="#void",e.innerHTML='<img ismap src="'+On+'" alt="">',e.querySelector("img")}},Wn={element:"div",mutate:function(e){return e.innerHTML='<map name="image-map-tabindex-test"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#image-map-tabindex-test" tabindex="-1" alt="" src="'+On+'">',e.querySelector("img")}},Hn={element:function(e,t){var n=t.createElement("iframe");e.appendChild(n);var r=n.contentWindow.document;return r.open(),r.close(),n},mutate:function(e){e.style.visibility="hidden";var t=e.contentWindow.document,n=t.createElement("input");return t.body.appendChild(n),n},validate:function(e){var t=e.contentWindow.document,n=t.querySelector("input");return t.activeElement===n}},jn=!ln.is.WEBKIT,qn={element:"div",mutate:function(e){e.setAttribute("tabindex","invalid-value")}},Kn={element:"label",mutate:function(e){e.setAttribute("tabindex","-1")},validate:function(e,t){e.offsetHeight;return e.focus(),t.activeElement===e}},Gn="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBpZD0ic3ZnIj48dGV4dCB4PSIxMCIgeT0iMjAiIGlkPSJzdmctbGluay10ZXh0Ij50ZXh0PC90ZXh0Pjwvc3ZnPg==",Zn={element:"object",mutate:function(e){e.setAttribute("type","image/svg+xml"),e.setAttribute("data",Gn),e.setAttribute("width","200"),e.setAttribute("height","50"),e.style.visibility="hidden"}},Vn={name:"can-focus-object-svg",element:"object",mutate:function(e){e.setAttribute("type","image/svg+xml"),e.setAttribute("data",Gn),e.setAttribute("width","200"),e.setAttribute("height","50")},validate:function(e,t){return ln.is.GECKO?!0:t.activeElement===e}},$n=!ln.is.TRIDENT||!ln.is.IE9,Un={element:"div",mutate:function(e){return e.innerHTML='<map name="focus-redirect-img-usemap"><area href="#void" shape="rect" coords="63,19,144,45"></map><img usemap="#focus-redirect-img-usemap" alt="" src="'+On+'">',e.querySelector("img")},validate:function(e,t){var n=e.querySelector("area");return t.activeElement===n}},Xn={element:"fieldset",mutate:function(e){return e.innerHTML='<legend>legend</legend><input tabindex="-1"><input tabindex="0">',!1},validate:function(e,t){var n=e.querySelector('input[tabindex="-1"]'),r=e.querySelector('input[tabindex="0"]');return e.focus(),e.querySelector("legend").focus(),t.activeElement===n&&"focusable"||t.activeElement===r&&"tabbable"||""}},zn={element:"div",mutate:function(e){return e.setAttribute("style","width: 100px; height: 50px; overflow: auto;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>',e.querySelector("div")}},Jn={element:"div",mutate:function(e){e.setAttribute("style","width: 100px; height: 50px;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>'}},Qn={element:"div",mutate:function(e){e.setAttribute("style","width: 100px; height: 50px; overflow: auto;"),e.innerHTML='<div style="width: 500px; height: 40px;">scrollable content</div>'}},Yn={element:"details",mutate:function(e){return e.innerHTML="<summary>foo</summary><p>content</p>",e.firstElementChild}},er={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><text focusable="true">a</text></svg>',e.querySelector("text")},validate:function(e,t){var n=e.querySelector("text");return ln.is.TRIDENT?!0:t.activeElement===n}},tr={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><text tabindex="-1">a</text></svg>',e.querySelector("text")}},nr={element:"div",mutate:function(e){return e.innerHTML='<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>',e.firstChild},validate:function(e,t){var n=e.firstChild;return ln.is.TRIDENT?!0:t.activeElement===n}},rr={element:"div",mutate:function(e){e.setAttribute("tabindex","3x")}},ir={element:"table",mutate:function(e,t,n){var r=n.createDocumentFragment();r.innerHTML="<tr><td>cell</td></tr>",e.appendChild(r)}},or={element:"video",mutate:function(e){try{e.setAttribute("src",On)}catch(t){}}},ar=ln.is.GECKO||ln.is.TRIDENT,ur={cssShadowPiercingDeepCombinator:g,focusInZeroDimensionObject:p,focusObjectSwf:x,tabsequenceAreaAtImgPosition:y},sr={focusAreaImgTabindex:In,focusAreaTabindex:Cn,focusAreaWithoutHref:Ln,focusAudioWithoutControls:Nn,focusBrokenImageMap:Mn,focusChildrenOfFocusableFlexbox:_n,focusFieldsetDisabled:Pn,focusFieldset:Fn,focusFlexboxContainer:Rn,focusFormDisabled:Bn,focusImgIsmap:Dn,focusImgUsemapTabindex:Wn,focusInHiddenIframe:Hn,focusInvalidTabindex:qn,focusLabelTabindex:Kn,focusObjectSvg:Vn,focusObjectSvgHidden:Zn,focusRedirectImgUsemap:Un,focusRedirectLegend:Xn,focusScrollBody:zn,focusScrollContainerWithoutOverflow:Jn,focusScrollContainer:Qn,focusSummary:Yn,focusSvgFocusableAttribute:er,focusSvgTabindexAttribute:tr,focusSvg:nr,focusTabindexTrailingCharacters:rr,focusTable:ir,focusVideoWithoutControls:or},lr=null,cr=void 0,dr=/^\s*(-|\+)?[0-9]+\s*$/,fr=/^\s*(-|\+)?[0-9]+.*$/,mr=void 0,br=void 0,vr={input:!0,select:!0,textarea:!0,button:!0,fieldset:!0,form:!0},hr=function(){},gr={log:hr,debug:hr,info:hr,warn:hr,error:hr},pr="undefined"!=typeof console?console:gr,xr=void 0,yr=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector"],wr=null,Er=void 0;Z.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return Z({context:t,except:e})};return t.rules=Z,t};var Sr=Z.except({}),Tr=void 0,Ar=void 0,Or=/^(area)$/;ee.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return ee({context:t,except:e})};return t.rules=ee,t};var Ir=ee.except({}),Cr=void 0,Lr=void 0;se.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return se({context:t,except:e})};return t.rules=se,t};var Nr=se.except({}),kr=void 0;ce.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return ce({context:t,except:e})};return t.rules=ce,t};var Mr=ce.except({}),_r=de(Sr),Pr=void 0,Fr=void 0,Rr=/^(fieldset|table|td|body)$/;he.except=function(){var e=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],t=function(t){return he({context:t,except:e})};return t.rules=he,t};var Br=Sr.rules.except({flexbox:!0}),Dr=he.except({flexbox:!0}),Wr=he.except({}),Hr=void 0,jr={flexbox:!0,scrollable:!0,onlyTabbable:!0},qr={blur:c,disabled:B,focus:Pe};"undefined"!=typeof window&&function(){for(var e=0,t=["ms","moz","webkit","o"],n="",r="",i=0,o=t.length;o>i;++i)n=window[t[i]+"RequestAnimationFrame"],r=window[t[i]+"CancelAnimationFrame"]||window[t[i]+"CancelRequestAnimationFrame"];"function"!=typeof window.requestAnimationFrame&&(window.requestAnimationFrame=window[n]||function(t){var n=(new Date).getTime(),r=Math.max(0,16-(n-e)),i=window.setTimeout(function(){t(n+r)},r);return e=n+r,i}),"function"!=typeof window.cancelAnimationFrame&&(window.cancelAnimationFrame=window[r]||function(e){clearTimeout(e)})}();var Kr="undefined"!=typeof window&&window.CustomEvent||function(){};"function"!=typeof Kr&&(Kr=function(e,t){var n=document.createEvent("CustomEvent");return!t&&(t={bubbles:!1,cancelable:!1,detail:void 0}),n.initCustomEvent(e,t.bubbles,t.cancelable,t.detail),n},Kr.prototype=window.Event.prototype);var Gr=Kr,Zr=void 0,Vr=void 0,$r=De({engage:He,disengage:je}),Ur=void 0,Xr=void 0;"undefined"!=typeof document&&document.documentElement.createShadowRoot?!function(){var e=void 0,t=void 0,n=function(){i(),(window.clearImmediate||window.clearTimeout)(e),e=(window.setImmediate||window.setTimeout)(function(){o()})},r=function(e){e.addEventListener("blur",n,!0),t=e},i=function(){t&&t.removeEventListener("blur",n,!0),t=null},o=function(){var e=Ve();if(1===e.length)return void i();r(e[0]);var t=new CustomEvent("shadow-focus",{bubbles:!1,cancelable:!1,detail:{elements:e,active:e[0],hosts:e.slice(1)}});document.dispatchEvent(t)},a=function(){(window.clearImmediate||window.clearTimeout)(e),o()};Ur=function(){document.addEventListener("focus",a,!0)},Xr=function(){(window.clearImmediate||window.clearTimeout)(e),t&&t.removeEventListener("blur",n,!0),document.removeEventListener("focus",a,!0)}}():Ur=Xr=function(){};var zr=De({engage:Ur,disengage:Xr}),Jr={activeElement:$r,shadowFocus:zr},Qr=void 0,Yr=void 0,ei=ln.is.TRIDENT&&(ln.is.IE10||ln.is.IE11);ei?!function(){var e=function(e){var t=ke({context:e.target,except:{flexbox:!0,scrollable:!0}});if(t&&t!==e.target){window.setImmediate(function(){t.focus()});var n=[].map.call(t.children,function(e){var t=e.style.visibility||"",n=e.style.transition||"";return e.style.visibility="hidden",e.style.transition="none",[e,t,n]});window.setImmediate(function(){n.forEach(function(e){e[0].style.visibility=e[1],e[0].style.transition=e[2]})})}};Qr=function(t){t.addEventListener("mousedown",e,!0)},Yr=function(t){t.removeEventListener("mousedown",e,!0)}}():Qr=function(){};var ti=ze({engage:Qr,disengage:Yr}),ni=void 0,ri=void 0,ii=ln.is.OSX&&(ln.is.GECKO||ln.is.WEBKIT);ii?!function(){var e=function(e){if(!e.defaultPrevented&&H(e.target,"input, button, button *")){var t=ke({context:e.target});(window.setImmediate||window.setTimeout)(function(){t.focus()})}},t=function(e){if(!e.defaultPrevented&&H(e.target,"label, label *")){var t=ke({context:e.target});t&&t.focus()}};ni=function(n){n.addEventListener("mousedown",e,!1),n.addEventListener("mouseup",t,!1)},ri=function(n){n.removeEventListener("mousedown",e,!1),n.removeEventListener("mouseup",t,!1)}}():ni=function(){};var oi=ze({engage:ni,disengage:ri}),ai=void 0,ui=void 0,si=ln.is.WEBKIT;si?!function(){var e=function(e){var t=ke({context:e.target});!t||t.hasAttribute("tabindex")&&S(t)||(t.setAttribute("tabindex",0),(window.setImmediate||window.setTimeout)(function(){t.removeAttribute("tabindex")},0))};ai=function(t){t.addEventListener("mousedown",e,!0),t.addEventListener("touchstart",e,!0)},ui=function(t){t.removeEventListener("mousedown",e,!0),t.removeEventListener("touchstart",e,!0)}}():ai=function(){};var li=ze({engage:ai,disengage:ui}),ci={pointerFocusChildren:ti,pointerFocusInput:oi,pointerFocusParent:li},di={activeElement:Je,activeElements:Ve,focusRedirectTarget:Ne,focusTarget:ke,insignificantBranches:et,parents:D,shadowHostParents:Ke,shadowHost:a},fi={activeElement:s,disabled:ue,focusRelevant:Sr,focusable:Mr,onlyTabbable:Nr,shadowed:qe,tabbable:Wr,validArea:ie,validTabindex:S,visible:Ir},mi=function(e){return e.shadowRoot?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_SKIP};mi.acceptNode=mi;for(var bi={childList:!0,subtree:!0},vi=function(){function e(){var t=this,r=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],i=r.context,o=r.callback,a=r.config;n(this,e),this.config=a,this.disengage=this.disengage.bind(this),this.clientObserver=new MutationObserver(o),this.hostObserver=new MutationObserver(function(e){return e.forEach(t.handleHostMutation,t)}),this.observeContext(i),this.observeShadowHosts(i)}return an(e,[{key:"disengage",value:function(){this.clientObserver&&this.clientObserver.disconnect(),this.clientObserver=null,this.hostObserver&&this.hostObserver.disconnect(),this.hostObserver=null}},{key:"observeShadowHosts",value:function(e){var t=this,n=tt({context:e});n.forEach(function(e){return t.observeContext(e.shadowRoot)})}},{key:"observeContext",value:function(e){this.clientObserver.observe(e,this.config),this.hostObserver.observe(e,bi)}},{key:"handleHostMutation",value:function(e){if("childList"===e.type){var t=i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE});t.forEach(this.observeShadowHosts,this)}}}]),e}(),hi={attributes:!0,childList:!0,subtree:!0,attributeFilter:["tabindex","disabled","data-ally-disabled"]},gi=function(){function e(){var t=this,r=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],o=r.context,a=r.filter;n(this,e),this._context=i(o||document.documentElement)[0],this._filter=i(a),this._inertElementCache=[],this.disengage=this.disengage.bind(this),this.handleMutation=this.handleMutation.bind(this),this.renderInert=this.renderInert.bind(this),this.filterElements=this.filterElements.bind(this),this.filterParentElements=this.filterParentElements.bind(this);var u=ve({context:this._context,includeContext:!0,strategy:"all"});this.renderInert(u),this.shadowObserver=nt({context:this._context,config:hi,callback:function(e){return e.forEach(t.handleMutation)}})}return an(e,[{key:"disengage",value:function(){this._context&&(it(this._context),this._inertElementCache.forEach(function(e){return it(e)}),this._inertElementCache=null,this._filter=null,this._context=null,this.shadowObserver&&this.shadowObserver.disengage(),this.shadowObserver=null)}},{key:"listQueryFocusable",value:function(e){return e.map(function(e){return ve({context:e,includeContext:!0,strategy:"all"})}).reduce(function(e,t){return e.concat(t)},[])}},{key:"renderInert",value:function(e){var t=this,n=function(e){t._inertElementCache.push(e),rt(e)};e.filter(this.filterElements).filter(this.filterParentElements).filter(function(e){return!B(e)}).forEach(n)}},{key:"filterElements",value:function(e){var t=Qe({element:e,includeSelf:!0});return!this._filter.some(t)}},{key:"filterParentElements",value:function(e){var t=Qe({parent:e});return!this._filter.some(t)}},{key:"handleMutation",value:function(e){if("childList"===e.type){var t=i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE});if(!t.length)return;var n=this.listQueryFocusable(t);this.renderInert(n)}else"attributes"===e.type&&this.renderInert([e.target])}}]),e}(),pi={attributes:!1,childList:!0,subtree:!0},xi=function(){function e(){var t=arguments.length<=0||void 0===arguments[0]?{}:arguments[0],r=t.context,o=t.filter;n(this,e),this._context=i(r||document.documentElement)[0],this._filter=i(o),this.disengage=this.disengage.bind(this),this.handleMutation=this.handleMutation.bind(this),this.isInsignificantBranch=this.isInsignificantBranch.bind(this);var a=et({context:this._context,filter:this._filter});a.forEach(at),this.startObserver()}return an(e,[{key:"disengage",value:function(){this._context&&([].forEach.call(this._context.querySelectorAll("[data-cached-aria-hidden]"),ut),this._context=null,this._filter=null,this._observer&&this._observer.disconnect(),this._observer=null)}},{key:"startObserver",value:function(){var e=this;window.MutationObserver&&(this._observer=new MutationObserver(function(t){return t.forEach(e.handleMutation)}),this._observer.observe(this._context,pi))}},{key:"handleMutation",value:function(e){"childList"===e.type&&i(e.addedNodes).filter(function(e){return e.nodeType===Node.ELEMENT_NODE}).filter(this.isInsignificantBranch).forEach(at)}},{key:"isInsignificantBranch",value:function(e){var t=D({context:e});if(t.some(function(e){return"true"===e.getAttribute("aria-hidden")}))return!1;var n=Qe({element:e});return this._filter.some(n)?!1:!0}}]),e}(),yi=function(){function e(t){n(this,e),this._document=u(t),this.maps={}}return an(e,[{key:"getAreasFor",value:function(e){return this.maps[e]||this.addMapByName(e),this.maps[e]}},{key:"addMapByName",value:function(e){var t=te(e,this._document);t&&(this.maps[t.name]=ge({context:t}))}},{key:"extractAreasFromList",value:function(e){return e.filter(function(e){var t=e.nodeName.toLowerCase();if("area"!==t)return!0;var n=e.parentNode;return this.maps[n.name]||(this.maps[n.name]=[]),this.maps[n.name].push(e),!1},this)}}]),e}(),wi=function(){function e(t,r){n(this,e),this.context=t,this.sortElements=r,this.hostCounter=1,this.inHost={},this.inDocument=[],this.hosts={},this.elements={}}return an(e,[{key:"_registerHost",value:function(e){if(!e._sortingId){e._sortingId="shadow-"+this.hostCounter++,this.hosts[e._sortingId]=e;var t=a({context:e});t?(this._registerHost(t),this._registerHostParent(e,t)):this.inDocument.push(e)}}},{key:"_registerHostParent",value:function(e,t){this.inHost[t._sortingId]||(this.inHost[t._sortingId]=[]),this.inHost[t._sortingId].push(e)}},{key:"_registerElement",value:function(e,t){this.elements[t._sortingId]||(this.elements[t._sortingId]=[]),this.elements[t._sortingId].push(e)}},{key:"extractElements",value:function(e){return e.filter(function(e){var t=a({context:e});return t?(this._registerHost(t),this._registerElement(e,t),!1):!0},this)}},{key:"sort",value:function(e){var t=this._injectHosts(e);return t=this._replaceHosts(t),this._cleanup(),t}},{key:"_injectHosts",value:function(e){return Object.keys(this.hosts).forEach(function(e){var t=this.elements[e],n=this.inHost[e],r=this.hosts[e].shadowRoot;this.elements[e]=this._merge(t,n,r)},this),this._merge(e,this.inDocument,this.context)}},{key:"_merge",value:function(e,t,n){var r=Se({list:e,elements:t});return this.sortElements(r,n)}},{key:"_replaceHosts",value:function(e){return Se({list:e,elements:this.inDocument,resolveElement:this._resolveHostElement.bind(this)})}},{key:"_resolveHostElement",value:function(e){var t=Se({list:this.elements[e._sortingId],elements:this.inHost[e._sortingId],resolveElement:this._resolveHostElement.bind(this)}),n=T(e);return null!==n&&n>-1?[e].concat(t):t}},{key:"_cleanup",value:function(){Object.keys(this.hosts).forEach(function(e){delete this.hosts[e]._sortingId},this)}}]),e}(),Ei=void 0,Si={tab:9,left:37,up:38,right:39,down:40,pageUp:33,"page-up":33,pageDown:34,"page-down":34,end:35,home:36,enter:13,escape:27,space:32,shift:16,capsLock:20,"caps-lock":20,ctrl:17,alt:18,meta:91,pause:19,insert:45,"delete":46,backspace:8,_alias:{91:[92,93,224]}},Ti=1;26>Ti;Ti++)Si["f"+Ti]=Ti+111;for(var Ti=0;10>Ti;Ti++){var Ai=Ti+48,Oi=Ti+96;Si[Ti]=Ai,Si["num-"+Ti]=Oi,Si._alias[Ai]=[Oi]}for(var Ti=0;26>Ti;Ti++){var Ai=Ti+65,Ii=String.fromCharCode(Ai).toLowerCase();Si[Ii]=Ai}var Ci={alt:"altKey",ctrl:"ctrlKey",meta:"metaKey",shift:"shiftKey"},Li=Object.keys(Ci).map(function(e){return Ci[e]}),Ni={disabled:ot,hidden:st,tabFocus:wt},ki={"aria-busy":{"default":"false",values:["true","false"]},"aria-checked":{"default":void 0,values:["true","false","mixed",void 0]},"aria-disabled":{"default":"false",values:["true","false"]},"aria-expanded":{"default":void 0,values:["true","false",void 0]},"aria-grabbed":{"default":void 0,values:["true","false",void 0]},"aria-hidden":{"default":"false",values:["true","false"]},"aria-invalid":{"default":"false",values:["true","false","grammar","spelling"]},"aria-pressed":{"default":void 0,values:["true","false","mixed",void 0]},"aria-selected":{"default":void 0,values:["true","false",void 0]},"aria-atomic":{"default":"false",values:["true","false"]},"aria-autocomplete":{"default":"none",values:["inline","list","both","none"]},"aria-dropeffect":{"default":"none",multiple:!0,values:["copy","move","link","execute","popup","none"]},"aria-haspopup":{"default":"false",values:["true","false"]},"aria-live":{"default":"off",values:["off","polite","assertive"]},"aria-multiline":{"default":"false",values:["true","false"]},"aria-multiselectable":{"default":"false",values:["true","false"]},"aria-orientation":{"default":"horizontal",values:["vertical","horizontal"]},"aria-readonly":{"default":"false",values:["true","false"]},"aria-relevant":{"default":"additions text",multiple:!0,values:["additions","removals","text","all"]},"aria-required":{"default":"false",values:["true","false"]},"aria-sort":{"default":"none",other:!0,values:["ascending","descending","none"]}},Mi={attribute:ki,keycode:Si},_i=0,Pi=0,Fi=["touchstart","pointerdown","MSPointerDown","mousedown"],Ri=["touchend","touchcancel","pointerup","MSPointerUp","pointercancel","MSPointerCancel","mouseup"],Bi=De({engage:Lt,disengage:Ct}),Di={interactionType:Bi,shadowMutations:nt},Wi={firstTabbable:Mt,focusable:ve,shadowHosts:tt,tabbable:ge,tabsequence:bt},Hi="undefined"!=typeof document&&("onfocusin"in document?"focusin":"focus"),ji="undefined"!=typeof document&&("onfocusin"in document?"focusout":"blur"),qi=void 0,Ki=void 0,Gi=null,Zi=null,Vi={pointer:!1,key:!1,script:!1,initial:!1},$i=De({engage:Kt,disengage:qt}),Ui=void 0,Xi="undefined"!=typeof document&&("onfocusin"in document?"focusin":"focus"),zi="undefined"!=typeof document&&("onfocusin"in document?"focusout":"blur"),Ji="ally-focus-within",Qi=void 0,Yi=void 0,eo=void 0,to=De({engage:Xt,disengage:Ut}),no={focusSource:$i,focusWithin:to},ro={focusable:on,key:yt,visibleArea:rn},io="undefined"!=typeof window&&window.ally,oo={element:qr,event:Jr,fix:ci,get:di,is:fi,maintain:Ni,map:Mi,observe:Di,query:Wi,style:no,when:ro,version:wn,noConflict:function(){return"undefined"!=typeof window&&window.ally===this&&(window.ally=io),this}};t.exports=oo},{"array.prototype.findindex":2,"css.escape":3,platform:4}],2:[function(){!function(){if(!Array.prototype.findIndex){var e=function(e){var t=Object(this),n=Math.max(0,t.length)>>>0;if(0===n)return-1;if("function"!=typeof e||"[object Function]"!==Object.prototype.toString.call(e))throw new TypeError("Array#findIndex: predicate must be a function");for(var r=arguments.length>1?arguments[1]:void 0,i=0;n>i;i++)if(e.call(r,t[i],i,t))return i;return-1};if(Object.defineProperty)try{Object.defineProperty(Array.prototype,"findIndex",{value:e,configurable:!0,writable:!0})}catch(t){}Array.prototype.findIndex||(Array.prototype.findIndex=e)}}(this)},{}],3:[function(t,n,r){(function(t){!function(t,i){"object"==typeof r?n.exports=i(t):"function"==typeof e&&e.amd?e([],i.bind(t,t)):i(t)}("undefined"!=typeof t?t:this,function(e){if(e.CSS&&e.CSS.escape)return e.CSS.escape;var t=function(e){if(0==arguments.length)throw new TypeError("`CSS.escape` requires an argument.");for(var t,n=String(e),r=n.length,i=-1,o="",a=n.charCodeAt(0);++i<r;)t=n.charCodeAt(i),o+=0!=t?t>=1&&31>=t||127==t||0==i&&t>=48&&57>=t||1==i&&t>=48&&57>=t&&45==a?"\\"+t.toString(16)+" ":(0!=i||1!=r||45!=t)&&(t>=128||45==t||95==t||t>=48&&57>=t||t>=65&&90>=t||t>=97&&122>=t)?n.charAt(i):"\\"+n.charAt(i):"�";return o};return e.CSS||(e.CSS={}),e.CSS.escape=t,t})}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],4:[function(t,n,r){(function(t){(function(){"use strict";function i(e){return e=String(e),e.charAt(0).toUpperCase()+e.slice(1)}function o(e,t,n){var r={6.4:"10",6.3:"8.1",6.2:"8",6.1:"Server 2008 R2 / 7","6.0":"Server 2008 / Vista",5.2:"Server 2003 / XP 64-bit",5.1:"XP",5.01:"2000 SP1","5.0":"2000","4.0":"NT","4.90":"ME"};return t&&n&&/^Win/i.test(e)&&(r=r[/[\d.]+$/.exec(e)])&&(e="Windows "+r),e=String(e),t&&n&&(e=e.replace(RegExp(t,"i"),n)),e=u(e.replace(/ ce$/i," CE").replace(/\bhpw/i,"web").replace(/\bMacintosh\b/,"Mac OS").replace(/_PowerPC\b/i," OS").replace(/\b(OS X) [^ \d]+/i,"$1").replace(/\bMac (OS X)\b/,"$1").replace(/\/(\d)/," $1").replace(/_/g,".").replace(/(?: BePC|[ .]*fc[ \d.]+)$/i,"").replace(/\bx86\.64\b/gi,"x86_64").replace(/\b(Windows Phone) OS\b/,"$1").split(" on ")[0])}function a(e,t){var n=-1,r=e?e.length:0;if("number"==typeof r&&r>-1&&w>=r)for(;++n<r;)t(e[n],n,e);else s(e,t)}function u(e){return e=m(e),/^(?:webOS|i(?:OS|P))/.test(e)?e:i(e)}function s(e,t){for(var n in e)A.call(e,n)&&t(e[n],n,e)}function l(e){return null==e?i(e):O.call(e).slice(8,-1)}function c(e,t){var n=null!=e?typeof e[t]:"number";return!/^(?:boolean|number|string|undefined)$/.test(n)&&("object"==n?!!e[t]:!0)}function d(e){return String(e).replace(/([ -])(?!$)/g,"$1?")}function f(e,t){var n=null;return a(e,function(r,i){n=t(n,r,i,e)}),n}function m(e){return String(e).replace(/^ +| +$/g,"")}function b(e){function t(t){return f(t,function(t,n){return t||RegExp("\\b"+(n.pattern||d(n))+"\\b","i").exec(e)&&(n.label||n)})}function n(t){return f(t,function(t,n,r){return t||(n[X]||n[/^[a-z]+(?: +[a-z]+\b)*/i.exec(X)]||RegExp("\\b"+d(r)+"(?:\\b|\\w*\\d)","i").exec(e))&&r})}function r(t){return f(t,function(t,n){return t||RegExp("\\b"+(n.pattern||d(n))+"\\b","i").exec(e)&&(n.label||n)})}function i(t){return f(t,function(t,n){var r=n.pattern||d(n);return!t&&(t=RegExp("\\b"+r+"(?:/[\\d.]+|[ \\w.]*)","i").exec(e))&&(t=o(t,r,n.label||n)),t})}function a(t){return f(t,function(t,n){var r=n.pattern||d(n);return!t&&(t=RegExp("\\b"+r+" *\\d+[.\\w_]*","i").exec(e)||RegExp("\\b"+r+"(?:; *(?:[a-z]+[_-])?[a-z]+\\d+|[^ ();-]*)","i").exec(e))&&((t=String(n.label&&!RegExp(r,"i").test(n.label)?n.label:t).split("/"))[1]&&!/[\d.]+/.test(t[0])&&(t[0]+=" "+t[1]),
  n=n.label||n,t=u(t[0].replace(RegExp(r,"i"),n).replace(RegExp("; *(?:"+n+"[_-])?","i")," ").replace(RegExp("("+n+")[-_.]?(\\w)","i"),"$1 $2"))),t})}function v(t){return f(t,function(t,n){return t||(RegExp(n+"(?:-[\\d.]+/|(?: for [\\w-]+)?[ /-])([\\d.]+[^ ();/_-]*)","i").exec(e)||0)[1]||null})}function p(){return this.description||""}var x=h,y=e&&"object"==typeof e&&"String"!=l(e);y&&(x=e,e=null);var w=x.navigator||{},T=w.userAgent||"";e||(e=T);var A,I,C=y||S==g,L=y?!!w.likeChrome:/\bChrome\b/.test(e)&&!/internal|\n/i.test(O.toString()),N="Object",k=y?N:"ScriptBridgingProxyObject",M=y?N:"Environment",_=y&&x.java?"JavaPackage":l(x.java),P=y?N:"RuntimeObject",F=/\bJava/.test(_)&&x.java,R=F&&l(x.environment)==M,B=F?"a":"α",D=F?"b":"β",W=x.document||{},H=x.operamini||x.opera,j=E.test(j=y&&H?H["[[Class]]"]:l(H))?j:H=null,q=e,K=[],G=null,Z=e==T,V=Z&&H&&"function"==typeof H.version&&H.version(),$=t(["Trident",{label:"WebKit",pattern:"AppleWebKit"},"iCab","Presto","NetFront","Tasman","KHTML","Gecko"]),U=r(["Adobe AIR","Arora","Avant Browser","Breach","Camino","Epiphany","Fennec","Flock","Galeon","GreenBrowser","iCab","Iceweasel",{label:"SRWare Iron",pattern:"Iron"},"K-Meleon","Konqueror","Lunascape","Maxthon","Midori","Nook Browser","PhantomJS","Raven","Rekonq","RockMelt","SeaMonkey",{label:"Silk",pattern:"(?:Cloud9|Silk-Accelerated)"},"Sleipnir","SlimBrowser","Sunrise","Swiftfox","WebPositive","Opera Mini",{label:"Opera Mini",pattern:"OPiOS"},"Opera",{label:"Opera",pattern:"OPR"},"Chrome",{label:"Chrome Mobile",pattern:"(?:CriOS|CrMo)"},{label:"Firefox",pattern:"(?:Firefox|Minefield)"},{label:"IE",pattern:"IEMobile"},{label:"IE",pattern:"MSIE"},"Safari"]),X=a([{label:"BlackBerry",pattern:"BB10"},"BlackBerry",{label:"Galaxy S",pattern:"GT-I9000"},{label:"Galaxy S2",pattern:"GT-I9100"},{label:"Galaxy S3",pattern:"GT-I9300"},{label:"Galaxy S4",pattern:"GT-I9500"},"Google TV","Lumia","iPad","iPod","iPhone","Kindle",{label:"Kindle Fire",pattern:"(?:Cloud9|Silk-Accelerated)"},"Nook","PlayBook","PlayStation 4","PlayStation 3","PlayStation Vita","TouchPad","Transformer",{label:"Wii U",pattern:"WiiU"},"Wii","Xbox One",{label:"Xbox 360",pattern:"Xbox"},"Xoom"]),z=n({Apple:{iPad:1,iPhone:1,iPod:1},Amazon:{Kindle:1,"Kindle Fire":1},Asus:{Transformer:1},"Barnes & Noble":{Nook:1},BlackBerry:{PlayBook:1},Google:{"Google TV":1},HP:{TouchPad:1},HTC:{},LG:{},Microsoft:{Xbox:1,"Xbox One":1},Motorola:{Xoom:1},Nintendo:{"Wii U":1,Wii:1},Nokia:{Lumia:1},Samsung:{"Galaxy S":1,"Galaxy S2":1,"Galaxy S3":1,"Galaxy S4":1},Sony:{"PlayStation 4":1,"PlayStation 3":1,"PlayStation Vita":1}}),J=i(["Windows Phone ","Android","CentOS","Debian","Fedora","FreeBSD","Gentoo","Haiku","Kubuntu","Linux Mint","Red Hat","SuSE","Ubuntu","Xubuntu","Cygwin","Symbian OS","hpwOS","webOS ","webOS","Tablet OS","Linux","Mac OS X","Macintosh","Mac","Windows 98;","Windows "]);if($&&($=[$]),z&&!X&&(X=a([z])),(A=/\bGoogle TV\b/.exec(X))&&(X=A[0]),/\bSimulator\b/i.test(e)&&(X=(X?X+" ":"")+"Simulator"),"Opera Mini"==U&&/\bOPiOS\b/.test(e)&&K.push("running in Turbo/Uncompressed mode"),/^iP/.test(X)?(U||(U="Safari"),J="iOS"+((A=/ OS ([\d_]+)/i.exec(e))?" "+A[1].replace(/_/g,"."):"")):"Konqueror"!=U||/buntu/i.test(J)?z&&"Google"!=z&&(/Chrome/.test(U)&&!/\bMobile Safari\b/i.test(e)||/\bVita\b/.test(X))?(U="Android Browser",J=/\bAndroid\b/.test(J)?J:"Android"):(!U||(A=!/\bMinefield\b|\(Android;/i.test(e)&&/\b(?:Firefox|Safari)\b/.exec(U)))&&(U&&!X&&/[\/,]|^[^(]+?\)/.test(e.slice(e.indexOf(A+"/")+8))&&(U=null),(A=X||z||J)&&(X||z||/\b(?:Android|Symbian OS|Tablet OS|webOS)\b/.test(J))&&(U=/[a-z]+(?: Hat)?/i.exec(/\bAndroid\b/.test(J)?J:A)+" Browser")):J="Kubuntu",(A=/\((Mobile|Tablet).*?Firefox\b/i.exec(e))&&A[1]&&(J="Firefox OS",X||(X=A[1])),V||(V=v(["(?:Cloud9|CriOS|CrMo|IEMobile|Iron|Opera ?Mini|OPiOS|OPR|Raven|Silk(?!/[\\d.]+$))","Version",d(U),"(?:Firefox|Minefield|NetFront)"])),"iCab"==$&&parseFloat(V)>3?$=["WebKit"]:"Trident"!=$&&(A=/\bOpera\b/.test(U)&&(/\bOPR\b/.test(e)?"Blink":"Presto")||/\b(?:Midori|Nook|Safari)\b/i.test(e)&&"WebKit"||!$&&/\bMSIE\b/i.test(e)&&("Mac OS"==J?"Tasman":"Trident"))?$=[A]:/\bPlayStation\b(?! Vita\b)/i.test(U)&&"WebKit"==$&&($=["NetFront"]),"IE"==U&&(A=(/; *(?:XBLWP|ZuneWP)(\d+)/i.exec(e)||0)[1])?(U+=" Mobile",J="Windows Phone "+(/\+$/.test(A)?A:A+".x"),K.unshift("desktop mode")):/\bWPDesktop\b/i.test(e)?(U="IE Mobile",J="Windows Phone 8+",K.unshift("desktop mode"),V||(V=(/\brv:([\d.]+)/.exec(e)||0)[1])):"IE"!=U&&"Trident"==$&&(A=/\brv:([\d.]+)/.exec(e))?(/\bWPDesktop\b/i.test(e)||(U&&K.push("identifying as "+U+(V?" "+V:"")),U="IE"),V=A[1]):"Chrome"!=U&&"IE"==U||!(A=/\bEdge\/([\d.]+)/.exec(e))||(U="Microsoft Edge",V=A[1],$=["Trident"]),Z){if(c(x,"global"))if(F&&(A=F.lang.System,q=A.getProperty("os.arch"),J=J||A.getProperty("os.name")+" "+A.getProperty("os.version")),C&&c(x,"system")&&(A=[x.system])[0]){J||(J=A[0].os||null);try{A[1]=x.require("ringo/engine").version,V=A[1].join("."),U="RingoJS"}catch(Q){A[0].global.system==x.system&&(U="Narwhal")}}else"object"==typeof x.process&&(A=x.process)?(U="Node.js",q=A.arch,J=A.platform,V=/[\d.]+/.exec(A.version)[0]):R&&(U="Rhino");else l(A=x.runtime)==k?(U="Adobe AIR",J=A.flash.system.Capabilities.os):l(A=x.phantom)==P?(U="PhantomJS",V=(A=A.version||null)&&A.major+"."+A.minor+"."+A.patch):"number"==typeof W.documentMode&&(A=/\bTrident\/(\d+)/i.exec(e))&&(V=[V,W.documentMode],(A=+A[1]+4)!=V[1]&&(K.push("IE "+V[1]+" mode"),$&&($[1]=""),V[1]=A),V="IE"==U?String(V[1].toFixed(1)):V[0]);J=J&&u(J)}V&&(A=/(?:[ab]|dp|pre|[ab]\d+pre)(?:\d+\+?)?$/i.exec(V)||/(?:alpha|beta)(?: ?\d)?/i.exec(e+";"+(Z&&w.appMinorVersion))||/\bMinefield\b/i.test(e)&&"a")&&(G=/b/i.test(A)?"beta":"alpha",V=V.replace(RegExp(A+"\\+?$"),"")+("beta"==G?D:B)+(/\d+\+?/.exec(A)||"")),"Fennec"==U||"Firefox"==U&&/\b(?:Android|Firefox OS)\b/.test(J)?U="Firefox Mobile":"Maxthon"==U&&V?V=V.replace(/\.[\d.]+/,".x"):"Silk"==U?(/\bMobi/i.test(e)||(J="Android",K.unshift("desktop mode")),/Accelerated *= *true/i.test(e)&&K.unshift("accelerated")):/\bXbox\b/i.test(X)?(J=null,"Xbox 360"==X&&/\bIEMobile\b/.test(e)&&K.unshift("mobile mode")):!/^(?:Chrome|IE|Opera)$/.test(U)&&(!U||X||/Browser|Mobi/.test(U))||"Windows CE"!=J&&!/Mobi/i.test(e)?"IE"==U&&Z&&null===x.external?K.unshift("platform preview"):(/\bBlackBerry\b/.test(X)||/\bBB10\b/.test(e))&&(A=(RegExp(X.replace(/ +/g," *")+"/([.\\d]+)","i").exec(e)||0)[1]||V)?(A=[A,/BB10/.test(e)],J=(A[1]?(X=null,z="BlackBerry"):"Device Software")+" "+A[0],V=null):this!=s&&"Wii"!=X&&(Z&&H||/Opera/.test(U)&&/\b(?:MSIE|Firefox)\b/i.test(e)||"Firefox"==U&&/\bOS X (?:\d+\.){2,}/.test(J)||"IE"==U&&(J&&!/^Win/.test(J)&&V>5.5||/\bWindows XP\b/.test(J)&&V>8||8==V&&!/\bTrident\b/.test(e)))&&!E.test(A=b.call(s,e.replace(E,"")+";"))&&A.name&&(A="ing as "+A.name+((A=A.version)?" "+A:""),E.test(U)?(/\bIE\b/.test(A)&&"Mac OS"==J&&(J=null),A="identify"+A):(A="mask"+A,U=j?u(j.replace(/([a-z])([A-Z])/g,"$1 $2")):"Opera",/\bIE\b/.test(A)&&(J=null),Z||(V=null)),$=["Presto"],K.push(A)):U+=" Mobile",(A=(/\bAppleWebKit\/([\d.]+\+?)/i.exec(e)||0)[1])&&(A=[parseFloat(A.replace(/\.(\d)$/,".0$1")),A],"Safari"==U&&"+"==A[1].slice(-1)?(U="WebKit Nightly",G="alpha",V=A[1].slice(0,-1)):(V==A[1]||V==(A[2]=(/\bSafari\/([\d.]+\+?)/i.exec(e)||0)[1]))&&(V=null),A[1]=(/\bChrome\/([\d.]+)/i.exec(e)||0)[1],537.36==A[0]&&537.36==A[2]&&parseFloat(A[1])>=28&&"IE"!=U&&"Microsoft Edge"!=U&&($=["Blink"]),Z&&(L||A[1])?($&&($[1]="like Chrome"),A=A[1]||(A=A[0],530>A?1:532>A?2:532.05>A?3:533>A?4:534.03>A?5:534.07>A?6:534.1>A?7:534.13>A?8:534.16>A?9:534.24>A?10:534.3>A?11:535.01>A?12:535.02>A?"13+":535.07>A?15:535.11>A?16:535.19>A?17:536.05>A?18:536.1>A?19:537.01>A?20:537.11>A?"21+":537.13>A?23:537.18>A?24:537.24>A?25:537.36>A?26:"Blink"!=$?"27":"28")):($&&($[1]="like Safari"),A=A[0],A=400>A?1:500>A?2:526>A?3:533>A?4:534>A?"4+":535>A?5:537>A?6:538>A?7:601>A?8:"8"),$&&($[1]+=" "+(A+="number"==typeof A?".x":/[.+]/.test(A)?"":"+")),"Safari"==U&&(!V||parseInt(V)>45)&&(V=A)),"Opera"==U&&(A=/\bzbov|zvav$/.exec(J))?(U+=" ",K.unshift("desktop mode"),"zvav"==A?(U+="Mini",V=null):U+="Mobile",J=J.replace(RegExp(" *"+A+"$"),"")):"Safari"==U&&/\bChrome\b/.exec($&&$[1])&&(K.unshift("desktop mode"),U="Chrome Mobile",V=null,/\bOS X\b/.test(J)?(z="Apple",J="iOS 4.3+"):J=null),V&&0==V.indexOf(A=/[\d.]+$/.exec(J))&&e.indexOf("/"+A+"-")>-1&&(J=m(J.replace(A,""))),$&&!/\b(?:Avant|Nook)\b/.test(U)&&(/Browser|Lunascape|Maxthon/.test(U)||/^(?:Adobe|Arora|Breach|Midori|Opera|Phantom|Rekonq|Rock|Sleipnir|Web)/.test(U)&&$[1])&&(A=$[$.length-1])&&K.push(A),K.length&&(K=["("+K.join("; ")+")"]),z&&X&&X.indexOf(z)<0&&K.push("on "+z),X&&K.push((/^on /.test(K[K.length-1])?"":"on ")+X),J&&(A=/ ([\d.+]+)$/.exec(J),I=A&&"/"==J.charAt(J.length-A[0].length-1),J={architecture:32,family:A&&!I?J.replace(A[0],""):J,version:A?A[1]:null,toString:function(){var e=this.version;return this.family+(e&&!I?" "+e:"")+(64==this.architecture?" 64-bit":"")}}),(A=/\b(?:AMD|IA|Win|WOW|x86_|x)64\b/i.exec(q))&&!/\bi686\b/i.test(q)&&(J&&(J.architecture=64,J.family=J.family.replace(RegExp(" *"+A),"")),U&&(/\bWOW64\b/i.test(e)||Z&&/\w(?:86|32)$/.test(w.cpuClass||w.platform)&&!/\bWin64; x64\b/i.test(e))&&K.unshift("32-bit")),e||(e=null);var Y={};return Y.description=e,Y.layout=$&&$[0],Y.manufacturer=z,Y.name=U,Y.prerelease=G,Y.product=X,Y.ua=e,Y.version=U&&V,Y.os=J||{architecture:null,family:null,version:null,toString:function(){return"null"}},Y.parse=b,Y.toString=p,Y.version&&K.unshift(V),Y.name&&K.unshift(U),J&&U&&(J!=String(J).split(" ")[0]||J!=U.split(" ")[0]&&!X)&&K.push(X?"("+J+")":"on "+J),K.length&&(Y.description=K.join(" ")),Y}var v={"function":!0,object:!0},h=v[typeof window]&&window||this,g=h,p=v[typeof r]&&r,x=v[typeof n]&&n&&!n.nodeType&&n,y=p&&x&&"object"==typeof t&&t;!y||y.global!==y&&y.window!==y&&y.self!==y||(h=y);var w=Math.pow(2,53)-1,E=/\bOpera/,S=this,T=Object.prototype,A=T.hasOwnProperty,O=T.toString;"function"==typeof e&&"object"==typeof e.amd&&e.amd?e(function(){return b()}):p&&x?s(b(),function(e,t){p[t]=e}):h.platform=b()}).call(this)}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}]},{},[1])(1)});

};
//# sourceMappingURL=ally.min.js.map

/**
 * @name SIA
 * @description Define global autoCompleteLanguage functions
 * @version 1.0
 */

SIA.autoCompleteLanguage = function(){
	var global = SIA.global;
	var doc = global.vars.doc;
	var win = global.vars.win;
	var	config = global.config;
	var body = global.vars.body;
	var languageJSON = global.vars.languageJSON;
	

	var autoCompleteCustom = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.airportData = that.options.airportData;
		that.timer = null;
		that.timerResize = null;

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('div.custom-select');
			var bookingAutoComplete = field.autocomplete({
					minLength : 0,
					open: that.options.open,
					change: that.options.change,
					select: that.options.select,
					close: that.options.close,
					response: that.options.response,
					search: that.options.search,
					// source: that.airportData,
					source: function(request, response) {
						// create a regex from ui.autocomplete for 'safe returns'
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
						// match the user's request against each destination's keys,

						var match = $.grep(that.airportData, function(airport) {
							var flag = airport.flag;
							var value = airport.value;

							return (matcher.test(value) || matcher.test(flag)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
						});

						// ... return if ANY of the keys are matched
						response(match);
					},
					appendTo : that.options.autoCompleteAppendTo
				}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function (ul, item) {
				// customising our suggestion dropdowns here
				return $('<li class="autocomplete-item">')
				/*.attr('data-value', item.order)
				.attr('data-language', item.language)
				.attr('data-flag', item.flag)*/
				.attr({'data-value': item.order, 'data-language': item.language, 'data-flag': item.flag})
				.append('<a class="autocomplete-link" href="javascript:void(0);"><img src="images/transparent.png" alt="" class="flags '+ item.flag +'">'+ item.value + '</a>')
				.appendTo(ul);
			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			bookingAutoComplete._move = function( direction ) {
				var item, previousItem,
				last = false,
				api = this.menu.element.data('jsp'),
				li = $(),
				minus = null;
				if (!api) {
					if (!this.menu.element.is(':visible')) {
						this.search(null, event);
						return;
					}
					if (this.menu.isFirstItem() && /^previous/.test(direction) ||
							this.menu.isLastItem() && /^next/.test(direction) ) {
						this._value( this.term );
						this.menu.blur();
						return;
					}
					this.menu[direction](event);
				}
				else {
					var currentPosition = api.getContentPositionY();
					switch(direction){
						case 'next':
							if(this.element.val() === ''){
								api.scrollToY(0);
								li = this.menu.element.find('li:first');
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.next();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
								// console.log(currentPosition, previousItem.position().top);
							}
							if(!item){
								last = true;
								li = this.menu.element.find('li').removeClass('active').first();
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(0);
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								minus = li.position().top + li.innerHeight();
								if(minus - this.menu.element.height() > currentPosition){
									api.scrollToY(Math.max(0, minus - this.menu.element.height()));
								}
							}
							$('#wcag-custom-select').html(item.value);
							break;
						case 'previous':
							if(this.element.val() === ''){
								last = true;
								item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.prev();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
							}
							if(!item){
								last = true;
								item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(this.menu.element.find('.jspPane').height());
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								if(li.position().top <= currentPosition){
									api.scrollToY(li.position().top);
								}
							}
							break;
					}
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');
			// if(window.Modernizr.touch || window.navigator.msPointerEnabled){
			// 	win.off('resize.blur'+index).on('resize.blur'+index, function(){
			// 		clearTimeout(that.timerResize);
			// 		// that.timerResize = setTimeout(function(){
			// 		// 	field.blur();
			// 		// }, 200);
			// 	});
			// }
			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				var self = $(this);
				self.closest('.custom-select').addClass('focus');
				// return false;
				if(global.vars.isIE()){
					doc.off('mousedown.hideAutocompleteLanguage').on('mousedown.hideAutocompleteLanguage', function(e){
						if(!$(e.target).closest('.ui-autocomplete').length){
							field.closest('.custom-select').removeClass('focus');
							field.autocomplete('close');
						}
					});
				}

				SIA.WcagGlobal.customSelectAriaLive(field.closest('.custom-select .select__text'));

				// var dataAutocomplete = self.data('uiAutocomplete');
				// if(dataAutocomplete && self.val()) {
				// 	if(!dataAutocomplete.selectedItem) {
				// 		clearTimeout(timerTriggerSearch);
				// 		timerTriggerSearch = setTimeout(function() {
				// 			dataAutocomplete.search();
				// 		}, 100);
				// 	}
				// }
			});
			if(!global.vars.isIE()){
				field.off('blur.highlight').on('blur.highlight', function(){
					that.timer = setTimeout(function(){
						field.closest('.custom-select').removeClass('focus');
						field.autocomplete('close');
					}, 200);

					// if(window.Modernizr.touch || window.navigator.msMaxTouchPoints){
					// 	win.off('resize.reposition');
					// }

				});
				field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
					clearTimeout(that.timer);
				});
			}

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					e.preventDefault();
					if(field.autocomplete('widget').find('li').length === 1){
						field.autocomplete('widget').find('li').trigger('click');
						return;
					}
					field.autocomplete('widget').find('li.active').trigger('click');
				}
			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
			// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
	};

	var autoCompleteLanguage = function(){
		var ppLanguage = global.vars.ppLanguage;
		if (!ppLanguage.length) {
			return;
		}
		var optionLanguage = ppLanguage.find('.custom-radio');
		var parentOptionLanguage = optionLanguage.parent();
		var defaultFlag = ppLanguage.find('.custom-select img').attr('class').split(' ')[1];
		parentOptionLanguage.children().not(':eq(0)').remove();
		// optionLanguage.not(':eq(0)').hide();
		var dataLanguage = ['en_UK', 'zh_CN', 'fr_FR', 'pt_BR', 'de_DE', 'zh_TW', 'ja_JP', 'ko_KR', 'ru_RU', 'es_ES'];
		var detectLanguge = function(lng){
			var lngs = lng.split(',');
			var isLng = [];

			for(var i = 0; i < lngs.length; i ++){
				for(var ii = 0; ii < dataLanguage.length; ii ++){
					if($.trim(lngs[i]) === $.trim(dataLanguage[ii])){
						isLng.push(ii);
					}
				}
			}

			return isLng;
		};

		var _selectLanguage = function(arr){
			parentOptionLanguage.empty();
			optionLanguage.find(':radio').removeAttr('checked');
			for(var i = 0; i< arr.length; i ++){
				var t = optionLanguage.eq(arr[i]).clone().appendTo(parentOptionLanguage);
				if(i === 0){
					t.find(':radio').prop('checked', true);
				}
				else{
					t.find(':radio').prop('checked', false);
				}
			}
		};
		var timerAutocompleteLang = null;
		var timerAutocompleteLangOpen = null;

		var getFlags = function(value){
			var a = {
				idx : 0,
				flag : false
			};
			for(var i = 0; i < languageJSON.data.length; i ++){
				if(languageJSON.data[i].value === value){
					a = {
						idx : i,
						flag : true
					};
				}
			}
			return a;
		};

		autoCompleteCustom({
			containerAutocomplete : ppLanguage,
			autocompleteFields : 'input#text-country',
			autoCompleteAppendTo: body,
			airportData : languageJSON.data,
			open: function(){
				var self = $(this);
				self.autocomplete('widget').hide();
				clearTimeout(timerAutocompleteLangOpen);
				timerAutocompleteLangOpen = setTimeout(function(){
					self.autocomplete('widget').show().css({
						'left': self.closest('.custom-select--2').offset().left,
						'top': self.closest('.custom-select--2').offset().top + self.closest('.custom-select--2').outerHeight(true)
					});
					self.autocomplete('widget')
					.jScrollPane({
						scrollPagePercent				: 10
					}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
				}, 100);
			},
			select: function(event, ui){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				flag.removeClass().addClass('flags ' + ui.item.flag);
				_selectLanguage(detectLanguge(ui.item.language));
				if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){
					win.trigger('resize.popupLanguage');
				}
				setTimeout(function(){
					self.blur();
				}, 100);
			},
			response: function(event, ui){
				if(ui.content.length ===1){
					// $(this).val(ui.content[0].value);
					// $(this).select();
				}
			},
			search: function(){
				var self = $(this);
				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				clearTimeout(timerAutocompleteLang);
				timerAutocompleteLang = setTimeout(function(){
					// if(self.autocomplete('widget').find('li').length === 1){
					// 	self.autocomplete('widget').find('li').addClass('active');
					// }
				}, 100);
			},
			close: function(){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				var item = getFlags(self.val());
				if(item.flag){
					flag.removeClass().addClass('flags ' + languageJSON.data[item.idx].flag);
					_selectLanguage(detectLanguge(languageJSON.data[item.idx].language));
				}
				$('#wcag-custom-select').html(self.val());
				self.prev().focus();
				if(!$.trim(self.val())){
					flag.removeClass().addClass('flags ' + defaultFlag);
					var defaultLanguage = 'en_UK, zh_CN';
					_selectLanguage(detectLanguge($.trim(defaultLanguage)));
				}
				$(this).autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				doc.off('mousedown.hideAutocompleteLanguage');
			},
			setWidth: 0
		});
	};
	// init
	autoCompleteLanguage();
};

/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'checkboxAll';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var that = this;
			var chbMaster = that.element.find('[data-checkbox-master]').first();
			var chbItems = that.element.find('[data-checkbox-item]');


			chbMaster.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checked = $(this).is(':checked');
				chbItems.filter(':visible').prop('checked', checked);
			});

			chbItems.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checkedAll = (chbItems.filter(':visible').length === chbItems.filter(':checked').length);
				chbMaster.prop('checked', checkedAll);
			});

		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
	};

	// $(function() {
	// 	$('[data-checkbox-all]')[pluginName]();
	// });

}(jQuery, window));

/**
 * @name SIA
 * @description Define global cookiesUse functions
 * @version 1.0
 */
SIA.cookiesUse = function(){
	var popupCookies = $('.popup--cookie').appendTo(SIA.global.vars.container);
	// cookies

	var setCookie = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+d.toGMTString();
		document.cookie = cname + '=' + cvalue + ';' + expires;
	};

	var getCookie = function(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i].trim();
			if (c.indexOf(name) === 0){
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	var checkCookie = function () {
		var user = getCookie('seen');
		
		if (user !== '') {
			popupCookies.addClass('hidden');
		} else {
			popupCookies.removeClass('hidden');
		}
	};
	// end cookies
	checkCookie();
	popupCookies.find('.popup__close').off('click.closeCookie').on('click.closeCookie', function(e){
		e.preventDefault();
		setCookie('seen', true, 7);
		popupCookies.addClass('hidden');
	});
};

(function($, window, undefined) {
	var pluginName = 'customSelect';
	var openedSelect = null;
	var getUID = (function(){
		var id = 0;
		return function(){
			return pluginName + '-' + id++;
		};
	})();
	var win = $(window);
	var doc = $(document);
	var body = $(document.body);
	var isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};
	var key = {
		DOWN: 40,
		UP: 38,
		ENTER: 13,
		ESC: 27,
		SPACE: 32,
		TAB: 9
	};


	var returnFalse = false;

	function scrollWithADistance(that, top) {
		that.scrollBarBtn.css({
			'top' : Math.min(top, that.scrollContainer.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : Math.max(-top * that.ratioOfWrapperToScroll + top * that.ratioOfContenToScroll, that.scrollContainer.height() - that.wrapperOption.height())
		});
	}

	function scrollWithY(that, Y) {
		var y = Y;
		that.scrollBarBtn.css({
			'top' : Math.min(y/(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll), that.scrollBarWrapper.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : -Math.min(that.itemsLength*that.options.heightItem - that.scrollContainer.height(), y)
		});
	}

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.idPlugin = getUID();
			that.options = plugin.options;
			that.curItem = $();
			// that.hasDefaultClass = that.hasClass('default');
			plugin.inPopup = that.closest('.popup');
			that.selectEl = that.find(plugin.options.selectEl);
			if(!that.selectEl.is('select')){
				return;
			}
			that.customText = that.find(plugin.options.customText);
			that.customIcon = that.find('.ico-dropdown');
			that.arrow = that.find(plugin.options.arrow);
			that.selectLabel = that.find('.select__label');

			that.customText.attr('aria-hidden', true);
			that.customIcon.attr('aria-hidden', true);

			that.simulatorListbox = $('<ul/>')
				.attr({
					'role': 'listbox',
					'id': that.idPlugin + '-listbox',
					'aria-multiselectable': false
				});

			that.scroll = $('<div/>')
				.addClass('custom-scroll custom-dropdown')
				.append( $('<div/>')
					.addClass('scroll-container')
					.append( that.simulatorListbox )
					.append( $('<div/>' )
						.addClass('scroll-bar')
						.attr('role', 'presentation')
						.html('<span>&nbsp;</span>')
					)
				);

			that.scrollContainer = that.scroll.find('.scroll-container');
			that.wrapperOption = that.scrollContainer.find('ul');
			that.scrollBarWrapper = that.scroll.find('.scroll-bar');
			that.scrollBarBtn = that.scrollBarWrapper.children();

			plugin._createTemplate();
			plugin.refresh();
			var closeSelect = function(){
				if(openedSelect){
					openedSelect.hide();
					// openedSelect = null;
				}
			};

			// if(that.data('filter')){
			// 	that.find('[data-trigger]').off('click.show').on('click.show', function(e){
			// 		if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
			// 		that.trigger('beforeSelect');
			// 		e.stopPropagation();
			// 		if(!that.hasClass('active')){
			// 			plugin.show();
			// 			closeSelect();
			// 			openedSelect = plugin;
			// 		}
			// 		else{
			// 			// openedSelect = null;
			// 			plugin.hide();
			// 		}
			// 	});
			// }else{
				that.off('click.show').on('click.show', function(e){
					clearTimeout(that.simulatorTimer);
					if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
					that.trigger('beforeSelect');
					e.stopPropagation();
					if(that.hasClass('active')){
						// openedSelect = null;
						plugin.hide();
					}
					else{
						$(document).trigger('click');
						plugin.show();
						closeSelect();
						openedSelect = plugin;
					}
				});
				that.find('.select__tips').on('click', function(e){
					e.stopPropagation();
				});
				// reroute the label from the hidden select to the visible combobox
				that.selectLabel.attr({
					'for': that.idPlugin + '-combobox'
				});

				that.simulatorCombobox = $('<input/>')
					.addClass('input-overlay')
					.attr({
						'id': that.idPlugin + '-combobox',
						'name': that.idPlugin + '-combobox',
						'aria-expanded': false,
						'aria-autocomplete': 'none',
						'aria-owns': that.idPlugin + '-listbox', // semantic sugar for NVDA, in combination with aria-selected
						'role': 'combobox',
						'readonly': 'readonly' // requires VO users to navigate with VO + arrow keys, and select with VO + space
					});

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {

					if ( SIA.WcagGlobal.isIE() ) {
						that.attr('role', 'document'); // ensures that combobox is announced and selection option and description are available
					}

				}

				that.simulatorCombobox
					.val( $('#' + that.idPlugin + '-listbox li.active').text() ) // this communicates the selected option to VO and JAWS
					.insertAfter( that.selectEl );

				that.append( $('<p></p>')
					.addClass('select__tips ui-helper-hidden-accessible')
					.attr({
						'id': that.idPlugin + '-customSelectUsage'
					})
				);
				if(typeof L10n !== 'undefined'){
					if(navigator.platform === 'MacIntel'){
						$('#' + that.idPlugin + '-customSelectUsage').text( L10n.tips.customSelectUsage.osx );
					}else{
						$('#' + that.idPlugin + '-customSelectUsage').text( L10n.tips.customSelectUsage.windows );
					}
				}

				if(typeof SIA.WcagGlobal !== 'undefined'){
					SIA.WcagGlobal.assignDescriptionAttributes({
						descriptionSelector:'#'+that.idPlugin+'-customSelectUsage',
						descriptionCategory:'customSelectUsage',
						uniqueHostSelector:'#'+that.idPlugin+'-combobox'
					});
				}

				that.simulatorCombobox
					.off('click.preventDefault').on('click.preventDefault', function(e){
						e.preventDefault();
					})
					.off('focusin.focusHighlighting').on('focusin.focusHighlighting', function(e) {
						$(that).addClass('focus'); // .focus.active breaks SPACE listener
						e.preventDefault();
					})
					.off('focusout.focusHighlighting').on('focusout.focusHighlighting', function(e) {
						$(that).removeClass('focus'); // .focus.active breaks SPACE listener
						e.preventDefault();
					})
					.off('click.show').on('click.show', function() {
						if(that.selectEl.prop('disabled') || that.hasClass('disabled')){
							return;
						}
						that.simulatorTimer = setTimeout(function(){
							if(!that.hasClass('active')){
								plugin.show();
								closeSelect();
								openedSelect = plugin;
							}
						}, 200);
					})
					.off('keypress.show').on('keypress.show', function(event) {
						if(that.selectEl.prop('disabled') || that.hasClass('disabled')){
							return;
						}

						// allow the spacebar to open the menu
						var code = event.keycode || event.which;
						switch(code) {
							case key.SPACE:
								event.preventDefault();
								that.simulatorTimer = setTimeout(function(){
									if(!that.hasClass('active')){
										plugin.show();
										closeSelect();
										openedSelect = plugin;
									}
								}, 200);
								break;
						}
					})
					.off('keydown.preventSubmit').on('keydown.preventSubmit', function(event) {

						// prevent form submit when the custom select is focussed but closed
						var code = event.keycode || event.which;
						switch(code) {
							case key.ENTER:
								event.preventDefault();
								break;
						}
					});
					/*
					.off('click.show').on('click.show', function() {
						// Removed in WCAG update as this opens then closes the menu on click, presumably because the focus moves directly off the combobox
						that.focusoutTimer = setTimeout(function(){
							plugin.hide();
						}, 200);
					});
					*/
			// }

			that.selectEl.css('visibility', 'hidden');
			//that.selectEl.attr('aria-hidden', true); // test for JAWS, did't fix anything

			that.arrow.off('click.show').on('click.show', function(){
				that.trigger('focusin.show');
				return false;
			});

			// the original select, which is replaced by the custom select
			that.selectEl.off('keypress.select').on('keypress.select', function(event) {
				var code = event.keycode || event.which;
				switch(code) {
					case key.ENTER:
						event.preventDefault();
						plugin.show();
						returnFalse = true;
						break;
				}
			});

			if(plugin.inPopup.length) {
				plugin.inPopup.off('scroll.inPopup' + that.idPlugin).on('scroll.inPopup' + that.idPlugin, function() {
					plugin.hide();
				});
			}
		},
		_createTemplate: function() {
			var plugin = this,
				that = plugin.element;
			that.items = that.selectEl.children();
			that.itemsLength = that.items.length;
			that.wrapperOption.empty().css('margin-top', '');
			that.scrollContainer.css('height', 0);
			that.scrollBarWrapper.css('height', 0);
			that.scrollBarBtn.css('top', '');
			var items  = [];
			if(!that.items.length){
				that.customText.text('');
			}
			that.items.each(function(i) {
				var self = $(this),
						optionClass = self.attr('class') || '';
				var item = '<li role="option" data-value="' + self.val() + '" id="' + that.idPlugin + '-option-' + i + '" class="' + (self.is(':selected') ? 'active' + optionClass : optionClass) + '">' + self.text() + '</li>';
				items.push(item);
				if(self.is(':selected')){
					that.customText.text(self.text());
				}
				// item.outerHeight(plugin.options.heightItem);
				// item.off('click.select').on('click.select', function(){
				// 	that.curItem = item;
				// 	that.customText.text(self.text());
				// 	that.selectEl.prop('selectedIndex', idx);
				// 	item.siblings('.active').removeClass('active');
				// 	item.addClass('active');
				// 	if(plugin.options.afterSelect){
				// 		plugin.options.afterSelect.call(that, that.selectEl, self, idx);
				// 	}
				// 	that.trigger('afterSelect');
				// });
			});
			if(that.items.length <= plugin.options.itemsShow){
				that.scrollContainer.height(that.items.length*plugin.options.heightItem);
				that.scrollBarWrapper.height(that.items.length*plugin.options.heightItem);
			}
			else{
				that.scrollContainer.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
				that.scrollBarWrapper.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
			}
			// when a mouse user clicks a menu item
			that.wrapperOption.html(items.join(''));
			that.wrapperOption.undelegate('.select')
				.delegate('[data-value]','click.select', function(){
					// e.stopPropagation();
					that.curItem = $(this);
					if (!that.curItem.closest('[data-keep-limit]')) {
						if(that.curIndex === that.curItem.index()){
							return;
						}
					}
					that.curIndex = that.curItem.index();
					that.customText.text(that.items.eq(that.curIndex).text());
					that.selectEl.prop('selectedIndex', that.curIndex);

					that.curItem.siblings('.active').removeClass('active').attr('aria-selected', false);
					that.curItem.addClass('active');
					plugin.setAriaSelected( that.curItem );

					if(plugin.options.afterSelect){
						plugin.options.afterSelect.call(that, that.selectEl, that.items.eq(that.curIndex), that.curIndex);
						if(void 0 !== that.selectEl.closest('form').data('validator')){
							that.selectEl.valid();
						}
					}

					that.trigger('afterSelect', that.curItem.data('value'));
					that.selectEl.trigger('change');
					that.selectEl.trigger('blur');
					// plugin.hide();
				})
				.delegate('[data-value]', 'mousedown.select', function() {
					setTimeout(function() {
						clearTimeout(that.focusoutTimer);
					}, 200);
				});
			that.scroll = that.scroll.appendTo(body);
			if(that.selectEl.closest('.popup').length){
				that.scroll.css({
					'z-index': '1004'
				});
			}
		},
		refresh: function () {
			var plugin = this,
				that = plugin.element;
			that.customText.text(that.selectEl.find(':selected').text());
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(that.selectEl.prop('selectedIndex')).addClass('active').attr('aria-selected', true);
			that.curIndex = that.selectEl.prop('selectedIndex');
			if($.trim(that.selectEl.find(':selected').val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		update: function () {
			var plugin = this,
				that = plugin.element;
			var text = '';
			var sel = that.selectEl.find(':selected');
			text = sel.text();
			that.customText.text(text);
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(sel.index()).addClass('active').attr('aria-selected', true);
			that.find('input').val(sel.text());
			that.curIndex = sel.index();
			if($.trim(sel.val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		updateSync: function () {
			var plugin = this,
				that = plugin.element;
			var text = '';
			var sel = that.selectEl.find('[selected="selected"]');
			text = sel.text();
			that.customText.text(text);
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(sel.index()).addClass('active').attr('aria-selected', true);
			that.find('input').val(sel.text());
			that.curIndex = sel.index();
			if($.trim(sel.val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		setFocus: function($element) {
			var plugin = this,
				that = plugin.element;

			// save the scroll position
			var y = $(window).scrollTop();

			if ( ! $element.is(':focusable') ) {
				$element.attr('tabindex', -1);
			}

			$element.addClass('keyboardfocus').focus();

			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				if ( $element.is( that.simulatorCombobox ) ) {
					$(that).addClass('focus');
				}
			}

			// revert the scroll position
			window.scrollTo( 0, y );
		},
		setAriaSelected: function($selected) {
			var plugin = this,
					that = plugin.element;

			// prevent errors when parts of this compound widget are used in other compound widgets
			if(typeof that.simulatorCombobox !== 'undefined'){
				that.simulatorCombobox.val($selected.text());
				$selected.attr('aria-selected', true);
			}
		},
		show: function(){
			var plugin = this,
					that = plugin.element;
			if(that.is(':hidden')){
				return;
			}
			that.setDimension = function(){
				var top,
						maxW = 0;

				that.scroll.show();

				// prevent errors when parts of this compound widget are used in other compound widgets
				if ( typeof that.simulatorCombobox !== 'undefined' ) {
					that.simulatorCombobox.attr('aria-expanded', true);
				}

				maxW = that.outerWidth();

				var $option = that.scrollContainer.find('[role="option"]');
				var $optionSelected = $option.filter('.active');
				// set keyboardfocus to the .active option, or failing that, the first option
				if ( $optionSelected.length ) {
					plugin.setFocus( $optionSelected );
				}
				else {
					if (!that.parent().is('[data-keep-limit]')) {
							plugin.setFocus( $option.eq(0) );
					}
				}

				// fix issue page mb-select-meals: dropdown width > select width
				// that.scroll.find('li').each(function() {
				// 	maxW = Math.max(maxW, this.offsetWidth);
				// });

				that.scroll.width(maxW - plugin.options.padding);
				if(that.items.length <= plugin.options.itemsShow){
					that.scrollContainer.width(maxW);
				}
				else{
					that.scrollContainer.width(maxW - plugin.options.padding - that.scrollBarWrapper.outerWidth(true));
				}

				if(!isIE() && plugin.inPopup.length && plugin.inPopup.css('position') === 'fixed'){
					top = that.offset().top - win.scrollTop() + that.outerHeight();
					if(that.offset().top + that.scroll.outerHeight(true) + that.outerHeight() > win.scrollTop() + win.height()){
						top = that.offset().top - that.scroll.outerHeight(true) - win.scrollTop();
					}
					that.scroll.css('position', 'fixed');
				}
				else{
					top = that.offset().top + that.outerHeight();
					if(top + that.scroll.outerHeight(true) > win.scrollTop() + win.height()) {
						top = that.offset().top - that.scroll.outerHeight(true);
					}
					that.scroll.css('position', 'absolute');
				}

				that.scroll.css({
					'z-index': '1000',
					top: top,
					left: that.offset().left
				});
				if(that.selectEl.closest('.popup').length){
					that.scroll.css({
						'z-index': '1004'
					});
				}

				if(plugin.options.onOpen){
					plugin.options.onOpen(that.selectEl);
				}
			};
			var clearResize = null;
			that.setDimension();

			that.addClass('active focus');
			win.off('resize.kCustomSelect').on('resize.kCustomSelect', function(){
				clearTimeout(clearResize);
				clearResize = setTimeout(function(){
					that.setDimension();
				}, 300);
			});
			plugin._initCustomScroll();
			that.wrapperOptionHeight = that.wrapperOption.outerHeight(true);
			$(document).off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
				if (!plugin.options.preventClose) {
					plugin.hide();
				}
			});

			if(that.closest('.popup__inner').length) {
				that.closest('.popup__inner').off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
					plugin.hide();
				});
			}
		},
		hide: function(){
			var plugin = this,
				that = plugin.element;
			if(plugin.options.onClose){
				plugin.options.onClose(that.selectEl);
			}
			that.scroll.hide();

			var $option = that.scrollContainer.find('[role="option"]');
			if ( $option.length ) {
				$option.removeClass('keyboardfocus');
			}
			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.attr('aria-expanded', false);

				if ( that.hasClass('focus') ) {
					plugin.setFocus( that.simulatorCombobox );
				}
			}

			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.removeClass('active');
			}
			else {
				that.removeClass('active focus');
			}

			if(that.curItem.length){
				if(that.items.eq(that.curItem.index()).val()){
					that.removeClass('default');
				}
				else{
					that.addClass('default');
				}
			}

			win.off('resize.kCustomSelect');
			openedSelect = null;
			doc.off('mousemove.scrollBar'+ that.idPlugin);
			doc.off('mouseup.scrollBar'+ that.idPlugin);
			doc.off('keydown.scrollBar'+ that.idPlugin);
			doc.off('click.hide' + that.idPlugin);
			that.off('mousewheel.scrollBar');
			that.scroll.off('mousewheel.scrollBar');
		},
		_initCustomScroll: function(){
			var plugin = this,
				that = plugin.element;
			that.draggable = false;
			that.enableScrollBar = true;
			that.ratioOfContenToWrapper = that.scrollContainer.height()/that.wrapperOption.height();
			that.scrollBarBtn.height(that.scrollBarWrapper.height()*that.ratioOfContenToWrapper);
			that.ratioOfContenToScroll = that.scrollContainer.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.ratioOfWrapperToScroll = that.wrapperOption.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.firstY = 0;
			that.scrollBarBtnPos = 0;
			that.hiddenScroll = false;
			// that.scrollBarBtn.css('top', '');
			if (that.scrollContainer.height() >= that.wrapperOption.height() - 3) {
				that.scrollBarWrapper.hide();
				that.hiddenScroll = true;
			}
			else{
				if(that.scrollBarWrapper.is(':hidden')){
					that.scrollBarWrapper.show();
					that.hiddenScroll = false;
				}
			}

			var scroll = function (e, a) {
				//if(!that.enableScrollBar) return;
				var top = that.scrollBarBtn.position().top,
				preventDefault = true;
				if(e.keyCode === 40 || a < 0) {
					top += that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else if (e.keyCode === 38 || a > 0) {
					top -= that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else {
					preventDefault = true;
				}
				if (top <= 0){
					top = 0;
				}
				if (top + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.height()){
					top = that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, top);
				return preventDefault;
			};

			that.scrollBarBtn.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {
				that.draggable = true;
				that.firstY = e.pageY;
				that.scrollBarBtnPos = that.scrollBarBtn.position().top;
				return false;
			});

			that.scrollBarBtn.off('click.scrollBar').on('click.scrollBar', function (e) {
				e.stopPropagation();
			});

			that.scrollBarWrapper.off('click.scrollBar').on('click.scrollBar', function (e) {
				e.stopPropagation();
				clearTimeout(that.focusoutTimer);
				var a = (e.pageY - that.scrollBarWrapper.offset().top) - that.scrollBarBtn.outerHeight()/2;
				if (a <= 0){
					a = 0;
				}
				if (a + that.scrollBarBtn.outerHeight()/2 >= that.scrollBarWrapper.outerHeight()){
					a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, a);
			});

			// Fix bugs closing the custom select when holded mouse on the scrollbar.
			that.scrollBarWrapper.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {
				e.preventDefault();
			});

			that.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});

			that.scroll.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});

			doc.off('mousemove.scrollBar'+ that.idPlugin).on('mousemove.scrollBar'+ that.idPlugin, function(e){
				if (that.draggable) {
					var a = that.scrollBarBtnPos + (e.pageY - that.firstY);
					if (a <= 0){
						a = 0;
					}
					if (a + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.outerHeight()){
						a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
					}
					scrollWithADistance(that, a);
				}
				return false;
			});

			doc.off('mouseup.scrollBar'+ that.idPlugin).on('mouseup.scrollBar'+ that.idPlugin, function(){
				that.draggable = false;
				return false;
			});

			// this seems to be used when entering digts
			doc.off('keydown.scrollBar'+ that.idPlugin).on('keydown.scrollBar'+ that.idPlugin, function(e){
				var cur, next, prev, curPos, minus, character, isStartedWithSelectedCharacter, collection;

				// when the menu is open and the custom scrollbar is required, the scrollbar and list are navigable with the arrow keys
				if(!that.hiddenScroll){
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.keyboardfocus');
							next = cur.removeClass('keyboardfocus').removeAttr('tabindex').next();

							if(next.length){
								curPos = that.scrollBarBtn.position().top*(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll);
								minus = next.index()*next.outerHeight(true) + next.outerHeight(true) - that.scrollContainer.height() ;
								if(minus  > curPos){
									scrollWithY(that, Math.max(0, minus));
								}
							}
							else{
								next = that.scrollContainer.find('li:first');
								scrollWithY(that, next.index()*next.outerHeight(true), plugin.options.itemsShow);
							}

							plugin.setFocus( next );
							returnFalse = true;

							break;
						case key.UP:
							cur = that.scrollContainer.find('li.keyboardfocus');
							prev = cur.removeClass('keyboardfocus').removeAttr('tabindex').prev();

							if(prev.length){
								curPos = that.scrollBarBtn.position().top * (that.ratioOfWrapperToScroll - that.ratioOfContenToScroll) - that.scrollContainer.height();
								minus = prev.index()*prev.outerHeight(true) - that.scrollContainer.height();
								if(minus <= curPos){
									scrollWithY(that, prev.index()*prev.outerHeight(true));
								}
							}
							else{
								prev = that.scrollContainer.find('li:last');
								scrollWithY(that, prev.index()*prev.outerHeight(true), plugin.options.itemsShow);
							}

							plugin.setFocus( prev );
							returnFalse = true;

							break;
						case key.ENTER:
							e.preventDefault(); // don't submit the form

							cur = that.scrollContainer.find('li.keyboardfocus').addClass('active');

							plugin.setAriaSelected( cur );

							cur.trigger('click.select'); // save the .active value to the real select
							plugin.hide();

							returnFalse = true;

							break;
						case key.ESC:
						case key.TAB:
							// allow ESC to be used by screen reader to switch modes
							// allow TAB to move to next/previous focusable element
							plugin.hide();
							break;
						case key.SPACE:
							e.preventDefault(); // don't enter a space
							plugin.hide();
							break;
						default:
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.keyboardfocus');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}

								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return $(this).text()[0].toLowerCase() === character && $(this).data('value') !== '';
								});

								if(!collection.length) {
									break;
								}

								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('keyboardfocus');
									cur = collection.first();
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('keyboardfocus');
										cur = collection.eq(collection.index(cur) + 1);
									}
									else {
										cur.removeClass('keyboardfocus');
										cur = collection.first();
									}
								}

								scrollWithY(that, cur.index() * cur.outerHeight(true), plugin.options.itemsShow);

								plugin.setFocus( cur );
							}
							break;
					}
				}
				else{
					// the custom scrollbar is not required and therefore hidden
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.keyboardfocus');
							next = cur.removeClass('keyboardfocus').next();
							if(!next.length){
								next = that.scrollContainer.find('li:first');
							}

							plugin.setFocus( next );

							returnFalse = true;

							break;
						case key.UP:
							cur = that.scrollContainer.find('li.keyboardfocus'); // else we can only navigate one spot away
							prev = cur.removeClass('keyboardfocus').prev();

							if(!prev.length){
								prev = that.scrollContainer.find('li:last');
							}

							plugin.setFocus( prev );

							returnFalse = true;

							break;
						case key.ENTER:
							cur = that.scrollContainer.find('li.keyboardfocus').addClass('active');

							plugin.setAriaSelected( cur );

							cur.trigger('click.select');
							plugin.hide();
							returnFalse = true;

							break;
						case key.ESC:
						case key.TAB:
							// allow tab to have default action of moving to next/previous focusable element
							plugin.hide();

							break;
						case key.SPACE:
							e.preventDefault();

							if(!plugin.element[0].closest('[data-keep-limit]')) {
								plugin.hide();
							}

							break;
						default:
							// jump to list option by typing first letter
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.keyboardfocus');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}
								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return ($(this).text()[0].toLowerCase() === character && $(this).data('value') !== '');
								});
								if(!collection.length) {
									break;
								}
								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('keyboardfocus');
									cur = collection.first();
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('keyboardfocus');
										cur = collection.eq(collection.index(cur) + 1);
									}
									else {
										cur.removeClass('keyboardfocus');
										cur = collection.first();
									}
								}

								plugin.setFocus( cur );
							}
							break;
					}
				}

				if(returnFalse){
					return false;
				}
				//return scroll(e);
			});

			that.setDimension();

			var curActive = that.scrollContainer.find('li.active');

			scrollWithY(that, Math.max(0, curActive.index()*curActive.outerHeight(true)));
			// doc.off('mousemove.scrollBar'+ that.idPlugin +' mouseup.scrollBar'+ that.idPlugin +' keydown.scrollBar'+ that.idPlugin).on({
			// 	'mousemove.scrollBar' : function (e) {
			// 	},
			// 	'mouseup.scrollBar' : function () {
			// 		that.draggable = false;
			// 		return false;
			// 	},
			// 	'keydown.scrollBar' : function (e) {
			// 	}
			// });
		},
		_reInitCustomScroll: function(){
			var plugin = this,
				that = plugin.element;

			var curActive = that.scrollContainer.find('li.active');

			scrollWithY(that, Math.max(0, curActive.index()*curActive.outerHeight(true)));
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		},
		enable: function() {
			var that = this.element;
			that.removeClass('disabled');

			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.removeAttr('aria-disabled');

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {
					SIA.WcagGlobal.attachDescription('#' + that.idPlugin + '-customSelectUsage');
				}
			}
		},
		disable: function() {
			var that = this.element;
			that.addClass('disabled');

			// prevent errors when parts of this compound widget are used in other compound widgets
			if ( typeof that.simulatorCombobox !== 'undefined' ) {
				that.simulatorCombobox.attr('aria-disabled', true);

				if ( typeof SIA.WcagGlobal !== 'undefined' ) {
					SIA.WcagGlobal.detachDescription('#' + that.idPlugin + '-customSelectUsage');
				}
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				/* jshint -W030 */
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		selectEl: '> select',
		customText: '> span.select__text',
		arrow: '> a',
		itemsShow: 4,
		scrollWith : 2,
		heightItem: 43,
		padding: 3,
		preventClose: false,
		onOpen: function(){
			$('.custom-scroll:visible').attr({
				'open' : 'true' // proprietary
			});

			// legacy ARIA
			// $('.custom-scroll:visible').attr({
			// 	'role':'applicatiopn'
			// });
			// $('.custom-scroll:visible').find('ul').attr({
			// 	'role':'navigation',
			// 	'tabindex': '10'
			// });
			// $('.custom-scroll:visible').find('ul').children().attr({
			// 	'role':'button',
			// 	'tabindex': '-1'
			// });
			//$('.custom-scroll:visible').find('ul').children().eq(0).focus();

			var _scrollContainer = $('.custom-scroll:visible');

			if ( _scrollContainer.find('[role="option"].active').length ) {
				_scrollContainer.find('[role="option"].active').attr('tabindex', -1).focus();
			}

		},

		afterSelect: function(data){
			console.log(data);
		},
		onClose: function(){}
	};

}(jQuery, window));

/**
 * @name SIA
 * @description Define global highlightInput functions
 * @version 1.0
 */
SIA.highlightInput = function(){
	var doc = SIA.global.vars.doc;
	var el = null;

	// fix issue autocomplete when clear the text and focus other input, the autocomplete doesn't close
	doc.off('focus.highlightInput').on('focus.highlightInput', 'input, select', function(e) {
		if (el && el.length && el.closest('[data-autocomplete]').length && el.get(0) !== e.target) {
			setTimeout(function() {
				if(el.data('uiAutocomplete')){
					el.autocomplete('close');
					el = $(e.target);
				}
			}, 500);
		}
		else {
			el = $(e.target);
		}
	});

	var ip = $('.input-3,.input-1,.textarea-1,.textarea-2').not('.disabled').filter(function(idx, el){
		if(!$(el).closest('[data-return-flight]').length){
			return el;
		}
	});

	ip.off('focusin.highlightInput').on('focusin.highlightInput', function(){
		var self = $(this);
		if (!self.hasClass('disabled')) {
			self.addClass('focus');
			self.removeClass('default');
		}
	}).off('focusout.highlightInput').on('focusout.highlightInput', function(){
		var self = $(this);
		var child = self.children();
		self.removeClass('focus');
		// fix issue for alphanumeric if there is no rule-required
		if(child.data('rule-alphanumeric') && !child.data('rule-required')){
			self.closest('.grid-col').removeClass('error').find('.text-error').remove();
		}
	}).find('input').off('change.removeClassDefault').on('change.removeClassDefault', function(){
		var self = $(this);
		if(!self.val() || self.val() === self.attr('placeholder')){
			self.addClass('default');
		}
		else{
			self.removeClass('default');
		}
	}).trigger('change.removeClassDefault');
};

'use strict';
/**
 * @name SIA
 * @description Define global initCustomSelect functions
 * @version 1.0
 */
SIA.initCustomSelect = function(){
	if($('.passenger-details-page').length){
		return;
	}
	var win = SIA.global.vars.win;
	$('[data-customselect]').each(function(){
		var self = $(this);
		if(!self.data('customSelect')){
			self.customSelect({
				itemsShow: 5,
				heightItem: 43,
				scrollWith: 2,
				afterSelect: function(el, item, idx){
					if(el.is('#economy-5')){
						var outerWraper = el.closest('.form-group');
						var byRoute = outerWraper.siblings('.by-route');
						var byFlightNo = outerWraper.siblings('.by-flight-no');
						if(el.prop('selectedIndex')){
							byRoute.removeClass('hidden');
							byFlightNo.addClass('hidden');
						}
						else{
							byRoute.addClass('hidden');
							byFlightNo.removeClass('hidden');
						}
						win.trigger('resize.resetTabMenu');
					}
					if(el.closest('[data-fare-deal]').length){
						$('#fare-deals').children('.manualFareDeals').hide().eq(idx).show();
					}
				}
			});
		}
	});
};

/**
 * @name SIA
 * @description Define global initConfirmCheckbox functions
 * @version 1.0
 */
SIA.initConfirmCheckbox = function() {
	var wrapperFieldConfirm = $('[data-confirm-tc]');
	var btnSubmit = $('[data-button-submit]');

	if(wrapperFieldConfirm.length) {
		var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

		confirmCheckbox.off('change.confirm').on('change.confirm', function(e) {
			e.preventDefault();
			var btn = btnSubmit.length ? btnSubmit : confirmCheckbox.closest('.form-group-full').next().find(':submit');
			if(btn.length) {
				if(confirmCheckbox.is(':checked')) {
					btn.removeClass('disabled').prop('disabled', false);
				}
				else {
					btn.addClass('disabled').prop('disabled', true);
				}
			}
		}).trigger('change.confirm');
	}
};

/**
 * @name SIA
 * @description Define global initLangToolbar functions
 * @version 1.0
 */
SIA.initLangToolbar = function() {
	var body = SIA.global.vars.body;

	if(typeof(Storage) !== 'undefined' && !body.hasClass('popup-window-login-page') && !body.hasClass('popup-window-logout-page')) {
		// if(JSON.parse(localStorage.getItem('translateTo'))) {
		// condition alway to return true value
		if(localStorage.getItem('translateTo')) {
			return;
		}

		var global = SIA.global;
		var body = global.vars.body;
		// var container = $('#container');

		var hideTbar = function(tpl) {
			tpl.css('margin-top', -tpl.height());
			// container.css('padding-top', 0);
			body.removeClass('translateOn').off('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd').on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function() {
				body.removeClass('transition-all');
				tpl.remove();
			});
		};

		var showTBar = function(tpl) {
			var delayTime = 200;
			var tBarHeight;
			var skipToContent = $('.skip-to-content');
			//body.prepend(tpl);
			if(skipToContent.length>0){
				$(tpl).insertAfter('.skip-to-content');
			}else{
				body.prepend(tpl);
			}
			tBarHeight = tpl.height();
			tpl.css('margin-top', -tBarHeight);
			if($('.at-a-glance-page').length){
				delayTime += 800;
			}

			setTimeout(function() {
				// container.css('padding-top', tBarHeight);
				body.addClass('transition-all');
				tpl.removeAttr('style');
				body.addClass('translateOn');
			}, delayTime);
		};

		if(!body.is('.interstitial-page')) {
			$.get(global.config.url.langToolbarTemplate, function(templateStr) {
				var tpl = $(templateStr);
				tpl.find('.toolbar__close').off('click.closeToolbar').on('click.closeToolbar', function(e) {
					e.preventDefault();
					hideTbar(tpl);
				});
				tpl.find('[data-lang-toolbar]').off('click.translateLang').on('click.translateLang', function(e) {
					e.stopPropagation();
					hideTbar(tpl);
					localStorage.setItem('translateTo', $(this).data('langToolbar'));
				});
				// global.vars.win.off('resize.langToolbar').on('resize.langToolbar', function() {
				// 	if(tpl.length) {
				// 		container.css('padding-top', tpl.height());
				// 	}
				// });
				showTBar(tpl);
			}, 'html');
		}
	}
};

/**
 * @name SIA
 * @description Define global fixPlaceholder functions
 * @version 1.0
 */
SIA.fixPlaceholder = function(){
	// creating a fake placeholder for not supporting placeholder
	var placeholderInput = $('[placeholder]');
	if(!window.Modernizr.input.placeholder){
		placeholderInput.placeholder();
	}
};

/**
 * @name SIA
 * @description Define global initSubMenu functions
 * @version 1.0
 */
SIA.initSubMenu = function() {
	var subMenu = $('[data-device-submenu]');

	if(subMenu.length) {
		subMenu.find('option[selected]').prop('selected', true);
		subMenu.off('change.submenu').on('change.submenu', function() {
			var url = $(this).find('option:selected').data('url');
			if(url) {
				window.location.href = url;
			}
		});
	}
};

/**
 * @name SIA
 * @description Define global initToggleButtonValue functions
 * @version 1.0
 */
SIA.initToggleButtonValue = function() {
	var buttonToggle = $('[data-toggle-value]');
	buttonToggle.off('click.toggleLabel').on('click.toggleLabel',function() {
		var toggleValue = $(this).data('toggle-value');
		var currentValue = $(this).is('input') ? $(this).val() : $(this).html();
		var slideItem = $(this).closest('.slide-item');
		var index = slideItem.attr('index');
		$(this).data('toggle-value', currentValue);
		if($(this).is('input')) {
			$(this).val(toggleValue);
		}
		else {
			$(this).html(toggleValue);
		}
		if(slideItem.length){
			slideItem.siblings('[index="'+ index +'"]').find('[data-toggle-value]').html(toggleValue);
		}
	});
};

/**
 * @name SIA
 * @description Define global LoggedProfilePopup functions
 * @version 1.0
 */
SIA.LoggedProfilePopup = (function(){
	var global = SIA.global;
	var vars = global.vars;
	var config = global.config;
	var win = vars.win;
	var body = vars.body;
	var mainMenu = vars.mainMenu;
	var menuT = vars.menuT;
	var menuBar = vars.menuBar;
	var popupLoggedProfile = vars.popupLoggedProfile;
	var loggedProfileSubmenu = vars.loggedProfileSubmenu;

	popupLoggedProfile = loggedProfileSubmenu.clone().appendTo(document.body);
	loggedProfileSubmenu.removeClass().addClass('menu-sub hidden');
	popupLoggedProfile.removeClass('menu-sub');
	if(popupLoggedProfile.length){
		var triggerLoggedProfile = menuBar.find('ul a.status');
		var timerLoggedProfileResize = null;
		var realSub = loggedProfileSubmenu;
		var popupLoggedProfilePosition = function(){
			popupLoggedProfile.removeClass('hidden');
			var leftPopupLoggedProfile = triggerLoggedProfile.offset().left - popupLoggedProfile.outerWidth()/2 + triggerLoggedProfile.outerWidth()/2 - 30;
			if(leftPopupLoggedProfile + popupLoggedProfile.outerWidth() > win.width()){
				leftPopupLoggedProfile = leftPopupLoggedProfile - (leftPopupLoggedProfile + popupLoggedProfile.outerWidth() - win.width());
			}
			popupLoggedProfile.css({
				left: leftPopupLoggedProfile,
				top: triggerLoggedProfile.offset().top + triggerLoggedProfile.children().innerHeight() + (vars.isIE() ? 33 : 27)
			});
			popupLoggedProfile.find('.popup__arrow').css({
				left : Math.round(triggerLoggedProfile.offset().left - leftPopupLoggedProfile + 21)
			});
		};

		triggerLoggedProfile.off('click.showLoggedProfile').on('click.showLoggedProfile', function(e){
			e.preventDefault();
			e.stopPropagation();
			var winInnerW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			//hide all custom-select
			$('[data-customselect]').customSelect('hide');
			if(winInnerW >= config.tablet/* - config.width.docScroll*/){
				vars.detectHidePopup();
				vars.hidePopupSearch(true);
				var self = $(this);
				if(popupLoggedProfile.hasClass('hidden')){
					self.addClass('active');
					popupLoggedProfilePosition();
					if(window.Modernizr.cssanimations){
						popupLoggedProfile.addClass('animated fadeIn');
						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body).addClass('animated fadeInOverlay').show();
						popupLoggedProfile.triggerPopup = self;
					}
					else{
						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body);
						popupLoggedProfile.overlay.fadeIn(config.duration.overlay);
						popupLoggedProfile.triggerPopup = self;
						popupLoggedProfile.hide().fadeIn(config.duration.popupSearch);
					}
					win.off('resize.LoggedProfile').on('resize.LoggedProfile', function(){
						clearTimeout(timerLoggedProfileResize);
						timerLoggedProfileResize = setTimeout(function(){
							popupLoggedProfilePosition();
							if(win.width() < config.tablet - config.width.docScroll){
								self.removeClass('active');
								popupLoggedProfile.addClass('hidden');
								popupLoggedProfile.overlay.remove();
								win.off('resize.LoggedProfile');
							}
						}, 100);
					}).trigger('resize.LoggedProfile');
					popupLoggedProfile.overlay.off('click.closeLoggedProfile').on('click.closeLoggedProfile', function(e){
						e.preventDefault();
						e.stopPropagation();
						triggerLoggedProfile.trigger('click.showLoggedProfile');
					});
				}
				else{
					self.removeClass('active');
					if(window.Modernizr.cssanimations){
						popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
						popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
							popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
							popupLoggedProfile.overlay.remove();
							popupLoggedProfile.overlay = $();
							popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{
						popupLoggedProfile.fadeOut(config.duration.popupSearch, function(){
							popupLoggedProfile.addClass('hidden');
						});
						popupLoggedProfile.overlay.fadeOut(config.duration.overlay, function(){
							popupLoggedProfile.overlay.remove();
							popupLoggedProfile.overlay = $();
						});
					}
					win.off('resize.LoggedProfile');
				}
			}
			else {
				// Mobile
				var currentX; //use percent as unit
				var targetX;
				var spaceToMove;
				var interval;
				var animateInterval;
				var menuSub = menuT.find('.logged-in .menu-sub');
				var langToolbarEl = $('.toolbar--language'),
						langToolbarH = 0;
				if(langToolbarEl.length) {
					langToolbarH = langToolbarEl.height();
				}
				if(!menuSub.data('onceIncreaseTop')) {
					menuSub.data('onceIncreaseTop', true).css('top', -menuBar.find('.logged-in').offset().top);
				}
				menuT.find('.menu-inner').css('overflowY', 'hidden');
				if(realSub.hasClass('hidden')){
					if(window.Modernizr.cssanimations) {
						realSub.removeClass('hidden');
						mainMenu.addClass('active');
						mainMenu.parent().scrollTop(0);
						// realSub.css('top', - mainMenu.height());
						menuBar.addClass('active');
					}
					else {
						//No CSS3 animation
						currentX = 0; //use percent as unit
						targetX = -109;
						spaceToMove = 10;
						interval = 20;
						animateInterval = window.setInterval(function() {
							if(currentX <= targetX) {
								window.clearInterval(animateInterval);
								//
								mainMenu.children('ul').add(menuBar).css({
									'transform': '',
									'-moz-transform': '',
									'-webkite-transform': '',
									'-ms-transform': '',
									'-o-transform': ''
								});
								//menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
								realSub.removeClass('hidden');
								mainMenu.addClass('active');
								menuBar.addClass('active');
							}
							else {
								currentX -= spaceToMove;
								mainMenu.children('ul').add(menuBar).css({
									'transform': 'translateX(' + currentX + '%)',
									'-moz-transform': 'translateX(' + currentX + '%)',
									'-webkite-transform': 'translateX(' + currentX + '%)',
									'-ms-transform': 'translateX(' + currentX + '%)',
									'-o-transform': 'translateX(' + currentX + '%)'
								});
							}
						}, interval);
					}
					menuSub.css('top', 18 + langToolbarH - menuBar.find('.logged-in').offset().top);
				}
				else{
					realSub.addClass('hidden');
					mainMenu.removeClass('active').removeClass('hidden');
					menuBar.removeClass('active');
				}
			}
			SIA.WcagGlobal.assignAttributes();
		});

		realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
			e.preventDefault();
			menuT.find('.menu-inner').css('overflowY', 'auto');
			if (window.Modernizr.cssanimations) {
				setTimeout(function(){
					realSub.addClass('hidden');
					realSub.closest('.menu-item').removeClass('active');
				},500);
				mainMenu.removeClass('active');
				menuBar.removeClass('active');
			}
			else {
				var currentX = -109; //use percent as unit
				var targetX = 0;
				var spaceToMove = 10;
				var interval = 20;
				var animateInterval = window.setInterval(function() {
					if(currentX >= targetX) {
						window.clearInterval(animateInterval);
						//
						mainMenu.children('ul').add(menuBar).css({
							'transform': '',
							'-moz-transform': '',
							'-webkite-transform': '',
							'-ms-transform': '',
							'-o-transform': ''
						});
						//setTimeout(function(){
						realSub.addClass('hidden');
						realSub.closest('.menu-item').removeClass('active');
						//},500);
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
					}
					else {
						currentX += spaceToMove;
						mainMenu.children('ul').add(menuBar).css({
							'transform': 'translateX(' + currentX + '%)',
							'-moz-transform': 'translateX(' + currentX + '%)',
							'-webkite-transform': 'translateX(' + currentX + '%)',
							'-ms-transform': 'translateX(' + currentX + '%)',
							'-o-transform': 'translateX(' + currentX + '%)'
						});
					}
				}, interval);
			}
		});

		vars.popupLoggedProfile = popupLoggedProfile;
	}
})();

/**
 *  @name Popup
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'Popup';
	var win = $(window);
	var body = $(document.body);
	var prevFocusingItem;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	function onIpad() {
		// var userAgent = navigator.userAgent.toLowerCase();
		// var isIOS = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || ('ontouchstart' in window);
		var mobile = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
		if (!mobile) {
			return false;
		} else {
			return true;
		}
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
					that = plugin.element;
			that.options = plugin.options;
			that.modal = that.attr('data-modal-popup') ? $(that.attr('data-modal-popup')) : that;
			that.modal = that.modal.appendTo(body);
			that.detectIpad = onIpad();
			that.isShow = false;
			that.isAnimate = false;
			that.timer = null;
			that.freezeBody = false;
			that.heightHolderContainer = 0;
			that.isTablet = false;
			that.isMobile = false;
			that.isIpad = /iPad/i.test(window.navigator.userAgent);
			that.scrollTopHolder = 0;

			that.modal.addClass('hidden');

		},
		freeze: function(){
			var plugin = this,
					that = plugin.element,
					h = win.height() + that.scrollTopHolder,
					langToolbarEl = $('.toolbar--language');

			if(langToolbarEl.length) {
				var langH = langToolbarEl.height();
				h -= langH;
				if(win.scrollTop() > langH) {
					// langToolbarEl.addClass('hidden');
					body.removeClass('transition-all').removeClass('translateOn');
					// langToolbarEl.css('marginTop', -langH);
				}
			}
			// detect not remove style for container when exits popup
			if(!that.data('parentContainerStyle')) {
				$(plugin.options.container).css({
					// 'marginTop': - that.scrollTopHolder,
					'height': h,
					// 'overflow': 'hidden'
				});
				body.addClass('no-flow');
			}
		},
		showTablet: function() {
			var plugin = this,
					that = plugin.element;
			plugin.reposition();
			plugin.freeze();
			that.modal.removeClass('hidden').addClass('animated fadeIn').css('zIndex', (that.options.zIndex + 1));
			that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
					win.scrollTop(0);
				}
				if(that.options.afterShow){
					that.options.afterShow(that);
				}
				plugin.freeze();
				that.trigger('afterShow');
				that.isAnimate = false;
				that.isTablet = true;
				plugin.addResize();
				that.modal.removeClass('fadeIn');
				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		},
		reset: function(){
			var plugin = this,
				that = plugin.element;
			if(that.isTablet){
				that.modal.css('height', '');
				that.modal.find('.popup__content').removeClass('popup__scrolling').css('height', '');
				if (that.modal/*.find('.popup__content')*/.outerHeight(true) > win.height()) {
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
				}
				else{
					that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
				}
				if(that.isShow){
					plugin.freeze();
				}
			}
		},
		show: function() {
			var plugin = this,
					that = plugin.element;
			if(that.isShow){
				return;
			}
			that.trigger('beforeShow');
			if(that.options.beforeShow){
				that.options.beforeShow(that);
			}
			prevFocusingItem = document.activeElement;
			that.isShow = true;
			that.isAnimate = true;
			that.overlay = $(that.options.overlayBGTemplate).appendTo(body);
			that.scrollTopHolder = win.scrollTop();
			if(window.Modernizr.csstransitions){
				that.overlay.show().css({
					'zIndex': that.options.zIndex,
					opacity: 0.8
				}).addClass('animated fadeInOverlay');
				that.modal.removeClass('hidden');
				plugin.showTablet();
			}
			else{
				plugin.reposition();
				that.isTablet = true;
				plugin.freeze();
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').addClass('popup__scrolling').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
				}
				// if(window.isIE() && window.isIE() < 9){
				// 	that.overlay.css({
				// 		'zIndex': that.options.zIndex,
				// 		opacity: 0.5
				// 	}).show();
				// 	that.modal.addClass(that.options.modalShowClass).hide().show().css('zIndex', (that.options.zIndex + 1));
				// 	plugin.freeze();
				// 	if(that.options.afterShow){
				// 		that.options.afterShow(that);
				// 	}
				// 	that.trigger('afterShow');
				// 	that.isAnimate = false;
				// 	plugin.addResize();
				// }
				// else{
				// }
				that.overlay.css({
					'zIndex': that.options.zIndex
				}).fadeIn(plugin.options.duration);
				that.modal.removeClass('hidden').addClass(that.options.modalShowClass).fadeIn(plugin.options.duration, function(){
					plugin.freeze();
					// plugin.reposition();
					if(that.options.afterShow){
						that.options.afterShow(that);
					}
					that.trigger('afterShow');
					that.isAnimate = false;
					plugin.addResize();
				}).css('zIndex', (that.options.zIndex + 1));
			}

			that.modal.find(that.options.triggerCloseModal).on({
				'click.hideModal': function(e) {
					e.preventDefault();
					if(!that.isAnimate){
						plugin.hide();
					}
					if(that.find('[data-customselect]').length){
						$(document).trigger('click');
					}
				}
			});
			that.modal.off('click.doNothing').on('click.doNothing', function(){
				// fix for lumina 820 and 1020
			});
			if(that.options.closeViaOverlay){
				that.overlay.on({
					'click.hideModal': function(e) {
						e.preventDefault();
						if(!that.isAnimate){
							plugin.hide();
						}
					}
				});

				that.modal.on({'click.hideModal': function(e) {
						if(!that.isAnimate && !$(e.target).closest('.popup__inner').length){
							// e.preventDefault();
							plugin.hide();
						}
					}
				});
				// that.modal.find('.popup__inner').off('click.hideModal').on('click.hideModal', function(e) {
				// 	e.stopPropagation();
				// });
			}

			SIA.WcagGlobal.assignAttributes();
			SIA.WcagGlobal.ppMoveFocus();

			that.isShow = true;

		},
		addResize: function(){
			var plugin = this,
				that = plugin.element;
			// resize
			win.on({
				'resize.reposition': function() {
					if(that.timer){
						clearTimeout(that.timer);
					}
					that.timer = setTimeout(function(){
						plugin.reset();
						if(that.isShow){
							plugin.reposition();
						}
					}, that.options.duration + 100);
				},
				'keyup.escape': function(e) {
					if (e.which === 27) {
						plugin.hide();
						if(that.find('[data-customselect]').length){
							$(document).trigger('click');
						}
					}
				},
				'orientationchange.hide': function() {
					plugin.hide();
				}
			});
		},
		hide: function() {
			var plugin = this,
					that = plugin.element,
					langToolbarEl = $('.toolbar--language');

			if(that.overlay){
				that.isShow = false;
				that.trigger('beforeHide');
				that.modal.find(that.options.triggerCloseModal).off('click.hideModal');
				that.overlay.off('click.hideModal');
				if(langToolbarEl.length) {
					// langToolbarEl.removeClass('hidden');
					langToolbarEl.removeAttr('style');
					body.addClass('transition-all translateOn');
				}
				if(that.options.beforeHide){
					that.options.beforeHide(that);
				}
				if(window.Modernizr.csstransitions){
					that.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					that.modal.addClass('animated fadeOut');
					if(!that.data('parentContainerStyle')) {
						// $(plugin.options.container).removeAttr('style');
						$(plugin.options.container).css('height', '');
						body.removeClass('no-flow');
						// if(langToolbarEl.length) {
						// 	$(plugin.options.container).css('padding-top', langToolbarEl.height());
						// }
					}
					win.scrollTop(that.scrollTopHolder);
					win.off('resize.reposition keyup.escape');
					that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						that.modal.removeAttr('style').addClass('hidden').removeClass('animated fadeOut fadeIn slideInRight');
						that.scrollTopHolder = 0;
						that.overlay.off('click.hideModal').remove();
						that.overlay = null;

						that.isAnimate = false;
						that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
						if(that.options.afterHide){
							that.options.afterHide(that);
						}
						that.trigger('afterHide');
						that.modal.css('right', '');
						that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				}
				else{
					if(!that.data('parentContainerStyle')) {
						$(plugin.options.container).removeAttr('style');
						// if(langToolbarEl.length) {
						// 	$(plugin.options.container).css('padding-top', langToolbarEl.height());
						// }
					}
					win.off('resize.reposition keyup.escape');
					win.scrollTop(that.scrollTopHolder);
					that.scrollTopHolder = 0;
					// if(window.isIE() && window.isIE() < 9){
					// 	that.modal.removeClass(that.options.modalShowClass).hide();
					// 	that.modal.removeAttr('style').hide();
					// 	that.isAnimate = false;
					// 	that.modal.find('.popup__content').removeAttr('style');
					// 	if(that.options.afterHide){
					// 		that.options.afterHide(that);
					// 	}
					// 	that.trigger('afterHide');
					// 	that.overlay.hide().off('click.hideModal').remove();
					// }
					// else{
					// }
					that.modal.removeClass(that.options.modalShowClass).fadeOut(plugin.options.duration, function(){
						that.modal.removeAttr('style').addClass('hidden');
						that.isAnimate = false;
						that.modal.find('.popup__content').removeClass('popup__scrolling').removeAttr('style');
						if(that.options.afterHide){
							that.options.afterHide(that);
						}
						that.trigger('afterHide');
					});
					that.overlay.fadeOut(plugin.options.duration, function(){
						that.overlay.off('click.hideModal').remove();
						that.overlay = null;
					});
				}
			}
			prevFocusingItem.focus();
		},
		reposition: function() {
			var plugin = this,
					that = plugin.element,
					popupInner = that.modal.find('.popup__inner');
			that.modal.removeClass('hidden').css({
				position: (!that.detectIpad) ? ((win.height() > popupInner.height()) ? 'fixed' : 'fixed') : (win.width() < plugin.options.mobile) ? 'absolute' : 'fixed'
			});
			popupInner.css({
				top: win.height() > popupInner.height() ? Math.max(0, (win.height() - popupInner.height()) / 2) : 0,
				left: Math.max(0, (win.width() - that.modal.width()) / 2)
			});

			// that.modal.show().css({
			// 	// top: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0, (win.height() - that.modal.height()) / 2),
			// 	top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  0,
			// 	// top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  Math.max(0, (win.height() - that.modal.height()) / 2) + win.scrollTop(),
			// 	position : (!that.detectIpad) ? ((win.height() > that.modal.height()) ? 'fixed' : 'absolute') : 'absolute',
			// 	// left: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// 	left: Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// });
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		overlayBGTemplate: '<div class="bg-modal radial" id="overlay"></div>',
		modalShowClass: 'modal-show',
		triggerCloseModal: '.modal_close, a.btn-gray, [data-close]',
		duration: 500,
		container: '#container',
		mobile: 768,
		tablet: 988,
		zIndex: 20,
		closeViaOverlay: true,
		beforeShow: function() {},
		beforeHide: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global initSkipTabIndex functions
 * This function is created for accessibility
 * @version 1.0
 */

SIA.initSkipTabIndex = function(elm){
	if(elm){
		elm.find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
	}else{
		$('[data-skip-tabindex]').each(function(){
			$(this).find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
			$(this).removeAttr('data-skip-tabindex');
		});
	}
};

/**
 * @name SIA
 * @description Define social network functions
 * @version 1.0
 */
SIA.socialNetworkAction = function(){
	var global = SIA.global;
	var config = global.config;
	var social = $('[data-social]');
	var cocialSharing = function (element, link) {
		$(element).attr('href', link);
		$(element).attr('target', '_blank');
	};
	social.each(function(){
		var fb = $('[data-facebook]', this);
		var tw = $('[data-twitter]', this);
		var gplus = $('[data-gplus]', this);
		cocialSharing(fb, config.url.social.facebookSharing + window.location.href);
		cocialSharing(tw, config.url.social.twitterSharing + window.location.href);
		cocialSharing(gplus, config.url.social.gplusSharing + window.location.href);
	});
};

(function($, window, undefined) {
  var pluginName = 'stickyWidget',
      win = $(window),
      doc = $(document);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          el = that.element,
          elPos = el.offset().top,
          openStickyEl = el.find('[data-sticky-open]'),
          stickyBlock = el.find('.booking-widget-block'),
          closeStickyEl = el.find('.sticky__close');
      win.off('scroll.' + pluginName).on('scroll.' + pluginName,
        function(){
        if(win.scrollTop() >= elPos) {
          el.addClass('sticky');
          if(stickyBlock.is('.hidden')) {
            openStickyEl.removeClass('hidden');
          }
          el.find('input').blur();
          $('.ui-datepicker').hide();
        } else {
          openStickyEl.addClass('hidden');
          stickyBlock.addClass('hidden');
          closeStickyEl.css('display','none');
          el.removeClass('sticky');
        }
      })
      openStickyEl.off('click.openSticky').on('click.openSticky', function(){
          stickyBlock.slideDown('fast').removeClass('hidden');
          openStickyEl.addClass('hidden');
          closeStickyEl.css('display', 'block');
          stickyBlock.parents('[data-booking-widget]').focus();
      });
      closeStickyEl.off('click.closeSticky').on('click.closeSticky', function(){
          $.when(stickyBlock.slideUp('fast')).done(function(){
              openStickyEl.removeClass('hidden');
          });
          closeStickyEl.css('display', 'none');
      });
      closeStickyEl.off('keydown.closeSticky').on('keydown.closeSticky', function(e){
          e.preventDefault();
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 9 || keyCode === 13) {
          $(this).trigger('click.closeSticky');
        }
      });

      // doc.off('keydown.closeSticky').on('keydown.closeSticky', function(e){
      //     e.preventDefault();
      //   // var keyCode = e.keyCode || e.which || e.charCode;

      //   // if(keyCode === 9 || keyCode === 13) {
      //   //   $(this).trigger('click.closeSticky');
      //   // }
      //   console.log($(this));
      // });

      this.checkRadio();
    },

    checkRadio: function() {
      var that = this,
          el   = that.element,
          radioEl = el.find('input:radio'),
          target = el.find('[data-target]');
      radioEl.each(function(idx){
        var self = $(this);
        if(self.is(':checked')){
          if(idx){
            target.eq(1).removeClass('hidden');
            target.eq(0).addClass('hidden');
          }
          else{
            target.eq(0).removeClass('hidden');
            target.eq(1).addClass('hidden');
          }
        }
        self.off('change.stickyWidget').on('change.stickyWidget', function(){
          if(self.is(':checked')){
            if(idx){
              target.eq(1).removeClass('hidden');
              target.eq(0).addClass('hidden');
            }
            else{
              target.eq(0).removeClass('hidden');
              target.eq(1).addClass('hidden');
            }
          }
        });
      });
    },

    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
  };

  $(function() {
   $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));

(function($, window, undefined) {
  var pluginName = 'tabMenu';
  var win = $(window);
  var body = $(document.body);
  var html = $('html');
  var itemTab = $('li.tab-nav-item');
  var tabNav  = $('.tab-nav');
  var customRadio = $('.custom-radio');
  var customRadioFocus = $('[data-tab-nav-item]').find('.custom-radio');
  var centerPopup = function(el, visible, scrollTopHolder){
    el.css({
      top: Math.max(0, (win.height() - el.innerHeight()) / 2) + scrollTopHolder,
      display: (visible ? 'block' : 'none'),
      left: Math.max(0, (win.width() - el.innerWidth()) / 2)
    });
  };
  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.selectOnMobile = that.find(that.options.selectOnMobile);
      that.freezeBody = false;
      that.isIpad = /iPad/i.test(window.navigator.userAgent);
      that.scrollTopHolder = 0;
      var container = $(plugin.options.container);
      var tabContentOverlay = $();
      var tabMenuTimer = null;
      var isOpen = false;
      var isTablet = false;
      var onResize = null;
      var listTabs = that.find('.tab > .tab-item');
      // var isIE = function() {
      //  var myNav = navigator.userAgent.toLowerCase();
      //  return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
      // };
      itemTab.on('click', function() {
        var $this = $(this),
            itemClick = $(this).index(),
            itemActive = that.find('[data-tab-nav-item]');
        if(!$(this).is('.active')) {
          itemTab.removeClass('active');
          $(this).addClass('active');
          var tabItem =  itemActive.eq(itemClick);
          var tabItemIndex = itemActive.eq(itemClick).index();
          var customSelectActive = $('#travel-widget [data-customselect].active');
          itemActive.removeClass('active');
          if(itemClick === tabItemIndex){
            tabItem.addClass('active');
          }
          $this.parent().find('.tab-nav-item').removeClass('active');
          $this.parent().find('.tab-nav-item').attr('aria-selected', false);
          $this.addClass('active');
          $this.attr('aria-selected', true);

          if(customSelectActive.length > 0) {
            customSelectActive.removeClass('focus');
            customSelectActive.customSelect('hide');
          }

          return false;
        }
      });
      var customRadioTab = function(){
        var radioBook = that.find('#travel-radio-1'),
          radioRedeem = that.find('#travel-radio-2'),
          bookFlight = that.find('#book-flight'),
          bookRedeem = that.find('#book-redem');
          if (radioRedeem.is(':checked')){
            bookRedeem.addClass('active');
          }else{
            bookRedeem.removeClass('active');
          }
          if (radioBook.is(':checked')) {
            bookFlight.addClass('active');
          }else{
             bookFlight.removeClass('active');
          }
      };
      customRadio.on('change',function() {
        if(that.data('widget-v1')){
           customRadioTab();
        }
      });
      customRadioFocus.on('keydown', function(e){
        var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
        if(isSafari){
          var RadioFocusIndex = customRadioFocus.index();
          switch(e.keyCode){
            case 37: //left
               RadioFocusIndex = RadioFocusIndex - 1;
               if(RadioFocusIndex > length){
                  RadioFocusIndex = 0 ;
                  customRadioFocus.focus();
               }else if(RadioFocusIndex < 0){
                 RadioFocusIndex = length;
                 customRadioFocus.focus();
               }
            break;
            case 38: //UP
               RadioFocusIndex = RadioFocusIndex - 1;
               if(RadioFocusIndex > length){
                  RadioFocusIndex = 0 ;
                  customRadioFocus.focus();
               }else if(RadioFocusIndex < 0){
                 RadioFocusIndex = length;
                 customRadioFocus.focus();
               }
            break;
            case 39: //right
               RadioFocusIndex = RadioFocusIndex + 1;
               if(RadioFocusIndex > length){
                  RadioFocusIndex = 0 ;
                  customRadioFocus.focus();
               }else if(RadioFocusIndex < 0){
                 RadioFocusIndex = length;
                 customRadioFocus.focus();
               }
            break;
            case 40: //down
                RadioFocusIndex = RadioFocusIndex + 1;
               if(RadioFocusIndex > length){
                  RadioFocusIndex = 0 ;
                  customRadioFocus.focus();
               }else if(RadioFocusIndex < 0){
                 RadioFocusIndex = length;
                 customRadioFocus.focus();
               }
            break;
          }
        }
      });
        //
       itemTab.on('keydown', function(e){
        var $this = $(this), index = $this.index(),
        itemTabLength = itemTab.length;
        switch(e.keyCode){
          case 37: //left
            index = index - 1;
            if(index >= 0) {
              itemTab.eq(index).trigger('click');
            }
            break;
          case 39: //right
            index = index + 1;
            if(index <= itemTabLength) {
              itemTab.eq(index).trigger('click');
            }
          break;
          case 38: //up
            index = index - 1;
            if(index >= 0) {
              itemTab.eq(index).trigger('click');
            }
          break;
          case 40: //down
            index = index + 1;
            if(index <= itemTabLength) {
              itemTab.eq(index).trigger('click');
            }
          break;
          case 9:
            var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
            if(isSafari){
                $this.next().addClass('focus-outline');
                $this.next().focus();
            }
            break;
          case 13: // enter
             $this.trigger('click');
             var tabContentActive = $('.tab-content.active');
             var tabContentActiveItem = tabContentActive.find('[data-tab-nav-item].active');
             if(tabContentActiveItem.find('.custom-radio').length) {
                tabContentActiveItem.find('.custom-radio').children(':first').focus();
                tabContentActiveItem.find('.custom-radio').children(':first').parent().focus();
             }else if(tabContentActiveItem.find('input:first').length){
                tabContentActiveItem.find('input:first').focus();
             }
          break;
        }
      });
      if(that.options.isPopup){
        onResize = function(parent, self){
          centerPopup(parent, true, 0);
          parent.css('height', '');
          that.tabContent.eq(that.tab.index(self)).css('height', '');
          // container.css('height', '');
          if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) > win.height()){
            parent.find('.tab-content.active').css({
              height: win.height() - parseInt(that.tabContent.eq(that.tab.index(self)).css('padding-top')),
              'overflow-y': 'auto'
            });
            container.css({
              'marginTop': - that.scrollTopHolder,
              'height': win.height() + that.scrollTopHolder,
              'overflow': 'hidden'
            });
          }
          else{
            that.tabContent.eq(that.tab.index(self)).removeAttr('style');
            container.css({
              'marginTop': - that.scrollTopHolder,
              'height': win.height() + that.scrollTopHolder,
              'overflow': 'hidden'
            });
          }
          if(win.width() >= that.options.tablet - that.options.docScroll){
            tabContentOverlay.remove();
            parent.removeClass('animated fadeIn').removeAttr('style');
            that.tabContent.eq(that.tab.index(self)).css('height', '');
            if(that.freezeBody){
              body.removeAttr('style');
              html.removeAttr('style');
              that.freezeBody = false;
            }
            $(plugin.options.container).removeAttr('style');
            win.scrollTop(that.scrollTopHolder);
            that.scrollTopHolder = 0;
            isOpen = false;
            win.off('resize.resetTabMenu');
            return;
          }
          parent.show();
        };
      }

      //that.tabContent.removeClass('hidden').not(':eq(' + that.index + ')').hide();

      that.selectOnMobile.prop('selectedIndex', that.index);
      that.selectOnMobile.off('change.triggerTab').on('change.triggerTab', function(){
        if(that.data('tab')){
          that.tab.eq(that.selectOnMobile.prop('selectedIndex')).trigger('click.show');
        }
        else{
          if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).is('a')){
            window.location.href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).attr('href');
          }
          else{
            if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').length){
              var href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').attr('href');
              if(!href.match(':void')){
                window.location.href = href;
              }
            }
          }
        }
      }).triggerHandler('blur.resetValue');

      var changeTab = function(self){
        // if(!self.hasClass('hidden')) {
        that.tab/*.eq(that.index)*/.removeClass(that.options.activeClass);
        // }
        if(that.options.animation){
          that.tabContent.eq(that.index).fadeOut(that.options.duration);
        }
        else{
          if(!that.tab.eq(that.index + 1).is('.limit-item')) {
            that.tabContent.eq(that.index).removeClass(that.options.activeClass);
          }
        }

        if(!self.hasClass('hidden')) {
          self.addClass(that.options.activeClass);
        }

        that.index = that.tab.index(self);

        if(that.options.animation){
          that.tabContent.eq(that.index).css({
            'position': 'absolute',
            top: 80
          }).fadeIn(that.options.duration, function(){
            $(this).css('position', '');
          });
        }
        else{
          if(!that.tab.eq(that.index).is('.limit-item')) {
            that.tabContent
              .removeClass(that.options.activeClass)
              .eq(that.index).addClass(that.options.activeClass);
          }
          that.selectOnMobile.prop('selectedIndex', that.index);
        }
      };

      if(!that.data('tab')){
        return;
      }
      that.tab.off('click.show').on('click.show', function(e) {
        e.preventDefault();
        if($.isFunction(that.options.beforeChange)){
          var resultBeforeChange = that.options.beforeChange.call(that, that.tabContent, $(this));
          if(false === resultBeforeChange) {
            return false;
          }
        }

        // show as popup for tabMenu

        if(that.options.isPopup){
          var parent = that.tabContent.parent();
          var self = $(this);
          isOpen = true;

          if(window.innerWidth < that.options.tablet){
            changeTab(self);
            if(window.Modernizr.csstransitions){ // support css3
              that.scrollTopHolder = win.scrollTop();
              parent.show();
              tabContentOverlay = $(that.options.templateOverlay).appendTo(body).show().css({'opacity': 0.5, 'zIndex': that.options.zIndex}).addClass('animated fadeInOverlay');
              parent.css('display', 'block');
              centerPopup(parent, true, 0);
              parent.css('position', 'fixed');
              parent.addClass('animated fadeIn');
              $(plugin.options.container).css({
                'marginTop': - that.scrollTopHolder,
                'height': win.height() + that.scrollTopHolder,
                'overflow': 'hidden'
              });
              parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
                if(parent.outerHeight(true) > win.height()){
                  parent.find('.tab-content.active').removeAttr('style').css({
                    height: win.height() - parseInt(parent.find('.tab-content.active').css('padding-top')),
                    'overflow-y': 'auto'
                  });
                }
                centerPopup(parent, true, 0);
                isOpen = false;
                isTablet = true;
                win.scrollTop(0);
                parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
                if($.isFunction(that.options.afterChange)){
                  that.options.afterChange.call(that, that.tabContent, $(this));
                }
                that.trigger('afterChange');
              });
            }
            else{ // does not support css3
              if(window.innerWidth >= that.options.mobile){
                parent.css('display', 'block');
                centerPopup(parent, true, that.scrollTopHolder);
                parent.css('opacity', 0);
                that.scrollTopHolder = win.scrollTop();
                centerPopup(parent, true, that.scrollTopHolder);
                parent.animate({
                  opacity: 1
                }, that.options.duration);
                isOpen = false;
                tabContentOverlay = $(that.options.templateOverlay).appendTo(body).css({'opacity': 0.5, 'zIndex': that.options.zIndex}).fadeIn(that.options.duration);
              }
            }
            tabContentOverlay.off('click.closeTabContent').on('click.closeTabContent', function(e){
              e.preventDefault();
              that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
            });
            win.off('resize.resetTabMenu').on('resize.resetTabMenu', function(){
              clearTimeout(tabMenuTimer);
              tabMenuTimer = setTimeout(function(){
                onResize(parent, self);
              }, 10);
            });
            return;
          }
        }
        // if($(this).hasClass(that.options.activeClass)){
        //  return;
        // }
        if ($(this).is('[data-keep-limit]')) {
          return;
        }
        changeTab($(this));

        if($.isFunction(that.options.afterChange)){
          that.options.afterChange.call(that, that.tabContent, $(this));
        }

        var widget = $(this).closest('[data-widget-v2]');

        if(widget) {
          var btnCollapse = widget.find('[data-bookatrip] [data-trigger-collapsed]'),
              navWrapper = widget.find('[data-bookatrip] .tab-nav-wrapper');

          if(navWrapper.is('.tab-form-expand')) {
            btnCollapse.trigger('click.collapseFrom');
          }

        };

        that.trigger('afterChange');
      });

      if(that.options.isPopup){
        var closeOnTablet = function(parent){
          // var langToolbarEl = $('.toolbar--language');
          tabContentOverlay.addClass('animated fadeOutOverlay');
          parent.removeClass('fadeIn').addClass('animated fadeOut');
          parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
            $(that.options.container).removeAttr('style');
            // if(langToolbarEl.length) {
            //  $(that.options.container).css('padding-top', langToolbarEl.height());
            // }
            win.scrollTop(that.scrollTopHolder);
            tabContentOverlay.off('click.closeTabContent').remove();
            parent.removeClass('animated fadeOut');
            that.scrollTopHolder = 0;
            isOpen = false;
            parent.removeAttr('style');
            parent.find('.tab-content.active').removeAttr('style');
            parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
          });
        };
        that.find(that.options.triggerClosePopup).off('click.hideTabPopup').on('click.hideTabPopup', function(e){
          e.preventDefault();
          var parent = $(this).closest('.tab-wrapper');
          if(isOpen){
            return;
          }
          isOpen = true;
          if(window.innerWidth < that.options.tablet){
            if(window.Modernizr.csstransitions){ // support css3
              closeOnTablet(parent);
            }
            else{ // does not support css3
              parent.fadeOut(that.options.duration);
              tabContentOverlay.fadeOut(that.options.duration, function(){
                tabContentOverlay.remove();
                if(that.freezeBody){
                  body.removeAttr('style');
                  that.freezeBody = false;
                }
                win.scrollTop(that.scrollTopHolder);
                that.scrollTopHolder = 0;
                isOpen = false;
                parent.removeAttr('style');
              }).off('click.closeTabContent');
            }
            win.off('resize.resetTabMenu');
          }
          if($.isFunction(that.options.beforeClosePopup)){
            that.options.beforeClosePopup.call(that, that.tabContent, $(this));
          }
        });
      }

      that.on('keydown', function(e){
        var currActive = $(ally.get.activeElement());
        function moveTab(dir){
          var currTab = $(e.target).parents('.tab-item');
          var currTabInx;
          if(dir === 'next'){
              if(listTabs.index(currTab) === listTabs.length - 1){
                return;
              }
              currTabInx = listTabs.index(currTab) + 1;
              if(currTabInx >= listTabs.length){
                currTabInx = 0;
              }
          }else if(dir === 'prev'){
              if(listTabs.index(currTab) === 0 ){
                return;
              }
              currTabInx = listTabs.index(currTab) - 1;
              if(currTabInx < 0 ){
                currTabInx = listTabs.length - 1;
              }
          }
          var currentActiveTab = listTabs.eq(currTabInx);

          if(currentActiveTab && !currActive.is('input')) {
            ally.element.focus(currentActiveTab.children()[0]);
          }

          currentActiveTab.children().attr('tabindex', 0);
          currentActiveTab.siblings().children().attr('tabindex', -1);
          currentActiveTab.attr({
            'aria-selected': true
          });
          currentActiveTab.siblings().attr({
            'aria-selected': false
          });
          // if(currTabInx < 0 || currTabInx > listTabs.length - 1) {
          //  return;
          // }
          changeTab(currentActiveTab);
          currentActiveTab.trigger('click');

        }
        function moveContent(dir, tabContentSel){
          var currContent = $(e.target);
          var listContent = that.find(tabContentSel + '.active').find('[tabindex], button, a, input').not('.hidden').not('.main--tabs tab-item a');
          var currContentInx;

          if(dir === 'next'){
              currContentInx = listContent.index(currContent) + 1;
              if(currContentInx >= listContent.length){
                currContentInx = 0;
              }
          }else if(dir === 'prev'){
              currContentInx = listContent.index(currContent) - 1;
              if(currContentInx < 0 ){
                currContentInx = listContent.length - 1;
              }
          }
          var currentActiveContent = listContent.eq(currContentInx);
          if(!currContent.parents('.main--tabs').is('.main--tabs') && !currActive.is('input')) {
            ally.element.focus(currentActiveContent[0]);
          }
        }
        function handleKeyDownTab(tabContentSel) {
            switch(e.keyCode) {
                case 39:
                case 40:
                  if(currActive.parents(tabContentSel).length){
                    moveContent('next', tabContentSel);
                  }else{
                    moveTab('next');
                  }
                    break;
                case 37:
                case 38:
                  if(currActive.parents(tabContentSel).length){
                    moveContent('prev', tabContentSel);
                  }else{
                    moveTab('prev');
                  }
                    break;

                case 9:
                if(tabContentSel === '.tab-content-1') {
                  var fistTabindex = that.find(tabContentSel+'.active').find('[tabindex]').eq(0);

                  if(currActive.parents('.tab-content.tabs--4').find(tabContentSel+'.active').length){
                    if(currActive.is(fistTabindex) && e.shiftKey){
                      ally.element.focus(that.find('.main--tabs .tab-item.active').children()[0]);
                      e.stopPropagation();
                      e.preventDefault();
                      return false;
                    }
                    return true;
                  }
                    // if(e.shiftKey){
                    // console.log(document.activeElement);

                    //   return true;
                    // }
                  if(fistTabindex.length > 0) {
                    ally.element.focus(fistTabindex[0]);
                  }
                  return false;
                } else {
                  var fistTabindex = that.find(tabContentSel+'.active').find('[aria-selected="true"]');

                  if(currActive.parents(tabContentSel).length){
                    if(currActive.is(fistTabindex) && e.shiftKey){
                      ally.element.focus(that.find('.tab-item.active').children()[0]);
                      e.stopPropagation();
                      e.preventDefault();
                      return false;
                    }
                    return true;
                  }else{
                    if(e.shiftKey){
                      return true;
                    }
                  }
                  if(fistTabindex.length > 0) {
                    ally.element.focus(fistTabindex[0]);
                  }
                  return false;
                }


              case 36:
                  if(currActive.parents(tabContentSel).length){
                    return true;
                  }
                  var targetEle = listTabs.eq(0).children();
                  if(targetEle.length > 0) {
                    ally.element.focus(targetEle[0]);
                  }

                  changeTab(targetEle);
                    targetEle.trigger('click');
                  return false;

              case 35:
                  if(currActive.parents(tabContentSel).length){
                    return true;
                  }
                  var targetEle = listTabs.eq(listTabs.length - 1).children();
                  if(targetEle.is('[data-customselect]')){
                    return;
                  }
                  if(targetEle.length > 0) {
                    ally.element.focus(targetEle[0]);
                  }
                  changeTab(targetEle);
                  targetEle.trigger('click');
                  return false;
          }
        }

        if(that.is('.tab-content')){
          listTabs = that.find('.main--tabs .tab-item');
          handleKeyDownTab('.tab-content-1');
        } else {
          listTabs = that.find('.tab .tab-item').not('.tab-item-level--2');
          handleKeyDownTab('.tab-content');
        }
      });
    },
    update: function(){
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.freezeBody = false;
    },
    option: function(options) {
      this.element.options = $.extend({}, this.options, options);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    tab: '',
    tabContent: '',
    activeClass: 'active',
    animation: false,
    duration: 500,
    isPopup: false,
    tablet: 988,
    container: '#container',
    mobile: 768,
    zIndex: 14,
    templateOverlay: '<div class="overlay"></div>',
    triggerClosePopup: '.tab-wrapper .popup__close',
    selectOnMobile: '> select',
    docScroll: window.Modernizr.touch ? 0 : 20,
    beforeClosePopup: function(){},
    afterChange: function() {},
    beforeChange: function(){}
  };

}(jQuery, window));

/**
 *  @name tooltip
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'kTooltip';
	var win = $(window);
	var tooltipOnDevice = null;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
			that = plugin.element;
			that.options = plugin.options;
			that.isShow = false;
			that.timer = 0;
			that.tooltip = $();
			that.maxWidth = plugin.options.maxWidth;
			if(true){
				// this transform only applies to the fare table tooltips
				var oldContent = that.data('content');
				var newContent = oldContent.replace('class="title-conditions"', 'class="title-conditions" id="tooltip-title"');
				that.attr({
					'data-content': newContent,
					'aria-label': 'View more information'
				});

				that.bind('click.showTooltip', function(e){
					e.preventDefault();
					if(that.hasClass('disabled') && !that.data('open-tooltip')){
						return;
					}
					e.stopPropagation();
					if(tooltipOnDevice){
						tooltipOnDevice.tooltip.remove();
						tooltipOnDevice.tooltip = $();
						tooltipOnDevice.isShow = false;
						win.off('resize.hideTooltip');
						win.off('click.hideTooltip');
						win.off('keyup.escapeTooltip');
						$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
						tooltipOnDevice = null;
					}
					if(that.isShow){
						return;
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
			}
			else{
				that.off('click.showTooltip').on('click.showTooltip', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(that.isShow){
						return;
					}
					if(that.timer){
						clearTimeout(that.timer);
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
				that.off('mouseleave.hideTooltip').on('mouseleave.hideTooltip', function(){
					that.timer = setTimeout(function(){
							that.tooltip.remove();
							that.tooltip = $();
							that.isShow = false;
							win.off('resize.hideTooltip');
						}, 200);
				});
				// that.hover(
				// 	function(){
				// 		if(that.timer){
				// 			clearTimeout(that.timer);
				// 		}
				// 		plugin.generateTooltip();
				// 	},
				// 	function(){
				// 		that.timer = setTimeout(function(){
				// 			that.tooltip.remove();
				// 			that.tooltip = $();
				// 		}, 200);
				// 	}
				// );
			}
		},
		generateTooltip: function(){
			var plugin = this,
			that = plugin.element;
			tooltipOnDevice = that;
			if(that.tooltip.length){
				return;
			}
			// if(that.attr('data-type') === 2){
			// 	that.tooltip = $(that.options.template2).appendTo(document.body);
			// }
			// else if(that.attr('data-type') === 3){
			// 	that.tooltip = $(that.options.template3).appendTo(document.body);
			// }
			// else{
			// 	that.tooltip = $(that.options.template1).appendTo(document.body);
			// 	that.tooltip.find('a.popup_close').unbind('click.close').bind('click.close', function(e){
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		that.tooltip.remove();
			// 		that.tooltip = $();
			// 	});
			// }
			if(that.data('type') === 2){
				that.tooltip = $(that.options.template2).appendTo(document.body);
				that.tooltip.find('a.tooltip__close').off('click.close').on('click.close', function(e){
					e.preventDefault();
					e.stopPropagation();
					that.tooltip.remove();
					that.tooltip = $();
					that.isShow = false;
					win.off('resize.hideTooltip');
					$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
				});
			}
			else if(that.data('type') === 3){
				that.tooltip = $(that.options.template3).appendTo(document.body);
			}
			else{
				that.tooltip = $(that.options.template1).appendTo(document.body);
			}

			// that.tooltip = $(that.options.template1).appendTo(document.body);
			that.tooltip.show().find('.tooltip__content').html(that.attr('data-content'));

			// add aria-labels to content
			// IE11+JAWS doesn't read the aria-live
			that.tooltip.find('.summary-fare__conditions .ico-check-thick').after('<span class="says">You may</span>');
			that.tooltip.find('.summary-fare__conditions .ico-close').after('<span class="says">You may not</span>');

			that.tooltip.off('click.preventDefault').on('click.preventDefault', function(e){
				e.stopPropagation();
			}).find('a.tooltip__close').unbind('click.close').bind('click.close', function(e){
				e.preventDefault();
				/*e.stopPropagation();
				that.tooltip.remove();
				that.tooltip = $();*/
				plugin.closeTooltip();
			});

			var actualWidth = that.tooltip.outerWidth(true);
			if(that.data('max-width')){
				that.maxWidth = that.data('max-width');
			}
			if(actualWidth > that.maxWidth){
				actualWidth = that.maxWidth;
			}
			that.tooltip.innerWidth(that.maxWidth);
			that.tooltip.css({
				'max-width': that.tooltip.width()
			}).css('width', '');


			var left = Math.max(0, that.offset().left - actualWidth/2 + that.outerWidth(true)/2 + 5);

			var top = (that.data('type') === 3) ? that.offset().top  + that.innerHeight() : that.offset().top - that.tooltip.outerHeight() - 10;

			if((left + actualWidth) > $(window).width()){
				left = $(window).width() - actualWidth;
			}

			that.tooltip.css({
				position: 'absolute',
				'z-index': 999999,
				// top: that.offset().top - that.tooltip.outerHeight() - that.height()/2,
				top: top,
				left: left
			});
			that.tooltip.find('.tooltip__arrow').css({
				left: that.offset().left - left + that.width()/2
			});

			win.off('click.hideTooltip').on('click.hideTooltip', function(){
				plugin.closeTooltip();
			});
			win.off('keyup.escapeTooltip').on('keyup.escapeTooltip', function(e){
				if (e.which === 27) {
					plugin.closeTooltip();
				}
			});
			win.off('resize.hideTooltip').on('resize.hideTooltip', function(){
				plugin.closeTooltip();
			});
			// TODO: this is triggered when the document scrolls to reveal the popup
			//$(document).off('scroll.hideTooltip mousewheel.hideTooltip').on('scroll.hideTooltip mousewheel.hideTooltip', function(){
			//	console.log('closing due to scroll');
			//	plugin.closeTooltip();
			//});

			if($.isFunction(that.options.afterShow)){
				that.options.afterShow.call(that, that.tooltip);
			}
		},
		closeTooltip: function(){
			var plugin = this,
				that = plugin.element;

			if(that.tooltip.length){
				that.isShow = false;
				that.tooltip.remove();
				that.tooltip = $();
				win.unbind('click.hideTooltip');
				win.unbind('keyup.escapeTooltip');
				tooltipOnDevice = null;
			}
			win.off('resize.hideTooltip');
			//$(document).off('scroll.hideTooltip mousewheel.hideTooltip'); // see above

			if($.isFunction(that.options.afterClose)){
				that.options.afterClose.call(that, that.tooltip);
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		afterShow: function(){
			if ( this.data('type') === 2 || this.data('type') === 3 ) {

				this.addClass('current');

				this.tooltip.attr({
					'tabindex' : '0',
					'open' : 'true',
					'role' : 'dialog'
				});

				if(!this.hasClass('info-card')){
					this.tooltip.attr({
						'aria-describedby': (this.parents('.tooltip-wrapper').length>0)? this.parents('.tooltip-wrapper').attr('data-aria') : 'bsp-wcag-' + this.parent().attr('tabindex')
					});
				}else{
					this.tooltip.attr({
						'aria-describedby': 'bsp-wcag-current'
					});
				}


				var help = 'Beginning of dialog window. Press escape to cancel and close this window.'; // TODO l10n.prop

				this.tooltip.prepend('<p id="tooltip--help" class="says">' + help + '</p>');

				this.tooltip.find('.tooltip__content').attr({
					'role' : 'document', // NVDA doesn't read dialogs see https://github.com/nvaccess/nvda/issues/4493
					'tabindex' : '0',
					'aria-describedby' : 'tooltip--help'
				}).focus();
				this.tooltip.find('.tooltip__content .summary-fare').attr({
					'aria-hidden':true
				});
			}
		},
		afterClose: function(){
			if ( this.data('type') === 2 ) {
				if(this.parents('li').next().length>0){
					this.parents('li').next().focus();
				}else{
					$('[data-tooltip].current').attr('tabindex', -1).focus();
					this.removeClass('current');
				}

			}
			if ( this.data('type') === 3 ) {
				if($('[data-aria="'+this.parents('.tooltip-wrapper').data('aria')+'"]').length > 0 ){
					$('[data-aria="'+this.parents('.tooltip-wrapper').data('aria')+'"]').focus();
				}else{
					$('[data-tooltip].current').focus();
					this.removeClass('current');
				}

			}
		},
		maxWidth: 240,
		template1: '<div class="tooltip"><span class="tooltip__content"></span><em class="tooltip__arrow"></div>',
		template2: '<aside class="tooltip tooltip--info"><em class="tooltip__arrow"></em><div class="tooltip__content"></div><a href="#" class="tooltip__close" aria-label="Close button">&#xe60d;</a></aside>',
		template3: '<aside class="tooltip tooltip--conditions-1"><em class="tooltip__arrow type-top"></em><div class="tooltip__content"></div><a href="#" class="tooltip__close" aria-label="Close button">&#xe60d;</a></aside>'

	};

}(jQuery, window));

/**
 * @name SIA
 * @description Define global wcag language functions
 * @version 1.0
 */

SIA.WcagGlobal = (function(){

  var global = SIA.global;

  var onInitHandler = function(){

    closeButton();
    customRadioLabel();
    customCheckboxLabel();
    addAriaLabel();
    thePopupLanguage();
    dataSearchFlights();
    assignSiblingDescriptionAttributes({
      descriptionSelector: '[data-customselect] .select__tips:not([data-description-category="customSelectUsage"])',
      descriptionCategory: 'tip',
      genericSiblingHostSelector: '.input-overlay'
    });
    attachDescription('[data-customselect] .select__tips');
    disableSelects();
    externalLinks();
    rangeSlider();
    //removeEmptyElements();
    initKeyboardOutlines();
  };

  var addAriaLabel = function(){
    var textCountry = $('[name="text-country"]');
    textCountry.attr({
      'aria-label': 'Country / Region'
    });
  };

  /** Remove Empty Labels **/
  var removeEmptyElements = function(){
    /** List elements that are empty **/
    var emptyEls = 'label.hidden, h2, h3, h4, h5, h6';

    $(emptyEls).each(function(){
      var self = $(this);
      var text = $(this).text();
      if(!text.trim()){
        self.remove();
      }
    });

  };

  /** Close popup, ESC Key **/
  var closeButton = function(isModal){

    if(!isModal){
      isModal = false;
    }

    if(isModal){
      var popup = $('.popup:not(.hidden)');
      var popupContent = $('.popup__content', popup);
      var popupClose = $('.popup__close', popup);
      popupClose.attr({
        'aria-label':'Close button'
      });
      popupContent.append(popupClose);

    }else{
      var popup = $('.popup');
      popup.each(function(){
        var self = $(this);
        var popupContent = $('.popup__content', self);
        var popupClose = $('.popup__close', self);
        popupClose.attr({
          'aria-label':'Close button'
        });
        popupContent.append(popupClose);

      });
    }

  };

  var assignAttributes = function(){

    var popup = $('.popup:visible');

    var ppContent = $('.popup__content', popup);
    var textCountry = $('#text-country', popup);
    var ppInner = $('.popup__inner', popup);
    var ppUiComplete = $('.ui-autocomplete:visible', popup);
    var ppHeading = $('.popup__heading', popup);
    var menuSubInner = $('.menu-sub-inner', popup);

    popup.attr({
      'role':'dialog',
      'tabindex':-1
    });

    ppInner.attr({
      'tabindex':-1
    });

    if(!popup.hasClass('popup--logged-profile')){
      ppContent.attr({
        'role':'document',
        'tabindex':-1
      });
    }else{
      menuSubInner.attr({
        'role':'document',
        'tabindex':0
      });
    }


    ppHeading.attr({
      'tabindex': 0,
      'aria-hidden': false
    }).focus();

  };

  // var ppMoveFocus = function(){
  //   var popup = $('.popup:not(.hidden)');
  //   var ppHeading = popup.find('.popup__content').children(':not(.popup__close)').first();
  //   ppHeading.attr({
  //     'tabindex': 0
  //   }).focus();
  // };

  var ppMoveFocus = function(){
    //Popup will focus whole popup when show
    var popup = $('.popup:not(.hidden)');
    var ppHeading = popup.find('.popup__content');
    ppHeading.addClass('focus-outline');
    setTimeout(function(){
      ppHeading.attr({
        'tabindex': 0
      }).focus();
    },200);
  };

  var externalLinks = function(){
    var linkCheck = $('a.external');

    linkCheck.each(function(){
      var self = $(this);
      var linkText = self.text();
      self.attr({
        'aria-label': 'External Link. ' + linkText
      });
    });

  };

  var customRadioLabel = function(){
    var customRadio = $('.popup .custom-radio');
    customRadio.each(function(){
      var self = $(this);
      var customRadioInput = $('input[type="radio"]', self);
      var customRadioLabel = $('label[for]', self);

      customRadioInput.attr({
        'aria-label': customRadioLabel.text()
      });
    });
  };

  var customCheckboxLabel = function(){
    var customCheckbox = $('.custom-checkbox');
    customCheckbox.each(function(){
      var self = $(this);
      var customCheckboxInput = $('input[type="checkbox"]', self);
      var customCheckboxLabel = $('label[for]', self);

      customCheckboxInput.attr({
        'aria-label': customCheckboxLabel.text()
      });
    });
  };

  var customSelectAriaLive = function(selectText){

    if( selectText.find('#wcag-custom-select').length > 0 ){
      selectText.find('#wcag-custom-select').remove();
    }

    $( '<span>', {
				role: 'status',
				"aria-live": 'assertive',
        id: 'wcag-custom-select'
			})
			.addClass( 'ui-helper-hidden-accessible' )
			.appendTo( selectText );
  };

  var customSelectAriaLiveLang = function(search){

    if($(search).parent().find('#wcag-autocomplete-lang').length>0){
      $(search).parent().find('#wcag-autocomplete-lang').remove();
    }

    $( '<span>', {
				role: 'status',
				"aria-live": 'polite',
        id: 'wcag-autocomplete-lang'
			})
			.addClass( 'ui-helper-hidden-accessible' )
			.appendTo( $(search).parent() );

  };

  var thePopupLanguage = function(){
    var popup = $('.popup--language');
    var customSelect = $('.custom-select', popup);
    var focusable = ['.popup__heading', '.select__text'];

    popup.find('.popup__content *').attr({
      'tabindex': -1,
      'aria-hidden': true
    });

    $('.popup__heading').removeAttr('aria-label');

    popup.find('[aria-live].aria-live, form, fieldset, .form-group, .custom-select, .custom-radio, .custom-checkbox').attr({
      'aria-hidden': false
    });

    popup.find('input[name="language"], input[name="submit-2"], input[name="checkbox-2"], .popup__close').attr({
      'tabindex': 0,
      'aria-hidden': false
    });


    customSelect.each(function(){
      var self = $(this);
      var customSelectImage = self.find('img');
      var customSelectLabel = self.find('label[for]');
      var selectText = self.find('.select__text');
      var selfHeading = self.find('.popup__heading');
      var selfFlag = $('a.flag');

      // self.find('img, label[for], input, .ico-dropdown').attr({
      //   'aria-hidden': true,
      //   'tabindex': -1
      // });

      // self.find('[aria-live]').attr({
      //   'tabindex': -1
      // });
      //

      self.attr({
        'aria-hidden': false
      });

      selfHeading.attr({
        'tabindex': 0,
        'aria-hidden': false
      });

      selectText.attr({
        'aria-label':'Country / Region',
        'tabindex': 0,
        'aria-hidden': false,
        'role':'combobox'
      });
      //
      // selectText.off('keydown').on('keydown', function(e){
      //   var keyCode = e.keyCode || e.which || e.charCode;
      //   var selfSelectText = $(this);
      //   if(keyCode === 9){
      //     selectText.find('input').trigger('blur.highlight');
      //     selectText.find('input').autocomplete('close');
      //     selfSelectText.parents('.form-group').next().find('input:first').focus();
      //   }
      // });
      //
      self.find('.select__text').focus(function(){
        $(this).find('input').trigger('focus.highlight');
      });
      //
      // customSelect.find('*').focus(function(){
      //   console.log($(this));
      // });

    });
  };

  var dataSearchFlights = function(){
    var dataSearchFlights = $('[data-search-flights]');
    var dataSearchFlightsForm = $('[data-search-flights-form]');
    var formGroupTooltips = $('.form-group--tooltips');

    formGroupTooltips.each(function(){
      var self = $(this);

      /** Check if the radio is inside the form **/
      if(self.closest('form').length>0){
        return;
      }

      var selfChild = self.children();
      var selfParents = self.parents('.form-group--tooltips');
      var focusPos = 0;


      self.attr({
        'role':'radiogroup'
      });

      selfChild.each(function(){
        var child = $(this);
        var dataSearchFlightsValue = child.find('input').data('search-flights');
        var form = $('[data-search-flights-form="'+dataSearchFlightsValue+'"]');

        child.attr({
          'aria-describedby': 'wcag-form-' + dataSearchFlightsValue,
          'role': 'radio',
          'tabindex': -1,
          'aria-checked':false,
          'aria-label': child.find('label').text()
        });


        $('.radio-tooltips__text', form).attr('id', 'wcag-form-' + dataSearchFlightsValue);

        child.find('input').attr({
          'tabindex': -1
        });

        child.find('input').next().attr({
          'aria-hidden':true,
          'tabindex':-1
        });

        if(child.index() === 0){
          child.attr({
            'tabindex': 0,
            'aria-checked':true
          });
        }

      });

      selfChild.off('keyup').on('keyup', function(e){
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 39){
          if(focusPos !== selfChild.length-1){
            focusPos++;
            selfChild.eq(focusPos).focus();
          }else{
            selfChild.eq(0).focus();
            focusPos = 0;
          }
        }

        if(keyCode === 37){
          if(focusPos !== 0){
            focusPos--;
            selfChild.eq(focusPos).focus();
          }else{
            selfChild.eq(selfChild.length-1).focus();
            focusPos = selfChild.length-1;
          }
        }

      });

      selfChild.off('focus').on('focus', function(e){
        var selfFocus = $(this);
        selfFocus.find('input').trigger('click');
        selfFocus.attr({
          'aria-checked':true,
          'tabindex': 0
        });
        selfFocus.siblings().attr({
          'aria-checked':false,
          'tabindex': -1
        });
      });

    });
  };

  var assignDescriptionAttributes = function(options) {

    var defaultOptions = ({
      descriptionSelector: false,
      descriptionCategory: false,
      unique_host_selector: false
    });

    var _options = $.extend(defaultOptions, options);

    if ( !_options.descriptionSelector || !_options.descriptionCategory || !_options.unique_host_selector ) {
      return;
    }

    var descriptionSelector = _options.descriptionSelector;
    var descriptionCategory = _options.descriptionCategory;
    var unique_host_selector = _options.unique_host_selector;

    $(descriptionSelector).each( function(i, item) {
      var $description = $(item),
          $host = $(unique_host_selector),
           hostId = $host.attr('id') || 'description-host-' + i + '_js';

      $host.attr('id', hostId);

      $description.attr({
        'data-description-host': '#' + hostId,
        'data-description-category': descriptionCategory
      });
    });
  };

  var assignSiblingDescriptionAttributes = function(options) {

    var defaultOptions = ({
      descriptionSelector: false,
      descriptionCategory: false,
      genericSiblingHostSelector: false
    });

    var _options = $.extend(defaultOptions, options);

    if ( !_options.descriptionSelector || !_options.descriptionCategory || !_options.genericSiblingHostSelector ) {
      return;
    }

    var descriptionSelector = _options.descriptionSelector;
    var descriptionCategory = _options.descriptionCategory;
    var genericSiblingHostSelector = _options.genericSiblingHostSelector;

    $(descriptionSelector).each( function(i, item) {
      var $description = $(item),
          $host = $description.siblings(genericSiblingHostSelector).eq(0),
           hostId = $host.attr('id') || 'description-host-' + i + '_js';

      $host.attr('id', hostId);

      $description.attr({
        'data-description-host': '#' + hostId,
        'data-description-category': descriptionCategory
      });
    });
  };

  var attachDescription = function(descriptionSelector) {

    var $_descriptions = $(descriptionSelector),
        $_description,
        $_host,
         _id,
         _host_descriptions,
         _old_id_list = [],
         _new_id_list = [],
         _str = '';

    // a host may have mutiple descriptions
    $_descriptions.each( function(i, item) {

      // the current description
      $_description = $(item);

      // get host selector
      $_host = $( $_description.data('description-host') );

      // if the host canot be found, exit
      if ( ! $_host.length ) {
        return;
      }

      // get or create description id
      _id = $_description.attr('id') || 'description-' + i + '_js';
      $_description.attr('id', _id);

      // Inject a vocal pause between tips, by adding one to this description
      // But NVDA reads . as 'dot'
      // And IE/JAWS duplicates the description text into the span
      // if ( ! $_description.find('[data-wcag-pause]').length ) {
      //  $_description.append('<span class="ui-helper-hidden-accessible" data-wcag-pause>, </span>');
      // }

      // get the existing list of description ids
      _host_descriptions = $_host.attr('aria-describedby') || '';

      // ids are stored as a space separated list
      // convert into an array
      _old_id_list = _host_descriptions.split(' ');

      // reset the array and string for each description
      _new_id_list = [];
      _str = '';

      // add the new ID to the end of the old list
      // so that it is announced after the existing descriptions
      _old_id_list.push(_id);

      // filter out any duplicate IDs
      // http://stackoverflow.com/a/9229932/6850747
      // this prevents screen readers from reading an attached description multiple times
      $.each(_old_id_list, function(i, el){
        if( $.inArray(el, _new_id_list) === -1 ) {
          _new_id_list.push(el);
        }
      });

      // build a new space separated string
      $.each(_new_id_list, function(i, _id) {
        _str += _id + ' ';
      });

      // remove any leading or trailing spaces
      _str = $.trim( _str );

      // attach the description
      if ( _str.length ) {
        $_host.attr('aria-describedby', _str );
      }

      // reveal description to AT
      $_description.removeAttr('aria-hidden');
    });
  };

  var detachDescription = function(descriptionSelector) {

    if ( !descriptionSelector ) {
      return;
    }

    var $_descriptions = $(descriptionSelector),
        $_description,
        $_host,
         _id,
         _host_descriptions,
         _old_id_list = [],
         _old_id_index,
         _str = '';

    // a host may have mutiple descriptions
    $_descriptions.each( function(i, item) {

      // the current description
      $_description = $(item);

      // get host selector
      $_host = $( $_description.data('description-host') );

      // if the host canot be found, exit
      if ( ! $_host.length ) {
        return;
      }

      // get description id
      _id = $_description.attr('id');

      // get the existing list of description ids
      _host_descriptions = $_host.attr('aria-describedby') || '';

      // ids are stored as a space separated list
      // convert into an array
      _old_id_list = _host_descriptions.split(' ');

      // location of this description's id in the array
      _old_id_index = _old_id_list.indexOf(_id);

      // remove this description ID from the list
      if (_old_id_index !== -1) {
        _old_id_list.splice(_old_id_index, 1);
      }

      // if this description was the only one attached to this host
      // remove the attachment attribute
      if ( ! _old_id_list.length ) {
        $_host.removeAttr('aria-describedby');
      }
      else {
        // build a string from the array
        $.each(_old_id_list, function(i, id) {

          id = $.trim(id);

          if ( id.length ) {
            if ( i > 0 ) {
              _str += ' ';
            }
            _str += id;
          }
        });
      }

      _str = $.trim( _str );

      if ( _str.length ) {
        $_host.attr('aria-describedby', _str );
      }

      // hide the description from AT
      // as we're leaving it in the DOM for reattachment later
      $_description.attr('aria-hidden', true);
    });
  };

  var isIE = function() {

    // http://stackoverflow.com/a/25380644/6850747
    var msie = parseInt((/msie (\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);

    if (isNaN(msie)) {
      msie = parseInt((/trident\/.*; rv:(\d+)/.exec(navigator.userAgent.toLowerCase()) || [])[1]);
    }

    return msie ? true : false;
  };

  var disableSelects = function() {

    var $disabled = $('[data-customselect].disabled');

    $disabled.customSelect('disable');
  };

  var detectIE = function () {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      var rv = ua.indexOf('rv:');
      return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      return true;
    }

    return false;
  }

  var rangeSlider = function() {


    $('.ui-slider_title').attr({
      'tabindex': 0
    });
    $('.ui-slider-result').attr({
      'tabindex': 0
    });
    $('.ui-slider-handle').each(function(index){
      var fromEl = $('.ui-slider_from'),
          toEl = $('.ui-slider_to'),
          minPrice = fromEl.html(),
          maxPrice = toEl.html(),
          _self = $(this),
          oneButton = false;

      if(fromEl.length === 0 && toEl.length === 0) {
        oneButton = true;
        var priceSpec = $('[data-mile="true"]').html().split(" ")[0];
        minPrice = $('#slider-range').data('kris-min');
        maxPrice = $('#slider-range').data('kris-max');
      }

      if(detectIE() === true) {
        if(_self.find('.ui-helper-hidden-accessible').length  === 0) {
          _self.append('<span class="ui-helper-hidden-accessible" id="ui-slider-handle_label' + (index + 1)+ '"></span>');
        }
      }

      _self.attr({
        'role': 'slider',
        'aria-valuemin': minPrice,
        'aria-valuemax': maxPrice,
        'aria-labelledby': 'ui-slider-handle_label' + (index + 1),
        'aria-controls': 'ui-slider-handle_text' + (index + 1),
        'tabindex': 0
      });

      if(index === 0) {
        if(fromEl.length === 0 && toEl.length === 0) {
          _self.attr({
            'aria-valuenow': priceSpec,
            'aria-valuetext': priceSpec,
            'title': priceSpec
          });
          _self.find('#ui-slider-handle_label' + (index + 1)).text(priceSpec);
          _self.append('<span class="ui-helper-hidden-accessible">current</span>');
        } else {
          _self.attr({
          'aria-valuenow': minPrice,
          'aria-valuetext': minPrice,
          'title': minPrice
         });
        _self.find('#ui-slider-handle_label' + (index + 1)).text(minPrice);
         _self.append('<span class="ui-helper-hidden-accessible">minimum</span>');
        }
      } else {
        _self.attr({
          'aria-valuenow': maxPrice,
          'aria-valuetext': maxPrice,
          'title': maxPrice
        });
        _self.find('#ui-slider-handle_label' + (index + 1)).text(maxPrice);
        _self.append('<span class="ui-helper-hidden-accessible">maximum</span>');
      }

      _self.on('keyup', function(e){

        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 34 || keyCode === 33 || keyCode === 37 || keyCode === 38 || keyCode === 39 || keyCode === 40 || keyCode === 35 || keyCode === 36) {

          var price, title;

          if(index === 0) {
            if(fromEl.length === 0 && toEl.length === 0) {
              price = $('[data-mile="true"]').html().split(" ")[0];
            } else {
              price = fromEl.html();
            }
          } else {
            price = toEl.html();
          }

          _self.find('#ui-slider-handle_label' + (index + 1)).text(price);

          _self.attr({
            'aria-valuenow': price,
            'aria-valuetext': price,
            'title': price
          });
        }
      });
    });
  };

  var initKeyboardOutlines = function() {
    $('*').on('keydown.tab', function(e){
      /*
      TAB or Shift Tab, Aw.
      Add some more key code if you really want
      */
      if ( 9== e.which && this == e.target ){
        window.setTimeout( function(){
          $('.focus-outline').removeClass('focus-outline');
          $(document.activeElement).addClass('focus-outline');

          if(document.activeElement.type == 'radio') {
            initRadioButtonOutlines();
          } else {
            $(document).off('keydown.radioLogic');
          }
        }, 50 );
      }
    });
  }

  var initRadioButtonOutlines = function() {
    $(document).on('keydown.radioLogic', function(e) {
      if(e.which == 37 || e.which == 38 || e.which == 39 || e.which == 40) {
        window.setTimeout( function(){
          $('.focus-outline').removeClass('focus-outline');
          $(document.activeElement).addClass('focus-outline');
        }, 50 );
      }
    });
  }

  return {
    init: onInitHandler,
    addAriaLabel: addAriaLabel,
    closeButton: closeButton,
    assignAttributes: assignAttributes,
    ppMoveFocus: ppMoveFocus,
    externalLinks: externalLinks,
    customRadioLabel: customRadioLabel,
    customCheckboxLabel: customCheckboxLabel,
    customSelectAriaLive: customSelectAriaLive,
    customSelectAriaLiveLang: customSelectAriaLiveLang,
    thePopupLanguage: thePopupLanguage,
    dataSearchFlights: dataSearchFlights,
    assignDescriptionAttributes: assignDescriptionAttributes,
    assignSiblingDescriptionAttributes: assignSiblingDescriptionAttributes,
    attachDescription: attachDescription,
    detachDescription: detachDescription,
    isIE: isIE,
    disableSelects: disableSelects,
    rangeSlider: rangeSlider
  };

})();


setTimeout(function(){

  SIA.WcagGlobal.init();

  var currentClickedElement;

  $(document).on('click', 'a', function(){
    var linkSelf = $(this);
    setTimeout(function(){
      if($('.popup:visible').length>0){
        if(linkSelf.hasClass('active')){
          linkSelf = linkSelf.removeClass('active');
        }
        currentClickedElement = linkSelf;
      }
    }, 1000);

  });

  $(document).on('keydown', 'a', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;

    if(keyCode === 13){
      var linkSelf = $(this);
      setTimeout(function(){
        if($('.popup:visible').length>0){
          if(linkSelf.hasClass('active')){
            linkSelf = linkSelf.removeClass('active');
          }
          currentClickedElement = linkSelf;
        }
      }, 1000);
    }

  });

  $(document).on('keydown', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;
    var popup = $('.popup:visible');
    var popupClose = $('.popup__close');
    var overlay = $('.overlay');
    var self = $(':focus');
  var enterToContent = function(timeout) {
    if(self.parent().hasClass('tab-item') || self.is('.keyboardfocus')){
      self.trigger('click');
      if (self.parent().is('.limit-item')) {
        // dropdown
        setTimeout(function(){
          var dropdownMenuSelect = self.find('input.input-overlay').attr('aria-owns');
          if (dropdownMenuSelect) {
            $('#' + dropdownMenuSelect).find('li:first').attr('tabindex', 0).focus();
          }
          }, timeout);
      } else  {
        var tabContentActive = self.parents('[data-tab]').find('.tab-content.active');
        var tabContentActiveHeading = $('.main-heading', tabContentActive);
        if(tabContentActive.find('.tab-nav').length) {
          tabContentActive.find('.tab-nav').find('a:first').attr('tabindex', 0).focus();
        } else if (tabContentActive.find('input:first').length) {
          e.preventDefault() // Prevent close accordion when focus
          tabContentActive.find('input:first').attr('tabindex', 0).focus();
        } else if (tabContentActive.find('.tab a:first').length) {
          tabContentActive.find('.tab a:first').attr('tabindex', 0).focus();
        }
      }

    }
  }
    if(keyCode === 27){
      if(overlay.length>0){
        if($(currentClickedElement).parent().hasClass('logged-in')){
          setTimeout(function(){
            $(currentClickedElement).parent().attr('tabindex', -1).focus();
          },600);
        } else {
          $(currentClickedElement).focus();
        }
        overlay.trigger('click');
      }
    }
    if(keyCode === 13){
      enterToContent(300);
    }
    if(keyCode === 9) {
      var tabWrapperSel = '.tab-level-1';
      if (e.shiftKey) {
        setTimeout(function(){
          var wrapper = self.closest('.multi-tabs--1').find(tabWrapperSel);
          if (wrapper.find('.tab-item.active a').length) {
            wrapper.find('.tab-item.active a').attr('tabindex', 0).focus();
          }
          if (wrapper.find('.tab-item.active [data-customselect]').length) {
            wrapper.find('.tab-item.active [data-customselect]').attr('tabindex', 0).focus();
          }
        }, 500);

      } else if (self.closest(tabWrapperSel).length) {
        e.preventDefault();
        enterToContent(0);
      }
    }
  });


  $('.popup__close').on('keydown', function(e){
    var close = $(this);
    var keyCode = e.keyCode || e.which || e.charCode;
    var overlay = $('.overlay');
    if(keyCode === 13 || keyCode === 9){
      if(overlay.length>0){
        close.trigger('click');
        /* Only set the last clicked element if one existed first */
        if(currentClickedElement) {
          $(currentClickedElement).focus();
        }
      }
    }
  });

  /** Prevent focus moving to skip to main content **/
  $('.popup--language .popup__heading').off('keydown').on('keydown', function(e){
    var keyCode = e.keyCode || e.which || e.charCode;
    if(keyCode === 27){
      $('.flag').focus();
    }

  });


}, 5000);
