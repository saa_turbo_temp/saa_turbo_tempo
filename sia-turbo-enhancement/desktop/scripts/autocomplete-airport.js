/**
 * @name SIA
 * @description Define global autocomplete search functions
 * @version 1.0
 */
SIA.autocompleteAirport = function(){
  var vars = SIA.global.vars;
  var win = vars.win;
  var doc = vars.doc;
  var searchEl = $('[data-autocomplete-airport]');
  var term = '';
  var timerResize = null;
  var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var timerAutocompleteLangOpen = null;
  var source;

  // Init autocomplele search
  var initAutocompleteAirport = function() {
    $('.overlay-loading').removeClass('hidden');
    searchEl.each(function(index, el) {
      var field = $(el).find('input'), sourceUrl, dataJson;
      sourceUrl = field.data('autocomplete-source');
      if(sourceUrl) {
        $.get(sourceUrl, function(data) {
          dataJson = data;
          if(dataJson) {
            var searchAutoComplete = field.autocomplete({
              minLength: 0,
              open: function() {
                var self = $(this);
                self.autocomplete('widget').hide();
                clearTimeout(timerAutocompleteLangOpen);
                timerAutocompleteLangOpen = setTimeout(function(){
                  self.autocomplete('widget').show().css({
                    'left': self.parent().offset().left,
                    'top': self.parent().offset().top + self.parent().outerHeight(true)
                  });
                  self.autocomplete('widget')
                    .jScrollPane({
                      scrollPagePercent: 10
                    }).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
                      e.preventDefault();
                      e.stopPropagation();
                    });
                }, 100);
              },
              source: function(request, response) {
                term = request.term;
                var regex = $.ui.autocomplete.escapeRegex(term);
                var matcher = new RegExp( "\\b" + regex, "i" );
                if(!dataJson) {
                  $('.overlay-loading').removeClass('hidden');
                  var airportData = dataJson.carHotelSearchVo.airportDetailsVo,
                      match1;

                  if(airportData.length) {
                    match1 = $.grep(airportData, function(airport) {
                      var city = airport.cityName,
                          country = airport.countryName;

                      return (matcher.test(city) || matcher.test(country));
                    });
                  }
                  $('.overlay-loading').addClass('hidden');
                  response(match1);
                } else {
                  var airportData = dataJson.carHotelSearchVo.airportDetailsVo,
                      match1;

                  if(airportData.length) {
                    match1 = $.grep(airportData, function(airport) {
                      var city = airport.cityName,
                          country = airport.countryName;

                      return (matcher.test(city) || matcher.test(country));
                    });
                  }

                  response(match1);
                }
              },
              search: function() {
                var self = $(this);
                self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
              },
              response: function(event, ui){
                if(!ui.content.length) {
                 ui.content.push({
                  city: L10n.globalSearch.noMatches,
                  label: L10n.globalSearch.noMatches,
                  value: null
                 });
                }
               },
              close: function() {
                var self = $(this);
                self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
              },
              select: function(evt, ui) {
                var menu = $(this).data("uiAutocomplete").menu.element;
                var itemLi = menu.find("li:has(a.ui-state-focus)");
                ui.item.value = $.trim(itemLi.text());
                var inputs = $(this).closest('form').find(':input');
                inputs.eq( inputs.index(this)+ 1 ).focus();
              }
            }).data('ui-autocomplete');
            searchAutoComplete._renderItem = function(ul, item) {
              if(item.value === null) {
                return $('<li class="autocomplete-item" data-value="'+item.value+'"><a href="javascript:void(0);">' + item.label
                     + '</a></li>').appendTo(ul);
              }else{
                var textNoAirportName = (item.cityName ? item.cityName : '')+', '+(item.countryName ? item.countryName : '');
                var textHasAirportName = (item.airportName ? item.airportName : '')+', '+(item.cityName ? item.cityName : '')+', '+(item.countryName ? item.countryName : '');
                if (!ul.hasClass('auto-suggest')) {
                  ul.addClass('auto-suggest');
                }
                if(field.is('[data-hotel-json]') === true){
                  return $('<li class="autocomplete-item" data-value="'+textNoAirportName+'"><a href="javascript:void(0);"><span class="ico-building-2"></span>'+textNoAirportName+'</a></li>')
                    .appendTo(ul);
                }
                if(field.is('[data-cars-json]') === true){
                  if(item.airportName !==''){
                    return $('<li class="autocomplete-item" data-value="'+textHasAirportName+'"><a href="javascript:void(0);"><span class="ico-airplane"></span>'+ textHasAirportName+'</a></li><li class="autocomplete-item" data-value="'+textNoAirportName+'"><a href="javascript:void(0);"><span class="ico-building-2"></span>'+textNoAirportName+'</a></li>').appendTo(ul);
                  }else{
                    return $('<li class="autocomplete-item" data-value="'+textNoAirportName+'"><a href="javascript:void(0);"><span class="ico-building-2"></span>'+ textNoAirportName+'</a></li>').appendTo(ul);
                  }
              }
              }
            };
            $('.overlay-loading').addClass('hidden');
            searchAutoComplete._resizeMenu = function () {
              this.menu.element.outerWidth(field.parent().outerWidth(true));
            };
            searchAutoComplete._move = function( direction ) {
              var item, previousItem,
                last = false,
                li = $(),
                minus = null,
                api = this.menu.element.data('jsp');

              if (!api) {
                if (!this.menu.element.is(':visible')) {
                  this.search(null, event);
                  return;
                }
                if (this.menu.isFirstItem() && /^previous/.test(direction) ||
                    this.menu.isLastItem() && /^next/.test(direction) ) {
                  this._value( this.term );
                  this.menu.blur();
                  return;
                }
                this.menu[direction](event);
              }
              else {
                var currentPosition = api.getContentPositionY();
                switch(direction){
                  case 'next':
                    if(this.element.val() === ''){
                      api.scrollToY(0);
                      li = this.menu.element.find('li:first');
                      item = li.addClass('active').data( 'ui-autocomplete-item' );
                    }
                    else{
                      previousItem = this.menu.element.find('li.active').removeClass('active');
                      li = previousItem.next();
                      item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
                    }
                    if(!item){
                      last = true;
                      li = this.menu.element.find('li').removeClass('active').first();
                      item = li.addClass('active').data( 'ui-autocomplete-item' );
                    }
                    this.term = li.text();
                    this.element.val(this.term);
                    if(last){
                      api.scrollToY(0);
                      last = false;
                    }
                    else{
                      currentPosition = api.getContentPositionY();
                      minus = li.position().top + li.innerHeight();
                      if(minus - this.menu.element.height() > currentPosition){
                        api.scrollToY(Math.max(0, minus - this.menu.element.height()));
                      }
                    }
                    break;
                  case 'previous':
                    if(this.element.val() === ''){
                      last = true;
                      li = this.menu.element.find('li:last');
                      item = this.menu.element.find(li).addClass('active').data( 'ui-autocomplete-item' );
                    }
                    else{
                      previousItem = this.menu.element.find('li.active').removeClass('active');
                      li = previousItem.prev();
                      item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
                    }
                    if(!item){
                      last = true;
                      li = this.menu.element.find('li:last');
                      item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
                    }
                    this.term = li.text();
                    this.element.val(this.term);
                    if(last){
                      api.scrollToY(this.menu.element.find('.jspPane').height());
                      last = false;
                    }
                    else{
                      currentPosition = api.getContentPositionY();
                      if(li.position().top <= currentPosition){
                        api.scrollToY(li.position().top);
                      }
                    }
                    break;
                }
              }
            };

            field.autocomplete('widget').addClass('autocomplete-menu');

            field.off('blur.autocomplete');
            field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll').on('scroll.preventScroll mousewheel.preventScroll', function(e){
              e.stopPropagation();
            });

            field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
              e.stopPropagation();
            });

            field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
             if(e.which === 13){
               e.preventDefault();
               $($('li.ui-menu-item.active').data('autocomplete')).trigger('click');
               $('ul.ui-autocomplete').css('display', 'none');
             }else if(e.keyCode === 9){
              $($('li.ui-menu-item.active').data('autocomplete')).trigger('click');
               $('ul.ui-autocomplete').css('display', 'none');
             }
            });

            field.off('blur.highlight').on('blur.highlight', function(){
              setTimeout(function(){
                field.closest('.custom-select').removeClass('focus');
                field.autocomplete('close');
              }, 200);
            });
          }
        });
      }

    });
  };

  win.off('resize.blurSearch').on('resize.blurSearch', function() {
    clearTimeout(timerResize);
    timerResize = setTimeout(function() {
      var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if (winW !== width) {
        winW = width;
        var currentEl = searchEl.not(':hidden');
        if (currentEl.length) {
          var currentMenu = currentEl.autocomplete('widget');
          if (currentMenu.length && currentMenu.is(':visible')) {
            currentEl.autocomplete('close');
          }
        }
      }
    }, 50);
  });

  // $('[data-autocomplete-airport] input').each(function(){
  //   $(this).off('click.autocomplete').on('click.autocomplete', function() {
  //     if ($(this).is(':focus')) {
  //       // $(this).autocomplete({ minLength: 0 });
  //       $(this).autocomplete('search', '');
  //     }
  //   });
  // });

  var initModule = function() {
    initAutocompleteAirport();
  };

  initModule();
};
