/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.coachBeta = function() {
	var body = $('body');
	var coachs = $('.coach-popup');
	var closeBtn = $('[data-close-coach]');
	var nextBtn = $('[data-proceed]');
	var overlay = $('.overlay-beta');
	// var overlayInner = overlay.find('.overlay-beta__inner');
	var freezeFlag = false;
	var sticky = $();
	var toggleBtn = $();
	var minibarTpl = '<em class="arrow-up"></em>BETA';
	var nonMinibarTpl = '<em class="arrow-right"></em>BETA.Singaporeair.com';

	var showFirstCoach = function() {
		showCoach($('.coach-1'));
	};

	var freezeBody = function(){
		body.addClass('no-flow');
		freezeFlag = true;
	};

	var deFreeze = function(){
		if(freezeFlag){
			body.removeClass('no-flow');
			freezeFlag = false;
		}
	};

	// var handleScroll = function(){
	// 	var sticky = $('.sticky');
	// 	var h = overlayInner.height();
	// 	overlay.css('overflow-y', 'auto');
	// 	overlayInner.height(h + sticky.height());
	// };

	var showOverlay = function() {
		var isFreeze = body.hasClass('no-flow');
		if(!isFreeze){
			freezeBody();
		}
		overlay.removeClass('hidden');
		setTimeout(function() {
			overlay.addClass('active');
			toggleBtn.html(nonMinibarTpl);
			sticky
				.trigger('mouseenter.additionBeta')
				.data('prevent-mouseevent', true);
		}, 0);
		// handleScroll();
	};

	var hideOverlay = function() {
		var currentCoach = getCurrentCoach();
		currentCoach.removeClass('active');
		toggleBtn.html(minibarTpl);
		sticky.removeData('prevent-mouseevent').trigger('mouseleave.additionBeta');
		overlay.removeClass('active');
		deFreeze();
		setTimeout(function() {
			currentCoach.addClass('hidden');
			overlay.addClass('hidden');
		}, 700);
	};

	var getCurrentCoach = function() {
		return coachs.filter('.active');
	};

	var showCoach = function(coach) {
		var currentCoach = getCurrentCoach();
		currentCoach.addClass('hidden').removeClass('active');
		coach.removeClass('hidden');
		setTimeout(function() {
			coach.addClass('active');
		}, 0);
		setTimeout(function(){
			coach.addClass('active');
		}, 0);
	};

	body.on('click.coachBeta', '.what-new-btn', function(e) {
		e.preventDefault();
		if (!(sticky.length && toggleBtn.length)) {
			sticky = $('.sticky');
			toggleBtn = sticky.find('.mini-beta-link');
		}
		if($(this).hasClass('disabled')){
			return;
		}
		$(this).addClass('disabled');
		showOverlay();
		showFirstCoach();
	});

	closeBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		hideOverlay();
		$('.what-new-btn').removeClass('disabled');
	});

	nextBtn.on('click.coachBeta', function(e) {
		e.preventDefault();
		var btn = $(this);
		var nextCoach = $(btn.data('next-btn'));
		showCoach(nextCoach);
	});
};
