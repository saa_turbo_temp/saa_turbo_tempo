/**
 * @name SIA
 * @description Define global ORB Booking Summary functions
 * @version 1.0
 */
SIA.MBBookingSummary = function() {
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var flightSearch = $('.flights__searchs');
	var flightUpgrades = $('.flights-upgrade');
	var bookingSummaryWidget = $('.booking-summary');
	var bookingSummaryHeading = bookingSummaryWidget.find('.booking-summary__heading');
	var bookingSummaryControl = null;
	var bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');
	var bookingSummaryOffset = bookingSummaryWidget.offset();
	var tooltipPopup = $('.add-ons-booking-tooltip');
	var passengerCount = bookingSummaryWidget.find('.number-passengers');
	var bspTexts = L10n.bookingSummary.texts;

	var monthsListAbbr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
	var monthsList = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var daysListAbbr = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
	var daysList = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

	var l10n = {
		flightInfo: 'You are now on a fare table. To navigate through the fare selection, press tab. To quickly move to the other available flights, use the up or down arrow keys. To select a fare, press enter. To go directly to the Booking Summary Panel, press control Y. ',
		// tableFirstRow += Please press tab to choose a deal
		tableFirstRow: 'Flight SQ 2. Departing from Singapore at Changi Terminal 3 on Saturday 14 February 2016 at 18:30. Arriving in San Francisco at San Francisco International Terminal on Sunday 15 February 2016 at 22:55. Total flight duration: 17 hours and 25 minutes. Flight selected includes 1 transit in Hong Kong at Hong Kong International Terminal 1 on Saturday 14 February 2016 at 22:15. Transit duration: 1 hour and 25 minutes.',
		bspCollapse: 'You are now at the Booking Summary Panel. The total amount of your fare is: {totalcost} Singapore Dollars and Total to be refunded is: {totalrefund} for {passengercount}.',
		bspCollapseWithRefund: 'You are now at the Booking Summary Panel. The total amount of your fare is: {totalcost} Singapore Dollars and {totalmiles} for {passengercount}. The total to be refunded is: {totalrefund}.',
		bspViewBreakdown: 'To view the full cost breakdown of your flight, press enter. ',
		bspNextButton: 'To proceed to the next page, press tab. ',
		tooltip: 'Conditions for the Sweet Deals 2 To Go fare: you may change your booking at a cost of 50 US Dollars, you may earn KrisFlyer miles at 50% of the usual accrual rate, you may not refund or cancel; - you may not upgrade your flight using KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines. For KrisFlyer terms and conditions, press tab. To close this tooltip and return to the fare table, press escape.',
		faresTableCaption1: 'Outbound fares',
		faresTableCaption2: 'Inbound fares',
		tabNavigationHelp: 'Please use the tab key to move through this application',
		bookingSummaryShortcut: 'Please press CONTROL + Y to enable Booking Summary shortcuts',
		informationIcon: 'View More Information',
		landmarkMain: 'Select flights',
		landmarkLanguage: 'Language',
		selectFare: 'To select this fare press enter.'
	};

	var wcagConfig = {
		bsp: {
			wrapper: $('.booking-summary'),
			target: $('.booking-summary').find('[data-tabindex]'),
			heading: {
				el: $('.booking-summary .booking-summary__heading'),
				copy: '2 Adults, 1 Infant. Sgd 1,750.00. Total fare including taxes and surcharges. ', // TODO l10n.prop
				instruction: 'This is the instruction for expanding the BSP' // TODO l10n.prop
			},
			focus: {
				header: false,
				tooltip: false,
				buttons: false,
				ctaGroup: false
			},
			expanded: false,
			tabIndex: 300,
			total: 0,
			namespace: 'bsp-wcag-',
			ariaDescribe: 'aria-describedby',
			active: false
		},
		tableCaptions: {
			fareTable1: {
				el: $('.flights__table').eq(0),
				label: l10n.faresTableCaption1,
				id: '1'
			},
			fareTable2: {
				el: $('.flights__table').eq(1),
				label: l10n.faresTableCaption2,
				id: '2'
			}
		},
		tableSummaries: {
			fareTable1: {
				el: $('.flights__table').eq(0),
				instruction: l10n.flightInfo,
				id: '1'
			},
			fareTable2: {
				el: $('.flights__table').eq(1),
				instruction: l10n.flightInfo,
				id: '2'
			}
		},
		toolsList: '.tools-list',
		landmarks: {
			language: {
				role: 'complementary',
				el: $('.toolbar--language'),
				label: l10n.landmarkLanguage,
				instruction: false,
				wrapEl: false,
				tabindex: false
			},
			navigation: {
				role: 'navigation',
				el: $('#container > .menu'),
				label: false,
				instruction: false,
				wrapEl: false,
				tabindex: false
			},
			search: {
				role: 'search',
				el: $('#container .menu-bar .search').parent(),
				label: false,
				instruction: false,
				wrapEl: false,
				tabindex: false
			},
			main: {
				role: 'main',
				el: false,
				label: false,
				instruction: false,
				wrapEl: $('.main-inner'),
				tabindex: 0
			},
			flights: {
				role: 'application',
				el: $('.main-inner'),
				label: l10n.landmarkMain,
				instruction: l10n.tabNavigationHelp,
				wrapEl: false,
				tabindex: false
			}
		},
		summaryFare: $('.summary-fare'),
		focus: {
			tooltip: false,
			bookingSummary: false,
			tableFlightSearch: false,
			customRadio: false,
			summaryfare: false,
			sort: false
		}
	};

	// This function uses remove format Number
	var unformatNumber = function(number) {
		number = window.accounting.unformat(number);
		return parseFloat(number);
	};

	// This function uses format Number
	var formatNumber = function(number, fraction) {
		return window.accounting.formatNumber(number, (typeof(fraction) !== 'undefined') ?
			fraction : 2, ',', '.');
	};

	var bookingObject = {
		template: {
			heading:
				'<a href="#" class="booking-summary__control" data-tabindex="true">' +
					'{0}' +
					'<em class="ico-point-d"></em>' +
				'</a>' +
				'<div class="booking-summary__info">' +
					'<p class="number-passengers">{1}</p>' +
					'<span class="total-title">{2}</span>' +
					'<span data-headtotal="true" class="total-cost" data-totaltobepaid="true">' +
						'{3}{4}' +
					'</span>' +
					'<span class="total-title">{5}</span>' +
					'<span data-headtotal="true" class="total-cost" data-totaltoberefunded="true">' +
						'{6}{7}' +
					'</span>' +
				'</div>',

			grandTotal:
				'<span class="total-title" aria-hidden="true">{0}</span>' +
				'<span data-headtotal="true"  aria-hidden="true" class="total-info" data-totaltobepaid="true">' +
					'{1}{6}{2}' +
				'</span>' +
				'<span class="total-title" aria-hidden="true">{3}</span>' +
				'<span data-headtotal="true"  aria-hidden="true" class="total-info" data-totaltoberefunded="true">' +
					'{4}{7}{5}{9}' +
				'</span>' +
				'{8}',

			convertedTpl: '<span class="payment-currency-text{0}">{1}</span>',

			cancelPaid:
				'<div class="grand-total" data-tabindex="true">' +
					'<span class="total-title" aria-hidden="true">{0}</span>' +
					'<span data-headtotal="true" class="total-info"  aria-hidden="true" data-totaltobepaid="true">' +
						'{1}{3}{2}' +
					'</span>' +
				'</div>',

			cancelRefund:
				'<div class="grand-total" data-tabindex="true">' +
					'<span class="total-title" aria-hidden="true">{0}</span>' +
					'<span data-headtotal="true" class="total-info"  aria-hidden="true" data-totaltoberefunded="true">' +
						'{1}{3}{2}{4}' +
					'</span>' +
				'</div>',

			cancelFee:
				'<ul class="flights-cost__details">' +
					'{0}{1}' +
				'</ul>'
		}
	};

	// Generate Booking sumary Notes
	var generateBSPNotes = function(bsinfo) {
		var miles = bspTexts.miles;
		var html = '';

		if ((bsinfo.expiredMilesNonRefund && parseFloat(bsinfo.expiredMilesNonRefund) > 0) || (bsinfo.totalMilesExpired && parseFloat(bsinfo.totalMilesExpired) > 0)) {
			html += '<ul class="miles-note">';

			if (bsinfo.expiredMilesNonRefund) {
				var exMileNonRefund = parseFloat(bsinfo.expiredMilesNonRefund);
				html += '<li><span class="list-miles__order">(i)</span><p class="text-miles">' + bspTexts.remainExpiredMile + '</p>';
				html += '<span data-krisflyer-remain-expired-miles="' + exMileNonRefund + '">';
				html += (formatNumber(exMileNonRefund, 0) + ' ' + miles);
				html += '</span></span></li>';
			}

			if (bsinfo.totalMilesExpired) {
				var totalMilesExpired = parseFloat(bsinfo.totalMilesExpired);
				html += '<li>';
				html += formatNumber(totalMilesExpired, 0) + ' ' + bspTexts.usedToKrisFlyer;
				html += '</li>';
			}

			html += '</ul>';
		}

		return html;
	};

	// Generate heading Booking sumary
	var headingBSP = function() {
		if (globalJson.bookingSummary) {
			bookingSummaryHeading.children().not('.loading--medium-2').remove();
			var oruJson = globalJson.bookingSummary.oruUpgradeJsonVO;
			var bsinfo = globalJson.bookingSummary.bookingSummary;
			var miles = bspTexts.miles;
			var html = '';

			if (bsinfo.adultCount) {
				html += bsinfo.adultCount + (bsinfo.adultCount > 1 ?
					L10n.bookingSummary.paxInfor.adults : L10n.bookingSummary.paxInfor.adult);
			}

			if (bsinfo.childCount) {
				html += (html.length ? ', ' : '') + bsinfo.childCount + (bsinfo.childCount > 1 ?
					L10n.bookingSummary.paxInfor.children : L10n.bookingSummary.paxInfor.child);
			}

			if (bsinfo.infantCount && bsinfo.scenario !== 'ORC') {
				html += (html.length ? ', ' : '') + bsinfo.infantCount + (bsinfo.infantCount > 1 ?
					L10n.bookingSummary.paxInfor.infants : L10n.bookingSummary.paxInfor.infant);
			}

			var textsPaid = '',
				totalToBePaidMiles = '',
				totalToBePaidCash = '',
				textsRefund = '',
				milesToBeRefunded = '',
				cashToBeRefunded = '';

			// ATC
			if (bsinfo.scenario === 'ATC') {
				// textsPaid = ((bsinfo.totalToBePaidCash && parseFloat(bsinfo.totalToBePaidCash) > 0) || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = ''; // (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (cashToBeRefunded ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');
			}

			// Cancel
			if (bsinfo.scenario === 'Cancel') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + miles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (cashToBeRefunded ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');
			}

			// ORC
			if (bsinfo.scenario === 'ORC') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');
				milesToBeRefunded = '';
			}

			// ORU
			if (bsinfo.scenario === 'ORU') {
				textsPaid = bspTexts.paid;

				totalToBePaidCash = (typeof oruJson.topUpCashForORU !== 'undefined' ? '<span>' + oruJson.currencyCode + ' ' + formatNumber(oruJson.topUpCashForORU, 2) + '</span>' : '');

				totalToBePaidMiles = '<span>' + (oruJson.topUpCashForORU && parseFloat(oruJson.topUpCashForORU) > 0 ? '<small>+</small>&nbsp;' : '') + formatNumber(oruJson.totMilesForORU, 0) + ' ' + bspTexts.krisFlyerMiles + '</span>';

				textsRefund = '';
				cashToBeRefunded = '';
				milesToBeRefunded = '';
			}

			bookingSummaryHeading
				.prepend(bookingObject.template.heading.format(
					bspTexts.titleBS,
					html,
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded
				));
		}
	};

	// Set booking sumary flight infomation
	var setBookingSummaryFlightInfo = function() {
		var oruJson = globalJson.bookingSummary.oruUpgradeJsonVO;
		var bsinfo = globalJson.bookingSummary.bookingSummary;
		var flightsInfo = bookingSummaryWidget.find('[data-flight-info]');
		var flightsInfoHtml = '';
		var currency = bsinfo.currency ? bsinfo.currency : '';
		var miles = bspTexts.miles;

		flightsInfo.empty();

		// start render common baggage

		var commonBags = bsinfo.commonBaggages;
		if (commonBags) {
			var commonBaggageContent = bookingSummaryWidget.find('[data-common-baggage]');
			var contentHtml = '';
			var grandTotalHtml = '';
			var commonBagsHtml = '';

			if (commonBags && commonBags.baggageList && commonBags.baggageList.length) {
				for (var i = 0; i < commonBags.baggageList.length; i++) {
					var bag = commonBags.baggageList[i];
					contentHtml += '<li class="addon--item">' +
						'<span>' + bag.type + '</span>' +
						'<span class="price">' + formatNumber(bag.amount, 2) + '</span>' +
						'</li>';
				}
			}

			if (typeof bsinfo.totalToBePaidCash !== 'undefined') {
				grandTotalHtml += '<div class="grand-total" data-tabindex="true">' +
				'<span class="total-title" aria-hidden="true">TOTAL TO BE PAID</span>' +
				'<span data-headtotal="true"  aria-hidden="true" class="total-info" data-totaltobepaid="true">' +
				'<span class="unit" aria-hidden="true">' + commonBags.cost + ' ' + formatNumber(commonBags.total, 2) + '</span>' +
				'</span>' +
				'</div>';
			}

			commonBagsHtml = '<div class="flights-cost" data-tabindex="true">' +
				'<h4 class="flights-cost-title">' +
				'<span class="text-left">' + bspTexts.cost + '</span>' +
				'<span class="text-right">' + commonBags.cost + '</span>' +
				'</h4>' +
				'<ul class="flights-cost__details" data-addons="true">' +
				contentHtml +
				'<li class="sub-total sub-total-2">' +
				'<span>' + bspTexts.subTotal + '</span>' +
				'<span class="price">' + commonBags.cost + ' ' + formatNumber(commonBags.total, 2) + '</span>' +
				'</li>' +
				'</ul>' +
				'</div>' +
				grandTotalHtml;

			bookingSummaryWidget.find('.booking-group:lt(2)').remove();
			bookingSummaryWidget.find('.booking-group.booking-group-addon').siblings().remove();
			commonBaggageContent.empty().append(commonBagsHtml);

			return;
		}
		else {
			bookingSummaryWidget.find('.booking-group:eq(2)').remove();
		}

		// end render common baggage

		if (bsinfo.scenario === 'Cancel') {
			flightsInfo.siblings('.booking-heading').find('h3')
				.text(bspTexts.itinerary);
		}

		// new Itinerary
		for (var i = 0; i < bsinfo.flight.length; i++) {

			var departureDate = bsinfo.flight[i].flightSegments[0].deparure.date;
			/** Aria describe for flights info START **/
			var flightsAriaHeading = (i + 1);

			for( var k = 0; k < monthsListAbbr.length; k++ ){
				if( departureDate.indexOf(monthsListAbbr[k]) >= 0 ){
					departureDate = departureDate.replace(monthsListAbbr[k], monthsList[k]);
				}
			}

			for( var d = 0; d < daysListAbbr.length; d++ ){
				if( departureDate.indexOf(daysListAbbr[d]) >= 0 ){
					departureDate = departureDate.replace(daysListAbbr[d], daysList[d]);
				}
			}

			var flightsAriaDate = departureDate + ' - ' + bsinfo.flight[i].flightSegments[0].deparure.time;
			var flightsAriaFrom = bsinfo.flight[i].flightSegments[0].deparure.cityName;
			var flightsAriaTo = bsinfo.flight[i].flightSegments[bsinfo.flight[i].flightSegments.length - 1].arrival.cityName;
			var flightsAria = 'Flight ' + flightsAriaHeading + '. ' + flightsAriaDate + '. ' + flightsAriaFrom +' to '+ flightsAriaTo;
			/** Aria describe for flights info ENDS **/

			flightsInfoHtml += '<div class="flights-info">';
			flightsInfoHtml += '	<div class="flights-info-heading" data-tabindex="true" data-aria-text="'+flightsAria+'">';
			flightsInfoHtml += '		<h4 aria-hidden="true">Flight ' + (i + 1) + '</h4>';
			flightsInfoHtml += '		<span aria-hidden="true">' + bsinfo.flight[i].flightSegments[0].deparure.date + ' - ' + bsinfo.flight[i].flightSegments[0].deparure.time + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '	<div class="flights-info__country" aria-hidden="true">';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.airportCode + '</span>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[bsinfo.flight[i].flightSegments.length - 1].arrival.airportCode + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '</div>';
		}

		flightsInfo.html(flightsInfoHtml);

		// Flights
		// old itinerary
		var bookingContentFlight =
			bookingSummaryWidget.find('.booking-group:eq(1) .booking-group__content');
		var itineraryHtml = '';
		bookingContentFlight.empty();

		// Old Itinerary
		if (bsinfo.oldItinerary) {
			itineraryHtml += '<div class="flights-cost" data-tabindex="true">' +
				'<h4 class="flights-cost-title">' +
				'<span class="text-left">' +
				bspTexts.oldItinerary +
				'</span>' +
				'<span class="text-right">&nbsp;</span>' +
				'</h4>';

			itineraryHtml += '<ul class="flights-cost__details">';

			// ATC
			if (bsinfo.scenario === 'ATC') {
				var subAndFareTotal = '';
				var oldCurrency = bsinfo.oldItinerary.currency || '';

				if (bsinfo.oldItinerary.fareTotal &&
					parseFloat(bsinfo.oldItinerary.fareTotal) > 0) {

					subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);
				}
				else if ((bsinfo.oldItinerary.costPaidByCash &&
					parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0) ||
					(bsinfo.oldItinerary.costPaidByMiles &&
					parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0)) {

					subAndFareTotal = (bsinfo.oldItinerary.costPaidByCash ? currency + ' ' + formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) : '') + (bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ? ' + ' + '<span>' + formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) + miles + '</span>' : '');

					// subAndFareTotal = (bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ?
					// 		formatNumber(bsinfo.oldItinerary.costPaidByMiles, 2) +
					// 			bspTexts.miles +
					// 			(bsinfo.oldItinerary.costPaidByCash ? ' + ' : '') : '') +
					// 	(bsinfo.oldItinerary.costPaidByCash &&
					// 		parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
					// 			'<span>' + currency + ' ' +
					// 			formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) + '</span>' : '');
				}

				if (subAndFareTotal) {
					itineraryHtml += '<li data-old-fare="true" class="sub-total first">' +
						'<span>' +
						bspTexts.fareOld +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}
			}
			// Cancel
			else if (bsinfo.scenario === 'Cancel') {
				var subAndFareTotal = '';
				var taxPaid = '';
				var oldCurrency = bsinfo.oldItinerary.currency || '';

				if (bsinfo.oldItinerary.fareTotal &&
					parseFloat(bsinfo.oldItinerary.fareTotal) > 0) {

					subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareRefundable +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.taxTotal &&
					parseFloat(bsinfo.oldItinerary.taxTotal) > 0) {

					taxPaid = oldCurrency + ' ' + formatNumber(bsinfo.oldItinerary.taxTotal, 2);

					itineraryHtml += '<li data-old-tax-paid="true">' +
						'<span>' +
						bspTexts.taxSurchargeRefundable +
						'</span>' +
						'<span class="values">' +
						taxPaid +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash || bsinfo.oldItinerary.costPaidByMiles) {
					var paidCash = bsinfo.oldItinerary.costPaidByCash;
					var paidMiles = bsinfo.oldItinerary.costPaidByMiles;

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						((paidCash ? oldCurrency + ' ' + formatNumber(paidCash, 2) : '') + (paidMiles ? ' + ' : '')) +
						(paidMiles ? '<span>' + formatNumber(paidMiles, 0) + ' ' + miles + '</span>' : '') +
						'</span>' +
						'</li>';
				}

				if ((bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0) ||
					(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0)) {
					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ?
							formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) +
							miles +
							(bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
								' + ' : '') : '') +
						(bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
						'<span class="unit">' + oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
						'</span>' : '') +
						'</span>' +
						'</li>';
				}
			}
			// ORU
			else if (bsinfo.scenario === 'ORU') {
				var oldCurrency = bsinfo.oldItinerary.currency || '';
				if (bsinfo.oldItinerary.fareTotal) {
					var subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareOld +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash) {
					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
						'</span>' +
						'</li>';
				}
			}
			// ORC
			else {
				var oldCurrency = bsinfo.oldItinerary.currency ?
					bsinfo.oldItinerary.currency : '';
				var subAndFareTotal = '';
				var taxesAndSurcharges = '';

				if (bsinfo.oldItinerary.totalMilesForORB &&
					parseFloat(bsinfo.oldItinerary.totalMilesForORB) > 0) {

					subAndFareTotal = formatNumber(bsinfo.oldItinerary.totalMilesForORB, 2) +
						miles;

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareOldWithout +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.grandTotal &&
					parseFloat(bsinfo.oldItinerary.grandTotal) > 0) {

					taxesAndSurcharges = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.grandTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.taxesAndSurcharges +
						'</span>' +
						'<span class="values">' +
						taxesAndSurcharges +
						'</span>' +
						'</li>';
				}

				if (subAndFareTotal || taxesAndSurcharges) {
					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						(subAndFareTotal ?
							'<span class="values">' + subAndFareTotal +
							(taxesAndSurcharges ? ' + ' : '') : '') +
						(taxesAndSurcharges ? '<span>' + taxesAndSurcharges + '</span>' : '') +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						(subAndFareTotal ?
							'<span class="values">' + subAndFareTotal +
							(taxesAndSurcharges ? ' + ' : '') : '') +
						(taxesAndSurcharges ? '<span>' + taxesAndSurcharges + '</span>' : '') +
						'</span>' +
						'</li>';
				}
			}

			itineraryHtml += '</ul>';
		}

		// New Itinerary
		// ATC
		if (bsinfo.scenario === 'ATC') {
			if (bsinfo.fareTotal || bsinfo.taxTotal || bsinfo.surchargeTotal ||
				bsinfo.rebookingFee || bsinfo.grandTotal) {

				itineraryHtml += '<h4 class="flights-cost-title">' +
					'<span class="text-left">' +
					bspTexts.newItinerary +
					'</span>' +
					'<span class="text-right">&nbsp;</span>' +
					'</h4>';

				itineraryHtml += '<ul class="flights-cost__details">';

				if (bsinfo.fareTotal) {
					itineraryHtml += '<li data-new-fare="true">' +
						'<span>' +
						bspTexts.fareNew +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.fareTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.taxTotal) {
					itineraryHtml += '<li data-new-taxes="true">' +
						'<span>' +
						bspTexts.taxes +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.taxTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.surchargeTotal) {
					itineraryHtml += '<li data-new-surcharge="true">' +
						'<span>' +
						bspTexts.surcharges +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.surchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.rebookingFee) {
					itineraryHtml += '<li data-new-rebooking="true">' +
						'<span>' +
						bspTexts.rebooking +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.rebookingFee, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.grandTotal) {
					itineraryHtml += '<li data-new-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.grandTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash || bsinfo.oldItinerary.costPaidByMiles) {
					var totalPaid = (bsinfo.oldItinerary.costPaidByCash ?
						(bsinfo.oldItinerary.currency ? bsinfo.oldItinerary.currency + ' ' : '') + formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) : '') +
						(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ? ' + ' + '<span>' + formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) + ' ' +	miles + '</span>' : '');

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyNewPaid +
						'</span>' +
						'<span class="values">' +
						totalPaid +
						// '<span>' + (bsinfo.currency ? bsinfo.currency + ' ' : '') +
						// formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>'
						// '</span>' +
						'</li>';
				}

				itineraryHtml += '</ul></div>';
			}
		}
		// Cancel: New Itinerary is not needed in cancel review page booking summary
		else if (bsinfo.scenario === 'Cancel') {
			// if (bsinfo.fareTotal || bsinfo.taxTotal || bsinfo.surchargeTotal ||
			// 	bsinfo.rebookingFee || bsinfo.grandTotal) {

			// 	itineraryHtml += '<h4 class="flights-cost-title">' +
			// 		'<span class="text-left">' +
			// 		bspTexts.itinerary +
			// 		'</span>' +
			// 		'<span class="text-right">&nbsp;</span>' +
			// 		'</h4>';

			// 	itineraryHtml += '<ul class="flights-cost__details">';

			// 	if (bsinfo.fareTotal) {
			// 		itineraryHtml += '<li data-new-fare="true">' +
			// 			'<span>' +
			// 			bspTexts.fareNew +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.fareTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.taxTotal) {
			// 		itineraryHtml += '<li data-new-taxes="true">' +
			// 			'<span>' +
			// 			bspTexts.taxes +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.taxTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.surchargeTotal) {
			// 		itineraryHtml += '<li data-new-surcharge="true">' +
			// 			'<span>' +
			// 			bspTexts.surcharges +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.surchargeTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.rebookingFee) {
			// 		itineraryHtml += '<li data-new-rebooking="true">' +
			// 			'<span>' +
			// 			bspTexts.rebooking +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.rebookingFee, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.grandTotal) {
			// 		itineraryHtml += '<li data-new-total="true" class="sub-total">' +
			// 			'<span>' +
			// 			bspTexts.total +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.grandTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.oldItinerary && bsinfo.oldItinerary.costPaidByCash ||
			// 		bsinfo.oldItinerary.costPaidByMiles) {
			// 		itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
			// 			'<span>' +
			// 			bspTexts.previouslyNewPaid +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			(bsinfo.oldItinerary.costPaidByMiles ?
			// 			formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) +
			// 				bspTexts.miles +
			// 				(bsinfo.oldItinerary.costPaidByCash ? ' + ' : '') : '') +
			// 			(bsinfo.oldItinerary.costPaidByCash ?
			// 				'<span class="unit">' +
			// 				bsinfo.oldItinerary.currency + ' ' +
			// 				formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
			// 				'</span>' : '') +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	itineraryHtml += '</ul></div>';
			// }
		}
		// ORC
		else if (bsinfo.scenario === 'ORC') {
			if (bsinfo.fareTotalInMiles || bsinfo.taxTotal || bsinfo.surchargeTotal ||
				bsinfo.rebookingFee || bsinfo.grandTotal || bsinfo.orbQSurchargeTotal ||
				bsinfo.totalMilesForORB) {

				itineraryHtml += '<h4 class="flights-cost-title">' +
					'<span class="text-left">' +
					bspTexts.newItinerary +
					'</span>' +
					'<span class="text-right">&nbsp;</span>' +
					'</h4>';

				itineraryHtml += '<ul class="flights-cost__details">';

				if (bsinfo.fareTotalInMiles) {
					itineraryHtml += '<li data-new-fare="true">' +
						'<span>' +
						bspTexts.fareNew + '</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.fareTotalInMiles, 0) + ' ' + miles +
						'</span>' +
						'</li>';
				}

				if (bsinfo.taxTotal) {
					itineraryHtml += '<li data-new-taxes="true">' +
						'<span>' +
						bspTexts.taxes +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.taxTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.surchargeTotal) {
					itineraryHtml += '<li data-new-surcharge="true">' +
						'<span>' +
						bspTexts.surcharges +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.surchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.orbQSurchargeTotal) {
					itineraryHtml += '<li data-new-orbQSurchargeTotal="true">' +
						'<span>' +
						bspTexts.qSurchargeTotal +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.orbQSurchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				var rebookFee = '';
				var grandTotal = '';
				var totalMiles = '';

				if (bsinfo.rebookingFee) {
					rebookFee = currency + ' ' + formatNumber(bsinfo.rebookingFee, 2);

					itineraryHtml += '<li data-new-rebooking="true">' +
						'<span>' +
						bspTexts.rebooking + '</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.rebookingFee, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.grandTotal) {
					grandTotal = currency + ' ' + formatNumber(bsinfo.grandTotal, 2);
				}

				if (bsinfo.totalMilesForORB) {
					totalMiles = formatNumber(bsinfo.totalMilesForORB, 2) +
						miles;
				}

				itineraryHtml += '<li data-new-total="true" class="sub-total">' +
					'<span>' +
					bspTexts.total +
					'</span>' +
					'<span class="values">' +
					totalMiles +
					(totalMiles && (grandTotal || rebookFee) ? ' + ' : '') +
					'<span>' +
					(grandTotal ? grandTotal + (rebookFee ? ' + ' : '') : '') +
					'</span>' +
					(rebookFee ? '<span>' + rebookFee + '</span>' : '') +
					'</span>' +
					'</li>';

				if (bsinfo.oldItinerary &&
					(bsinfo.oldItinerary.grandTotal || bsinfo.oldItinerary.fareTotalInMiles)) {

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyNewPaid +
						'</span>' +
						'<span class="values">' +
						(bsinfo.oldItinerary.fareTotalInMiles ?
						formatNumber(bsinfo.oldItinerary.fareTotalInMiles, 0) +
						miles : '') +
						(bsinfo.oldItinerary.grandTotal ?
						(bsinfo.oldItinerary.fareTotalInMiles ? ' + ' : '') +
						'<span class="unit">' +
						formatNumber(bsinfo.oldItinerary.grandTotal, 2) +
						'</span>' : '') +
						'</span>' +
						'</li>';
				}

				itineraryHtml += '</ul></div>';
			}
		}

		bookingContentFlight.html(itineraryHtml);

		// add on if there is commonAddons attribute then rendering
		if (bsinfo.commonAddons && bsinfo.commonAddons.length) {
			var addTemplate = '';
			addTemplate += '<div class="flights-cost" data-tabindex="true"><h4 class="flights-cost-title"><span class="text-left">' + bspTexts.addon + '</span></h4><ul class="flights-cost__details" data-addons="true">';

			for (var i = 0; i < bsinfo.commonAddons.length; i++) {
				addTemplate += '<li class="addon--item"><span>' + bsinfo.commonAddons[i].type + '</span><span class="price">' + bsinfo.commonAddons[i].amount + '</span></li>';
			}
			addTemplate += '<li class="sub-total"><span>' + bspTexts.subTotal + '</span><span class="price">' + bsinfo.currency + ' ' + bsinfo.addonSubTotal + '</span></li></ul></div>';
			$(addTemplate).appendTo(bookingContentFlight);
		}

		var convertPriceChb = $('.payment-currency').find('[data-toggler] input');
		var isConvertPriceChecked = convertPriceChb.length && convertPriceChb.is(':checked');
		var convertClass = isConvertPriceChecked ? '' : ' hidden';
		var convertTpl = bookingObject.template.convertedTpl;

		var textsPaid = '',
			totalToBePaidMiles = '',
			totalToBePaidCash = '',
			textsRefund = '',
			milesToBeRefunded = '',
			creditCard = '',
			cashToBeRefunded = '',
			milesNote = '',
			textConvert = '',
			krisFlyerAccount = '';

		// Cancel
		if (bsinfo.scenario === 'Cancel') {
			var cancellationText = '',
				remainExpiredMileText = '',
				cancellationFeeCash = '',
				cancellationFeeMiles = '',
				cancelHtml = '',
				remainHtml = '';

			textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ||  (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

			if (textsPaid) {
				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ?
					'<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) +
					'</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ?
					'<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') +
					formatNumber(bsinfo.totalToBePaidMiles, 0) + miles + '</span>' : '');

				textConvert = typeof bsinfo.totalToBePaidCash !== 'undefined' ?
					convertTpl.format(
						convertClass,
						L10n.payment.convertText.format(bsinfo.totalToBePaidCash, 2)) : '';

				$(bookingObject.template.cancelPaid.format(
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textConvert
				)).appendTo(bookingContentFlight);
			}

			cancellationText = (bsinfo.cancellationFeeCash || (bsinfo.cancellationFeeMiles && parseFloat(bsinfo.cancellationFeeMiles) > 0) ? bspTexts.cancellation : '');

			if (cancellationText) {
				cancellationFeeCash = (bsinfo.cancellationFeeCash ? bsinfo.currency + ' ' + formatNumber(bsinfo.cancellationFeeCash, 2) : '');
				cancellationFeeMiles = (bsinfo.cancellationFeeMiles && parseFloat(bsinfo.cancellationFeeMiles) > 0 ? formatNumber(bsinfo.cancellationFeeMiles, 0) + miles : '');

				cancelHtml = '<li data-cancellation-fee="true" class="pre-paid"><span>' +
					cancellationText +
					'</span>' +
					'<span class="values">' +
					(cancellationFeeCash ?
						cancellationFeeCash + (cancellationFeeMiles ? ' + ' : '') : '') +
					(cancellationFeeMiles ? '<span class="unit">' + cancellationFeeMiles +
						'</span>' : '') +
					'</span></li>';
			}

			remainExpiredMileText = bsinfo.oldItinerary.taxTotal ? bspTexts.remainExpiredMile : '';

			if (remainExpiredMileText) {
				remainHtml = '<li data-remain-mile="true" class="pre-paid"><span>' +
					remainExpiredMileText +
					'</span>' +
					'<span class="values">' +
					formatNumber(bsinfo.oldItinerary.taxTotal, 0) + ' ' + miles +
					'</span></li>';
			}

			if (cancellationText || remainExpiredMileText) {
				$(bookingObject.template.cancelFee.format(cancelHtml, remainHtml))
					.appendTo(bookingContentFlight);
			}

			textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

			if (textsRefund) {
				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ?
					'<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				creditCard = bsinfo.creditCardNo ? '<span class="grand-sub-title">' + bspTexts.creditCard.format(bsinfo.creditCardNo.substring(bsinfo.creditCardNo.length - 5)) + '</span>' : '';

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ?
					'<span>' + (!creditCard && cashToBeRefunded ? '<small>+</small>&nbsp;' : '') +
					formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');

				krisFlyerAccount = bsinfo.KFAccount ? '<span class="grand-sub-title">' + bsinfo.KFAccount + '</span>' : '';

				$(bookingObject.template.cancelRefund.format(
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded,
					creditCard,
					krisFlyerAccount
				)).appendTo(bookingContentFlight);
			}
		}
		else {
			// set total to be paid
			var totalToBePaid = $('<div class="grand-total" data-tabindex="true"></div>').appendTo(bookingContentFlight);

			// ATC
			if (bsinfo.scenario === 'ATC') {
				// textsPaid = ((bsinfo.totalToBePaidCash && parseFloat(bsinfo.totalToBePaidCash) > 0) || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = ''; // (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				creditCard = bsinfo.creditCardNo ? '<span class="grand-sub-title">' + bspTexts.creditCard.format(bsinfo.creditCardNo.substring(bsinfo.creditCardNo.length - 5)) + '</span>' : '';

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (!creditCard && typeof cashToBeRefunded !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');

				krisFlyerAccount = bsinfo.KFAccount ? '<span class="grand-sub-title">' + bsinfo.KFAccount + '</span>' : '';

				milesNote = generateBSPNotes(bsinfo);
			}
			// ORC
			if (bsinfo.scenario === 'ORC') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles ? '<span>' + (typeof totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = '';
				creditCard = '';
				milesNote = '';
			}
			// ORU
			if (bsinfo.scenario === 'ORU') {
				textsPaid = bspTexts.paid;
				totalToBePaidCash = (typeof oruJson.topUpCashForORU !== 'undefined' ? '<span class="unit">' + oruJson.currencyCode + ' ' + formatNumber(oruJson.topUpCashForORU, 2) + '</span>' : '');

				totalToBePaidMiles = '<span>' + (typeof totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(oruJson.totMilesForORU, 0) + ' ' + bspTexts.krisFlyerMiles + '</span>';

				textsRefund = '';
				cashToBeRefunded = '';
				milesToBeRefunded = '';
				creditCard = '';
				milesNote = '';
			}

			textConvert = typeof totalToBePaidCash !== 'undefined' ?
				convertTpl.format(
					convertClass,
					L10n.payment.convertText.format(formatNumber(bsinfo.scenario === 'ORU' ?
						oruJson.topUpCashForORU : bsinfo.totalToBePaidCash, 2))
				) : '';

			totalToBePaid.html(
				bookingObject.template.grandTotal.format(
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded,
					textConvert,
					creditCard,
					milesNote,
					krisFlyerAccount
				)
			);
		}

		if (body.hasClass('payments-page') && isConvertPriceChecked) {
			convertPriceChb.trigger('change.exchange');
		}
	};

	// Set state for radio button
	var preselectFlights = function() {
		// flight search
		if (globalJson.bookingSummary && globalJson.bookingSummary.fareAvailablityVO) {
			var flightInfo = globalJson.bookingSummary.fareAvailablityVO;
			if (flightInfo.dafaults) {
				for (var i = flightInfo.dafaults.length - 1; i >= 0; i--) {
					if (flightInfo.dafaults[i] !== null) {
						flightSearch
							.filter('[data-flight="' + (i + 1) + '"]')
							.find('input[value="' + flightInfo.dafaults[i] + '"]')
							.prop('checked', true)
							.trigger('change.select-flight');

						$('[name="selectedFlightIdDetails[' + i + ']"]').val(flightInfo.dafaults[i]);
					}
				}
			}

			var radioEls = flightSearch.find('input:radio');

			//Check for enable flights
			radioEls.each(function() {
				if (!$.isEmptyObject(flightInfo.messages)) {
					var flightId = $(this).val();
					if (typeof(flightInfo.messages[flightId]) === 'undefined') {
						$(this).prop('disabled', true);
						$(this)
							.closest('td.hidden-mb, .package--price')
							.find('[data-tooltip]')
							.addClass('disabled').attr('tabindex', '-1');
						$(this).parents('td').attr('data-package-disabled', 'true');
					} else {
						$(this).prop('disabled', false);
						$(this)
							.closest('td.hidden-mb, .package--price')
							.find('[data-tooltip]')
							.removeClass('disabled');
						$(this).parents('td').attr('data-package-disabled', 'false');
					}
				} else {
					$(this).prop('disabled', false);
					$(this)
						.closest('td.hidden-mb, .package--price')
						.find('[data-tooltip]')
						.removeClass('disabled');
					$(this).parents('td').attr('data-package-disabled', 'false');
				}
			});

			if ($.isEmptyObject(flightInfo.messages)) {
				var firstFare = flightSearch.eq(0).find('input:checked:first');
				var isWaitlisted = firstFare.data('waitlisted');

				flightSearch.not(':first').find(isWaitlisted ? 'input[data-waitlisted="true"]' : 'input[data-waitlisted="false"]').each(function() {
					$(this).prop('disabled', true);
					$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
					$(this).parents('td').attr('data-package-disabled', 'true');
				});
			}

			radioEls.trigger('change.flightTableBorder');
		}
	};

	var payWithKfMilesChange = function() {
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		payWithKfMilesCheckbox.off('change.pay-with-miles').on('change.pay-with-miles', function() {
			setBookingSummaryFlightInfo();
		});
	};

	payWithKfMilesChange();

	var fillUpgradeBlocks = function() {
		flightUpgrades.empty().addClass('hidden');
	};

	// Print summary of fare conditions
	var printFareCondition = function(res) {
		if ($('.flight-select-page').length && res.fareFamilyCondition) {
			var fareCondition = $('.summary-fare__conditions');
			var html = '';
			for (var i = 0; i < res.fareFamilyCondition.length; i++) {
				html += '<li>';
				if (res.fareFamilyCondition[i].isAllowed) {
					html += '<em class="ico-check-thick"></em>';
				} else {
					html += '<em class="ico-close"></em>';
				}
				html += res.fareFamilyCondition[i].description;
				html += '</li>';
			}
			fareCondition.html(html);
		}
	};

	// Render popup details
	var renderPopupDetails = function(res) {
		$.get(config.url.orbBookingSummaryDetailsPopupTemplate, function(data) {
			if (!$('.add-ons-page, .payments-page').length) {
				res.bookingSummary.commonAddons = [];
			}

			var template = window._.template(data, {
				data: res,
				confirmationPage: $('.orb-confirmation-page').length
			});

			var popupContent = $('.popup--flights-details .popup__content');
			popupContent.children(':not(.popup__close)').remove();
			popupContent.append(template);
			popupContent.find('[data-need-format]').each(function() {
				var number = unformatNumber($(this).text());
				$(this).text(formatNumber(number, $(this).data('need-format')));
			});

			popupContent
				.find('.flights--detail span')
				.off('click.getFlightInfo')
				.on('click.getFlightInfo', function() {
					var self = $(this);
					var details = self.siblings('.details');
					if (details.is(':empty')) {
						$.ajax({
							url: config.url.orbFlightInfoJSON,
							type: config.ajaxMethod,
							dataType: 'json',
							data: {
								flightNumber: self.parent().data('flight-number'),
								carrierCode: self.parent().data('carrier-code'),
								date: self.parent().data('date'),
								origin: self.parent().data('origin')
							},
							success: function(res) {
								self.children('em').toggleClass('ico-point-d ico-point-u');
								details.toggleClass('hidden');
								var html = '';
								html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
								for (var ft in res.flyingTimes) {
									html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
								}
								details.html(html);
							},
							error: function(jqXHR, textStatus, errorThrown) {
								console.log(jqXHR);
								if (textStatus !== 'abort') {
									window.alert(errorThrown);
								}
							},
							beforeSend: function() {
								self.children('em').addClass('hidden');
								self.children('.loading').removeClass('hidden');
							},
							complete: function() {
								self.children('.loading').addClass('hidden');
								self.children('em').removeClass('hidden');
							}
						});
					} else {
						self.children('em').toggleClass('ico-point-d ico-point-u');
						if (details.is('.hidden')) {
							details.hide().removeClass('hidden').slideDown(400);
						}
						else {
							details.slideUp(400, function() {
								details.addClass('hidden');
							});
						}
					}
				});
		});
	};

	var BSPAjax;

	// Call Ajax for Booking sumary
	var callBSPAjax = function(onchange, extData, callback, radioEl) {
		var data = {};
		$.extend(data, extData);
		if (onchange) {
			flightSearch.each(function(i, it) {
				var selectedFlightId = $(it).find('input:radio:checked').first().val();
				if (selectedFlightId) {
					selectedFlightId = i + '|' + selectedFlightId.substring(2);
					data['selectedFlightIdDetails[' + i + ']'] = selectedFlightId;
				}
			});
		}
		if (BSPAjax) {
			BSPAjax.abort();
		}
		BSPAjax = $.ajax({
			url: onchange ? config.url.mbFlightSelectOnChange : config.url.mbFlightSelect,
			type: config.ajaxMethod,
			data: data,
			dataType: 'json',
			success: function(res) {
				globalJson.bookingSummary = res;

				if ($('.payments-page').length) {
					var triggerCostPayableByCash = new jQuery.Event('change.costPayableByCash');
					var bspInfo = res.bookingSummary;
					var scenario = bspInfo.scenario;

					// triggerCostPayableByCash.cash = res.bookingSummary.costPayableByCash;

					triggerCostPayableByCash.cash = (scenario === 'ORU' ?
						res.oruUpgradeJsonVO.topUpCashForORU : bspInfo.totalToBePaidCash);
					bookingSummaryWidget.trigger(triggerCostPayableByCash);
				}

				headingBSP();
				preselectFlights();

				setBookingSummaryFlightInfo();
				fillUpgradeBlocks();
				printFareCondition(res);
				renderPopupDetails(res);

				if (typeof(callback) === 'function') {
					callback();
				}

				toggleBookingSummary();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				if (textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			},
			beforeSend: function() {
				passengerCount
					.text('...');

				if(body.hasClass('flight-select-page') && !body.hasClass('mb-payments-page') &&
					radioEl && onchange) {
					var bspInfo = bookingSummaryWidget.find('.booking-summary__info');
					bspInfo.find('.total-cost').addClass('hidden');
					bspInfo.find('.fare-notice').addClass('hidden');
					bspInfo.find('.total-title').addClass('hidden');
					bspInfo.siblings('.loading--medium-2').removeClass('hidden');
					flightSearch.find('input:radio').not(radioEl).not(':checked').prop('disabled', true);
					bspInfo.attr('aria-busy', true);
				}
				else {
					SIA.preloader.show();
				}
			},
			complete: function() {
				if (body.hasClass('flight-select-page') && !body.hasClass('mb-payments-page') &&
					onchange) {
					var bspInfo = bookingSummaryWidget.find('.booking-summary__info');
					bspInfo.find('.total-cost').removeClass('hidden');
					bspInfo.find('.fare-notice').removeClass('hidden');
					bspInfo.find('.total-title').removeClass('hidden');
					bspInfo.siblings('.loading--medium-2').addClass('hidden');
					bspInfo.attr('aria-busy', false);
					$('#booking-summary__info-copy').html('');
					$('#booking-summary__info-copy').delay(300).html( bspInfo.text().replace('Booking summary, ', ''));
				}
				else {
					SIA.preloader.hide();
				}
				setupTabIndex();
				updateBookingSummaryInfo(true, true);
			}
		});
	};

	callBSPAjax(false, {}, function() {
		$('[data-flight]').each(function() {
			$(this).find('input:radio:checked').eq(0).trigger('change.select-flight');
		});
	});

	var paymentKFMiles = function() {
		bookingSummaryWidget.off('change.KfMiles').on('change.KfMiles', function(e) {
			callBSPAjax(true, {
				selectedMiles: e.miles
			}, e.callback);
		});
	};

	paymentKFMiles();

	// Fill data to table flight search
	var fillData = function() {
		flightSearch
			.off('change.fillData')
			.on('change.fillData', 'input[type="radio"]', function(e) {
				if (!bookingSummaryWidget.length) {
					return;
				}
				var radio = $(this);
				var tableIndex = flightSearch.index($(this).closest('.flights__searchs'));
				$('[name="selectedFlightIdDetails[' + tableIndex + ']"]').val($(this).val());

				if (!flightSearch.data('remove-wailist')) {
					callBSPAjax(true, {
						tripType: $(this).val()[0]
					}, function() {
						if (e.originalEvent) {
							toogleTooltip(tooltipPopup, L10n.bookingSummary.fare, radio.next('label').find('strong.package--price-number').text());
						}
					}, radio);
				}
			});

		flightUpgrades
			.off('change.fillData')
			.on('change.fillData', 'input[type="checkbox"]', function(e) {
				var checkbox = $(this);
				var isChecked = $(this).is(':checked');

				setBookingSummaryFlightInfo();
				if (e.originalEvent && isChecked) {
					toogleTooltip(tooltipPopup, checkbox.data('upgrade-title'), checkbox.data('upgrade-price'));
				}
			});
	};

	fillData();

	// Scroll popup
	var popupScroll = function() {
		var trigger = bookingSummaryWidget.find('[data-popup-anchor]');
		trigger.off('click.setAnchor').on('click.setAnchor', function() {
			var anchor = $(this).data('popup-anchor');
			var popup = $($(this).data('trigger-popup'));
			popup.data('anchor', anchor);
		});
		trigger.each(function() {
			var popup = $($(this).data('trigger-popup'));
			if (!popup.data('boundScroll')) {
				popup.data('boundScroll', true);
				popup.off('afterShow.scrollToAnchor').on('afterShow.scrollToAnchor', function() {
					var pop = $(this);
					window.setTimeout(function() {
						var paddingTop = parseInt(pop.find('.popup__content').css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						if (anchorElement.length) {
							popup.scrollTop(anchorElement.position().top - paddingTop);
						}
					}, 1);
				});
			}
		});
	};

	popupScroll();

	// Set position for sticky
	var stickyWidget = function() {
		var setStickyPosition = function() {
			if ($('.passenger-details-page, .seatsmap-page, .orb-confirmation-page').length || !bookingSummaryOffset) {
				return;
			}
			var fixedClassName = 'booking-summary--fixed';
			var footer = $('footer.footer');
			if ($(window).scrollTop() >= bookingSummaryOffset.top && Modernizr.mq('(min-width: ' + SIA.global.config.tablet + 'px)')) {
				var right = $(window).width() - $('.main-inner').width();
				right = right / 2;
				bookingSummaryWidget.addClass(fixedClassName).css('right', right);

				//vars
				var winHeight = $(window).height(),
					winTop = $(window).scrollTop(),
					bmHeight = bookingSummaryWidget.height(),
					footTop = footer.offset().top;
				if (bmHeight <= winHeight) {
					//if widget's height is smaller than window's height
					if (winTop + bmHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});
					} else {
						bookingSummaryWidget.css({
							top: '',
							bottom: ''
						});
					}
				} else {
					//widget's height is larger than window's height
					if (winTop + winHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: winTop + winHeight - footTop
						});
					} else {
						bookingSummaryWidget.css({
							top: '',
							bottom: ''
						});
					}
				}
			} else {
				bookingSummaryWidget.removeClass(fixedClassName).css({
					right: '',
					top: '',
					bottom: ''
				});
			}
		};

		var originalWidth = $(window).width();
		$(window).on('resize.sticky-booking-summary', function() {
			if ($(this).width() !== originalWidth) {
				setStickyPosition();
			}
		}).on('scroll.sticky-booking-summary', function() {
			setStickyPosition();
		}).trigger('scroll.sticky-booking-summary');

		return {
			setStickyPosition: setStickyPosition
		};
	};

	var sticky = stickyWidget();

	// Toggle Booking sumary
	var toggleBookingSummary = function() {
		bookingSummaryControl = bookingSummaryHeading.find('.booking-summary__control');
		bookingSummaryControl.off('click.openBS').on('click.openBS', function(e) {
			e.preventDefault();

			var state = $(this).attr('aria-expanded') === 'false' ? true : false;
			$(this).attr('aria-expanded', state);

			var controlledId = $(this).attr('aria-controls');

			setTimeout( function() {
				$('#' + controlledId).find('.booking-group:first-child .booking-heading h3').focus();
			}, 50);

			if ($('.orb-flight-select-page').length) {
				if ($('[data-flight] input[type=radio]:checked').length === 0) {
					return;
				}
			}

			if (window.innerWidth < SIA.global.config.tablet) {
				var popupSeatSelect = $('[data-infomations-1]'),
					popupSeatChange = $('[data-infomations-2]');
				if (popupSeatSelect.length) {
					popupSeatSelect.find('.tooltip__close').trigger('click');
				}
				if (popupSeatChange.length) {
					popupSeatChange.find('.tooltip__close').trigger('click');
				}
			}

			bookingSummaryWidget.toggleClass('active');
			bookingSummaryContent.toggle(0, function() {
				if (bookingSummaryWidget.hasClass('active') &&
					bookingSummaryWidget.closest('body').hasClass('add-ons-page')) {
					bookingSummaryWidget.closest('.main-inner').css('min-height', bookingSummaryWidget.outerHeight(true) + 'px');
				} else {
					bookingSummaryWidget.closest('.main-inner').css('min-height', '');
				}
				//$(window).trigger('scroll.sticky-booking-summary');
			});
			sticky.setStickyPosition();
		});

		if ($('.payments-page').length) {
			bookingSummaryControl.trigger('click.openBS');
		}
	};

	// Toggle tooltip
	var toogleTooltip = function(tooltipElement, upperText, priceAdded) {
		if (!tooltipElement.hasClass('active') && !bookingSummaryWidget.hasClass('active')) {
			if (!bookingSummaryWidget.length) {
				return;
			}
			var overwriteTxtTooltip = tooltipElement.find('.tooltip__content');
			var position = bookingSummaryWidget.offset(),
				scrollTop = $(window).scrollTop(),
				posTop = Math.max(position.top - scrollTop, 0),
				/*posRight = window.innerWidth - position.left*/
				posRight = (document.body.clientWidth || $(window).width()) - position.left + 15;

			overwriteTxtTooltip.html(
				upperText +
				'<span class="text-1">+' + formatNumber(priceAdded) + ' miles</span>'
			);
			if (window.innerWidth < SIA.global.config.tablet) {
				var posBottomBooking = position.top + bookingSummaryWidget.height();
				if (scrollTop > posBottomBooking) {
					posTop = 15;
				} else {
					posTop = posBottomBooking - scrollTop + 15;
				}
				posRight = position.left;
			}

			tooltipElement.addClass('active').stop().css({
				position: 'fixed',
				top: posTop,
				right: posRight
			}).fadeIn(400).delay(2000).fadeOut(400, function() {
				$(this).removeClass('active');
			});
		}

		$(window).on('scroll.sticky-booking-summary', function() {
			if (bookingSummaryWidget.hasClass('booking-summary--fixed')) {
				tooltipElement.css('top', '5px');
			} else {
				tooltipElement.css('top', bookingSummaryWidget.offset().top - $(window).scrollTop());
			}

			if (window.innerWidth < SIA.global.config.tablet) {
				var posBottomBooking = bookingSummaryWidget.offset().top + bookingSummaryWidget.height();
				if ($(window).scrollTop() > posBottomBooking) {
					tooltipElement.css('top', '15px');
				} else {
					tooltipElement.css('top', posBottomBooking - $(window).scrollTop() + 15);
				}
			}
		});
	};

	var setupTabIndex = function(){

		/** reset tabindex **/
		wcagConfig.bsp.tabIndex = 300;

		var bspParent = bookingSummaryWidget;
		var selfBookingSummaryHeading = $('.booking-summary__heading', bspParent);
		var selfBookingSummaryInfo = $('.booking-summary__info', bspParent);
		var selfBookingSummaryControl = $('.booking-summary__control', bspParent);
		var selfBookingSummaryContent = $('.booking-summary__content', bspParent);


		selfBookingSummaryHeading.attr({
			'id': 'booking-summary__heading'
		});

		selfBookingSummaryInfo
			.after('<div id="booking-summary__info-copy" />')
			.next().attr({
			'aria-live':'polite',
			'role':'status',
			'tabindex':'10',
			'class': 'says'
		});

		$('.loading').attr({
			'aria-hidden':true
		});

		selfBookingSummaryControl.attr({
			'aria-describedby': 'aria-bsh',
			'data-tabindex': true,
			'role':'button',
			'aria-expanded':'false',
			'aria-controls': 'booking-summary__content'
		});
		selfBookingSummaryContent.attr({
			'id': 'booking-summary__content'
		});

		bookingSummaryWidget.find('[data-tabindex]').each(function(){

			var self = $(this);
			var attr = self.attr('data-aria-text');
			var selfWcagTpl = $('<div class="bsp-wcag says" />');

			/** Start tabindex increment **/
			var tabIndex = wcagConfig.bsp.tabIndex++;

			/** Update the total number of elements that are part of the tabindex flow **/
			wcagConfig.bsp.total = tabIndex;

			/** Tooltip **/
			if( self.attr('data-tooltip') ){
				self.wrap('<span />');
				//Add tabindex attribute
				self.parent().attr({
					'tabindex': tabIndex,
					'aria-label': l10n.informationIcon,
					'aria-describedby' : wcagConfig.bsp.namespace + tabIndex,
					'role' : 'button'
				});

				self.removeAttr('data-tabindex');

				//Add tabindex for target elements
				self.parent().append(selfWcagTpl).find('.bsp-wcag').attr('id', wcagConfig.bsp.namespace + tabIndex);

				self.parent().find('.bsp-wcag').html(self.data('aria-text'));

			}else{

				//Add tabindex attribute
				self.attr('tabindex', tabIndex);

				if(!self.hasClass('booking-summary__control')){

					if( self.hasClass('flights-cost') ){
						//Add aria-describedby
						self.attr(wcagConfig.bsp.ariaDescribe, wcagConfig.bsp.namespace + tabIndex);
						self.append('<div id="'+wcagConfig.bsp.namespace + tabIndex+'" class="says">'+self.text()+'</div>');
					}else{
						//Add aria-describedby
						if(self.parent().hasClass('booking-heading') || self.parent().hasClass('grand-total')){
							self.attr('aria-label', self.text());
							self.find('.bsp-wcag').remove();
						}else{
							if(!self.hasClass('info-charge')){
								self.attr(wcagConfig.bsp.ariaDescribe, wcagConfig.bsp.namespace + tabIndex);
							}
						}
					}
				}
			}

			if (typeof self.attr('data-aria-text') !== typeof undefined && self.attr('data-aria-text') !== false && !self.attr('data-tooltip') ) {

				if(!self.hasClass('flights-cost')){
					//Add aria-describedby
					self.attr(wcagConfig.bsp.ariaDescribe, wcagConfig.bsp.namespace + tabIndex);
				}
				//Add tabindex for target elements
				self.append(selfWcagTpl).find('.bsp-wcag').attr('id', wcagConfig.bsp.namespace + tabIndex);
			}

			//Add wcag text container
			var selfWcagCon = self.find('.bsp-wcag');
			selfWcagCon.addClass('says');

			if(self.hasClass('grand-total')){
				self.append(selfWcagTpl).find('.bsp-wcag').attr('id', wcagConfig.bsp.namespace + tabIndex);
				self.find('.bsp-wcag').html(self.text());
				//self.parent().next().remove();
				//self.parent().find('.link-4').prev('.link-4').remove();

			}
			//Inject the more details tabindex
			injectMoreDetails(self, tabIndex);

			if(typeof attr !== typeof undefined && attr !== false && self.find('.bsp-wcag').length === 0 && !self.hasClass('flights-cost') ){

				/** If false, we will grab the content of the element **/
				if( self.data('aria-text') === 'false' ){
					selfWcagCon.html(self.find('span').text());

				/** if true, we will grabe the aria-text arrtibute value **/
				}else{
					selfWcagCon.html('hello');
				}
			}else{
				selfWcagCon.html(self.data('aria-text'));
			}

			if( !self.hasClass('flights-cost') && !self.hasClass('grand-total') && self.attr('data-tooltip') ){
				self.children(':not(.bsp-wcag)').attr('aria-hidden', 'true');
			}

			$('#btn-next').attr('tabindex', tabIndex);


		});
		// end loop

		if( $('.booking-summary__info').find('.says').length ){
			$('.booking-summary__info').find('.says').remove();
			$('.booking-summary__info').prepend('<span class="says">Booking summary, Selected Fare:</span>');
		}else{
			$('.booking-summary__info').prepend('<span class="says">Booking summary, Selected Fare:</span>');
		}


		if( $('.booking-summary__heading').find('#aria-bsh').length ){
			$('.booking-summary__heading').find('#aria-bsh').remove();
		}

		updateBookingSummaryInfo();

	};

	var updateBookingSummaryInfo = function(breakdown = false, nextbutton = false){

		var bshUpdate = l10n.bspCollapse;

		var breakDown = l10n.bspViewBreakdown;
		var nextButton = l10n.bspNextButton;

		if( !breakdown ){
			breakDown = '';
		}

		if( !nextbutton ){
			nextButton = '';
		}

		bshUpdate = bshUpdate.replace('{totalcost}', $('.booking-summary__info [data-totaltobepaid] span').text());
		bshUpdate = bshUpdate.replace('{totalrefund}', $('.booking-summary__info [data-totaltoberefunded]').text());
		bshUpdate = bshUpdate.replace('{passengercount}', $('.booking-summary__info .number-passengers').text());


		if($('.booking-summary__heading').find('#aria-bsh').length > 0){
			$('#aria-bsh').remove();
		}

		$('.booking-summary__heading').attr({
			'aria-describedby': 'aria-bsh'
		}).append('<span id="aria-bsh" class="says">'+ bshUpdate + breakDown + nextButton + '</span>');
	};

	var injectMoreDetails = function(self, tabIndex){
		/*** Assign tabindex to more details button START ****/
		if( self.parent('.flights-info').is(':last-child') ){

			self.attr('tabindex', tabIndex++);
			self.parents('.booking-group').find('[data-popup-anchor]').wrap('<span class="link-4" tabindex="'+(tabIndex++)+'" aria-label="More Details">');

			// move the link to the end of the group
			// so that it's in the right place in the tab order
			// self.parents('.booking-group').each( function() {
			// 	var group = $(this);
			// 	var _link = group.find('span.link-4');
			// 	group.append(_link);
			// });
			var _link = self.parents('.booking-group').find('span.link-4');
			if(self.parents('.booking-group').next('.link-4').length > 0){
				self.parents('.booking-group').next().remove();
			}else{
				self.parents('.booking-group').append(_link);
				_link.prev('.link-4').remove();
			}


		}else if(self.hasClass('grand-total') && self.is(':last-child') ){
			self.attr('tabindex', tabIndex++);
			self.parents('.booking-group').find('[data-popup-anchor]').wrap('<span class="link-4" tabindex="'+(tabIndex++)+'" aria-label="More Details">');
			// move the link to the end of the group
			// so that it's in the right place in the tab order
			// self.parents('.booking-group').each( function() {
			// 	var group = $(this);
			// 	var _link = group.find('span.link-4');
			// 	group.append(_link);
			// });
			var _link = self.parents('.booking-group').find('span.link-4');
			if(self.parents('.booking-group').next('.link-4').length > 0){
				self.parents('.booking-group').next().remove();
			}else{
				self.parents('.booking-group').append(_link);
				_link.prev('.link-4').remove();
			}

		}
		/*** Assign tabindex to more details button ENDS ****/
	};

	var wcag = function(){

		var flightsTable = $('.flights__table');
		var flow = [
			//'th',
			'[data-flight]',
			'td:not([data-package-disabled="true"]) .custom-radio',
			'td:not([data-package-disabled="true"]) .tooltip-wrapper'
		];
		var skip = [
			'.custom-radio input',
			//'.custom-radio label',
			//'.flights__table--1__inner td.first',
			'.tooltip-wrapper a',
			//'.flights__table--1__inner td.first *',
			//'.flights__table--1__inner td[data-disabled="true"]',
			'#upgrade-checkbox-2',
			'#upgrade-checkbox-1',
			'.control',
			'.control a',
			'.flights-info__country', // following the convention, but it would be better to use aria-label to override text
			'.flights-info-heading > h4',
			'.flights-info-heading > span'
			//'.flights__info--group'
		];

		var init = function(){
			assignLandmarks();
			assignTabIndex();
			skipElements();
			focusTriggers();
			addButtonClass();
			bookingSummaryElements();
			tableFlightSearchElements();
			assignTableCaptions();
			assignTableInstructions();
			customRadioState();
		};

		var addButtonClass = function(){
			var nextButton = $('#btn-next');
			if(!nextButton.hasClass('cta-wcag-btn')){
				$('#btn-next').addClass('cta-wcag-btn');
			}
		};

		var assignAriaLabel = function(){

			//TOOLS List
			$(wcagConfig.toolsList).children().each(function(){
				if($('em', $(this)).hasClass('ico-mail')){
					$('a', $(this)).attr({'aria-label':'Email','tabindex': 0});
				}
				if($('em', $(this)).hasClass('ico-print')){
					$('a', $(this)).attr({'aria-label':'Print','tabindex': 0});
				}
			});

			var heading = $('.blk-heading');
			var flightsTarget = $('.flights__target');

			heading.each(function(){
				$('h2.main-heading', $(this)).attr({ 'tabindex':0, 'aria-label': $(this).find('.main-heading').text() });
			});

			flightsTarget.each(function(){
				var childs = ['h3 span', 'h3 span + a', 'h3 a:last-child'];

				for(var i = 0; i < childs.length; i++){
					if($(childs[i]).is('span')){
						$('h3', $(this)).attr({ 'tabindex':0, 'aria-label': $(childs[i], $(this)).text() });
					}else{
						$(childs[i], $(this)).attr({ 'tabindex':0, 'aria-label': $(childs[i], $(this)).text() });
					}
				}

			});

		};

		assignAriaLabel();

		var tableFlightSearchElements = function(){
			flightsTable.each(function(tableIdx){
				var self = $(this);
				var selfThead = $('thead', self);
				var selfTh = $('th', selfThead);
				var selfTr = $('.flights__table--1__inner tr', self);

				self.attr('tabindex', 0);

				self.find('>tbody>tr').removeClass('hidden');
				$('.see-more-btn').addClass('hidden');

				/** Assign tab index to each cell **/
				// selfTr is within the nested table
				selfTr.each(function(r){

					// The nested table
					$(this).parent().parent()
						.prepend('<caption id="table-' + r + '__caption" class="says">' + l10n.tableFirstRow + '</caption>')
						.attr({
							'aria-describedby': 'table-' + r + '__caption',
							'tabindex': 0
						});

					// ALL tds
					$(this).find('td').each(function(i){

						var $this = $(this);

						//Assign tabindex for each 'td'
						//Skip first 'td' in each row
						//Skip disabled td
						if(!$this.hasClass('flights__info--group') && !$this.find('[type="radio"]').attr('disabled') ){

							/*** CUSTOM RADIO START ***/
							$this.find('.custom-radio').each(function(){
								var selfRadio = $(this);
								var selfRadioParent = selfRadio.parent();
								var realRadio = selfRadio.find('input');
								var realRadioName = realRadio.attr('name');
								var realRadioId = realRadio.attr('id');
								var $realRadioLabel = selfRadio.find('label');
								var seatLeft = $('.seat-left', selfRadioParent).text();
								var dealType = selfTh.eq(i).text();

								$realRadioLabel.attr('id', realRadioId + '-label');

								// Generate a unique identifier for each radio
								// radiogroup isn't appropriate, because all radios in the table use the same name
								// 	not just the ones in this row
								// flightnumber also isn't appropriate because some routes involve multiple flights.
								if(seatLeft.length){
									$realRadioLabel.prepend('<span class="says">' + seatLeft + ' </span>');
								}
								$realRadioLabel.prepend('<span class="says">' + dealType + ' </span>');

								// Append 'select fare' instruction
								$realRadioLabel.html( $realRadioLabel.html() + '<span class="says">. ' + l10n.selectFare +'</span>');

								if( selfRadio.find('input[disabled]').length === 0 ){

									selfRadio.attr({
										'aria-labelledby': realRadioId + '-label',
										'role':'radio',
										'aria-checked': 'false',
										'data-radiogroup': realRadioName
									});

									if ( realRadio.prop('checked') ) {
										selfRadio.attr({
											'aria-checked': 'true'
										});
									}
								}else{
									selfRadio.attr('tabindex', '-1');
								}

							});
							/*** CUSTOM RADIO ENDS ***/

							/*** TOOLTIP START ***/
							$this.find('[data-tooltip]').each(function(){
								var self = $(this);

								self.text('');

								if( !self.hasClass('disabled') ){

									self.parent()
										.attr({
											'aria-label': l10n.informationIcon,
											'role':'button',
											'data-aria': 'wcag-tooltip-' + tableIdx + '-' + r + '-' + i
										})
										.append('<span id="wcag-tooltip-' + tableIdx + '-' + r + '-' + i + '" class="says">'+ l10n.tooltip +'</div>');

									// support mouse interaction which matches the visual focus style
									self.parent()
										.on( 'click.linkProxy', function(e) {
											$(this).find('[data-tooltip]').click();
											e.stopPropagation();
										});
								}

								self.focus(function(){
									// wcagConfig.tfs.focus.tooltip = true;
									// wcagConfig.tfs.focus.last = $(this).attr('tabindex');

								});

								self.blur(function(){
									// wcagConfig.tfs.focus.tooltip = false;
								});

							});
							/*** TOOLTIP START ***/

						}

					});

				});

				// captureKeys(self);

			});

			// $('#form-flight-search .summary-fare').each(function(){
			// 	$(this).find('*').focus(function(){
			// 		wcagConfig.focus.summaryfare = true;
			// 	});
			// });

			// $('#form-flight-search .summary-fare').find('a').wrap('<span class="wcag-link" tabindex="0" />');

			$('#form-flight-search .button-group-1').before('<div class="after-summary-fare" tabindex="0" />');

			$('.after-summary-fare').focus(function(){

				bookingSummaryControl.focus();

				wcagConfig.focus.tableFlightSearch = false;
				wcagConfig.focus.bookingSummary = true;
				wcagConfig.focus.summaryfare = false;
			});
		};

		var bookingSummaryElements = function(){

			bookingSummaryWidget.each(function(){


				setupTabIndex();

				$('.cta-wcag-btn').each(function(){
					var self = $(this);
					wcagConfig.bsp.tabIndex++;
					if(self.not(':last-child')){
						self.attr('tabindex', wcagConfig.bsp.tabIndex);
					}else{
						self.attr('tabindex', wcagConfig.bsp.tabIndex++);
					}
				});

			});

		};

		var skipElements = function(){
			for(var s = 0; s < skip.length; s++){
				$(skip[s]).attr({
					'aria-hidden':'true',
					'tabindex': -1
				});
			}
		};

		var assignTabIndex = function(){
			flightsTable.each(function(){
				for(var i = 0; i < flow.length; i++){
					$(this).find(flow[i]).attr({
						'tabindex': 0
					});
				}
			});
		};

		var assignTableCaptions = function() {
			/*
				HTML Table Captions allow screen reader users
				to get a quick overview of data tables in the page
				and quickly jump between tables.

				The caption is read out with the number of rows and columns.

				How to test:
				-----------------
				OS X 10.10.5 + Safari 10.0 + VoiceOver
				1. [CONTROL]+[OPTION]+[u] = open rotor
				2. [left][right] = select tables menu
				3. [up][down] = select table
				4. [ENTER] = jump to selected table
				5. [CONTROL]+[OPTION]+[a] = start reading (contents of table)
				-----------------
				Win 7 + IE 11 + JAWS 17.0.2619:
				1. [t] = jump to next table
				2. [SHIFT]+[t] = jump to previous table
				3. starts reading table automatically
				-----------------
				Win7 + Firefox 49.0.1 + NVDA 2016.2.1:
				1. [t] = jump to next table
				2. [SHIFT]+[t] = jump to previous table
				3. starts reading table automatically
			*/

			if ( ! $('body').hasClass('flight-select-page') ) {
				return;
			}

			$.each( wcagConfig.tableCaptions, function() {
				if ( ! this.label ) {
					return;
				}
				else {
					var oldDescriptionId = this.el.attr('aria-describedby'); // eg caption
					var newDescriptionId = 'table-caption__' + this.id;
					var instruction = this.label;

					if ( oldDescriptionId ) {
						// link multiple descriptions
						this.el.attr('aria-describedby', oldDescriptionId + ' ' + newDescriptionId);

						// tell the screen reader to pause between them
						instruction = ', ' + instruction;
					}
					else {
						// link one description
						this.el.attr('aria-describedby', newDescriptionId);
					}

					// inject one description
					this.el.prepend('<caption id="table-caption__' + this.id + '" class="says">' + instruction + '</caption>');
				}
			});

		};

		var assignTableInstructions = function(){

			if(!$('body').hasClass('flight-select-page')){
				return;
			}

			$.each( wcagConfig.tableSummaries, function(){
				if ( ! this.instruction ) {
					return;
				}
				else {
					// this.el.attr('summary', this.instruction); // NVDA doesn't announce the table summary

					var oldDescriptionId = this.el.attr('aria-describedby'); // eg caption
					var newDescriptionId = 'table-summary__' + this.id;
					var instruction = this.instruction;

					if ( oldDescriptionId ) {
						// link multiple descriptions
						this.el.attr('aria-describedby', oldDescriptionId + ' ' + newDescriptionId);

						// tell the screen reader to pause between them
						instruction = ', ' + instruction;
					}
					else {
						// link one description
						this.el.attr('aria-describedby', newDescriptionId);
					}

					// inject one description
					this.el.before('<p id="table-summary__' + this.id + '" class="says">' + instruction + '</p>');
				}
			});

		};

		var assignLandmarks = function() {
			/*
				ARIA Landmarks (aka Regions) allow screen reader users
				to get a quick overview of the page structure
				and quickly jump between regions.

				The label is read out with the region type.
				The instruction is read out when a user starts reading within the region.

				Note that the application landmark is required to intercept the ENTER key event in IE11+JAWS.
				 See: http://stackoverflow.com/questions/23051610/using-jaws-to-get-key-events
				 Safari does not display the application landmark in Rotor.
				 NVDA does not display the application landmark in the elements list.

				How to test:
				-----------------
				OS X 10.10.5 + Safari 10.0 + VoiceOver
				1. [CONTROL]+[OPTION]+[u] = open rotor
				2. [left][right] = select landmarks menu
				3. [up][down] = select landmark
				4. [ENTER] = jump to selected landmark
				5. [CONTROL]+[OPTION]+[a] = start reading (contents of region)
				-----------------
				Win 7 + IE 11 + JAWS 17.0.2619:
				1. [r] = jump to next landmark
				2. [q] = jump to main landmark
				3. [down] = start reading (contents of region)
				-----------------
				Win7 + Firefox 49.0.1 + NVDA 2016.2.1:
				1. [INSERT]+[F7] = open elements list
				2. [left][right] = select landmarks list
				3. [TAB] = focus landmarks pane
				4. [up][down] = select landmark
				5. [ENTER] = jump to selected landmark
				6. starts reading (contents of region) automatically
				-----------------
				Win7 + Firefox 49.0.1 + NVDA 2016.2.1:
				1. [d] = jump to next landmark
				2. [SHIFT]+[d] = jump to previous landmark
				3. starts reading (contents of region) automatically
			*/

			if ( ! $('body').hasClass('flight-select-page') ) {
				return;
			}

			$.each( wcagConfig.landmarks, function() {

				// every landmark requires a role
				if ( ! this.role ) {
					return;
				}

				// inject a container for a landmark
				if ( ! this.el ) {
					if ( this.wrapEl ) {
						this.wrapEl.wrapAll('<div></div>');
						this.el = this.wrapEl.parent();
					}
					else {
						return;
					}
				}

				// role
				this.el.attr('role', this.role);
				//this.el.attr('tabindex', 0);

				// tabindex
				if ( this.tabindex !== false ) {
					this.el.attr('tabindex', this.tabindex);
				}

				// label
				if ( this.label ) {
					this.el.attr('aria-label', this.label);
				}

				// instruction
				// note:
				// aria-describedby is not used because the region navigation mechanism
				// does not assign the [role] programmatic focus;
				// so in VoiceOver, if we were asking a user to use the Tab key,
				// then this would actually move to the next region.
				if ( this.instruction ) {
					this.el.prepend('<p class="says">' + this.instruction + '</p>');
				}

			});

		};

		var customRadioState = function() {

			// N radio buttons with one name are just one control
			$( '.flights__table table' ).on( 'change', '.custom-radio > input', function() {

				// the radio that was just checked
				var $checked = $(this);

				// everything with the same name toggles to unchecked
				// name requires walking the DOM, so we use a pointer to all related parents
				var radiogroup = $checked.attr('name');
				$('[data-radiogroup="' + radiogroup + '"]').attr( 'aria-checked', false );

				// except the one that was selected
				$checked.parent().attr( 'aria-checked', true );
			});

		};

		ally.when.key({

			// Safari+VO tells users to use this combo to select a radio button
			// although ENTER also works
			'ctrl+alt+space': function(){
				var target = ally.get.activeElement();
				if($(target).hasClass('custom-radio')){
					$(target).find('input').click();
				}
			},

			enter: function(e) {

				try {
					//Get Target Element
					var target = ally.get.activeElement();

					/** IF TOOLTIP **/
					if($(target).hasClass('tooltip-wrapper')){

						$(target).find('a').click();

					}else if($(target).parents('.booking-summary__content').length>0){

						if ( $(target).is('a') ) {
							$(target).click();
						}
						else {
							$(target).find('a').click();
						}

					/** CUSTOM RADIO **/
					}else if($(target).hasClass('custom-radio')){
						$(target).find('input').click();
						e.preventDefault(); // don't submit form in IE11

					}else if($(target).parent().hasClass('custom-select')){

						$(target).click();
						e.preventDefault(); // don't submit form in IE11

					/** BOOKING SUMMARY **/
					}else if(wcagConfig.focus.bookingSummary === true){
						// if the tooltip has just been closed, leave the focus where it is
						// (chrome has focussed the launcher, ie11 is still on the tooltip close button)
						if(!$(target).attr('data-type') && !$(target).hasClass('tooltip__close')){

							$('.booking-summary__control').trigger('press');

							updateBookingSummaryInfo();

						}

					/** See More Button **/
					}else if($(target).hasClass('see-more-btn')){
						var targetTableTd = $(target).prev().find('table > tbody > tr:last-child table td:first-child + td');
						targetTableTd.find('.custom-radio input').click().parent().focus();

					}

				}catch(e){
					//console.log(e);
				}

			},

			// IE11 + JAWS
			space: function(e) {

				//Get Target Element
				var target = ally.get.activeElement();

				/** IF TOOLTIP **/
				if($(target).hasClass('tooltip-wrapper')){

					$(target).find('a').click();

				}else if($(target).parents('.booking-summary__content').length>0){

					$(target).find('a').click();
				}
				/** CUSTOM RADIO **/
				else if($(target).hasClass('custom-radio')){
					$(target).find('input').click();
					e.preventDefault();
				}

			},

			/** UP ARROW **/
			38: function(e){

				//Get Target Element
				var target = ally.get.activeElement();
				var tr;

				if($(target).hasClass('.flights__table--1__inner')){
					tr = $(target).parent().parent();
				}else{
					tr = $(target).closest('tr[data-price]');
				}

				tr.prev()
					.find('.flights__table--1__inner')
						.scrollView(50, 0)
						.focus();

				e.preventDefault();

			},

			/** DOWN ARROW **/
			40: function(e){

				var target = ally.get.activeElement();
				var tr;

				if($(target).hasClass('.flights__table--1__inner')){
					tr = $(target).parent().parent();
				}else{
					tr = $(target).closest('tr[data-price]');
				}

				tr.next()
					.find('.flights__table--1__inner')
						.scrollView(50, 0)
						.focus();

				e.preventDefault();

			},

			'ctrl+y': function() {

				var target = $('.booking-summary__control');
				target.focus();

				wcagConfig.focus.tableFlightSearch = false;
				wcagConfig.focus.bookingSummary = true;

			},

			'tab': function(){

				var target = ally.get.activeElement();

				focusTriggers(target);
				headControls(target, 'true');

				$('.flights__table--1__inner').each(function(){
					$(this).focus(function(){
						$(this).parents('.flights__table').next().find('.wi-icon-previous').click();
					});
				});

			},

			'shift+tab': function(){
				//Get Target Element
				var target = ally.get.activeElement();
				focusTriggers(target);

				headControls(target, 'false');

				if($('*:focus').hasClass('tooltip__content')){
					$('.tooltip__close').click();
					bookingSummaryWidget.find('[data-tooltip]').focus();
				}

				$('.flights__table--1__inner').each(function(){
					$(this).focus(function(){
						$(this).parents('.flights__table').next().find('.wi-icon-next').click();
					});
				});

				//console.log(wcagConfig.focus);

			},
			27: function(){
				$('.tooltip__close').click();
				bookingSummaryWidget.find('[data-tooltip]').parent().focus();
				if($('*:focus').hasClass('select_text') && $('*:focus').parent().hasClass('active') ){
					$('.select_text').click().focus();
				}else if($('*:focus').parents('.custom-scroll').length>0){
					$('.select_text').click().focus();
				}
			}

		});

		// scroll an element into view
		$.fn.scrollView = function(offset, duration){
			return this.each(function(){
				$('html, body').animate({
					scrollTop: $(this).offset().top - offset
				}, duration);
			});
		};

		var headControls = function(target, shift){

			var selector = '.flights__table';
			var innerTable = $(target).parents('.flights__table--1__inner');
			var parent = $('*:focus').parents( selector );
			var totalNumber = innerTable.find('td.hidden-mb:not(.last)').length;
			var numberOfVisible = parseInt(parent.data('column'));
			var currentPackageIdx = parseInt($(target).parent().index());

			if( shift === 'true' ){
				if( currentPackageIdx === numberOfVisible ){
					if($('*:focus').hasClass('tooltip-wrapper')){
						if(parent.hasClass('next-package')){
							$('*:focus').parents('.flights__table').next().find('.wi-icon-next').click();
						}
					}
				}else if(currentPackageIdx > totalNumber){
					$('*:focus').parents('.flights__table').next().find('.wi-icon-previous').click();
				}

			}else{

				if( currentPackageIdx-1 > numberOfVisible ){
					if($('*:focus').hasClass('custom-radio')){
						if(parent.hasClass('next-package')){
							$('*:focus').parents('.flights__table').next().find('.wi-icon-next').click();
						}
					}
				}else if( currentPackageIdx < (numberOfVisible+2) ){
					if($('*:focus').hasClass('custom-radio')){
						if(parent.hasClass('previous-package')){
							$('*:focus').parents('.flights__table').next().find('.wi-icon-previous').click();
						}
					}
				}

			}


			// if(shift === 'true'){
			//
			// 	var idx = $(target).index() + 1;
			//
			//
			// 	/** Trigger next package group **/
			// 	if( idx === parent.data('column') && parent.next().find('.wi-icon-next').is(':visible') ){
			// 		if($('*:focus').hasClass('title-head')){
			// 			$(target).parents('.flights__table').next().find('.wi-icon-next').click();
			// 		}
			//
			// 	}else if( $('*:focus').parent('td').length > 0 ){
			//
			// 		if($(target).parent().index() === parent.data('column')){
			// 			$(target).parents('.flights__table').next().find('.wi-icon-next').click();
			// 		}else if($('*:focus').is(':last-child')){
			// 			$(target).parents('.flights__table').next().find('.wi-icon-previous').click();
			// 		}
			// 	}else if( $('*:focus').hasClass('title-head') ){
			// 		if($('*:focus:last-child').is(':visible')){
			// 			$(target).parents('.flights__table').next().find('.wi-icon-previous').click();
			// 		}
			// 	}
			//
			//
			// }else{
			//
			// 	var idx = $(target).index() - 1;
			// 	var triggerEl = Math.round(parent.find('thead tr').children().size() / parent.data('column'));
			//
			// 	/** Trigger prev package group **/
			// 	if( idx === triggerEl && parent.next().find('.wi-icon-previous').is(':visible') ){
			// 		$(target).parents('.flights__table').next().find('.wi-icon-previous').click();
			// 	}
			// }

		};

		/** Collapse the BSP when the focus is in the next button **/
		$('#btn-next').focus(function(){
			if(bookingSummaryContent.is(':visible')){
				bookingSummaryControl.trigger('click.openBS');
				updateBookingSummaryInfo(true, true);
			}
		});

		$('.logo').focus(function(){
			if(bookingSummaryContent.is(':visible')){
				bookingSummaryControl.trigger('click.openBS');
				updateBookingSummaryInfo(true, true);
			}
		});

		$('#booking-summary *').focus(function(){
			wcagConfig.focus.bookingSummary = true;
		});

		var focusTriggers = function(t){

			/** If focus is currently inside the flight table **/
			if($(t).parents('table').length>0){
				wcagConfig.focus.tableFlightSearch = true;
				wcagConfig.focus.bookingSummary = false;

				if(!$(t).parents('.booking-summary').find('.booking-summary__content').is(':visible')){
					updateBookingSummaryInfo(true, true);
				}

				wcagConfig.focus.tableFlightSearch = true;

			/** If Focus is outside the table but previous is set **/
			}else if($(t).parents('table').length===0){
				if(flightsTable.next().find('.wi-icon-previous').is(':visible')){
					flightsTable.next().find('.wi-icon-previous').click();
				}

			/** If focus is currently inside the bsp **/
			}else if($(t).parents('.booking-summary').length>0){
				wcagConfig.focus.tableFlightSearch = false;
				wcagConfig.focus.bookingSummary = true;

				if($(t).parents('.booking-summary').find('.booking-summary__content').is(':visible')){
					updateBookingSummaryInfo();
				}

			}else{
				wcagConfig.focus.tableFlightSearch = false;
				wcagConfig.focus.bookingSummary = false;
			}

			// if($('*:focus').parents('.summary-fare').next().length > 0){
			// 	wcagConfig.focus.summaryfare = true;
			// }

			// if(wcagConfig.focus.summaryfare){
			// 	$('#booking-summary').attr({
			// 		'tabindex':-1
			// 	}).focus();
			//
			// 	wcagConfig.focus.tableFlightSearch = false;
			// 	wcagConfig.focus.bookingSummary = true;
			// 	wcagConfig.focus.summaryfare = false;
			//
			// }

			/** Close the tooltip and move to the next element **/
			if($('*:focus').hasClass('tooltip__close')){
				$('.tooltip__close').click();
				bookingSummaryWidget.find('[data-tooltip]').parent().focus();
			}

			/** Close the tooltip and move to the next element **/
			if($('*:focus').hasClass('select_text') || $('*:focus').parents('.custom-select').length>0){
				wcagConfig.focus.sort = true;
			}else{
				wcagConfig.focus.sort = false;
			}

		};


		var customSelectDropdown = function(){

			var customSelect = $('[data-customselect]');
			var textCustomSelect = $('.select_text', customSelect);

			textCustomSelect.attr({
				'aria-label': 'Sort Dropdown'
			});

			if(!textCustomSelect.attr('tabindex')){
				textCustomSelect.attr({
					'tabindex': 0
				});
			}

			$('li[data-value]').on( 'keyup', function(e){



				var keyCode = e.keyCode || e.which || e.charCode;

				if( keyCode === 13 ){

					$('*:focus').trigger('click');
					$('*:focus').click();

					$('.custom-select > .select_text').focus();

				}

				e.preventDefault();
			});
		};

		//customSelectDropdown();

		init();

	};

	setTimeout(function(){
		wcag();
	},1000);

};
