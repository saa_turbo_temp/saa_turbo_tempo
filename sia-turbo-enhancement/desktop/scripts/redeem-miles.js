/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */

SIA.KFRedeemMiles = function(){
	var global = SIA.global;
	// var config = global.config;

	var initBlockRedeem = function(){
		var blockRedeem = $('.block--redeem');
		var form = $('.form-general', blockRedeem);
		// var button = $('.button-group input', blockRedeem);
		// var termsConditions = $('.terms-conditions input', blockRedeem);
		var triggerShowKrisInfo = $('#cb-prefill', blockRedeem);
		var isSubmited = false;

		/*termsConditions.off('change.enable').on('change.enable', function(){
			if(termsConditions.is(':checked')){
				button.removeClass('disabled').prop('disabled', false);
			}
			else{
				button.addClass('disabled').prop('disabled', true);
			}
		}).trigger('change.enable');*/

		triggerShowKrisInfo.off('change.checkKrisFlyer').on('change.checkKrisFlyer', function() {
			var formFields = blockRedeem.find('input:not(:button,:submit), select, textarea');

			if($(this).is(':checked')) {
				formFields.each(function() {
					var that = this;
					var el = $(that);
					var parentElement = that.parentNode;
					var dataParent = $(parentElement).data();

					/*if(Object.keys(dataParent).length) {*/
					if(!$.isEmptyObject(dataParent)) {
						for(var objData in dataParent) {
							if(globalJson.krisFlyer[objData]) {
								el.val(globalJson.krisFlyer[objData]);
								if(/^(select-one|select-multiple)$/.test(that.type)) {
									// var parentEl = $(parentElement);
									var parentEl = el.closest('.autocomplete');
									if(parentEl.data('autocomplete')) {
										if(parentEl.find('input').data('uiAutocomplete')){
											parentEl.removeClass('default').find('input').data('uiAutocomplete')._value(
												el.find(':selected').data('text')
											);
											parentEl.find('input').valid();
										}
									} else {
										$(parentElement).customSelect('refresh');
									}
								}
							}
						}
					}
					$(that).valid();
				});
			} else {
				formFields.each(function() {
					var that = this;
					that.value = '';
					if($(that).is(':input:text')){
						setTimeout(function(){
							$(that).blur();
						}, 200);
					}
					if(/^(select-one|select-multiple)$/.test(that.type)) {
						var parentEl = $(that.parentNode);
						if(parentEl.data('autocomplete')) {
							if(parentEl.find('input').data('uiAutocomplete')){
								parentEl.addClass('default').find('input').data('uiAutocomplete')._value('');
								parentEl.find('input').valid();
							}
						} else {
							$(parentEl).customSelect('refresh');
						}
					}
					$(that).valid();
				});
				if (!isSubmited) {
					form.data('validator').resetForm();
				}
			}
		});

		form.validate({
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			invalidHandler: function() {
				isSubmited = true;
			}
		});
	};
	initBlockRedeem();
};
