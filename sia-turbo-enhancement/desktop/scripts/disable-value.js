/**
 * @name SIA
 * @description Define global disableValue functions
 * @version 1.0
 */
SIA.disableValue = function() {
	var disabledValueEls = $('[data-disable-value]');

	var disableValue = function() {
		disabledValueEls.each(function(){
			var wrap = $(this);
			var checkBox = $('input:checkbox', wrap);
			var inputSibling = $('input:text', wrap);
			if(inputSibling.data('ruleRequired') || inputSibling.data('disableCheckbox')) {
				checkBox.each(function(i) {
					var self = $(this);
					var sibl = inputSibling.eq(i);
					self.off('change.disabledInputSibling').on('change.disabledInputSibling', function(){
						if(self.is(':checked')){
							sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
							wrap.find('.text-error').remove();
							wrap.find('.error').removeClass('error');
						}
						else{
							sibl.prop('disabled', false).closest('span').removeClass('disabled');
						}
					});
				});
			}
		});
	};

	var initModule = function() {
		disableValue();
	};

	initModule();
};
