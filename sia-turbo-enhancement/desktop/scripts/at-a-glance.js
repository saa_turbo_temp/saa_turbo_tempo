/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.atAGlance = function(){
	if(!$('.at-a-glance-page').length){
		return;
	}
	var win = $(window);
	var global = SIA.global;
	var config = global.config;
	// var getDrawData = function(jsonUrl,templateUrl,appendToElement){
	// 	var drawContent = function(res,apElement) {
	// 		var appendElement = $(apElement);
	// 		if(appendElement.length){
	// 			$.get(templateUrl, function (data) {
	// 				appendElement.empty();
	// 				var template = window._.template(data, {
	// 					data: res
	// 				});
	// 				$(template).appendTo(appendElement);
	// 				$('[data-accordion-wrapper]').accordion('refresh');
	// 			}, 'html');
	// 		}
	// 	};


	// 	var ajaxSuccess = function(res) {
	// 		drawContent(res,appendToElement);
	// 	};
	// 	$.ajax({
	// 			url: jsonUrl,
	// 			type: global.config.ajaxMethod,
	// 			dataType: 'json',
	// 			success: ajaxSuccess,
	// 			error: function(xhr, status) {
	// 				if(status !== 'abort') {
	// 					window.alert(L10n.flightSelect.errorGettingData);
	// 				}
	// 			}
	// 		});
	// };

	// getDrawData(config.url.kfAtAGlanceJSON ,config.url.kfAtAGlanceTemplate,'[data-accordion-wrapper-content=1]');
	//getDrawData('ajax/your-bookings.json','template/your-bookings.html','.accordion__content:first');
	//getDrawData('ajax/recent-activities.json','template/recent-activities.html','.table-2 tbody');

	var tabContent = $('.tab-content.active');
	var responsiveTable = $('.table-responsive');
	var dialsChartItem = $('.dials-chart__item');
	var twoColsWrapper = $('.two-cols-wrapper');
	var customiseLinkForm = $('.popup--customise-link form');
	var inscreaseHeight = 245;
	var inscreaseWidth = 275;
	var delayChart = 200;
	var totalDurationChart = 600;
	var timeChart = null;
	var windowWidth = 0;
	var windowHeight = 0;
	var customiseLinkChkbs = customiseLinkForm.find('input:checkbox');
	var totalCustome = customiseLinkForm.data('totalCustome');

	// Set min-height for the "Quick links" block
	var balanceHeight = function(wrapper){
		var blkContentWrap = wrapper.find('.block--expiring-miles .blk-content-wrap');
		var blkContent = wrapper.find('.block--quick-links .blk-content');
		blkContentWrap.removeAttr('style');
		blkContent.removeAttr('style');
		var contentWrapHeight = blkContentWrap.height();
		var contentHeight = blkContent.height();
		var maxLength = (contentWrapHeight > contentHeight ? contentWrapHeight : contentHeight);
		blkContent.css('min-height',maxLength);
		blkContentWrap.css('min-height',maxLength);
	};

	win.off('resize.balanceHeight').on('resize.balanceHeight',function(){
		if(win.width() !== windowWidth || win.height() !== windowHeight){
			windowWidth = win.width();
			windowHeight = win.height();
			balanceHeight(twoColsWrapper);
		}
	}).trigger('resize.balanceHeight');

	customiseLinkChkbs.off('change.customiseLinkChkbs').on('change.customiseLinkChkbs',function(){
		var unCheckedChkbxs = customiseLinkForm.find('input:checkbox:not(:checked)');
		if(customiseLinkForm.find('input:checkbox:checked').length >= totalCustome){
			unCheckedChkbxs.prop('disabled', true);
			unCheckedChkbxs.parent('.custom-checkbox').addClass('disabled');
		}
		else {
			unCheckedChkbxs.prop('disabled', false);
			unCheckedChkbxs.parent('.custom-checkbox').removeClass('disabled');
		}
	}).trigger('change.customiseLinkChkbs');

	if(Modernizr.cssanimations){
		$('.dials-tab').tabMenu({
			tab: '> ul.tab .tab-item',
			tabContent: '> div.tab-wrapper > div.tab-content',
			activeClass: 'active',
			beforeChange: function(tab, el) {
				if(el.hasClass('active')) {
					return false;
				}
			},
			afterChange: function(tab) {
				clearTimeout(timeChart);
				var chartItem = tab.filter('.active').find('.dials-chart__item');
				chartItem.chart('reset');
				chartItem.eq(0).chart('refresh');
				timeChart = setTimeout(function(){
					chartItem.eq(1).chart('refresh');
				}, delayChart);
			}
		});

		dialsChartItem.chart({
			startVal: 0,
			endVal: 4000,
			increment: 180 / 100,
			incrementVal: 70
		});

		var chartItem = tabContent.find('.dials-chart__item');
		chartItem.eq(0).chart('show');
		timeChart = setTimeout(function(){
			chartItem.eq(1).chart('show');
		}, delayChart);

	}
	else {
		// Init chart
		var initChart = function(chartitem){
			var dialsChartCircle = chartitem.find('.dials-chart-circle');
			var dialsChartItemDetails = chartitem.find('.dials-chart__item-details');
			var circleFill = chartitem.find('.circle__fill:not(.circle--fix)');
			var circleFix = chartitem.find('.circle__fill.circle--fix');
			var kfPointElement = dialsChartItemDetails.find('[data-kf-points]');
			var iconDials = kfPointElement.siblings('.ico-dials');
			var chartOld = dialsChartCircle.data('chart-old');
			var colorCircle = chartitem.data('color');

			circleFill.css('background-color',colorCircle);
			circleFix.css('background-color',colorCircle);
			iconDials.addClass('ico-dials--'+ chartOld);
		};

		// Reset chart
		var resetChart = function(chartitem){
			var dialsChartCircle = chartitem.find('.dials-chart-circle');
			var chartOld = dialsChartCircle.data('chart-old');
			var itemDesc = chartitem.find('.item-desc__info__heading');
			var xDimension = 0;
			if(chartOld){
				xDimension = (chartOld - 1) * inscreaseWidth;
			}
			dialsChartCircle.css('background-position', '-' + xDimension + 'px -0px');
			itemDesc.text(accounting.formatNumber(0));
		};

		// Show chart
		var showChart = function(chartitem){
			var dialsChartCircle = chartitem.find('.dials-chart-circle');
			var dialsChartItemDetails = chartitem.find('.dials-chart__item-details');
			var itemDesc = chartitem.find('.item-desc__info__heading');

			var kfPointElement = dialsChartItemDetails.find('[data-kf-points]');
			var kfPoints = kfPointElement.data('kfPoints');
			var kfRequired = dialsChartItemDetails.find('[data-kf-required]').data('kfRequired');
			var chartOld = dialsChartCircle.data('chart-old');

			var limitTimes = Math.round((kfPoints * 10) / (kfPoints + kfRequired));
			var duration = totalDurationChart / limitTimes;
			var increasePointEachTime = kfPoints / limitTimes;
			var yDimension = 0;
			var xDimension = 0;
			var count = 0;
			if(chartOld){
				xDimension = (chartOld - 1) * inscreaseWidth;
			}
			var simulateInterval = function(){
				setTimeout(function(){
					if(count >= limitTimes){
						return;
					}
					count++;
					itemDesc.text(accounting.formatNumber(increasePointEachTime*count));
					dialsChartCircle.css('background-position', '-' + xDimension + 'px -'+ yDimension + 'px');
					yDimension += inscreaseHeight;
					simulateInterval();
				}, duration);
			};
			simulateInterval();
			itemDesc.text(accounting.formatNumber(kfPoints));
		};

		dialsChartItem.each(function(){
			initChart($(this));
		});

		var chartItems = $('.tab-content').filter('.active').find('.dials-chart__item');
		showChart(chartItems.eq(0));
		resetChart(chartItems.eq(1));
		clearTimeout(timeChart);

		timeChart = setTimeout(function(){
			showChart(chartItems.eq(1));
		}, delayChart);

		$('.dials-tab').tabMenu({
			tab: '> ul.tab .tab-item',
			tabContent: '> div.tab-wrapper > div.tab-content',
			activeClass: 'active',
			beforeChange: function(tab, el) {
				if(el.hasClass('active')) {
					return false;
				}
			},
			afterChange: function(tab) {
				var chartItems = tab.filter('.active').find('.dials-chart__item');
				showChart(chartItems.eq(0));
				resetChart(chartItems.eq(1));
				clearTimeout(timeChart);
				timeChart = setTimeout(function(){
					showChart(chartItems.eq(1));
				}, delayChart);
			}
		});
	}

	// This function uses the sort order for the table.
	var sortOrderTable = function(resTable){
		var trs = resTable.find('tbody tr');
		var tbody = resTable.find('tbody');
		var elems = $.makeArray(trs);
		var ascending = false;
		var sortTrigger = resTable.find('thead th:first a');
		var evenOrder = function(table){
			table.find('tbody tr:odd').removeClass().addClass('even');
			table.find('tbody tr:even').removeClass().addClass('old');
		};
		sortTrigger.off('click.sortTrigger').on('click.sortTrigger',function(){
			var self = $(this);
			// var emElem = $(this).find('em');
			ascending = !ascending;
			if(ascending){
				// emElem.removeClass().addClass('ico-point-d');
				self.removeClass().addClass('active');
			}
			else{
				self.removeClass();
				// emElem.removeClass().addClass('ico-point-u');
			}
			sortDate(elems,tbody);
			evenOrder(resTable);
		});
		var sortDate = function(elems,tbody){
			var els = elems.sort(function(tr1, tr2) {
				var date1 = new Date($.trim($(tr1).find('td:first span').text()));
				var date2 = new Date($.trim($(tr2).find('td:first span').text()));
				if (ascending) {
					return ((date1 > date2) ? 1 : -1 );
				} else {
					return ((date1 > date2) ? -1 : 1 );
				}
				return 0;
			});
			tbody.html(els);
		};
	};

	sortOrderTable(responsiveTable);

	// This function uses render for the Your bookings block.
	var renderYourBookings = function(){
		var dataAccordionWrapperContent = $('[data-accordion-wrapper-content="1"]');
		if (dataAccordionWrapperContent.length > 1) {
			dataAccordionWrapperContent = $(dataAccordionWrapperContent[1]);
		}
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.children('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfUpcomingFlights[idx];
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			dataAccordionWrapperContent.children('[data-accordion]').remove();
			$.get(config.url.kfAtAGlanceTemplate, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfUpcomingFlights
				});
				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};

	//this function used render for the Booking to be confirm block.
	var renderYourConfirmBookings = function(){
		var dataAccordionWrapperContent = $('.block--confirm-booking [data-accordion-wrapper-content="1"]');
		var getBookingItemInfo = function() {
			var bookingBlock = dataAccordionWrapperContent.find('[data-accordion]');
			$.get(config.url.kfAtAGlanceTemplate01, function (html) {
				$.get(config.url.kfAtAGlanceTemplateButton, function (html1) {
					bookingBlock.each(function(idx) {
						var bookingItem = $(this);
						var acContent = bookingItem.find('[data-accordion-content="1"]');
						$.ajax({
							url: bookingItem.data('url'),
							type: global.config.ajaxMethod,
							data: {},
							dataType: 'json',
							success: function(res) {
								if(res){
									res.flightInfor = globalJson.kfBookingConfirm[idx];
									res.bookingConfirmUrl = dataAccordionWrapperContent.data('payment-url');
									var template = window._.template(html, {
										data: res
									});
									var accordionInner = bookingItem.find('.accordion__control-inner');
									var templateBtn = window._.template(html1, {
										data: res
									});
									accordionInner.find('.loading').addClass('hidden');
									$(templateBtn).appendTo(accordionInner);
									$(template).appendTo(acContent);
									bookingItem.find('.ico-point-d').removeClass('hidden');
									if(bookingBlock.length-1 === idx){
										SIA.accordion.initAccordion();
									}
								}
							},
							error: function() {
								window.alert(L10n.upcomingFlights.errorGettingData);
							}
						});
					});
				});
			});
		};
		var loadGlobalJson = function() {
			$.get(config.url.kfAtAGlanceTemplateSf, function(html) {
				var template = window._.template(html, {
					data: globalJson.kfBookingConfirm
				});

				dataAccordionWrapperContent.children().remove();

				$(template).appendTo(dataAccordionWrapperContent);
				getBookingItemInfo();

				$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
			});
		};

		loadGlobalJson();
	};
	renderYourBookings();
	renderYourConfirmBookings();

	var renderInfoBox = function(){
		var infoBox = $('.info-box');

		infoBox.each(function(){
			var self = $(this);
			var infoBoxButton = $('.info__button', self);

			infoBoxButton.off('click').on('click', function(){
				self.remove();
			});
		});
	};

	renderInfoBox();

};
