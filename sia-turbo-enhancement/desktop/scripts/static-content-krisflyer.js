SIA.staticContentKrisflyer = function(){
	var global = SIA.global,
			win = global.vars.win;
			// config = global.config;
			// totalTabletSlide = 3,
			// timerDetectSlider;

	// banner slider
	var bannerSlider = $('#banner-slider');
	var imgBannerLength = bannerSlider.find('img.img-main').length - 1;
	var loadBackgroundBanner = function(self, idx){
		// self.closest('.slide-item').css({
		// 	'background-image': 'url(' + self.attr('src') + ')',
		// 	'background-position': self.closest('.slide-item').data('desktop')
		// });

		// if(global.vars.detectDevice.isTablet()){
		// 	self.closest('.slide-item').css({
		// 		'background-position': self.closest('.slide-item').data('tablet-bg')
		// 	});
		// }

		// self.attr('src', config.imgSrc.transparent);

		if(idx === imgBannerLength){
			bannerSlider.find('.loading').hide();
			bannerSlider.css('visibility', 'visible');
			bannerSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					draggable: true,
					infinite: true,
					arrows: false,
					speed: 500,
					fade: true,
					slide: 'div',
					cssEase: 'linear',
					// start wcag opts
					accessibility: true,
					assistiveTechnology: true,
					customPaging: function(slider, i) {
						var index = (i+1);
						return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.krisFlyer.customPagingTextPre + index + L10n.sliders.krisFlyer.customPagingTextPost);
					},
					pauseOnFocus: true,
					focusOnSelect: true,
					pauseOnHover: true,
					// end wcag opts
				});
		}
	};

	bannerSlider.find('img.img-main').each(function(idx) {
		var self = $(this);
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundBanner(self, idx);
		};
		nI.src = self.attr('src');
	});

	// Highlight slider
	var highlightSlider = $('#highlight-slider');
	var wrapperHLS = highlightSlider.parent();
	var imgHighlightLength = highlightSlider.find('img').length - 1;

	var loadBackgroundHighlight = function(self, parentSelt, idx){
		// if(global.vars.detectDevice.isTablet()){
		// 	parentSelt.css({
		// 		'background-image': 'url(' + self.attr('src') + ')'
		// 	});
		// 	self.attr('src', config.imgSrc.transparent);
		// }

		if(idx === imgHighlightLength){
			highlightSlider.width(wrapperHLS.width() + 22);
			highlightSlider.css('visibility', 'visible');
			highlightSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: 4,
					slidesToScroll: 4,
					useCSS: global.vars.isNewIE() ? false : true,
					// start wcag opts
					accessibility: true,
					assistiveTechnology: true,
					customPaging: function(slider, i) {
						var index = (i+1);
						return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.highlights.customPagingTextPre + index + L10n.sliders.highlights.customPagingTextPost);
					},
					pauseOnFocus: true,
					focusOnSelect: true,
					pauseOnHover: true,
					// end wcag opts
					responsive: [
						{
							breakpoint: 988,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3
							}
						}
					]
				});

			win.off('resize.highlightSlider').on('resize.highlightSlider',function() {
				highlightSlider.width(wrapperHLS.width() + 22);
			}).trigger('resize.highlightSlider');
		}
	};

	highlightSlider.find('img').each(function(idx) {
		var self = $(this);
		var parentSelt = self.parent();
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundHighlight(self, parentSelt, idx);
		};
		nI.src = self.attr('src');
	});
};
