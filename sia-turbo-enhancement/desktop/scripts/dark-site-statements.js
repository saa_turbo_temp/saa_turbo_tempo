/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteStatements = function() {
	var player;

	var initYoutubePlayer = function() {
		var thumbnails = $('[data-statement-url]');
		var url = thumbnails.eq(0).data('statement-url');

		thumbnails.each(function() {
			var thumbnail = $(this);
			thumbnail.off('click.playYoutube').on('click.playYoutube', function(e) {
				e.preventDefault();
				url = thumbnail.data('statement-url');
				if (!player) {
					window.onYouTubeIframeAPIReady();
				} else {
					player.loadVideoById(url);
				}
			});
		});

		window.onYouTubeIframeAPIReady = function() {
			var playerEl = $('#player');
			player = new window.YT.Player('player', {
				height: playerEl.height(),
				width: playerEl.width(),
				videoId: url
			});
		};
	};

	var handleArrows = function(){
		var slides = $('.slick-slide');
		var len = slides.length;
		var prevBtn = $('.flexslider-prev');
		var nextBtn = $('.flexslider-next');
		nextBtn.css({ 'display': slides.eq(len - 1).hasClass('slick-active') ? 'none' : 'block'});
		prevBtn.css({ 'display': slides.eq(0).hasClass('slick-active') ? 'none' : 'block'});
	};

	var initSlider = function() {
		var youtubeList = $('.watch-list-1');
		youtubeList.on('init', function(){
			handleArrows();
		}).on('afterChange', function(){
			handleArrows();
		}).slick({
			siaCustomisations: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			draggable: false,
			slide: 'div',
			infinite: false,
			// start wcag opts
			accessibility: true,
			assistiveTechnology: true,
			prevArrow: '<a href="#" class="slick-prev" role="button" aria-label="' + L10n.sliders.youtubeList.prevArrowLabel + '">' + L10n.sliders.youtubeList.prevArrowText + '</a>',
			nextArrow: '<a href="#" class="slick-next" role="button" aria-label="' + L10n.sliders.youtubeList.nextArrowLabel + '">' + L10n.sliders.youtubeList.nextArrowText + '</a>',
			customPaging: function(slider, i) {
				var index = (i+1);
				return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.youtubeList.customPagingTextPre + index + L10n.sliders.youtubeList.customPagingTextPost);
			},
			pauseOnFocus: true,
			focusOnSelect: true,
			pauseOnHover: true,
			// end wcag opts
		});
	};

	var initModule = function() {
		initYoutubePlayer();
		initSlider();
	};

	initModule();
	window.onYouTubeIframeAPIReady();
};
