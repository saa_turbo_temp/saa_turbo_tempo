
/**
 * @name SIA
 * @description Define global addBaggage functions
 * @version 1.0
 */
SIA.addBaggage = function(){
	var global = SIA.global;
	var config = global.config;
	var template = '';
	var addBaggageTable = $('[data-add-baggage-table]');
	var popupAddBaggage = $('.popup--add-baggage');
	var formAddBaggage = $('.form--add-baggage');

	// This function use for init Additional baggage popup
	var initPopupAddBaggage = function(){
		popupAddBaggage.Popup({
			overlayBGTemplate: config.template.overlay,
			triggerCloseModal: '.popup__close, [data-close]'
		});

		var initTemplatePopup = function(){
			var appendTemplate = function(data){
				var templateRender = function(template){
					addBaggageTable.empty();
					var html = window._.template(template, {
						data: data
					});

					addBaggageTable.append(html);
					if(popupAddBaggage.length){
						popupAddBaggage.Popup('show');
					}
				};

				if(!template){
					$.get(config.url.addBaggagePopup.template, function (tpl) {
						template = tpl;
						templateRender(template);
					});
				}else{
					templateRender(template);
				}
			};

			$.ajax({
				url: formAddBaggage.attr('action'),
				type: global.config.ajaxMethod,
				data: formAddBaggage.serialize(),
				dataType: 'json',
				beforeSend: function(){
					SIA.preloader.show();
				},
				success: function(data){
					if(data){
						appendTemplate(data);
					}
				},
				error: function(err) {
					window.alert(err);
				},
				complete: function(){
					SIA.preloader.hide();
				}
			});
		};

		formAddBaggage.on('submit.addBage', function(e){
			e.preventDefault();
			initTemplatePopup();
		});
	};

	// Scroll to passenger have data-baggage-passenger and data-baggage-segment-id respective
	// Example: mb-add-baggage.html?paxId=2&segmentId=1 will scroll to block passenger have data-baggage-passenger="2", data-baggage-segment-id="1"

	var scrollToPassenger = function() {
		var htmlBody = $('html, body');

		var getURLParams = function(sParam) {
			for (var i = 0; i < sURLVariables.length; i++) {
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] === sParam) {
					return sParameterName[1];
				}
			}
		};

		var search = window.location.search;
		var sPageURL = search.substring(1);
		var sURLVariables = sPageURL.split('&');

		var curPaxId = getURLParams('paxId');
		var curSegmentId = getURLParams('segmentId');
		var curSegmentEl = $('[data-baggage-segment-id="' + curSegmentId + '"]');
		var curPaxEl = curSegmentEl.find('[data-baggage-passenger="' + curPaxId + '"]');

		if (curSegmentEl.length && curPaxEl.length) {
			$('[data-accordion-trigger]').filter('.active').trigger('click.accordion');
			htmlBody.animate({
				scrollTop: curPaxEl.offset().top
			}, 400, function() {
				curPaxEl
					.closest('[data-accordion-content]')
					.find('[data-accordion-trigger]')
					.trigger('click.accordion');
			});
		}
	};

	var initModule = function(){
		if(window.location.search){
			scrollToPassenger();
		}
		initPopupAddBaggage();
	};

	initModule();
};
