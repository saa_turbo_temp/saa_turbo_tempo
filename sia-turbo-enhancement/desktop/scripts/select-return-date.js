/**
 * @name SIA
 * @description Define global selectReturnDate functions
 * @version 1.0
 */
SIA.selectReturnDate = function(){
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var _selectReturnDate = function(){
		// declare varibale
		// var returnSelect = $('#travel-radio-4');
		// var oneWaySelect = $('#travel-radio-5');
		// var returnField = $('#one-way-1');
		// var oneWayField = $('#one-way-2');
		var oneWayDatepickers = $('[data-oneway]');
		var timerDatepicker = null;
		var nonLimitedDate = $('[data-non-limited-date]');
		// var wp = returnField.closest('.animated');

		// select oneway or return
		// var selectFight = function(){
		// 	returnSelect.off('change.showReturnField').on('change.showReturnField', function(){
		// 		if($(this).is(':checked')){
		// 			returnField.removeClass('hidden');
		// 			oneWayField.addClass('hidden');
		// 			wp = returnField.closest('.animated');
		// 			wp.css('height', '');
		// 			if(win.width() < config.mobile && wp.length){
		// 				if(win.height() > wp.height()){
		// 					container.height(win.height());
		// 				}
		// 				else{
		// 					container.height(wp.height());
		// 				}
		// 			}
		// 		}
		// 	});
		// 	oneWaySelect.off('change.showOneWayField').on('change.showOneWayField', function(){
		// 		if($(this).is(':checked')){
		// 			oneWayField.removeClass('hidden');
		// 			returnField.addClass('hidden');
		// 			wp = returnField.closest('.animated');
		// 			if(win.width() < config.mobile && wp.length){
		// 				if(win.height() > wp.height()){
		// 					container.height(win.height());
		// 				}
		// 				else{
		// 					container.height(wp.height());
		// 				}
		// 			}
		// 		}
		// 	});
		// 	if((global.vars.isIE() && global.vars.isIE() < 9) || global.vars.isSafari){
		// 		returnSelect.off('afterTicked.showReturnField').on('afterTicked.showReturnField', function(){
		// 			returnSelect.trigger('change.showReturnField');
		// 		});
		// 		oneWaySelect.off('afterTicked.showOneWayField').on('afterTicked.showOneWayField', function(){
		// 			oneWaySelect.trigger('change.showOneWayField');
		// 		});
		// 	}
		// };

		var setBackToday = function(datepicker) {
			var today = new Date((new Date()).toDateString());
			var maxDate = $.datepicker._determineDate(datepicker.data('datepicker'), datepicker.datepicker('option', 'maxDate'), new Date());
			var enteredDate = datepicker.datepicker('getDate');
			if(nonLimitedDate.length) {
				if(enteredDate > today) {
					datepicker.datepicker('setDate', today);
				}
				return;
			}
			if(enteredDate < today) {
				datepicker.datepicker('setDate', today);
			}
			if(enteredDate > maxDate) {
				datepicker.datepicker('setDate', maxDate);
			}
		};

		oneWayDatepickers.each(function(){
			var oneWayDatepicker = $(this);
			if(oneWayDatepicker.data('init-oneway')){
				return;
			}
			var wp = oneWayDatepicker.closest('.input-3');

			var nonLimitedDate = $('[data-non-limited-date]');

			oneWayDatepicker.blur();
			oneWayDatepicker.data('init-oneway', true);
			// create an input for onway flight
			// var storeOneDay = $('<input type="text" value="" readonly class="datepicker-holder"/>').insertAfter(oneWayDatepicker).css({
			// 	'opacity': 0,
			// 	'visibility': 'visible',
			// 	'text-indent': '-20em',
			// 	'height': 50
			// });
			// storeOneDay.css({
			// 	width: oneWayDatepicker.width()
			// });

			wp.off('click.showDatepicker').on('click.showDatepicker', function(){
				var self = $(this);
				oneWayDatepicker.datepicker('show');
				if (!self.hasClass('disabled')) {
					self.addClass('focus');
				}
				win.off('resize.hideDatepickerOneWay').on('resize.hideDatepickerOneWay', function(){
					oneWayDatepicker.datepicker('hide');
				});
			}).off('focusout.highlightInput');

			oneWayDatepicker.datepicker({
				// defaultDate: '+1w',
				numberOfMonths: config.datepicker.numberOfMonths,
				minDate: nonLimitedDate.length ? null : new Date(),
				maxDate: nonLimitedDate.length ? new Date() : '+355d',
				dateFormat: 'dd/mm/yy',
				onChangeMonthYear: function(y, m, inst){
					clearTimeout(timerDatepicker);
					timerDatepicker = setTimeout(function() {
						$(inst.dpDiv).css('zIndex', config.zIndex.datepicker);
					}, 430);
				},
				beforeShow: function(input, inst){
					clearTimeout(timerDatepicker);
					timerDatepicker = setTimeout(function() {
						$(inst.dpDiv).css({
							'zIndex': config.zIndex.datepicker,
							'left': wp.offset().left,
							// 'top': wp.offset().top + wp.height() + 10,
							'display': 'block'
						}).off('click.doNothing').on('click.doNothing', function(){
							// fix for lumina
						});
					}, 100);
				},
				onSelect: function(dateText){
					oneWayDatepicker.val(dateText);
					if(oneWayDatepicker.closest('[data-target]').length){
						oneWayDatepicker.closest('[data-target]').prev().find('[data-start-date]').val(dateText).data('date', dateText);
					}
					if(oneWayDatepicker.closest('#travel-widget').data('widget-v1') || oneWayDatepicker.closest('#travel-widget').data('widget-v2')) {
						$('#travel-widget [data-start-date], #travel-widget [data-oneway]').not(oneWayDatepicker).closest('.input-3').removeClass('default');
						$('#travel-widget [data-oneway]').val(dateText);
						$('#travel-widget [data-start-date]').each(function(){
							$(this).val(dateText);
							var selfReturn = $(this).closest('[data-return-flight]').find('[data-return-date]');
							if(selfReturn.val() !== "") {
								var pselfReturn = selfReturn.datepicker('getDate').getTime();
								var pDepartDate = oneWayDatepicker.datepicker('getDate').getTime();
								if(selfReturn.is('[data-plus-date]')) {
									if(pselfReturn <= pDepartDate) {
										selfReturn.val(null);
									}
								} else {
									if(pselfReturn < pDepartDate) {
										selfReturn.val(null);
									}
								}
							}
							var startDateTmp = oneWayDatepicker.datepicker('getDate');
							var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate()));
							selfReturn.datepicker('option', 'minDate',
								returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
							);
						});
					};
					$(this).valid();
					// var validator = oneWayDatepicker.closest('form').data('validator');
					// var validatedOnce = oneWayDatepicker.closest('form').data('validatedOnce');
					// if(validator && validatedOnce) {
					// 	validator.element(oneWayDatepicker);
					// }
				},
				onClose: function(date, inst){
					var currentReturnDatepicker = $(this).val();
					var isValidDay = $.inputmask.isValid(currentReturnDatepicker, { alias: 'dd/mm/yyyy'});
					if(!isValidDay){
						if(!('ActiveXObject' in window)){ // fix ie 11 detect ie11
							oneWayDatepicker.val('');
							oneWayDatepicker.closest('[data-target]').prev().find('[data-start-date]').val('').data('date', '');
						}
					}
					wp.removeClass('focus');
					if(oneWayDatepicker.val()) {
						wp.removeClass('default');
					} else {
						wp.addClass('default');
					}
					win.off('resize.hideDatepickerOneWay');
					$(inst.dpDiv).off('click.doNothing');
				},
				dayNamesMin: config.formatDays
			}).keydown(function(e){
				var code = e.keyCode || e.which;
				if((code === 37 || code === 38 || code === 39 || code === 40) && (e.ctrlKey || e.metaKey)){
					var inst = $.datepicker._getInst(e.target);
					var dateStr = $.datepicker._formatDate(inst);
					var self = $(this);
					self.parent().find('#wcag-oneway-day').remove();
					if(!self.parent().find('#wcag-oneway-day').is('.say')) {
						self.parent().append('<span id="wcag-oneway-day" aria-live="assertive" class="says"></span>');
					}

					self.siblings('#wcag-oneway-day').html(dateStr);

					self.attr({
						'aria-label':dateStr,
						'role':'application',
						'aria-describeby':'wcag-oneway-day'
					});
					setTimeout(function(){
						self.datepicker('setDate', dateStr);
					}, 100);
				}
				if(code === 13) {
					$.datepicker._hideDatepicker();
				}
			});

			oneWayDatepicker.inputmask('date', {
				showMaskOnHover: false,
				oncomplete: function() {
					setBackToday(oneWayDatepicker);
				},
				onincomplete: function() {
					if(!window.Modernizr.input.placeholder) {
						setTimeout(function() {
							oneWayDatepicker.triggerHandler('blur.placeholder');
						}, 10);
					}
				}
			}).on('mouseover mouseout', function() {
				if(!$(this).inputmask('isComplete') && !window.Modernizr.input.placeholder) {
					$(this).triggerHandler('blur.placeholder');
				}
			})
			.on('blur.check-complete', function() {
				if(!$(this).inputmask('isComplete')) {
					$(this).val('');
				}
			})
			.triggerHandler('mouseout');

			oneWayDatepicker.off('focus.highlight').on('focus.highlight', function(){
				wp.addClass('focus');
			}).off('blur.highlight').on('blur.highlight', function(){
				wp.removeClass('focus');
			});

			// setTimeout(function() {
			// 	oneWayDatepicker.datepicker('setDate', 'today');
			// 	oneWayDatepicker.data('date', oneWayDatepicker.val()).closest('.input-3').removeClass('default');
			// }, 4000);
		});

		// change flight
		// selectFight();
	};

	// init
	// pick up return date
	_selectReturnDate();
};
