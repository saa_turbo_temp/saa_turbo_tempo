SIA.googleMap = function(gMapsLoaded){
  var global = SIA.global;
  var config = global.config;
  var win = $(window);
  var infoWindow;
  var maps = $('.hotel-block .hotel-list');
  var googlemapKey = config.map.googlemap.apiKey;
  var googlemapLib = config.map.googlemap.lib;
  var googlemapZoom = config.map.googlemap.zoom;
  var googlemapRadius = config.map.googlemap.radius;
  var gMapsLoaded = gMapsLoaded !== undefined ? gMapsLoaded : false;
  window.gMapsCallback = function(){
    maps.each(function(i, e){
      var mapEl = $(e);
      var lat = mapEl.data('latitude');
      var long = mapEl.data('longtitude');
      var mapId = mapEl.data('map-id');
      var hotelImg = mapEl.data('hotel-mainimg');
      var hotelName = mapEl.data('hotel-name');
      var rate = mapEl.data('rate');
      var star;
      triggerMapEl = mapEl.find('[data-hotel-map-trigger]');
      if(rate === 0.5){
        star = '<em class="ico-3-hotel-star-half"></em>';
      }else if(rate === 1){
        star = '<em class="ico-3-star-1"></em>';
      }else if(rate === 1.5){
        star = '<em class="ico-3-star-15"></em>';
      }else if(rate === 2){
        star = '<em class="ico-3-star-2"></em>';
      }else if(rate === 2.5){
        star = '<em class="ico-3-star-25"></em>';
      }else if(rate === 3){
        star = '<em class="ico-3-star-3"></em>';
      }else if(rate === 3.5){
        star = '<em class="ico-3-star-35"></em>';
      }else if(rate === 4){
        star = '<em class="ico-3-star-4"></em>';
      }else if(rate === 4.5){
        star = '<em class="ico-3-star-45"></em>';
      }else if(rate === 5){
        star = '<em class="ico-3-star-5"></em>';
      }
      triggerMapEl.off('click.loadHotelMap').on('click.loadHotelMap', function(){
        if($('#map_canvas_' + mapId).children().length <= 0){
          initialize(lat, long, mapId, hotelImg, hotelName, star);
        }
      });
    });
  };
  function loadGoogleMaps(){
    if(gMapsLoaded) return window.gMapsCallback();
    var script_tag = document.createElement('script');
    script_tag.setAttribute('type','text/javascript');
    script_tag.setAttribute('src','http://maps.google.com/maps/api/js?key=' + googlemapKey +'&libraries='+ googlemapLib +'&callback=gMapsCallback');
    (document.getElementsByTagName('head')[0] || document.documentElement).appendChild(script_tag);
  }
  function callbackRestaurant(results, status){
   if (status === google.maps.places.PlacesServiceStatus.OK){
     for (var i = 0; i < results.length; i++){
       createMarker(results[i], 'restaurant');
     }
   }
  }
  function callbackBank(results, status){
   if (status === google.maps.places.PlacesServiceStatus.OK){
     for (var i = 0; i < results.length; i++){
       createMarker(results[i], 'bank');
     }
   }
  }
  function callbackHotel(results, status){
   if (status === google.maps.places.PlacesServiceStatus.OK){
     for (var i = 0; i < results.length; i++){
       createMarker(results[i], 'hotel');
     }
   }
  }
 function createMarker(place, type){
   var placeLoc = place.geometry.location;
   if(type === 'restaurant'){
     var marker = new google.maps.Marker({
       map: map,
       icon: 'images/restaurant.png',
       position: place.geometry.location
     });
   }else if(type === 'bank'){
     var marker = new google.maps.Marker({
       map: map,
       icon: 'images/bank.png',
       position: place.geometry.location
     });
   }else if(type === 'hotel'){
     var marker = new google.maps.Marker({
       map: map,
       icon: 'images/balloon.png',
       position: place.geometry.location
     });
   }
   google.maps.event.addListener(marker, 'click', function(){
     infoWindow.setContent(place.name);
     infoWindow.open(map, this);
   });
 }
 function initialize(lat, long, mapId, hotelImg, hotelName, star){
   var pyrmont = new google.maps.LatLng(lat,long),
   mapOptions, markerMain, infoWindowMain, service,
   mapCanvasEl = document.getElementById('map_canvas_' + mapId);
   mapOptions = {
     zoom: googlemapZoom,
     center: pyrmont,
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     streetViewControl: false,
     mapTypeControl: false,
     zoomControl: false
   };
   map = new google.maps.Map(mapCanvasEl, mapOptions);
   markerMain = new google.maps.Marker({
     map: map,
     position: new google.maps.LatLng(lat,long),
     icon: 'images/main-location.png',
     zIndex: 1000
   });
   infoWindowMain = new google.maps.InfoWindow({content:'<div class="mainWindow"><div class="image"><img src="'+hotelImg+'" /></div><div class="desc"><h5>'+hotelName+'</h5>'+star+'</div></div>'});
   infoWindowMain.close();
   google.maps.event.addListener(markerMain, 'click', function(){
     infoWindowMain.open(map,markerMain);
   });
   infoWindow = new google.maps.InfoWindow();
   service = new google.maps.places.PlacesService(map);
   service.nearbySearch({
     location: pyrmont,
     radius: googlemapRadius,
     types: ['restaurant']
   }, callbackRestaurant);
   service.nearbySearch({
     location: pyrmont,
     radius: googlemapRadius,
     types: ['bank']
   }, callbackBank);
   service.nearbySearch({
     location: pyrmont,
     radius: googlemapRadius,
     types: ['hotel']
   }, callbackHotel);
 }
 if($('.hotel-block').data('baidu-map') === undefined){
   loadGoogleMaps();
 }
}
