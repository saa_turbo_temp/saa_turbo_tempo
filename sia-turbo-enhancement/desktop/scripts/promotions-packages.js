
/**
 * @name SIA
 * @description Define global promotionsPackages functions
 * @version 1.0
 */
SIA.promotionsPackages = function(){
	var dataPackagesResult = $('[data-result]');
	var itemsWrapper = dataPackagesResult.find('.items');
	var items = itemsWrapper.children();
	var filterdItems = $();
	var filterdItemsLength = filterdItems.length;
	var packagesForm = $('.form-promotion-packages');

	var sliderRange = $('#slider-range');

	var fromSL = packagesForm.find('.from-select');
	var fromInput = fromSL.find('input');

	var toSL = packagesForm.find('.to-select');
	var toInput = toSL.find('input');

	var durationSL = packagesForm.find('.duration-sl');
	var durationSLDefault = durationSL.find('select');

	var typeSL = packagesForm.find('.type-sl');
	var typeSLDefault = typeSL.find('select');

	var classSL = packagesForm.find('.class-sl');
	var classSLDefault = classSL.find('select');

	var inputSliderFrom = sliderRange.find('[name="from-price"]');
	var inputSliderTo = sliderRange.find('[name="to-price"]');
	var initValue = [inputSliderFrom.val(), inputSliderTo.val()];
	var labelFrom = sliderRange.find('.ui-slider_from');
	var labelTo = sliderRange.find('.ui-slider_to');
	var slideFrom = initValue[0];
	var slideTo = initValue[1];

	var count = 1;
	var currentShow = 6;
	var limitOnceShow = 6;
	var totalshownItem = 0;

	var seemoreBtn = $('[data-see-more]');

	var searchItems = function(items, from, to, duration, typeFlight, classFlight, min, max) {

		return items
			.addClass('hidden')
			.filter(function() {
				var self = $(this);

				return (!from || self.data('promotion-package-from').toLocaleLowerCase() === from.toLocaleLowerCase()) &&
					(!to || self.data('promotion-package-to').toLocaleLowerCase() === to.toLocaleLowerCase()) &&
					(!duration || self.data('promotion-package-duration').toLocaleLowerCase() === duration.toLocaleLowerCase()) &&
					(!typeFlight || self.data('promotion-package-typeflight').toLocaleLowerCase() === typeFlight.toLocaleLowerCase()) &&
					(!classFlight || self.data('promotion-package-classflight').toLocaleLowerCase() === classFlight.toLocaleLowerCase()) &&
					(accounting.unformat(self.data('promotion-package-price')) <= max && accounting.unformat(self.data('promotion-package-price')) >= min);
			});

	};

	var showItem = function(numberofitem){

		filterdItems.filter(':lt('+ numberofitem +')').removeClass('hidden');

	};

	var refreshData = function(){

		filterdItems = searchItems(items, fromInput.val(), toInput.val(), durationSLDefault.val(), typeSLDefault.val(), classSLDefault.val(), sliderRange.slider('values', 0), sliderRange.slider('values', 1));
		totalshownItem = currentShow + limitOnceShow*(count -1);
		showItem(totalshownItem);
		filterdItemsLength = filterdItems.length;

		if(filterdItemsLength > currentShow) {
			seemoreBtn.removeClass('hidden');
		}
		else {
			seemoreBtn.addClass('hidden');
		}

	};

	var initSlider = function(){

		sliderRange.slider({
			range: true,
			min: sliderRange.data('current-min'),
			max: sliderRange.data('current-max'),
			values: initValue,
			create: function() {
				labelFrom.text(accounting.formatMoney(initValue[0], '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(initValue[1], '', 0, ',', '.'));
			},
			slide: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
				inputSliderFrom.val(slideFrom);
				inputSliderTo.val(slideTo);
			},
			stop: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
			}
		});

	};

	var seeMoreHandle = function(){

		seemoreBtn.off('click.seemore').on('click.seemore', function(e){
			var wcagSeemore = itemsWrapper.find('.ui-helper-hidden-accessible.seemore');
			count++;
			if(filterdItemsLength > totalshownItem) {
				totalshownItem = currentShow + limitOnceShow*(count -1);
				if(count === 4 ){
					totalshownItem = filterdItemsLength;
					seemoreBtn.addClass('hidden');
					seemoreBtn.text(L10n.kfSeemore.seeMore);
				}
				else {
					if(seemoreBtn.is(':hidden')){
						seemoreBtn.removeClass('hidden');
					}
					if (count === 3) {
						seemoreBtn.text(L10n.kfSeemore.seeAll);
					}
				}
				showItem(totalshownItem);

				wcagSeemore.text('');
				wcagSeemore.text(L10n.wcag.seemoreLabel.format(limitOnceShow*(count -1)));
				filterdItems.eq(currentShow).find('.flight-item__inner').children('a').focus();

			}
			e.preventDefault();

		});

	};

	var updateHandle = function(){

		packagesForm.on('submit.packagesForm', function(e){
			var wcagUpdate = itemsWrapper.find('.ui-helper-hidden-accessible.update');

			currentShow = 6;
			count = 1;
			refreshData();

			wcagUpdate.text('');
			wcagUpdate.text(L10n.wcag.foundLabel.format(filterdItems.length));

			e.preventDefault();
		});

	};

	var wcag = function() {
		var items = $('.flight-item');

		if($('#aria-seemore').length === 0) {
			itemsWrapper.prepend('<span class="ui-helper-hidden-accessible seemore" id="aria-seemore" aria-atomic="true" aria-live="assertive"></span>')
		};
		if($('#aria-update').length === 0) {
			itemsWrapper.prepend('<span class="ui-helper-hidden-accessible update" id="aria-update" aria-atomic="true" aria-live="assertive"></span>')
		};

		items.each(function(index, e){
			var item = $(e),
				itemDetails = item.find('.flight-item__details');
				itemHeading = itemDetails.find('.packages-heading'),
				itemContent = itemDetails.find('p');

				itemHeading.attr({
					'tabindex': 0
				});

				itemContent.attr({
					'tabindex': 0
				});
		});

	};

	var initModule = function(){
		initSlider();
		refreshData();
		seeMoreHandle();
		updateHandle();
		wcag();
	};

	initModule();
};
