/**
 * @name SIA
 * @description Define function to multiple price points
 * @version 1.0
 */
SIA.pricePoints = function() {
	var pricePoints = $('[data-price-points]');

	var initShuffle = function(result){
		result.shuffle({
			speed: 0,
			itemSelector: '.promotion-item',
			sizer: result.find('.promotion-item').eq(0)
		});
	};

	var filterShuffle = function(result, filterItems) {
		result.prepend(filterItems);
		result.data('shuffle').sizer = $('.promotion-item').eq(0)[0];
		result.shuffle('shuffle', function($el){
			return !$el.hasClass('hidden');
		});
	};

	var filterContent = function(resultWrapper, dest) {
		var promotionItems = resultWrapper.find('.promotion-item');
		return !dest ?
			promotionItems.removeClass('hidden') :
			promotionItems.addClass('hidden').filter(function() {
				return $(this).data('destination') === dest;
			}).removeClass('hidden');
	};

	pricePoints.each(function(){
		var self = $(this);
		var form = self.find('.form-station');
		var filterDestEl = self.find('[data-filter-destination]');
		var resultWrapper = self.find('.promotion-result');
		var filterDest = $.trim(filterDestEl.find('select').val());

		filterContent(resultWrapper, filterDest);
		initShuffle(resultWrapper);

		form.off('submit.filterPP').on('submit.filterPP', function(e){
			e.preventDefault();
			var filterDest = $.trim(filterDestEl.find('select').val());
			var filterItems = filterContent(resultWrapper, filterDest);
			filterShuffle(resultWrapper, filterItems);
		});
	});
};
