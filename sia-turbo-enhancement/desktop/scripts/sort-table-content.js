/**
 * @name SIA
 * @description Define global sortTabletContent functions
 * @version 1.0
 */
SIA.sortTableContent = function(){
	var resultTableBody = $('[data-sort-content]').find('tbody');
	var destSortEl = $('[data-sort-target]');

	var sortContent = function(target) {
		var targetIdx = target.closest('th').index();
		var sltor = 'td:nth(' + targetIdx + ')';
		return $(resultTableBody.find('tr').toArray().sort(function(a, b) {
			var aVlue = $(a).find(sltor).text();
			var bVlue = $(b).find(sltor).text();
			return target.hasClass('active') ?
				(aVlue > bVlue ? -1 : 1) :
				(aVlue > bVlue ? 1 : -1);
		}));
	};

	var resetOddEven = function(list) {
		if (list.hasClass('odd') || list.hasClass('even')) {
			list.removeClass('odd even');
			list.each(function(idx, el) {
				$(el).addClass(idx % 2 === 0 ? 'odd' : 'even');
			});
		}
		return list;
	};

	destSortEl
		.off('click.sortContent')
		.on('click.sortContent', function(e) {
			e.preventDefault();
			var self = $(this);
			resultTableBody.html(resetOddEven(sortContent(self)));
			self.toggleClass('active');
		});
};
