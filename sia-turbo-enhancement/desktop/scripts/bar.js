/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.bar = function() {
	var global = SIA.global;
	var body = $('body');

	// Init
	var initModule = function() {
		$.get(global.config.url.barTemplate, function (data) {
			var template = window._.template(data, {
				data: {}
			});
			var templateEl = $(template);
			body.append(templateEl);
		});
	};

	initModule();
};
