/**
 * @name SIA
 * @description Define global kf-message functions
 * @version 1.0
 */
SIA.KFMessage = function() {
	var global = SIA.global;
	var config = global.config;
	var checkboxAll = $('[data-wrapper-checkboxall]'),
			btnCheckboxAll = checkboxAll.find(':checkbox'),
			linkMesMark = $('[data-message-mark]'),
			linkMesDelete = $('[data-message-delete]'),
			linkMesSeeMore = $('[data-see-more]'),
			sortEl = $('[data-message-sort]'),
			form = checkboxAll.closest('[data-message-form]'),
			mesList = form.find('.messages-list'),
			templateItemMes,
			listitem,
			seemoreCount = 0,
			lastItemIdx = 0;

	mesList.prepend('<span class="ui-helper-hidden-accessible" aria-atomic="true" id="aria-loaded-item" aria-live="assertive"></span>');
	mesList.prepend('<span class="ui-helper-hidden-accessible" id="aria-filter-item" aria-atomic="true" aria-live="assertive"></span>');

	var ajaxSuccess = function(res) {
		var foreachItems = function(items, callback) {
			for(var i = items.length; i > 0; i--) {
				var currentItem = items[i - 1];
				if($.parseJSON(currentItem.status)) {
					callback(form.find('#' + currentItem.id).closest('.messages-list__item'));
				}
			}
		};

		seemoreCount++;

		switch(res.type) {
			case 'message-mark':
				if(res.messages) {
					foreachItems(res.messages, function(item) {
						item.addClass('messages-list__read');
					});
				}
				break;
			case 'message-delete':
				if(res.messages) {
					foreachItems(res.messages, function(item) {
						item.remove();
					});
				}
				break;
			case 'message-seemore':
				if($.parseJSON(res.isLast)) {
					// linkMesSeeMore.addClass('disabled');
					linkMesSeeMore.addClass('hidden');
					linkMesSeeMore.text(L10n.kfSeemore.seeMore);
					seemoreCount = 0;
				}
				else if (seemoreCount === 2) {
					linkMesSeeMore.text(L10n.kfSeemore.seeAll);
				}

				if(res.messages) {
					btnCheckboxAll.prop('checked', false);
					if(templateItemMes) {
						mesList.append(window._.template(templateItemMes, {'items' : res.messages}));
						global.vars.checkAllList(btnCheckboxAll, mesList);
						wcag();
					}
					else {
						$.get(config.url.kfMessageItemTemplate, function(templateStr) {
							templateItemMes = templateStr;
							mesList.append(window._.template(templateItemMes, {'items' : res.messages}));
							global.vars.checkAllList(btnCheckboxAll, mesList);
							wcag();
						}, 'html');
					}
				}
				break;
		}
	};

	var ajaxFail = function(jqXHR, textStatus) {
		console.log(textStatus);
	};

	var initAjax = function(url, data, type, notChecked) {
		type = type || 'json';
		var checked = notChecked ? '' : form.find('[type="checkbox"]:checked');
		if(checked.length || notChecked) {
			var dataAjax = (notChecked ? '' : checked.serialize()) + (data ? '&' + $.param(data) : '');
			$.ajax({
				url: url,
				type: global.config.ajaxMethod,
				dataType: type,
				data: dataAjax,
				success: ajaxSuccess,
				error: ajaxFail
			});
		}
	};

	var sortItems = function(listItemEl, timerEl, type) {
		var sortArr;
		listitem = listItemEl.find('.messages-list__item');
		listitem.detach();
		sortArr = listitem.sort(function(a, b) {
			var timeA = Date.parse($(a).find(timerEl).data('time')),
					timeB = Date.parse($(b).find(timerEl).data('time'));
			if(type === 'oldest-mail') {
				return timeA - timeB;
			}
			return timeB - timeA;
		});
		listItemEl.append(sortArr);
		var len = listItemEl.find('.messages-list__item').length;
		$('#aria-filter-item').text('');
		$('#aria-filter-item').text(L10n.wcag.foundLabel.format(len));
	};

	linkMesMark.off('click.messageMark').on('click.messageMark', function(e) {
		e.preventDefault();
		initAjax(config.url.kfMessageMarkJSON);
	});

	linkMesDelete.closest('form').off('submit.messageDelete').on('submit.messageDelete', function() {
		initAjax(config.url.kfMessageDeleteJSON);
		return false;
	});

	linkMesSeeMore.off('click.messageSeeMore').on('click.messageSeeMore', function(e) {
		e.preventDefault();
		lastItemIdx = mesList.find('.messages-list__item').length;
		if(!$(this).hasClass('disabled')) {
			var url = config.url.kfMessageItemsJSON;
			if(global.vars.win.width() < config.tablet) {
				url = config.url.kfMessageItemsTabletJSON;
			}
			initAjax(url, {'paginate':(seemoreCount + 1 === 3 ? 'all' : seemoreCount + 1)}, null, true);
		}
	});

	sortEl.off('change.sortMessage').on('change.sortMessage', function() {
		sortItems(mesList, '.messages-list__time', this.value);
	});

	// Trigger close popup and custom select when opened menu
	$('.menu .ico-nav').click(function() {
		global.vars.body.trigger('click');
	});

	var wcag = function() {

		var items = mesList.find('.messages-list__item');

		items.each(function(idx, e){
			var _self = $(e),
				desc = _self.find('.messages-list__content p'),
				time = _self.find('.messages-list__time p'),
				heading = _self.find('.sub-heading-2--dark a');

			desc.attr({
				'tabindex' : 0
			});

			time.attr({
				'tabindex' : 0
			});
		});

		if(lastItemIdx !== 0) {
				items.eq(lastItemIdx).find('.sub-heading-2--dark a').focus();
				$('#aria-loaded-item').text('');
				$('#aria-loaded-item').text(L10n.wcag.seemoreLabel.format(items.length - lastItemIdx));
		}

	};

	wcag();

	global.vars.checkAllList(btnCheckboxAll, checkboxAll.siblings('.messages-list'));
};
