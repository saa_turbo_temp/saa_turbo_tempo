/**
 * @name SIA
 * @description Define global faqs functions
 * @version 1.0
 */
SIA.faqs = function(){
	var accorWrapperContent = $('[data-accordion-wrapper-content]');
	var openAllAccor = $('.open-all-btn');

	var scrollToAccor = function() {
		accorWrapperContent
			.off('click.removeHighlight')
			.on('click.removeHighlight', '[data-accordion-trigger]', function(e) {
				e.preventDefault();
				accorWrapperContent
					.find('[data-accordion-content]')
					.removeClass('jump-highlight');

				$(this).off('setHash.faqsPages').on('setHash.faqsPages', function() {
					var hash = window.location.hash.split('?');
					hash[0] = $(this).closest('[data-accordion]').attr('id');
					window.location.hash = hash.join('?');
				});
			});

		openAllAccor
			.off('click.openAll')
			.on('click.openAll', function() {
				var jumpHighlight = $('.jump-highlight');
				if (jumpHighlight.length) {
					jumpHighlight.removeClass('jump-highlight');
				}
			});

		var accorId = window.location.hash.split('?')[0];
		var accor = $(accorId);

		if (accor.length && accor.hasClass('accordion')) {
			accor.find('[data-accordion-content]').addClass('jump-highlight');
			accor.find('[data-accordion-trigger]').trigger('click.accordion');
		}
	};

	var initIframe = function(){
		var videoEls = $('[data-video-url]');
		videoEls.each(function(idx, item){
			var videoEl = $(item);
			var url = videoEl.data('video-url');
			var iframe = '<iframe width="420" height="315"src="http://www.youtube.com/embed/' + url + '" allowfullscreen></iframe>';
			videoEl.html(iframe);
		});
	};

	var initModule = function(){
		scrollToAccor();
		initIframe();
	};

	initModule();
};
