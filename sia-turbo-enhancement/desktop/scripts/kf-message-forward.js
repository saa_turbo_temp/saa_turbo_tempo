/**
 * @name SIA
 * @description Define global kf-message-forward functions
 * @version 1.0
 */
SIA.KFMessageForward = function() {
	var global = SIA.global;
	var formValidate = $('[data-form-validate]');

	if(formValidate.length) {
		formValidate.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	}
};
