/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.grayscaleModule = function() {
	var config = SIA.global.config;
	var vars = SIA.global.vars;
	var win = vars.win;

	var grayscaleLogo = function() {
		var logo = $('.logo').find('img');
		var newSrc = {
			svg: 'images/svg/logo-singaporeairlines-white.svg',
			png: 'images/logo-singaporeairlines-white.png'
		};
		logo.eq(0).attr('src', newSrc.svg);
		logo.eq(1).attr('src', newSrc.png);
	};
	grayscaleLogo();

	var grayScaleFunc = function() {
		var getBrowser = function() {
			var userAgent = navigator.userAgent.toLowerCase();
			var msie = /msie/.test(userAgent) && !/opera/.test(userAgent);

			if (msie) {
				return 'ie';
			}
		};

		if (getBrowser() === 'ie') {
			$('body').addClass('ie');
		}

		var getInternetExplorerVersion = function() {
			var rv = -1;
			if (navigator.appName === 'Microsoft Internet Explorer') {
				var ua = navigator.userAgent;
				var re = new RegExp('MSIE ([0-9]{1,}[\\.0-9]{0,})');
				if (re.exec(ua) !== null) {
					rv = parseFloat(RegExp.$1);
				}
			} else if (navigator.appName === 'Netscape') {
				var ua = navigator.userAgent;
				var re = new RegExp('Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})');
				if (re.exec(ua) !== null) {
					rv = parseFloat(RegExp.$1);
				}
			}
			return rv;
		};

		if (getInternetExplorerVersion() >= 10) {
			$('body').addClass('ie11');
		}

		if (getInternetExplorerVersion() >= 10) {
			var grayscaleIE10 = function(src) {
				var canvas = document.createElement('canvas');
				var ctx = canvas.getContext('2d');
				var imgObj = new Image();
				imgObj.src = src;
				canvas.width = imgObj.width;
				canvas.height = imgObj.height;
				ctx.drawImage(imgObj, 0, 0);
				if (!canvas.width || !canvas.height) {
					return;
				}
				var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
				for (var y = 0; y < imgPixels.height; y++) {
					for (var x = 0; x < imgPixels.width; x++) {
						var i = (y * 4) * imgPixels.width + x * 4;
						var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
						imgPixels.data[i] = avg;
						imgPixels.data[i + 1] = avg;
						imgPixels.data[i + 2] = avg;
					}
				}
				ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
				return canvas.toDataURL();
			};

			var img = $('img').filter(function() {
				return !$(this).parent().hasClass('logo');
			});

			img.each(function(idx) {
				var el = $(this);
				if (el.closest('.full-banner--img').length) {
					var fullBannerImg = el.closest('.full-banner--img');
					var imgSrc = el.attr('src');

					el.data('imgSrc', imgSrc).attr('src', config.imgSrc.transparent);
					fullBannerImg.css({
						'backgroundImage': 'url(' + grayscaleIE10(imgSrc) + ')'
					}).removeClass('visibility-hidden');

					var resizeFullBanner = function() {
						if(vars.detectDevice.isTablet() && fullBannerImg.data('tablet-bg')){
							fullBannerImg.css({
								'background-position': fullBannerImg.data('tablet-bg')
							});
						}
						else if(window.innerWidth > config.tablet){
							if(fullBannerImg.data('desktop')){
								fullBannerImg.css('background-position', fullBannerImg.data('desktop'));
							}
							else{
								fullBannerImg.css('background-position', '');
							}
						}
					};

					win
						.off('resize.fullBannerImg' + idx)
						.on('resize.fullBannerImg' + idx, resizeFullBanner).trigger('resize.fullBannerImg' + idx);
				}
				else {
					el.wrap('<div class="img_wrapper" style="display: inline-block; max-width: 100%">').clone().addClass('img_grayscale').css({
						'position': 'absolute',
						'z-index': '5',
						'opacity': '0'
					}).insertBefore(el).queue(function() {
						var el = $(this);
						el.parent().css({
							'position': 'relative',
						});
						el.dequeue();
					});
					this.src = grayscaleIE10(this.src);
				}
			});
		}
	};
	grayScaleFunc();
};
