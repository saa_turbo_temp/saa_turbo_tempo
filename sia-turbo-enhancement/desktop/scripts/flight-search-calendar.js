/**
 * @name SIA
 * @description Define global flightSearchCalendar functions
 * @version 1.0
 */
SIA.flightSearchCalendar = function(){
	if(!$('.flight-search-calendar-page, .orb-flight-schedule').length){
		return;
	}

	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;

	var bindActions = function() {
		var flightSearchCalendar = $('.flight-search-calendar');
		var next = $('.flight-search-calendar__control .slick-next', flightSearchCalendar);
		var prev = $('.flight-search-calendar__control .slick-prev', flightSearchCalendar);
		var btnNext = $('[data-btn-search]').length ? $('[data-btn-search]') : $('.select-flights-page .btn-1');

		var disableNextBtn = function(e) {
			e.preventDefault();
			return;
		};

		// var resetCalendarTable = function(rowFareElement) {
		// 	btnNext.addClass('disabled').on('click.disabled', disableNextBtn);
		// 	$('input[type="radio"]', rowFareElement).prop('checked', false);
		// };

		var setTypeDisplay = function(element) {
			if(window.innerWidth >= config.tablet) {
				element.data('typeDisplay', 'desktop');
			} else {
				element.data('typeDisplay', 'tablet');
			}
		};

		var checkBtnNext = function() {
			var check = true;
			flightSearchCalendar.each(function() {
				var that = $(this),
						checkedInput = $('input:checked:visible', that);
				// if(!Object.keys(checkedInput).length) {
				if(!checkedInput.length) {
					check = false;
					return;
				}
			});

			if(check) {
				btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
			}
		};

		var hiddenItem = function(number) {
			if (number === 7) {
				$('.flight-search-calendar__item').removeClass('hidden');
			}
			else {
				flightSearchCalendar.each(function() {
					var fsCalendar = $(this);
					var items = fsCalendar.find('.flight-search-calendar__item');
					var len = items.length;
					var idx = fsCalendar.find('input[type="radio"]:checked')
						.closest('.flight-search-calendar__item').index();

					var l = idx <= 1 ? 0 : ((idx > 1 && idx + number <= len) ? (idx - 1) : (len - number));
					var r = l + number - 1;

					items.removeClass('hidden-tablet hidden-mb').addClass('hidden').filter(function(index) {
						return index >= l && index <= r;
					}).removeClass('hidden');
				});
			}
		};

		setTypeDisplay(btnNext);

		win.off('resize.flightSearchCalendar').on('resize.flightSearchCalendar', function() {
			var type, number;
			if(window.innerWidth >= config.tablet) {
				type = 'desktop';
				number = 7;
				next.attr('href', next.data('url-desktop'));
				prev.attr('href', prev.data('url-desktop'));
			}
			else {
				type = 'tablet';
				number = 5;
				next.attr('href', next.data('url-tablet'));
				prev.attr('href', prev.data('url-tablet'));
			}

			hiddenItem(number);

			next.find('.number').text(number);
			prev.find('.number').text(number);

			if(btnNext.data('typeDisplay') !== type) {
				btnNext.data('typeDisplay', type);
				// $('input[type="radio"]:checked', flightSearchCalendar).each(function() {
				// 	var that = $(this);
				// 	if(that.parents('.flight-search-calendar__item').is(':last-child, :first-child')) {
				// 		resetCalendarTable(that.parents('.flight-search-calendar'));
				// 	}
				// });
			}
		}).trigger('resize.flightSearchCalendar');

		$('input[type="radio"]', flightSearchCalendar).off('change.checkedFare').on('change.checkedFare', function() {
			checkBtnNext();
		});
	};

	if($('.flight-search-calendar-page').length) {
		//CIB page
		bindActions();
	}
	else {
		//ORB Flight Schedule page
		$.get(config.url.orbFlightSchedule, function(html) {
			var template = window._.template(html, {
				data: globalJson.flightSchedule
			});

			$('#form-flight-schedule').html(template);

			bindActions();
		});
	}
};
