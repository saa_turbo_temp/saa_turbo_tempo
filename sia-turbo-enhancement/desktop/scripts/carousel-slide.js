/**
 * @name SIA
 * @description Define function to carousel slide
 * @version 1.0
 */
SIA.carouselSlide = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var isCurrentTablet = global.vars.detectDevice.isTablet();
	var carouselSlide = $('[data-carousel-slide]');
	var paddingRight = 22;

	if(carouselSlide.length){
		carouselSlide.each(function(){
			var carousel = $(this);
			var numberSlide = carousel.data('carousel-slide');
			var wrapperHLS = carousel.parent();
			var imgHighlightLength = carousel.find('img').length - 1;

			var loadBackgroundHighlight = function(self, parentSelt, idx) {
				if (global.vars.detectDevice.isTablet()) {
					parentSelt.css({
						'backgroundImage': 'url(' + self.attr('data-img-src') + ')'
					});
					self.attr('src', config.imgSrc.transparent);
				}

				if (idx === imgHighlightLength) {
					carousel.width(wrapperHLS.width() + paddingRight);
					carousel.css('visibility', 'visible');
					carousel.find('.slides')
						.slick({
							siaCustomisations: true,
							dots: true,
							speed: 300,
							draggable: true,
							slidesToShow: numberSlide,
							slidesToScroll: numberSlide,
							useCSS: global.vars.isNewIE() ? false : true,
							// start wcag opts
							accessibility: true,
							assistiveTechnology: true,
							customPaging: function(slider, i) {
								var index = (i+1);
								return $('<button type="button" data-role="none" role="tab" tabindex="0" />').text(L10n.sliders.carousel.customPagingTextPre + index + L10n.sliders.carousel.customPagingTextPost);
							},
							pauseOnFocus: true,
							focusOnSelect: true,
							pauseOnHover: true,
							// end wcag opts
							responsive: [{
								breakpoint: 988,
								settings: {
									slidesToShow: 3,
									slidesToScroll: 3
								}
							}]
						});
				}
			};

			carousel.find('img').each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				self.attr('data-img-src', self.attr('src'));

				nI.onload = function() {
					loadBackgroundHighlight(self, parentSelt, idx);
				};

				nI.src = self.attr('src');
			});
		});

		win.off('resize.carouselSlide').on('resize.carouselSlide', function() {
			carouselSlide.each(function(){
				var carousel = $(this);
				var wrapperHLS = carousel.parent();

				var changeBgToTablet = function() {
					carousel.find('img').each(function() {
						var self = $(this);
						var parentSelt = self.parent();

						parentSelt.css({
							'backgroundImage': 'url(' + self.attr('data-img-src') + ')'
						});

						self.attr('src', config.imgSrc.transparent);
					});
				};

				var changeBgToDesktop = function() {
					carousel.find('img').each(function() {
						var self = $(this);
						var parentSelt = self.parent();

						parentSelt.css('backgroundImage', '');
						self.attr('src', self.attr('data-img-src'));
					});
				};

				carousel.width(wrapperHLS.width() + paddingRight);
				if (global.vars.detectDevice.isTablet() && !isCurrentTablet) {
					isCurrentTablet = true;
					changeBgToTablet();
				}
				else if (!global.vars.detectDevice.isTablet() && isCurrentTablet) {
					isCurrentTablet = false;
					changeBgToDesktop();
				}
			});
		}).trigger('resize.carouselSlide');

		$('.popup--intro-lightbox').on('afterHide', function() {
			win.trigger('resize.carouselSlide');
		});
	}
};
