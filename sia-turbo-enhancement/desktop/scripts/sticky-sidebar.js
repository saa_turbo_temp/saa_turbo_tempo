/**
 * @name SIA
 * @description Define global stickySidebar functions
 * @version 1.0
 */
SIA.stickySidebar = function() {
  // var dropdownsOnMobile = function(wrapper, ch, sl){
  //  var chs = wrapper.find(ch);
  //  var sls = wrapper.find(sl);
  //  sls.off('change.direct').on('change.direct', function(){
  //    window.location.href = chs.eq(sls.prop('selectedIndex')).attr('href');
  //  });
  // };

  var global = SIA.global,
    win = global.vars.win;

  var bookingNav = $('[data-fixed].sidebar');
  var innerBookingNav = bookingNav.children('.inner');
  var navBookingNav = bookingNav.find('.booking-nav');
  var listTabs = navBookingNav.find('.booking-nav__item');

  if (bookingNav.length) {
    // reset scroll Top
    win.scrollTop(0);
    var wrapperBookingNav = bookingNav.parent();
    // var isOneScroll = true;
    var startScroll, maxScroll, topRelative;

    win.off('scroll.sticky').on('scroll.sticky', function() {
      if (window.innerWidth >= global.config.tablet) {
        var currentTop = win.scrollTop();
        // if(isOneScroll) {
        startScroll = bookingNav.offset().top;
        maxScroll = (wrapperBookingNav.height() + wrapperBookingNav.offset().top) - innerBookingNav.height() - 140;
        topRelative = bookingNav.height() - innerBookingNav.height();
        innerBookingNav.css({
          left: bookingNav.offset().left
        });
        // isOneScroll = false;
        // }
        if (startScroll <= currentTop) {
          innerBookingNav.removeClass('inner-relative').addClass('inner-fixed').css('top', '');
          if (currentTop + innerBookingNav.height() >= bookingNav.height() + startScroll) {
            innerBookingNav.removeClass('inner-fixed').addClass('inner-relative').css({
              top: topRelative
            });
          }
          // console.log(startScroll, win.scrollTop());
        } else {
          innerBookingNav.removeClass('inner-fixed inner-relative').css('top', '');
        }
        // if(innerBookingNav.height() + 50 < win.height()) {
        //  if(startScroll < currentTop) {
        //    if(maxScroll < currentTop) {
        //      if(!innerBookingNav.hasClass('stop-scroll')) {
        //        innerBookingNav.removeClass('inner-fixed').addClass('inner-relative stop-scroll').css({
        //          top : topRelative
        //        });
        //      }
        //    } else {
        //      innerBookingNav.addClass('inner-fixed').removeClass('stop-scroll').removeClass('inner-relative');
        //    }
        //  } else {
        //    innerBookingNav.css('position', 'static').removeClass('inner-fixed');
        //  }
        // }
      }
    });

    win.off('resize.stickySidebar').on('resize.stickySidebar', function() {
      innerBookingNav.css('left', 'auto');
      if (window.innerWidth < global.config.tablet) {
        innerBookingNav.removeClass('inner-fixed').removeClass('inner-relative');
      }
    });


    /*var wrapperBookingNav = bookingNav.parent();
    var maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;
    var offsetBookingNavInWrapper = parseFloat(bookingNav.css('top'));
    var bookingNavOffsetTop = wrapperBookingNav.offset().top;
    SIA.global.vars.win.off('scroll.sticky mousewheel.sticky').on('scroll.sticky mousewheel.sticky', function(){
      maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;
      bookingNavOffsetTop = wrapperBookingNav.offset().top;
      if((bookingNavOffsetTop +offsetBookingNavInWrapper) <= SIA.global.vars.win.scrollTop()){
        if(SIA.global.vars.win.scrollTop() > maxScroll){
          bookingNav.removeAttr('style').css({
            position: 'fixed',
            left: 'auto',
            bottom : SIA.global.vars.win.scrollTop() - maxScroll ,
            top : 'auto'
          });
        }
        else{
          bookingNav.removeAttr('style').css({
            position: 'fixed',
            left: 'auto',
            top: 0
          });
        }
      }
      else{
        bookingNav.removeAttr('style');
      }
    });*/

    innerBookingNav.attr({ role: 'application' });
    navBookingNav.attr({ role: 'tablist' });
    listTabs.attr({ role: 'tab' });
    bookingNav.off('keydown.booking').on('keydown.booking', function(e) {
      if (bookingNav.find(':focus').length === 0) {
        return false;
      }
      var currActive = $(ally.get.activeElement());
      var innerBookingNav = bookingNav.children('.inner');
      var navBookingNav = bookingNav.find('.booking-nav');
      var listTabs = navBookingNav.find('.booking-nav__item');
      function moveTab(dir) {
        var currTab = $(e.target);
        var currTabInx;
        var currenActive = $(ally.get.activeElement());
        if (dir === 'next') {
          currTabInx = listTabs.index(currTab) + 1;
          if (currTabInx >= listTabs.length) {
            currTabInx = 0;
          }
        } else if (dir === 'prev') {
          currTabInx = listTabs.index(currTab) - 1;
          if (currTabInx < 0) {
            currTabInx = listTabs.length - 1;
          }
        }
        var currentActiveTab = listTabs.eq(currTabInx);
        if (currentActiveTab && !currenActive.is('input')) {
          ally.element.focus(currentActiveTab);
        }
        currentActiveTab.attr('tabindex', 0);
        currentActiveTab.siblings().attr('tabindex', -1);
        currentActiveTab.trigger('click');

      }

      function moveContent(dir) {
        var currContent = $(e.target);
        var listContent = navBookingNav.find('.booking-nav__item.active').find('[tabindex], button, a, input').not('.hidden');
        var currContentInx;
        var currenActive = $(ally.get.activeElement());

        if (dir === 'next') {
          currContentInx = listContent.index(currContent) + 1;
          if (currContentInx >= listContent.length) {
            currContentInx = 0;
          }
        } else if (dir === 'prev') {
          currContentInx = listContent.index(currContent) - 1;
          if (currContentInx < 0) {
            currContentInx = listContent.length - 1;
          }
        }
        var currentActiveContent = listContent.eq(currContentInx);
        if (currentActiveContent && !currenActive.is('input')) {
          ally.element.focus(currentActiveContent[0]);
        }

      }
      switch (e.keyCode) {
        case 39:
        case 40:
        case 9:
          moveTab('next');
          e.preventDefault();
          break;
        case 37:
        case 38:
          moveTab('prev');
          break;
        case 13:
          //Enter handle case here
          var fistTabindex = bookingNav.parents('.wrap-select-meals').find('.form--select-meals').find('[tabindex]').eq(0);
          // Code for focusing on generic
          if (e.shiftKey) {
            return true;
          }
          if ($('.seatmap.seatmap--generic').length) { //Handle Case it seatmap available
            var seatMap = $('.seatmap.seatmap--generic');
            var visibleSelect = seatMap.find('[data-customselect]:visible').first(); //Find the first focusable custom select
            if (visibleSelect.length) { //Focus on the select
              visibleSelect.click().click();
              return false;
            } else { //Focus on the button
              ally.element.focus(seatMap.parent('.form-seatmap').find('.button-group-1').find('input:visible').first()[0]);
              return false;
            }
          }
          if (currActive.parents('.form--select-meals').length) {
            if (currActive.is(fistTabindex) && e.shiftKey) {
              ally.element.focus(navBookingNav.find('.booking-nav__item.active')[0]);
              return false;
            }
            return true;
          } else {
            if (e.shiftKey) {
              return true;
            }
          }
          if (fistTabindex.length === 0) {
            fistTabindex = bookingNav.siblings('.passenger-detail').find('[tabindex], button, a').not('.hidden');
          }
          fistTabindex.length && ally.element.focus(fistTabindex[0]);
          return;
        // case 9:
        //  // Tab handle
        //   var fistTabindex = bookingNav.parents('.wrap-select-meals').find('.form--select-meals').find('[tabindex]').eq(0);
        //   // Code for focusing on generic
        //   if (e.shiftKey) {
        //     return true;
        //   }
        //   if ($('.seatmap.seatmap--generic').length) { //Handle Case it seatmap available
        //     var seatMap = $('.seatmap.seatmap--generic');
        //     var visibleSelect = seatMap.find('[data-customselect]:visible').first(); //Find the first focusable custom select
        //     if (visibleSelect.length) { //Focus on the select
        //      visibleSelect.click();
        //      return false;
        //     } else { //Focus on the button
        //      ally.element.focus(seatMap.parent('.form-seatmap').find('.button-group-1').find('input:visible').last()[0]);
        //      return false;
        //     }
        //   }
        //   if (currActive.parents('.form--select-meals').length) {
        //     if (currActive.is(fistTabindex) && e.shiftKey) {
        //       ally.element.focus(navBookingNav.find('.booking-nav__item.active')[0]);
        //       return false;
        //     }
        //     return true;
        //   } else {
        //     if (e.shiftKey) {
        //       return true;
        //     }
        //   }
        //   if (fistTabindex.length === 0) {
        //     fistTabindex = bookingNav.siblings('.passenger-detail').find('[tabindex], button, a').not('.hidden');
        //   }
        //   ally.element.focus(fistTabindex[0]);
        //   return false;
        case 36:
          if (currActive.parents('.form--select-meals').length) {
            return true;
          }
          var targetEle = listTabs.eq(0);
          if (targetEle.length > 0) {
            ally.element.focus(targetEle[0]);
            targetEle.trigger('click');
          }
          return false;

        case 35:
          if (currActive.parents('.form--select-meals').length) {
            return true;
          }
          var targetEle = listTabs.eq(listTabs.length - 1);
          if (targetEle.length) {
            ally.element.focus(targetEle[0]);
          }
          targetEle.trigger('click');
          return false;
      }
    });
  }
};
