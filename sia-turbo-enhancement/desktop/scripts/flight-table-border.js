/**
 * @name SIA
 * @description Define condition to add border to specific collumn
 * @version 1.0
 */

SIA.flightTableBorder = function(){
	var flightTable = $('[data-flight]');
	// var radioEls = flightTable.find('input[type="radio"]');

	// setTimeout(function() {
	// 	var td = radioEls.filter(':checked').closest('td');
	// 	td.addClass('active');
	// 	td.eq(0).removeClass('active');
	// }, 2500);

	flightTable.each(function() {
		var radioEls = $(this).find('input[type="radio"]');
		radioEls.off('change.flightTableBorder').on('change.flightTableBorder', function(){
			var el = $(this);
			var td = el.closest('td');
			if (el.prop('checked')) {
				radioEls.closest('td').removeClass('active');
				td.addClass('active');
			}
			else {
				td.removeClass('active');
			}
		});
	});
};

