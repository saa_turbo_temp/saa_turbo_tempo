/**
 * @name SIA
 * @description sqc-expenditure
 * @version 1.0
 */
SIA.SQCExpenditure = function() {
	var tableEl = $('[data-table-report]'),
			btnSeemoreEl = $('[data-see-more]'),
			linkSort = $('[data-sortable]'),
			lastShowItemIdx = 0;


	var sortItems = function(listItemEl, colIndex, sortType) {
		return listItemEl.sort(function(a, b) {
			var valA = $(a).find('td:nth-child(' + colIndex + ') [data-sort-value]').data('sortValue'),
					valB = $(b).find('td:nth-child(' + colIndex + ') [data-sort-value]').data('sortValue');
			if(sortType === 'DESC') {
				return valA < valB ? 1 : -1;
			}
			return valA >= valB ? 1 : -1;
		});
	};

	btnSeemoreEl.off('click.messageSeeMore').on('click.messageSeeMore', function(e) {
		e.preventDefault();
		if(!$(this).hasClass('hidden')) {
			var hiddenEl = tableEl.find('tr.hidden');

			hiddenEl.removeClass('hidden');

			tableEl.find('tr').eq(lastShowItemIdx + 1).focus();
			wcagSeeMore(hiddenEl.length);

			btnSeemoreEl.addClass('hidden');

		}
	});

	linkSort.off('click.sortable').on('click.sortable', function(e) {
		e.preventDefault();
		var el = $(this),
				bodyTable = tableEl.find('tbody'),
				listItemEl = bodyTable.find('tr').not(':hidden'),
				colIndex = el.closest('th').index() + 1,
				sortType = el.data('sortType') || 'DESC';

		if(listItemEl.length && colIndex !== -1) {
			listItemEl = sortItems(listItemEl.detach(), colIndex, sortType);
			bodyTable.prepend(listItemEl);
			if(sortType === 'DESC') {
				el.removeClass('active').data('sortType', 'ASC');
			} else {
				el.addClass('active').data('sortType', 'DESC');
			}
		}
	});

	var wcagSeeMore = function(count) {
		var status = btnSeemoreEl.parent('.expenditure').find('#wcag-seemore');

		status.text('');
		status.text(L10n.wcag.seemoreLabel.format(count));

	};

	var wcag = function() {
		var bodyTable = tableEl.find('tbody'),
				listEl = bodyTable.find('tr'),
				listShowEl = bodyTable.find('tr').not(':hidden');

		lastShowItemIdx = listShowEl.length;

		if(btnSeemoreEl.parents('.expenditure').find('#wcag-seemore').length <= 0) {
			btnSeemoreEl.parents('.expenditure').append('<span id="wcag-seemore" aria-live="assertive" aria-atomic="true" class="ui-helper-hidden-accessible"></span>');
		}

		listEl.each(function(bookingIdx, e){
			var trEl = $(e),
				desc = trEl.find('.desc'),
				country = trEl.find('td').eq(1).find('[data-sort-value]')

			trEl.attr({
				'tabindex' : 0
			});

			desc.attr({
				'tabindex': 0
			});

			country.attr({
				'tabindex': 0
			});

		});
	};

	wcag();
};
