/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */

SIA.flightStatus = function(){
	if(!$('.flight-status-page').length){
		return;
	}
	var global = SIA.global;
	var config = global.config;
	var formSearchByFlightNumber = $('[data-search-number]');
	var formSearchByRoute = $('[data-search-route]');
	var noJsonTemplate = '<div class="alert-block checkin-alert error-alert">' +
					              '<div class="inner">' +
					                '<div class="alert__icon"><em class="ico-close-round-fill"></em></div>' +
					                '<div class="alert__message">' + L10n.flightStatus.nodata + '</div>'+
					              '</div>' +
					            '</div>';
	// var formPost = false;

	var prePopulate = function() {
		if(globalJson.searchFlightParameters) {
			var radioType = formSearchByFlightNumber.find('[data-option] input[type="radio"]');
			if(globalJson.searchFlightParameters.from) {
				radioType.eq(0).prop('checked', true);
			}
			else {
				radioType.eq(1).prop('checked', true);
			}
			radioType.trigger('change');

			formSearchByFlightNumber
			.find('[data-departure-date]')
			.val(globalJson.searchFlightParameters.date)
			.parent().customSelect('refresh');

			formSearchByFlightNumber
			.find('[data-carrier-code]')
			.val(globalJson.searchFlightParameters.carrierCode)
			.parent().customSelect('refresh');

			formSearchByFlightNumber
			.find('[data-flight-number]')
			.val(globalJson.searchFlightParameters.flightNumber);

			var cityFrom = formSearchByFlightNumber
			.find('#selectDeparting [data-autocomplete] select');

			var cityTo = formSearchByFlightNumber
			.find('#selectArriving [data-autocomplete] select');

			if(globalJson.searchFlightParameters.from) {
				cityFrom.val(globalJson.searchFlightParameters.from);

				formSearchByFlightNumber
				.find('#selectDeparting [data-autocomplete] input')
				.val(cityFrom.find(':selected').data('text'))
				.closest('[data-autocomplete]')
				.removeClass('default');
			}
			else {
				cityTo.val(globalJson.searchFlightParameters.to);

				formSearchByFlightNumber
				.find('#selectArriving [data-autocomplete] input')
				.val(cityTo.find(':selected').data('text'))
				.closest('[data-autocomplete]')
				.removeClass('default');
			}

		}

		if(globalJson.searchRouteParameter) {
			var routeFrom = formSearchByRoute.find('.city-from [data-autocomplete] select');
			if(globalJson.searchRouteParameter.from) {
				routeFrom.val(globalJson.searchRouteParameter.from);

				formSearchByRoute
				.find('.city-from [data-autocomplete] input')
				.val(routeFrom.children(':selected').data('text'))
				.closest('[data-autocomplete]')
				.removeClass('default');
			}

			var routeTo = formSearchByRoute.find('.city-to [data-autocomplete] select');
			if(globalJson.searchRouteParameter.to) {
				routeTo.val(globalJson.searchRouteParameter.to);

				formSearchByRoute
				.find('.city-to [data-autocomplete] input')
				.val(routeTo.children(':selected').data('text'))
				.closest('[data-autocomplete]')
				.removeClass('default');
			}

			formSearchByRoute
			.find('[data-departure-date]')
			.val(globalJson.searchRouteParameter.date)
			.parent().customSelect('refresh');
		}
	};

	prePopulate();

	// var setHeightFlightAlert = function(){
	// 	var flightAlert = $('.flight-alert');

	// 	flightAlert.each(function(){
	// 		var wrapper = $('.wrapper', $(this));
	// 		wrapper.css('height', wrapper.find(':first-child').outerHeight(true));
	// 	});
	// };

	// var flightStatusTabPopup = function(hdlers, ppup, ppupContents, closePpup, setHeight){
	// 	var handlers = hdlers,
	// 		// popup = ppup.addClass('flight-status'),
	// 		popup = ppup,
	// 		popupContents = $(ppupContents, popup);

	// 	if(handlers.length && popup.length){
	// 		handlers.each(function(i){
	// 			global.vars.popupGesture(popup, handlers, closePpup, (ppupContents + ':eq(' + i + ')'), true, function(t){
	// 				popupContents.removeClass('active').eq(handlers.index(t)).addClass('active');
	// 				if(setHeight){
	// 					setHeightFlightAlert();
	// 				}
	// 			});
	// 		});
	// 	}
	// };

	var fillContent = function(res) {
		var flightStatusDetails = $('[data-result]');
		if(flightStatusDetails.length){
			$.get(config.url.flightStatusTemplate, function (data) {
				var template = window._.template(data, {
					data: res
				});

				flightStatusDetails.html(template);

			}, 'html');
		}
	};

	if(globalJson.flightStatusResult) {
		fillContent(globalJson.flightStatusResult);
		//Set active tab
		var tab = $('.flight-status[data-tab]');
		tab.find('.tab-item, .tab-content').removeClass('active');
		if((/Flight|Number/g).test(globalJson.flightStatusResult.heading.flight)) {
			tab.find('.tab-item').eq(0).addClass('active');
			tab.find('.tab-content').eq(0).addClass('active');
		}
		else {
			tab.find('.tab-item').eq(1).addClass('active');
			tab.find('.tab-content').eq(1).addClass('active');
		}
	} else {
		var flightStatusDetails = $('[data-result]');
		window.noJsonHandler = true;
		flightStatusDetails.html(noJsonTemplate);
	}

	// var ajaxSuccess = function(res) {
	// 	fillContent(res);
	// };

	// var ajaxFail = function() {
	// 	var flightStatusDetails = $('[data-result]');
	// 	flightStatusDetails.html('<div class="blk-heading"><h2>' + L10n.emptyData + '</h2></div>');
	// };

	// var appendLoading = function() {
	// 	//append loading
	// 	var flightStatusDetails = $('[data-result]');
	// 	flightStatusDetails.html(config.template.loadingMedium);
	// };

	// var submitSearchFlight = function() {
	// 	var form = formSearchByFlightNumber;
	// 	if(formPost) {
	// 		return true;
	// 	}
	// 	else {
	// 		appendLoading();
	// 		$.ajax({
	// 			url: config.url.flightStatusJSON,
	// 			type: global.config.ajaxMethod,
	// 			dataType: 'json',
	// 			data: {
	// 				requestType: 'Number',
	// 				flightNumber: form.find('[data-flight-number]').val(),
	// 				carrierCode: form.find('[data-carrier-code]').val(),
	// 				departureDate: form.find('[data-departure-date]').val(),
	// 				originAirport: form.find('[data-option] input:checked').val() === 'departing' ? form.find('#selectDeparting [data-autocomplete] input').val() : '',
	// 				destinationAirport: form.find('[data-option] input:checked').val() === 'arriving' ? form.find('#selectArriving [data-autocomplete] input').val() : ''
	// 			},
	// 			success: ajaxSuccess,
	// 			error: ajaxFail
	// 		});
	// 		return false;
	// 	}
	// };

	formSearchByFlightNumber.validate({
		// submitHandler: function() {
		// 	// return submitSearchFlight();
		// 	return true;
		// },
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});

	// var submitSearchRoute = function() {
	// 	var form = formSearchByRoute;
	// 	if(formPost) {
	// 		return true;
	// 	}
	// 	else {
	// 		// appendLoading();
	// 		// $.ajax({
	// 		// 	url: config.url.flightStatusJSON,
	// 		// 	type: global.config.ajaxMethod,
	// 		// 	dataType: 'json',
	// 		// 	data: {
	// 		// 		requestType: 'Route',
	// 		// 		departureDate: form.find('[data-departure-date]').val(),
	// 		// 		originAirport: form.find('.city-from [data-autocomplete] input').val(),
	// 		// 		destinationAirport: form.find('.city-to [data-autocomplete] input').val()
	// 		// 	},
	// 		// 	success: ajaxSuccess,
	// 		// 	error: ajaxFail
	// 		// });
	// 		return false;
	// 	}
	// };

	formSearchByRoute.validate({
		// submitHandler: function() {
		// 	// return submitSearchRoute();
		// 	return true;
		// },
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});

	var initSearchByNumber = function() {
		var flightSchedule = formSearchByFlightNumber;
		var option = $('[data-option] input:radio', flightSchedule);
		var target = $('[data-target]', flightSchedule );

		//select depart or arrive
		var _selectFlightBy = function() {
			option.each(function(idx){
				var self = $(this);
				self.off('change.filightScheduleTrip').on('change.filightScheduleTrip', function(){
					if(self.is(':checked')){
						if(idx){
							target.eq(1).removeClass('hidden');
							target.eq(0).addClass('hidden');
							if(flightSchedule.data('validator') && flightSchedule.data('validatedOnce')) {
								flightSchedule.data('validator').element(target.eq(1).find(':input'));
							}
						}
						else{
							target.eq(0).removeClass('hidden');
							target.eq(1).addClass('hidden');
							if(flightSchedule.data('validator') && flightSchedule.data('validatedOnce')) {
								flightSchedule.data('validator').element(target.eq(0).find(':input'));
							}
						}
					}
				});
			}).filter(':checked').trigger('change.filightScheduleTrip');
		};
		_selectFlightBy();
	};
	initSearchByNumber();

	// var formPostOnSwitchingTab = function() {
	// 	var tab = $('.flight-status[data-tab]');
	// 	tab
	// 	.off('afterChange.form-post')
	// 	.on('afterChange.form-post', function() {
	// 		var form = $(this).find('.tab-content.active form');
	// 		formPost = true;
	// 		form.submit();
	// 	});
	// };

	// formPostOnSwitchingTab();
};
