SIA.KFCheckIns = function() {
	var config = SIA.global.config;
	var loadPlaceholder = $('[data-load-placeholder]');

	var loadGlobalJson = function() {
		SIA.preloader.show();
		loadPlaceholder.children('[data-booking-item]').remove();
		$.get(config.url.kfCheckInTemplate, function(html) {
			var template = window._.template(html, {
				data: globalJson.kfCheckIns
			});
			loadPlaceholder.find('.check-ins-content').append($(template));
			SIA.preloader.hide();

			$('.checkin-alert').hide().removeClass('hidden').delay(1000).fadeIn(400);
		});
	};

	loadGlobalJson();
};