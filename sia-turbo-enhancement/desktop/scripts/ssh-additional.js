/**
 * @name SIA
 * @description Define global sshAdditional functions
 * @version 1.0
 */
SIA.sshAdditional = function(){
	// var global = SIA.global;
	// var config = global.config;
	var addGuestEl = $('[data-additional-guest]');
	var addBreakfastEl = $('[data-additional-breakfast]');
	var accounting = window.accounting;
	var airportCheckbox = $('[data-addon-for="airport-transfer"]').find('input[type="checkbox"]');
	var breakfastCheckbox = $('[data-addon-for="breakfast"]').find('input[type="checkbox"]');
	var wheelchairCheckbox = $('[data-addon-for="wheelchair-transfer"]').find('input[type="checkbox"]');
	var roomChargeVal = $('[data-room-charge]').data('value');

	var addOnAirport = $('[data-addon-type="airport-transfer"]');
	var addOnWheelchair = $('[data-addon-type="wheelchair-transfer"]');
	var addOnBreakfast = $('[data-addon-type="breakfast"]');

	airportCheckbox.off('change.sshAdditional').on('change.sshAdditional', function(){
		var el = $(this);
		var airportVal;
		var breakfastVal = breakfastCheckbox.is(':checked') ? breakfastCheckbox.parent().data('value') : 0;
		var wheelchairVal = wheelchairCheckbox.is(':checked') ? wheelchairCheckbox.parent().data('value') : 0;
		var textArr = addOnAirport.text().split(' ');
		if(el.is(':checked')){
			airportVal = el.parent().data('value');
			textArr[1] = accounting.formatNumber(airportVal, 2, ',', '.');
		}else{
			airportVal = 0;
			textArr[1] = accounting.formatNumber(airportVal, 2, ',', '.');
		}
		grandTotal(airportVal, breakfastVal, wheelchairVal);
		addOnAirport.text(textArr.join(' '));
	});

	breakfastCheckbox.off('change.sshAdditional').on('change.sshAdditional', function(){
		var el = $(this);
		var breakfastVal;
		var airportVal = airportCheckbox.is(':checked') ? airportCheckbox.parent().data('value') : 0;
		var wheelchairVal = wheelchairCheckbox.is(':checked') ? wheelchairCheckbox.parent().data('value') : 0;
		var textArr = addOnBreakfast.text().split(' ');
		if(el.is(':checked')){
			breakfastVal = el.parent().data('value');
			textArr[1] = accounting.formatNumber(breakfastVal, 2, ',', '.');
		}else{
			breakfastVal = 0;
			textArr[1] = accounting.formatNumber(breakfastVal, 2, ',', '.');
		}
		grandTotal(airportVal, breakfastVal, wheelchairVal);
		addOnBreakfast.text(textArr.join(' '));
	});

	wheelchairCheckbox.off('change.sshAdditional').on('change.sshAdditional', function(){
		var el = $(this);
		var wheelchairVal;
		var airportVal = airportCheckbox.is(':checked') ? airportCheckbox.parent().data('value') : 0;
		var breakfastVal = breakfastCheckbox.is(':checked') ? breakfastCheckbox.parent().data('value') : 0;
		var textArr = addOnWheelchair.text().split(' ');
		if(el.is(':checked')){
			wheelchairVal = el.parent().data('value');
			textArr[1] = accounting.formatNumber(wheelchairVal, 2, ',', '.');
		}else{
			wheelchairVal = 0;
			textArr[1] = accounting.formatNumber(wheelchairVal, 2, ',', '.');
		}
		grandTotal(airportVal, breakfastVal, wheelchairVal);
		addOnWheelchair.text(textArr.join(' '));
	});

	var grandTotal = function(airportVal, breakfastVal, wheelchairVal){
		var totalEl = $('#form--additional-requests').find('.total-info');
		var totalText = totalEl.text().split(' ');
		var totalVal = totalEl.data('addon-grand-total');

		totalVal = airportVal + breakfastVal + wheelchairVal + roomChargeVal;
		totalVal = accounting.formatNumber(totalVal, 2, ',', '.');
		totalText[1] = totalVal;
		totalText = totalText.join(' ');
		totalEl.text(totalText);
	};

	var setAdditional = function() {
		if (globalJson && globalJson.bookingSummary) {
			var json = globalJson.bookingSummary.bookingSummary;
			var adultCount = json.adultCount ? parseInt(json.adultCount) : 0;
			var childCount = json.childCount ? parseInt(json.childCount) : 0;
			var guestCount  = adultCount + childCount;

			var initSelectEl = function(el, count) {
				var html = '',
					i = 0;
				for (i = 1; i <= count; i++) {
					html += '<option value="' + i + '">' + i + '</option>';
				}

				el
					.find('select')
					.empty()
					.append(html)
					.find('option:last-child').prop('selected', true);

				el.customSelect('_createTemplate');
			};

			initSelectEl(addBreakfastEl, guestCount);
			initSelectEl(addGuestEl, guestCount > 4 ? 4 : guestCount);
		}
	};

	var initModule = function(){
		setAdditional();
		SIA.sshAdditional.setAdditional = setAdditional;
	};

	initModule();
};
