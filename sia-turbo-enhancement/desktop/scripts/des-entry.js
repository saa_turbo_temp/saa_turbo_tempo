/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */
SIA.desEntry = function(){
	var global = SIA.global,
		win = global.vars.win,
		config = global.config,
		btnSeeMore = $('.country-button [data-see-more]'),
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startDeskNum = 0,
		startTabNum = 0,
		defaultDeskNum = 15,
		defaultTabNum = 9,
		seeMoreDeskNum = 5,
		seeMoreTabNum = 3,
		seeMoreCount = 0,
		isShuffleActive = false,
		isTablet = window.innerWidth < config.tablet;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var initShuffle = function() {
		var countryItem = container.find('.static-item.col-mb-3:not(.hidden)').eq(0);
		container.shuffle({
			speed: 0,
			itemSelector: '.static-item:not(.hidden)',
			sizer: countryItem,
			supported: false
		});
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg === region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region') === region) &&
					(!city || self.data('city') === city);
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);

		// if (!isTablet){
		// 	startDeskNum = 0;
		// }
		// else {
		// 	startTabNum = 0;
		// }

		// fillContent(false);

		return (!isTablet && (startDeskNum = 0) || (startTabNum = 0)) || fillContent(false);
	};

	var generateTemplate = function(tpl, resLen) {
		if (!isShuffleActive) {
			initShuffle();
			isShuffleActive = true;
		}
		else {
			container.shuffle('appended', tpl);
		}
		preventClick = false;
		changeSeemoreText(resLen);
	};

	var changeSeemoreText = function(resLen) {
		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.addClass('hidden');
			btnSeeMore.text(L10n.kfSeemore.seeMore);

			// if (!isTablet) {
			// 	startDeskNum = resLen;
			// }
			// else {
			// 	startTabNum = resLen;
			// }
			return !isTablet ? (startDeskNum = resLen) : (startTabNum = resLen);

		}
		else {
			btnSeeMore.text(seeMoreCount === 2 ?
				L10n.kfSeemore.seeAll : L10n.kfSeemore.seeMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx, resLen, isReadmore) {
		var showItems = res.filter(':lt(' + endIdx + ')').filter(':visible'),
				tpl = res.filter(':lt(' + endIdx + ')').filter('.hidden'),
				wcagSeemore = container.find('.ui-helper-hidden-accessible.seemore');

		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');


				if (!img.data('loaded')) {
					var newImg = new Image();

					newImg.onload = function() {
						self.removeClass('hidden');
						img.data('loaded', true);

						if (!tpl.filter('.hidden').length) {
							generateTemplate(tpl, resLen);
						}
						img.parent().css({
							'background-image': 'url(' + this.src + ')'
						});
					};

					newImg.src = img.data('img-src');
					img.attr('src', config.imgSrc.transparent);
				}

				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(tpl, resLen);
					}
				}
			});

			if(isReadmore) {

				wcagSeemore.text('');
				wcagSeemore.text(L10n.wcag.seemoreLabel.format(tpl.length));

				setTimeout(function(){
					res.eq(showItems.length).find('.static-item__info').children('a').focus();
				}, 50);

			}

		}
	};

	var fillContent = function(isResize, setNumber, isReadmore, isSearch) {
		if (staticItems.length) {
			var	endIdx = 0;
			var resLen = staticItems.length;

			if (isResize) {
				generateClass(staticItems);
			}

			if (!isTablet) {
				endIdx = !startDeskNum ?
					(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 5 : startDeskNum + defaultDeskNum)) :
					(seeMoreCount > 2 ? resLen : startDeskNum + seeMoreDeskNum);
				startDeskNum = endIdx;
			}
			else {
				endIdx = !startTabNum ?
					(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 3 : startTabNum + defaultTabNum)) :
					(seeMoreCount > 2 ? resLen : startTabNum + seeMoreTabNum);
				startTabNum = endIdx;
			}

			getTemplate(staticItems, endIdx, resLen, isReadmore);

		}
	};

	var generateClass = function(res) {
		var wcagUpdate = container.find('.ui-helper-hidden-accessible.update');

		resetClass(res.removeAttr('style'), 'static-item--large col-mb-6', 'hidden col-mb-3');
		if (!isTablet) {
			generateClassForDesktop(res);
		}
		else {
			generateClassForTablet(res);
		}


		wcagUpdate.text(L10n.wcag.foundLabel.format(res.length));
		setTimeout(function(){
			wcagUpdate.text('');
		}, 100);

		// return (!isTablet && generateClassForDesktop(res)) || generateClassForTablet(res);
	};

	var resetClass = function(elm, rClass, aClass) {
		elm
			.removeClass(rClass)
			.addClass(aClass);
	};

	var generateClassForDesktop = function(res) {
		var i = -6, temp = 0;
		do {
			if (temp <= 2) {
				i += 6;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 3;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	var generateClassForTablet = function(res) {
		var i = -4, temp = 0;
		do {
			if (temp <= 1) {
				i += 4;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 2;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	win.off('resize.template').on('resize.template', function() {
		if(window.innerWidth >= config.tablet){
			if(isTablet) {
				var setNumber = Math.ceil(startTabNum / 3);
				isTablet = false;
				startDeskNum = 0;
				fillContent(true, setNumber);
			}
		}
		else {
			if (!isTablet) {
				var setNumber = Math.ceil(startDeskNum / 5);
				isTablet = true;
				startTabNum = 0;
				fillContent(true, setNumber);
			}
		}
	});

	btnSeeMore.off('click.template').on('click.template', function(e) {
		e.stopPropagation();
		e.preventDefault();
		if (!preventClick) {
			seeMoreCount++;
			fillContent(false, null, true);
		}
	});

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		if (!preventClick) {
			preventClick = true;
			seeMoreCount = 0;
			var region = regionInput.val();
			cityValue = cityInput.val();
			renderTemplate(region, cityValue);
		}
	});

	var wcag = function() {

		if($('#aria-seemore').length === 0) {
			container.prepend('<span class="ui-helper-hidden-accessible seemore" id="aria-seemore" aria-live="assertive" aria-atomic="true"></span>')
		};
		if($('#aria-update').length === 0) {
			container.prepend('<span class="ui-helper-hidden-accessible update" id="aria-update" aria-live="assertive" aria-atomic="true"></span>')
		};

	};

	var initModule = function() {
		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);

		wcag();
	};

	initModule();
};
