/**
 * @name SIA
 * @description Define global flightScheduleTrip functions
 * @version 1.0
 */
SIA.flightScheduleTrip = function(){
	var global = SIA.global;
	var container = global.vars.container;
	var win = global.vars.win;
	var config = SIA.global.config;
	var flightSchedules = $('[data-flight-schedule]');
	var flightSearchSwitch = $('[data-search-flights]');

	flightSearchSwitch
		.off('change.switch-form')
		.on('change.switch-form', function() {
			var form = $(this).data('search-flights');
			$('[data-search-flights-form]')
				.removeClass('active')
				.filter('[data-search-flights-form="' + form + '"]')
				.addClass('active');
		})
		.filter(':checked').trigger('change.switch-form');

	flightSchedules.each(function(){
		var flightSchedule = $(this);
		var option = $('[data-option] input:radio', flightSchedule);
		var target = $('[data-target]', flightSchedule );
		var wp = flightSchedule.closest('.animated');
		var dataOnewayInput = flightSchedules.filter('.form-search-flight').find('[data-oneway]');
		var dataReturnInputs = flightSchedules.filter('.form-search-flight').find('[data-return-flight] input:text');

		var checkValueInput = function(input){
			if(input && input.val() !== ''){
				input.closest('div.input-3:visible').removeClass('default');
			}
		};
		//select depart or arrive
		var _selectFlightBy = function() {
			option.each(function(idx){
				var self = $(this),
						timerChange;

				self.off('change.filightScheduleTrip').on('change.filightScheduleTrip', function(){
					if(self.is(':checked')){
						clearTimeout(timerChange);
						timerChange = setTimeout(function() { $('#ui-datepicker-div').hide(); }, 100);

						if(idx){
							target.eq(1).removeClass('hidden');
							target.eq(0).addClass('hidden');
							checkValueInput(dataOnewayInput);
							// if(flightSchedule.data('validator') && flightSchedule.data('validatedOnce')) {
							// 	flightSchedule.data('validator').element(target.eq(1).find(':input'));
							// }
						}
						else{
							target.eq(0).removeClass('hidden');
							target.eq(1).addClass('hidden');
							dataReturnInputs.each(function(){
								checkValueInput($(this));
							});
							// if(flightSchedule.data('validator') && flightSchedule.data('validatedOnce')) {
							// 	flightSchedule.data('validator').element(target.eq(0).find(':input'));
							// }

							var datepickerDepart = target.eq(0).find('[data-start-date]');
							var datepickerReturn = target.eq(0).find('[data-return-date]');

							if(datepickerDepart.length && datepickerReturn.length) {
								var dateDepart = datepickerDepart.datepicker('getDate');
								var dateReturn = datepickerReturn.datepicker('getDate');
								if(dateDepart && dateReturn && dateDepart > dateReturn) {
									datepickerDepart.val('').focus();
									datepickerReturn.val('');
								}
							}
						}
						wp = flightSchedule.closest('.animated');
						wp.css('height', '');
						if(win.width() < config.mobile && wp.length){
							if(win.height() > wp.height()){
								wp.height(win.height());
								container.height(win.height());
							}
							else{
								wp.height(wp.height());
								container.height(wp.height());
							}
						}
					}
				});
			}).filter(':checked').trigger('change.filightScheduleTrip');
		};
		_selectFlightBy();
		//check have value or not in init.
		checkValueInput(dataOnewayInput);
		dataReturnInputs.each(function(){
			checkValueInput($(this));
		});

		//Validation of Flight Schedules form
		if(!flightSchedule.data('validator') && flightSchedule.is('form')) {
			flightSchedule.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess
			});
		}
		else{
			if(!flightSchedule.find('form').data('validator')){
				flightSchedule.find('form').each(function() {
					$(this).validate({
						focusInvalid: true,
						errorPlacement: global.vars.validateErrorPlacement,
						success: global.vars.validateSuccess
					});
				});
				// flightSchedule.find('form').validate({
				// 	focusInvalid: true,
				// 	errorPlacement: global.vars.validateErrorPlacement,
				// 	success: global.vars.validateSuccess
				// });
			}
		}

		flightSchedule.find('[data-form-action]').click(function(e) {
			e.preventDefault();
			var form = $(this).closest('form');
			if(form.valid()) {
				form.attr('action', $(this).data('form-action')).submit();
			}
			else{
				form.submit();
			}
		});
	});
};
