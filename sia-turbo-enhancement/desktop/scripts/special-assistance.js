/**
 * @name SIA
 * @description Define global specialAssistance functions
 * @version 1.0
 */
SIA.specialAssistance = function() {
	// var disabledValueEls = $('[data-disable-value]');
	// var vars = SIA.global.vars;
	var requiredChbGroup = $('[data-rule-chbsrequired]');

	requiredChbGroup.off('change.validateChb').on('change.validateChb', function() {
		$(this).valid();
	});

	$.validator.addMethod('chbsRequired', function(value, el, param) {
		var chbs = $(param);
		return chbs.length && chbs.is(':checked');
	}, L10n.validator.selectOneOpt);

	/* jshint ignore:start */

// ------------------------ BEGIN UPLOAD FILE MODULE ---------------------------

	var totalSize = 17;
	var totalFileSize = 0;
	var leftSize = totalSize;
	var imageArray = [];
	var mbRate = 1048576;
	var attachList = $('.list-attachment');
	var attachEl = $('.attachment');
	var attachError = attachEl.find('.text-error-1');
	var chooseFileEl = $('.choose-file');
	var cloneChooseFileEl = chooseFileEl.clone();
	var isSupportHTML5 = !!window.FileReader;
	var imgFileTpl = '<img style="width: 84px; height: 84px;" class="attached-image" src="{0}" alt="" />';
	var otherFileTpl = '<div class="icon-wrapper"><em class="{0}">&nbsp;</em></div>';
	var unsupportHtml5Tpl = '<li>{0}<a href="#"> delete</a></li>';
	var errorTpl = '<p class="text-error">{0}</p>';
	var pattern = /(jpg|jpeg|png|gif|tif|tiff|doc|docx|pdf|txt|zip)$/i;

	var attachTpl = '<li>{4}<a href="#" class="attachment-close"><input type="hidden" class="ret-file-size" value="{0}"/><input type="hidden" class="ret-file-name" value="{1}"/><em class="ico-close-rounded"></em></a>{2}<p class="chose-img-name">{3}</p></li>';

	var uploadError = {
		'notSupport': 'Not supported file format.',
		'fileExceed': 'File size exceeded.',
		'totalExceed': '17MB exceeded.',
		'fileExists': 'File name already exists.',
		'notSupportIe10': 'Not supported file format on ie 10 and below.'
	};

	var removeFileName = function(imageArray, removeItem) {
		return jQuery.grep(imageArray, function(value) {
			return value !== removeItem;
		});
	};

	var checkDuplicate = function(imageArray, files) {
		var duplicate = '';
		for(var i = 0, len = imageArray.length; i < len - 1; i++){
			if(imageArray[i] === files[0].name){
				duplicate = 'yes';
				break;
			}
		}
		return duplicate;
	};

	var renderFileSuccess = function(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl) {
		attachList.prepend($(attachTpl.format(self.files[j].size, self.files[j].name, imgTpl, self.files[j].name, fileIcon)).append(currentChooseFileEl.find('input:file').addClass('hidden').data('choosed-file', true)));

		chooseFileEl.remove();
		attachList.append(cloneChooseFileEl);

		totalFileSize = totalFileSize + filesize;
		leftSize = totalSize - totalFileSize;

		chooseFileEl = $('.choose-file:not([data-choosed-file])').find('.chose-img-name').text(Math.round(leftSize) + ' MB left').end();
	};

	var renderFileError = function(fileName, currentChooseFileEl, err) {
		imageArray = removeFileName(imageArray, fileName);
		currentChooseFileEl.val('');
		attachError = $(errorTpl.format(err)).insertAfter(attachList);
	};

	chooseFileEl.find('.chose-img-name').text(Math.round(leftSize) + 'MB left');
	// $('[data-support-html5="' + isSupportHTML5 + '"]').removeClass('hidden');
	attachError.empty().removeClass('hidden');

	attachEl
		.off('click.removeFile')
		.on('click.removeFile', 'a, .attachment-close', function(e) {
			e.preventDefault();
			$(this).parent().remove();
			attachError.remove();

			if (isSupportHTML5) {
				var removeItem = $(this).find('.ret-file-name').val();
				var thisSize = $(this).find('.ret-file-size').val()/1048576;

				imageArray = removeFileName(imageArray, removeItem);
				totalFileSize = totalFileSize - thisSize;
				leftSize = totalSize - totalFileSize;

				chooseFileEl.find('.chose-img-name').text(Math.round(leftSize) + 'MB left');
				$(this).find('.ret-file-size').val(0);
			}
			else {
				chooseFileEl.find('input:file').val('');
				chooseFileEl.removeClass('hidden');
			}
		})
		.off('change.chooseFile')
		.on('change.chooseFile', 'input:file', function() {
			var currentChooseFileEl = $(this);
			var files = this.files;

			if (files && files.length) {
				attachError.remove();

				if (isSupportHTML5) {
					imageArray.push(files[0].name);
					var duplicate = checkDuplicate(imageArray, files);

					for (var i = 0, len = files.length; i < len; i++) {
						var file = files[i];
						var fileName = file.name;
						var fileExtension = fileName.split('.').pop().toLowerCase();
						var filesize = 0;

						if(!duplicate){
							filesize = file.size/mbRate;

							if(leftSize >= filesize){
								if (!pattern.test(fileExtension)) {
									renderFileError(fileName, currentChooseFileEl, uploadError.notSupport);
								}
								else if (filesize > 3) {
									renderFileError(fileName, currentChooseFileEl, uploadError.fileExceed);
								}
								else {
									(function (j, self, extension) {
										var reader = new FileReader();

										reader.onload = function (e) {
											cloneChooseFileEl = chooseFileEl.clone();

											var fileIcon = '';
											var imgTpl = '';

											if (fileExtension !== 'tiff' && fileExtension !== 'tif') {
												fileIcon = extension === 'pdf' ?
													otherFileTpl.format('ico-pdf-2') :
													((extension === 'doc' || extension === 'docx' || extension === 'txt') ?
														otherFileTpl.format('ico-document') : ((extension === 'zip') ? otherFileTpl.format('ico-zip') : ''));

												imgTpl = (extension === 'png' || extension === 'jpg' || extension === 'gif' || extension === 'jpeg') ? imgFileTpl.format(e.target.result) : '';

												renderFileSuccess(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl);
											}
											else {
												if (SIA.global.vars.ieVersion() !== -1 && SIA.global.vars.ieVersion() <= 10) {
													renderFileError(fileName, currentChooseFileEl, uploadError.notSupportIe10);
												}
												else {
													Tiff.initialize({
														TOTAL_MEMORY: 300 * 1024 * 1024
													});

													var tiff = new Tiff({
														buffer: e.target.result
													});

													fileIcon = '';
													imgTpl = imgFileTpl.format(tiff.toDataURL('image/png'));

													renderFileSuccess(self, j, filesize, fileIcon, imgTpl, cloneChooseFileEl, currentChooseFileEl);
												}
											}
										};

										if (fileExtension === 'tiff' || fileExtension === 'tif') {
											reader.readAsArrayBuffer(file);
										}
										else {
											reader.readAsDataURL(file);
										}

									})(i, this, fileExtension);
								}
							}
							else {
								renderFileError(fileName, currentChooseFileEl, uploadError.totalExceed);
							}
						}
						else {
							renderFileError(fileName, currentChooseFileEl, uploadError.fileExists);
						}
					}
				}
				else {
					var fileName = files[0].name;
					var fileExtension = fileName.split('.').pop().toLowerCase();

					if (pattern.test(fileExtension)) {
						attachList.prepend(unsupportHtml5Tpl.format(fileName));
						chooseFileEl.addClass('hidden');
					}
					else {
						attachError = $(errorTpl.format('Not supported file format.')).insertAfter(attachList);
						this.value = '';
					}
				}
			}
		})
		.off('mousedown.chooseFile')
		.on('mousedown.chooseFile', 'input:file', function() {
			if (SIA.global.vars.ieVersion() === 10) {
				$(this).trigger('click');
			}
		});

// ------------------------ END UPLOAD FILE MODULE -----------------------------

/* jshint ignore:end */

};
