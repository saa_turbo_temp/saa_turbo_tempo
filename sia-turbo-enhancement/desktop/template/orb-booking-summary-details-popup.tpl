<% var scenario = data.bookingSummary.scenario; %>
<% var currency = scenario === 'ORU' ? (data.oruUpgradeJsonVO.currencyCode ? data.oruUpgradeJsonVO.currencyCode : '') : data.bookingSummary.currency; %>

<% if(!confirmationPage) { %>
	<h2 data-anchor="flights" class="popup__heading">Flight details</h2>
	<div class="flights-target flights-target--2">
		<% _.each(data.bookingSummary.flight, function(flight, flightIdx) { %>
			<h3 class="title-1">
				<%= (flightIdx + 1) %>. <%= flight.origin %> to <%= flight.destination %>
			</h3>
			<div class="flights-target__content">
				<div class="flights__info--group">
					<% _.each(flight.flightSegments, function(segment, segmentIdx) { %>
						<div class="flights__info none-border">
							<div class="flights--detail left"
								data-flight-number="<%= segment.flightNumber %>"
								data-carrier-code="<%= segment.carrierCode %>"
								data-date="<%= segment.deparure.date %>"
								data-origin="<%= segment.deparure.airportCode %>">
								<span>Flight <%= segment.carrierCode %> <%= segment.flightNumber %>
									<em class="ico-point-d"></em>
									<span class="loading loading--small hidden">Loading...</span>
								</span>
								<% if (segment.airCraftType && flight.totalTravelTime) { %>
									<div class="details hidden">
										<p>Aircraft type: <%= segment.airCraftType %></p>
										<p>Flying time: <%= flight.totalTravelTime %></p>
									</div>
								<% } else { %>
									<div class="details hidden"></div>
								<% } %>
							</div><span class="class-flight"><%= segment.cabinClassDesc %></span>
						</div>

						<div class="flights__info">
							<div class="flights__info--detail">
								<em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
								<span class="hour">
									<%= segment.deparure.airportCode %> <%= segment.deparure.time %>
								</span>
								<span class="country-name">
									<%= segment.deparure.cityName %>
								</span>
								<span class="date">
                  <%= segment.deparure.date %>,<br> <%= segment.deparure.airportName %>
                  <span class="terminal">
                    <% if(segment.deparure.terminalName) { %>
                      <%= segment.deparure.terminalName %>
                    <% } %>
                    <%= segment.deparure.terminal %>
                  </span>
                </span>
							</div>
							<div class="flights__info--detail">
								<span class="hour">
									<%= segment.arrival.airportCode %> <%= segment.arrival.time %>
								</span>
								<span class="country-name">
									<%= segment.arrival.cityName %>
								</span>
								<span class="date">
                  <%= segment.arrival.date %>,<br> <%= segment.arrival.airportName %>
                  <span class="terminal">
                    <% if(segment.arrival.terminalName) { %>
                      <%= segment.arrival.terminalName %>
                    <% } %>
                    <%= segment.arrival.terminal %>
                  </span>
                </span>
							</div>
							<p class="operated"><%= L10n.oparated %></p>
						</div>

						<% if(segment.layoverTime) { %>
							<div class="flights__info"><span>Layover time: <%=segment.layoverTime %></span></div>
						<% } %>

					<% }); %>
					<div class="flights__info none-border"><span>Total travel time: <%= flight.totalTravelTime %></span></div>
				</div>
			</div>
		<% }); %>
	</div>

	<%
    var renderSeat = function(detail, seat){
      print('<div class="booking-details booking-details--2">' +
        '<div class="booking-col col-1">' +
          '<em class="ico-change-seat"></em>' +
        '</div>' +
        '<div class="booking-col col-2">' + L10n.bookingSummary.texts.seat + '</div>' +
        '<div class="booking-col col-3">' +
          '<div class="align-wrapper">' +
            '<div class="align-inner">' +
              '<div class="has-cols">' +
                '<p class="target-info">' + detail.from + ' to ' + detail.to + '</p>' +
                '<p class="seat-info">' + seat.description + '</p>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>');
    };
    var renderBags = function(bags){
      var bagHtml = '';
      bagHtml += '<div class="booking-details booking-details--2">' +
        '<div class="booking-col col-1">' +
          '<em class="ico-business-1"></em>' +
        '</div>' +
        '<div class="booking-col col-2">' + L10n.bookingSummary.texts.baggage + '</div>' +
        '<div class="booking-col col-3">' +
          '<div class="align-wrapper">' +
            '<div class="align-inner">';

      for(var i = 0; i < bags.length; i++) {
        bagHtml += '<p>' + bags[i].description + '</p>';
      }

      bagHtml += '</div></div></div></div>';
      print(bagHtml);
    };
    var renderSubHeading = function(detail) {
      _.each(data.bookingSummary.flight, function(f, fIdx) {
        _.each(f.flightSegments, function(s, sIdx) {
          if(s.deparure.airportCode == detail.from && s.arrival.airportCode == detail.to) {
            print('<strong>' + s.carrierCode + ' ' + s.flightNumber + '</strong> - ' + s.deparure.cityName + ' to ' + s.arrival.cityName);
          }
        });
      });
    };
  %>

	<h2 class="popup__heading"><%-L10n.bookingSummary.texts.passengers%></h2>
	<div class="flights-target">
		<% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
			<% if(pax.detailsPerFlight) { %>
				<%var arrBags = [];%>
				<div class="booking-info--2">
          <h3 class="sub-heading-3--dark"><%= (paxIdx + 1) %>. <%= pax.paxName %></h3>
					<% _.each(pax.detailsPerFlight, function(detail, detailIndex) { %>
            <div class="booking-info--2__item">
              <h4 class="sub-heading">
                <span>
                  <%renderSubHeading(detail);%>
                </span>
              </h4>
              <div class="booking-details__group">
                <%if(detail.addonPerPax && detail.addonPerPax.length){%>
                  <%arrBags = [];%>
                  <%_.each(detail.addonPerPax, function(addOn, adIdx){%>
                    <%addOn.type === 'Seat' ? (renderSeat(detail, addOn)) :
                      addOn.type === 'Excess baggage' ? (arrBags.push(addOn)) : ''%>
                  <%})%>
                  <%renderBags(arrBags);%>
                <%}%>
              </div>
            </div>
					<% }); %>
				</div>
			<% } %>
		<% }); %>
	</div>
<% } %>

<% if (scenario !== 'ORU' && scenario !== 'Cancel') { %>
	<h2 data-anchor="cost" class="popup__heading">Cost breakdown</h2>
	<% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
		<div class="cost-breakdown">
			<h3 class="title-1">Passenger <%= (paxIdx + 1) %> - <%= pax.paxName %></h3>
			<table class="table-allocation table-allocation--cost">
				<thead>
					<tr>
						<th class="first"><span class="text-bold">Fare</span></th>
						<th class="second">&nbsp;</th>
						<th class="third">Cost in <%= currency %></th>
					</tr>
				</thead>
				<tbody>
					<% if (pax.fareDetails) { %>
						<% _.each(pax.fareDetails.milesAllocation, function(ma, maIdx) { %>
							<tr class="<%= (maIdx == pax.fareDetails.milesAllocation.length - 1) ? 'type-1' : '' %>">
								<td>
									<span><%= ma.category %></span>
									<ul class="list-allocation visible-mb">
										<li>
											<span>KrisFlyer miles</span>
											<% if(ma.milesAllocated) { %>
												<span data-need-format="0">
													<%= ma.milesAllocated %>
												<span>
											<% } else { %>
												<span>-</span>
											<% } %>
										</li>
										<li>
											<span><%= currency %></span>
											<% if(ma.totalAmount) { %>
												<span data-need-format="2">
													<%= ma.totalAmount %>
												</span>
											<% } else { %>
												<span>-</span>
											<% } %>
										</li>
									</ul>
								</td>

								<% if(ma.milesAllocated) { %>
									<td data-need-format="0">
										<%= ma.milesAllocated %>
									</td>
								<% } else { %>
									<td>&nbsp;</td>
								<% } %>

								<% if(ma.totalAmount) { %>
									<td data-need-format="2">
										<%= ma.totalAmount %>
									</td>
								<% } else { %>
									<td>-</td>
								<% } %>

							</tr>
						<% }); %>
						<tr>
							<td><span class="text-bold">Airport / Government taxes:</span></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<% _.each(pax.fareDetails.taxes, function(tax, taxIdx) { %>
							<tr class="state-1 <%= (taxIdx == pax.fareDetails.taxes.length - 1) ? 'type-1' : '' %>">
								<td>
									<span><%= tax.description %> (<%= tax.code %>)</span>
									<span class="visible-mb" data-need-format="2"><%= tax.amount %></span>
								</td>
								<td>&nbsp;</td>
								<td data-need-format="2">
									<%= tax.amount %>
								</td>
							</tr>
						<% }); %>
						<tr>
							<td><span class="text-bold">Carrier Surcharges:</span></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<% _.each(pax.fareDetails.surcharge, function(charge, chargeIdx) { %>
							<tr class="state-1 <%= (chargeIdx == pax.fareDetails.taxes.length - 1) ? 'type-1' : '' %>">
								<td>
									<span><%= charge.description %> (<%= charge.code %>)</span>
									<span class="visible-mb" data-need-format="2"><%= charge.amount %></span></td>
								<td>&nbsp;</td>
								<td data-need-format="2"><%= charge.amount %></td>
							</tr>
						<% }); %>
						<tr class="subtotal">
							<td><span>Subtotal</span>
								<ul class="list-allocation visible-mb">
									<!-- <li>
										<span>KrisFlyer miles</span>
										<span data-need-format="0">&nbsp;</span>
									</li> -->
									<li>
										<span><%= currency %></span>
										<span data-need-format="2"><%= pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount %></span>
									</li>
								</ul>
							</td>
							<td>&nbsp;</td>
							<td data-need-format="2"><%= pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount %></td>
						</tr>
					<% } %>
				</tbody>
			</table>
		</div>
	<% }); %>
<% } %>

<div class="grand-total">
	<% if ((scenario === 'ORU' && data.oruUpgradeJsonVO.topUpCashForORU && parseFloat(data.oruUpgradeJsonVO.topUpCashForORU) > 0) || (scenario !== 'ORU' && data.bookingSummary.totalToBePaidCash && parseFloat(data.bookingSummary.totalToBePaidCash) > 0)) { %>
		<span class="total-title">Grand total payable</span>
		<span class="total-info right">
			<% if (scenario !== 'ORU') { %>
				<%= currency %> <span data-need-format="2"><%= data.bookingSummary.totalToBePaidCash %></span>
			<% } else { %>
				<%= currency %> <span data-need-format="2"><%= data.oruUpgradeJsonVO.topUpCashForORU %></span>
			<% } %>
		</span>
	<% } %>
</div>

