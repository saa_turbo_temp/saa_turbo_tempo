<% if(data && data.length) { %>
  <% _.each(data, function(vehicle, vehicleIdx){ %>
    <% if(vehicleIdx > 2) { %>
      <div class="item-result hidden" data-vehicleId="<%- vehicle.vehicleId %>">
        <figure><img src="<%- vehicle.largeImageURL %>" alt="<%- vehicle.vehicleName %>" longdesc="img-desc.html"/>
        </figure>
        <div class="content-result">
          <div class="title-content"><span class="title-5--blue"><%= vehicle.vehicleName %></span><span>&nbsp;or similar</span></div>
          <ul class="seat-car">
            <li><%= vehicle.seats %> Seats</li>
            <li><%= vehicle.doors %> Doors</li>
            <li><%= vehicle.bigSuitcase %> Large bags</li>
          </ul>
          <ul class="condition">
            <% if(vehicle.aircon === 'Yes') { %>
              <li>Air Conditioning</li>
            <% } %>
            <% if(vehicle.automatic === 'Automatic tranmission') { %>
              <li>Automatic Tranmission</li>
            <% } else { %>
              <li>Manual Tranmission</li>
            <% } %>
          </ul>
          <ul class="location">
            <% if(!vehicle.route.pickUp.onAirport) { %>
              <li><em class="ico-building-2"></em><span>Pick up location</span><span class="text"><%= vehicle.route.pickUp.locationName %></span></li>
            <% } else { %>
              <li><em class="ico-airplane"></em><span>Pick up location</span><span class="text">In terminal</span></li>
            <% } %>
            <li><em class="ico-2-petro-vector"></em><span>Fuel policy</span><span class="text"><%= vehicle.fuelPolicy.description %></span></li>
          </ul>
          <ul class="cancellation">
            <% if(vehicle.freeCancellation) { %>
              <span class="free-tooltip"><em class="ico-check-thick"></em><span>Free cancellation</span><em data-tooltip="true" data-type="2" data-max-width="255" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Free cancellation up to 48 hours before your pick-up (deposit payments excluded)&lt;/p&gt;" class="ico-tooltips" tabindex="0"></em></span>
            <% } %>
            <% if(vehicle.theftProtection) { %>
              <li><em class="ico-check-thick"></em><span>Theft protection</span></li>
            <% } %>
            <% if(vehicle.freeAmendments) { %>
              <li><em class="ico-check-thick"></em><span>Free ammendments</span></li>
            <% } %>
            <% if(vehicle.theftProtection) { %>
              <li><em class="ico-check-thick"></em><span>Collision damage waiver</span></li>
            <% } %>
          </ul>
          <div class="supplied-by"><span>Supplied by</span><img src="<%- vehicle.supplier.smallLogo %>" alt="<%- vehicle.supplier.supplierName %>" longdesc="img-desc.html"/>
          </div>
          <div class="price-car"><span class="note">Price for 3 days</span>
            <div class="sub-heading-2--blue"><%= vehicle.price.baseCurrency + ' ' + vehicle.price.basePrice %></div><span class="miles">EARN 2000 MILES</span>
            <div class="button-group-3">
              <input type="button" name="select-car" id="select-car-<%- vehicle.vehicleId %>" value="Select car" class="btn-1"/>
              <input type="button" name="remove-car" id="remove-car-<%- vehicle.vehicleId %>" value="Remove car" class="btn-1 hidden"/>
            </div>
          </div>
        </div>
      </div>
    <% } else { %>
      <div class="item-result" data-vehicleId="<%- vehicle.vehicleId %>">
        <figure><img src="<%- vehicle.largeImageURL %>" alt="<%- vehicle.vehicleName %>" longdesc="img-desc.html"/>
        </figure>
        <div class="content-result">
          <div class="title-content"><span class="title-5--blue"><%= vehicle.vehicleName %></span><span>&nbsp;or similar</span></div>
          <ul class="seat-car">
            <li><%= vehicle.seats %> Seats</li>
            <li><%= vehicle.doors %> Doors</li>
            <li><%= vehicle.bigSuitcase %> Large bags</li>
          </ul>
          <ul class="condition">
            <% if(vehicle.aircon === 'Yes') { %>
              <li>Air Conditioning</li>
            <% } %>
            <% if(vehicle.automatic === 'Automatic tranmission') { %>
              <li>Automatic Tranmission</li>
            <% } else { %>
              <li>Manual Tranmission</li>
            <% } %>
          </ul>
          <ul class="location">
            <% if(!vehicle.route.pickUp.onAirport) { %>
              <li><em class="ico-building-2"></em><span>Pick up location</span><span class="text"><%= vehicle.route.pickUp.locationName %></span></li>
            <% } else { %>
              <li><em class="ico-airplane"></em><span>Pick up location</span><span class="text">In terminal</span></li>
            <% } %>
            <li><em class="ico-2-petro-vector"></em><span>Fuel policy</span><span class="text"><%= vehicle.fuelPolicy.description %></span></li>
          </ul>
          <ul class="cancellation">
            <% if(vehicle.freeCancellation) { %>
              <span class="free-tooltip"><em class="ico-check-thick"></em><span>Free cancellation</span><em data-tooltip="true" data-type="2" data-max-width="255" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Free cancellation up to 48 hours before your pick-up (deposit payments excluded)&lt;/p&gt;" class="ico-tooltips" tabindex="0"></em></span>
            <% } %>
            <% if(vehicle.theftProtection) { %>
              <li><em class="ico-check-thick"></em><span>Theft protection</span></li>
            <% } %>
            <% if(vehicle.freeAmendments) { %>
              <li><em class="ico-check-thick"></em><span>Free ammendments</span></li>
            <% } %>
            <% if(vehicle.theftProtection) { %>
              <li><em class="ico-check-thick"></em><span>Collision damage waiver</span></li>
            <% } %>
          </ul>
          <div class="supplied-by"><span>Supplied by</span><img src="<%- vehicle.supplier.smallLogo %>" alt="<%- vehicle.supplier.supplierName %>" longdesc="img-desc.html"/>
          </div>
          <div class="price-car"><span class="note">Price for 3 days</span>
            <div class="sub-heading-2--blue"><%= vehicle.price.baseCurrency + ' ' + vehicle.price.basePrice %></div><span class="miles">EARN 2000 MILES</span>
            <div class="button-group-3">
              <input type="button" name="select-car" id="select-car-<%- vehicle.vehicleId %>" value="Select car" class="btn-1"/>
              <input type="button" name="remove-car" id="remove-car-<%- vehicle.vehicleId %>" value="Remove car" class="btn-1 hidden"/>
            </div>
          </div>
        </div>
      </div>
      <% } %>
  <% }) %>
  <% if(data.length > 3) { %>
    <a href="javascript:void(0)" class="see-more-btn" data-seemore-car><span class="text">See more</span></a>
  <% } %>
<% } %>
