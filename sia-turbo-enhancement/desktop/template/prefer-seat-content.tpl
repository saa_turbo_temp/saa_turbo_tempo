<div class="table-row table-row--heading">
  <div class="table-col table-col-1"><%- data.heading.passenger %></div>
  <div class="table-col table-col-2"><%- data.heading.seat %></div>
  <div class="table-col table-col-3"><%- data.heading.price %></div>
</div>
<% _.each( data.flightInfo, function( flightInfor, idx ){ %>
  <% if(data.isICE){ %>
    <div class="table-row"><span class="prefer_staus"><span><%- flightInfor.departureCityCode %></span><em class="ico-1-plane"></em><span><%- flightInfor.arrivalCityCode %></span></span></div>
    <div class="table-content">
    </div>
  <% } else if(flightInfor.selected === 'true') { %>
    <div class="table-row"><span class="prefer_staus"><span><%- flightInfor.departureCityCode %></span><em class="ico-1-plane"></em><span><%- flightInfor.arrivalCityCode %></span></span></div>
    <div class="table-content">
    </div>
  <% }%>
<% })%>
<div class="table-row">
  <div class="prefer-result"><span class="sub"><%- data.total.text %></span><span><%- data.total.number %></span></div>
</div>