<% var i = 0, j = 3 %>
<% _.each( data.info, function( vouchers ){ %>
  <% _.each( vouchers.type, function( voucher ){ %>
    <tr class="<%= i % 2 === 0 ? 'odd' : 'even'  %>" data-partner="<%= vouchers.partner %>">
      <td>
        <div class="data-title"><%= data.headerMobile[0] %></div>
        <%= voucher.name %>
      </td>
      <td>
        <div class="data-title"><%= data.headerMobile[1] %></div>
        <%= voucher.mileRequired %>
      </td>
      <td>
        <div class="data-title"><%= data.headerMobile[2] %></div>
        <div data-validate-row="true">
          <div data-validate-col="true">
            <div data-customselect="true" class="custom-select custom-select--2">
              <label for="select-<%= j %>" class="select__label">&nbsp;</label>
              <span class="select__text"></span>
              <span class="ico-dropdown">Select</span>
              <select id="select-<%= j %>" name="select-<%= j %>" data-rule-required="true" data-msg-required="Select Quantity." data-field-quantity="<%= i %>" data-voucher-miles-required="<%= voucher.mileRequired %>" data-voucher-type="<%= voucher.name %>">
                <option selected="selected" value="">Select</option>
                <% _.each(voucher.quantity, function(val) { %>
                  <option value="<%= val %>"><%= val %></option>
                <% }) %>
              </select>
            </div>
          </div>
        </div>
      </td>
      <td>
        <div class="data-title"><%= data.headerMobile[3] %></div>
        <span data-total-miles="true">0</span>
      </td>
    </tr>
    <% i = i + 1 %>
    <% j = j + 1 %>
  <% }) %>
<% }) %>
