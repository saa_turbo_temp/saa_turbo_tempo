<div class="accrual-calculator-result">
  <h3 class="sub-heading-3--dark" tabindex="0"><%- data.origin%> to <%- data.destination%> <span><%- data.detail%></span></h3>
  <table class="table-1">
    <thead>
      <tr tabindex="0">
        <% _.each( data.header, function( header, idx ){ %>
          <th tabindex="0" arialabel='<%- header%>' class='<%= (idx === 0) ? "" : "align-right" %>'><%- header%></th>
        <%})%>
      </tr>
    </thead>
    <tbody>
      <% _.each( data.classMiles, function( classMile, idx ){ %>
        <tr tabindex="0" class='<%= (idx%2 ===0) ? "odd" : "even" %>'>
          <td tabindex="0" arialabel='<%- classMile.category%>'><%- classMile.category%></td>
          <td tabindex="0" arialabel='<%- classMile.miles%>' class="align-right"><%- classMile.miles%></td>
        </tr>
      <%})%>
    </tbody>
  </table>
  <p tabindex="0"><%- data.note%></p>
</div>
