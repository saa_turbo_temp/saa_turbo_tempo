<% if(data) { %>
  <div class="popup__inner">
    <div class="popup__content">
      <h2 class="popup__heading title-popup">Rentalcars.com’s terms and conditions</h2>
      <% _.each(data.rentalTermsList, function(datajson, idx) { %>
          <div class="block-content">
            <h3 class="title-4--blue"><%- datajson.type %></h3>
            <p class="title-5--blue"><%- datajson.terms.caption %></p>
            <p><%= datajson.terms.body %></p>
          </div>
           <% _.each(datajson.terms, function(datajsonItem, idx) { %>
            <div class="block-content">
              <p class="title-5--blue"><%- datajsonItem.caption %></p>
              <p><%= datajsonItem.body %></p>
            </div>
          <% }); %>
      <% }); %>
      <a href="#" class="popup__close" aria-label="Close button"><span class="ui-helper-hidden-accessible">Close button</span><span class="text"></span></a>
    </div>
  </div>
<% } %>
