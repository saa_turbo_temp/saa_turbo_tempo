<aside id="youtube-lightbox" class="popup popup--video-lightbox hidden">
  <div class="popup__inner">
    <div class="popup__content">
      <a href="#" class="popup__close" data-close="true"></a>
      <div class="wrap-video" id="flowplayer">
        <video>
          <source type="application/x-mpegurl" src="<%- data.url.m3u8 %>">
          <source type="video/webm" src="<%- data.url.webm %>">
          <source type="video/mp4" src="<%- data.url.mp4 %>">
        </video>
      </div>
      <ul class="watch-list">
        <% _.each(data.image, function(album, idx) { %>
          <li>
            <a href="#" class="info-watch">
              <img src="<%- album.url %>" alt="" longdesc="img-desc.html">
              <div class="desc-thumb">
                <p><%- album.description %></p>
              </div>
            </a>
          </li>
        <% }); %>
      </ul>
  </div>
</aside>
