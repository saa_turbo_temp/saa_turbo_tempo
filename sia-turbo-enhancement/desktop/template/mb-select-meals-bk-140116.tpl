<% _.each(data.segmentMealVO.sectorMealListInformationVO, function(flight, flightIdx) { %>
  <div data-accordion="1" class="block-2 accordion accordion--2" data-select-meal="true">
    <div data-accordion-trigger="1" class="accordion__control active">
      <h3 class="sub-heading-3--dark accordion__heading-redemp"><%= (flightIdx + 1)%>. <%= flight.departureCityCode%> to <%= flight.arrivalCityCode%></h3><em class="ico-point-d"></em>
    </div>
    <div data-accordion-content="1" class="accordion__content" data-rule-selectCategory="true" data-msg-selectCategory="<%= L10n.selectMeal.msgSelectCategory %>">
      <% var selectedPax = data.MealVO.passengerAndMealAssociationVO[data.MealVO.passengerStartingPoint - 1];

      _.each(flight.mealService, function(trip, tripIdx) { %>
        <%
          var categoryName = '';
          var mealName = '';
          var categoryCode = '';
          var selectedCategory = [];
          var selectedInfo = selectedPax.flightDateInformationVO[flightIdx].SectorMealSelectedInfo[tripIdx];

          if (selectedInfo) {
            categoryName = selectedInfo.categoryName;
            categoryCode = selectedInfo.mealCategoryCode
            mealName = selectedInfo.Name;
            selectedCategory = _.filter(trip.mealList, function(meal) {
              return meal.mealCategoryCode === categoryCode;
            });
          }
        %>
        <div class="select-meal-item" data-meal-portion="true">
          <h4 class="text-dark"><%= trip.type%><span class="flight-schedule"><%= trip.origin%><em class="ico-plane"></em><%= trip.destination%></span></h4>
          <div class="form-group grid-row">
            <div class="grid-col one-half">
              <div class="grid-inner">
                <span class="item-label">Category</span>
                <div data-customselect="true" class="custom-select custom-select--2">
                  <label for="category-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= categoryName ? categoryName : L10n.selectMeal.defaultOption %></span><span class="ico-dropdown">Select</span>
                  <select id="category-<%= flightIdx%>-<%= tripIdx%>" name="category-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Category." data-select-category="true">  <option<%= !categoryName ? ' selected="selected" ' : ''%> value="<%= L10n.selectMeal.defaultOption %>" data-populate="false"><%= L10n.selectMeal.defaultOption %></option>
                    <% _.each(trip.mealList, function(category, categoryIdx) { %>
                      <% if ((selectedPax.passengerType === 'ADT' && category.mealCategoryCode !== 'IF' && category.mealCategoryCode !== 'CH' && category.mealCategoryCode !== 'BZ') || (selectedPax.passengerType === 'CHD' && category.mealCategoryCode !== 'IF') || (selectedPax.passengerType === 'INF' && category.mealCategoryCode === 'IF')) { %>
                        <%
                          var listMeal = _.map(category.mealServiceList, function(num, key) {return num.mealName}).toString();
                          var listCode = _.map(category.mealServiceList, function(num, key) {return num.mealCode}).toString();
                        %>
                        <option<%= categoryCode && category.mealCategoryCode === categoryCode ? ' selected="selected" ' : ''%> value="<%= category.mealCategoryName %>" data-code="<%= category.mealCategoryCode %>" data-service-list-meal='<%= listMeal %>' data-service-list-code='<%= listCode %>' data-populate="<%= (category.mealCategoryCode === 'BC' || category.mealCategoryCode === 'BZ' || category.mealCategoryCode === 'JP') ? true : false %>"><%= category.mealCategoryName %></option>
                      <% } %>
                    <%});%>
                  </select>
                </div>
              </div>
            </div>
            <div class="grid-col one-half">
              <div class="grid-inner">
                <span class="item-label">Meal</span>
                <div data-customselect="true" class="custom-select custom-select--2">
                  <label for="meal-<%= flightIdx%>-<%= tripIdx%>" class="select__label">&nbsp;</label><span class="select__text"><%= mealName ? mealName : L10n.selectMeal.defaultOption %></span><span class="ico-dropdown">Select</span>
                  <select  id="meal-<%= flightIdx%>-<%= tripIdx%>" name="meal-<%= flightIdx%>-<%= tripIdx%>" data-rule-required="true" data-msg-required="Select Meal." data-option-meal="true">
                    <%if(categoryName === L10n.selectMeal.defaultOption){%>
                      <option <%= !mealName ? ' selected="selected" ' : ''%> value="<%= L10n.selectMeal.defaultOption %>"><%= L10n.selectMeal.defaultOption %></option>
                    <%}%>
                    <% if (selectedInfo && selectedCategory && selectedCategory.length) { %>
                      <% _.each(selectedCategory[0].mealServiceList, function(meal, mealIdx) { %>
                        <option <%= mealName && meal.mealName.toLowerCase() === mealName.toLowerCase() ? ' selected="selected" ' : ''%> value="<%= meal.mealName%>" data-code="<%= meal.mealCode%>"><%= meal.mealName %></option>
                      <%});%>
                    <% } %>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="alert-block checkin-alert" style="display: none;">
          <div class="inner">
            <div class="alert__icon"><em class="ico-alert"></em></div>
            <div class="alert__message"><span>By selecting a {0} meal, all your {1} meal selections from <%= flight.departureCityCode%> to <%= flight.arrivalCityCode%> have been changed to {0} meal selections.</span> <a href="#" data-undo="true">Undo</a></div>
          </div>
        </div>
      <%});%>

    </div>
  </div>
<%});%>
