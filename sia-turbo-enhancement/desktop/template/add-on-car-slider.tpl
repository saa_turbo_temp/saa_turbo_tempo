<% if(data && data.length > 0) { %>
  <div class="car-avaliable">
    <h4 class="title-5--dark">Showing cars avaliable at San Francisco Airport</h4>
    <div class="car-avaliable-wrapper">
      <div id="car-avaliable-slider" data-mobile-slider="true">
        <div class="slides">
          <% _.each(data, function(vehicle, vehicleIdx){ %>
            <a href="#" class="slide-car-item" data-carsize="<%- vehicle.carSize.code %>" data-carsize-desc="<%- vehicle.carSize.description %>" tabindex="-1">
              <div class="item-inner">
                <div class="car-detail" tabindex="0">
                  <span class="btn-close" data-remove-carsize><em class="ico-close"><span class="ui-helper-hidden-accessible">Close button</span></em></span>
                  <span class="title-6--blue"><%= vehicle.carSize.description %></span>
                  <!-- <em data-tooltip="true" data-type="2" data-max-width="255" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;This car can fit up to <%- vehicle.seats %> adults and <%- vehicle.bigSuitcase %> big luggages.&lt;/p&gt;" class="ico-tooltips"></em> -->
                  <figure><img src="<%- vehicle.imageURL%>" alt="<%- vehicle.carSize.description %>" longdesc="img-desc.html"/>
                  </figure>
                  <ul class="package-list">
                    <li>
                      <em class="ico-user"></em><%= vehicle.seats %>
                    </li>
                    <li>
                      <em class="ico-business-1"></em><%= vehicle.bigSuitcase %>
                    </li>
                    <li>
                      <em class="ico-business-1 small"></em><%= vehicle.smallSuitcase %>
                    </li>
                  </ul>
                  <p class="price">From <%=vehicle.price.baseCurrency + ' ' + (vehicle.price.basePrice + vehicle.price.discount)%></p>
                </div>
              </div>
            </a>
          <% }); %>
        </div>
      </div>
    </div>
  </div>
<% } %>
