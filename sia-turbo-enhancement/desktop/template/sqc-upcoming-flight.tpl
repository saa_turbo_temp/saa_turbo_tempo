<% _.each(data, function(flight, flightIdx) { %>
  <div data-accordion="1" class="block-2 accordion accordion--1 booking--style-2 <%= flightIdx >= 3 ? 'hidden' : '' %>" data-upcoming-flight="<%= data.flightIdx %>" data-url="<%= flight.url %>">
    <div class="accordion__control-inner">
      <a href="#" data-accordion-trigger="1" class="accordion__control accordion__control--1">
        <h3 class="sub-heading-2--blue">Booking Reference <%= flight.referenceNumber %></h3>
        <div class="sub-text-1">
          <%= flight.from.name %> to <%= flight.to.name %>
          <span class="hidden-mb">&nbsp;-&nbsp;</span>
          <span><%= flight.departureDate %></span>
        </div>
        <em class="ico-point-d hidden"></em>
      </a>
      <span class="loading loading--small">Loading...</span>
      <div class="booking-form hidden">
        <a href="#" class="btn-1">
          <!-- Manage check-in -->
        </a>
      </div>
    </div>
    <div data-accordion-content="1" class="accordion__content" style="display: none;">
      <div class="booking-info-group">
        <!--Details-->
      </div>
    </div>
    <div class="accordion__content-bottom">
    <strong class="sub-title">Passengers:</strong>
      <ul class="inline-list-style" data-passengers="<%= flight.passengers %>"><% _.each(flight.passengers, function(pass, passIdx) { %><% if(passIdx < 3) { %><li class="<% passIdx === 2 ? 'last' : '' %>"><%= pass %><% if(passIdx < 3 - 1) { print(','); } %></li><% } %><% }); %>
      </ul>
      <% if(flight.passengers && flight.passengers.length > 3) { %>
        <a href="#" class="link-4" data-toggle-passengers-list="true">
          <em class="ico-point-r"></em>
          <span class="more">See more</span>
          <span class="less">See less</span>
        </a>
      <% } %>
    </div>
  </div>
  <% if(flight.delay) { %>
    <div class="alert-block checkin-alert--middle <%= flightIdx >= 3 ? 'hidden' : '' %>" data-upcoming-flight-alert="<%= data.flightIdx %>">
      <div class="alert-wrapper">
        <div class="alert__icon alert-inner"><em class="ico-alert"></em></div>
        <div class="alert__message alert-inner"><%= flight.delay %></div>
      </div>
    </div>
  <% } %>
<% }); %>
