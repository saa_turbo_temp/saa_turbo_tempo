<% if(data.sshList !== void 0 && data.sshList.length > 0){ %>
  <% _.each(data.sshList, function(sshList, sshListIdx) { %>
    <% if(sshList.eligible === "true") { %>
      <div class="block-2 add-ons-item">
        <div class="add-ons-item__header">
          <h5 class="sub-heading-3--dark"><%- L10n.ssh.title %></h5>
          <a href="#"><em class="ico-point-r"></em>Add</a>
        </div>
        <h6 class="item-sub-heading"><%- sshList.from %> to <%- sshList.to %></h6>
      </div>
    <% } else if(sshList.hotelName !== void 0 && sshList.hotelName !== '' && sshList.checkInDate !== void 0 && sshList.checkInDate !== '' && sshList.checkOutDate !== void 0 && sshList.checkOutDate !== ''){ %>
      <div class="block-2 add-ons-item">
        <div class="add-ons-item__header">
          <h5 class="sub-heading-3--dark"><%- L10n.ssh.title %></h5>
          <a href="#" class="hidden"><em class="ico-point-r"></em>Add</a>
        </div>
        <h6 class="item-sub-heading"><%- sshList.from %> to <%- sshList.to %></h6>
        <div class="add-ons-item__content">
          <a href="#" <%= sshList.hotelImage === void 0 ? 'class="add-ons-item__img hidden"' : 'class="add-ons-item__img"' %>>
            <img src=<%- sshList.hotelImage %> alt="San Francisco to Singapore" longdesc="img-desc.html">
          </a>
          <div class="item-content__info">
            <div class="item-info__head"><span class="item-name"><%- sshList.hotelName %></span>
              <ul class="item-actions hidden">
                <li>
                  <a href="#"><em class="ico-point-r"></em>Change</a>
                </li>
                <li>
                  <a href="#" data-trigger-popup=".popup--cancel-baggage"><em class="ico-point-r"></em>Cancel</a>
                </li>
              </ul>
            </div>
            <ul class="info-details">
              <li <%= sshList.checkInDate === void 0 ? 'class="hidden"' : '' %>><span>Check in: </span><%- sshList.checkInDate %>
              </li>
              <li <%= sshList.checkOutDate === void 0 ? 'class="hidden"' : '' %>><span>Check out: </span><%- sshList.checkOutDate %>
              </li>
              <% if(sshList.roomTypes !== void 0){ %>
                <li <%= sshList.roomTypes.length === 0 ? 'class="hidden"' : '' %>><span>Room types: </span>
                  <% _.each(sshList.roomTypes, function(roomType, roomTypesIdx) { %>
                    <% if(roomTypesIdx === sshList.roomTypes.length - 1) { %>
                      <%- roomType %>
                    <% }else{ %>
                      <%- roomType %> ,
                    <% } %>
                  <% }) %>
                </li>
              <% } %>
              <li <%= sshList.noOfRooms === void 0 ? 'class="hidden"' : '' %>><span>No. of rooms: </span><%- sshList.noOfRooms %>
              </li>
              <li <%= sshList.packageInfo === void 0 ? 'class="hidden"' : '' %>><span>Package: </span><%- sshList.packageInfo %>
              </li>
            </ul>
          </div>
        </div>
      </div>
    <% } %>
  <% }) %>
<% } %>

<% if(data.insuranceList !== void 0 && data.insuranceList.length > 0){ %>
  <% _.each(data.insuranceList, function(insurance, insuranceListIdx) { %>
    <div class="block-2 add-ons-item">
      <div class="add-ons-item__header">
        <h5 class="sub-heading-3--dark"><%-insurance.title%></h5>
        <% if(insurance.links && insurance.links.length) {%>
          <ul>
            <% _.each(insurance.links, function(link, linkIdx) {%>
              <%if(link.type !== 'cancel') {%>
                <li>
                  <a href="<%=link.href%>"><em class="ico-point-r"></em><%=link.text%></a>
                </li>
              <%} else {%>
                <li>
                  <a href="<%=link.href%>" data-trigger-popup=".popup--cancel-travel-insurance"><em class="ico-point-r"></em><%=link.text%></a>
                </li>
              <%}%>
            <%});%>
          </ul>
        <%}%>
      </div>
      <div class="add-ons-item__content item-row"><a href="#" class="<%=insurance.insuranceImg ? 'img-block' : 'hidden'%>"><img src="<%=insurance.insuranceImg%>" alt="<%=insurance.title%>" longdesc="img-desc.html"></a>
        <div class="item-caption">
          <p><%-insurance.description%> <%=insurance.findMore ? '<a href="#">' + insurance.findMore + '</a>' : ''%></p>
        </div>
      </div>
    </div>
  <% }) %>
<% } %>

<% if(data.carRentalList !== void 0 && data.carRentalList.length > 0){ %>
  <% _.each(data.carRentalList, function(carRentalList, carRentalListIdx) { %>
    <div class="block-2 add-ons-item">
      <div class="add-ons-item__header">
        <h5 class="sub-heading-3--dark"><%- carRentalList.title %></h5>
        <ul>
          <li><a href="#"><em class="ico-point-r"></em>Change</a></li>
          <li><a href="#"><em class="ico-point-r"></em>Cancel</a></li>
        </ul>
      </div>
      <div class="add-ons-item__content">
        <div class="item-content__inner">
          <div class="item-left-col"><a href="#" <%= carRentalList.carImg === void 0 ? 'class="hidden"' : '' %>><img src=<%- carRentalList.carImg %> alt="Car rental" longdesc="img-desc.html"></a></div>
          <div class="item-right-col--1">
            <p class="item-name"><%- carRentalList.name %></p>
            <div class="info-details text-bolder"><span <%= carRentalList.PickUpDate === void 0 ? 'class="hidden"' : '' %>>Pick up: </span><%- carRentalList.PickUpDate %>
            </div>
            <div class="info-details text-bolder"><span <%= carRentalList.DropOffDate === void 0 ? 'class="hidden"' : '' %>>Drop off: </span><%- carRentalList.DropOffDate %>
            </div>
          </div>
        </div>
      </div>
    </div>
  <% }) %>
<% } %>

<% if(data.hotelList !== void 0 && data.hotelList.length > 0){ %>
  <% _.each(data.hotelList, function(hotelList, hotelListIdx) { %>
    <div class="block-2 add-ons-item">
      <div class="add-ons-item__header">
        <h5 class="sub-heading-3--dark"><%- hotelList.title %></h5>
        <ul>
          <li><a href="#"><em class="ico-point-r"></em>Change</a></li>
          <li><a href="#"><em class="ico-point-r"></em>Cancel</a></li>
        </ul>
      </div>
      <div class="add-ons-item__content">
        <div class="item-content__inner">
          <div class="item-left-col"><a href="#" <%= hotelList.hotelImg === void 0 ? 'class="hidden"' : '' %>><img src=<%- hotelList.hotelImg %> alt="Hotel" longdesc="img-desc.html"></a></div>
          <div class="item-right-col--1">
            <p class="item-name"><%- hotelList.name %></p>
            <div class="info-details text-bolder"><span <%= hotelList.checkOutDate === void 0 ? 'class="hidden"' : '' %>>Check out: </span><%- hotelList.checkOutDate %>
            </div>
            <div class="info-details text-bolder"><span <%= hotelList.checkInDate === void 0 ? 'class="hidden"' : '' %>>Check in: </span><%- hotelList.checkInDate %>
            </div>
          </div>
        </div>
      </div>
    </div>
  <% }) %>
<% } %>

<% if(data.airportList !== void 0 && data.airportList.length > 0){ %>
  <% _.each(data.airportList, function(airportList, airportListIdx) { %>
    <div class="block-2 add-ons-item">
      <div class="add-ons-item__header">
        <h5 class="sub-heading-3--dark"><%- airportList.title %></h5><a href="#" data-disabled-link="true" class="disabled"><em class="ico-point-r"></em>Cancel</a>
      </div>
      <div class="add-ons-item__content item-row">
        <p><%- airportList.description %></p>
        <% if(airportList.detailInfo !== void 0){ %>
          <p><%- airportList.detailInfo.city %>:&nbsp;<strong><%- airportList.detailInfo.transferDate %>&nbsp;</strong>- <%- airportList.detailInfo.time %></p>
        <% } %>
      </div>
    </div>
  <% }) %>
<% } %>
