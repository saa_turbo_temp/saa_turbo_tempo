<div class="items">
  <% _.each( data.cityVO, function( city, idx ){ %>
    <div class="flight-item">
      <div class="flight-item__inner" tabindex="0">
        <a href="<%- city.shareurl %>" tabindex="-1"><img src="<%- city.imgUrl %>" alt="" longdesc="img-desc.html">
          <span class="flight-item__vignette">&nbsp;</span>
          <div class="flight-item__info-1">
            <p class="info-promotions"><%- city.destinationCityName %><span><%- city.tripType %> <%- city.flighttype %> <%- city.awardType %></span></p>
          </div>
          <div class="flight-item__info-2">
            <p class="info-promotions"><%- city.milesText %> <span>miles</span></p>
          </div>
        </a>
        <% if(city.type === 'promo'){%>
          <div class="flight-item__status-1"><em tabindex="0" aria-label="promo" class="bg-status bg-status--1">promo</em></div>
        <%}%>
        <a href="javascript:;" class="flight-item__favourite <%=(city.favourite) ? 'favourited' : '' %>"><em tabindex="0" <%=(city.favourite) ? 'aria-label="favourited"' : 'aria-label="unfavourite"' %> class="ico-star"><span class="ui-helper-hidden-accessible"><% if(city.favourite){ %>Click to unfavourite<% }else{ %>Click to mark as favourite<% } %></span></em><span class="loading loading--small hidden">Loading...</span></a>
      </div>
    </div>
  <% })%>
</div>
