/**
 * @name SIA
 * @description Define global KFStatement functions
 * @version 1.0
 */

SIA.KFStatement = function(){
	var global = SIA.global;
	var initBlockStatemenet = function(){
		var statements = $('.statements');
		var period = $('[data-period] select', statements);
		var transaction = $('[data-transaction] select', statements);
		var stTable = $('.table-responsive tbody', statements);
		var seemore = $('[data-see-more]', statements);
		var search = $('#Search', statements);
		var triggerSort = $('.table-responsive thead th:first', statements);
		var allData = null;
		var page = 1;
		var limitItem = 8;
		var itemsLength;
		var currentItem = 0;
		var tplItem;

		var renderPeriodTemplate = function(el, json){
			el.empty();
			var options = [];
			for(var i = 0; i < json.period.length; i++){
				options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.period[i] +'"' + '>' + json.period[i] + '</option>');
			}
			el.html(options.join(''));
			el.defaultSelect('refresh');
		};

		var renderTransactionTemplate = function(el, json){
			el.empty();
			var options = [];
			for(var i = 0; i < json.transaction.length; i++){
				options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + json.transaction[i] +'"' + '>' + json.transaction[i] + '</option>');
			}
			el.html(options.join(''));
			el.defaultSelect('refresh');
		};

		var filterData = function(data){
			var currentPeriod = $('#period-select').find('option').filter(':selected').val();
			var currentTransaction = $('#transaction-select').find('option').filter(':selected').val();
			var filteredData = [];

			if(currentPeriod === 'All' && currentTransaction === 'All'){
				return data;
			}
			if(currentPeriod !== 'All'){
				filteredData = data.filter(function(item){
					return item.date.indexOf(currentPeriod) >= 0;
				});
			}
			if(currentTransaction !== 'All'){
				data = filteredData.length ? filteredData : data;
				filteredData = data.filter(function(item){
					return item.type === currentTransaction;
				});
			}
			return filteredData;
		};

		var buildHtml = function(items){
			var limit = page * limitItem;
			var filteredData = filterData(items.statement);
			itemsLength = filteredData.length;
			var dataFilter = {
				headerDesktop: items.headerDesktop,
				headerMobile: items.headerMobile,
				statement:[]
			};
			if(itemsLength <= limit){
				limit = itemsLength;
				seemore.hide();
				setTimeout(function() {
					seemore.text('see more');
				}, 200);
			}
			else{
				seemore.show();
				seemore.removeAttr('style');
			}
			for(currentItem; currentItem < limit; currentItem++){
				if(!filteredData[currentItem]){
					break;
				}
				dataFilter.statement.push(filteredData[currentItem]);
				// if(currentItem === limit - 1){
				// 	page++;
				// }
			}

			if(tplItem) {
				$(window._.template(tplItem, {data: dataFilter})).appendTo(stTable);
			} else {
				$.get(SIA.global.config.url.kfStatement, function (html) {
					tplItem = html;
					var template = window._.template(tplItem, {
						data: dataFilter
					});
					$(template).appendTo(stTable);
					fillHeaderData(dataFilter);
				});
			}
		};

		var fillHeaderData = function(data){
			var headers = stTable.parent().find('th');
			headers.each(function(idx){
				var header = $(this);
				var text = data.headerDesktop[idx];
				if(idx === 0){
					var emEl = header.find('em');
					header.find('a').text(text).append(emEl);
				}else if(idx === headers.length - 1){
					var firstTrim = text.indexOf('(');
					var lastTrim = text.indexOf(')');
					var spanEl = $('<span></span>', { text: text.substr(firstTrim, lastTrim)});
					text = text.slice(0, firstTrim).trim();
					header.text(text).append(spanEl);
				}else{
					header.text(text);
				}
			});
		};


		var renderTable = function(data){
			stTable.empty();
			buildHtml(data);
		};

		var sortDate = function(trigger){
			trigger.off('click.sortDate').on('click.sortDate', function(e){
				// var dataSort = $.makeArray($('.table-responsive tbody tr',statements));
				// if(dataSort.length){
				// 	e.preventDefault();
				// 	if(trigger.hasClass('ascend')){
				// 		trigger.removeClass('ascend');
				// 		// trigger.find('em').removeClass('ico-point-u').addClass('ico-point-d');
				// 		trigger.removeClass('active');
				// 	}
				// 	else{
				// 		trigger.addClass('active');
				// 		trigger.addClass('ascend');
				// 		// trigger.find('em').removeClass('ico-point-d').addClass('ico-point-u');
				// 	}
				// 	dataSort = dataSort.sort(function(a, b){
				// 		var d1 = new Date($(a).find('> td:first-child span').text());
				// 		var d2 = new Date($(b).find('> td:first-child span').text());
				// 		return trigger.hasClass('ascend') ? d1 - d2 : d2 - d1;
				// 	});
				// 	reStyle($(dataSort));
				// }

				e.preventDefault();
				if(trigger.hasClass('ascend')){
					trigger.removeClass('ascend');
					trigger.removeClass('active');
				}
				else{
					trigger.addClass('active');
					trigger.addClass('ascend');
				}

				allData.statement.sort(function(a, b) {
					var d1 = new Date(a.date);
					var d2 = new Date(b.date);
					return trigger.hasClass('ascend') ? d1 - d2 : d2 - d1;
				});

				currentItem = 0;
				filter(allData);
			});

			// var reStyle = function(els){
			// 	els.each(function(idx){
			// 		var self = $(this);
			// 		self.removeClass();
			// 		if(idx%2 === 0){
			// 			self.addClass('odd');
			// 		}
			// 		else{
			// 			self.addClass('even');
			// 		}
			// 		self.appendTo(stTable);
			// 	});
			// };
		};

		var filter = function(data){
			var dataFilter = {
				headerDesktop: data.headerDesktop,
				headerMobile: data.headerMobile,
				statement:[]
			};

			for(var i = 0; i < data.statement.length; i++){
				if(period.val() === 'All' && transaction.val() === 'All'){
					dataFilter.statement.push(data.statement[i]);
				}
				else if(period.val() === 'All' && transaction.val() === data.statement[i].type){
					dataFilter.statement.push(data.statement[i]);
				}
				else if(transaction.val() === 'All' && Number(period.val()) === new Date(data.statement[i].date).getFullYear()){
					dataFilter.statement.push(data.statement[i]);
				}
				else if(Number(period.val()) === new Date(data.statement[i].date).getFullYear() && transaction.val() === data.statement[i].type){
					dataFilter.statement.push(data.statement[i]);
				}
			}
			renderTable(dataFilter);
		};

		seemore.off('click.seemore').on('click.seemore', function(e){
			e.preventDefault();
			page++;
			if(page === 3){
				setTimeout(function() {
					seemore.text('see all');
				}, 200);
			}
			if(page > 3){
				page = allData.statement.length;
			}
			buildHtml(allData);
			if(!seemore.is(':hidden')){
				seemore.removeClass('hidden').removeAttr('style');
			}
		});

		search.off('click.filter').on('click.filter', function(e){
			setTimeout(function() {
				seemore.text('see more');
			}, 200);
			e.preventDefault();
			page = 1;
			currentItem = 0;
			filter(allData);
		});

		// sortDate(triggerSort);

		$.ajax({
			url: 'ajax/kf-statement.json',
			dataType: 'json',
			type: global.config.ajaxMethod,
			success: function(data) {
				// allData = data;
				// renderPeriodTemplate(period, allData);
				// renderTransactionTemplate(transaction, allData);
				// renderTable(allData);

				if (data && data.statement && data.statement.length) {
					data.statement.sort(function(a, b) {
						var d1 = new Date(a.date);
						var d2 = new Date(b.date);
						return triggerSort.hasClass('ascend') ? d1 - d2 : d2 - d1;
					});
					allData = data;
					sortDate(triggerSort);
					renderPeriodTemplate(period, allData);
					renderTransactionTemplate(transaction, allData);
					renderTable(allData);
					seemore.removeClass('hidden').removeAttr('style');
				}
			},
			error: function(xhr, status) {
				if(status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	};

	initBlockStatemenet();
};
