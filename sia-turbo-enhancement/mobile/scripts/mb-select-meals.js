/**
 * @name SIA
 * @description Define global selectMeal functions
 * @version 1.0
 */
SIA.mbSelectMeal = function(){
	var global = SIA.global;
	var config = global.config;
	var isValidated = false;
	var navPaxs = $();
	var selectedPaxIdx = -1;

	var populateData = function(){
		// Render Meals template
		var renderMealTemplate = function(el, json, code, selectedMealVal){
			var options = [];
			var category = el.closest('[data-meal-portion]').find('[data-select-category]').val();
			var isSelected = false;

			el.empty();

			if (category === L10n.selectMeal.defaultOption) {
				options.push('<option selected value="' + L10n.selectMeal.defaultOption +
					'">' + L10n.selectMeal.defaultOption + '</option>');
				isSelected = true;
				// el.data('option-meal', L10n.selectMeal.defaultOption);
			}

			if (json && json.length && code && code.length) {
				for(var i = 0; i < json.length; i++){
					if (!isSelected && selectedMealVal && json[i] === selectedMealVal) {
						isSelected = true;
						options.push('<option selected value="' + json[i] +'"' + ' data-code="' +
							code[i] + '">' + json[i] + '</option>');
					}
					else {
						options.push('<option value="' + json[i] +'"' + ' data-code="'+ code[i] +
							'">' + json[i] + '</option>');
					}
				}
			}

			el.html(options.join('')).defaultSelect('refresh');
			el.data('option-meal', el.val());
		};

		// Update value for selectbox Meals.
		var populateMealBlock = function(el, value, cValue){
			var blocks = el.siblings('.select-meal-item');
			blocks.each(function(){
				var category = $('[data-select-category]', this);
				var meal = $('[data-option-meal]', this);
				if(category.children(':selected').data('populate')) {
					category
						.val(cValue)
						.defaultSelect('refresh');
					category.data('old-select-category', category.data('select-category'));
					category.data('select-category', category.val());
					meal
						.val(value)
						.data('option-meal', value)
						.defaultSelect('refresh')
						.closest('[data-customSelect]')
						.addClass('alert-selectbox');
				}
			});
		};

		// Update value for selectbox Category.
		var populateCategoryBlock = function(obj){
			var wrapper = obj.ml.closest('.select-meal-item');
			var oldMeal = obj.ml.data('option-meal');

			renderMealTemplate(obj.ml, obj.param[0], obj.param[1], obj.param[2]);
			obj.chks.hide();
			if(obj.ctg.children(':selected').data('populate') && obj.chks.length > 1){
				wrapper
					.next()
					.show()
					.find('span').text(obj.wp.data('alertTemplate').format(obj.ml.val(), oldMeal));
			}
			obj.ctg.data('old-select-category', obj.ctg.data('select-category'));
			obj.ctg.data('select-category', obj.ctg.val());
			wrapper.next().data('option-meal', oldMeal);

			obj.ctgs.each(function(){
				var that = $(this);
				var category = $('[data-select-category]', that);
				var meal = $('[data-option-meal]', that);
				if(category.children(':selected').data('populate') && obj.ctg.children(':selected').data('populate')) {
					category
						.val(obj.cVal)
						.defaultSelect('refresh');

					category.data('old-select-category', category.data('select-category'));
					category.data('select-category', category.val());
					that.next().data('option-meal', meal.data('option-meal'));
					renderMealTemplate(meal, obj.param[0], obj.param[1], obj.param[2]);

					meal
						.val(obj.ml.val())
						.data('option-meal', obj.ml.val())
						.defaultSelect('refresh')
						.closest('[data-customSelect]')
						.addClass('alert-selectbox');
				}
			});
		};

		var intergrateData = function(el, alert, wp){
			var category = $('[data-select-category]', el);
			var meal = $('[data-option-meal]', el);
			var customSelectMeal = meal.closest('[data-customselect]');
			var checkinAlert = $(alert);

			var undoAction = function(wp){
				wp.each(function(){
					var self = $(this);
					var wpCategory = self.find('[data-select-category]');
					var wpMeal = self.find('[data-option-meal]');

					if(wpCategory && wpCategory.children(':selected').data('undo')){
						wpCategory
							.val(wpCategory.data('old-select-category'))
							.data('select-category', wpCategory.data('old-select-category'))
							.defaultSelect('refresh');

						var optSelected = wpCategory.children('option:selected');
						var serviceMeal = optSelected.data('service-list-meal');
						var listMeal = serviceMeal ? serviceMeal.split(',') : [];
						var serviceCode = optSelected.data('service-list-code');
						var listCode = serviceCode ? serviceCode.split(',') : [];
						var cValue = wpCategory.find('option:selected').data('code');
						var selectedMealVal = (cValue === 'RL' || cValue === 'BZ') ? getSelectedMealValue(category) : '';

						renderMealTemplate(wpMeal, listMeal, listCode, selectedMealVal);

						wpMeal
							.val(self.next().data('option-meal'))
							.data('option-meal', self.next().data('option-meal'))
							.defaultSelect('refresh');
					}
				});
			};

			// Get value from selectbox Meals
			var getSelectedMealValue = function(cEl) {
				var selectedMealVal = '';
				var categoryEls = cEl.closest('[data-accordion]').find('[data-select-category]').not(cEl);
				categoryEls.each(function() {
					var self = $(this);
					if (self.val() === cEl.val()) {
						selectedMealVal = self.closest('[data-meal-portion]').find('[data-option-meal]').val();
						return false;
					}
				});
				return selectedMealVal;
			};

			meal.data('option-meal', meal.val());
			category.data('select-category', category.val());
			category.data('old-select-category', category.val());
			el.next().data('option-meal', meal.data('option-meal'));

			customSelectMeal.removeClass('alert-selectbox');

			category.data('selectedIndex', category.prop('selectedIndex'));
			category.off('change.updateMeal').on('change.updateMeal', function(){
				var optSelected = category.children('option:selected');
				var serviceMeal = optSelected.data('service-list-meal');
				var listMeal = serviceMeal ? serviceMeal.split(',') : [];
				var serviceCode = optSelected.data('service-list-code');
				var listCode = serviceCode ? serviceCode.split(',') : [];
				var cValue = category.find('option:selected').data('code');
				var selectedMealVal = (cValue === 'RL' || cValue === 'BZ') ? getSelectedMealValue(category) : '';

				var obj = {
					ctgs: category.closest('.select-meal-item').siblings('.select-meal-item'),
					cVal: category.val(),
					// mVal: meal.val(),
					ctgCode: cValue,
					ctg: category,
					ml: meal,
					wp: wp,
					param: [listMeal, listCode, selectedMealVal],
					chks: checkinAlert
				};

				populateCategoryBlock(obj);
				// checkinAlert.hide();

				if (isValidated) {
					var accor = $(this).closest('[data-accordion]');
					validateAccordion(accor);
				}
			});

			checkinAlert.hide();

			customSelectMeal.off('animationend.category webkitAnimationEnd.category MSAnimationEnd.category oanimationend.category').on('animationend.category webkitAnimationEnd.category MSAnimationEnd.category oanimationend.category' , function(){
				customSelectMeal.removeClass('alert-selectbox');
			});

			meal.off('change.updateMeal').on('change.updateMeal', function(){
				var wrapper = $(this).closest('.select-meal-item');
				var selectedCEl = category.children(':selected');

				if(selectedCEl.data('populate')){
					populateMealBlock(wrapper, meal.val(), category.val());
					customSelectMeal.addClass('alert-selectbox');
				}

				if (selectedCEl.data('undo')) {
					checkinAlert.hide();
					checkinAlert.data('option-meal', meal.data('option-meal'));
					category.data('old-select-category', category.data('select-category'));
					category.data('select-category', category.val());

					if(checkinAlert.length > 1){
						wrapper
							.next().show()
							.find('span').text(wp.data('alertTemplate').format(meal.val(), meal.data('option-meal')));
					}

					meal.data('option-meal', meal.val());

				}

				if (isValidated) {
					var accor = $(this).closest('[data-accordion]');
					validateAccordion(accor);
				}
			});

			checkinAlert.find('[data-undo]')
				.off('click.undo')
				.on('click.undo', function(e){
					e.preventDefault();
					undoAction(checkinAlert.prev());
					checkinAlert.hide();
				});
		};

		// Append data from JSON to template
		var renderTemplateFromJSON = function(mealData){
			$.get(config.url.mb.selectMeal, function (tpl) {
				var mealWrapper = $('[data-accordion-wrapper-content]');
				mealWrapper.find('[data-accordion]').remove();
				var template = window._.template(tpl, {
					data: mealData
				});
				$(template).prependTo(mealWrapper);

				mealWrapper.find('[data-customselect]').each(function(){
					var self = $(this);
					if(!self.data('customSelect')){
						self.find('select').defaultSelect({
							wrapper: '.custom-select',
							textCustom: '.select__text',
							isInput: false
						});
					}
				});

				var blockMeal = $('[data-select-meal]', blockMeal);
				var applyMeal = function(mealPortion, checkinAlert, self){
					mealPortion.each(function(){
						intergrateData($(this), checkinAlert, self);
					});
				};

				blockMeal.each(function(){
					var self = $(this);
					var mealPortion = $('[data-meal-portion]', self);
					var checkinAlert = $('.checkin-alert', self);
					applyMeal(mealPortion, checkinAlert, self);

					if(!self.data('alertTemplate')){
						self.data('alertTemplate', checkinAlert.eq(0).find('span').text());
						window.alertTemplate = self.data('alertTemplate');
					}
				});

				if (SIA.accordion.initAccordion) {
					SIA.accordion.initAccordion();
				}
			});
		};

		// Render sidebar template
		var renderSidebarTemplate = function(mealData) {
			$.get(config.url.mb.selectMealSidebar, function (tpl) {
				var sidebarContent = $('.sidebar .booking-nav');
				var template = window._.template(tpl, {
					data: mealData
				});
				if (sidebarContent.length) {
					$(template).appendTo(sidebarContent.empty());
					navPaxs = $('.sidebar .booking-nav a.booking-nav__item');
					selectedPaxIdx = navPaxs.siblings('select').find('option:selected').index();
				}
			});
		};

		// Call Ajax
		var getAjaxMeal = function(){
			$.ajax({
				url: typeof window.mealJSON !== 'undefined' ? window.mealJSON : global.config.url.mealJSON,
				dataType: 'json',
				success: function(mealJSON){
					if (mealJSON && mealJSON.MealVO && mealJSON.segmentMealVO) {
						globalJson.selectMeals = mealJSON;

						setHiddenValue(mealJSON);

						renderSidebarTemplate(mealJSON.MealVO);
						renderTemplateFromJSON(mealJSON);
					}
					else {
						renderSidebarTemplate(globalJson.selectMeals.MealVO);
						renderTemplateFromJSON(globalJson.selectMeals);
					}
				},
				error: function() {
					window.alert(L10n.selectMeal.msgError);
				}
			});
		};

		// Set value for input hidden
		var setHiddenValue = function(json) {
			var hiddenPaxId = $('#passenger-id');
			var hiddenSegmentEl = $();
			var hiddenMealCategoryEl = $('#meal-category');
			var selectedIdx = json.MealVO.passengerStartingPoint - 1;
			var mealCategory = '';
			var paxAndMealVo = json.MealVO.passengerAndMealAssociationVO[selectedIdx];

			if (paxAndMealVo) {
				var paxId = paxAndMealVo.passengerId;
				if (paxId) {
					hiddenPaxId.val(paxId);
				}

				var mealSelectedInfo = paxAndMealVo.flightDateInformationVO[0]
					.SectorMealSelectedInfo;
				if (mealSelectedInfo && mealSelectedInfo.length) {
					$.each(mealSelectedInfo, function(idx, mealInfo) {
						hiddenSegmentEl = $('#segment-' + (idx + 1));
						if (hiddenSegmentEl.length) {
							hiddenSegmentEl.val((idx + 1) + ':' + mealInfo.Name);
						}

						if (mealInfo.mealCategoryCode) {
							mealCategory += mealInfo.mealCategoryCode + ',';
						}
					});

					if (mealCategory.lastIndexOf(',') === mealCategory.length - 1) {
						mealCategory = mealCategory.substr(0, mealCategory.length - 1);
					}

					hiddenMealCategoryEl.val(mealCategory);
				}
			}
		};

		getAjaxMeal();
	};

	// Validate accordion
	var validateAccordion = function(accor) {
		var isValid = true;
		var mealItem = accor.find('.select-meal-item');
		var cEls = mealItem.length ?
			mealItem.find('[data-select-category]') : $();

		if (!validateCategory(cEls)) {
			isValid = false;

			if (!accor.find('p.text-error span').length) {
				accor.append('<p class="text-error"><span>' + L10n.selectMeal.msgCategory +
					'</span></p>');
				cEls.closest('.form-group').addClass('error');
			}
		}
		else {
			if (accor.find('p.text-error span').length) {
				accor.find('p.text-error span').remove();
				cEls.closest('.form-group').removeClass('error');
			}
		}

		return isValid;
	};

	// Validate category
	var validateCategory = function(cEls) {
		if (cEls.length > 1) {
			var isValid = true;
			var cVlue = '';
			var mVlue = '';

			cEls.each(function() {
				var cEl = $(this);
				var mEl = cEl.closest('.select-meal-item').find('[data-option-meal]');
				var cElVal = cEl.val();
				var cElDataOp = cEl.find('option:selected').data('code');

				if(!cVlue && cElDataOp !== 'JP' && cElDataOp !== 'BC'){
					cVlue = cElVal;
				}
				// if(!cElDataOp){
				// 	isValid = false;
				// }
				if (cElVal !== cVlue && cVlue) {
					if(cElDataOp !== 'JP' && cElDataOp !== 'BC'){
						cVlue = cElVal;
						mVlue = mEl.val();
						isValid = false;
					}
				}
			});
			return isValid;
		}
		return true;
	};

	// Handle validation
	var handleValidation = function() {
		var bookingNav = $('.sidebar .booking-nav');
		var form = $('#form--select-meals');
		var directPage = $('[data-url]', form);
		var resetSelected = false;

		var validateForm = function() {
			var isValid = true;
			isValidated = true;
			form.find('[data-accordion]').each(function() {
				var self = $(this);
				if(!validateAccordion(self)) {
					isValid = false;
				}
			});
			if (!isValid) {
				var accorTrigger = $('p.text-error span').eq(0)
					.closest('[data-accordion]').find('[data-accordion-trigger]');
				if (accorTrigger.length && !accorTrigger.hasClass('active')) {
					accorTrigger.trigger('click.accordion');
				}
			}
			return isValid;
		};

		bookingNav
			.off('change.switch-passenger')
			.on('change.switch-passenger', 'select', function() {
				var selectTab = $(this);
				var index = selectTab.find('option:selected').index();
				resetSelected = false;
				if (validateForm() && navPaxs.length) {
					window.location.href = $.trim(navPaxs.eq(index).attr('href'));
				}
				else {
					resetSelected = true;
					// selectTab.find('option').eq(selectedPaxIdx).prop('selected', true);
				}
			})
			.off('blur.switch-passenger focusout.switch-passenger')
			.on('blur.switch-passenger focusout.switch-passenger', function() {
				if (resetSelected) {
					var selectTab = $(this);
					selectTab.find('option').eq(selectedPaxIdx).prop('selected', true);
				}
			});

		form.off('submit.slCategory').on('submit.slCategory', function(e) {
			if (!validateForm()) {
				e.preventDefault();
			}
		});

		directPage.off('click.changeurlForm').on('click.changeurlForm', function(){
			form.attr('action', directPage.data('url'));
			isValidated = true;
			if (validateForm()) {
				form[0].submit();
			}
		});
	};

	var init = function(){
		populateData();
		handleValidation();
	};

	init();
};
