/**
 * @name SIA
 * @description Define global selectReturnDate functions
 * @version 1.0
 */
SIA.selectReturnDate = function(){
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	// var isAndroid = /android/g.test(navigator.userAgent.toLowerCase());
	var _selectReturnDate = function(){
		// declare varibale
		// var returnSelect = $('#travel-radio-4');
		// var oneWaySelect = $('#travel-radio-5');
		// var returnField = $('#one-way-1');
		// var oneWayField = $('#one-way-2');
		var oneWayDatepickers = $('[data-oneway]');
		var timerDatepicker = null;
		var winW = win.width();
		var winH = win.height();
		var nonLimitedDate = $('[data-non-limited-date]');

		var detectOrientation = function() {
			return window.screen.height < window.screen.width;
		};

		// var wp = returnField.closest('.animated');

		// select oneway or return
		// var selectFight = function(){
		// 	returnSelect.off('change.showReturnField').on('change.showReturnField', function(){
		// 		if($(this).is(':checked')){
		// 			returnField.removeClass('hidden');
		// 			oneWayField.addClass('hidden');
		// 			wp = returnField.closest('.animated');
		// 			wp.css('height', '');
		// 			if(win.width() < config.mobile && wp.length){
		// 				if(win.height() > wp.height()){
		// 					container.height(win.height());
		// 				}
		// 				else{
		// 					container.height(wp.height());
		// 				}
		// 			}
		// 		}
		// 	});
		// 	oneWaySelect.off('change.showOneWayField').on('change.showOneWayField', function(){
		// 		if($(this).is(':checked')){
		// 			oneWayField.removeClass('hidden');
		// 			returnField.addClass('hidden');
		// 			wp = returnField.closest('.animated');
		// 			if(win.width() < config.mobile && wp.length){
		// 				if(win.height() > wp.height()){
		// 					container.height(win.height());
		// 				}
		// 				else{
		// 					container.height(wp.height());
		// 				}
		// 			}
		// 		}
		// 	});
		// 	if((global.vars.isIE() && global.vars.isIE() < 9) || global.vars.isSafari){
		// 		returnSelect.off('afterTicked.showReturnField').on('afterTicked.showReturnField', function(){
		// 			returnSelect.trigger('change.showReturnField');
		// 		});
		// 		oneWaySelect.off('afterTicked.showOneWayField').on('afterTicked.showOneWayField', function(){
		// 			oneWaySelect.trigger('change.showOneWayField');
		// 		});
		// 	}
		// };

		// var setBackToday = function(datepicker) {
		// 	var today = new Date((new Date()).toDateString());
		// 	var enteredDate = datepicker.datepicker('getDate');
		// 	if(enteredDate < today) {
		// 		datepicker.datepicker('setDate', today);
		// 	}
		// };

		oneWayDatepickers.each(function(){
			var oneWayDatepicker = $(this);
			if(oneWayDatepicker.data('init-oneway')){
				return;
			}
			oneWayDatepicker.data('init-oneway', true);
			// create an input for onway flight
			// var storeOneDay = $('<input type="text" value="" readonly class="datepicker-holder"/>').insertAfter(oneWayDatepicker).css({
			// 	'opacity': 0,
			// 	'visibility': 'visible',
			// 	'text-indent': '-20em',
			// 	'height': 50
			// });
			// storeOneDay.css({
			// 	width: oneWayDatepicker.width()
			// });

			oneWayDatepicker.closest('.input-3').off('click.showDatepicker').on('click.showDatepicker', function(){
				var self = $(this);
				winW = win.width();
				winH = win.height();
				oneWayDatepicker.datepicker('show');
				if (!self.hasClass('disabled')) {
					self.addClass('focus');
				}
				win.off('resize.hideDatepickerOneWay').on('resize.hideDatepickerOneWay', function(){
					if(winW !== win.width() && winH !== win.height()){
						oneWayDatepicker.datepicker('hide');
						winW = win.width();
						winH = win.height();
					}
				});
			}).off('focusout.highlightInput');

			oneWayDatepicker.datepicker({
				// defaultDate: '+1w',
				numberOfMonths: 1,
				minDate: nonLimitedDate.length ? null : new Date(),
				maxDate: nonLimitedDate.length ? new Date() : '+355d',
				dateFormat: 'dd/mm/yy',
				onChangeMonthYear: function(y, m, inst){
					clearTimeout(timerDatepicker);
					timerDatepicker = setTimeout(function() {
						$(inst.dpDiv).css('zIndex', config.zIndex.datepicker);
					}, 430);
				},
				beforeShow: function(input, inst){
					var el = $(input),
							nbOfMonth = el.datepicker('option', 'numberOfMonths'),
							dpEl = $(inst.dpDiv);
					if(detectOrientation()) {
						dpEl.width('auto');
						if(nbOfMonth !== 2) {
							el.datepicker('option', 'numberOfMonths', 2);
						}
					} else {
						if(nbOfMonth !== 1) {
							el.datepicker('option', 'numberOfMonths', 1);
						}
					}

					clearTimeout(timerDatepicker);
					timerDatepicker = setTimeout(function() {
						var leftDp = oneWayDatepicker.closest('.input-3').offset().left;
						dpEl.css({
							'zIndex': config.zIndex.datepicker,
							'right': leftDp,
							'width': 'auto',
							'display': 'block'
						}).off('click.doNothing').on('click.doNothing', function(){
							// fix for lumina
						});
					}, 100);
				},
				onSelect: function(dateText){
					oneWayDatepicker.val(dateText);
					if(oneWayDatepicker.closest('[data-target]').length){
						oneWayDatepicker.closest('[data-target]').prev().find('[data-start-date]').val(dateText).data('date', dateText);
					}
					if(oneWayDatepicker.closest('#travel-widget').data('widget-v1') || oneWayDatepicker.closest('#travel-widget').data('widget-v2')) {
						$('#travel-widget [data-start-date], #travel-widget [data-oneway]').not(oneWayDatepicker).closest('.input-3').removeClass('default');
						$('#travel-widget [data-oneway]').val(dateText);
						$('#travel-widget [data-start-date]').each(function(){
							$(this).val(dateText);
							var selfReturn = $(this).closest('[data-return-flight]').find('[data-return-date]');
							if(selfReturn.val() !== "") {
								var pselfReturn = selfReturn.datepicker('getDate').getTime();
								var pDepartDate = oneWayDatepicker.datepicker('getDate').getTime();
								if(selfReturn.is('[data-plus-date]')) {
									if(pselfReturn <= pDepartDate) {
										selfReturn.val(null);
									}
								} else {
									if(pselfReturn < pDepartDate) {
										selfReturn.val(null);
									}
								}
							}
							var startDateTmp = oneWayDatepicker.datepicker('getDate');
							var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate()));
							selfReturn.datepicker('option', 'minDate',
								returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
							);
						});
						// var checkout = $('#book-hotel [data-return-date]');
						// if(checkout && checkout.data('plus-date')) {
						// 	var plusNumber = parseInt(checkout.data('plus-date')) || 0;
						// 	var fromDate = checkout.closest('form').find('[data-start-date]');
						// 	var startDateTmp = fromDate.datepicker('getDate');
						// 	var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate() + plusNumber));
						// 	checkout.datepicker('option', 'minDate',
						// 		returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
						// 	);
						// }
					};
					$(this).valid();
					// var validator = oneWayDatepicker.closest('form').data('validator');
					// var validatedOnce = oneWayDatepicker.closest('form').data('validatedOnce');
					// if(validator && validatedOnce) {
					// 	validator.element(oneWayDatepicker);
					// }
				},
				onClose: function(date, inst){
					var currentReturnDatepicker = $(this).val();
					var isValidDay = $.inputmask.isValid(currentReturnDatepicker, { alias: 'dd/mm/yyyy'});
					if(!isValidDay){
						oneWayDatepicker.val('');
						oneWayDatepicker.closest('[data-target]').prev().find('[data-start-date]').val('').data('date', '');
					}
					oneWayDatepicker.closest('.input-3').removeClass('focus');
					win.off('resize.hideDatepickerOneWay');
					$(inst.dpDiv).off('click.doNothing');
				},
				dayNamesMin: config.formatDays
			}).prop('readonly', true).click(function() {
				$(this).datepicker('show');
			});

			/*if(!isAndroid) {
				oneWayDatepicker.inputmask('date', {
					showMaskOnHover: false,
					oncomplete: function() {
						setBackToday(oneWayDatepicker);
					},
					onincomplete: function() {
						if(!window.Modernizr.input.placeholder) {
							setTimeout(function() {
								oneWayDatepicker.triggerHandler('blur.placeholder');
							}, 10);
						}
					}
				}).on('mouseover mouseout', function() {
					if(!$(this).inputmask('isComplete') && !window.Modernizr.input.placeholder) {
						$(this).triggerHandler('blur.placeholder');
					}
				}).triggerHandler('mouseout');
			}*/

			oneWayDatepicker.off('focus.highlight').on('focus.highlight', function(){
				oneWayDatepicker.closest('.input-3').addClass('focus');
			}).off('blur.highlight').on('blur.highlight', function(){
				oneWayDatepicker.closest('.input-3').removeClass('focus');
			});

			// setTimeout(function() {
			// 	oneWayDatepicker.datepicker('setDate', 'today');
			// 	oneWayDatepicker.data('date', oneWayDatepicker.val()).closest('.input-3').removeClass('default');
			// }, 4000);
		});

		// change flight
		// selectFight();
	};

	// init
	// pick up return date
	_selectReturnDate();
};
