/**
 * @name SIA
 * @description Define function to carousel slide
 * @version 1.0
 */
SIA.carouselSlide = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var carouselSlide = $('[data-carousel-slide]');
	var paddingRight = 22;

	if(carouselSlide.length){
		carouselSlide.each(function(){
			var carousel = $(this);
			var wrapperHLS = carousel.parent();
			var imgHighlightLength = carousel.find('img').length - 1;

			var loadBackgroundHighlight = function(self, parentSelt, idx){
				parentSelt.css({
					'backgroundImage': 'url(' + self.attr('src') + ')'
				});
				self.attr('src', config.imgSrc.transparent);
				if(idx === imgHighlightLength){
					if(window.innerWidth > 480){
						carousel.width(wrapperHLS.width() + paddingRight);
					}
					else{
						carousel.width(wrapperHLS.width());
					}
					carousel.css('visibility', 'visible');
					carousel.find('.slides')
						.slick({
							siaCustomisations: true,
							dots: true,
							speed: 300,
							useCSS: false,
							draggable: true,
							slidesToShow: 2,
							slidesToScroll: 2,
							accessibility: false,
							autoplay: false,
							pauseOnHover: false,
							responsive: [
								{
									breakpoint: 768,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 2
									}
								},
								{
									breakpoint: 480,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1
									}
								}
							]
						});
				}
			};

			carousel.find('img').each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				nI.onload = function(){
					loadBackgroundHighlight(self, parentSelt, idx);
				};
				nI.src = self.attr('src');
			});
		});

		win.off('resize.carouselSlide').on('resize.carouselSlide',function() {
			carouselSlide.each(function(){
				var carousel = $(this);
				var wrapperHLS = carousel.parent();

				if(window.innerWidth > 480){
					carousel.width(wrapperHLS.width() + paddingRight);
				}
				else{
					carousel.width(wrapperHLS.width());
				}
			});
		}).trigger('resize.carouselSlide');
	}
};
