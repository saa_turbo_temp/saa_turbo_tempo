/**
 * @name SIA
 * @description Define global swipe datepicker functions
 * @version 1.0
 */
SIA.swipeDatepicker = function(){
	var touchTimer,
			isIE = SIA.global.vars.isIE();
	var swipeDetect = function(el, callback) {
		var touchSurface = $(el),
				swipedir,
				startX,
				startY,
				distX,
				distY,
				threshold, // required min distance traveled to be considered swipe
				restraint = 100, // maximum distance allowed at the same time in perpendicular direction
				allowedTime = 300, // maximum time allowed to travel that distance
				elapsedTime,
				startTime,
				handleSwipe = callback || function(){};

		touchSurface.css('-ms-touch-action', 'none');
		touchSurface.off('touchstart.swipeDatepicker MSPointerDown.swipeDatepicker').on('touchstart.swipeDatepicker MSPointerDown.swipeDatepicker', function(e) {
			var event = e.originalEvent;
			swipedir = 'none';
			distX = 0;
			distY = 0;
			startTime = new Date().getTime();

			if(isIE) {
				if(event.preventManipulation) {
					event.preventManipulation();
				}
				startX = event.pageX;
				startY = event.pageY;
			} else {
				startX = event.changedTouches[0].pageX;
				startY = event.changedTouches[0].pageY;
			}
		});

		touchSurface.off('touchmove.swipeDatepicker MSPointerMove.swipeDatepicker').on('touchmove.swipeDatepicker MSPointerMove.swipeDatepicker', function(e) {
			if(e.originalEvent.preventManipulation) {
				event.preventManipulation();
			}
			e.preventDefault();
		});

		touchSurface.off('touchend.swipeDatepicker MSPointerUp.swipeDatepicker').on('touchend.swipeDatepicker MSPointerUp.swipeDatepicker', function(e) {
			var event = e.originalEvent,
					pageX, pageY;

			threshold = touchSurface.width() / 3;

			if(isIE) {
				if(event.preventManipulation) {
					event.preventManipulation();
				}
				pageX = event.pageX;
				pageY = event.pageY;
			} else {
				pageX = event.changedTouches[0].pageX;
				pageY = event.changedTouches[0].pageY;
			}

			distX = pageX - startX;
			distY = pageY - startY;
			elapsedTime = (new Date().getTime() - startTime) / 1000;

			if(elapsedTime <= allowedTime) {
				if(Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) {
					swipedir = (distX < 0) ? 'left' : 'right';
				} else if(Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) {
					swipedir = (distY < 0) ? 'up' : 'down';
				}
			}
			handleSwipe(swipedir, touchSurface);
		});
	};

	swipeDetect(document.getElementById('ui-datepicker-div'), function(swipedir, el) {
		if(swipedir !== 'none') {
			clearTimeout(touchTimer);
			touchTimer = setTimeout(function() {
				if(swipedir === 'left') {
					el.find('[data-handler="next"]').trigger('click');
				} else if(swipedir === 'right') {
					el.find('[data-handler="prev"]').trigger('click');
				}
			}, 400);
		}
	});
};
