/**
 * @name SIA
 * @description Define cookies used for voucher
 * @version 1.0
 */
SIA.voucherStored = function() {
	var setLocalStorage = function(name, data) {
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem(name, JSON.stringify(data));
    }
	};

	var getLocalStorage = function(name) {
		return JSON.parse(localStorage.getItem(name));
	};
  // Return public function
	return {
		setLocalStorage: setLocalStorage,
		getLocalStorage: getLocalStorage
	}
};
