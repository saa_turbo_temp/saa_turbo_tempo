/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.multipleSubmit = function() {
	// var global = SIA.global;
	// var nextPassengerBtn = $('#adult-passenger-input-1');
	// var passengerItems = $('.booking-nav__item');
	// var mainForm = $('#form-passenger-1');
	// var numberOfPassengers = passengerItems.length;
	// var groupData = [];

	// mainForm.validate({
	// 	ignore: ':hidden',
	// 	focusInvalid: true,
	// 	onkeyup: false,
	// 	onfocusout: false,
	// 	errorPlacement: global.vars.validateErrorPlacement,
	// 	success: global.vars.validateSuccess
	// });

	// var submitable = function() {
	// 	if (!groupData.length) {
	// 		return false;
	// 	}

	// 	for (var i = 0; i < numberOfPassengers; i++) {
	// 		if (groupData[i] === void 0) {
	// 			return false;
	// 		}
	// 	}

	// 	return true;
	// };

	// var triggerSubmit = function(form) {
	// 	form.trigger('submit');
	// };

	// var isFormValid = function(form) {
	// 	return form.valid();
	// };

	// var resetForm = function(form) {
	// 	form[0].reset();
	// };

	// var showPassengerItems = function(currentPassenger) {
	// 	passengerItems.removeClass('active');
	// 	currentPassenger.addClass('active');
	// };

	// var saveFormData = function(form, idx) {
	// 	var data = {};
	// 	var formData = form.serialize().split('&');
	// 	var len = formData.length;

	// 	for (var i = 0; i < len; i++) {
	// 		data[formData[i].split('=')[0]] = formData[i].split('=')[1];
	// 	}

	// 	groupData[idx] = data;
	// };

	// var loadDataBack = function(form, idx) {
	// 	var data = groupData[idx];
	// 	if (data === void 0) {
	// 		return;
	// 	}

	// 	for (var key in data) {
	// 		if (data.hasOwnProperty(key)) {
	// 			if (!data[key]) {
	// 				continue;
	// 			}
	// 			data[key] = data[key].replace(/\+/g, ' ').replace(/%2F/g, '/');
	// 			var field = $('#' + key);
	// 			var value = data[key];
	// 			if (field.attr('type') === 'text') {
	// 				field.val(value);
	// 			}
	// 			if (field.is('select')) {

	// 			}
	// 		}
	// 	}

	// 	console.log(groupData[idx]);
	// };

	// var selectNextPassenger = function() {
	// 	var currentPassenger = passengerItems.filter('.active');
	// 	if (submitable()) {
	// 		console.log('submit group data');
	// 		return;
	// 	}

	// 	triggerSubmit(mainForm);
	// 	if (isFormValid(mainForm)) {
	// 		var nextPassenger = currentPassenger.next();
	// 		var idx = currentPassenger.data('idx');
	// 		showPassengerItems(nextPassenger);
	// 		saveFormData(mainForm, idx);
	// 		resetForm(mainForm);
	// 	}
	// };

	// var selectPassenger = function(e) {
	// 	e.preventDefault();
	// 	var passengerItem = $(this);
	// 	var idx = passengerItem.data('idx');
	// 	showPassengerItems(passengerItem);
	// 	resetForm(mainForm);
	// 	loadDataBack(mainForm, idx);
	// };

	// var mainFormSubmit = function(e) {
	// 	e.preventDefault();
	// };

	// nextPassengerBtn.on('click.groupValidate', selectNextPassenger);
	// passengerItems.on('click.groupValidate', selectPassenger);
	// mainForm.on('submit', mainFormSubmit);
};


/**
 * @name SIA
 * @description Define global passengerDetail functions
 * @version 1.0
 */
// SIA.passengerDetail = function(){
// 	var global = SIA.global;
// 	var win = global.vars.win;
// 	var config = global.config;
// 	var wrapPassenger = $('.wrap-passenger');
// 	var formPassenger = $('.form-passenger-detail');

// 	if(!formPassenger.length){
// 		return;
// 	}

// 	var convertMonth = function(n, isGetIndex){
// 		var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
// 		return isGetIndex ? m.indexOf(n ? n.slice(0, 3) : 'Jan') : m[n];
// 	};

// 	$.validator.addMethod('required_issue_place', function(value, el, param) {
// 		if(!!$(param[0]).val()){
// 			return !!this.elementValue(el);
// 		}
// 		return false;
// 	}, L10n.validator.requiredIssuePlace);

// 	$.validator.addMethod('checkofadult', function(value, el, param) {
// 		var day = $(param[1]).val();
// 		var month = convertMonth($(param[2]).val(), true);
// 		var year = $(param[3]).val();
// 		var age = param[0];
// 		var mydate = new Date();
// 		mydate.setFullYear(year, month, day);

// 		var setDate = new Date();
// 		setDate.setFullYear(mydate.getFullYear() + age,month, day);
// 		var currdate = new Date();

// 		if ((currdate - setDate) >= 0){
// 			return true;
// 		}else{
// 			return false;
// 		}
// 	}, L10n.validator.checkofadult);

// 	$.validator.addMethod('checkofchild', function(value, el, param) {
// 		var day = $(param[1]).val();
// 		var month = convertMonth($(param[2]).val(), true);
// 		var year = $(param[3]).val();
// 		var age = param[0];
// 		var mydate = new Date();
// 		mydate.setFullYear(year, month, day);

// 		var currdate = new Date();
// 		var setDate = new Date();
// 		setDate.setFullYear(mydate.getFullYear() + age,month, day);

// 		if (currdate - mydate < (2 * 365 * 24 * 60 * 60 * 1000) || currdate - setDate > 0 || mydate > currdate){
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	}, L10n.validator.checkofchild);

// 	$.validator.addMethod('checkofinfant', function(value, el, param) {
// 		var arr = param[2].split(',');
// 		var day = $(arr[0]).val();
// 		var month = convertMonth($(arr[1]).val(), true);
// 		var year = $(arr[2]).val();
// 		var age = param[0];
// 		var date = param[1];
// 		var mydate = new Date();
// 		mydate.setFullYear(year, month, day);

// 		var currdate = new Date();
// 		var setDate = new Date();
// 		setDate.setFullYear(mydate.getFullYear() + age,month, day);

// 		if ((currdate - setDate) > 0 || mydate > currdate){
// 			return false;
// 		}else{
// 			if((currdate - mydate)/(24*60*60*1000) < date)
// 			{
// 				return false;
// 			}
// 			return true;
// 		}
// 	}, L10n.validator.checkofinfant);

// 	$.validator.addMethod('checkCurrentDate', function(value, el, param) {
// 		if($(el).is('.hidden')) {
// 			return true;
// 		}
// 		var arr = param[0].split(',');
// 		var cd = new Date();
// 		var optDate = new Date($(arr[2]).val(), convertMonth($(arr[1]).val(), true), $(arr[0]).val());
// 		// ignore validate
// 		if($(param[1]).is(':checked')){
// 			return true;
// 		}
// 		// detect validate with condition
// 		if(optDate < cd){
// 			return false;
// 		}
// 		else{
// 			return true;
// 		}
// 		return true;
// 	}, L10n.validator.checkCurrentDate);

// 	$.validator.addMethod('checkpassport', function(value, el, param) {
// 		if($(el).is('.hidden')) {
// 			return true;
// 		}
// 		var arr = param[1].split(',');
// 		var cd = new Date();
// 		var optDate = new Date($(arr[2]).val(), convertMonth($(arr[1]).val(), true), $(arr[0]).val());
// 		// ignore validate
// 		if($(param[3]).is(':checked')){
// 			return true;
// 		}
// 		// detect validate with condition
// 		if(param[2]){
// 			if((optDate - cd)/(30*24*60*60*1000) < param[0] && $(param[2]).is(':checked')){
// 				return false;
// 			}
// 			else{
// 				return true;
// 			}
// 		}
// 		else{
// 			if((optDate - cd)/(30*24*60*60*1000) < param[0]){
// 				return false;
// 			}
// 		}
// 		return true;
// 	}, L10n.validator.checkpassport);

// 	$.validator.addMethod('check_alpha_numberic', function(value) {
// 		if(!value.length){
// 			return true;
// 		}
// 		return /^[a-zA-Z0-9]+$/i.test(value);
// 	}, L10n.validator.checkAlphaNumberic);

// 	$.validator.addMethod('check_addr_usa', function(value) {
// 		if(!value.length){
// 			return true;
// 		}
// 		return /^[a-zA-Z0-9\s\-\#\/\'\,\[\]]+$/i.test(value);
// 	}, L10n.validator.checkAddrUsa);

// 	$.validator.addMethod('check_kf_membership_number', function(value) {
// 		if(value.length === 10 && /^81/i.test(value)) {
// 			return true;
// 		}
// 		return false;
// 	}, L10n.validator.checkKfMembershipNumber);

// 	// var passengerCbk1 = $('#passerger-checkbox-1');
// 	// var passengerCbk2 = $('#passerger-checkbox-2');
// 	var passsengerName = $('.sidebar .booking-nav a.booking-nav__item');
// 	var selectTab = passsengerName.siblings('select');
// 	var popupShow = $();
// 	var timerTabAfterChange = null;
// 	var flyingFocus = $();

// 	var detectGender = function(resourceElement, referenceElement) {
// 		if(!resourceElement.length || !referenceElement.length) {
// 			return;
// 		}
// 		var arrFemale = referenceElement.data('groupFemale').split(',');
// 		var triggerGender = function(element) {
// 			if($.inArray(element.val(), arrFemale) >= 0) {
// 				resourceElement.find('input[type="radio"]').filter(function() {
// 					return this.value === 'female';
// 				}).trigger('click');
// 			} else {
// 				resourceElement.find('input[type="radio"]').filter(function() {
// 					return this.value === 'male';
// 				}).trigger('click');
// 			}
// 		};

// 		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
// 			triggerGender($(this));
// 		});
// 		triggerGender(referenceElement);
// 	};

// 	detectGender($('[data-gender-resource]'), $('[data-gender-reference]'));

// 	var detectSucess = function(){
// 		var dt = {
// 			index: 0,
// 			success: true
// 		};
// 		formPassenger.each(function(i){
// 			if(!$(this).data('success')){
// 				dt = {
// 					index: i,
// 					success: false
// 				};
// 				return false;
// 			}
// 		});
// 		return dt;
// 	};
// 	var afertUpdate = function(currentTab){
// 		var checkBoxConfirm = currentTab.find('[class|="button-group"] input');
// 		var wpCf = currentTab.find('[data-confirm-tc]');
// 		if(formPassenger.filter(function(){
// 			return $(this).data('success');
// 		}).length === formPassenger.length - 1){
// 			if(!currentTab.data('success')){
// 				if(currentTab.closest('[data-lastpax]').length > 0 && true === currentTab.closest('[data-lastpax]').data('lastpax') && wpCf.length) {
// 					// checkBoxConfirm.val(L10n.passengerDetail.proceed);
// 					if(wpCf.find(':checkbox').is(':checked')){
// 						checkBoxConfirm.removeClass('disabled').prop('disabled', false);
// 					}
// 					else{
// 						checkBoxConfirm.addClass('disabled').prop('disabled', true);
// 					}
// 					wpCf.show();
// 				}
// 			}
// 		}else{
// 			// checkBoxConfirm.removeClass('disabled').prop('disabled', false).val(L10n.passengerDetail.next);
// 			checkBoxConfirm.removeClass('disabled').prop('disabled', false);
// 			wpCf.hide();

// 		}
// 	};

// 	// var correctDate = function(){
// 	// 	var target = $('[data-rule-validatedate]');
// 	// 	var detectDate = function(d, m, y, el){
// 	// 		var nd = new Date();
// 	// 		var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
// 	// 		if(d > getLastDate.getDate()){
// 	// 			el.val(getLastDate.getDate());
// 	// 			el.defaultSelect('refresh');
// 	// 			setTimeout(function(){
// 	// 				el.children().eq(getLastDate.getDate()).prop('selected', true);
// 	// 				el.blur();
// 	// 			}, 400);
// 	// 		}
// 	// 	};
// 	// 	target.each(function(){
// 	// 		var self = $(this);
// 	// 		var data = self.data('rule-validatedate');
// 	// 		var date = $(data[0]);
// 	// 		var month = $(data[1]);
// 	// 		var year = $(data[2]);
// 	// 		// console.log(date, month, year);
// 	// 		/*date.off('change.correctDate').on('change.correctDate', function(){
// 	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
// 	// 		});
// 	// 		month.off('change.correctDate').on('change.correctDate', function(){
// 	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
// 	// 		});
// 	// 		year.off('change.correctDate').on('change.correctDate', function(){
// 	// 			detectDate(date.val(), month.find(':selected').index(), year.val(), date);
// 	// 		});*/

// 	// 		date.off('blur.correctDate').on('blur.correctDate', function() {
// 	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
// 	// 		});
// 	// 		month.off('blur.correctDate').on('blur.correctDate', function() {
// 	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
// 	// 		});
// 	// 		year.off('blur.correctDate').on('blur.correctDate', function() {
// 	// 			detectDate(date.val(), convertMonth(month.val(), true) + 1, year.val(), date);
// 	// 		});
// 	// 	});

// 	// };

// 	// correctDate();

// 	// var correctDMY = function(){
// 	// 	$('[data-month],[data-date],[data-year]').each(function(){
// 	// 		var self  = $(this);
// 	// 		var input = self.find('input');
// 	// 		var select = self.find('select');
// 	// 		var hasValue = function(val, el){
// 	// 			var valid = false;
// 	// 			el.each(function(){
// 	// 				if($(this).data('value') === val){
// 	// 					valid = true;
// 	// 				}
// 	// 			});
// 	// 			return valid;
// 	// 		};
// 	// 		input.off('blur.correctMonth').on('blur.correctMonth', function(){
// 	// 			var li = input.autocomplete( 'widget' ).find('li');
// 	// 			if(li.length === 1 && li.data('value') === L10n.globalSearch.noMatches){
// 	// 				input.val(select.find('option:first').text());
// 	// 				self.removeClass('default');
// 	// 			}
// 	// 			else{
// 	// 				if(li.length !== select.children().length){
// 	// 					if(!hasValue(input.val(), li)){
// 	// 						input.val(li.first().data('value'));
// 	// 						self.removeClass('default');
// 	// 					}
// 	// 				}
// 	// 			}
// 	// 		});
// 	// 	});
// 	// };

// 	// correctDMY();

// 	// Init Confirm pax pop up && Mismatch pop up
// 	var popupConfirmPassenger = $('.popout--confirm-passenger');
// 	var popupMismatch = $('.popout--mismatch');
// 	var popupAlertConfirm = $('.popup--passenger-condition');

// 	if(!popupConfirmPassenger.data('Popup')){
// 		popupConfirmPassenger.Popup({
// 			overlayBGTemplate: config.template.overlay,
// 			modalShowClass: '',
// 			triggerCloseModal: '.popup__close, [data-close]'
// 		});
// 	}
// 	if(!popupMismatch.data('Popup')){
// 		popupMismatch.Popup({
// 			overlayBGTemplate: config.template.overlay,
// 			modalShowClass: '',
// 			triggerCloseModal: '.popup__close, [data-close]'
// 		});
// 	}
// 	if(!popupAlertConfirm.data('Popup')){
// 		popupAlertConfirm.Popup({
// 			overlayBGTemplate: config.template.overlay,
// 			modalShowClass: '',
// 			triggerCloseModal: '.popup__close, [data-close]'
// 		});
// 	}

// 	if(formPassenger.length > 1) {
// 		$('[data-tab]').tabMenu({
// 			tab: 'aside.sidebar a.booking-nav__item',
// 			tabContent: 'form.form-passenger-detail',
// 			selectOnMobile: 'aside.sidebar .booking-nav > select',
// 			activeClass: 'active',
// 			afterChange: function(tabContent){
// 				var currentTab = tabContent.filter('.active');
// 				if(!currentTab.data('initValidation')){
// 					initValidation(currentTab, currentTab.index());
// 					/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
// 						// on Desktop
// 						$('[data-customselect]').not(':hidden').customSelect({
// 							itemsShow: 5,
// 							heightItem: 43,
// 							scrollWith: 2
// 						});
// 					}
// 					else{
// 						// on Tablet and Mobile
// 						$('select').not(':hidden').each(function(){
// 							if(!$(this).data('defaultSelect')){
// 								$(this).defaultSelect({
// 									wrapper: '.custom-select',
// 									textCustom: '.select__text',
// 									isInput: false
// 								});
// 							}
// 						});
// 					}*/

// 					// on Tablet and Mobile
// 					$('select').not(':hidden').each(function(){
// 						if(!$(this).closest('[data-autocomplete]').length){
// 							if(!$(this).data('defaultSelect')){
// 								$(this).defaultSelect({
// 									wrapper: '.custom-select',
// 									textCustom: '.select__text',
// 									isInput: false
// 								});
// 							}
// 						}
// 					});
// 				}
// 				clearTimeout(timerTabAfterChange);
// 				timerTabAfterChange =setTimeout(function(){
// 					$('html,body').stop().animate({
// 						'scrollTop': 0
// 					}, 1000, function(){
// 						flyingFocus = $('#flying-focus');
// 						if(flyingFocus.length){
// 							flyingFocus.remove();
// 						}
// 					});
// 				}, 200);
// 				afertUpdate(currentTab);
// 			}
// 		});
// 	}
// 	else {
// 		passsengerName.on('click.switch-passenger', function(e) {
// 			e.preventDefault();
// 			if(formPassenger.data('validator').checkForm()) {
// 				// formPassenger.submit();
// 				var confirmCheckbox = formPassenger.find('[data-confirm-tc] :checkbox');
// 				if(confirmCheckbox.length) {
// 					if(!confirmCheckbox.is(':checked')) {
// 						popupAlertConfirm.Popup('show');
// 						formPassenger.data('checkBoxConfirm', true);
// 					} else {
// 						formPassenger.data('checkBoxConfirm', false);
// 					}
// 				}
// 				if(!formPassenger.data('checkBoxConfirm')) {
// 					formPassenger.submit();
// 				}
// 			}
// 			else {
// 				window.setTimeout(function() {
// 					if(!formPassenger.data('validatedOnce')) {
// 						formPassenger.data('validatedOnce', true);
// 					}
// 					formPassenger.valid();
// 				}, 1);
// 				var firstInvalidInput = formPassenger.find('.error:input').eq(0);
// 				// var formGroupInvalid = firstInvalidInput.closest('.form-group');
// 				// firstInvalidInput.focus();
// 				if(firstInvalidInput.is('select') && firstInvalidInput.parent().is('[data-customselect]')) {
// 					firstInvalidInput.parent().addClass('focus');
// 				}
// 				// if(formGroupInvalid) {
// 				// 	$('html, body').animate({
// 				// 		scrollTop: formGroupInvalid.offset().top
// 				// 	}, 400);
// 				// }
// 			}
// 		});
// 		var selectedIndex = selectTab.prop('selectedIndex');
// 		selectTab.on('change.switch-passenger', function(e) {
// 			e.preventDefault();
// 			if(formPassenger.data('validator').checkForm()) {
// 				// formPassenger.submit();
// 				var confirmCheckbox = formPassenger.find('[data-confirm-tc] :checkbox');
// 				if(confirmCheckbox.length) {
// 					if(!confirmCheckbox.is(':checked')) {
// 						popupAlertConfirm.Popup('show');
// 						formPassenger.data('checkBoxConfirm', true);
// 					} else {
// 						formPassenger.data('checkBoxConfirm', false);
// 					}
// 				}
// 				if(!formPassenger.data('checkBoxConfirm')) {
// 					formPassenger.submit();
// 				}
// 			}
// 			else {
// 				window.setTimeout(function() {
// 					if(!formPassenger.data('validatedOnce')) {
// 						formPassenger.data('validatedOnce', true);
// 					}
// 					formPassenger.valid();
// 				}, 1);
// 				// $(this).prop('selectedIndex', selectedIndex);
// 				// var firstInvalidInput = formPassenger.find('.error').eq(0);
// 				// firstInvalidInput.focus();
// 				// $('html, body').animate({
// 				// 	scrollTop: firstInvalidInput.closest('.form-group').offset().top
// 				// }, 400);
// 			}
// 		}).off('blur.resetValue').on('blur.resetValue', function(){
// 			$(this).children().eq(selectedIndex).prop('selected', true);
// 		});
// 	}
// 	// passengerCbk1.on('change.resetError', function(){
// 	// 	if($(this).is(':checked')){
// 	// 		var parent = $('#lim-2').closest('.form-group');
// 	// 		parent.find('.text-error').remove();
// 	// 		// parent.parent().find('.ico-error').remove();
// 	// 		parent.removeClass('error');
// 	// 	}
// 	// });
// 	// passengerCbk2.on('change.resetError', function(){
// 	// 	if($(this).is(':checked')){
// 	// 		var parent = $('#lim-19').closest('.form-group');
// 	// 		parent.find('.text-error').remove();
// 	// 		// parent.parent().find('.ico-error').remove();
// 	// 		parent.removeClass('error');
// 	// 	}
// 	// });

// 	// var validateFormGroup = function(formGroup){
// 	// 	formGroup.each(function(){
// 	// 		var self = $(this);
// 	// 		var doValidate = function(els){
// 	// 			var pass = true;
// 	// 			els.each(function(){
// 	// 				if(!pass){
// 	// 					return;
// 	// 				}
// 	// 				pass = $(this).valid();
// 	// 			});
// 	// 		};
// 	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
// 	// 			formGroup.each(function(){
// 	// 				if($(this).data('change')){
// 	// 					doValidate($(this).find('select, input'));
// 	// 				}
// 	// 			});
// 	// 		});

// 	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
// 	// 			formGroup.not(self).each(function(){
// 	// 				if($(this).data('change')){
// 	// 					doValidate($(this).find('select, input'));
// 	// 				}
// 	// 			});
// 	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
// 	// 			self.data('change', true);
// 	// 		});
// 	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
// 	// 			self.data('change', true);
// 	// 			$(this).valid();
// 	// 		}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
// 	// 			if($(this).val()){
// 	// 				self.data('change', true);
// 	// 			}
// 	// 		});
// 	// 		if(global.vars.isSafari){
// 	// 			self.find(':radio').off('afterTicked.passengerDetail').on('afterTicked.passengerDetail', function(){
// 	// 				self.data('change', true);
// 	// 			});
// 	// 		}
// 	// 	});
// 	// };

// 	var initValidation = function(formPassengerDetail){
// 		// var formGroup = formPassengerDetail.find('.form-group');
// 		var popupAccountUpdate = $(formPassengerDetail.data('popup'));
// 		var wrapCheckInput = $('[data-disable-value]',formPassengerDetail);

// 		wrapCheckInput.each(function(){
// 			var wrap = $(this);
// 			var checkBox = $('input:checkbox', wrap);
// 			var inputSibling = $('input:text', wrap);
// 			if(inputSibling.data('ruleRequired')) {
// 				checkBox.each(function(i){
// 					var self = $(this);
// 					var sibl = inputSibling.eq(i);
// 					self.off('change.disabledInputSibling').on('change.disabledInputSibling', function(){
// 						if(self.is(':checked')){
// 							sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
// 							wrap.find('.text-error').remove();
// 							wrap.find('.error').removeClass('error');
// 						}
// 						else{
// 							sibl.prop('disabled', false).closest('span').removeClass('disabled');
// 						}
// 					});
// 				});
// 			}
// 		});

// 		if(popupAccountUpdate.length){
// 			popupShow = popupAccountUpdate;
// 			// update target field
// 			var passport = formPassengerDetail.find('[data-passport] input');
// 			var country = formPassengerDetail.find('[data-country] input');
// 			var email = formPassengerDetail.find('[data-email] input');
// 			var expireDay = formPassengerDetail.find('[data-expire] [data-day] select');
// 			var expireMonth = formPassengerDetail.find('[data-expire] [data-month] select');
// 			var expireYear = formPassengerDetail.find('[data-expire] [data-year] select');
// 			var mobileCode = formPassengerDetail.find('[data-mobile] [data-country-code] input');
// 			var mobileArea = formPassengerDetail.find('[data-mobile] [data-area] input');
// 			var mobilePhone = formPassengerDetail.find('[data-mobile] [data-phone] input');

// 			// compare field
// 			// var cpPassport = popupAccountUpdate.find('[data-compare-passport] input');
// 			// var cpCountry = popupAccountUpdate.find('[data-compare-country] input');
// 			// var cpExpire = popupAccountUpdate.find('[data.compare-expire] input');
// 			// var cpMobile = popupAccountUpdate.find('[data-compare-mobile] input');

// 			// update field
// 			var cpPassport = popupAccountUpdate.find('[data-update-passport] input');
// 			var cpCountry = popupAccountUpdate.find('[data-update-country] input');
// 			var cpEmail = popupAccountUpdate.find('[data-update-email] input');
// 			var cpExpire = popupAccountUpdate.find('[data-update-expire] input');
// 			var cpMobile = popupAccountUpdate.find('[data-update-mobile] input');

// 			// update value
// 			passport.off('change.passport').on('change.passport', function(){
// 				cpPassport.val($(this).val());
// 			});
// 			cpPassport.val(passport.val());
// 			email.off('change.passport').on('change.passport', function(){
// 				cpEmail.val($(this).val());
// 			});
// 			cpEmail.val(email.val());
// 			// country.closest('.custom-select').off('afterSelect.country').on('afterSelect.country', function(){
// 			// 	cpCountry.val(country.find('option:selected').text());
// 			// });
// 			country.off('blur.country').on('blur.country', function(){
// 				cpCountry.val($(this).val());
// 			});
// 			cpCountry.val(country.val());

// 			// expireDay.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
// 			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			// });
// 			expireDay.off('change.expireDay').on('change.expireDay', function(){
// 				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			});
// 			// expireMonth.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
// 			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			// });
// 			expireMonth.off('change.expireDay').on('change.expireDay', function(){
// 				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			});
// 			// expireYear.closest('.custom-select').off('afterSelect.expireDay').on('afterSelect.expireDay', function(){
// 			// 	cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			// });
// 			expireYear.off('change.expireDay').on('change.expireDay', function(){
// 				cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());
// 			});
// 			cpExpire.val(expireDay.val() + ' ' + expireMonth.val() + ' ' + expireYear.val());

// 			// mobileCode.closest('.custom-select').off('afterSelect.mobileCode').on('afterSelect.mobileCode', function(){
// 			// 	cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			// });
// 			mobileCode.off('blur.mobileCode').on('blur.mobileCode', function(){
// 				if(mobileCode.val()) {
// 					cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length -1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 				}
// 			});
// 			/*mobileArea.closest('.custom-select').off('afterSelect.mobileArea').on('afterSelect.mobileArea', function(){
// 				cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			});*/
// 			mobileArea.off('change.mobileArea').on('change.mobileArea', function(){
// 				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length -1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			});
// 			/*mobilePhone.closest('.custom-select').off('afterSelect.mobilePhone').on('afterSelect.mobilePhone', function(){
// 				cpMobile.val(mobileCode.val() + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			});*/
// 			mobilePhone.off('change.mobilePhone').on('change.mobilePhone', function(){
// 				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length -1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			});
// 			if(mobileCode.val()) {
// 				cpMobile.val(mobileCode.val().split('(')[1].substring(0, mobileCode.val().split('(')[1].length -1) + ' ' + mobileArea.val() + ' ' + mobilePhone.val());
// 			}

// 			// init popup
// 			var	flyingFocus = $('#flying-focus');
// 			popupAccountUpdate.Popup({
// 				overlayBGTemplate: config.template.overlay,
// 				modalShowClass: '',
// 				triggerCloseModal: '.popup__close, [data-close]',
// 				afterShow: function(){
// 					flyingFocus = $('#flying-focus');
// 					if(flyingFocus.length){
// 						flyingFocus.remove();
// 					}
// 					// if($(window).width() < 768){
// 					// 	container.css('height', $('.popup--login').find('.popup__content').outerHeight(true));
// 					// 	container.css('overflow', 'hidden');
// 					// }
// 				},
// 				afterHide: function(){
// 					// if($(window).width() < 768){
// 					// 	container.css('height', '');
// 					// 	container.css('overflow', '');
// 					// }
// 					// setTimeout(function(){
// 					// 	$('html,body').animate({
// 					// 		'scrollTop': 0
// 					// 	});
// 					// }, 200);
// 				}
// 			}).find('[class|="button-group"] .btn-1').off('click.checksubmit').on('click.checksubmit', function(){
// 				// update btn
// 				// if(!detectSucess().success){
// 				// 	e.preventDefault();
// 				// 	passsengerName.eq(detectSucess().index).trigger('click.show');
// 				// 	if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
// 				// 		selectTab.prop('selectedIndex', detectSucess().index);
// 				// 	}
// 				// 	popupShow.Popup('hide');
// 				// }
// 			}).end().find('[class|="button-group"] .btn-2').off('click.checksubmit').on('click.checksubmit', function(){
// 				// skip btn
// 				// if(!detectSucess().success){
// 				// 	e.preventDefault();
// 				// 	passsengerName.eq(detectSucess().index).trigger('click.show');
// 				// 	if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
// 				// 		selectTab.prop('selectedIndex', detectSucess().index);
// 				// 	}
// 				// 	popupShow.Popup('hide');
// 				// }
// 				// e.preventDefault();
// 				// popupShow.Popup('hide');
// 			});
// 			global.vars.checkAllList(popupAccountUpdate.find('input[data-checkbox-master]'), popupAccountUpdate.find('.table > div').not(':eq(0)'));

// 			var renderLoggedInUser = function(){
// 				popupAccountUpdate.find('input:text').each(function() {
// 					var that = this;
// 					var el = $(that);
// 					var parentElement = that.parentNode.parentNode;
// 					var dataParent = $(parentElement).data();
// 					if(!$.isEmptyObject(dataParent)) {
// 						for(var objData in dataParent) {
// 							if(globalJson.loggedInUser[objData]) {
// 								el.val(globalJson.loggedInUser[objData]);
// 							}
// 						}
// 					}
// 				});
// 			};
// 			if(globalJson.loggedInUser) {
// 				renderLoggedInUser();
// 			}
// 		}

// 		var checkUpdate = function(els , els1){
// 			var change = false;
// 			els.each(function(i){
// 				if($(this).val() !== els1.eq(i).val()){
// 					change = true;
// 					return;
// 				}
// 			});
// 			return change;
// 		};

// 		formPassengerDetail.data('initValidation', true);
// 		/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
// 					// on Desktop
// 			if(!$('[data-customselect]').data('customSelect')){
// 				$('[data-customselect]').not(':hidden').customSelect({
// 					itemsShow: 5,
// 					heightItem: 43,
// 					scrollWith: 2
// 				});
// 			}
// 		}
// 		else{
// 			// on Tablet and Mobile
// 			$('select').not(':hidden').each(function(){
// 				if(!$(this).data('defaultSelect')){
// 					$(this).defaultSelect({
// 						wrapper: '.custom-select',
// 						textCustom: '.select__text',
// 						isInput: false
// 					});
// 				}
// 			});
// 		}*/

// 		// on Tablet and Mobile
// 		$('select').not(':hidden').each(function(){
// 			if(!$(this).closest('[data-autocomplete]').length){
// 				if(!$(this).data('defaultSelect')){
// 					$(this).defaultSelect({
// 						wrapper: '.custom-select',
// 						textCustom: '.select__text',
// 						isInput: false
// 					});
// 				}
// 			}
// 		});

// 		formPassengerDetail.validate({
// 			ignore:':hidden',
// 			focusInvalid: true,
// 			errorPlacement: global.vars.validateErrorPlacement,
// 			success: global.vars.validateSuccess,
// 			// onfocusout: global.vars.validateOnfocusout,
// 			submitHandler: function(){
// 				passsengerName.closest('.active').addClass('passed').find('.ico-check-thick').show();
// 				formPassengerDetail.data('success', true);
// 				flyingFocus = $('#flying-focus');
// 				if(flyingFocus.length){
// 					flyingFocus.remove();
// 				}
// 				if(detectSucess().success){
// 					if(wrapPassenger.data('li')){
// 						if(checkUpdate(popupShow.find('[data-compare-passport] input,[data-compare-country] input,[data-compare-expire] input,[data-compare-mobile] input, [data-compare-email] input'),popupShow.find('[data-update-passport] input,[data-update-country] input,[data-update-expire] input,[data-update-mobile] input, [data-update-email] input'))){
// 							popupShow.Popup('show');
// 						}
// 					}

// 					// process for Confirm pax pop up && Mismatch pop up
// 					if(wrapPassenger.data('lastStep')) {
// 						if(globalJson.cibPassengerInfoCompare) {
// 							var isCompare = true;
// 							var infoCompares = globalJson.cibPassengerInfoCompare;

// 							for(var nameInfo in infoCompares) {
// 								var elCompare = formPassengerDetail.find('[name="' + nameInfo + '"]').val().toString();
// 								if(elCompare) {
// 									if(elCompare !== infoCompares[nameInfo]) {
// 										isCompare = false;
// 										break;
// 									}
// 								}
// 							}

// 							if(!isCompare) {
// 								popupMismatch.Popup('show');
// 								popupMismatch.find('.btn-2').off('click.closeMismatch').on('click.closeMismatch',
// 								function(e){
// 									e.preventDefault();
// 									popupMismatch.Popup('hide');
// 								});
// 								return false;
// 							}
// 						}

// 						popupConfirmPassenger.Popup('show');
// 						popupConfirmPassenger.find('.btn-2').off('click.closeConfirm').on('click.closeConfirm',
// 						function(e){
// 							e.preventDefault();
// 							popupConfirmPassenger.Popup('hide');
// 						});

// 						return false;
// 					}
// 				}
// 				else{
// 					if(!detectSucess().success){
// 						passsengerName.eq(detectSucess().index).trigger('click.show');
// 						if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
// 							selectTab.prop('selectedIndex', detectSucess().index);
// 						}
// 					}
// 				}
// 				/*if(global.vars.isIE() && global.vars.isIE() < 9){
// 					if(SIA.preloader.isVisible()){
// 						SIA.preloader.hide();
// 					}
// 				}*/
// 				// console.log(!wrapPassenger.data('li') && detectSucess().success);
// 				if(!wrapPassenger.data('li') && detectSucess().success){
// 					return true;
// 				}
// 				else if(wrapPassenger.data('li') && detectSucess().success){
// 					return false;
// 				}
// 				else if(!detectSucess().success){
// 					return false;
// 				}
// 			},
// 			invalidHandler: function(form, validator) {
// 				var errors = validator.numberOfInvalids();
// 				if (errors) {
// 					// win.scrollTop($(validator.errorList[0].element).closest('.grid-col').offset().top - 40);
// 					var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
// 					if(errorColEl.length) {
// 						win.scrollTop(errorColEl.offset().top - 40);
// 					}
// 				}
// 			}
// 		});

// 		// validateFormGroup(formGroup);
// 		afertUpdate(formPassengerDetail);

// 		// confirm checkbox
// 		// var wrapperFieldConfirm = formPassengerDetail.find('[data-confirm-tc]');
// 		// var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

// 		// if(confirmCheckbox.length){
// 		// 	confirmCheckbox.off('change.confirm').on('change.confirm', function(e){
// 		// 		e.preventDefault();
// 		// 		var btn = confirmCheckbox.closest('.form-group-full').next().find('input');
// 		// 		if(confirmCheckbox.is(':checked')){
// 		// 			btn.removeClass('disabled').prop('disabled', false);
// 		// 		}
// 		// 		else{
// 		// 			btn.addClass('disabled').prop('disabled', true);
// 		// 		}
// 		// 	}).trigger('change.confirm');
// 		// 	if(global.vars.isSafari){
// 		// 		confirmCheckbox.off('afterTicked.confirm').on('afterTicked.confirm', function(e){
// 		// 			e.preventDefault();
// 		// 			confirmCheckbox.trigger('change.confirm');
// 		// 		});
// 		// 	}
// 		// }

// 		var formFields = formPassengerDetail.find('input, textarea, select');
// 		var displayCheckTab = function(element) {
// 			if(formPassengerDetail.validate('validator').checkForm()) {
// 				element.addClass('passed');
// 				// element.find('.ico-check-thick').show();
// 			} else {
// 				element.removeClass('passed');
// 				element.find('.ico-check-thick').hide();
// 			}
// 		};

// 		// passsengerName.removeClass('passed').find('.ico-check-thick').hide();

// 		// Change label button
// 		if(!wrapPassenger.data('lastStep')) {
// 			var btnSubmits = formPassengerDetail.find('[type="submit"]');
// 			// var btnSubmit = btnSubmits.not('[data-url]');
// 			var btnUrl = btnSubmits.filter('[data-url]');
// 			// formFields.each(function(){
// 			// 	// $(this).change(function(){
// 			// 	$(this).off('change.txtNextButton').on('change.txtNextButton', function() {
// 			// 		if(formPassengerDetail.validate('validator').checkForm()) {
// 			// 			if(btnSubmit.val().toLowerCase() === L10n.passengerDetail.next.toLowerCase()) {
// 			// 				btnSubmit.val(L10n.passengerDetail.nextPassenger);
// 			// 			}
// 			// 		} else {
// 			// 			if(btnSubmit.val().toLowerCase() === L10n.passengerDetail.nextPassenger.toLowerCase()) {
// 			// 				btnSubmit.val(L10n.passengerDetail.next);
// 			// 			}
// 			// 		}
// 			// 	});
// 			// });
// 			btnUrl.off('click.changeURLSubmit').on('click.changeURLSubmit', function(e){
// 				e.preventDefault();
// 				var self = $(this);
// 				var form = self.closest('form');
// 				form.attr('action', self.data('url'));
// 				form[0].submit();
// 			});
// 		} else {
// 			passsengerName.closest('.active').addClass('passed')/*.prevAll().find('.ico-check-thick').show()*/;
// 			var compareDateWithNow = function(year,month,day,islessthan){
// 				var now = new Date();
// 				var birthday = new Date(year +' '+ month +' '+ day);
// 				return islessthan ? (now > birthday) : (now < birthday);
// 			};
// 			var birthInfo = $('[data-birth-info]');
// 			birthInfo.off('change.birthSelects', 'select').on('change.birthSelects', 'select', function(){
// 				var dayOfBirth = birthInfo.find('[data-day] option:selected').val();
// 				var monthOfBirth = birthInfo.find('[data-month] option:selected').val();
// 				var yearOfBirth = birthInfo.find('[data-year] option:selected').val();
// 				if(dayOfBirth !== '' && monthOfBirth !== '' && yearOfBirth !== ''){
// 					var under12ConfirmMess = $('[data-under-12]');
// 					var over12ConfirmMess = $('[data-over-12]');
// 					if(compareDateWithNow(parseInt(yearOfBirth) + 5,monthOfBirth,dayOfBirth,true)){
// 						if(compareDateWithNow(parseInt(yearOfBirth) + 12,monthOfBirth,dayOfBirth,false)){
// 							under12ConfirmMess.removeClass('hidden');
// 							over12ConfirmMess.addClass('hidden');
// 						}
// 						else if(compareDateWithNow(parseInt(yearOfBirth) + 18,monthOfBirth,dayOfBirth,false)){
// 							over12ConfirmMess.removeClass('hidden');
// 							under12ConfirmMess.addClass('hidden');
// 						}
// 						else {
// 							over12ConfirmMess.addClass('hidden');
// 							under12ConfirmMess.addClass('hidden');
// 						}
// 					}
// 					else {
// 						over12ConfirmMess.addClass('hidden');
// 						under12ConfirmMess.addClass('hidden');
// 					}
// 					showPopupConfirm();
// 				}
// 			});
// 		}

// 		displayCheckTab(passsengerName.closest('.active'));

// 		// Control hide show for icon the check
// 		formFields.each(function(){
// 			$(this).off('change.formField').on('change.formField', function() {
// 				// displayCheckTab(passsengerName.closest('.active'));
// 				var iconCheckForm = passsengerName.closest('.active').find('.ico-check-thick');
// 				if(!iconCheckForm.is(':hidden')) {
// 					iconCheckForm.hide();
// 				}
// 			});
// 		});

// 	};

// 	// initValidation(formPassenger.filter('.active'), formPassenger.filter('.active').index());
// 	initValidation(formPassenger);

// 	/* Select Citizenship/Visa Status */
// 	formPassenger.each(function() {
// 		var citizenshipStatus = $(this).find('[data-citizenship-visa-status]');

// 		citizenshipStatus.off('change.change-citizenship').on('change.change-citizenship', function() {
// 			// var citizenshipFields = $(this).closest('form').find('[data-citizenship-visa-status-relation]');
// 			var citizenshipFields = $(this).closest('fieldset').find('[data-citizenship-visa-status-relation]');
// 			var status = $(this).data('citizenship-visa-status');
// 			var shownFields = citizenshipFields.hide().filter('[data-citizenship-visa-status-relation="' + status + '"]').show();
// 			shownFields.find('select').each(function() {
// 				if(!$(this).closest('[data-autocomplete]').length){
// 					if(!$(this).data('defaultSelect')) {
// 						$(this).defaultSelect({
// 							wrapper: '.custom-select',
// 							textCustom: '.select__text',
// 							isInput: false
// 						});
// 					}
// 				}
// 			});

// 			// if($(this).closest('form').data('validatedOnce')) {
// 			// 	citizenshipFields.find('input, select, textarea').not(':hidden').each(function() {
// 			// 		$(this).closest('form').validate().element($(this));
// 			// 	});
// 			// }
// 			$(this).trigger('change.txtNextButton');
// 			citizenshipFields.find('[data-use-passport-expiry-date]').trigger('change.use-expiry-date');
// 		}).filter(':checked').trigger('change.change-citizenship');

// 		var chbUsePassportExpiryDate = $(this).find('[data-use-passport-expiry-date]');

// 		chbUsePassportExpiryDate
// 			.off('change.use-expiry-date')
// 			.on('change.use-expiry-date', function() {
// 				var passportExpiryDate = $($(this).data('use-passport-expiry-date')).closest('[data-autocomplete]');
// 				var checked = $(this).is(':checked');

// 				if(passportExpiryDate.length) {
// 					passportExpiryDate.find('input').prop('disabled', checked);
// 					passportExpiryDate.toggleClass('disabled', checked);
// 				} else {
// 					passportExpiryDate.find('select').prop('selectedIndex', 0).prop('disabled', checked).toggleClass('hidden', checked);
// 				}

// 				var validator = $(this).closest('form').data('validator');
// 				var validatedOnce = $(this).closest('form').data('validatedOnce');
// 				if((validator && validatedOnce) || checked) {
// 					passportExpiryDate.find('select').each(function(){validator.element(this);});
// 				}
// 				$(this).trigger('change.txtNextButton');

// 				/*passportExpiryDate
// 					.toggleClass('disabled', checked)
// 					.find('select')
// 					.prop('selectedIndex', 0)
// 					.prop('disabled', checked)
// 					.toggleClass('hidden', checked);
// 				var validator = $(this).closest('form').data('validator');
// 				var validatedOnce = $(this).closest('form').data('validatedOnce');
// 				if((validator && validatedOnce) || checked) {
// 					passportExpiryDate.find('select').each(function(){validator.element(this);});
// 				}
// 				$(this).trigger('change.txtNextButton');*/
// 			}).trigger('change.use-expiry-date');
// 	});

// 	$('.passenger-logged .btn-1').off('click.loginPassengerDetail').on('click.loginPassengerDetail', function(){
// 		var triggerLoginPoup = $('.menu-bar a.login');
// 		triggerLoginPoup.trigger('click.showLoginPopup');
// 	});

// 	var showPopupConfirm = function() {
// 		var regionPopupConfirm = $('[data-region-popup-confirm]'),
// 				popupConfirm = regionPopupConfirm.children();

// 		if(popupConfirm.filter('.hidden').length === popupConfirm.length) {
// 			regionPopupConfirm.addClass('hidden');
// 		} else {
// 			regionPopupConfirm.removeClass('hidden');
// 		}
// 	};

// 	showPopupConfirm();

// 	var populatePassportExpiry = function() {
// 		var cbxPassportExpiryDate = $('[data-use-passport-expiry-date]'),
// 				wrapPassportReference = $('[data-passport-reference]'),
// 				selectsPassportReference = wrapPassportReference.find('input[type="text"]');

// 		selectsPassportReference.off('blur.passportExpiry').on('blur.passportExpiry', function() {
// 			if(cbxPassportExpiryDate.is(':checked')) {
// 				var el = $(this),
// 						datePassportExpiry = cbxPassportExpiryDate.data('usePassportExpiryDate'),
// 						arrDateId;

// 				if(datePassportExpiry) {
// 					var dayEl, monthEl, yearEl;
// 					arrDateId = datePassportExpiry.split(',');

// 					if(el.closest('[data-day]').length) {
// 						dayEl = $(arrDateId[0]);
// 						dayEl.val(el.val());
// 						/*if(dayEl.data('defaultSelect')) {
// 							dayEl.data('defaultSelect').refresh();
// 						}*/
// 					} else if(el.closest('[data-month]').length) {
// 						monthEl = $(arrDateId[1]);
// 						monthEl.val(el.val());
// 						/*if(monthEl.data('defaultSelect')) {
// 							monthEl.data('defaultSelect').refresh();
// 						}*/
// 					} else if(el.closest('[data-year]').length) {
// 						yearEl = $(arrDateId[2]);
// 						yearEl.val(el.val());
// 						/*if(yearEl.data('defaultSelect')) {
// 							yearEl.data('defaultSelect').refresh();
// 						}*/
// 					}
// 				}
// 			}
// 		});

// 		cbxPassportExpiryDate.on('change.use-expiry-date', function() {
// 			selectsPassportReference.trigger('blur.passportExpiry');
// 		});
// 	};

// 	populatePassportExpiry();

// 	var frequentFlyerProgrammeEl = $('[data-check-kf-value]'),
// 			membershipNumberEl = $('[data-check-kf-field]');

// 	if(frequentFlyerProgrammeEl.length && membershipNumberEl.length) {
// 		membershipNumberEl.data('backupMsgRequired', membershipNumberEl.data('msgRequired'));
// 		frequentFlyerProgrammeEl.off('blur.checkKrisFlyer').on('blur.checkKrisFlyer', function() {
// 			if(this.value && this.value === $(this).data('checkKfValue')) {
// 				membershipNumberEl.data('ruleCheck_kf_membership_number', true);
// 				membershipNumberEl.data('msgRequired', L10n.validator.checkKfMembershipNumber);
// 			} else {
// 				membershipNumberEl.removeData('ruleCheck_kf_membership_number');
// 				membershipNumberEl.data('msgRequired', membershipNumberEl.data('backupMsgRequired'));
// 			}
// 		});
// 	}
// };
