/**
 * @name SIA
 * @description Define function to positioning specific block when window scrolling
 * @version 1.0
 */
SIA.forceInput = function() {
	var onlyDigitEl = $('[data-rule-digits]');
	var onlyCharEl = $('[data-rule-onlycharacter]');

	onlyCharEl.prop('autocomplete', 'off');

	onlyCharEl.off('keydown.forceInput').on('keydown.forceInput', function(e){
		var keyCode = e.keyCode;
		var allowKeyCode = [9, 8, 32, 37, 39, 46];
		if(keyCode === 9 && e.shiftKey){
			return true;
		}
		if(allowKeyCode.indexOf(keyCode) >= 0){
			return true;
		}
		if(keyCode < 65 || keyCode > 90){
			e.preventDefault();
		}
	});

	onlyDigitEl.off('keydown.forceInput').on('keydown.forceInput', function(e){
		var keyCode = e.keyCode;
		var allowKeyCode = [9, 8, 37, 39, 46];
		if(keyCode === 9 && e.shiftKey){
			return true;
		}
		if(e.ctrlKey || e.shiftKey || e.altKey){
			e.preventDefault();
		}
		if(allowKeyCode.indexOf(keyCode) >= 0){
			return true;
		}
		if((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106)){
			return true;
		}
		e.preventDefault();
	});
};
