/**
 * @name SIA
 * @description Define global SQCRegistration functions
 * @version 1.0
 */
SIA.sqcRegistration = function() {
	var countryEl = $('#country-region');
	var postCode = $('#postcode');
	var requiredChbGroup = $('[data-rule-chbsrequired]');
	var genderResEl = $('[data-gender-resource]');
	var genderRefEl = $('[data-gender-reference]');
	var travelProgram = $('#programme-select');
	var travelProgramDesc = $('[data-programme-desc]');
	var countryCity = $('#country-city');
	var fieldsSme = $('[data-programme="sme"]');
	// var validateRowEls = $('[data-validate-row]');

	var checkPostcode = function(){
		var countryVal = countryEl.val();
		// var postCode = $('#postcode');
		if(countryVal === 'Singapore (+65)'){
			postCode.attr('data-rule-required', 'true');
			postCode.attr('data-msg-required', 'Singapore must have a postcode!');
		}
		else{
			postCode.removeAttr('data-rule-required');
			postCode.removeAttr('data-msg-required');
		}
	};

	var detectGender = function(resourceElement, referenceElement) {
		if(!resourceElement.length || !referenceElement.length) {
			return;
		}
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		var doValidate = function(els){
	// 			var pass = true;
	// 			els.each(function(){
	// 				if(!pass){
	// 					return;
	// 				}
	// 				pass = $(this).valid();
	// 				// fix for checkin- sms
	// 				if(els.closest('[data-validate-col]').length && pass){
	// 					els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
	// 				}
	// 			});
	// 		};
	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 			formGroup.each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('input'));
	// 				}
	// 			});
	// 		});

	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.sqcRegistration').on('beforeSelect.sqcRegistration', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('input'));
	// 				}
	// 			});
	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 			self.data('change', true);
	// 		});

	// 		self.find('input, select').off('change.sqcRegistration').on('change.sqcRegistration', function(){
	// 			self.data('change', true);
	// 			$(this).valid();
	// 		}).off('blur.sqcRegistration').on('blur.sqcRegistration', function(){
	// 			if($(this).val()){
	// 				self.data('change', true);
	// 			}
	// 			else{
	// 				// fix for checkin- sms
	// 				if($(this).closest('[data-validate-col]').length){
	// 					doValidate($(this).closest('[data-validate-row]').find('input'));
	// 				}
	// 			}
	// 		});
	// 	});
	// };

	$.validator.addMethod('chbsRequired', function(value, el, param) {
		var chbs = $(param);
		return chbs.length && chbs.is(':checked');
	}, L10n.validator.selectOneOpt);

	$.validator.addMethod('checkWebUrl', function(value) {
		var reg = new RegExp('https?\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,}');
		return reg.test(value);
	}, L10n.validator.webUrl);

	countryEl.off('blur.registerForm').on('blur.registerForm', function(){
		checkPostcode();
	});

	requiredChbGroup.off('change.validateChb').on('change.validateChb', function() {
		$(this).valid();
	});

	var toggleFields = function(selected){

		/** switch programme description **/
		travelProgramDesc.hide();
		$('[data-programme-desc="'+selected+'"]').show();

		if(selected === 'sme'){
			travelProgram.prevAll('.select__text').text('Singapore Airlines SME Travel Programme');
			fieldsSme.show();
			return;
		}
		travelProgram.prevAll('.select__text').text('Singapore Airlines SQC Travel Programme');

		fieldsSme.hide();
		countryCity.trigger('blur');

	};

	var travelProgramme = function(){
		var selectProgramme = travelProgram.val();

		toggleFields(selectProgramme);

		travelProgram.off('change').on('change', function(){
			toggleFields(travelProgram.val());
		});

		countryCity.off('blur').on('blur', function(){
			var self = $(this);
			var selectProgramme = travelProgram.val();
			var countryValue = ['Singapore', 'Singapore - SIN'];

			if(selectProgramme !== 'sme'){
				if( countryValue.indexOf(self.val()) > -1 ){
					self.parents('.form-group').next().show();
				}else{
					self.parents('.form-group').next().hide();
				}
			}
		});

	};

	travelProgramme();

	// validateRowEls.off('blur.validateRow').on('blur.validateRow', 'input', function() {
	// 	var self = $(this);
	// 	var els = self.closest('[data-validate-row]').find('input').not(self);
	// 	els.valid();
	// });

	var initModule = function() {
		detectGender(genderResEl, genderRefEl);
		// validateFormGroup(validateRowEls);
	};

	initModule();
};
