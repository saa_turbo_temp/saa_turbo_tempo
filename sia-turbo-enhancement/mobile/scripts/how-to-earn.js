/**
 * @name SIA
 * @description Define global KFMileHowToEarn functions
 * @version 1.0
 */

SIA.KFMileHowToEarn = function(){
	var global = SIA.global;
	var config = global.config;

	var ajaxShowAccrualCalculator = function(){
		var form = $('.form-calculate-miles-1');
		var calculateBtn = $('#calculate');
		var accrualProcessClass = '.accrual-calculator-process';
		var accrualResultClass = '.accrual-calculator-result';
		var nodataEarn = '.checkin-alert.error-alert';
		var process = calculateBtn.closest(accrualProcessClass);
		var earnMiles = $('.block--earn-miles');
		if(earnMiles.find('.accrual-calculator-result').length) {
			earnMiles.removeClass('block--shadow').addClass('block--shadow');
		} else {
			earnMiles.removeClass('block--shadow');
		}
		var renderTemplate = function(data){
			if(process.siblings(accrualResultClass).length){
				process.siblings(accrualResultClass).remove();
			}
			if(process.siblings(nodataEarn).length){
				process.siblings(nodataEarn).remove();
			}
			if(data.origin){
				$.get(config.url.kfTableTemplate, function (html) {
					var template = window._.template(html, {
						data: data
					});
					$(template).insertAfter(process);
					earnMiles.removeClass('block--shadow').addClass('block--shadow');
				});
			}else {
				earnMiles.removeClass('block--shadow');
				$('<div class="alert-block checkin-alert error-alert"><div class="inner"><div class="alert__icon"><em class="ico-close-round-fill">&nbsp;</em></div><div class="alert__message">' + L10n.kfMiles.nodataEarn +'</div></div></div>').insertAfter(process);
			}
		};

		form.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			onfocusout: global.vars.validateOnfocusout,
			submitHandler: function(){
				$.ajax({
					url: window.ajaxhowtoEarn,
					dataType: 'json',
					type: global.config.ajaxMethod,
					success: function(data) {
						renderTemplate(data);
					},
					error: function(xhr, status) {
						if(status !== 'abort') {
							window.alert(L10n.flightSelect.errorGettingData);
						}
					}
				});
				return false;
			}
		});
	};

	var resetCustomeSelect = function(){
		var calculateMilesForm = $('.form-calculate-miles-1');
		calculateMilesForm.find('.custom-select input:text').val('');
	};

	ajaxShowAccrualCalculator();
	resetCustomeSelect();
};
