/**
 * @name SIA
 * @description Define global initAutocompleteCity functions
 * @version 1.0
 */
SIA.initAutocompleteCity = function(){
	var global = SIA.global;
	// var doc = global.vars.doc;
	var win = global.vars.win;
	// var	config = global.config;
	var body = global.vars.body;
	// var container = global.vars.container;
	var _autoComplete = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			// containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		// that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.autocompleteFields = that.options.autocompleteFields;
		that.airportData = that.options.airportData;
		that.cityData = that.options.cityData;
		that.timer = null;
		that.timerResize = null;
		that.winW = win.width();

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('[data-autocomplete]');
			var bookingAutoComplete = field.autocomplete({
					minLength : 0,
					open: that.options.open,
					change: that.options.change,
					select: that.options.select,
					close: that.options.close,
					search: that.options.search,
					response: that.options.response,
					// source: that.airportData,
					source: function(request, response) {
						/*// create a regex from ui.autocomplete for 'safe returns'
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
						// match the user's request against each destination's keys,

						var match = $.grep(that.airportData, function(airport) {
							var label = airport.label;
							var value = airport.value;

							return (matcher.test(label) || matcher.test(value)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
						});

						// ... return if ANY of the keys are matched
						response(match);*/

						if(!request.term) {
							response(that.airportData);
							return;
						}

						try {
							that.group = '';
							var matcher = new RegExp('(^' + $.ui.autocomplete.escapeRegex(request.term) + ')|(\\s' + $.ui.autocomplete.escapeRegex(request.term) + ')|([(]' + $.ui.autocomplete.escapeRegex(request.term) + ')', 'ig');
							var match1 = [];
							if(that.cityData.length){
								match1 = $.grep(that.cityData, function(airport) {
									var label = airport.label;

									return (matcher.test(label) || matcher.test(value));
								});
							}


							var match = $.grep(that.airportData, function(airport) {
								var label = airport.label;
								var value = airport.city;

								return (matcher.test(label) || matcher.test(value));
							});
							if(match1.length){
								response(match1.concat(match));
							}
							else{
								response(match);
							}
						} catch(err) {
							response(that.airportData);
						}
					},
					position: that.options.position,
					appendTo : that.options.autoCompleteAppendTo
				}).data('ui-autocomplete');
			bookingAutoComplete._renderItem = function (ul, item) {
				if(item.group){
					that.group = item.group;
					return $('<li class="group-item">'+ item.group +'</li>')
					.appendTo(ul);
				}else {
					// since all cities are under a country/group, if item.parent does not belong to that.options.group, you can just prepend to ul
					if(item.parent === that.group){
						return $('<li class="autocomplete-item">')
						.attr('data-value', item.city)
						.append('<a class="autocomplete-link">' + item.city +'</a>')
						.appendTo(ul);
					}else if(item.parent){
						return $('<li class="autocomplete-item redundancy">')
						.attr('data-value', item.city)
						.append('<a class="autocomplete-link">' + item.city +'</a>')
						.prependTo(ul);
					}
					return $('<li class="autocomplete-item">')
						.attr('data-value', item.city)
						.append('<a class="autocomplete-link">' + item.city +'</a>')
						.appendTo(ul);

				}
			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			// bookingAutoComplete._move = function() {
			// 	that.group = '';
			// };


			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function () {
				var self = $(this);
				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				win.off('resize.repositionAutocompleteCity').on('resize.repositionAutocompleteCity', function(){
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function(){
						if(field.autocomplete('widget').is(':visible')){
							// $('#travel-radio-3').siblings('label').text((field.offset().top - container.offset().top) + 'resize');
							// field.autocomplete('widget').css({
							// 	'left': field.closest('[data-autocomplete]').offset().left,
							// 	'top': field.closest('[data-autocomplete]').offset().top + field.closest('[data-autocomplete]').outerHeight(true)
							// });
							// for windows phone
							// if(window.navigator.msMaxTouchPoints){
							// 	self.autocomplete('widget').show().css({
							// 		'top': (field.offset().top - container.offset().top) + self.closest('[data-autocomplete]').outerHeight(true)
							// 	});
							// }
							if(that.winW !== win.width()){
								field.trigger('blur.highlight');
								that.winW = win.width();
							}
						}
					}, 100);
				});
			});
			field.off('blur.highlight').on('blur.highlight', function(){
				that.timer = setTimeout(function(){
					field.closest('.custom-select').removeClass('focus');
					field.autocomplete('close');
				}, 200);
				win.off('resize.repositionAutocompleteCity');
			});
			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
				clearTimeout(that.timer);
			});

			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					field.autocomplete('widget').find('li.active').trigger('click');
					e.preventDefault();
				}
			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
			// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
	};
	var initAutocompleteCity = function(){
		// init autocomplete
		var autocompleteEl = body.find('[data-autocomplete]');
		var timerAutocomplete = null;
		// var timerAutocompleteSearch = null;
		// var timerAutocompleteOpen = null;

		if(autocompleteEl.length){
			autocompleteEl.each(function(){
				var self = $(this),
						inputEl = self.find('input:text');

				if(self.data('init-automcomplete')){
					return;
				}
				if(self.find('input:text').is('[readonly]')){
					self.find('input:text').off('focus').off('click');
					return;
				}
				var select = self.find('select');
				var options = select.children();
				var data = [];
				var arrDataValue = [];
				var clearHideaddClear = null;

				self.data('minLength', 2);
				self.data('init-automcomplete', true);
				self.find('input:text').focus(function() {
					// $(this).val('');
					/*var dataAutocomplete = inputEl.data('uiAutocomplete');
					if(dataAutocomplete && inputEl.val()) {
						var indexItem = $.inArray(inputEl.val(), arrDataValue);
						clearTimeout(timerAutocompleteSearch);
						timerAutocompleteSearch = setTimeout(function() {
							if(indexItem === -1) {
								dataAutocomplete.search();
							} else {
								inputEl.autocomplete('widget').find('li').eq(indexItem).addClass('active');
							}
						}, 200);
					}*/
				});
				if(select.data('city-group')){
					var addData = function(els){
						els.each(function(){
							data.push({
								city: $(this).text(),
								label: $(this).text(),
								value: $(this).data('text'),
								parent: $(this).parent().attr('label')
							});
							arrDataValue.push({
								city: $(this).text(),
								label: $(this).data('text'),
								value: $(this).data('text')
							});
						});
					};
					options.each(function(){
						var self = $(this);
						if(self.attr('label')){
							data.push({
								city: self.attr('label'),
								label: self.attr('label'),
								group: self.attr('label'),
								// value: self.data('text')
								value: self.attr('label')
							});
						}
						else{
							data.push({
								city: self.text(),
								label: self.text(),
								value: self.data('text')
							});
						}
						if(self.children().length){
							addData(self.children());
						}
					});
				}
				else{
					options.each(function(){
						data.push({
							city: $(this).text(),
							label: $(this).text(),
							value: $(this).data('text')
						});
						// arrDataValue.push($(this).data('text'));
					});
				}
				select.off('change.selectCity').on('change.selectCity', function(){
					select.closest('[data-autocomplete]').find('input').val(select.find(':selected').data('text'));
				});

				var isFixed = false;
				self.parents().each(function() {
					isFixed = $(this).css('position') === 'fixed';
					return !isFixed;
				});

				_autoComplete({
					autocompleteFields : self.find('input:text'),
					autoCompleteAppendTo: body,
					airportData : data,
					cityData : arrDataValue,
					position: isFixed ? { collision: 'flip'} : { my: 'left top', at: 'left bottom', collision: 'none' },
					open: function(){
						var self = $(this);
						self.autocomplete('widget').find('.redundancy').remove();
						clearTimeout(clearHideaddClear);
						if(isFixed){
							self.autocomplete('widget').css({
								position: 'fixed'
							});
						}
						// self.addClear('hide');
						// self.autocomplete('widget').hide();
						// clearTimeout(timerAutocompleteOpen);
						// timerAutocompleteOpen = setTimeout(function(){
						// 	// self.autocomplete('widget').show();
						// 	// $('#travel-radio-3').siblings('label').text((self.offset().top - container.offset().top)  + 'open');
						// 	// self.autocomplete('widget').show().css({
						// 	// 	'left': self.closest('[data-autocomplete]').offset().left,
						// 	// 	'top': self.closest('[data-autocomplete]').offset().top + self.closest('[data-autocomplete]').outerHeight(true)
						// 	// });
						// 	// if(window.navigator.msMaxTouchPoints){
						// 	// 	self.autocomplete('widget').show().css({
						// 	// 		'top': (self.offset().top - container.offset().top) + self.closest('[data-autocomplete]').outerHeight(true)
						// 	// 	});
						// 	// }
						// }, 100);
					},
					response: function(event, ui){
						if(ui.content.length ===1){
							// $(this).val(ui.content[0].value);
							// $(this).select();
						}
						if(!ui.content.length) {
							ui.content.push({
								city: L10n.globalSearch.noMatches,
								label: L10n.globalSearch.noMatches,
								value: null
							});
						}
					},
					search: function(){
						// var self = $(this);
						// clearTimeout(timerAutocompleteSearch);
						// timerAutocompleteSearch = setTimeout(function(){
							// if(self.autocomplete('widget').find('li').length === 1){
							// 	self.autocomplete('widget').find('li').addClass('active');
							// }
						// }, 100);
					},
					close: function(){
						var self = $(this);
						self.closest('[data-autocomplete]').removeClass('focus');

						// if(self.rules().required && this.defaultValue && !this.value) {
						// 	self.val(this.defaultValue).valid();
						// }

						if(!self.val() || self.val() === self.attr('placeholder')){
							self.closest('[data-autocomplete]').addClass('default');
						}
						clearHideaddClear = setTimeout(function(){
							self.blur();
							self.addClear('hide');
						}, 200);

						if(self.data('autopopulate')){
							$(self.data('autopopulate')).val(self.val()).closest('[data-autocomplete]').removeClass('default');
							if(self.parents('.grid-col').hasClass('error')){
								$(self.data('autopopulate')).valid();
							}
						}

						if(self.data('autopopulateholder')){
							self.val(self.val() || self.data('autopopulateholder')).closest('[data-autocomplete]').removeClass('default');
							self.valid();
						}
					},
					select: function (event, ui) {
						// $(this).addClear('hide');
						var self = $(this),
								selfAutoComplete = self.parents('[data-autocomplete]');
						if(!ui.item.value) {
							window.setTimeout(function() {
								self.trigger('blur.triggerByGroupValidate');
							}, 400);
							return;
						} else {
							if(self.closest('#travel-widget').data('widget-v1') || self.closest('#travel-widget').data('widget-v2')) {
								$('#travel-widget .from-select, #travel-widget .to-select').not(selfAutoComplete).removeClass('default');
								if(selfAutoComplete.is('.from-select')) {
									$('#travel-widget .from-select').not(selfAutoComplete).find('input').val(ui.item.value);
								} else if(selfAutoComplete.is('.to-select')) {
									$('#travel-widget .to-select').not(selfAutoComplete).find('input').val(ui.item.value);
								}
							};
						}

						self.closest('[data-autocomplete]').removeClass('default');
						//var wrapper = self.closest('div');
						/*if(initLoading){
							loadingStatus.css({
								'width': wrapper.outerWidth(true),
								'height': wrapper.outerHeight(true),
								'top': wrapper.offset().top,
								'left': wrapper.offset().left,
								'display': 'block'
							});
						}*/
						if(self.parents('.from-select').length){
							self.closest('.form-group').data('change', true);
							if(self.parents('.from-to-container').find('.to-select').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').find('.to-select .ico-dropdown').trigger('click.triggerAutocomplete');
									},100);
								}
							}
							else{
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										//self.blur();
										// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
									}, 100);
								}
							}
						}
						if(self.parents('.to-select').length){
							if($('#travel-radio-4').length && $('#travel-radio-5').length && ($('#travel-radio-4').is(':visible') || $('#travel-radio-5').is(':visible'))){
								if(self.is('#city-2')){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										if($('#travel-radio-4').is(':checked')){
											self.blur();
											setTimeout(function(){
												$('#travel-start-day').focus();
												// $('#travel-start-day').closest('.input-3').trigger('click.showDatepicker');
											}, 300);
										}
										else if($('#travel-radio-5').is(':checked')){
											self.blur();
											setTimeout(function(){
												// self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
												self.closest('.form-group').siblings('[data-target]').find('[data-oneway]').focus();
											}, 300);
										}
										// if(self.closest('form').data('validator')){
										// 	self.closest('.form-group').find('input').valid();
										// }
									}, 100);
								}
							}
							else if(self.closest('form').find('[data-return-flight]').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
										if(self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')){
											self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
										}
										else{
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.blur();
										setTimeout(function(){
											// self.closest('form').find('[data-return-flight]').find('[data-start-date]').closest('.input-3').trigger('click.showDatepicker');
											if(self.closest('form').find('[data-return-flight]').find('[data-start-date]').is(':visible')){
												self.closest('form').find('[data-return-flight]').find('[data-start-date]').focus();
											}
											else{
												self.closest('form').find('.form-group').find('[data-oneway]').focus();
											}
										}, 300);
									}, 100);
								}
							}
							else if(self.closest('form').find('.form-group').length){
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
										self.closest('form').find('.form-group').find('[data-oneway]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.blur();
										setTimeout(function(){
											// self.parents('.from-to-container').siblings('.form-group').find('[data-oneway]').closest('.input-3').trigger('click.showDatepicker');
											self.closest('form').find('.form-group').find('[data-oneway]').focus();
										}, 300);
									}, 100);
								}
							}
							else if(self.parents('.from-to-container').children('[data-trigger-date]').length) {
								if(window.navigator.msMaxTouchPoints){
									// fix lumina 820 can not open autocomplete of To
									self.blur();
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									},500);
								}
								else{
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										self.parents('.from-to-container').children('[data-trigger-date]').find('[data-oneway]').focus();
									}, 100);
								}
							}
							else if(self.closest('[data-flight-schedule]').length){
								var dtfs = self.closest('[data-flight-schedule]');
								if(dtfs.find('[data-return-flight]').length){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										dtfs.find('[data-start-date]').focus();
									}, 500);
								}
								else if(dtfs.find('[data-oneway]').length){
									clearTimeout(timerAutocomplete);
									timerAutocomplete = setTimeout(function(){
										dtfs.find('[data-oneway]').focus();
									}, 500);
								}
							}
						}
						setTimeout(function(){
							if(self.closest('form').data('validator')){
								self.valid();
							}
							/*if(initLoading){
								loadingStatus.hide();
							}*/
							self.closest('[data-autocomplete]').siblings('.mobile').find('.select__text').text(ui.city).siblings('select').val(ui.value);
							self.trigger('change.programCode');
						}, 200);
					},
					setWidth: 0
				});

				if(inputEl.val() === '' || inputEl.val() === inputEl.attr('placeholder')){
					self.closest('[data-autocomplete]').addClass('default');
				} else {
					self.closest('[data-autocomplete]').removeClass('default');
				}
			});

			// win.off('touchmove.closeAutocomplete').on('touchmove.closeAutocomplete', function() {
			// 	body.find('.focus[data-autocomplete] input').trigger('blur.highlight');
			// });
		}
	};

	// init
	initAutocompleteCity();
};
