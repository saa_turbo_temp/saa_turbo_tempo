SIA.KFFlightHistory = function() {
	var global = SIA.global;
	var config = global.config;
	var tableHistory = $('[data-table-history]');
	var jsonData = globalJson.kfFlightHistory;
	var sorter = tableHistory.find('[data-link-sort]');
	var formFilter = {
		form: $('[data-form-filter]'),
		fields: {
			date : $('[data-form-filter] [data-start-date]'),
			from: $('[data-form-filter] [data-city-from] input'),
			to: $('[data-form-filter] [data-city-to] input')
		}
	};
	var btnShowMore = $('[data-see-more]');
	var showMoreCount = 0;
	var showNumber = 7;
	var htmlTpl = '';

	var renderTemplate = function(json, callback) {
		var sortAsc = tableHistory.data('sort-asc');
		if(sortAsc) {
			json = json.sort(function(a, b) {
				var dateA = new Date(a.shortDate);
				var dateB = new Date(b.shortDate);
				if (dateA < dateB) {
					return -1;
				}
				else if (dateA === dateB) {
					return 0;
				}
				else {
					return 1;
				}
			});
		}
		else {
			json = json.sort(function(a, b) {
				var dateA = new Date(a.shortDate);
				var dateB = new Date(b.shortDate);
				if (dateA < dateB) {
					return 1;
				}
				else if (dateA === dateB) {
					return 0;
				}
				else {
					return -1;
				}
			});
		}

		var template = window._.template(htmlTpl, {
			data: json,
			showNumber: showNumber
		});

		tableHistory.children('tbody').html(template);

		if(json.length > showNumber) {
			btnShowMore.text(L10n.kfSeemore.seeMore);
			btnShowMore.removeClass('hidden');
		}
		else {
			btnShowMore.addClass('hidden');
		}

		if(typeof(callback) === 'function') {
			callback();
		}
	};

	var renderTable = function(json, callback) {
		// tableHistory.children('tbody').empty();
		if (htmlTpl) {
			renderTemplate(json, callback);
		}
		else {
			$.get(config.url.kfFlightHistoryTemplate, function(tpl) {
				htmlTpl = tpl;
				renderTemplate(json, callback);
			});
		}
	};

	// var stTable = $('.table-responsive tbody');
	var sortTable = function() {
		sorter.off('click.sort').on('click.sort', function() {
			if(tableHistory.data('sort-asc') || typeof(tableHistory.data('sort-asc')) === 'undefined') {
				tableHistory.data('sort-asc', false);
			}
			else {
				tableHistory.data('sort-asc', true);
			}
			// sorter.find('em').toggleClass('ico-point-u ico-point-d');
			sorter.toggleClass('active');
			renderTable(jsonData);
			// var listItemTable = $('tr:not(:hidden)',stTable);
			// listItemTable.detach();
			// console.log(listItemTable);
			// if(listItemTable.length){
			// 	e.preventDefault();
			// 	if(sorter.hasClass('ascend')){
			// 		sorter.removeClass('ascend');
			// 		sorter.find('em').removeClass('ico-point-u').addClass('ico-point-d');
			// 	}
			// 	else{
			// 		sorter.addClass('ascend');
			// 		sorter.find('em').removeClass('ico-point-d').addClass('ico-point-u');
			// 	}
			// 	listItemTable = listItemTable.sort(function(a, b){
			// 		var d1 = new Date($(a).find('> td:first-child span').text());
			// 		var d2 = new Date($(b).find('> td:first-child span').text());
			// 		return sorter.hasClass('ascend') ? d2 - d1 : d1 - d2;
			// 	});
			// 	reStyle($(listItemTable));
			// }
		});
		// var reStyle = function(els){
		// 	els.each(function(idx){
		// 		var self = $(this);
		// 		self.removeClass();
		// 		if(idx%2 === 0){
		// 			self.addClass('odd');
		// 		}
		// 		else{
		// 			self.addClass('even');
		// 		}
		// 		self.appendTo(stTable);
		// 	});
		// };
	};

	var showMore = function() {
		var seeMore = function(isSeeAll) {
			if (isSeeAll) {
				tableHistory.find('tr.hidden').removeClass('hidden');
				showMoreCount = 0;
			}
			else {
				tableHistory.find('tr.hidden:lt(7)').removeClass('hidden');
			}

			showNumber = tableHistory.find('tbody tr:not(.hidden)').length;

			if(tableHistory.find('tr.hidden').length === 0) {
				btnShowMore.addClass('hidden');
				btnShowMore.text(L10n.kfSeemore.seeMore);
			}
		};

		btnShowMore.off('click.show-more').on('click.show-more', function(e) {
			e.preventDefault();
			showMoreCount++;

			if (showMoreCount < 2) {
				seeMore(false);
			}
			else if (showMoreCount === 2) {
				seeMore(false);
				btnShowMore.text(L10n.kfSeemore.seeAll);
			}
			else {
				seeMore(true);
			}
		});
	};

	var filter = function() {
		formFilter.form.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			onfocusout: global.vars.validateOnfocusout,
			ignore: ':hidden, [data-ignore]',
			submitHandler: function(){
				showMoreCount = 0;
				showNumber = 7;
				jsonData = $.grep(globalJson.kfFlightHistory, function(n) {
					var compareDate = true;
					var compareCityFrom = true;
					var compareCityTo = true;

					if(!formFilter.fields.date.datepicker('getDate') && formFilter.fields.date.val() !== formFilter.fields.date.attr('placeholder')) {
						compareDate = true;
					}
					else {
						var a = new Date(n.shortDate);
						var b = formFilter.fields.date.datepicker('getDate');
						compareDate = (a.getDate() === b.getDate() && a.getMonth() === b.getMonth() && a.getYear() === b.getYear());
					}
					if(!$.trim(formFilter.fields.from.val()) && $.trim(formFilter.fields.from.val()) !== formFilter.fields.from.attr('placeholder')) {
						compareCityFrom = true;
					}
					else {
						if(formFilter.fields.from.val().toLowerCase().indexOf(n.from.toLowerCase()) >= 0 || formFilter.fields.from.val().toLowerCase() === L10n.option.all) {
							compareCityFrom = true;
						}
						else {
							compareCityFrom = false;
						}
					}

					if(!$.trim(formFilter.fields.to.val()) && $.trim(formFilter.fields.to.val()) !== formFilter.fields.to.attr('placeholder')) {
						compareCityTo = true;
					}
					else {
						if(formFilter.fields.to.val().toLowerCase().indexOf(n.to.toLowerCase()) >= 0 || formFilter.fields.to.val().toLowerCase() === L10n.option.all) {
							compareCityTo = true;
						}
						else {
							compareCityTo = false;
						}
					}

					return compareDate && compareCityFrom && compareCityTo;
				});

				renderTable(jsonData);
				return false;
			}
		});
	};

	var initModule = function() {
		renderTable(jsonData);
		sortTable();
		showMore();
		filter();
	};

	initModule();
};
