/**
 * @name SIA
 * @description sqc-user
 * @version 1.0
 */
SIA.SQCUser = function() {
	var global = SIA.global,
		config = global.config,
		wrapMessage = $('[data-message]'),
		wrapListCheckbox = $('[data-wrapper-checkboxlist]'),
		listCheckbox = wrapListCheckbox.find(':checkbox'),
		btnCheckboxAll = $('[data-wrapper-checkboxall] input:checkbox'),
		btnMultiDelete = $('[data-multi-delete]'),
		btnSingleDelete = $('[data-single-delete]'),
		listUserDelete = $('[data-user-list]'),
		popupUserDelete = $('.popup--manage-user-delete'),
		hiddenUserDelete = $('#list-user-delete'),
		strUserId, strListUser;

	var ajaxSuccess = function(res) {
		var foreachItems = function(items, callback) {
			for(var i = items.length - 1; i >= 0; i--) {
				var currentItem = items[i];
				if($.parseJSON(currentItem.status)) {
					callback(wrapListCheckbox.find(':checkbox[value="' + currentItem.id + '"]').closest('tr'));
				}
			}
		};

		if(res.users) {
			foreachItems(res.users, function(item) {
				item.remove();
				wrapMessage.removeClass('hidden error-alert').find('.alert__message').text(L10n.sqcUser.msgSuccess);
			});
			popupUserDelete.data('Popup').hide();
		}
	};

	var ajaxFail = function() {
		wrapMessage.removeClass('hidden success-alert').addClass('error-alert').find('.alert__message').text(L10n.sqcUser.msgError);
		popupUserDelete.data('Popup').hide();
	};

	var initAjax = function(url, data, type, successFunc, errorFunc) {
		type = type || 'json';
		successFunc = successFunc || ajaxSuccess;
		errorFunc = errorFunc || ajaxFail;
		$.ajax({
			url: url,
			type: config.ajaxMethod,
			dataType: type,
			data: data,
			success: successFunc,
			error: errorFunc
		});
	};

	global.vars.checkAllList(btnCheckboxAll, wrapListCheckbox);

	popupUserDelete.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close]'
	});

	btnMultiDelete.off('click.deleteUser').on('click.deleteUser', function(e) {
		e.preventDefault();
		var checkboxChecked = wrapListCheckbox.find(':checkbox:checked');
		if(checkboxChecked.length) {
			strUserId = '';
			strListUser = '';
			checkboxChecked.each(function() {
				var checkboxEl = $(this);
				strUserId += this.name + '=' + checkboxEl.val() + '&';
				strListUser += '<li>' + checkboxEl.siblings('[data-sort-value]').data('sortValue') + '</li>';
			});
			hiddenUserDelete.val(strUserId.slice(0, -1));
			listUserDelete.html(strListUser);
			popupUserDelete.data('Popup').show();
		}
	});

	btnSingleDelete.off('click.deleteUser').on('click.deleteUser', function(e) {
		e.preventDefault();
		var checkboxEl = $(this).closest('tr').find(':checkbox');
		strUserId = checkboxEl.attr('name') + '=' + checkboxEl.val();
		strListUser = '<li>' + checkboxEl.siblings('[data-sort-value]').data('sortValue') + '</li>';
		hiddenUserDelete.val(strUserId);
		listUserDelete.html(strListUser);
		popupUserDelete.data('Popup').show();
	});

	popupUserDelete.find('form').off('submit.deleteUser').on('submit.deleteUser', function() {
		initAjax(config.url.sqcUserDelete, hiddenUserDelete.val());
		return false;
	});

	listCheckbox.add(btnCheckboxAll).off('change.checkboxChecked').on('change.checkboxChecked', function() {
		if(listCheckbox.filter(':checked').length) {
			btnMultiDelete.addClass('active');
		} else {
			btnMultiDelete.removeClass('active');
		}
	});

	var sortByDate = function() {
		var sortEl = $('[data-filter]');
		var sortMethod = '';
		var listItemEls = $();

		var sort = function(sortItems, sortMethod) {
			return sortItems.sort(function(a, b) {
				var dateA = new Date($(a).data('user-date'));
				var dateB = new Date($(b).data('user-date'));
				return sortMethod === 'oldest-user' ? (dateA > dateB ? 1 : -1) : (dateA > dateB ? -1 : 1);
			}).remove().each(function(idx, el) {
				var self = $(el);
				self.removeClass('odd even');
				if (idx % 2 === 0) {
					self.addClass('odd');
				}
				else {
					self.addClass('even');
				}
			});
		};

		sortEl.find('select').off('change.sortUser').on('change.sortUser', function() {
			sortMethod = $(this).val();
			listItemEls = wrapListCheckbox.find('tbody').find('tr').not(':hidden');
			wrapListCheckbox.prepend(sort(listItemEls, sortMethod));
		}).trigger('afterSelect.sortUser');
	};

	var initModule = function() {
		sortByDate();
	};

	initModule();
};
