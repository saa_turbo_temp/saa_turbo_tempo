/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'checkChange';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	function hookUnloadEvent(options) {
		window.onpagehide = function() {
			var message = options.iOsMessage;
			window.alert(message);
		};

		window.onbeforeunload = window.onbeforeunload || function (e) {
			// If we haven't been passed the event get the window.event
			e = e || window.event;

			var message = options.desktopMessage;

			// For IE6-8 and Firefox prior to version 4
			if (e) {
				e.returnValue = message;
			}

			// For Chrome, Safari, IE8+ and Opera 12+
			return message;
		};
	}

	Plugin.prototype = {
		init: function() {
			var self = this;

			if(self.element.data('check-change') === 'important') {
				hookUnloadEvent(self.options);
			}

			self.element.on('change.' + pluginName, ':input', function() {
				hookUnloadEvent(self.options);
			});

			if(self.element.is('form')) {
				self.element.on('submit.' + pluginName, function() {
					if(self.element.data('validator')) {
						if(self.element.data('validator').checkForm()) {
							window.onbeforeunload = window.onpagehide = null;
						}
					} else {
						window.onbeforeunload = window.onpagehide = null;
					}
				}).on('reset.' + pluginName, function() {
					if(self.element.data('check-change') !== 'important') {
						window.onbeforeunload = window.onpagehide = null;
					}
				});
			}

			var popupUnload = $('.popup--unload');
			if(!popupUnload.data('Popup')) {
				popupUnload.Popup({
					overlayBGTemplate: SIA.global.config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]',
					beforeShow: function(){
						popupUnload.data('parentContainerStyle', true);
					},
					afterHide: function(){
						if(popupUnload.data('parentContainerStyle')) {
							popupUnload.removeData('parentContainerStyle');
						}
					}
				});
			}

			$('body').off('click.leave-page').on('click.leave-page', self.options.prompClass, function(e) {
				e.preventDefault();
				var self = $(this);
				var href = self.attr('href');
				var target = self.attr('target');

				popupUnload
					.find('[data-confirm]')
					.off('click.unload')
					.on('click.unload', function() {
						window.onbeforeunload = window.onpagehide = null;
						if(href && href.indexOf('#') === 0){
							if($(href).length) {
								$('html, body').scrollTop($(href).offset().top);
							}
						}
						else {
							if (target && target === '_blank') {
								window.open(href, target);
							}
							else {
								window.location.href = href;
							}
						}
						popupUnload.Popup('hide');
					});

				popupUnload.Popup('show');
			});
		},
		changed: function() {
			hookUnloadEvent(this.options);
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.log(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		prompClass: 'a.prompt-user',
		desktopMessage: 'By choosing to leave this page, your selections will no longer be valid and you\'ll have to start over.',
		iOsMessage: 'By choosing to leave this page, your selections will no longer be valid and you\'ll have to start over.'
	};
}(jQuery, window));

SIA.FormCheckChange = function() {
	window.formsToCheck = window.formsToCheck || [];
	$('[data-check-change]').checkChange();
};

SIA.FormCheckChange();
