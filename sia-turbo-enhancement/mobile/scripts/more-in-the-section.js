/**
 * @name SIA
 * @description Define global moreInTheSection functions
 * @version 1.0
 */
SIA.moreInTheSection = function(){
	// var global = SIA.global;
	// var	config = global.config;
	// var	win = global.vars.win;
	var totalMobileSlide = 1;
	var	totalLandscapeSlide = 2;
	// var	timerDetectSlider;

	var initSliderMoreIn = function(){
		var blockMoreIn = $('[data-tablet-slider] .slides');
		blockMoreIn.each(function() {
			var slider = $(this);
			if(!slider.hasClass('slick-initialized')) {
				// slider.slick('unslick');
				slider
					.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: totalLandscapeSlide,
						slidesToScroll: totalLandscapeSlide,
						accessibility: false,
						arrows: false,
						responsive: [
							{
								breakpoint: 480,
								settings: {
									slidesToShow: totalMobileSlide,
									slidesToScroll: totalMobileSlide
								}
							}
						]
					});
			}
		});
	};

	var initModule = function(){
		initSliderMoreIn();
	};

	initModule();
};
