/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.flightSelect = function(){
	if(!$('.flight-select-page').length){
		return;
	}
	var global = SIA.global;
	var config = global.config;
	var previousBtn = $('a.wi-icon.wi-icon-previous');
	var nextBtn = $('a.wi-icon.wi-icon-next');
	var flightsSearch = $('div.flights__searchs');
	var flightsTable = flightsSearch.find('table.flights__table');
	var marginLeftPreBtn = Math.round(previousBtn.width()/2);
	var marginTopPreBtn = Math.round(previousBtn.height()/2);
	var marginTopNexBtn = Math.round(nextBtn.height()/2);
	var btnNext = $('#btn-next');
	var checked2Flights = false;
	var fareLink = $('[data-toggle-link]');
	var flightInfo = $('[data-toggle-content]');
	var flyingFocus = $('#flying-focus');
	var buttonShowMore = $('[data-see-more]');

	// var getFlightClassFromId = function(id) {
	// 	var classes = {
	// 		'FF32' : 'Flexi Saver 2 to go',
	// 		'FF42' : 'Sweet Deals 2 to go',
	// 		'FF44' : 'Sweet Deals 4 to go',
	// 		'FF1' : 'Fully Flexi',
	// 		'FF2' : 'Flexi',
	// 		'FF3' : 'Flexi Saver',
	// 		'FF4' : 'Sweet Deals',
	// 		'FF5' : 'Super Deals',
	// 		'FF6' : 'Business',
	// 		'FF8' : 'First/Suites',
	// 		'FC1' : 'Corporate Economy',
	// 		'FC2' : 'Corporate Business',
	// 		'FC3' : 'Corporate First',
	// 		'FD1' : 'Economy Class - Special Fares',
	// 		'FD2' : 'Business Class - Special Fares',
	// 		'FD3' : 'First Class - Special Fares',
	// 		'FD4' : 'Credit Card Promotion - Economy',
	// 		'FD5' : 'Credit Card Promotion - Business',
	// 		'FD6' : 'Credit Card Promotion - First'
	// 	};
	// 	var id = id.substring(2, 5);
	// 	if(classes[id]) {
	// 		return classes[id];
	// 	}
	// 	else {
	// 		id = id.substring(0, 2);
	// 		return classes[id] ? classes[id] : '';
	// 	}
	// };

	var initTableTooltip = function(table) {
		table.find('[data-tooltip]').each(function(){
			if(!$(this).data('tooltip-initialized')) {
				if(!$(this).data('kTooltip')){
					$(this).kTooltip({
						afterShow: function(tt){
							var tp = $('[data-trigger-popup]', tt);
							tp.each(function(){
								var self = $(this);
								var popup = $(self.data('trigger-popup'));
								if(!popup.data('Popup')){
									popup.Popup({
										overlayBGTemplate: config.template.overlay,
										modalShowClass: '',
										triggerCloseModal: '.popup__close, [data-close]',
										afterShow: function(){
											flyingFocus = $('#flying-focus');
											if(flyingFocus.length){
												flyingFocus.remove();
											}
										}
									});
								}
								self.off('click.showPopup').on('click.showPopup', function(e){
									e.preventDefault();
									tt.hide();
									popup.Popup('show');
								});
							});
						}
					});
				}
				$(this).data('tooltip-initialized', true);
			}
		});
	};

	initTableTooltip(flightsTable);

	buttonShowMore
	.off('click.show-more')
	.on('click.show-more', function() {
		var idx = $(this).data('see-more');

		var table = flightsSearch.filter('[data-flight="' + idx + '"]');
		table.find('table.flights__table > tbody').children('.hidden:lt(6)').removeClass('hidden');
		if(!table.children('.hidden').length) {
			$(this).addClass('hidden');
		}
	}).each(function() {
		var idx = $(this).data('see-more');

		var table = flightsSearch.filter('[data-flight="' + idx + '"]');
		if(table.find('table.flights__table > tbody').children('.hidden').length === 0) {
			$(this).addClass('hidden');
		}
	});

	/*  Switch active state between rows */
	flightsTable.on('change.select-flight', 'input[type="radio"]', function() {
		var self = $(this);
		var tableRow = self.parents('tr').last();

		var waitlistedBlock = $(this).closest('.flights__table').find('.waitlisted');
		var waitlistedBlockMessage = waitlistedBlock.find('.message-waitlisted');

		if(self.data('waitlisted')) {
			waitlistedBlock.removeClass('hidden');
			if(globalJson.bookingSummary && globalJson.bookingSummary.pwmMessage) {
				var pwmMessage = '<em class="ico-checkbox"></em>';
				pwmMessage += globalJson.bookingSummary.pwmMessage[0][self.val()];
				waitlistedBlockMessage.html(pwmMessage);
			}
		}
		else {
			waitlistedBlock.addClass('hidden');
		}

		var classFlight = tableRow.find('.flights__info--group .class-flight');
		if(!tableRow.is('.active')) {
			tableRow.addClass('active').siblings('.active').removeClass('active');
		}

		var related = self.data('related');
		var relatedRdo = tableRow.find('[data-related="' + related + '"]').not(self);
		relatedRdo.prop('checked', true);

		checked2Flights = flightsTable.filter(function() {
			return $(this).find('input[type="radio"]:checked').length > 0;
		}).length === flightsTable.length;

		btnNext.prop('disabled', !checked2Flights).toggleClass('disabled', !checked2Flights);
		classFlight.text(self.data('flight-class'));

		self.closest('table.flights__table').find('.flights__info--price').removeClass('active');
		if (relatedRdo.closest('.flights__info--price').length) {
			relatedRdo.closest('.flights__info--price').addClass('active');
		}
		if(self.closest('.flights__info--price').length) {
			self.closest('.flights__info--price').addClass('active');
		}
	});

	flightsTable.off('click.showInformation').on('click.showInformation', '.flights--detail span', function(){
		var infFlightDetail = $(this).next();
		var self = $(this);

		if(infFlightDetail.is('.hidden')) {
			infFlightDetail.removeClass('hidden').hide();
		}

		// self.children('em').toggleClass('ico-point-d ico-point-u');
		self.toggleClass('active');

		if(!infFlightDetail.is(':visible')) {
			self.children('em').addClass('hidden');
			self.children('.loading').removeClass('hidden');
			$.ajax({
				url: global.config.url.flightSearchFareFlightInfoJSON,
				dataType: 'json',
				type: global.config.ajaxMethod,
				data: {
					flightNumber: self.parent().data('flight-number'),
					carrierCode: self.parent().data('carrier-code'),
					date: self.parent().data('date'),
					origin: self.parent().data('origin')
				},
				success: function(data) {
					var flyingTime = '';
					for(var ft in data.flyingTimes){
						flyingTime = data.flyingTimes[ft];
					}
					var textAircraftType = L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType;
					var textFlyingTime = L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime;
					var rowAircraftType = $('<p>' + textAircraftType + '</p>');
					var rowFlyingTime = $('<p>' + textFlyingTime + '</p>');
					infFlightDetail.empty();
					infFlightDetail.append(rowAircraftType, rowFlyingTime);
					infFlightDetail.stop().slideToggle(400);
				},
				error: function(xhr, status) {
					if(status !== 'abort'){
						window.alert(L10n.flightSelect.errorGettingData);
					}
				},
				complete: function() {
					self.children('em').removeClass('hidden');
					self.children('.loading').addClass('hidden');
				}
			});
		}
		else {
			infFlightDetail.stop().slideToggle(400);
		}
	});

	previousBtn.css({'left':'50%','top':'50%','margin-left':-marginLeftPreBtn,'margin-top':-marginTopPreBtn}).hide();
	previousBtn.off('click.showHidenFare').on('click.showHidenFare',function(){
		flightsTable.find('thead th.title-head.hidden').removeClass('hidden');
		flightsTable.find('thead th.title-head.hidden-mb').eq(-2).addClass('hidden');
		flightsTable.find('thead th.title-head.active').removeClass('active');
		flightsTable.find('thead th.title-head').eq(-3).addClass('active');
		nextBtn.show();
		previousBtn.hide();
	});
	nextBtn.css({'left':'85%','top':'50%','margin-top':-marginTopNexBtn});
	nextBtn.off('click.showHidenFare').on('click.showHidenFare',function(){
		flightsTable.find('thead th.title-head.hidden').removeClass('hidden');
		flightsTable.find('thead th.title-head.hidden-mb:first').addClass('hidden');
		flightsTable.find('thead th.title-head.active').removeClass('active');
		flightsTable.find('thead th.title-head:first').addClass('active');
		previousBtn.show();
		nextBtn.hide();
	});
	flightInfo.each(function(){
		if($(this).hasClass('visible-mb')){
			$(this).parent().find(fareLink).addClass('active');
		}
	});
	flightsTable.off('click.showHidenFares').on('click.showHidenFares', '.flights__info a.link-5', function(e){
		e.preventDefault();
		var link = $(this);
		var showFlightInfoMD = link.parent().parent().find('.flights__info--mb');

		showFlightInfoMD.toggleClass('visible-mb');
		link.toggleClass('active');
	});


	var popupActions = function(){
		// init after render html template
		var popupEditSearch = $('.popup--edit-search');
		var popupCompare = $('.popup--search-compare');
		var triggerEditSearch = $('.flights__target a.search-link');
		var triggerCompare = $('a.btn-compare');
		var flyingFocus = $('#flying-focus');

		var radioTab = function(wp, r, t){
			wp.each(function(){
				var radios = wp.find(r);
				var tabs = wp.find(t);
				radios.each(function(i){
					var self = $(this);
					self.off('change.selectTabs').on('change.selectTabs', function(){
						tabs.removeClass('active').eq(i).addClass('active');
					});
				});
			});
		};
		radioTab(popupEditSearch, '[data-search-flights]', '.widget-edit-search');

		if(!popupEditSearch.data('Popup')){
			popupEditSearch.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function(){
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				},
				beforeHide: function() {
					// popupEditSearch.find('[data-customselect]').customSelect('hide');
				},
				closeViaOverlay: false
			});
		}

		triggerEditSearch.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
			e.preventDefault();
			popupEditSearch.Popup('show');
		});

		if(!popupCompare.data('Popup')){
			popupCompare.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function(){
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				}
			});
		}

		triggerCompare.off('click.triggerCompare').on('click.triggerCompare', function(e){
			e.preventDefault();
			popupCompare.Popup('show');
		});
	};

	var groupTooltip = function(){
		var groupTooltips = $('.form-group--tooltips');
		groupTooltips.each(function(){
			var self = $(this);
			var radioFilter = self.find('.custom-radio input[type="radio"]');
			var radioTooltips = self.find('.radio-tooltips');
			//var cityFrom = formTravel.find('#city-1');
			//var cityTo = formTravel.find('#city-2');

			radioFilter.each(function(index, el){
				$(el).off('change.showTooltip').on('change.showTooltip', function(){
					radioTooltips.removeClass('active');
					radioTooltips.eq(index).addClass('active');
				});
			});
		});
	};

	popupActions();
	groupTooltip();

	var sortData = function() {
		var filter = $('[data-filter]');
		filter.find('select').off('change.sort').on('change.sort', function() {
			var self = $(this);
			flightsSearch.each(function() {
				var table = $(this).find('.flights__table');
				var rows = table.find('> tbody > tr');
				var visibleRowsNumber = rows.filter(':visible').length;
				var sortBy = self.val();
				switch(sortBy) {
					case 'recommended':
						rows.sort(function(a, b) {
							if($(a).data('recommended') > $(b).data('recommended')) {
								return 1;
							}
							if($(a).data('recommended') < $(b).data('recommended')) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'price-asc':
						rows.sort(function(a, b) {
							var priceA = window.accounting.unformat($(a).data('price'));
							var priceB = window.accounting.unformat($(b).data('price'));
							if(priceA > priceB) {
								return 1;
							}
							else if (priceA < priceB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'travel-asc':
						rows.sort(function(a, b) {
							if($(a).data('travel-time') > $(b).data('travel-time')) {
								return 1;
							}
							else if($(a).data('travel-time') < $(b).data('travel-time')) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'departure-asc':
						rows.sort(function(a, b) {
							var departA = new Date($(a).data('departure-time'));
							var departB = new Date($(b).data('departure-time'));
							if(departA > departB) {
								return 1;
							}
							else if(departA < departB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'departure-desc':
						rows.sort(function(a, b) {
							var departA = new Date($(a).data('departure-time'));
							var departB = new Date($(b).data('departure-time'));
							if(departA < departB) {
								return 1;
							}
							else if(departA > departB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'arrival-asc':
						rows.sort(function(a, b) {
							var arrivalA = new Date($(a).data('arrival-time'));
							var arrivalB = new Date($(b).data('arrival-time'));
							if(arrivalA > arrivalB) {
								return 1;
							}
							else if(arrivalA < arrivalB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'arrival-desc':
						rows.sort(function(a, b) {
							var arrivalA = new Date($(a).data('arrival-time'));
							var arrivalB = new Date($(b).data('arrival-time'));
							if(arrivalA < arrivalB) {
								return 1;
							}
							else if(arrivalA > arrivalB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					default:
						break;
				}
				rows.removeClass('hidden even-item').each(function(rowIdx, row) {
					if(rowIdx % 2 !== 0) {
						$(row).addClass('even-item');
					}
					if(rowIdx > visibleRowsNumber - 1) {
						$(row).addClass('hidden');
					}
				});
				rows.detach().appendTo(table.children('tbody'));
			});
		});
	};

	sortData();
};
