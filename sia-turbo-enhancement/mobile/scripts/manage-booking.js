/**
 * @name SIA
 * @description manage booking functions
 * @version 1.0
 */
SIA.manageBooking = function(){
	var global = SIA.global;
	var upgradeFlightForm = $('#form-choose-flight-upgrade');
	var body = $('body');
	var win = global.vars.win;
	/*var upgradeMile = function() {
		var btnUpgradeMile = $('[data-disable-click]');

		btnUpgradeMile.each(function() {
			var that = $(this),
					tooltipData = that.data('kTooltip');
			if(tooltipData) {
				tooltipData.options.afterShow = function() {
					that.addClass('disabled');
				};
				tooltipData.options.afterClose = function() {
					that.removeClass('disabled');
				};
			}
		});
	};

	upgradeMile();*/

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	var paramUrl = sURLVariables[1];
	var sName = paramUrl ? paramUrl.split('=')[1] : '';

	// Get params from URL
	var getURLParams = function(sParam) {
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1];
			}
		}
	};

	// Get Ajax and fill content fo Addon
	var renderAddOn = function(){
		var fillContent = function(res) {
			var listAddOns = $('.add-ons__list');
			if(listAddOns.length){
				$.get(global.config.url.addOnMbTemplate, function (data) {
					var template = window._.template(data, {
						data: res
					});
					listAddOns.html(template);
					listAddOns
						.off('click.disabledLink')
						.on('click.disabledLink', '[data-disabled-link]', function(e) {
							if ($(this).hasClass('disabled')) {
								e.preventDefault();
								e.stopImmediatePropagation();
							}
						});
				}, 'html');
			}
		};
		if(getURLParams('addon')){
			$.get('ajax/JSONS/' + sName, function(data){
				fillContent(data);
			});
		}
		else{
			fillContent(globalJson.dataAddOn);
		}
	};

	upgradeFlightForm.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: function(label, element){
			global.vars.validateSuccess(label, element);
		}
	});

	// Render html for Baggage and Meals
	var getBaggageMealInfo = function() {
		var bookingDetails = $('[data-pax-id]');
		if (bookingDetails.length) {
			$.ajax({
				url: globalJson && globalJson.bagMealUrl ? globalJson.bagMealUrl.mealUrl :
					global.config.url.manageBookingMealJSON,
				dataType: 'json',
				success: function(mealJson){
					if (mealJson && mealJson.MealVO &&
						mealJson.MealVO.passengerAndMealAssociationVO &&
						mealJson.MealVO.passengerAndMealAssociationVO.length) {
						renderMealTpl(mealJson.MealVO);
					}
				},
				error: function() {
					window.alert(L10n.manageBooking.mealAjaxErr);
				}
			});

			$.ajax({
				url: globalJson && globalJson.bagMealUrl ? globalJson.bagMealUrl.bagUrl :
					global.config.url.baggageJSON,
				dataType: 'json',
				success: function(bagJson){
					if (bagJson && bagJson.excessBaggage && bagJson.excessBaggage.length) {
						renderBagTpl(bagJson.excessBaggage);
					}
				},
				error: function() {
					window.alert(L10n.manageBooking.bagAjaxErr);
				}
			});
		}

		// Render Meals
		var renderMealTpl = function(mealJson) {
			if (bookingDetails.length) {
				$.get(global.config.url.bookingDetailMealTemplate, function (mealTpl) {
					bookingDetails.each(function() {
						var self = $(this);
						var paxId = self.data('pax-id');
						var segmentId = self.data('segment-id');
						var isInf = !!self.data('infrant');

						var mealHtml = window._.template(mealTpl, {
							data: getMealInfo(mealJson.passengerAndMealAssociationVO, paxId,
								segmentId, isInf)
						});

						var bookingBlocks = self.find('.booking-details');
						if (bookingBlocks.length) {
							$(mealHtml).insertAfter(bookingBlocks.eq(0));
						}
						else {
							self.prepend(mealHtml);
						}
					});
				}, 'html');
			}
		};

		// Get Meals infomation
		var getMealInfo = function(paxAndMealVo, paxId, segmentId, isInf) {
			var i = 0;
			for (i = 0; i < paxAndMealVo.length; i++) {
				var meal = paxAndMealVo[i];
				if (meal.passengerId === paxId &&
					(meal.passengerType === 'INF') === isInf &&
					meal.flightDateInformationVO && meal.flightDateInformationVO.length) {
					return {
						mealInfo: getMealDetail(meal.flightDateInformationVO, segmentId),
						paxId: paxId
					};
				}
			}
			return null;
		};

		// Get details Meals infomation
		var getMealDetail = function(flightDateInfo, segmentId) {
			var i = 0;
			for (i = 0; i < flightDateInfo.length; i++) {
				var info = flightDateInfo[i];
				if (info.segmentID === segmentId) {
					return info;
				}
			}
			return null;
		};

		// Render baggage
		var renderBagTpl = function(bagJson) {
			if (bookingDetails.length) {
				$.get(global.config.url.bookingDetailBagTemplate, function (bagTpl) {
					bookingDetails.each(function() {
						var self = $(this);
						var paxId = self.data('pax-id');
						var segmentId = self.data('segment-id');
						var isInf = !!self.data('infrant');

						var bagHtml = window._.template(bagTpl, {
							data: getBaggageInfo(bagJson, paxId, segmentId, isInf)
						});

						self.append(bagHtml);
					});
				}, 'html');
			}
		};

		// Get Baggage infomation
		var getBaggageInfo = function(bagJson, paxId, segmentId, isInf) {
			var i = 0;
			for (i = 0; i < bagJson.length; i++) {
				var bag = bagJson[i];
				if (bag.id === paxId && (bag.paxType === 'INF') === isInf) {
					return {
						segmentDetail: getSegmentDetail(bag.segmentDetails, segmentId),
						paxId: paxId
					};
				}
			}
			return null;
		};

		// Get details Segment
		var getSegmentDetail = function(segDetails, segmentId) {
			if (segDetails && segDetails.length) {
				var i = 0;
				for (i = 0; i < segDetails.length; i++) {
					var segment =  segDetails[i];
					if (segment.segmentID === segmentId) {
						return segment;
					}
				}
				return null;
			}
			return null;
		};
	};

	// Get JSON and render html for flight infomation
	var getFlightInfo = function() {
		$('.flights--detail > span').off('click.get-flight-info').on('click.get-flight-info', function() {
			var self = $(this);
			self.toggleClass('active');
			if(self.siblings('.details').is('.hidden')) {
				$.ajax({
					url: SIA.global.config.url.flightSearchFareFlightInfoJSON,
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('data-date'),
						origin: self.parent().data('origin'),
					},
					type: SIA.global.config.ajaxMethod,
					dataType: 'json',
					beforeSend: function() {
						self.children('em').addClass('hidden');
						self.children('.loading').removeClass('hidden');
					},
					success: function(data) {
						var flyingTime = '';
						for(var ft in data.flyingTimes){
							flyingTime = data.flyingTimes[ft];
						}

						var html = '';
						html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType + '</p>';
						html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime + '</p>';

						self.siblings('.details').removeClass('hidden').html(html).css('display', 'none').slideDown(400);

					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						window.alert(errorThrown);
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				$.when(self.siblings('.details').slideUp(400)).done(function(){
					self.siblings('.details').addClass('hidden');
				});
				// self.children('em').toggleClass('ico-point-u ico-point-d');
			}
		});
	};

	var renderAddOnSales = function(){
      var fillContent = function(res) {
          var listAddOns = $('.add-ons__list');
          if(listAddOns.length){
              $.get(global.config.url.addOnMbTemplateSales, function (data) {
                  var template = window._.template(data, {
                      data: res
                  });
                  listAddOns.html(template);
              }, 'html');
          }
      };
      if(getURLParams('addon')){
          $.get('ajax/JSONS/' + sName, function(data){
              fillContent(data);
          });
      }
      else{
          fillContent(globalJson.dataAddOn);
      }
      setTimeout(function() {
          win.trigger('resize.sortItemMB');
      }, 2000);
  };

	var renderNoAddOnSales = function(){
      return false;
  };

	var renderHotelAddOnSales = function(){
      var fillContent = function(res) {
          var listAddOns = $('.add-ons__list');
          if(listAddOns.length){
              $.get(global.config.url.addOnMbHotelTemplateSales, function (data) {
                  var template = window._.template(data, {
                      data: res
                  });
                  listAddOns.html(template);

                  var totalImg = $(template).find('img');
									var countImage = 0;
									if(totalImg.length){
										totalImg.each(function(){
											var that = $(this);
											var img = new Image();
											img.onload = function(){
												countImage ++;
												if(countImage === totalImg.length){
													sortItem();
												}
											};
											img.onerror = function(){
												sortItem();
											};
											img.src = that.attr('src');
										});
									}

              }, 'html');
          }
      };
      if(getURLParams('addon')){
          $.get('ajax/JSONS/' + sName, function(data){
              fillContent(data);
          });
      }
      else{
          fillContent(globalJson.dataAddOn);
      }
      setTimeout(function() {
          win.trigger('resize.sortItemMB');
      }, 2000);
  };

  var renderCarAddOnSales = function(){
		var templateBookingCar;
		var appendAfterDiv = $('.add-ons__list--sales');
		var CarDetailAddOnSales = function(data1){
	    $.get(global.config.url.bookingDetailCarResponse, function (data) {
	      var template = window._.template(data, {
	        data: data1
	      });
	      templateBookingCar = $(template);
   			appendAfterDiv.append(templateBookingCar);
	    });
		};
		$.ajax({
      url: global.config.url.bookingDetailResponse,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
      	var data2 = reponse.response;
        CarDetailAddOnSales(data2);
      }
	  });
	};

	var initModule = function(){
		getFlightInfo();
		getBaggageMealInfo();

		if(body.is('.mb-addon-sales')) {
        renderAddOnSales();
        renderCarAddOnSales();
    } else if(body.is('.mb-no-addon-sales')) {
        renderNoAddOnSales();
    } else if(body.is('.mb-hotel-addon-sales')){
    		renderHotelAddOnSales();
    } else{
    	renderAddOn();
    }
	};

	initModule();
};
