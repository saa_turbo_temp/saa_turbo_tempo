/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.payment = function(){

	var global = SIA.global;
	// var config = global.config;
	var win = global.vars.win;
	var isPreventLoadJson = false;
	var slide = $('#slider-range');
	var minus = slide.siblings('.ico-minus');
	var plus = slide.siblings('.ico-plus');
	var krisMin = slide.data('kris-min');
	var krisMax = slide.data('kris-max');
	var cMin = slide.data('current-min');
	var cMax = slide.data('current-max');
	var cVal = slide.data('current-value');
	var cStep = slide.data('step');
	var krisUserMax = slide.data('krisuser-max');
	var rateMilesCurrency = parseFloat(eval(slide.data('rate-miles-currency')));
	var rateIncrement = parseFloat(eval(slide.data('increment')));
	var rateDecrement = parseFloat(eval(slide.data('decrement')));
	var paymentDetail = $('.payments-detail');
	var triggerOpenPaymentSlider = paymentDetail.find('.payments-heading .custom-checkbox :checkbox');
	var paymentSlider = $('.payments-group__slider', paymentDetail);
	var stripMax = slide.siblings('.stripe-2');
	var stripMin = slide.siblings('.stripe-1');
	var mile = $('[data-mile]', paymentDetail);
	var mileTemplate = mile.length ? mile.html() : '';
	var payment = $('[data-payment]', paymentDetail);
	var paymentTemplate = payment.length ? payment.html() : '';
	var grandTotal = $('[data-grandtotal]', paymentDetail);
	var grandTotalTemplate = grandTotal.length ? grandTotal.html() : '';
	var bookingSummary = $('.booking-summary');
	var flyingFocus = $('#flying-focus');

	var acceptCon = $('#accept-cb');
	// var btnMakePayment = $('[name="make-payment"]');

	var krisflyerAccount = $('.krisflyer-account');
	var formPayment = $('#payments-detail__form');
	var customSelect = $('.custom-select');

	var triggerShowKrisInfo = $('#krisflyer-account-cb');
	var currentCurrency = 'SGD';
	var defaultCurrency = 'SGD';

	var switchKfPayment = $('[data-switch-payment]');
	var contentKf = $('[data-content-krisflyer]');
	var creditDebitNumber = $('#credit-debit-number');

	var enableToggler = $('.payment-currency [data-toggler] input');

	var autoDetectCard = $('[data-auto-detect-card]');

	stripMax.hide();
	stripMin.hide();

	var renderCurrencySelect = function(cardType){
		if(globalJson.currencyRate) {
			var dataRate = $('[data-rate] select');
			var getCardData = globalJson.currencyRate.mcpCards[cardType] ? globalJson.currencyRate.mcpCards[cardType] : [];
			dataRate.empty();
			var options = [];
			for(var i = 0; i < getCardData.length; i++){
				options.push('<option '+ ((i === 0) ? 'selected' : '') +' value="' + getCardData[i].currencyCode +'"' + '>' + getCardData[i].currencyCode + '</option>');
			}
			dataRate.html(options.join(''));
			dataRate.defaultSelect('refresh');
			globalJson.exchangeRate = {};
			for (var x in getCardData) {
				globalJson.exchangeRate[getCardData[x].currencyCode] = getCardData[x].conversionRate;
			}
			currentCurrency = dataRate.val();
			defaultCurrency = dataRate.val();
			dataRate.trigger('change.exchange');
			// console.log(currentCurrency);
		}
		else{
			globalJson.exchangeRate = {};
		}
	};

	var detectCardTypeAndRenderCurrency = function(control){
		var block = $('.payment-currency');

		control.off('change.detectCardType').on('change.detectCardType', function(){
			var debitCardNumber = $(this).val();
			var cardType = '';
			if ((/^3(?:0[0-5]|[68][0-9])/i).test(debitCardNumber)) {
				//Diners card
				cardType = 'DC';
			} else if ((/^3[47]/i).test(debitCardNumber)) {
				//Amex
				cardType = 'AX';
			} else if ((/^4/i).test(debitCardNumber)) {
				//Visa
				cardType = 'VI';
			} else if  ((/^5[1-5]/i).test(debitCardNumber)) {
				//Master Card
				cardType = 'MC';
			}
			else if ((/^(?:2131|1800|35)/i).test(debitCardNumber)) {
				//JCB
				cardType = 'JC';
			}
			if(cardType){
				renderCurrencySelect(cardType);
				block.show();
			}
			else{
				block.hide();
			}
		}).trigger('change.detectCardType');
	};
	// renderCurrencySelect('MC');

	detectCardTypeAndRenderCurrency(creditDebitNumber);

	var costPayableByCash = 0;

	var changeContentKrisFlyer = function(sliderVal) {
		if($('body').is('.kf-payments-page')) {
			costPayableByCash = window.accounting.unformat($('[data-source]').val());
		}
		// var enableToggler = $('.payment-currency [data-toggler] input');
		mile.html(mileTemplate.format(accounting.formatNumber(sliderVal)));
		var costMiles = globalJson.bookingSummary ? globalJson.bookingSummary.bookingSummary.costPayableByMiles : sliderVal;
		if(globalJson.exchangeRate) {
			if(enableToggler.is(':checked')) {
				payment.html(paymentTemplate.format(accounting.formatNumber(costPayableByCash * globalJson.exchangeRate[currentCurrency], 2), currentCurrency));
				grandTotal.html(grandTotalTemplate.format(accounting.formatNumber(costMiles), accounting.formatNumber(costPayableByCash * globalJson.exchangeRate[currentCurrency], 2), currentCurrency));
			}
			else {
				payment.html(paymentTemplate.format(accounting.formatNumber(costPayableByCash, 2), 'SGD'));
				grandTotal.html(grandTotalTemplate.format(accounting.formatNumber(costMiles), accounting.formatNumber(costPayableByCash, 2), 'SGD'));
			}
		}
		else {
			payment.html(paymentTemplate.format(accounting.formatNumber(costPayableByCash, 2), currentCurrency));
			grandTotal.html(grandTotalTemplate.format(accounting.formatNumber(costMiles), accounting.formatNumber(costPayableByCash, 2), currentCurrency));
		}
		$('[data-source]').val(accounting.formatNumber(costPayableByCash, 2));
	};

	var bookingSummaryTrigger = function() {
		bookingSummary
			.off('change.costPayableByCash')
			.on('change.costPayableByCash', function(e) {
				costPayableByCash = e.cash;
				var sliderVal = slide.slider('value');
				if (!slide.data('no-stripe')) {
					if (sliderVal < krisMin) {
						sliderVal = krisMin;
					} else {
						if (krisMax > krisUserMax) {
							if (sliderVal > krisUserMax) {
								sliderVal = krisUserMax;
							}
						} else {
							if (sliderVal > krisMax) {
								sliderVal = krisMax;
							}
						}
					}
				}

				changeContentKrisFlyer(sliderVal);
				// if(globalJson.bookingSummary) {
				// 	globalJson.bookingSummary.bookingSummary.costPayableByMiles = sliderVal;
				// }
			});

		bookingSummary.find('.booking-summary__control')
			.off('click.removeMinHeight')
			.on('click.removeMinHeight', function() {
				if (bookingSummary.css('min-height') !== '0px') {
					bookingSummary.css('min-height', '');
				}
			});

		win.off('resize.removeMinHeight').on('resize.removeMinHeight', function() {
			if (bookingSummary.css('min-height') !== '0px') {
				bookingSummary.css('min-height', '');
			}
		});
	};

	bookingSummaryTrigger();

	var determineStripe = function(){
		var sliderHandle = slide.find('a.ui-slider-handle');
		stripMin.css({
			left: krisMin*slide.width()/cMax + sliderHandle.width()/2,
			'display': 'block'
		});
		stripMax.css({
			left: krisMax*slide.width()/cMax + sliderHandle.width()/2,
			'display': 'block'
		});
	};

	var showAndHidePaymentSlider = function(){
		if(triggerOpenPaymentSlider.is(':checked')){
			paymentSlider.css('display', 'block');
			if(!slide.data('no-stripe')){
				determineStripe();
				win.off('resize.responsiveSlide').on('resize.responsiveSlide', function(){
					determineStripe();
				});
			}
		}
		else{
			paymentSlider.css('display', 'none');
			win.off('resize.responsiveSlide');
		}
	};

	// var checlAcceptConStatus = function(){
	// 	if(!btnMakePayment.data('notDisabled')) {
	// 		if(acceptCon.is(':checked')){
	// 			btnMakePayment.prop('disabled', false).removeClass('disabled');
	// 		}
	// 		else{
	// 			btnMakePayment.prop('disabled', true).addClass('disabled');
	// 		}
	// 	}
	// };

	acceptCon.off('change.validate').on('change.validate', function() {
		$(this).valid();
	});

	var radioTab = function(wp, r, t){
		wp.each(function(){
			var radios = wp.find(r);
			var tabs = wp.find(t);

			radios.each(function(){
				var self = $(this),
						parentActiveTab = $(this).parent().data('tabs');
				var triggerTab = function(element) {
					if($(element).prop('checked')) {
						var activeTab = $(element).parent().data('tabs');
						tabs.hide().removeClass('active').each(function() {
							if($(this).data('tabs') === activeTab) {
								$(this).show().addClass('active');
								creditDebitNumber.trigger('change.detectCardType');
							}
						});
						if (activeTab !== 3) {
							enableToggler.prop('checked', false);
							enableToggler.trigger('change.exchange');
						}
					}
				};

				triggerTab(this);

				if( parentActiveTab !== 0 && parentActiveTab !== -1) {
					self.off('change.selectTabs').on('change.selectTabs', function(){
						triggerTab(this);
						autoDetectCard.find('[data-format-card]').addClass('grey-out');
						if(self.data('trigger-detectcard')){
							$(self.data('trigger-detectcard')).trigger('change.detectFormatCard');
						}
						else{
							self.closest('.grid-row').find('[data-format-card]').removeClass('grey-out');
						}
					});
				}
			});
		});
	};

	radioTab($('.payments-group__charge'), '.custom-radio input', 'fieldset.complete-fields, fieldset.payment-currency');
	radioTab($('#payments-detail__form'), '[data-switch-payment] .custom-radio input', 'fieldset.complete-fields, fieldset.payment-currency, fieldset.krisflyer-account');

	if(!triggerOpenPaymentSlider.is('[disabled]') && triggerOpenPaymentSlider.length){
		// slide
		var timeoutSlide;
		slide.slider({
			range: 'min',
			value: cVal, //krisMin ? (krisMin + (krisMax - krisMin)/2) : cMax/2,
			min: cMin,
			max: cMax,
			step: cStep,
			slide: function(event, ui){
				if(!slide.data('no-stripe')){
					cStep = slide.data('step');
					if(cStep - krisMin < cStep && cStep - krisMin > 0) {
						cStep = cStep - krisMin;
					}
					else if(krisMax < krisUserMax) {
						if(krisMax - cStep < cStep) {
							cStep = krisMax - cStep;
						}
					}
					else if(krisUserMax < krisMax) {
						if(krisUserMax - cStep < cStep) {
							cStep = krisUserMax - cStep;
						}
					}
					if(cStep !== slide.data('step')) {
						slide.slider('option', 'step', cStep);
					}

					if(ui.value < krisMin){
						slide.slider('value', krisMin);
						slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.min);

						changeContentKrisFlyer(krisMin);
						return false;
					}
					else if(krisMax > krisUserMax){
						if(ui.value > krisUserMax){
							slide.slider('value', krisUserMax);
							slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.defaultString);
							changeContentKrisFlyer(krisUserMax);
							return false;
						}
					}
					else if(krisMax < krisUserMax){
						if(ui.value > krisMax){
							slide.slider('value', krisMax);
							slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.max);
							changeContentKrisFlyer(krisMax);
							return false;
						}
					}
					slide.find('a.ui-slider-handle .tooltip').hide();
				}
				changeContentKrisFlyer(ui.value);
				clearTimeout(timeoutSlide);
				timeoutSlide = setTimeout(function() {
					slide.trigger('calculate');
				}, 500);
			},
			change: function(){
				var triggerKfMiles = new jQuery.Event('change.KfMiles');
				var s = slide.slider('value');

				if (!slide.data('no-stripe')) {
					if (s < krisMin) {
						s = krisMin;
					}
					else {
						if (krisMax > krisUserMax) {
							if (s > krisUserMax) {
								s = krisUserMax;
							}
						} else {
							if (s > krisMax) {
								s = krisMax;
							}
						}
					}
				}


				if (isPreventLoadJson && s > krisMin && s < (krisUserMax > krisMax ? krisMax : krisUserMax)) {
					isPreventLoadJson = false;
				}

				triggerKfMiles.miles = s;
				triggerKfMiles.callback = function() {
					slide.trigger('calculate');

					// var block = $('.payment-currency');
					// var enableToggler = block.find('[data-toggler] input');
					// var convertedPaymentEl = $('.payment-currency-text');

					// if (convertedPaymentEl.length) {
					// 	convertedPaymentEl.toggleClass('hidden', !enableToggler.is(':checked'));
					// }
				};

				if (!isPreventLoadJson) {
					$('.booking-summary').trigger(triggerKfMiles);
					isPreventLoadJson = s === krisMin || s === (krisUserMax > krisMax ? krisMax : krisUserMax);
				}
			},
			create: function() {
				if(!slide.data('no-stripe')){
					slide.find('a.ui-slider-handle').append('<aside class="tooltip tooltip-4 tooltip--info-1" style="display:none"><em class="tooltip__arrow"></em><div class="tooltip__content"><p class="tooltip__text-2">'+ ((krisMax > krisUserMax) ? L10n.payment.defaultString : L10n.payment.max) +'</p></div></aside>');
					slide.find('a.ui-slider-handle').find('aside').off('mousedown.stop touchstart.stop').on('mousedown.stop touchstart.stop', function(e){
						e.stopPropagation();
						e.preventDefault();
					});
				}
			}
		});
		// slide.find('a.ui-slider-handle').find('aside').off('touchstart.stop').on('touchstart.stop', function(e){
		// 	e.stopPropagation();
		// 	e.cancelBubble = true;
		// });

		changeContentKrisFlyer(slide.slider('value'));

		var plusHold = function(){
			var hold = false;
			var clearRun = null;
			var run = function(){
				if(!hold){
					clearTimeout(clearRun);
					return;
				}
				clearRun = setTimeout(function(){
					funcPlusClick();
					run();
				}, 200);
			};
			plus.off('mousedown.plus touchstart.plus').on('mousedown.plus touchstart.plus', function(e){
				e.preventDefault();
				hold = true;
				run();
			});
			plus.off('mouseup.plus touchend.plus').on('mouseup.plus touchend.plus', function(e){
				e.preventDefault();
				hold = false;
			});
			$(window).off('mouseup.plus').on('mouseup.plus', function() {
				hold = false;
			});
		};

		var minesHold = function(){
			var hold = false;
			var clearRun = null;
			var run = function(){
				if(!hold){
					clearTimeout(clearRun);
					return;
				}
				clearRun = setTimeout(function(){
					funcMinusClick();
					run();
				}, 200);
			};
			minus.off('mousedown.minus touchstart.minus').on('mousedown.minus touchstart.minus', function(e){
				e.preventDefault();
				hold = true;
				run();
			});
			minus.off('mouseup.minus touchend.minus').on('mouseup.minus touchend.minus', function(e){
				e.preventDefault();
				hold = false;
			});
			$(window).off('mouseup.minus').on('mouseup.minus', function() {
				hold = false;
			});
		};

		plusHold();
		minesHold();
		var funcPlusClick = function(){
			slide.find('a.ui-slider-handle .tooltip').hide();
			var newValue = slide.slider('value') + rateMilesCurrency * rateIncrement;
			if(!slide.data('no-stripe')){
				if(krisMax > krisUserMax){
					if (newValue >= krisUserMax) {
						newValue = krisUserMax;
						slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.defaultString);
					}
				}
				else if(krisMax < krisUserMax){
					if (newValue >= krisMax) {
						newValue = krisMax;
						slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.max);
					}
				}
			}
			slide.slider('value', newValue);
			// changeContentKrisFlyer(slide.slider('value'));
			changeContentKrisFlyer(newValue);

			if (bookingSummary.css('min-height') === '0px') {
				setTimeout(function() {
					bookingSummary.css('min-height', bookingSummary.height());
				}, 200);
			}
		};

		var funcMinusClick = function(){
			slide.find('a.ui-slider-handle .tooltip').hide();
			var newValue = slide.slider('value') - rateMilesCurrency * rateDecrement;
			if(!slide.data('no-stripe')){
				if (newValue <= krisMin) {
					newValue = krisMin;
					slide.find('a.ui-slider-handle .tooltip').show().find('.tooltip__text-2').html(L10n.payment.min);
				}
			}
			slide.slider('value', newValue);
			// changeContentKrisFlyer(slide.slider('value'));
			changeContentKrisFlyer(newValue);

			if (bookingSummary.css('min-height') === '0px') {
				setTimeout(function() {
					bookingSummary.css('min-height', bookingSummary.height());
				}, 200);
			}
		};
	}

	// payment slide
	showAndHidePaymentSlider();
	triggerOpenPaymentSlider.off('change.showAndHidePaymentSlider').on('change.showAndHidePaymentSlider', showAndHidePaymentSlider);

	// checlAcceptConStatus();
	// acceptCon.off('change.acceptCon').on('change.acceptCon', function(){
	// 	checlAcceptConStatus();
	// });

	// check submit make-payment
	// btnMakePayment.off('click.check').on('click.check', function(e){
	// 	e.preventDefault();
	// });

	triggerShowKrisInfo.off('change.checkKrisFlyer').on('change.checkKrisFlyer', function() {
		var formFields = krisflyerAccount.find('input, select, textarea');

		if($(this).is(':checked')) {
			formFields.each(function() {
				var that = this;
				var el = $(that);
				var parentElement = that.parentNode;
				var dataParent = $(parentElement).data();

				/*if(Object.keys(dataParent).length) {*/
				if(!$.isEmptyObject(dataParent)) {
					for(var objData in dataParent) {
						if(globalJson.krisFlyer[objData]) {
							el.val(globalJson.krisFlyer[objData]);
							if(/^(select-one|select-multiple)$/.test(that.type)) {
								var parentEl = el.closest('.autocomplete');
								if(parentEl.data('autocomplete')) {
									parentEl.removeClass('default').find('.ui-autocomplete-input').data('uiAutocomplete')._value(
										el.find(':selected').data('text')
									);
									parentEl.find('input').valid();
								} else {
									el.defaultSelect('refresh');
								}
							}
						}
					}
				}
				$(that).valid();
			});
		} else {
			formFields.each(function() {
				var that = this;
				that.value = '';
				if(/^(select-one|select-multiple)$/.test(that.type)) {
					var parentEl = $(that.parentNode);
					if(parentEl.data('autocomplete')) {
						parentEl.addClass('default').find('.ui-autocomplete-input').data('uiAutocomplete')._value('');
						parentEl.find('input').valid();
					} else {
						$(that).defaultSelect('refresh');
					}
				}
				// $(that).valid();
			});
			if(!formPayment.data('validatedOnce')) {
				formPayment.data('validator').resetForm();
			} else {
				formPayment.valid();
			}
		}
	});

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					$(this).find('select, input').valid();
	// 				}
	// 			});
	// 		});

	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					$(this).find('select, input').valid();
	// 				}
	// 			});
	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 			self.data('change', true);
	// 		});

	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
	// 			self.data('change', true);
	// 		});

	// 		if(global.vars.isSafari){
	// 			self.find(':radio').off('afterTicked.passengerDetail').on('afterTicked.passengerDetail', function(){
	// 				self.data('change', true);
	// 			});
	// 		}
	// 	});
	// };

	formPayment.validate({
		focusInvalid: true,
		// errorPlacement: function(error, element) {
		// 	var formGroup = $(element).closest('.form-group-inner');
		// 	var containerForm = formGroup[0] ? formGroup : $(element).closest('.grid-col');
		// 	containerForm.data('valid', true);
		// 	formPayment.data('success', false);
		// 	containerForm.removeClass('success').addClass('error');
		// 	if(!containerForm.find('.text-error').length){
		// 		$(config.template.labelError).appendTo(containerForm).find('span').text(error.text());
		// 	}else{
		// 		containerForm.find('.text-error').find('span').text(error.text());
		// 	}
		// },
		// success: function(label, element) {
		// 	var formGroup = $(element).closest('.form-group-inner');
		// 	var containerForm = formGroup[0] ? formGroup : $(element).closest('.grid-col');
		// 	containerForm.removeClass('error').addClass('success');
		// 	containerForm.data('change', false);
		// 	containerForm.data('valid', false);
		// },
		onfocusout: global.vars.validateOnfocusout,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess,
		formPayment: function(){
			formPayment.data('success', true);
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		invalidHandler: function(form, validator) {
			var errors = validator.numberOfInvalids();
			if (errors) {
				win.scrollTop($(validator.errorList[0].element).closest('.grid-col').offset().top - 40);
			}
		}
	});

	customSelect.off('afterSelect.customSelect').on('afterSelect.customSelect', function(){
		var selectElement = $(this).find('select');
		formPayment.validate().element($(selectElement));
	});

	// validateFormGroup(formPayment.find('.form-group-inner'));

	/*Exchange*/
	var exchangeRateSystem = function() {
		var block = $('.payment-currency');
		// var enableToggler = block.find('[data-toggler] input');
		var txtSource = block.find('[data-source]');
		var selectRate = block.find('[data-rate]');
		var txtDest = block.find('[data-destination]');

		var setConvertedValue = function(convertedValue) {
			var convertedPaymentEl = $('.payment-currency-text');
			var totalPaidUnitEl = convertedPaymentEl.siblings('.unit');

			if (convertedPaymentEl.length && totalPaidUnitEl.length) {
				if (convertedPaymentEl.hasClass('hidden')) {
					totalPaidUnitEl.text('SGD ' + txtSource.val());
				}
				else {
					txtDest.val(convertedValue);
					totalPaidUnitEl.text(currentCurrency + ' ' + convertedValue);
				}
			}
		};

		var setDefaultValue = function() {
			var payment = costPayableByCash;
			// var paymentExchanged = payment * (globalJson.exchangeRate ? globalJson.exchangeRate[currentCurrency] : 0);
			var paymentExchanged = (globalJson.exchangeRate ? globalJson.exchangeRate[currentCurrency] : 0);
			var convertedValue = '';

			if (slide.length) {
				var slideValue = slide.slider('value');
				var newPayment = ((cMax - slideValue) / rateMilesCurrency) * paymentExchanged;

				convertedValue = accounting.formatMoney(newPayment, '', 2, ',', '.');

				// txtSource.val(accounting.formatMoney(payment, '', 2, ',', '.'));
				// txtDest.val(accounting.formatMoney(paymentExchanged, '', 2, ',', '.'));

				if(enableToggler.is(':checked')){
					txtSource.val(accounting.formatMoney(payment, '', 2, ',', '.'));
					setConvertedValue(convertedValue);
				}

				// changeContentKrisFlyer(slide.slider('value'));
			}
			else {
				convertedValue = accounting.formatMoney(txtSource.data('default-value'), '', 2, ',', '.');
				txtSource.val(convertedValue);
				setConvertedValue(convertedValue);
			}
		};

		setDefaultValue();

		slide.off('calculate').on('calculate', function() {
			setDefaultValue();
		});

		var resetValues = function() {
			selectRate.find('select').val(defaultCurrency).defaultSelect('refresh');
			calcValues();
		};

		var calcValues = function(currency) {
			var rateCode = currency ? currency : selectRate.find('select').val();
			var rate = globalJson.exchangeRate ? globalJson.exchangeRate[rateCode] : 0;
			var convertedValue = '';

			currentCurrency = rateCode;

			if(slide.length) {
				var slideValue = slide.slider('value');
				var newPayment = ((cMax - slideValue) / rateMilesCurrency) * rate;

				convertedValue = accounting.formatMoney(newPayment, '', 2, ',', '.');
				payment.html(paymentTemplate.format(accounting.formatNumber(newPayment, 2), currentCurrency));
				grandTotal.html(grandTotalTemplate.format(accounting.formatNumber(slide.slider('value')), accounting.formatNumber(newPayment, 2), currentCurrency));
			}
			else {
				var sourcePrice = window.accounting.unformat(txtSource.val());
				var destPrice = sourcePrice * rate;

				convertedValue = accounting.formatMoney(destPrice, '', 2, ',', '.');
			}

			setConvertedValue(convertedValue);
		};

		calcValues(currentCurrency);

		enableToggler
		.off('change.exchange')
		.on('change.exchange', function() {
			var checked = $(this).is(':checked');
			var convertedPaymentEl = $('.payment-currency-text');

			block.toggleClass('disabled', !checked);
			selectRate.toggleClass('disabled', !checked).find('select').prop('disabled', !checked);

			if (convertedPaymentEl) {
				convertedPaymentEl.toggleClass('hidden', !checked);
			}

			if(!checked) {
				resetValues();
			}
			else {
				calcValues();
			}
			changeContentKrisFlyer(slide.slider('value'));
		});

		selectRate.find('select')
		.off('change.exchange')
		.on('change.exchange', function() {
			calcValues();
		});
	};

	exchangeRateSystem();

	if(switchKfPayment.length && contentKf.length) {
		switchKfPayment.find(':radio').each(function() {
			var self = $(this);
			self.off('change.showDataKrisflyer').on('change.showDataKrisflyer', function() {
				if(self.data('triggerKrisflyer')) {
					contentKf.removeClass('hidden');
				} else {
					contentKf.addClass('hidden');
				}
			});
		});
	}

	var changePaymentTotalKf = function() {
		var paymentContent = $('[data-content-krisflyer].payment-currency');
		var selectedCurrency = $('[data-rate] select', paymentContent);
		var paymentTotal = $('[data-payment-total]', paymentContent);
		var paymentExchanged = paymentTotal.data('payment-total') * (globalJson.exchangeRate ? globalJson.exchangeRate[selectedCurrency.val()] : 0);
		paymentTotal.text(accounting.formatMoney(paymentExchanged, '', 2, ',', '.'));
	};

	var setEventChangeCurrency = function() {
		var paymentContent = $('[data-content-krisflyer].payment-currency');
		var selectRate = $('[data-rate] select', paymentContent);
		selectRate
		.off('change.change-payment-total')
		.on('change.change-payment-total', function() {
			changePaymentTotalKf();
		});
		changePaymentTotalKf();
	};
	setEventChangeCurrency();

	/*var	flightDetail = $('.flights--detail'),
		hasToggle = false,
		toggle;
	flightDetail.children('span').each(function(){
		var that = $(this),
		detailElement = that.next();
		detailElement.removeClass('hidden').hide();
		that.off('click.showFlightDetail').on('click.showFlightDetail', function() {
			if(!hasToggle){
				hasToggle = true;
				that.toggleClass('active');
				toggle = detailElement.slideToggle(400, function(){
					hasToggle = false;
				});
			}
		});
	});*/

	var	flightDetail = $('.flights--detail');

	flightDetail.children('span').each(function(){
		var el = $(this),
				detailElement = el.next();
		detailElement.removeClass('hidden').hide();
		el.off('click.showFlightDetail').on('click.showFlightDetail', function(e) {
			e.preventDefault();
			el.toggleClass('active');
			detailElement.slideToggle(400);
		});
	});

	if(autoDetectCard.length){
		var inputCard = autoDetectCard.find('[data-auto-detect-input]');
		if(inputCard.length){
			var formatCard = autoDetectCard.find('[data-format-card]');
			var card = {
				mastercard: function(value){
					if(/^(5[12345])/.test(value) && value.length === 16){
						return 'master';
					}
					return '';
				},
				visa: function(value){
					if(/^(4)/.test(value) && value.length === 16){
						return 'visa';
					}
					return '';
				},
				amex: function(value){
					if(/^(3[47])/.test(value) && value.length === 15){
						return 'amex';
					}
					return '';
				},
				dinersclub: function(value){
					if(/^(3(0[012345]|[68]))/.test(value) && value.length === 14){
						return 'dinersclub';
					}
					return '';
				},
				enroute: function(value){
					if(/^(2(014|149))/.test(value) && value.length === 15){
						return 'enroute';
					}
					return '';
				},
				discover: function(value){
					if(/^(6011)/.test(value) && value.length === 16){
						return 'discover';
					}
					return '';
				},
				jcb: function(value){
					if((/^(3)/.test(value) && value.length === 16) || (/^(2131|1800)/.test(value) && value.length === 15)){
						return 'jcb';
					}
					return '';
				}
			};

			var cardetector = function(value){
				var t = '';
				for(var c in card){
					t = card[c](value);
					if(t){
						break;
					}
				}
				formatCard.addClass('grey-out').filter(function(){
					return $(this).data('format-card') === t;
				}).removeClass('grey-out');
			};
			// var inputVL = inputCard.val().toString().trim();

			// var initDetectCard = function(inputVL){
			// 	formatCard.each(function() {
			// 		var self = $(this);

			// 		var numberCard = self.data('format-card').toString().trim();
			// 		var n;

			// 		if(numberCard.indexOf(',') >= 0){
			// 			var arr = numberCard.split(',').map(Function.prototype.call, String.prototype.trim);
			// 			for(var i = 0; i < arr.length; i++){
			// 				if(inputVL.indexOf(arr[i]) >= 0){
			// 					n = 1;
			// 					console.log(inputVL.indexOf(arr[i]));
			// 					break;
			// 				}
			// 			}
			// 		}else{
			// 			n = inputVL.indexOf(numberCard);
			// 		}

			// 		if(n >= 0){
			// 			self.removeClass('grey-out');
			// 		}else{
			// 			self.addClass('grey-out');
			// 		}
			// 	});
			// };

			// initDetectCard(inputVL);

			inputCard.off('change.detectFormatCard').on('change.detectFormatCard', function(){
				// initDetectCard($(this).val());
				cardetector($(this).val());
			}).triggerHandler('change.detectFormatCard');
		}
	}
};
