/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */
SIA.desEntry = function(){
	var global = SIA.global,
		win = global.vars.win,
		// config = global.config,
		btnSeeMore = $('.country-button [data-see-more]'),
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		isLandscape = win.width() > win.height(),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startMbLanNum = 0,
		startMbPosNum = 0,
		defaultMbPosNum = 4,
		seeMoreMbPosNum = 2,
		defaultMbLanNum = 8,
		seeMoreMbLanNum = 4,
		seeMoreCount = 0;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg === region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container.removeAttr('style')
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.removeAttr('style')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region') === region) &&
					(!city || self.data('city') === city);
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);

		return (!isLandscape ? (startMbPosNum = 0) : (startMbLanNum = 0)) || fillContent(false);
		// if (!isLandscape) {
		// 	startMbPosNum = 0;
		// }
		// else {
		// 	startMbLanNum = 0;
		// }

		// fillContent(false);
	};

	var generateTemplate = function(resLen) {
		preventClick = false;
		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.text(L10n.kfSeemore.seeMore);
			btnSeeMore.addClass('hidden');

			return !isLandscape ? (startMbPosNum = resLen) : (startMbLanNum = resLen);
			// if (!isLandscape) {
			// 	startMbPosNum = resLen;
			// }
			// else {
			// 	startMbLanNum = resLen;
			// }
		}
		else {
			btnSeeMore.text(seeMoreCount === 2 ? L10n.kfSeemore.seeAll : L10n.kfSeemore.seeMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx, resLen) {
		var tpl = res.filter(':lt(' + endIdx + ')').filter('.hidden');
		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');
				if(!img.data('loaded')) {
					img.one('load.loadImg', function() {
						self.removeClass('hidden');
						img.data('loaded', true);
						if (!tpl.filter('.hidden').length) {
							generateTemplate(resLen);
						}
					}).attr('src', img.data('img-src'));
				}
				// if (!img.data('loaded')) {
				// 	var newImg = new Image();

				// 	newImg.onload = function() {
				// 		self.removeClass('hidden');
				// 		img.data('loaded', true);

				// 		if (!tpl.filter('.hidden').length) {
				// 			generateTemplate(tpl, resLen);
				// 		}
				// 		img.parent().css({
				// 			'background-image': 'url(' + this.src + ')'
				// 		});
				// 	};

				// 	newImg.src = img.data('img-src');
				// 	img.attr('src', config.imgSrc.transparent);
				// }
				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(resLen);
					}
				}
			});
		}
	};

	var fillContent = function(isResize, setNumber) {
		if (staticItems.length) {
			var endIdx = 0;
			var resLen = staticItems.length;

			if (isResize) {
				container.removeAttr('style');
				generateClass(staticItems);
			}

			if (isLandscape) {
				endIdx = !startMbLanNum ?
					(seeMoreCount > 2 ?	resLen : (setNumber ? setNumber * 4 : startMbLanNum + defaultMbLanNum)) :
					(seeMoreCount > 2 ? resLen : startMbLanNum + seeMoreMbLanNum);
				startMbLanNum = endIdx;
			}
			else {
				endIdx = !startMbPosNum ?
					(seeMoreCount > 2 ? resLen : (setNumber ? setNumber * 2 : startMbPosNum + defaultMbPosNum)) :
					(seeMoreCount > 2 ? resLen : startMbPosNum + seeMoreMbPosNum);
				startMbPosNum = endIdx;
			}

			getTemplate(staticItems, endIdx, resLen);
		}
	};

	var generateClass	= function(res) {
		res.removeAttr('style').addClass('hidden col-mb-3');
	};

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		if (!preventClick) {
			preventClick = true;
			seeMoreCount = 0;
			var region = regionInput.val();
			cityValue = cityInput.val();
			renderTemplate(region, cityValue);
		}
	});

	win.off('resize.template').on('resize.template', function() {
		if(win.width() <= win.height()){
			if(isLandscape) {
				isLandscape = false;
				var setNumber =  Math.ceil(startMbLanNum / 4);
				startMbPosNum = 0;
				fillContent(true, setNumber);
			}
		}
		else {
			if (!isLandscape) {
				isLandscape = true;
				var setNumber = Math.ceil(startMbPosNum / 2);
				startMbLanNum = 0;
				fillContent(true, setNumber);
			}
		}
	});

	btnSeeMore.off('click.template').on('click.template', function(e) {
		e.preventDefault();
		if (!preventClick) {
			seeMoreCount++;
			fillContent(false);
		}
	});

	var initModule = function(){
		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);
	};

	initModule();
};
