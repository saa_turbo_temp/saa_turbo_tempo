/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.flightCalendar = function(){
	var global = SIA.global;
	var win = global.vars.win;

	if(!$('.fare-calendar-page').length){
		return;
	}

	var $calendar = $('.calendar__table'),
		$calendarRowColumn = $('td', $calendar);

	var controls = $('.control-horizontal, .control-vertical');
	var btnNext = $('.flight__calendar .btn-1');
	var dataChecked = $calendar.data('checked');
	var getPositionInTable = function(el) {
		var cellIndex = el.cellIndex;
		var rowIndex = el.parentNode.rowIndex;
		return rowIndex + ',' + cellIndex;
	};
	var disableNextBtn = function(e) {
		e.preventDefault();
		return;
	};
	var calcCellDisplay = function(calendarTable) {
		var totalCellDisplay = calendarTable.find('tr:eq(0) th:visible').length - 1;
		controls.find('.number').each(function(){
			$(this).html(totalCellDisplay);
		});
	};

	$calendarRowColumn.off('click.activeHead').on('click.activeHead',function(){
		if($(this).hasClass('not-available')){
			return;
		}

		var col = $(this).parent().children().index($(this)),
			$thHeader = $('th', $calendar),
			$headerColumn = $calendar.find('tr').eq(0).find('th');

		$calendarRowColumn.removeClass('active');
		$thHeader.removeClass('active');
		$(this).addClass('active');
		$(this).parent().find('th').addClass('active');
		$headerColumn.eq(col).addClass('active');

		// event checked radio button and set data table
		$(this).find('input[type="radio"]').prop('checked', true);
		$calendar.data('checked', getPositionInTable(this));
		if(btnNext.hasClass('disabled')) {
			btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
		}
	});

	btnNext.addClass('disabled').on('click.disabled', disableNextBtn);

	if(dataChecked) {
		var position = dataChecked.split(',');
		var rowIndex = parseInt(position[0]) - 1;
		var cellIndex = parseInt(position[1]);
		$calendar.find('tr:eq(' + cellIndex + ') td:eq(' + rowIndex + ')').trigger('click.activeHead');
		if(btnNext.hasClass('disabled')) {
			btnNext.removeClass('disabled').off('click.disabled', disableNextBtn);
		}
	}

	calcCellDisplay($calendar);
	win.off('resize.calculateCellDisplay').on('resize.calculateCellDisplay', function(){
		calcCellDisplay($calendar);
	});
};
