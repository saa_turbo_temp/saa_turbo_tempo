/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.cibConfirmation = function(){
	var global = SIA.global,
			config = global.config,
			triggerPopup = $('[data-trigger-popup]'),
			flyingFocus = $('#flying-focus'),
			flightDetail = $('.flights--detail'),
			hasToggle = false,
			toggle,
			dataBtnCheckin = $('[data-button-checkin]'),
			disabledLink = function(e) { e.preventDefault(); };

	if(globalJson.bookingConfirmInfo) {
		dataBtnCheckin.addClass('disabled').on('click.disabledLink', disabledLink);

		$.ajax({
			url: global.config.url.bookingFlightConfirm,
			type: global.config.ajaxMethod,
			dataType: 'json',
			data: globalJson.bookingConfirmInfo,
			success: function(res) {
				if(res.isEligible) {
					dataBtnCheckin.removeClass('disabled').off('click.disabledLink');
				}
			},
			error: function(jqXHR, textStatus) {
				window.console.log(textStatus);
			},
			complete: function() {
				dataBtnCheckin.removeClass('hidden')
				.siblings('.loading').addClass('hidden');
			}
		});
	}

	triggerPopup.each(function(){
		var self = $(this);
		var popup = $(self.data('trigger-popup'));
		if(!popup.data('Popup')){
			popup.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function(){
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				}
			});
		}
		self.off('click.showPopup').on('click.showPopup', function(e){
			e.preventDefault();
			popup.Popup('show');
		});
	});

	flightDetail.children('span').off('click.showFlightDetail').on('click.showFlightDetail', function() {
		if(!hasToggle) {
			var that = $(this),
				detailElement = that.next();

			that.toggleClass('active');

			if(detailElement.hasClass('hidden')) {
				$.ajax({
					url: global.config.url.flightSearchFareFlightInfoJSON,
					dataType: 'json',
					type: global.config.ajaxMethod,
					data: {
						flightNumber: that.parent().data('flight-number'),
						carrierCode: that.parent().data('carrier-code'),
						date: that.parent().data('date'),
						origin: that.parent().data('origin')
					},
					success: function(data) {
						var textAircraftType = '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType + '</p>',
							textFlyingTime = '';

						for(var ft in data.flyingTimes) {
							textFlyingTime += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + data.flyingTimes[ft] + '</p>';
						}

						detailElement.removeClass('hidden').hide(0);

						detailElement.empty();
						detailElement.append(textAircraftType, textFlyingTime);

						toggle = detailElement.slideToggle(400, function(){
							hasToggle = true;
						});

						toggle.promise().done(function(){
							hasToggle = false;
						});

					},
					error: function(xhr, status) {
						if(status !== 'abort'){
							window.alert(L10n.flightSelect.errorGettingData);
						}
					}
				});
			} else {
				toggle = detailElement.slideToggle(400, function(){
					hasToggle = true;
				});

				toggle.promise().done(function(){
					detailElement.addClass('hidden');
					hasToggle = false;
				});
			}
		}
	});

	var renderCarAddOnSales = function(){
		var templateBookingCar;
		var appendAfterDiv = $('.add-ons__list--sales');
		var CarDetailAddOnSales = function(data1){
	    $.get(global.config.url.bookingDetailCarResponse, function (data) {
	      var template = window._.template(data, {
	        data: data1
	      });
	      templateBookingCar = $(template);
   			appendAfterDiv.append(templateBookingCar);
	    });
		};
		$.ajax({
      url: global.config.url.bookingDetailResponse,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
      	var data2 = reponse.response;
        CarDetailAddOnSales(data2);
      }
	  });
	};
	renderCarAddOnSales();
};
