/**
 * @name SIA
 * @description Define global ORB Booking Summary functions
 * @version 1.0
 */
SIA.MBBookingSummary = function(){
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var flightSearch = $('.flights__searchs');
	var flightUpgrades = $('.flights-upgrade');
	var bookingSummaryWidget = $('.booking-summary');
	var bookingSummaryHeading = bookingSummaryWidget.find('.booking-summary__heading');
	var bookingSummaryControl = null;
	// var bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');
	var collapseBsp = $('[data-collapse-bsp]');
	var tooltipPopup = $('.add-ons-booking-tooltip');
	var passengerCount = bookingSummaryWidget.find('.number-passengers');
	var bspTexts = L10n.bookingSummary.texts;

	// This function uses remove format Number
	var unformatNumber = function(number) {
		number = window.accounting.unformat(number);
		return parseFloat(number);
	};

	// This function uses format Number
	var formatNumber = function(number, fraction) {
		return window.accounting.formatNumber(number, (typeof(fraction) !== 'undefined') ? fraction : 2, ',', '.');
	};

	var bookingObject = {
		template: {
			heading:
				'<a href="#" class="booking-summary__control">' +
					'{0}' +
					'<em class="ico-point-d"></em>' +
				'</a>' +
				'<div class="booking-summary__info">' +
					'<p class="number-passengers">{1}</p>' +
					'<span class="total-title">{2}</span>' +
					'<span data-headtotal="true" class="total-cost" data-totaltobepaid="true">' +
						'{3}{4}' +
					'</span>' +
					'<span class="total-title">{5}</span>' +
					'<span data-headtotal="true" class="total-cost" data-totaltoberefunded="true">' +
						'{6}{7}' +
					'</span>' +
				'</div>',

			grandTotal:
				'<span class="total-title">{0}</span>' +
				'<span data-headtotal="true" class="total-info" data-totaltobepaid="true">' +
					'{1}{6}{2}' +
				'</span>' +
				'<span class="total-title">{3}</span>' +
				'<span data-headtotal="true" class="total-info" data-totaltoberefunded="true">' +
					'{4}{7}{5}{9}' +
				'</span>' +
				'{8}',

			convertedTpl: '<span class="payment-currency-text{0}">{1}</span>',

			cancelPaid:
				'<div class="grand-total">' +
					'<span class="total-title">{0}</span>' +
					'<span data-headtotal="true" class="total-info" data-totaltobepaid="true">' +
						'{1}{3}{2}' +
					'</span>' +
				'</div>',

			cancelRefund:
				'<div class="grand-total">' +
					'<span class="total-title">{0}</span>' +
					'<span data-headtotal="true" class="total-info" data-totaltoberefunded="true">' +
						'{1}{3}{2}{4}' +
					'</span>' +
				'</div>',

			cancelFee:
				'<ul class="flights-cost__details">' +
					'{0}{1}' +
				'</ul>'
		}
	};

	// Generate Booking sumary Notes
	var generateBSPNotes = function(bsinfo) {
		var miles = bspTexts.miles;
		var html = '';

		if ((bsinfo.expiredMilesNonRefund && parseFloat(bsinfo.expiredMilesNonRefund) > 0) || (bsinfo.totalMilesExpired && parseFloat(bsinfo.totalMilesExpired) > 0)) {
			html += '<ul class="miles-note">';

			if (bsinfo.expiredMilesNonRefund) {
				var exMileNonRefund = parseFloat(bsinfo.expiredMilesNonRefund);
				html += '<li><span class="list-miles__order">(i)</span><p class="text-miles">' + bspTexts.remainExpiredMile + '</p>';
				html += '<span data-krisflyer-remain-expired-miles="' + exMileNonRefund + '">';
				html += (formatNumber(exMileNonRefund, 0) + ' ' + miles);
				html += '</span></span></li>';
			}

			if (bsinfo.totalMilesExpired) {
				var totalMilesExpired = parseFloat(bsinfo.totalMilesExpired);
				html += '<li>';
				html += formatNumber(totalMilesExpired, 0) + ' ' + bspTexts.usedToKrisFlyer;
				html += '</li>';
			}

			html += '</ul>';
		}

		return html;
	};

	// Generate heading Booking sumary
	var headingBSP = function() {
		if (globalJson.bookingSummary) {
			bookingSummaryHeading.children().not('.loading--medium-2').remove();
			var oruJson = globalJson.bookingSummary.oruUpgradeJsonVO;
			var bsinfo = globalJson.bookingSummary.bookingSummary;
			var miles = bspTexts.miles;
			var html = '';

			if (bsinfo.adultCount) {
				html += bsinfo.adultCount + (bsinfo.adultCount > 1 ?
					L10n.bookingSummary.paxInfor.adults : L10n.bookingSummary.paxInfor.adult);
			}

			if (bsinfo.childCount) {
				html += (html.length ? ', ' : '') + bsinfo.childCount + (bsinfo.childCount > 1 ?
					L10n.bookingSummary.paxInfor.children : L10n.bookingSummary.paxInfor.child);
			}

			if (bsinfo.infantCount && bsinfo.scenario !== 'ORC') {
				html += (html.length ? ', ' : '') + bsinfo.infantCount + (bsinfo.infantCount > 1 ?
					L10n.bookingSummary.paxInfor.infants : L10n.bookingSummary.paxInfor.infant);
			}

			var textsPaid = '',
				totalToBePaidMiles = '',
				totalToBePaidCash = '',
				textsRefund = '',
				milesToBeRefunded = '',
				cashToBeRefunded = '';

			// ATC
			if (bsinfo.scenario === 'ATC') {
				// textsPaid = ((bsinfo.totalToBePaidCash && parseFloat(bsinfo.totalToBePaidCash) > 0) || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = ''; // (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (cashToBeRefunded ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');
			}

			// Cancel
			if (bsinfo.scenario === 'Cancel') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + miles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (cashToBeRefunded ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');
			}

			// ORC
			if (bsinfo.scenario === 'ORC') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');
				milesToBeRefunded = '';
			}

			// ORU
			if (bsinfo.scenario === 'ORU') {
				textsPaid = bspTexts.paid;

				totalToBePaidCash = (typeof oruJson.topUpCashForORU !== 'undefined' ? '<span>' + oruJson.currencyCode + ' ' + formatNumber(oruJson.topUpCashForORU, 2) + '</span>' : '');

				totalToBePaidMiles = '<span>' + (oruJson.topUpCashForORU && parseFloat(oruJson.topUpCashForORU) > 0 ? '<small>+</small>&nbsp;' : '') + formatNumber(oruJson.totMilesForORU, 0) + ' ' + bspTexts.krisFlyerMiles + '</span>';

				textsRefund = '';
				cashToBeRefunded = '';
				milesToBeRefunded = '';
			}

			bookingSummaryHeading
				.prepend(bookingObject.template.heading.format(
					bspTexts.titleBS,
					html,
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded
				));
		}
	};

	// Set booking sumary flight infomation
	var setBookingSummaryFlightInfo = function() {
		var oruJson = globalJson.bookingSummary.oruUpgradeJsonVO;
		var bsinfo = globalJson.bookingSummary.bookingSummary;
		var flightsInfo = bookingSummaryWidget.find('[data-flight-info]');
		var flightsInfoHtml = '';
		var currency = bsinfo.currency ? bsinfo.currency : '';
		var miles = bspTexts.miles;

		flightsInfo.empty();

		// start render common baggage

		var commonBags = bsinfo.commonBaggages;
		if (commonBags) {
			var commonBaggageContent = bookingSummaryWidget.find('[data-common-baggage]');
			var contentHtml = '';
			var grandTotalHtml = '';
			var commonBagsHtml = '';

			if (commonBags && commonBags.baggageList && commonBags.baggageList.length) {
				for (var i = 0; i < commonBags.baggageList.length; i++) {
					var bag = commonBags.baggageList[i];
					contentHtml += '<li class="addon--item">' +
						'<span>' + bag.type + '</span>' +
						'<span class="price">' + formatNumber(bag.amount, 2) + '</span>' +
						'</li>';
				}
			}

			if (typeof bsinfo.totalToBePaidCash !== 'undefined') {
				grandTotalHtml += '<div class="grand-total">' +
				'<span class="total-title">TOTAL TO BE PAID</span>' +
				'<span data-headtotal="true" class="total-info" data-totaltobepaid="true">' +
				'<span class="unit">' + commonBags.cost + ' ' + formatNumber(commonBags.total, 2) + '</span>' +
				'</span>' +
				'</div>';
			}

			commonBagsHtml = '<div class="flights-cost">' +
				'<h4 class="flights-cost-title">' +
				'<span class="text-left">' + bspTexts.cost + '</span>' +
				'<span class="text-right">' + commonBags.cost + '</span>' +
				'</h4>' +
				'<ul class="flights-cost__details" data-addons="true">' +
				contentHtml +
				'<li class="sub-total sub-total-2">' +
				'<span>' + bspTexts.subTotal + '</span>' +
				'<span class="price">' + commonBags.cost + ' ' + formatNumber(commonBags.total, 2) + '</span>' +
				'</li>' +
				'</ul>' +
				'</div>' +
				grandTotalHtml;

			bookingSummaryWidget.find('.booking-group:lt(2)').remove();
			bookingSummaryWidget.find('.booking-group.booking-group-addon').siblings().remove();
			commonBaggageContent.empty().append(commonBagsHtml);

			return;
		}
		else {
			bookingSummaryWidget.find('.booking-group:eq(2)').remove();
		}

		// end render common baggage

		if (bsinfo.scenario === 'Cancel') {
			flightsInfo.siblings('.booking-heading').find('h3')
				.text(bspTexts.itinerary);
		}

		// new Itinerary
		for (var i = 0; i < bsinfo.flight.length; i++) {
			flightsInfoHtml += '<div class="flights-info">';
			flightsInfoHtml += '	<div class="flights-info-heading">';
			flightsInfoHtml += '		<h4>Flight ' + (i + 1) + '</h4>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.date + ' - ' + bsinfo.flight[i].flightSegments[0].deparure.time + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '	<div class="flights-info__country">';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.airportCode + '</span>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[bsinfo.flight[i].flightSegments.length - 1].arrival.airportCode + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '</div>';
		}

		flightsInfo.html(flightsInfoHtml);

		// Flights
		// old itinerary
		var bookingContentFlight =
			bookingSummaryWidget.find('.booking-group:eq(1) .booking-group__content');
		var itineraryHtml = '';
		bookingContentFlight.empty();

		// Old Itinerary
		if (bsinfo.oldItinerary) {
			itineraryHtml += '<div class="flights-cost">' +
				'<h4 class="flights-cost-title">' +
				'<span class="text-left">' +
				bspTexts.oldItinerary +
				'</span>' +
				'<span class="text-right">&nbsp;</span>' +
				'</h4>';

			itineraryHtml += '<ul class="flights-cost__details">';

			// ATC
			if (bsinfo.scenario === 'ATC') {
				var subAndFareTotal = '';
				var oldCurrency = bsinfo.oldItinerary.currency || '';

				if (bsinfo.oldItinerary.fareTotal &&
					parseFloat(bsinfo.oldItinerary.fareTotal) > 0) {

					subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);
				}
				else if ((bsinfo.oldItinerary.costPaidByCash &&
					parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0) ||
					(bsinfo.oldItinerary.costPaidByMiles &&
					parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0)) {

					subAndFareTotal = (bsinfo.oldItinerary.costPaidByCash ? currency + ' ' + formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) : '') + (bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ? ' + ' + '<span>' + formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) + miles + '</span>' : '');

					// subAndFareTotal = (bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ?
					// 		formatNumber(bsinfo.oldItinerary.costPaidByMiles, 2) +
					// 			bspTexts.miles +
					// 			(bsinfo.oldItinerary.costPaidByCash ? ' + ' : '') : '') +
					// 	(bsinfo.oldItinerary.costPaidByCash &&
					// 		parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
					// 			'<span>' + currency + ' ' +
					// 			formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) + '</span>' : '');
				}

				if (subAndFareTotal) {
					itineraryHtml += '<li data-old-fare="true" class="sub-total first">' +
						'<span>' +
						bspTexts.fareOld +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}
			}
			// Cancel
			else if (bsinfo.scenario === 'Cancel') {
				var subAndFareTotal = '';
				var taxPaid = '';
				var oldCurrency = bsinfo.oldItinerary.currency || '';

				if (bsinfo.oldItinerary.fareTotal &&
					parseFloat(bsinfo.oldItinerary.fareTotal) > 0) {

					subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareRefundable +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.taxTotal &&
					parseFloat(bsinfo.oldItinerary.taxTotal) > 0) {

					taxPaid = oldCurrency + ' ' + formatNumber(bsinfo.oldItinerary.taxTotal, 2);

					itineraryHtml += '<li data-old-tax-paid="true">' +
						'<span>' +
						bspTexts.taxSurchargeRefundable +
						'</span>' +
						'<span class="values">' +
						taxPaid +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash || bsinfo.oldItinerary.costPaidByMiles) {
					var paidCash = bsinfo.oldItinerary.costPaidByCash;
					var paidMiles = bsinfo.oldItinerary.costPaidByMiles;

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						((paidCash ? oldCurrency + ' ' + formatNumber(paidCash, 2) : '') + (paidMiles ? ' + ' : '')) +
						(paidMiles ? '<span>' + formatNumber(paidMiles, 0) + ' ' + miles + '</span>' : '') +
						'</span>' +
						'</li>';
				}

				if ((bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0) ||
					(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0)) {
					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ?
							formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) +
							miles +
							(bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
								' + ' : '') : '') +
						(bsinfo.oldItinerary.costPaidByCash && parseFloat(bsinfo.oldItinerary.costPaidByCash) > 0 ?
						'<span class="unit">' + oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
						'</span>' : '') +
						'</span>' +
						'</li>';
				}
			}
			// ORU
			else if (bsinfo.scenario === 'ORU') {
				var oldCurrency = bsinfo.oldItinerary.currency || '';
				if (bsinfo.oldItinerary.fareTotal) {
					var subAndFareTotal = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.fareTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareOld +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash) {
					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						'<span class="values">' +
						oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
						'</span>' +
						'</li>';
				}
			}
			// ORC
			else {
				var oldCurrency = bsinfo.oldItinerary.currency ?
					bsinfo.oldItinerary.currency : '';
				var subAndFareTotal = '';
				var taxesAndSurcharges = '';

				if (bsinfo.oldItinerary.totalMilesForORB &&
					parseFloat(bsinfo.oldItinerary.totalMilesForORB) > 0) {

					subAndFareTotal = formatNumber(bsinfo.oldItinerary.totalMilesForORB, 2) +
						miles;

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.fareOldWithout +
						'</span>' +
						'<span class="values">' +
						subAndFareTotal +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.grandTotal &&
					parseFloat(bsinfo.oldItinerary.grandTotal) > 0) {

					taxesAndSurcharges = oldCurrency + ' ' +
						formatNumber(bsinfo.oldItinerary.grandTotal, 2);

					itineraryHtml += '<li data-old-fare="true">' +
						'<span>' +
						bspTexts.taxesAndSurcharges +
						'</span>' +
						'<span class="values">' +
						taxesAndSurcharges +
						'</span>' +
						'</li>';
				}

				if (subAndFareTotal || taxesAndSurcharges) {
					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						(subAndFareTotal ?
							'<span class="values">' + subAndFareTotal +
							(taxesAndSurcharges ? ' + ' : '') : '') +
						(taxesAndSurcharges ? '<span>' + taxesAndSurcharges + '</span>' : '') +
						'</span>' +
						'</li>';

					itineraryHtml += '<li data-old-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.previouslyPaid +
						'</span>' +
						(subAndFareTotal ?
							'<span class="values">' + subAndFareTotal +
							(taxesAndSurcharges ? ' + ' : '') : '') +
						(taxesAndSurcharges ? '<span>' + taxesAndSurcharges + '</span>' : '') +
						'</span>' +
						'</li>';
				}
			}

			itineraryHtml += '</ul>';
		}

		// New Itinerary
		// ATC
		if (bsinfo.scenario === 'ATC') {
			if (bsinfo.fareTotal || bsinfo.taxTotal || bsinfo.surchargeTotal ||
				bsinfo.rebookingFee || bsinfo.grandTotal) {

				itineraryHtml += '<h4 class="flights-cost-title">' +
					'<span class="text-left">' +
					bspTexts.newItinerary +
					'</span>' +
					'<span class="text-right">&nbsp;</span>' +
					'</h4>';

				itineraryHtml += '<ul class="flights-cost__details">';

				if (bsinfo.fareTotal) {
					itineraryHtml += '<li data-new-fare="true">' +
						'<span>' +
						bspTexts.fareNew +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.fareTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.taxTotal) {
					itineraryHtml += '<li data-new-taxes="true">' +
						'<span>' +
						bspTexts.taxes +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.taxTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.surchargeTotal) {
					itineraryHtml += '<li data-new-surcharge="true">' +
						'<span>' +
						bspTexts.surcharges +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.surchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.rebookingFee) {
					itineraryHtml += '<li data-new-rebooking="true">' +
						'<span>' +
						bspTexts.rebooking +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.rebookingFee, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.grandTotal) {
					itineraryHtml += '<li data-new-total="true" class="sub-total">' +
						'<span>' +
						bspTexts.total +
						'</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.grandTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.oldItinerary.costPaidByCash || bsinfo.oldItinerary.costPaidByMiles) {
					var totalPaid = (bsinfo.oldItinerary.costPaidByCash ?
						(bsinfo.oldItinerary.currency ? bsinfo.oldItinerary.currency + ' ' : '') + formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) : '') +
						(bsinfo.oldItinerary.costPaidByMiles && parseFloat(bsinfo.oldItinerary.costPaidByMiles) > 0 ? ' + ' + '<span>' + formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) + ' ' +	miles + '</span>' : '');

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyNewPaid +
						'</span>' +
						'<span class="values">' +
						totalPaid +
						// '<span>' + (bsinfo.currency ? bsinfo.currency + ' ' : '') +
						// formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>'
						// '</span>' +
						'</li>';
				}

				itineraryHtml += '</ul></div>';
			}
		}
		// Cancel: New Itinerary is not needed in cancel review page booking summary
		else if (bsinfo.scenario === 'Cancel') {
			// if (bsinfo.fareTotal || bsinfo.taxTotal || bsinfo.surchargeTotal ||
			// 	bsinfo.rebookingFee || bsinfo.grandTotal) {

			// 	itineraryHtml += '<h4 class="flights-cost-title">' +
			// 		'<span class="text-left">' +
			// 		bspTexts.itinerary +
			// 		'</span>' +
			// 		'<span class="text-right">&nbsp;</span>' +
			// 		'</h4>';

			// 	itineraryHtml += '<ul class="flights-cost__details">';

			// 	if (bsinfo.fareTotal) {
			// 		itineraryHtml += '<li data-new-fare="true">' +
			// 			'<span>' +
			// 			bspTexts.fareNew +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.fareTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.taxTotal) {
			// 		itineraryHtml += '<li data-new-taxes="true">' +
			// 			'<span>' +
			// 			bspTexts.taxes +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.taxTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.surchargeTotal) {
			// 		itineraryHtml += '<li data-new-surcharge="true">' +
			// 			'<span>' +
			// 			bspTexts.surcharges +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.surchargeTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.rebookingFee) {
			// 		itineraryHtml += '<li data-new-rebooking="true">' +
			// 			'<span>' +
			// 			bspTexts.rebooking +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.rebookingFee, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.grandTotal) {
			// 		itineraryHtml += '<li data-new-total="true" class="sub-total">' +
			// 			'<span>' +
			// 			bspTexts.total +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			formatNumber(bsinfo.grandTotal, 2) +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	if (bsinfo.oldItinerary && bsinfo.oldItinerary.costPaidByCash ||
			// 		bsinfo.oldItinerary.costPaidByMiles) {
			// 		itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
			// 			'<span>' +
			// 			bspTexts.previouslyNewPaid +
			// 			'</span>' +
			// 			'<span class="values">' +
			// 			(bsinfo.oldItinerary.costPaidByMiles ?
			// 			formatNumber(bsinfo.oldItinerary.costPaidByMiles, 0) +
			// 				bspTexts.miles +
			// 				(bsinfo.oldItinerary.costPaidByCash ? ' + ' : '') : '') +
			// 			(bsinfo.oldItinerary.costPaidByCash ?
			// 				'<span class="unit">' +
			// 				bsinfo.oldItinerary.currency + ' ' +
			// 				formatNumber(bsinfo.oldItinerary.costPaidByCash, 2) +
			// 				'</span>' : '') +
			// 			'</span>' +
			// 			'</li>';
			// 	}

			// 	itineraryHtml += '</ul></div>';
			// }
		}
		// ORC
		else if (bsinfo.scenario === 'ORC') {
			if (bsinfo.fareTotalInMiles || bsinfo.taxTotal || bsinfo.surchargeTotal ||
				bsinfo.rebookingFee || bsinfo.grandTotal || bsinfo.orbQSurchargeTotal ||
				bsinfo.totalMilesForORB) {

				itineraryHtml += '<h4 class="flights-cost-title">' +
					'<span class="text-left">' +
					bspTexts.newItinerary +
					'</span>' +
					'<span class="text-right">&nbsp;</span>' +
					'</h4>';

				itineraryHtml += '<ul class="flights-cost__details">';

				if (bsinfo.fareTotalInMiles) {
					itineraryHtml += '<li data-new-fare="true">' +
						'<span>' +
						bspTexts.fareNew + '</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.fareTotalInMiles, 0) + ' ' + miles +
						'</span>' +
						'</li>';
				}

				if (bsinfo.taxTotal) {
					itineraryHtml += '<li data-new-taxes="true">' +
						'<span>' +
						bspTexts.taxes +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.taxTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.surchargeTotal) {
					itineraryHtml += '<li data-new-surcharge="true">' +
						'<span>' +
						bspTexts.surcharges +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.surchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.orbQSurchargeTotal) {
					itineraryHtml += '<li data-new-orbQSurchargeTotal="true">' +
						'<span>' +
						bspTexts.qSurchargeTotal +
						'</span>' +
						'<span class="values">' +
						currency + ' ' + formatNumber(bsinfo.orbQSurchargeTotal, 2) +
						'</span>' +
						'</li>';
				}

				var rebookFee = '';
				var grandTotal = '';
				var totalMiles = '';

				if (bsinfo.rebookingFee) {
					rebookFee = currency + ' ' + formatNumber(bsinfo.rebookingFee, 2);

					itineraryHtml += '<li data-new-rebooking="true">' +
						'<span>' +
						bspTexts.rebooking + '</span>' +
						'<span class="values">' +
						formatNumber(bsinfo.rebookingFee, 2) +
						'</span>' +
						'</li>';
				}

				if (bsinfo.grandTotal) {
					grandTotal = currency + ' ' + formatNumber(bsinfo.grandTotal, 2);
				}

				if (bsinfo.totalMilesForORB) {
					totalMiles = formatNumber(bsinfo.totalMilesForORB, 2) +
						miles;
				}

				itineraryHtml += '<li data-new-total="true" class="sub-total">' +
					'<span>' +
					bspTexts.total +
					'</span>' +
					'<span class="values">' +
					totalMiles +
					(totalMiles && (grandTotal || rebookFee) ? ' + ' : '') +
					'<span>' +
					(grandTotal ? grandTotal + (rebookFee ? ' + ' : '') : '') +
					'</span>' +
					(rebookFee ? '<span>' + rebookFee + '</span>' : '') +
					'</span>' +
					'</li>';

				if (bsinfo.oldItinerary &&
					(bsinfo.oldItinerary.grandTotal || bsinfo.oldItinerary.fareTotalInMiles)) {

					itineraryHtml += '<li data-old-paid="true" class="pre-paid">' +
						'<span>' +
						bspTexts.previouslyNewPaid +
						'</span>' +
						'<span class="values">' +
						(bsinfo.oldItinerary.fareTotalInMiles ?
						formatNumber(bsinfo.oldItinerary.fareTotalInMiles, 0) +
						miles : '') +
						(bsinfo.oldItinerary.grandTotal ?
						(bsinfo.oldItinerary.fareTotalInMiles ? ' + ' : '') +
						'<span class="unit">' +
						formatNumber(bsinfo.oldItinerary.grandTotal, 2) +
						'</span>' : '') +
						'</span>' +
						'</li>';
				}

				itineraryHtml += '</ul></div>';
			}
		}

		bookingContentFlight.html(itineraryHtml);

		// add on if there is commonAddons attribute then rendering
		if (bsinfo.commonAddons && bsinfo.commonAddons.length) {
			var addTemplate = '';
			addTemplate += '<div class="flights-cost"><h4 class="flights-cost-title"><span class="text-left">' + bspTexts.addon + '</span></h4><ul class="flights-cost__details" data-addons="true">';

			for (var i = 0; i < bsinfo.commonAddons.length; i++) {
				addTemplate += '<li class="addon--item"><span>' + bsinfo.commonAddons[i].type + '</span><span class="price">' + bsinfo.commonAddons[i].amount + '</span></li>';
			}
			addTemplate += '<li class="sub-total"><span>' + bspTexts.subTotal + '</span><span class="price">' + bsinfo.currency + ' ' + bsinfo.addonSubTotal + '</span></li></ul></div>';
			$(addTemplate).appendTo(bookingContentFlight);
		}

		var convertPriceChb = $('.payment-currency').find('[data-toggler] input');
		var isConvertPriceChecked = convertPriceChb.length && convertPriceChb.is(':checked');
		var convertClass = isConvertPriceChecked ? '' : ' hidden';
		var convertTpl = bookingObject.template.convertedTpl;

		var textsPaid = '',
			totalToBePaidMiles = '',
			totalToBePaidCash = '',
			textsRefund = '',
			milesToBeRefunded = '',
			creditCard = '',
			cashToBeRefunded = '',
			milesNote = '',
			textConvert = '',
			krisFlyerAccount = '';

		// Cancel
		if (bsinfo.scenario === 'Cancel') {
			var cancellationText = '',
				remainExpiredMileText = '',
				cancellationFeeCash = '',
				cancellationFeeMiles = '',
				cancelHtml = '',
				remainHtml = '';

			textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ||  (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

			if (textsPaid) {
				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ?
					'<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) +
					'</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ?
					'<span>' + (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') +
					formatNumber(bsinfo.totalToBePaidMiles, 0) + miles + '</span>' : '');

				textConvert = typeof bsinfo.totalToBePaidCash !== 'undefined' ?
					convertTpl.format(
						convertClass,
						L10n.payment.convertText.format(bsinfo.totalToBePaidCash, 2)) : '';

				$(bookingObject.template.cancelPaid.format(
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textConvert
				)).appendTo(bookingContentFlight);
			}

			cancellationText = (bsinfo.cancellationFeeCash || (bsinfo.cancellationFeeMiles && parseFloat(bsinfo.cancellationFeeMiles) > 0) ? bspTexts.cancellation : '');

			if (cancellationText) {
				cancellationFeeCash = (bsinfo.cancellationFeeCash ? bsinfo.currency + ' ' + formatNumber(bsinfo.cancellationFeeCash, 2) : '');
				cancellationFeeMiles = (bsinfo.cancellationFeeMiles && parseFloat(bsinfo.cancellationFeeMiles) > 0 ? formatNumber(bsinfo.cancellationFeeMiles, 0) + miles : '');

				cancelHtml = '<li data-cancellation-fee="true" class="pre-paid"><span>' +
					cancellationText +
					'</span>' +
					'<span class="values">' +
					(cancellationFeeCash ?
						cancellationFeeCash + (cancellationFeeMiles ? ' + ' : '') : '') +
					(cancellationFeeMiles ? '<span class="unit">' + cancellationFeeMiles +
						'</span>' : '') +
					'</span></li>';
			}

			remainExpiredMileText = bsinfo.oldItinerary.taxTotal ? bspTexts.remainExpiredMile : '';

			if (remainExpiredMileText) {
				remainHtml = '<li data-remain-mile="true" class="pre-paid"><span>' +
					remainExpiredMileText +
					'</span>' +
					'<span class="values">' +
					formatNumber(bsinfo.oldItinerary.taxTotal, 0) + ' ' + miles +
					'</span></li>';
			}

			if (cancellationText || remainExpiredMileText) {
				$(bookingObject.template.cancelFee.format(cancelHtml, remainHtml))
					.appendTo(bookingContentFlight);
			}

			textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

			if (textsRefund) {
				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ?
					'<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				creditCard = bsinfo.creditCardNo ? '<span class="grand-sub-title">' + bspTexts.creditCard.format(bsinfo.creditCardNo.substring(bsinfo.creditCardNo.length - 5)) + '</span>' : '';

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ?
					'<span>' + (!creditCard && cashToBeRefunded ? '<small>+</small>&nbsp;' : '') +
					formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');

				krisFlyerAccount = bsinfo.KFAccount ? '<span class="grand-sub-title">' + bsinfo.KFAccount + '</span>' : '';

				$(bookingObject.template.cancelRefund.format(
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded,
					creditCard,
					krisFlyerAccount
				)).appendTo(bookingContentFlight);
			}
		}
		else {
			// set total to be paid
			var totalToBePaid = $('<div class="grand-total"></div>').appendTo(bookingContentFlight);

			// ATC
			if (bsinfo.scenario === 'ATC') {
				// textsPaid = ((bsinfo.totalToBePaidCash && parseFloat(bsinfo.totalToBePaidCash) > 0) || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = ''; // (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0 ? '<span>' + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = ((bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0) || (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0) ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				creditCard = bsinfo.creditCardNo ? '<span class="grand-sub-title">' + bspTexts.creditCard.format(bsinfo.creditCardNo.substring(bsinfo.creditCardNo.length - 5)) + '</span>' : '';

				milesToBeRefunded = (bsinfo.milesToBeRefunded && parseFloat(bsinfo.milesToBeRefunded) > 0 ? '<span>' + (!creditCard && typeof cashToBeRefunded !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.milesToBeRefunded, 0) + miles + '</span>' : '');

				krisFlyerAccount = bsinfo.KFAccount ? '<span class="grand-sub-title">' + bsinfo.KFAccount + '</span>' : '';

				milesNote = generateBSPNotes(bsinfo);
			}
			// ORC
			if (bsinfo.scenario === 'ORC') {
				textsPaid = (typeof bsinfo.totalToBePaidCash !== 'undefined' || (bsinfo.totalToBePaidMiles && parseFloat(bsinfo.totalToBePaidMiles) > 0) ? bspTexts.paid : '');

				totalToBePaidCash = (typeof bsinfo.totalToBePaidCash !== 'undefined' ? '<span class="unit">' + bsinfo.currency + ' ' + formatNumber(bsinfo.totalToBePaidCash, 2) + '</span>' : '');

				totalToBePaidMiles = (bsinfo.totalToBePaidMiles ? '<span>' + (typeof totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(bsinfo.totalToBePaidMiles, 0) + bspTexts.krisFlyerMiles + '</span>' : '');

				textsRefund = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? bspTexts.refund : '');

				cashToBeRefunded = (bsinfo.cashToBeRefunded && parseFloat(bsinfo.cashToBeRefunded) > 0 ? '<span>' + bsinfo.currency + ' ' + formatNumber(bsinfo.cashToBeRefunded, 2) + '</span>' : '');

				milesToBeRefunded = '';
				creditCard = '';
				milesNote = '';
			}
			// ORU
			if (bsinfo.scenario === 'ORU') {
				textsPaid = bspTexts.paid;
				totalToBePaidCash = (typeof oruJson.topUpCashForORU !== 'undefined' ? '<span class="unit">' + oruJson.currencyCode + ' ' + formatNumber(oruJson.topUpCashForORU, 2) + '</span>' : '');

				totalToBePaidMiles = '<span>' + (typeof totalToBePaidCash !== 'undefined' ? '<small>+</small>&nbsp;' : '') + formatNumber(oruJson.totMilesForORU, 0) + ' ' + bspTexts.krisFlyerMiles + '</span>';

				textsRefund = '';
				cashToBeRefunded = '';
				milesToBeRefunded = '';
				creditCard = '';
				milesNote = '';
			}

			textConvert = typeof totalToBePaidCash !== 'undefined' ?
				convertTpl.format(
					convertClass,
					L10n.payment.convertText.format(formatNumber(bsinfo.scenario === 'ORU' ?
						oruJson.topUpCashForORU : bsinfo.totalToBePaidCash, 2))
				) : '';

			totalToBePaid.html(
				bookingObject.template.grandTotal.format(
					textsPaid,
					totalToBePaidCash,
					totalToBePaidMiles,
					textsRefund,
					cashToBeRefunded,
					milesToBeRefunded,
					textConvert,
					creditCard,
					milesNote,
					krisFlyerAccount
				)
			);
		}

		if (body.hasClass('payments-page') && isConvertPriceChecked) {
			convertPriceChb.trigger('change.exchange');
		}
	};

	// Set state for radio button
	var preselectFlights = function() {
		if(globalJson.bookingSummary && globalJson.bookingSummary.fareAvailablityVO) {
			var flightInfo = globalJson.bookingSummary.fareAvailablityVO;
			if(flightInfo.dafaults) {
				for (var i = flightInfo.dafaults.length - 1; i >= 0; i--) {
					if(flightInfo.dafaults[i] !== null) {
						flightSearch.filter('[data-flight="' + (i + 1) + '"]').find('input[value="' + flightInfo.dafaults[i] + '"]').prop('checked', true).trigger('change.select-flight');
						$('[name="selectedFlightIdDetails[' + i + ']"]').val(flightInfo.dafaults[i]);
					}
				}
			}

			var radioEls = flightSearch.find('input:radio');

			//Check for enable flights
			radioEls.each(function() {
				if(!$.isEmptyObject(flightInfo.messages)) {
					var flightId = $(this).val();
					if(typeof(flightInfo.messages[flightId]) === 'undefined') {
						$(this).prop('disabled', true);
						$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
					}
					else {
						$(this).prop('disabled', false);
						$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
					}
				}
				else {
					$(this).prop('disabled', false);
					$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
				}
			});

			if($.isEmptyObject(flightInfo.messages)) {
				var firstFare = flightSearch.eq(0).find('input:checked:first');
				var isWaitlisted = firstFare.data('waitlisted');

				flightSearch.not(':first').find(isWaitlisted ? 'input[data-waitlisted="true"]' : 'input[data-waitlisted="false"]').each(function() {
					$(this).prop('disabled', true);
					$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
				});
			}

			radioEls.trigger('change.flightTableBorder');
		}
	};

	var payWithKfMilesChange = function() {
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		payWithKfMilesCheckbox.off('change.pay-with-miles').on('change.pay-with-miles', function() {
			setBookingSummaryFlightInfo();
		});
	};

	payWithKfMilesChange();

	var fillUpgradeBlocks = function() {
		flightUpgrades.empty().addClass('hidden');
	};

	// Print summary of fare conditions
	var printFareCondition = function(res) {
		if($('.flight-select-page').length && res.fareFamilyCondition) {
			var fareCondition = $('.summary-fare__conditions');
			var html = '';
			for(var i = 0; i < res.fareFamilyCondition.length; i++) {
				html += '<li>';
				if(res.fareFamilyCondition[i].isAllowed) {
					html += '<em class="ico-check-thick"></em>';
				}
				else {
					html += '<em class="ico-close"></em>';
				}
				html += res.fareFamilyCondition[i].description;
				html += '</li>';
			}
			fareCondition.html(html);
		}
	};

	// Render popup details
	var renderPopupDetails = function(res) {
		$.get(config.url.orbBookingSummaryDetailsPopupTemplate, function(data) {
			if(!$('.add-ons-page, .payments-page').length) {
				res.bookingSummary.commonAddons = [];
			}
			var template = window._.template(data, {
				data: res,
				confirmationPage: $('.orb-confirmation-page').length
			});
			var popupContent = $('.popup--flights-details .popup__content');
			popupContent.children(':not(.popup__close)').remove();
			popupContent.append(template);
			popupContent.find('[data-need-format]').each(function() {
				var number = unformatNumber($(this).text());
				$(this).text(formatNumber(number, $(this).data('need-format')));
			});

			popupContent
				.find('.flights--detail span')
				.off('click.getFlightInfo')
				.on('click.getFlightInfo', function() {
					var self = $(this);
					var details = self.siblings('.details');
					if(details.is(':empty')) {
						$.ajax({
							url: config.url.orbFlightInfoJSON,
							type: config.ajaxMethod,
							dataType: 'json',
							data: {
								flightNumber: self.parent().data('flight-number'),
								carrierCode: self.parent().data('carrier-code'),
								date: self.parent().data('date'),
								origin: self.parent().data('origin')
							},
							success: function(res) {
								self.children('em').toggleClass('ico-point-d ico-point-u');
								details.toggleClass('hidden');
								var html = '';
								html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
								for(var ft in res.flyingTimes) {
									html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
								}
								details.html(html);
							},
							error: function(jqXHR, textStatus, errorThrown) {
								console.log(jqXHR);
								if(textStatus !== 'abort') {
									window.alert(errorThrown);
								}
							},
							beforeSend: function() {
								self.children('em').addClass('hidden');
								self.children('.loading').removeClass('hidden');
							},
							complete: function() {
								self.children('.loading').addClass('hidden');
								self.children('em').removeClass('hidden');
							}
						});
					}
					else {
						self.children('em').toggleClass('ico-point-d ico-point-u');
						if (details.is('.hidden')) {
							details.hide().removeClass('hidden').slideDown(400);
						}
						else {
							details.slideUp(400, function() {
								details.addClass('hidden');
							});
						}
					}
				});
		});
	};

	var showPeyMessage = function(res) {
		if($('.flight-select-page').length && res.bookingSummary.flight.length) {
			var flights = res.bookingSummary.flight;
			$.each(flights, function(idx, flight) {
				var fs = flightSearch.eq(idx);
				var packageMsg = fs.find('.package-message');
				var pkm = fs.find('input:checked').closest('div').find('.package-message');
				packageMsg.addClass('hidden');
				if (pkm.length && flight.cabinMismatch && flight.cabinMismatch === 'true') {
					pkm.removeClass('hidden');
				}
			});
		}
	};

	var BSPAjax;

	// Call Ajax for Booking sumary
	var callBSPAjax = function(onchange, extData, callback, radioEl) {
		var data = {
		};
		$.extend(data, extData);
		if(onchange) {
			flightSearch.each(function(i, it) {
				var selectedFlightId = $(it).find('input:radio:checked').first().val();
				if(selectedFlightId) {
					selectedFlightId = i + '|' + selectedFlightId.substring(2);
					data['selectedFlightIdDetails[' + i + ']'] = selectedFlightId;
				}
			});
		}
		if(BSPAjax) {
			BSPAjax.abort();
		}
		BSPAjax = $.ajax({
			url: onchange ? config.url.mbFlightSelectOnChange : config.url.mbFlightSelect,
			type: config.ajaxMethod,
			data: data,
			dataType: 'json',
			success: function(res) {
				globalJson.bookingSummary = res;

				if($('.payments-page').length) {
					var triggerCostPayableByCash = new jQuery.Event('change.costPayableByCash');
					var bspInfo = res.bookingSummary;
					var scenario = bspInfo.scenario;

					// triggerCostPayableByCash.cash = res.bookingSummary.costPayableByCash;

					triggerCostPayableByCash.cash = (scenario === 'ORU' ?
						res.oruUpgradeJsonVO.topUpCashForORU : bspInfo.totalToBePaidCash);
					bookingSummaryWidget.trigger(triggerCostPayableByCash);
				}

				headingBSP();
				preselectFlights();
				setBookingSummaryFlightInfo();
				fillUpgradeBlocks();
				printFareCondition(res);
				renderPopupDetails(res);
				showPeyMessage(res);

				if(typeof(callback) === 'function') {
					callback();
				}

				toggleBookingSummary();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				if(textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			},
			beforeSend: function() {
				passengerCount
				.text('...');

				if(body.hasClass('flight-select-page') && !body.hasClass('mb-payments-page') &&
					onchange && radioEl) {
					var bspInfo = $('.booking-summary').find('.booking-summary__info');
					bspInfo.find('.total-cost').addClass('hidden');
					bspInfo.find('.fare-notice').addClass('hidden');
					bspInfo.find('.total-title').addClass('hidden');
					bspInfo.siblings('.loading--medium-2').removeClass('hidden');
					flightSearch.find('input:radio').not(radioEl).not(':checked').prop('disabled', true);
				}
				else {
					SIA.preloader.show();
				}
			},
			complete: function() {
				if (body.hasClass('flight-select-page') && !body.hasClass('mb-payments-page')){
					var summaryFareP = $('.summary-fare').find('p').eq(0);
					var cloneBsp = $();

					summaryFareP.siblings('.booking-summary').remove();
					cloneBsp = bookingSummaryWidget.clone(true, true).insertAfter(summaryFareP);
					cloneBsp.addClass('visible-mb').find('[data-tooltip]').removeData('kTooltip').kTooltip();

					if(onchange) {
						var bspInfo = $('.booking-summary').find('.booking-summary__info');

						bspInfo.find('.total-cost').removeClass('hidden');
						bspInfo.find('.fare-notice').removeClass('hidden');
						bspInfo.find('.total-title').removeClass('hidden');
						bspInfo.siblings('.loading--medium-2').addClass('hidden');
					}
				}
				else {
					SIA.preloader.hide();
				}
			}
		});
	};

	callBSPAjax(false, {}, function() {
		$('[data-flight]').each(function() {
			$(this).find('input:radio:checked').eq(0).trigger('change.select-flight');
		});
	});

	var paymentKFMiles = function() {
		bookingSummaryWidget.off('change.KfMiles').on('change.KfMiles', function(e) {
			callBSPAjax(true, {
				selectedMiles: e.miles
			}, e.callback);
		});
	};

	paymentKFMiles();

	// Fill data to table flight search
	var fillData = function() {
		flightSearch
		.off('change.fillData')
		.on('change.fillData', 'input[type="radio"]', function(e) {
			if(!bookingSummaryWidget.length){
				return;
			}
			var radio = $(this);
			var tableIndex = flightSearch.index($(this).closest('.flights__searchs'));
			$('[name="selectedFlightIdDetails[' + tableIndex + ']"]').val($(this).val());

			if(!flightSearch.data('remove-wailist')) {
				callBSPAjax(true, {
					tripType: $(this).val()[0]
				}, function() {
					if(e.originalEvent) {
						toogleTooltip(tooltipPopup, L10n.bookingSummary.fare, radio.next('label').find('strong.package--price-number').text());
					}
				}, radio);
			}
		});

		flightUpgrades
		.off('change.fillData')
		.on('change.fillData', 'input[type="checkbox"]', function(e) {
			var checkbox = $(this);
			var isChecked = $(this).is(':checked');

			setBookingSummaryFlightInfo();
			if(e.originalEvent && isChecked) {
				toogleTooltip(tooltipPopup, checkbox.data('upgrade-title'), checkbox.data('upgrade-price'));
			}
		});
	};

	fillData();

	// Scroll popup
	var popupScroll = function() {
		var trigger = bookingSummaryWidget.find('[data-popup-anchor]');
		trigger.off('click.setAnchor').on('click.setAnchor', function() {
			var anchor = $(this).data('popup-anchor');
			var popup = $($(this).data('trigger-popup'));
			popup.data('anchor', anchor);
		});
		trigger.each(function() {
			var popup = $($(this).data('trigger-popup'));
			if(!popup.data('boundScroll')) {
				popup.data('boundScroll', true);
				popup.off('afterShow.scrollToAnchor').on('afterShow.scrollToAnchor', function() {
					var pop = $(this);
					window.setTimeout(function() {
						var paddingTop = parseInt(popup.find('.popup__content').css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						if(anchorElement.length){
							pop.scrollTop(anchorElement.position().top - paddingTop);
						}
					}, 1);
				});
			}
		});
	};

	popupScroll();

	// Toggle Booking sumary
	var toggleBookingSummary = function() {
		bookingSummaryControl = bookingSummaryHeading.find('.booking-summary__control');
		bookingSummaryControl.off('click.openBS').on('click.openBS', function(e){
			e.preventDefault();
			if($('.orb-flight-select-page').length) {
				if($('[data-flight] input[type=radio]:checked').length === 0) {
					return;
				}
			}

			var bspWidget = $(this).closest('.booking-summary');
			var bspContent = bspWidget.find('.booking-summary__content');
			bspWidget.toggleClass('active');
			bspContent.toggle(0);

			// bookingSummaryWidget.toggleClass('active');
			// bookingSummaryContent.toggle(0);
		});

		if(collapseBsp.length){
			collapseBsp.off('click.openBS').on('click.openBS', function(e){
				e.preventDefault();
				$(this).closest('.booking-summary').find('.booking-summary__control').trigger('click.openBS');
			});
		}

		if($('.payments-page').length) {
			bookingSummaryControl.trigger('click.openBS');
		}
	};

	// Toggle tooltip
	var toogleTooltip = function(tooltipElement, upperText, priceAdded) {
		if(!tooltipElement.hasClass('active')) {
			if(!bookingSummaryWidget.length){
				return;
			}
			var overwriteTxtTooltip = tooltipElement.find('.tooltip__content');
			overwriteTxtTooltip.html(
				upperText +
				'<span class="text-1">+' + formatNumber(priceAdded) + ' miles</span>'
			);
			var top = 15;
			if(bookingSummaryWidget.offset().top + bookingSummaryWidget.height() >= $(window).scrollTop()) {
				top = bookingSummaryWidget.offset().top + bookingSummaryWidget.height() - $(window).scrollTop() + 10;
			}
			tooltipElement.addClass('active').stop().css({
				position: 'fixed',
				top: top,
				right: 10
			}).fadeIn(400).delay(2000).fadeOut(400, function() {
				$(this).removeClass('active');
			});
		}
	};
};
