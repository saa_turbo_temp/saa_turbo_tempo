
/**
 * @name SIA
 * @description Define global redemptionNominee functions
 * @version 1.0
 */
SIA.redemptionNominee = function(){
	var global = SIA.global;
	var vars = global.vars;
	var win = vars.win;
	var body = vars.body;

	var htmlBody = $('html, body');
	var flyingFocus = $('#flying-focus');
	var timeAnimate = 400;
	var delayAnimation = 500;
	var smoothTime = 400;
	var clearDelay;
	var openedConAccord = $();
	var openedTriggerAccord = $();
	var popupUnload = $('.popup--unload');
	var popupDeleteConfirm = $('[data-popup-delete-nominee]');
	var groupUserInfor = $('[data-group-user-infor] li', popupDeleteConfirm);
	var popupUnloadConBtn = $('[data-confirm]', popupUnload);
	var inforPlaceHolder = [];
	var radio = $('input:radio');

	var nominee = function(){

		if(!popupDeleteConfirm.data('Popup')) {
			popupDeleteConfirm.Popup({
				overlayBGTemplate: SIA.global.config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]'
			});
		}

		if(!popupUnload.data('Popup')) {
			popupUnload.Popup({
				overlayBGTemplate: SIA.global.config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]'
			});
		}

		var detectDeleteUser = function(cl){
			cl.each(function(idx, el){
				if($(el).is(':checked')){
					groupUserInfor.eq(idx).show().find(':input').val('true');
				}
				else{
					groupUserInfor.eq(idx).hide().find(':input').val('false');
				}
			});
		};

		var animateHtmlBody = function(trigger){
			if(trigger.offset().top === win.scrollTop()){
				return;
			}
			htmlBody.stop().animate({
				scrollTop: trigger.offset().top
			}, timeAnimate, function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}

				// fix issue for set window.location.hash faqs pages
				if (body.hasClass('faqs-pages')) {
					if (trigger.hasClass('open-all-btn')) {
						window.location.hash = '';
					}
					else {
						trigger.trigger('setHash.faqsPages');
					}
				}
			});
		};

		var setDisable = function(el){
			el.prop('disabled', true);
		};
		var removeDisable = function(el){
			el.prop('disabled', false);
		};

		// var renderInforPlaceHolder = function(wp){
		// 	wp.each(function(){
		// 		inforPlaceHolder.push({});
		// 	});
		// };

		var setDataPlaceholder = function(wp, query){
			wp.each(function(){
				inforPlaceHolder.push(getUserInfor($(this), query));
			});
		};

		var getUserInfor = function(wp, query){
			var t = [];
			$(query, wp).each(function(){
				var that = $(this);
				var obj = {
					name: that.attr('name'),
					value: that.val()
				};
				if(that.is(':radio')){
					obj.isChecked = that.prop('checked');
				}
				t.push(obj);
			});
			return t;
		};

		var setPerUserInfor = function(wp, query, data){
			$(query, wp).each(function(idx){
				var that = $(this);
				that.val(data[idx].value);
				if(that.is('select')){
					that.defaultSelect('refresh');
				}
				if(that.is(':radio')){
					that.prop('checked', data[idx].isChecked);
					if(data[idx].isChecked){
						that.closest('.custom-radio').addClass('active');
					}
					else{
						that.closest('.custom-radio').removeClass('active');
					}
				}
			});
		};

		var setAllInfor = function(wp, query, data){
			wp.each(function(idx){
				setPerUserInfor($(this), query, data[idx]);
			});
		};

		$('[data-nominee-module="true"]').each(function(){
			var nomineeModule = $(this);

			var trigerCol = $('[data-accordion-trigger=1]', nomineeModule);
			var contentCol = $('[data-accordion-content=1]', nomineeModule);
			var contentWrapper = $('[data-accordion-wrapper=1]', nomineeModule);
			var allCheckboxDelete = $('input[type="checkbox"]',trigerCol);

			var nomineeAdd = $('[data-nominee-add="true"]', nomineeModule);
			var nomineeDelete = $('[data-nominee-delete="true"]', nomineeModule);
			var nomineeEdit = $('[data-nominee-edit="true"]', nomineeModule);
			var nomineeInfor = $('[data-nominee-information="true"]', nomineeModule);
			var nomineeAlert = $('.nominee-alert', nomineeModule);
			var nomineeItem = $('[data-nominee-item]', nomineeInfor);

			var buttonNominee = $('.button-group-nominee', nomineeModule);
			var buttonNomineeCancel = $('[data-button-cancel]', buttonNominee);

			var buttonGroupDelete = $('.button-group-delete', nomineeModule);
			var buttonGroupDeleteCancel = $('[data-button-cancel]', buttonGroupDelete);
			var buttonGroupDeleteNext = $('.btn-noneditable', buttonGroupDelete);

			var inputUserInfor = $('[name="userInfor"]', nomineeInfor);
			var radioGroupLeft = $('.radio-group--left', nomineeInfor);
			var editableQuery = '[data-nominee-editable="true"]';
			var nomineeUserInfoQuery = '[data-user-info]';



			var nItemActivity = function(its, query){
				its.each(function(){
					var it = $(this);
					var trigerAcc = $('[data-accordion-trigger=1]', it);
					var contentAcc = $('[data-accordion-content=1]', it);

					setDisable(it.find(query));

					trigerAcc.off('beforeAccordion.open').on('beforeAccordion.open', function(){
						if(!nomineeAlert.hasClass('hidden')){
							nomineeAlert.addClass('hidden');
						}
					});

					trigerAcc.off('afterAccordion.open').on('afterAccordion.open', function(){
						openedConAccord = contentAcc;
						openedTriggerAccord = trigerAcc;
						if(!trigerCol.hasClass('active')){
							contentAcc.addClass('non-editable');
							buttonNominee.addClass('hidden');
							nomineeEdit.removeClass('edit-detail');
							setDisable(it.find(query));
							setDataPlaceholder(nomineeItem, nomineeUserInfoQuery);
							// setPerUserInfor(openedConAccord, nomineeUserInfoQuery, inforPlaceHolder[nomineeItem.index(openedConAccord.parent())]);
							nomineeInfor.resetForm();
						}
						else{
							// contentAcc.removeClass('non-editable');
							if(!nomineeItem.hasClass('active-delete')){
								// nomineeEdit.addClass('edit-detail');
								// buttonNominee.removeClass('hidden');
								// removeDisable(it.find(query));
							}
						}
						if(it.hasClass('border-error')){
							removeDisable(it.find(query));
							it.find('.accordion__control').next().removeClass('non-editable');
							buttonNominee.removeClass('hidden');
							nomineeEdit.addClass('edit-detail');
						}
					});

					trigerAcc.off('stayAcc.ifopen').on('stayAcc.ifopen', function(){
						if(clearDelay){
							clearTimeout(clearDelay);
						}
						clearDelay = setTimeout(function(){
							contentCol.not(contentAcc).addClass('non-editable');
							setDisable(its.not(it).find(query));
							animateHtmlBody(trigerAcc);
						}, delayAnimation);
					});
				});
			};

			var openAllAcc = function(its, query){
				its.each(function(){
					$('[data-accordion-content=1]',$(this)).removeClass('non-editable');
					removeDisable($(this).find(query));
					if(buttonNominee.is(':hidden')){
						buttonNominee.removeClass('hidden');
					}
				});
			};

			var closeAllAcc = function(its, query){
				its.each(function(){
					$('[data-accordion-content=1]',$(this)).addClass('non-editable');
					setDisable($(this).find(query));
					buttonNominee.addClass('hidden');
				});
			};

			var disabledDeleteNext = function(){
				buttonGroupDeleteNext.addClass('disabled').attr('disabled', true);
			};

			var editActivity = function(){
				if(!trigerCol.hasClass('active')){
					contentWrapper.accordion('openAll');
				}
				else{
					openedConAccord.removeClass('non-editable');
					buttonNominee.removeClass('hidden');
					removeDisable(openedConAccord.find(editableQuery));
				}
				// contentWrapper.accordion('openAll');
				buttonNominee.removeClass('hidden');
				nomineeEdit.addClass('edit-detail');
			};

			var deleteActivity = function(){
				trigerCol.removeClass('non-editable');

				contentCol.addClass('non-editable');
				setDisable(nomineeItem.find(editableQuery));

				setPerUserInfor(openedConAccord, nomineeUserInfoQuery, inforPlaceHolder[nomineeItem.index(openedConAccord.parent())]);
				nomineeInfor.resetForm();
				nomineeItem.removeClass('border-error');

				nomineeItem.addClass('active-delete');
				buttonGroupDelete.removeClass('hidden');
				removeDisable(allCheckboxDelete);
			};

			var bindEventRadioGroup = function(){
				radioGroupLeft.each(function(){
					var rd = $(this).find(':radio');
					rd.off('change.changeState').on('change.changeState', function(){
						rd.closest('.custom-radio').removeClass('active');
						$(this).closest('.custom-radio').addClass('active');
					});
				});
			};

			var cancelActivity = function(){
				if(contentWrapper.data('accordion').element.isOpenAll){
					closeAllAcc(nomineeItem, editableQuery);
					contentWrapper.accordion('closeAll');
					setAllInfor(nomineeItem, nomineeUserInfoQuery, inforPlaceHolder);
				}
				else{
					openedTriggerAccord.trigger('click.accordion');
					setPerUserInfor(openedConAccord, nomineeUserInfoQuery, inforPlaceHolder[nomineeItem.index(openedConAccord.parent())]);
				}
				nomineeInfor.resetForm();
				nomineeItem.removeClass('border-error');
			};


			// contentCol.removeClass('non-editable');
			setDataPlaceholder(nomineeItem, nomineeUserInfoQuery);
			nItemActivity(nomineeItem, editableQuery);
			bindEventRadioGroup();
			setDisable(allCheckboxDelete);

			popupUnload.off('beforeHide.hidePopup').on('beforeHide.hidePopup', function(){
				nomineeAdd.removeClass('leavePage');
			});

			nomineeAdd.off('click.leavePage').on('click.leavePage', function(e){
				if(nomineeItem.hasClass('active-delete') || nomineeEdit.hasClass('edit-detail')){
					e.preventDefault();
					popupUnload.Popup('show');
				}
				nomineeAdd.addClass('leavePage');
			});

			nomineeDelete.off('click.delete').on('click.delete', function(e){
				nomineeDelete.addClass('process-delete');
				if(nomineeEdit.hasClass('edit-detail')){
					e.preventDefault();
					popupUnload.Popup('show');
				}
				else{
					deleteActivity();
				}
			});

			buttonGroupDeleteCancel.off('click.deleteCancel').on('click.deleteCancel', function(){
				trigerCol.addClass('non-editable');
				nomineeItem.removeClass('active-delete');
				buttonGroupDelete.addClass('hidden');
				allCheckboxDelete.prop('checked', false);

				// setDisable(allCheckboxDelete);
				// contentCol.removeClass('non-editable');
				removeDisable(nomineeItem.find(editableQuery));
				detectDeleteUser(allCheckboxDelete);
				disabledDeleteNext();
				nomineeDelete.removeClass('process-delete');
			});

			buttonGroupDeleteNext.off('click.nextConfirm').on('click.nextConfirm', function(e){
				e.preventDefault();
				popupDeleteConfirm.Popup('show');
			});

			nomineeEdit.off('click.edit').on('click.edit', function(){
				if(nomineeItem.hasClass('active-delete')){
					popupUnload.Popup('show');
				}
				else{
					editActivity();
				}
			});

			buttonNomineeCancel.off('click.editCancel').on('click.editCancel', function(){
				if(nomineeEdit.hasClass('edit-detail')){
					popupUnload.Popup('show');
				}
				else{
					cancelActivity();
				}
			});

			allCheckboxDelete.off('change.delete').on('change.delete', function(){
				if(allCheckboxDelete.is(':checked')){
					buttonGroupDeleteNext.removeClass('disabled').attr('disabled', false);
				}
				else{
					disabledDeleteNext();
				}
				detectDeleteUser(allCheckboxDelete);
			});

			contentWrapper.off('openAll.removeDisable').on('openAll.removeDisable', function(){
				openAllAcc(nomineeItem, editableQuery);
				if(clearDelay){
					clearTimeout(clearDelay);
				}
				clearDelay = setTimeout(function(){
					animateHtmlBody(trigerCol.eq(0));
				}, smoothTime);
			});

			popupUnloadConBtn.off('click.continueEdit').on('click.continueEdit', function(e){
				e.preventDefault();
				if(nomineeAdd.hasClass('leavePage')){
					window.location.href = nomineeAdd.attr('href');
				}
				else if(nomineeEdit.hasClass('edit-detail')){
					nomineeEdit.removeClass('edit-detail');
					buttonNomineeCancel.trigger('click.editCancel');
					popupUnload.Popup('hide');
					if(nomineeDelete.hasClass('process-delete')){
						deleteActivity();
					}
				}
				else{
					buttonGroupDeleteCancel.trigger('click.deleteCancel');
					popupUnload.Popup('hide');
					editActivity();
				}
				nomineeDelete.removeClass('process-delete');
			});

			vars.invalidHandler = function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
					if (errorColEl.length && !errorColEl.parents('.popup__content').length) {
						win.scrollTop(errorColEl.offset().top - 40);
					}
				}
			};

			nomineeInfor.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				// success: global.vars.validateSuccess,
				success: function(label, element) {

					var group = $(element).closest('.form-group, [data-validate-row]');
					var isValidateByGroup = group.data('validate-by-group');
					var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
					var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
					// var item = $(element).closest('[data-nominee-item]');

					// item.each(function(){
					// 	if(!$(this).find('.error').length){
					// 		$(this).removeClass('border-error');
					// 	}
					// });


					// var inputsInGroup = group.find(':input');
					// var validator = $(element).closest('form').data('validator');

					var removeMessage = function(container) {
						container.removeClass('error');
					};
					//If it's not validating by group
					if (!isValidateByGroup) {
						removeMessage(gridInner);
					}
					if(!$(element).closest('[data-nominee-item]').find('.error').length){
						$(element).closest('[data-nominee-item]').removeClass('border-error');
					}
				},
				// invalidHandler: global.vars.invalidHandler,
				invalidHandler: function(form, validator){
					var errors = validator.numberOfInvalids();
					if (errors) {
						var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
						$(validator.errorList).each(function(){
							$(this.element).closest('[data-nominee-item]').addClass('border-error');
						});
						if (errorColEl.length && !errorColEl.parents('.popup__content').length) {
							var tr = errorColEl.closest('[data-accordion]').find('[data-accordion-trigger]');
							if(!tr.hasClass('active')){
								tr.trigger('click.accordion');
							}
							if(clearDelay){
								clearTimeout(clearDelay);
							}
							clearDelay = setTimeout(function(){
								win.scrollTop(errorColEl.offset().top - 40);
							}, delayAnimation + 1000);
						}
					}
				},
				onfocusout: global.vars.validateOnfocusout,
				ignore: '',
				submitHandler: function() {
					inforPlaceHolder = [];
					setDataPlaceholder(nomineeItem, nomineeUserInfoQuery);
					inputUserInfor.val(JSON.stringify(inforPlaceHolder));
					$.ajax({
						url: global.config.url.success,
						type: global.config.ajaxMethod,
						dataType: 'json',
						data: nomineeInfor.serialize(),
						success: function(res){
							if(res.success){
								nomineeAlert.removeClass('hidden');
								contentWrapper.data('accordion').element.isOpenAll = true;
								closeAllAcc(nomineeItem, editableQuery);
								contentWrapper.accordion('closeAll');
								nomineeItem.removeClass('active-delete');
								nomineeEdit.removeClass('edit-detail');
								if(clearDelay){
									clearTimeout(clearDelay);
								}
								clearDelay = setTimeout(function(){
									animateHtmlBody(nomineeModule);
								}, delayAnimation);
							}
						}
					});
					return false;
				}
			});
		});

		radio.each(function(){
			var self = $(this);
			var selfParent = self.parents('.form-group');
			var nomineeCheckBox = selfParent.find('.authorised-nominee');

			self.off('change.changeState').on('change.changeState', function(){
				self.parent().next().removeClass('active');
				self.parent().next().find(radio).removeAttr('checked');
				self.parent().addClass('active');
				self.attr('checked', 'checked');

				if( self.val() === 'yes' ){
					nomineeCheckBox.show();
					nomineeCheckBox.find('input').rules('add', 'required');
				}
				else{
					nomineeCheckBox.hide();
					nomineeCheckBox.find('input').rules('remove', 'required');
					selfParent.find('.error').removeClass('error');
					selfParent.find('.text-error').remove();
				}
			});
		});
	};

	var initModule = function(){
		nominee();
	};

	initModule();
};
