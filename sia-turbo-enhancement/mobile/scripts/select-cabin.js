/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
var globalJson = globalJson || {};

SIA.selectCabin = function(){
	var selectCabin = function(){
		var widget = $('[data-widget]').not('[data-widget-hotel]');
		widget.each(function(){
			var formTravel = $(this);
			var	selCabin = $('[data-class] select', formTravel),
				selAdult = $('[data-adult] select', formTravel),
				selChild = $('[data-child] select', formTravel),
				selInfant = $('[data-infant] select', formTravel),
				dataJson,
				totalPassengers = 9,
				cabin,
				adult,
				children = 0,
				infant = 0;
			var maxAdult = 9;
			var maxChild = 7;
			var maxInfant = 4;
			if(!selCabin.length && !selAdult.length && !selChild.length && !selInfant.length){
				return;
			}

			var buildSel = function(length, sel, isAdult){
				var num = isAdult ? 1 : 0,
					hasSelected = false,
					options = [],
					val = parseInt(sel.val());
				sel.empty();
				for(var i = num; i < length + num; i++){
					var isSelected = '';
					var option = null;
					if(val === i){
						isSelected = 'selected';
						hasSelected = true;
					}
					option = '<option value="' + i + '"' + isSelected + '>' + i + '</option>';
					options.push(option);
				}
				if(!options.length) {
					options = ['<option value="0" selected>0</option>'];
				}
				sel.html(options.join(''));
				if(!hasSelected){
					sel.prop('selectedIndex', 0);
					val = num;
				}
				/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
					sel.closest('[data-customSelect]').customSelect('_createTemplate');
					sel.closest('[data-customSelect]').trigger('afterSelect');
				}
				else{
					sel.trigger('change.changeCabin');
					sel.closest('[data-customSelect]').find('.select__text').text(val);
				}*/
				//sel.trigger('change');
				sel.defaultSelect('refresh');
			};
			var calculateRemain = function(totalPassengers,currentSelectvalue,otherSelectValue,max){
				var remain = totalPassengers - currentSelectvalue;
				var result = remain - otherSelectValue;
				if(result > max){
					result = max;
				}
				return result;
			};
			var change = function(type, value){
				switch(type){
					case 'cabin':
						if (value === 'economy'){
							if(formTravel.data('searchFlightsForm') === 'redeem') {
								cabin = dataJson.economyRedem;
								maxAdult = 6;
								maxChild = 5;
								maxInfant = 0;
								totalPassengers = 6;
							} else {
								cabin = dataJson.economy;
								maxAdult = 9;
								maxChild = 7;
								maxInfant = 4;
								totalPassengers = 9;
							}
						}
						if(value === 'business'){
							cabin = dataJson.business;
							maxAdult = 4;
							maxChild = 3;
							maxInfant = 2;
							totalPassengers = 4;
						}
						if(value === 'firstSuite'){
							cabin = dataJson.firstSuite;
							maxAdult = 4;
							maxChild = 3;
							maxInfant = 2;
							totalPassengers = 4;
						}
						adult = cabin.adult;
						selAdult.val(selAdult.find('[selected]').text() || 1);
						selChild.val(0).defaultSelect('refresh');
						// selInfant.val(0);
						change('adult', 1);
						buildSel(adult.length, selAdult, true);
						/*buildSel(maxChild + 1, selChild, false);
						buildSel(maxInfant + 1, selInfant, false);
						buildSel(adult.length, selAdult, true);*/
						break;
					case 'adult':
						//var detecCabin = selCabin.length ? selCabin.val() : activeClass;
						var detecCabin = (typeof activeClass !== 'undefined') ? activeClass : selCabin.val();
						if(detecCabin === 'economy'){
							if(formTravel.data('searchFlightsForm') === 'redeem') {
								if(parseInt(value) === 1){
									maxChild = 5;
								}
								else if(parseInt(value) === 2){
									maxChild = 4;
								}
								else if(parseInt(value) === 3){
									maxChild = 3;
								}
								else if(parseInt(value) === 4){
									maxChild = 2;
								}
								else if(parseInt(value) === 5){
									maxChild = 1;
								}
								maxInfant = 0;
							} else {
								if(parseInt(value) === 1){
									maxChild = 5;
									maxInfant = 1;
								}
								else if(parseInt(value) === 2){
									maxChild = 7;
									maxInfant = 2;
								}
								else if(parseInt(value) === 3){
									maxChild = 6;
									maxInfant = 3;
								}
								else{
									maxChild = 7;
									maxInfant = 4;
								}
							}
						}
						else if(detecCabin === 'business'){
							if(parseInt(value) === 1){
								maxChild = 3;
								maxInfant = 1;
							}
							else if(parseInt(value) === 2){
								maxChild = 2;
								maxInfant = 2;
							}
							else if(parseInt(value) === 3){
								maxChild = 1;
								maxInfant = 1;
							}
						}
						else if ('firstSuite' === detecCabin) {
							if (parseInt(value) === 1) {
								maxChild = 3;
								maxInfant = 1;
							}
							else if(parseInt(value) === 2){
								maxChild = 2;
								maxInfant = 2;
							}
							else if(parseInt(value) === 3){
								maxChild = 1;
								maxInfant = 1;
							}
						}

						children = calculateRemain(totalPassengers,parseInt(value, 10),selInfant.val(),maxChild);
						infant = calculateRemain(totalPassengers,parseInt(value, 10),selChild.val(),maxInfant);
						buildSel(children + 1, selChild, false);
						buildSel(infant + 1, selInfant, false);
						break;
					case 'children':
						adult = calculateRemain(totalPassengers,parseInt(value, 10),selInfant.val(),maxAdult);
						infant = calculateRemain(totalPassengers,parseInt(value, 10),selAdult.val(),maxInfant);
						buildSel(adult, selAdult, true);
						buildSel(infant + 1, selInfant, false);
						break;
					case 'infant':
						adult = calculateRemain(totalPassengers,parseInt(value, 10),selChild.val(),maxAdult);
						children = calculateRemain(totalPassengers,parseInt(value, 10),selAdult.val(),maxChild);
						buildSel(adult, selAdult, true);
						buildSel(children +1, selChild, false);
						break;
					default:
						break;
				}


				// if(cabins !== 0){
				// 	cabin = data.cabin[cabins - 1];
				// 	adult = cabin.adult;
				// 	selAdult.empty();
				// 	for(var i = 1; i < adult.length + 1; i++){
				// 		var isSelected = '';
				// 		if(i === 1){
				// 			isSelected = 'selected';
				// 		}
				// 		$('<option value="' + i + '"' + isSelected + '>' + i + '</option>').appendTo(selAdult);
				// 	}
				// 	if(!window.Modernizr.touch){
				// 		selAdult.closest('[data-customSelect]').customSelect('_createTemplate');
				// 		selAdult.closest('[data-customSelect]').trigger('afterSelect');
				// 	}else{
				// 		selAdult.trigger('change.changeCabin');
				// 	}

				// }else{
				// 	var selectedAdult = adult[adults - 1];

				// 	selChild.empty();
				// 	selInfant.empty();

				// 	for(var j = 1; j < selectedAdult.children + 1; j++){
				// 		var isChildSelected = '';
				// 		if(j === 1){
				// 			isChildSelected = 'selected';
				// 		}
				// 		$('<option value="' + j + '"' + isChildSelected + '>' + j + '</option>').appendTo(selChild);
				// 	}

				// 	for(var k = 1; k < selectedAdult.infant + 1; k++){
				// 		var isInfantSelected = '';
				// 		if(k === 1){
				// 			isInfantSelected = 'selected';
				// 		}
				// 		$('<option value="' + k + '"' + isInfantSelected + '>' + k + '</option>').appendTo(selInfant);
				// 	}
				// 	if(!window.Modernizr.touch){
				// 		selChild.closest('[data-customSelect]').customSelect('_createTemplate');
				// 		selInfant.closest('[data-customSelect]').customSelect('_createTemplate');
				// 	}
				// }

			};

			if(globalJson.cabinData) {
				dataJson = globalJson.cabinData.cabin;
				selAdult.off('change.changeCabin').on('change.changeCabin', function(){
					change('adult', selAdult.val());
				});
				selChild.off('change.changeCabin').on('change.changeCabin', function(){
					change('children', selChild.val());
				});
				selInfant.off('change.changeCabin').on('change.changeCabin', function(){
					change('infant', selInfant.val());
				});
				if(typeof activeClass !== 'undefined'){
					change('cabin', activeClass);
				}
				else{
					selCabin.off('change.changeCabin').on('change.changeCabin', function(event, flag){
						if(flag === true) {
							change('cabin', $(this).find('[selected]').val());
						} else {
							change('cabin', $(this).find(':selected').val());
						}
					});
					//selCabin.closest('[data-customSelect]').customSelect('_createTemplate');
					selCabin.trigger('change.changeCabin');
					// fix bug cabin overlap datepicker
					var datepicker = $('[data-start-date],[data-return-date], [data-oneway]');
					selCabin.off('focus.hideDatepicker').on('focus.hideDatepicker', function(){
						if(datepicker.length){
							datepicker.datepicker('hide');
						}
					});
				}
				change('adult', 1);
			} else {
				window.noJsonHandler = true;
			}
		});
	};

	selectCabin();

	var selectCabinHotel = function(){
		var widgetHotel = $('[data-widget-hotel]');
		widgetHotel.each(function(){
			var formHotel = $(this),
					selAdult = $('[data-adult] select', formHotel),
					selChild = $('[data-child] select', formHotel),
					selRoom = $('[data-room] select', formHotel),
					maxAdult = 36;
					maxChild = 36;
					maxRoom = 9;
			var buildSel = function(length, sel, isAdult, valRoom){
				var num = isAdult ? 1 : 0,
					hasSelected = false,
					options = [],
					val = parseInt(sel.val());
				if(valRoom) {
					num = valRoom;
				}
				sel.empty();
				for(var i = num; i <= length; i++){
					var isSelected = '';
					var option = null;
					if(val === i){
						isSelected = 'selected';
						hasSelected = true;
					}
					option = '<option value="' + i + '"' + isSelected + '>' + i + '</option>';
					options.push(option);
				}
				if(!options.length) {
					options = ['<option value="0" selected>0</option>'];
				}
				sel.html(options.join(''));
				if(!hasSelected){
					sel.prop('selectedIndex', 0);
					val = num;
				}
				sel.defaultSelect('refresh');

				//sel.closest('[data-customSelect]').trigger('afterSelect');
			};

			var change = function(type, value){
				switch(type){
					case 'room':
						if(parseInt(value) === 1) {
							buildSel(maxRoom, selRoom, true);
							buildSel(maxAdult, selAdult, true);
							buildSel(maxChild, selChild, false);
						} else {
							buildSel(maxAdult, selAdult, true, parseInt(value));
						}
						break;

					default:
						break;
				}

			};

			selRoom.off('change.changeCabin').on('change.changeCabin', function(){
				change('room', selRoom.val());
			});

			change('room', 1);

		});
	};
	if($('[data-widget-hotel="true"]')) {
		selectCabinHotel();
	}
};
