/**
 * @name SIA
 * @description Define global youtubeApp functions
 * @version 1.0
 */
SIA.youtubeApp = function() {
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var win = global.vars.win;
	var flyingFocus = $();
	var dataVideo = globalJson.dataVideo;

	// if(dataVideo){
	// 	dataVideo = {
	// 		url: ['NUOcs1nZ35s', 'asPKzZ7X79w'],
	// 		image: [{
	// 			url: 'images/img-1.jpg',
	// 			description: 'Lorem ipsum dolor sit dipiscing elit.'
	// 		},{
	// 			url:'images/img-2.jpg',
	// 			description: 'Curabitur congue tortor vitae era.'
	// 		}]
	// 	};
	// }

	// init youtube function
	var initYoutube = function() {
		var player;
		var url = dataVideo.url[0];

		// apply youtube video
		var applyYoutube = function(temp) {
			var videoCollect = $('[data-youtube-url]');

			var videoLightbox = $(temp).appendTo(body);
			var thumbnail = videoLightbox.find('.info-watch');

			// close youtube video
			var closeYoutubeVideo = function(videoBlock, callback) {
				videoBlock.html('<div id="youtube-player"></div>');
				if (callback) {
					callback();
				}
			};

			// init video popup
			videoLightbox.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close], .cancel',
				afterShow: function() { // after showing popup
					flyingFocus = $('#flying-focus');
					if (flyingFocus.length) {
						flyingFocus.remove();
					}
				},
				beforeHide: function() { // before hiding popup
					player.stopVideo();
				},
				afterHide: function(popup) { // after hiding popup
					player = null;
					closeYoutubeVideo($(popup).find('.wrap-video'));

					win.trigger('resize.openMenuT');
					$('#travel-widget').tabMenu('onResize');
				}
			});

			// init youtube api plugin or load video url if already inited
			thumbnail.each(function(idx) {
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e) {
					e.preventDefault();
					url = dataVideo.url[idx];
					if (!player) {
						window.onYouTubeIframeAPIReady();
					} else {
						player.loadVideoById(url);
					}
				});
			});

			// show popup video
			videoCollect.each(function() {
				var self = $(this);
				self.off('click.showVideo').on('click.showVideo', function(e) {
					e.preventDefault();
					url = dataVideo.url[self.data('youtube-url')];
					if (!player) {
						window.onYouTubeIframeAPIReady();
					}
					// else{
					// 	player.loadVideoById(url);
					// }
					videoLightbox.Popup('show');
				});
			});

			// init youtube plugin when api is ready
			window.onYouTubeIframeAPIReady = function() {
				// var done = false;
				player = new window.YT.Player('youtube-player', {
					height: 390,
					width: 640,
					videoId: url,
					// playerVars: {
					//     listType:'playlist',
					//     list: 'UUPW9TMt0le6orPKdDwLR93w'
					//   },
					events: {
						// 'onReady': onPlayerReady,
						// 'onStateChange': onPlayerStateChange
						// 'onPlaybackQualityChange': onPlayerPlaybackQualityChange,
						// 'onError': onPlayerError
					}
				});
			};
		};

		// draw template
		$.get(config.url.videoLightbox.youtube, function(temp) {
			var template = window._.template(temp, {
				'data': dataVideo
			});
			applyYoutube(template);
		}, 'html');

	};

	// init module function
	var initModule = function() {
		initYoutube();
	};

	initModule();
};
