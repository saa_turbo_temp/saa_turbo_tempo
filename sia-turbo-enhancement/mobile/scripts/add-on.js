/**
 * @name SIA
 * @description Define global addon functions
 * @version 1.0
 */

SIA.addon = function(){
	var global = SIA.global,
		config = global.config,
		win = $(window),
		htmlBody = $('html,body'),
		isNewAddonPage = htmlBody.is('.add-ons-1-landing-page'),
		sortName = 'price-desc',
		arrSupplierChecked = [],
		arrLocalChecked = [],
		arrFuelChecked = [],
		arrAutomaticChecked = [],
		AirChecked,
		DoorChecked,
		partySize,
		sortBy,
		arrCarSizeChecked = null,
		initCarData = [],
		currentCarData = [],
		pickupDateObj,
		dropoffDateObj,
		hotelData = [];


	var bookingSummary = $('.booking-summary');
	var trigger = bookingSummary.find('.booking-summary__heading .booking-summary__control');
	// var bookingSummaryContent = bookingSummary.find('.booking-summary__content');

	// var btnAdd = $('#insurance-1');
	var addBookingSummary = $('[data-add-booking-summary]');
	// var acceptIn = $('#checkbox-insurance');

	var baggage = $('.table-baggage');
	var insuranceLeave = $('[data-insurance-leave]');

	var allTriggerAddon = addBookingSummary.add(baggage.find('tr .baggage-1 :checkbox'));

	var detectAdded = function(){
		var isAdded = false;
		allTriggerAddon.each(function(){
			var that = $(this);
			if(that.data('added')){
				isAdded = true;
				return !isAdded;
			}
		});
		return isAdded;
	};

	// var checlAcceptInStatus = function(){
	// 	if(acceptIn.is(':checked')){
	// 		btnAdd.prop('disabled', false).removeClass('disabled');
	// 	}
	// 	else{
	// 		btnAdd.prop('disabled', true).addClass('disabled');
	// 	}
	// };

	// enable and diable oversive
	baggage.find('tr').each(function(){
		var self = $(this);
		var cb = self.find('.baggage-1 :checkbox');
		var os = self.find('.baggage-3 :checkbox');
		// var os = self.find('[data-block-baggage] :checkbox');
		var addOversize = function(){
			if(cb.is(':checked')){
				os.prop('disabled', false);
				os.closest('.custom-checkbox').removeClass('disabled');
				os.closest('.custom-checkbox').siblings('[data-tooltip]').removeClass('disabled');
				if(!bookingSummary.hasClass('active')){
					trigger.trigger('click.openBS');
				}
			}
			else{
				os.prop({
					'disabled': true,
					'checked': false
				});
				os.closest('.custom-checkbox').addClass('disabled');
				os.closest('.custom-checkbox').siblings('[data-tooltip]').addClass('disabled');
			}
			if(!cb.data('added')){
				cb.data('added', true);
			}
			else{
				cb.data('added', false);
			}
		};
		addOversize();
		cb.data('added', false);

		cb.off('change.addOversize').on('change.addOversize', addOversize);
	});

	// Confirm continue without add on page.
	insuranceLeave.off('click.leave').on('click.leave', function(e){
		e.preventDefault();

		var self = $(this);
		var popupInsuranceLeave = $('.popup--insurance-leave');

		if(detectAdded()){
			if(!popupInsuranceLeave.data('Popup')) {
				popupInsuranceLeave.Popup({
					overlayBGTemplate: SIA.global.config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]'
				});
			}
		}else{
			window.location.href = self.attr('href');
		}

		$('body').off('click.leave-page').on('click.leave-page', '[data-insurance-leave]', function(e) {
			e.preventDefault();
			var btn = $(this);
			var href = btn.attr('href');
			var target = btn.attr('target');

			popupInsuranceLeave
				.find('[data-confirm]')
				.off('click.unload')
				.on('click.unload', function() {
					window.onbeforeunload = null;
					if(href && href.indexOf('#') === 0) {
						if($(href).length) {
							$('html, body').scrollTop($(href).offset().top);
						}
					}
					else {
						if (target && target === '_blank') {
							window.open(href, target);
						}
						else {
							window.location.href = href;
						}
					}
					popupInsuranceLeave.Popup('hide');
				});

			if(detectAdded()){
				popupInsuranceLeave.Popup('show');
			}
		});
	});

	// booking nav
	// trigger.off('click.openBS').on('click.openBS', function(e){
	// 	e.preventDefault();
	// 	if(bookingSummary.hasClass('active')){
	// 		bookingSummaryContent.css('display', 'none');
	// 		bookingSummary.removeClass('active');
	// 	}
	// 	else{
	// 		bookingSummaryContent.css('display', 'block');
	// 		bookingSummary.addClass('active');
	// 	}
	// });

	// checlAcceptInStatus();
	// acceptIn.off('change.acceptIn').on('change.acceptIn', function(){
	// 	checlAcceptInStatus();
	// });

	// addBookingSummary.off('click.add').on('click.add', function(e){
	// 	e.preventDefault();
	// 	if(!bookingSummary.hasClass('active')){
	// 		trigger.trigger('click.openBS');
	// 	}
	// });

	// 	Trigger click on link has data-toggle-value
	var synchronizeClone = function(el){
		var originEl = el.closest('.slide-item');
		if(originEl.length){
			$(originEl.data('slick-cloned')).find('a[data-toggle-value]').trigger('click.toggleLabel');
		}
	};

	addBookingSummary.off('click.add').on('click.add', function(e){
		e.preventDefault();

		var addedData = false,
			addOnAdded = $(this).closest('.addon-item').find('.addon-added'),
			accordionTrigger = $(this).closest('.addon-item').find('[data-accordion-trigger="1"]');

		addOnAdded.addClass('hidden');
		if($(this).next().val() === '' || $(this).next().val() === 'false'){
			addedData = true;
			if(addOnAdded.is('.hidden')) {
				addOnAdded.removeClass('hidden');
			}
		}
		$(this).data('added', addedData);
		synchronizeClone($(this));
		if(accordionTrigger.is('.active')) {
			accordionTrigger.trigger('click.accordion');
			htmlBody.animate({ scrollTop: $(this).closest('.addon-item').offset().top}, 'slow');
		}

	});

	// This function use for init slick slide.
	var initSlickSlide = function(isNewAddon){
		isNewAddon = isNewAddon === undefined ? false : isNewAddon;

		var flexSlider = $('.flexslider');
		// var slides = flexSlider.find('.slides');
		// var setAccordition;
		// var wrapperHLS = flexSlider.parent();

		// var loadBackgroundHighlight = function(self, parentSelt, idx, totalImage, fl){
		var init = function(fl){
			var next = fl.parent().siblings('.ico-point-r'),
				prev = fl.parent().siblings('.ico-point-l'),
				newNext = fl.children('.slick-next'),
				newPrev = fl.children('.slick-prev'),
				gl = fl.find('.slides'),
				hotelBlock = fl.closest('.hotel-block'),
				options = {},
				defaultOptions = {},
				customOptions = hotelBlock.data('slider-option') ? $.parseJSON(hotelBlock.data('slider-option').replace(/\'/gi, '"')) : {};

			defaultOptions = {
				dots: false,
				speed: 300,
				draggable: true,
				prevArrow: '',
				nextArrow: '',
				slidesToShow: 2,
				slidesToScroll: 2,
				accessibility: false,
				onAfterChange: function(){
					gl.css('visibility', 'visible');
				}
			};
			options = $.extend({}, defaultOptions, customOptions);

			// fl.width(wrapperHLS.width());
			fl.css('visibility', 'visible');
			if(!gl.is('.slick-initialized')) {
				gl.slick(options);
			}
			if(!isNewAddon) {

				next.off('click.next').on('click.next', function(e){
					e.preventDefault();
					gl.slick('slickNext');
				});
				prev.off('click.prev').on('click.prev', function(e){
					e.preventDefault();
					gl.slick('slickPrev');
				});

			} else {
				newNext.off('click.next').on('click.next', function(e){
					e.preventDefault();
					e.stopPropagation();
					gl.slick('slickNext');
				});
				newPrev.off('click.prev').on('click.prev', function(e){
					e.preventDefault();
					e.stopPropagation();
					gl.slick('slickPrev');
				});
			}


			// gl.find('a.btn-1').off('click.add').on('click.add', function(e){
			// 	e.preventDefault();
			// 	if(!bookingSummary.hasClass('active')){
			// 		trigger.trigger('click.openBS');
			// 	}
			// });
			// if(global.vars.detectDevice.isTablet()){
			// 	parentSelt.css({
			// 		'background-image': 'url(' + self.attr('src') + ')'
			// 	});
			// 	self.attr('src', config.imgSrc.transparent);
			// }
			// if(idx === totalImage - 1){
			// 	var next = fl.parent().siblings('.ico-point-r');
			// 	var prev = fl.parent().siblings('.ico-point-l');
			// 	var gl = fl.find('.slides');
			// 	fl.width(wrapperHLS.width());
			// 	fl.css('visibility', 'visible');
			// 	gl.slick({
			// 		dots: false,
			// 		speed: 300,
			// 		draggable: true,
			// 		prevArrow: '',
			// 		nextArrow: '',
			// 		slidesToShow: 2,
			// 		slidesToScroll: 2,
			// 		accessibility: false,
			// 		useCSS: global.vars.isNewIE() ? false : true
			// 	});

			// 	next.off('click.next').on('click.next', function(e){
			// 		e.preventDefault();
			// 		gl.slickNext();
			// 	});
			// 	prev.off('click.prev').on('click.prev', function(e){
			// 		e.preventDefault();
			// 		gl.slickPrev();
			// 	});
			// 	win.off('resize.flexSlider').on('resize.flexSlider',function() {
			// 		flexSlider.width(wrapperHLS.width());
			// 	}).trigger('resize.flexSlider');

			// 	setAccordition = setInterval(function(){
			// 		if(slides.hasClass('slick-initialized')){
			// 			initAccordion();
			// 			$('.slick-dots').css('margin-left', 0);
			// 			clearInterval(setAccordition);
			// 		}
			// 	}, 2000);
			// }
			$(fl).closest('.accordion__content').width($('.content-wrapper').width() - 20);
		};

		flexSlider.each(function(){
			var fl = $(this);
			// var totalImage = fl.find('img');
			init(fl);
			// totalImage.each(function(idx) {
			// 	var self = $(this);
			// 	var parentSelt = self.parent();
			// 	var nI = new Image();
			// 	nI.onload = function(){
			// 		loadBackgroundHighlight(self, parentSelt, idx, totalImage.length, fl);
			// 	};
			// 	nI.src = self.attr('src');
			// });
		});

		var originalWidth = $(window).width();
		$(window).on('resize.set-slider-width', function() {
			if(originalWidth !== $(this).width()) {
				originalWidth = $(this).width();
				flexSlider.each(function(f, fl) {
					$(fl).closest('.accordion__content').width($('.content-wrapper').width() - 20);
				});
			}
		});
	};

	initSlickSlide();

	// Toggle value of hidden input.
	var createAddOn = function() {
		$('body')
		.off('click.addOn')
		.on('click.addOn', '[data-add-booking-summary]', function(e) {
			e.preventDefault();
			var name = $(this).data('add-booking-summary');
			var toggleButton = $(this).data('toggle-value');
			var hiddenInput = $('input[name="' + name + '"]');
			if(typeof(toggleButton) !== 'undefined') {
				if(hiddenInput.val() === 'true') {
					hiddenInput.val('false');
				}
				else {
					hiddenInput.val('true');
				}
			}
			else {
				hiddenInput.val('true');
			}
			// var addedData = false;
			// if($(this).next().val() === '' || $(this).next().val() === 'false'){
			// 	addedData = true;
			// }
			$(this).data('added', hiddenInput.val());
			synchronizeClone($(this));
		});
		addBookingSummary.off('afterChangeName.btn').on('afterChangeName.btn', function(){
			setTimeout(function(){
				if(!bookingSummary.hasClass('active')){
					trigger.trigger('click.openBS');
				}
			}, 200);
		});
	};

	createAddOn();

	// render hotel addon
	var getHotelData = function() {
		$.ajax({
			url: config.url.addons.hotel.json,
			dataType: 'json',
			type: global.config.ajaxMethod,
			beforeSend: function() {},
			success: function(data) {
				if (data) {
					hotelData = data[0].responseBody.hotels;
					getHotelTemplate(data[0].responseBody);
				}
			},
			error: function(xhr, status) {
				if (status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	};

	var getHotelTemplate = function(data) {
		$.get(config.url.addons.hotel.template, function(tpl) {
			renderAllHotelList(tpl, data.hotels);
			hotelTemplateView(tpl, data);
			formSearchValidation();
		});
	};

	var renderAllHotelList = function(html, hotels) {
		var hotelBlock = $('.hotel-block'),
			listHotel = hotelBlock.find('.hotel-list'),
			seeMoreBtn = hotelBlock.find('.see-more-btn');

		var template = window._.template(html, { data: hotels });

		$(template).insertBefore(seeMoreBtn);

		initSlickSlide(true);
		if(SIA.accordion) {
			SIA.accordion();
		}

		if(SIA.initTabMenu) {
			SIA.initTabMenu();
		}
		if(SIA.multiTabsWithLongText) {
			SIA.multiTabsWithLongText();
		}

		if(SIA.plusOrMinusNumber) {
			SIA.plusOrMinusNumber();
		}
		$('.hotel-list [data-tooltip]').kTooltip();
		if(hotelBlock.is('[data-baidu-map]')) {
			SIA.baiduMap(true);
		} else {
			SIA.googleMap(true);
		}

		//accesibility for hotel listing
		$('.hotel-list').each(function(i, e) {
			var _self = $(e),
				hotelControl = _self.find('.head-hotel.accordion__control'),
				hotelId = _self.data('hotel-id'),
				btnPoliClose = _self.find('[data-trigger-poli-close]'),
				btnAmenClose = _self.find('[data-trigger-amen-close]'),
				btnMapClose = _self.find('[data-trigger-map-close]'),
				parentAccordionControll = _self.parent().parent().siblings('[data-accordion-trigger="1"]'),
				triggerArrow = _self.find('[data-trigger-arrow]'),
				roomTable = _self.find('table[data-room-table]'),
				btnAddRoom = _self.find('[data-add-room]'),
				btnRemoveRoom = _self.find('[data-remove-room]'),
				addOnItem = _self.closest('.addon-item'),
				addOnDescription = addOnItem.find('.description'),
				addOnAddedFlag = addOnItem.find('.addon-added');



			function changeAccState(e) {
				e.preventDefault();
				hotelControl.trigger('click.accordion');
				htmlBody.animate({ scrollTop: _self.offset().top}, 'slow');
			}

			btnPoliClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});
			btnAmenClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});
			btnMapClose.off('click.closeAccordion').on('click.closeAccordion', function(e){
				changeAccState(e);
			});

			_self.off('click.accordion keyup.accordion').on('click.accordion keyup.accordion', function(e){
				e.preventDefault();

				if((e.keyCode !== 13 && e.keyCode !== undefined) || !$(e.target).is('.hotel-list') ) {
					return;
				}

				hotelControl.trigger('click.accordion');

			});

			triggerArrow.off('click.accordion').on('click.accordion', function(e){
				e.preventDefault();
				hotelControl.trigger('click.accordion');
			});

			//add room
			btnAddRoom.off('click.addroom').on('click.addroom', function(e){
				var _self = $(e),
						listLinkEl = $('<ul class="list-link">' +
												'<li><a href="#" class="trigger-accordion-added"><em class="ico-point-r--addon"></em>Edit add-on</a>'+
												'</li><li><a href="#" class="trigger-accordion-remove"><em class="ico-point-r--addon"></em>Cancel</a>' +
												'</li></ul>'),
						roomData,
						selfAddBtn = $(this),
						surcharges,
						totalEl;

				//closeAccordion
				// parentAccordionControll.trigger('click.accordion');
				//show/hide added hotel
				addOnItem.addClass('hotel-added-wrapper');
				addOnDescription.addClass('hidden');
				addOnAddedFlag.removeClass('hidden');
				addOnItem.find('[data-trigger-ico]').addClass('hidden');

				if(parentAccordionControll.find('.list-link').length === 0) {
					parentAccordionControll.append(listLinkEl);
				}

				roomData = $.grep(hotelData, function(hotel){
					return parseInt(hotel.hotelID) === selfAddBtn.data('selfhotelid')
				});

				roomData = $.grep(roomData[0].rooms, function(room){
					return parseInt(room.roomID) === selfAddBtn.data('roomid')
				});

				roomData = roomData[0];

				totalEl = '<span class="title-6--grey">Total price</span><span class="include-price">'+ roomData.currency + ' ' + roomData.rate.totalRate +'</span>';

				if(roomData.surcharges && roomData.surcharges.length > 0) {
					totalEl += '<p class="text">'
					$.grep(roomData.surcharges, function(surcharge){
						if(surcharge.surchargeType === 'Mandatory') {
							totalEl += '<span>Included in price: ' + (parseInt(surcharge.surchargeTax) > 0 ? 'Hotel tax ' + surcharge.surchargeTax + '%, ' : '') + surcharge.surchargeName + ' ' + roomData.currency + ' ' + surcharge.surchargeTotalAmount + '</span>';
						} else if(surcharge.surchargeType === 'Excluded') {
							totalEl += '<span>Not included in price (collected by property): ' + surcharge.surchargeName + ' ' + roomData.currency + ' ' + surcharge.surchargeTotalAmount + '</span>';
						}
					});
				}

				totalEl += '</p><p>By adding this room, you agree to the<a href="#">&nbsp;terms and conditions&nbsp;</a>of your hotel booking.</p>'

				renderAddOnAddedSales(hotelId, addOnItem, roomData);

				//show/hide other btn
				// addOnItem.find('[data-add-room]').removeClass('hidden');
				// addOnItem.find('[data-remove-room]').addClass('hidden');

				// show/hide btn
				$('[data-add-room]').removeClass('hidden');
				$(this).addClass('hidden');
				$('[data-remove-room]').addClass('hidden');
				$(this).parent().find('[data-remove-room]').removeClass('hidden');

				var roomDetails = $(this).closest('.hotel-room--details');

				$('.hotel-room--details').each(function(){
					$(this).find('.terms-conditions').each(function(){
						if($(this).is('.added-room')) {
							$(this).empty();
							$(this).addClass('hidden');
						} else {
							$(this).removeClass('hidden');
						}
					});
				});

				roomDetails.find('.terms-conditions').each(function(){
					if($(this).is('.added-room')) {
						$(this).empty();
						$(this).append(totalEl);
						$(this).removeClass('hidden');
					} else {
						$(this).addClass('hidden');
					}
				});

				var totalPrice = roomDetails.find('.terms-conditions.added-room');

				setTimeout(function(){
					htmlBody.animate({ scrollTop: totalPrice.offset().top}, 'slow');
				}, 500);

				parentAccordionControll.children('.ico-point-r').addClass('hidden');
				parentAccordionControll.find('.list-link').removeClass('hidden');



				//scroll top

				var editAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-added'),
						cancelAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-remove');

				//edit add-on
				editAddonTriger.off('click.editAddon').on('click.editAddon', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(!parentAccordionControll.is('.active')){
						parentAccordionControll.trigger('click.accordion');
					}
					htmlBody.animate({ scrollTop: hotelControl.offset().top}, 'slow');

				});

				//cancel add-on
				cancelAddonTriger.off('click.cancelAddon').on('click.cancelAddon', function(e){
					e.preventDefault();
					e.stopPropagation();

					addOnItem.removeClass('hotel-added-wrapper');
					addOnItem.find('.add-ons-item-added').remove();
					addOnDescription.removeClass('hidden');
					addOnAddedFlag.addClass('hidden');
					addOnItem.find('[data-trigger-ico]').removeClass('hidden');
					parentAccordionControll.find('.list-link').addClass('hidden');

					// show/hide add/remove btn
					btnAddRoom.removeClass('hidden');
					btnRemoveRoom.addClass('hidden');
					parentAccordionControll.children('.ico-point-r').removeClass('hidden');
					htmlBody.animate({ scrollTop: addOnItem.offset().top}, 'slow');
					if(parentAccordionControll.is('.active')){
						parentAccordionControll.trigger('click.accordion');
					}

				});
			});

			btnRemoveRoom.off('click.removeroom').on('click.removeroom', function(e){
				var _self =$(e);
				var roomDetails = $(this).closest('.hotel-room--details');

				addOnItem.removeClass('hotel-added-wrapper');
				addOnItem.find('.add-ons-item-added').remove();
				addOnDescription.removeClass('hidden');
				addOnAddedFlag.addClass('hidden');
				addOnItem.find('[data-trigger-ico]').removeClass('hidden');
				parentAccordionControll.find('.list-link').addClass('hidden');

				// show/hide add/remove btn
				btnAddRoom.removeClass('hidden');
				btnRemoveRoom.addClass('hidden');
				parentAccordionControll.children('.ico-point-r').removeClass('hidden');

				roomDetails.find('.terms-conditions').each(function(){
					if(!$(this).is('.added-room')) {
						$(this).removeClass('hidden');
					} else {
						$(this).empty();
						$(this).addClass('hidden');
					}
				});
				// htmlBody.animate({ scrollTop: addOnItem.offset().top}, 'slow');
				// if(parentAccordionControll.is('.active')){
				// 		parentAccordionControll.trigger('click.accordion');
				// 	}


			});

		});
	}


	var hotelTemplateView = function(html, data) {
		var hotelBlock = $('.hotel-block'),
			hotelItems = hotelBlock.find('.hotel-list'),
			slider = $('#slider-range'),
			labelFrom = slider.find('.ui-slider_from'),
			labelTo = slider.find('.ui-slider_to'),
			inputFrom = slider.find('[name="from-price"]'),
			inputTo = slider.find('[name="to-price"]'),
			initValue = [slider.data('current-min'), slider.data('current-max')],
			seeMoreBtn = hotelBlock.find('.see-more-btn'),
			location = $('#location'),
			customLocation = location.closest('[data-customselect]'),
			sort = $('#custom-select--sort-1'),
			customSort = sort.closest('[data-customselect]'),
			firstView = hotelBlock.data('hotel-number-per-page') || 5,
			nextView = seeMoreBtn.data('see-more') || 5,
			filteredData = [],
			newData = [],
			holderUI = [],
			ratingBlock = $('.rating-block'),
			rate = ratingBlock.children(),
			startRating = ratingBlock.data('start-rate'),
			slideFrom = initValue[0],
			slideTo = initValue[1],
			filteredItems,
			sortDropdown = $('#custom-select--sort-1'),
			formSearchWrapper = hotelBlock.siblings('[data-search-hotel]')
			searchBtn = formSearchWrapper.find('#search-hotel-btn'),
			checkinDay = $('[data-search-hotel]').find('[data-start-date]'),
			checkoutDay = $('[data-search-hotel]').find('[data-return-date]');

		//start set default today for check-in and today + 3 for checkout
		var today = new Date(),
			dd = today.getDate(),
			ddCheckout = dd + 3,
			mm = today.getMonth()+1,
			yyyy = today.getFullYear(),
			ckDay = "";

		dd = dd < 10 ? '0' + dd : dd;
		mm = mm < 10 ? '0' + mm : mm;
		ddCheckout = ddCheckout < 10 ? '0' + ddCheckout : ddCheckout;

		today = dd+"/"+mm+"/"+yyyy;
		ckDay = ddCheckout +"/"+mm+"/"+yyyy;

		checkinDay.val(today);
		checkoutDay.val(ckDay);
		//end set default day

		//range slider
		slideFrom = data.minHotelSliderVal ? parseInt(data.minHotelSliderVal) : initValue[0];
		slideTo = data.maxHotelSliderVal ? parseInt(data.maxHotelSliderVal) : initValue[1];

		slider.slider({
			range: true,
			min: slideFrom,
			max: slideTo,
			values: initValue,
			create: function() {
				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
			},
			slide: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
				inputFrom.val(slideFrom);
				inputTo.val(slideTo);
			},
			stop: function(event, ui) {
				slideFrom = ui.values[0];
				slideTo = ui.values[1];
				holderUI = [slideFrom, slideTo];
				filterHotelByPrice();
				// filterHotelByRating();
				// filterHotelByLocation(locationSelect.val());
			}
		});

		var starRating = function() {
			var rate = $('[data-start-rate]').children();
			if (!startRating) {
				startRating = ratingBlock.data('start-rate');
			}
			var chooseRate = function() {
				rate.removeClass('rated');
				rate.each(function(i) {
					var self = $(this);
					if (i < startRating) {
						self.addClass('rated');
					}
				});
			};
			var clearStar = function(){
        rate.removeClass('rated');
      };
			rate.each(function(i) {
				$(this).off('click.rate').on('click.rate', function(e) {
					e.preventDefault();
					startRating = i + 1;
						chooseRate();
					filterHotelByPrice();
				});
			});
			$('[data-clear]').on('click',function(e){
        e.preventDefault();
        clearStar();
        filterHotelByPrice();
      });
      $('[data-clear]').on('keydown', function(e){
        switch(e.keyCode){
          case 13:
            e.preventDefault();
            clearStar();
            filterHotelByPrice();
          break;
        }
      });
			chooseRate();
		};



		var handleSeeMoreBtn = function(){
			if(filteredItems){

				if(filteredItems.length > nextView){
					seeMoreBtn.removeClass('hidden');
					filteredItems.addClass('hidden');
					filteredItems.slice(0, nextView).removeClass('hidden');
				} else {
					seeMoreBtn.addClass('hidden');
				}

				seeMoreBtn.on('click.sshHotel', function(e){
					e.preventDefault();
					var hiddenItems = filteredItems.filter('.hidden');
					var numberOfHiddenItems = hiddenItems.length;
					hiddenItems.slice(0,(numberOfHiddenItems > nextView) ? nextView : numberOfHiddenItems).removeClass('hidden');
					hiddenItems = filteredItems.filter('.hidden');
					if(hiddenItems.length === 0){
						seeMoreBtn.addClass('hidden');
					}
				});
			}
		};

		var filterHotelByPrice = function() {
			hotelItems = hotelBlock.find('.hotel-list');
			var minPrice = parseInt(labelFrom.text().replace(',', ''));
			var maxPrice = parseInt(labelTo.text().replace(',', ''));
			var hotelItemFiltered = hotelItems.filter(function() {
				var val = parseInt($(this).data('price').toString().replace(',', ''));
				return val >= minPrice && val <= maxPrice;
			});
			if(hotelItemFiltered.length > 0){
				$('.hotel-list').removeClass('hidden');
			}
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			filterHotelByRating();
		};

		var filterHotelByRating = function(isSearch) {
			var sliderV = $('#slider-range').slider('option', 'values');
			hotelItems = hotelBlock.find('.hotel-list').not('.hidden');
			var rateFilter = $('[data-start-rate]').children().filter('.rated').length;
			var hotelItemFiltered = hotelItems.filter(function() {
				var rate = $(this).data('rate');
				return rate >= rateFilter;
			});
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			filteredItems = hotelItemFiltered;
			if(isSearch == undefined) {
				$('#aria-add-on-filter').text('');
				$('#aria-add-on-filter').text(L10n.wcag.sliderLabel.format(sliderV[0], sliderV[1])+ ', '+ L10n.wcag.foundLabel.format(filteredItems.length));
			}
			handleSeeMoreBtn();

			if(sortName === 'price-desc') {
				sortData('price', true);
			} else if(sortName === 'price-asc') {
				sortData('price');
			} else if(sortName === 'rate-desc') {
				sortData('rate', true);
			} else if(sortName === 'price-asc') {
				sortData('rate');
			}

		};

		sortDropdown.off('change.sortData').on('change.sortData', function(e){

			var value = $(this).val(),
					attrName;

			e.preventDefault();
			if(value === 'price-desc') {
				attrName = 'price';
				sortName = 'price-desc';
				sortData(attrName, true);
			} else if(value === 'price-asc') {
				attrName = 'price';
				sortName = 'price-asc';
				sortData(attrName);
			} else if (value === 'star-rating-asc') {
				attrName = 'rate';
				sortName = 'rate-asc';
				sortData(attrName);
			} else if (value === 'star-rating-desc') {
				attrName = 'rate';
				sortName = 'rate-desc';
				sortData(attrName, true);
			}

		});

		function sortData(attrName, isDesc) {
			if(!filteredItems){
				return;
			}

			isDesc = isDesc || false;
			var currentLength = filteredItems.not('.hidden').length;
			filteredItems.removeClass('hidden');
			var elArr = [];
			var dataVals = getValues.call(filteredItems, attrName);
			var dataValsSorted = dataVals.sort(function(a, b) {
				return a - b;
			});
			var len = dataValsSorted.length;
			filteredItems.detach();

			for (var i = 0; i < len; i++) {
				for (var j = 0; j < len; j++) {
					var item = filteredItems.eq(j).not('.sorted');
					if(parseInt(item.length > 0 && item.attr('data-' + attrName).replace(',', '')) === dataValsSorted[i]){
						item.addClass('sorted')
								.addClass('hidden');
						elArr.push(item);
					}
				}
			}

			var list = isDesc ? elArr.reverse() : elArr;
			for (var i2 = 0; i2 < len; i2 ++) {
				if (i2 < 2) {
					$(list[i2]).removeClass('hidden');
				}
			}

			hotelBlock.prepend(list);
			// filteredItems = hotelBlock.find('.hotel-list');
			$('.sorted').removeClass('sorted');
			// list.addClass('hidden');
			// list.slice(0,currentLength).removeClass('hidden');
			// filteredItems.addClass('hidden');
			// filteredItems.slice(0,currentLength).removeClass('hidden');
			// if(filteredItems.slice(0,currentLength).length === 0){
			// 	$('.hotel-list').addClass('hidden');
			// }else{
			// 	$('.hotel-list').removeClass('hidden');
			// }
			handleSeeMoreBtn();
		}

		var getValues = function(attrName) {
			var len = this.length;
			var arr = [];
			for (var i = 0; i < len; i++) {
				arr.push(parseInt(this.eq(i).attr('data-' + attrName).replace(',', '')));
			}
			return arr;
		};

		starRating();
		filterHotelByPrice();

		searchBtn.off('click.searchHotel').on('click.searchHotel', function(){
			var checkin = formSearchWrapper.find('[data-checkin-date]').val(),
				checkout = formSearchWrapper.find('[data-checkout-date]').val(),
				rooms = formSearchWrapper.find('[data-room] select').val(),
				adults = formSearchWrapper.find('[data-adult] select').val(),
				children = formSearchWrapper.find('[data-child] select').val(),
				hotelItemFiltered;

			hotelItems = hotelBlock.find('.hotel-list');
			hotelItemFiltered = hotelItems.filter(function() {
				var _self = $(this),
						checkinData = _self.data('hotel-checkinday'),
						checkoutData = _self.data('hotel-checkoutday'),
						allRoomsData = _self.data('hotel-allrooms'),
						roomsData = parseInt(_self.data('hotel-rooms')),
						adultsData = parseInt(_self.data('hotel-adults')),
						childrenData = parseInt(_self.data('hotel-children'));

				return (rooms === 0 ? true : rooms <= roomsData)
							&& (adults === 0 ? true : adults <= adultsData)
							&& (children === 0 ? true : children <= childrenData);
			});

			if(hotelItemFiltered.length > 0){
				$('.hotel-list').removeClass('hidden');
			}
			hotelItems.addClass('hidden');
			hotelItemFiltered.removeClass('hidden');
			starRating();
			filterHotelByPrice();

		});

	};

	if(isNewAddonPage) {
		getHotelData();
	}

	var renderAddOnAddedSales = function(hId, addOnItem, roomData){
		var fillContent = function(res) {
			var listAddOnAddeds = addOnItem,
			addOnItemAdded = listAddOnAddeds.find('.add-ons-item-added'),
			checkinDate = $.datepicker.formatDate( "yy/mm/dd", new Date($('#check-in-day').val())),
			checkoutDate = $.datepicker.formatDate( "yy/mm/dd", new Date($('#check-out-day').val())),
			roomType = roomData.roomName,
			adult = parseInt($('#cabin-2').val()),
			child = parseInt($('#cabin-3').val()),
			guest = adult ? (adult > 1 ? adult + 'Adults' : adult + ' Adult') : '' + child ? (child > 1 ? adult + 'Childrens' : adult + ' Children') : '';

			if(listAddOnAddeds.length){
				$.get(global.config.url.addOnAddedTemplateSales, function (data) {
					var template = window._.template(data, {
						agodaBooking: res,
						checkinDate: checkinDate,
						checkoutDate: checkoutDate,
						roomType: roomType,
						guest: guest
					});
					//remove old added item
					if(addOnItemAdded.length) {
						addOnItemAdded.remove();
					}
					//add new added item
					$(template).prependTo(listAddOnAddeds);
				}, 'html');
				//render event
			}
		};
		_.forEach(globalJson.dataAddOn.agodaBookingList, function(value, i){
			if(parseInt(value.hotelID) === hId) {
				fillContent(value);
			}
		});
	};

  var formSearchValidation = function() {
		var formSearchHotel = $('[data-search-hotel]');
		formSearchHotel.each(function(i, e) {
			var _form = $(this);
			_form.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess
			});
		});
	};

	var formSearchCarValidation = function(){
		$('#form-search-car').validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	}

	formSearchCarValidation();

	var getCarData = function(url) {
		$.ajax({
			url: url,
			dataType: 'json',
			type: global.config.ajaxMethod,
			beforeSend: function() {},
			success: function(data) {
				if (data) {
					renderCheckbox(data.response.details.vehicles);
					getCarTemplate(data.response.details);
				}
			},
			error: function(xhr, status) {
				if (status !== 'abort') {
					window.alert(L10n.flightSelect.errorGettingData);
				}
			}
		});
	}

	var renderListCar = function(filteredData) {
		var templateCarUrl = config.url.addons.car.templateCar,
				listCar = $('.addons-landing-block .accordion__content--car .list-cars-result')[0];

		currentCarData = filteredData;

		$(listCar).empty();

		$.get(templateCarUrl, function (data) {
			if(filteredData.length > 0) {
				$('.accordion__content--car .addon-no-result').addClass('hidden');
				var template = window._.template(data, {
					data: filteredData
				});
				$(template).appendTo($(listCar));
			} else {
				$('.accordion__content--car .addon-no-result').removeClass('hidden');
			}
		$('.cancellation [data-tooltip]').kTooltip();
		}, 'html');
	}

	var renderSliderCar = function(dataCarSize, filterData) {
		var appendElement = $('.addons-landing-block .accordion__content--car #car-avaliable'),
				templateSliderUrl = config.url.addons.car.templateSlider;

		appendElement.empty();
    $.get(templateSliderUrl, function (data) {
      var template = window._.template(data, {
        data: dataCarSize
      });
      $(template).appendTo(appendElement);
      fitlerByCarSize(filterData);
      $('.addon-item').find('.accordion-car').on('click', function() {
      	sliderLandingSales();
      });
    }, 'html');

	}

	var fitlerByCarSize = function(data){
		var filterData = [],
				carSizeItem = $('#car-avaliable-slider .slide-car-item'),
				removeEl = carSizeItem.find('[data-remove-carsize]'),
				filterTag = $('.addon-car-editfilter').find('.block-result');

		arrCarSizeChecked = null;

		carSizeItem.each(function(){
			$(this).off('click.filterCarSize').on('click.filterCarSize', function(e){
				e.preventDefault();
				var carSize = $(this).data('carsize'),
						carSizeDesc = $(this).data('carsize-desc');
				if(!$(this).is('.active')) {
					carSizeItem.removeClass('active');
					filterTag.find('.result-item').each(function(){
						if($(this).data('carsize-tag') === arrCarSizeChecked) {
							$(this).remove();
						}
					});
					$(this).addClass('active');
					arrCarSizeChecked = carSize;
					filterTag.append('<div class="result-item" data-carsize-tag='+carSize+'><span>'+carSizeDesc+'</span><a href="#" class="close-btn"><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
					filterData = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);
					filterData = sortBy === 'Recommended' ? sortDataCar('order', filterData) : sortDataCar('price', filterData);
					if(JSON.stringify(filterData) !== JSON.stringify(currentCarData)) {
						renderListCar(filterData);
					}
				}
			});
		});

		removeEl.each(function(){
			$(this).off('click.removeFilterCarSize').on('click.removeFilterCarSize', function(e){
				e.preventDefault();
				e.stopPropagation();
				var selfItem = $(this).closest('.slide-car-item'),
						carSize = selfItem.data('carsize');
				if(selfItem.is('.active')) {
					arrCarSizeChecked = null;
					selfItem.removeClass('active');
					filterTag.find('.result-item').each(function(){
						if($(this).data('carsize-tag') === carSize) {
							$(this).remove();
						}
					});

					filterData = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);
					filterData = sortBy === 'Recommended' ? sortDataCar('order', filterData) : sortDataCar('price', filterData);
					if(JSON.stringify(filterData) !== JSON.stringify(currentCarData)) {
						renderListCar(filterData);
					}
				}
			});
		});

	}

	var getCarSizeData = function(data) {
		var newObj = {},
		 		codes = {},
		 		listCarSize = [];

		$.grep(data,function(vehicle, vehicleIdx){
			var code = vehicle.carSize.code;
			if(!(code in codes)) {
				newObj[code] = [];
				codes[code] = true;
			}
			newObj[code].push(vehicle);
		});

		_.each(newObj, function(carSize, carSizeIdx){
			if(carSize.length === 1) {
				listCarSize.push(carSize[0]);
			} else {
				var minCarSize = carSize.sort(function(a,b){
					return parseInt(a.price.basePrice) - parseInt(b.price.basePrice)
				})[0];

			listCarSize.push(minCarSize);
			}
		});

		return listCarSize;
	}

	var getCarTemplate = function(carData) {
		pickupDateObj = new Date(carData.pickup.dateAndTime);
		dropoffDateObj = new Date(carData.dropoff.dateAndTime);
		var	pickupDateFormat = convertDateObj(pickupDateObj),
				dropoffDateFormat = convertDateObj(dropoffDateObj),
				pickupId = carData.pickup.locationID,
				pickupLocation = carData.pickup.locationName,
				dropoffId = carData.dropoff.locationID,
				dropoffLocation = carData.dropoff.locationName,
				filteredData = [],
				dataCarSize = [],
				vehiclesList = carData.vehicles,
				sortBySelect = $('.addon-car-editfilter .sortby__select').find('select'),
				partySizeSelect = $('[data-select-partysize]').find('select');


		initDataSearchForm(pickupDateFormat, dropoffDateFormat, pickupLocation, dropoffLocation);
		filteredData = filterDataByLocation(vehiclesList,pickupId,dropoffId,filteredData);
		dataCarSize = getCarSizeData(filteredData);

		filteredData = sortDataCar('order', filteredData);

		initCarData = filteredData;

		partySize = $('[data-select-partysize]').find('select').val();

		renderSliderCar(dataCarSize, filteredData);

		editFilters(filteredData);

		renderListCar(filteredData);

		sortBySelect.off('change.handleSort').on('change.handleSort', function(){

			sortBy === $(this).val();

			var data = sortDataCar($(this).val(), currentCarData);

			if(JSON.stringify(data) !== JSON.stringify(currentCarData)) {
				renderListCar(data);
			}

		});

		partySizeSelect.off('change.filterByPartySize').on('change.filterByPartySize', function(){

			partySize = $(this).val();

			if(partySize !== '') {
				if($('[data-partysize]').length > 0) {
					$('[data-partysize]').find('span').attr('data-index', partySize).text(partySize);
				} else {
					$('.block-result').last().append('<div class="result-item" data-partySize="true"><span data-index="'+partySize+'">'+partySize+'</span><a href="#" class="close-btn"><em class="ico-close"></em></a></div>');
				}
			}

			var data = filterDataByCheckbox(initCarData, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);

			data = sortBy === 'Recommended' ? sortDataCar('order', data) : sortDataCar('price', data);

			if(JSON.stringify(data) !== JSON.stringify(currentCarData)) {
				renderListCar(data);
			}

		});

	}

	var initDataSearchForm = function(pickupDateObj, dropoffDateObj, pickupLocation, dropoffLocation){
		var pickupEl = $('[data-pickup]'),
				dropoffEl = $('[data-dropoff]'),
				pickupDate = pickupEl.find('[data-time-input]'),
				pickupHour = pickupEl.find('.pickup-time.time-minute'),
				pickupMinute = pickupEl.find('.pickup-time.time-second'),
				pickupLocal = pickupEl.find('[data-cars-json]'),
				dropoffDate = dropoffEl.find('[data-time-input]'),
				dropoffHour = dropoffEl.find('.dropoff-time.time-minute'),
				dropoffMinute = dropoffEl.find('.dropoff-time.time-second'),
				dropoffLocal = dropoffEl.find('[data-cars-json]');


		pickupHour.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(pickupDateObj.timeFormat.split(':')[0])) {
				$(this).attr('selected', 'selected');
			}
		});

		pickupMinute.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(pickupDateObj.timeFormat.split(':')[1])) {
				$(this).attr('selected', 'selected');
			}
		});

		pickupHour.defaultSelect('update');
		pickupMinute.defaultSelect('update');

		pickupHour.find('.select__text').text(pickupDateObj.timeFormat.split(':')[0]);

		pickupMinute.find('.select__text').text(pickupDateObj.timeFormat.split(':')[1]);


		dropoffHour.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(dropoffDateObj.timeFormat.split(':')[0])) {
				$(this).attr('selected', 'selected');
			}
		});

		dropoffMinute.find('option').each(function(){
			$(this).removeAttr('selected');
			if(parseInt($(this).val()) === parseInt(dropoffDateObj.timeFormat.split(':')[1])) {
				$(this).attr('selected', 'selected');
			}
		});

		dropoffHour.defaultSelect('update');
		dropoffMinute.defaultSelect('update');

		dropoffHour.find('.select__text').text(dropoffDateObj.timeFormat.split(':')[0]);

		dropoffMinute.find('.select__text').text(dropoffDateObj.timeFormat.split(':')[1]);

		pickupDate.val(pickupDateObj.dateFormat);
		dropoffDate.val(dropoffDateObj.dateFormat);

		if($('#locationForRentalCars') && $('#locationForRentalCars').val() !== '') {
			var term1 = $('#locationPickupForRentalCars').val(),
					term2 = $('#locationDropoffForRentalCars').val(),
					prepareApiSrc1 = 'http://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=1&solrTerm=' + term1,
					prepareApiSrc2 = 'http://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=1&solrTerm=' + term2;
			$.get(prepareApiSrc1, function(data) {
				var data1 = data.results.docs[0];
				pickupLocal.val(data1.name + ', ' + data1.country);
				$('.accordion__content--car input[name="pickup-locationID"]').val(data1.locationId);
				$('.accordion__content--car input[name="pickup-country"]').val(data1.country);
				$('.accordion__content--car input[name="pickup-city"]').val(data1.city);

			});

			$.get(prepareApiSrc2, function(data) {
				var data1 = data.results.docs[0];
				dropoffLocal.val(data1.name + ', ' + data1.country);
				$('.accordion__content--car input[name="dropoff-locationID"]').val(data1.locationId);
				$('.accordion__content--car input[name="dropoff-country"]').val(data1.country);
				$('.accordion__content--car input[name="dropoff-city"]').val(data1.city);

			})
		}

	}

	var filterDataByLocation = function(vehiclesList, pickupId, dropoffId,filteredData) {

		var filteredData = vehiclesList.filter(function(vehicle){
			var selfDropoffId = vehicle.route.dropOff.locationID,
					selfPickupId = vehicle.route.pickUp.locationID;
			return (selfDropoffId === dropoffId) && (selfPickupId === pickupId);
		});
		return filteredData;
	}

	var filterDataByCheckbox = function(data, arrCarsize, arrSupplier, arrLocal, arrFuel, arrAutomatic, aircondition, doors, partySize){
		var filterData = [],
				filterDataCarSize = [],
				filterDataSupplier = [],
				filterDataLocation = [],
				filterDataFuel = [],
				filterDataAutomatic = [],
				selfData = [];

		if(arrCarsize) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.carSize.code === arrCarsize;
			});
			data = selfData;
		}

		if(arrSupplier.length > 0) {
			$.grep(arrSupplier, function(supplier, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.supplier.supplierName === supplier;
				});
				filterDataSupplier = $.unique([].concat.apply([],[filterDataSupplier,selfData]));
			});
			data = filterDataSupplier;
		}

		if(arrLocal.length > 0) {
			$.grep(arrLocal, function(local, idx){
				selfData = [],
				local = true;
				selfData = data.filter(function(vehicle){
					return vehicle.route.pickUp.onAirport === local;
				});
				filterDataLocation = $.unique([].concat.apply([],[filterDataLocation,selfData]));
			});
			data = filterDataLocation;
		}

		if(arrFuel.length > 0) {
			$.grep(arrFuel, function(fuel, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.fuelPolicy.description === fuel;
				});
				filterDataFuel = $.unique([].concat.apply([],[filterDataFuel,selfData]));
			});
			data = filterDataFuel;
		}

		if(aircondition) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.aircon === 'Yes';
			});
			data = selfData;
		}

		if(arrAutomatic.length > 0) {
			$.grep(arrAutomatic, function(other, idx){
				selfData = [];
				selfData = data.filter(function(vehicle){
					return vehicle.automatic === other;
				});
				filterDataAutomatic = $.unique([].concat.apply([],[filterDataAutomatic,selfData]));
			});
			data = filterDataAutomatic;
		}

		if(doors) {
			selfData = [];
			selfData = data.filter(function(vehicle){
				return vehicle.doors >= 4;
			});
			data = selfData;
		}

		selfData = [];
		selfData = data.filter(function(vehicle){
			if(partySize === "1 to 2") {
				return parseInt(vehicle.seats) <= 5;
			} else if(partySize === "3 to 4") {
				return parseInt(vehicle.seats) === 5;
			} else if(partySize === "5 and more") {
				return parseInt(vehicle.seats) >= 5;
			} else {
				return parseInt(vehicle.seats) >= 1;
			}
		});
		data = selfData;

		return data;

	}

	var sortDataCar = function(name, data, isDesc){
		if(name === 'order') {
			data.sort(function(a, b){
				return parseInt(a.order) - parseInt(b.order);
			});
		} else if(name === 'price') {
			data.sort(function(a, b){
				return (parseInt(a.price.basePrice) + parseInt(a.price.discount)) - (parseInt(b.price.basePrice) + parseInt(b.price.discount));
			});
		}

		return data;
	}

	var getCheckbox = function(item, arrCheckbox, flag){
		var itemName = item,
				flag;

		if(arrCheckbox.length === 0) {
			arrCheckbox.push(itemName);
		} else {
			for(var i = 0; i < arrCheckbox.length; i ++) {
				if(arrCheckbox[i] === itemName) {
					flag = true;
					break;
				} else {
					flag = false;
				}
			}
			if(flag === false) {
				arrCheckbox.push(itemName);
			}
		}

		return arrCheckbox;
	}

	var checkOther = function(item, checkAir, check) {
		var flag = false;

		for(var i = 0; i < item.length; i++) {
			if(!checkAir) {
				if(parseInt(item[i].doors) > 4) {
					flag = true;
					break;
				}
			}
			else {
				if(item[i].aircon) {
					flag = true;
					break;
				}
			}
		}

		return flag;
	}

	var appendCheckbox = function(el, arrCheckbox, flag, air, door) {
		var listCheckbox = '';

		el.empty();

		flag === true ? listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ air +'" id="'+ air.replace(/\s/g,"-") +'" aria-labelledby="'+ air +'" type="checkbox" data-aircondition value="'+air+'" aria-label="'+ air +'"><label for="'+ air.replace(/\s/g,"-") +'">'+ air +'</label></div></li>' : ''

		$.grep(arrCheckbox, function(item, idx){
			listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ item +'" id="'+ item.replace(/\s/g,"-") +'" type="checkbox" data-automatic value="'+item+'" aria-label="'+ item +'"><label for="'+ item.replace(/\s/g,"-") +'">'+ item +'</label></div></li>'
		});

		flag === true ? listCheckbox += '<li><div class="custom-checkbox custom-checkbox--1"><input name="'+ door +'" id="'+ door.replace(/\s/g,"-") +'" aria-labelledby="'+ door +'" type="checkbox" data-4door value="'+door+'" aria-label="'+ door +'"><label for="'+ door.replace(/\s/g,"-") +'">'+ door +'</label></div></li>' : ''

		el.append($(listCheckbox));
	}

	var renderCheckbox = function(data) {
		var arrSupplier = [],
				arrFuel = [],
				arrCarAutomatic = [],
				arrSupplierLocal = [],
				flag = false,
				supplierCheckbox = $('[data-check-supplier]'),
				localtionCheckbox = $('[data-check-location]'),
				fuelCheckbox = $('[data-check-fuel]'),
				carspecificCheckbox = $('[data-check-carspecific]'),
				CarAir,
				CarDoor;


		for(var i = 0; i < data.length; i++) {
			if(data[i].supplier.supplierName) {
				flag = true;
				break;
			}
		};

		flag ? arrSupplierLocal.push('In Terminal') : '';


		checkOther(data, true) ? CarAir = 'Air-conditioning' : '';

		$.grep(data, function(vehicle, idx){
			arrSupplier = getCheckbox(vehicle.supplier.supplierName, arrSupplier);
			arrFuel = getCheckbox(vehicle.fuelPolicy.description, arrFuel);
			arrCarAutomatic = getCheckbox(vehicle.automatic, arrCarAutomatic);
		});

		checkOther(data, false) ? CarDoor = '4+ Doors' : '';

		appendCheckbox(supplierCheckbox, arrSupplier, false, null, null);
		appendCheckbox(localtionCheckbox, arrSupplierLocal, false, null, null);
		appendCheckbox(fuelCheckbox, arrFuel, false, null, null);
		appendCheckbox(carspecificCheckbox, arrCarAutomatic, true, CarAir, CarDoor);

	}

	var editFilters = function(data){
		var blockwrapper = $('.addon-car-editfilter'),
				blockFilters = blockwrapper.find('.block-item-choose'),
				showContentChoose = blockwrapper.find('.show-block-content'),
				resulItem = blockwrapper.find('.result-item'),
				closeBtn = blockwrapper.find('.close-btn'),
				itemChoose = blockwrapper.find('.list').find('input'),
				closeBlock = $('.block-content').find('.close-addon'),
				applyClick = $('.block-content').find('.btn-apply'),
				dataAfterCheckbox = [],
				filteredData;
		showContentChoose.on('click',function(e){
			$(this).addClass('hidden');
			blockwrapper.find('.block-content').removeClass('hidden');
			e.preventDefault();
		});
		$(document).on('click', '.close-addon', function(e){
			e.preventDefault();
			$(this).closest('.block-content').addClass('hidden');
		});
		$(document).on('click','.close-btn', function(e){
			e.preventDefault();
			var indexClose = $(this).closest('.result-item');
			if(indexClose.data('carsize-tag')) {
				var carSize = indexClose.data('carsize-tag');
				$('[data-carsize="'+carSize+'"] [data-remove-carsize]').trigger('click.removeFilterCarSize');
			} else if(indexClose.data('partysize')) {
				$('[data-select-partysize]').find('.select__text').text('Select');
				$('[data-select-partysize]').find('select').val('');
			} else {
				var dataIndex = indexClose.find('span').data('index');
				itemChoose.each(function(index, obj){
					if(obj.id === dataIndex){
						$(this).attr('checked', false);
					}
				});
			}
			applyClick.trigger('click.startFilter', true);
			indexClose.remove();
		});
		itemChoose.on('click', function(){
			var dataId = $(this).attr('id');
			var nameItem = $(this).closest('.custom-checkbox').find('label').text();
			var resultItem = $('.block-result').find('.result-item');
			if($(this).is(':checked')){
				blockFilters.find('.block-result').last().append('<div class="result-item"><span data-index="'+dataId+'">'+nameItem+'</span><a href="#" class="close-btn"><em class="ico-close"><span class="ui-helper-hidden-accessible">Close</span></em></a></div>');
			}else{
				resultItem.each(function(index, obj){
					if(obj.children[0].attributes[0] && obj.children[0].attributes[0].value === dataId){
						$(this).remove();
					}
				});
			}
		});
		closeBlock.on('click',function(){
			blockwrapper.find('.block-content').addClass('hidden');
			showContentChoose.removeClass('hidden');
		});
		applyClick.off('click.startFilter').on('click.startFilter',function(e, flag){

			arrSupplierChecked = [];
			arrLocalChecked = [];
			arrFuelChecked = [];
			arrAutomaticChecked = [];
			AirChecked = null;
			DoorChecked = null;

			 $('[data-check-supplier]').find('input[type="checkbox"]:checked').each(function(){
			 	arrSupplierChecked.push($(this).val());
			 });

			 $('[data-check-location]').find('input[type="checkbox"]:checked').each(function(){
			 	arrLocalChecked.push($(this).val());
			 });

			 $('[data-check-fuel]').find('input[type="checkbox"]:checked').each(function(){
			 	arrFuelChecked.push($(this).val());
			 });

			 $('[data-check-carspecific]').find('[data-automatic]:checked').each(function(){
			 	arrAutomaticChecked.push($(this).val());
			 });

			 $('[data-check-carspecific]').find('[data-aircondition]:checked').each(function(){
			 	AirChecked = $(this).val();
			 });

			 $('[data-check-carspecific]').find('[data-4door]:checked').each(function(){
			 	DoorChecked = $(this).val();
			 });

			 sortBy = $('addon-car-editfilter .sortby__select').find('input').val();

			 partySize = $('[data-select-partysize]').find('select').val();


			dataAfterCheckbox = filterDataByCheckbox(data, arrCarSizeChecked, arrSupplierChecked, arrLocalChecked, arrFuelChecked, arrAutomaticChecked, AirChecked, DoorChecked, partySize);

			if(!flag) {
				blockwrapper.find('.block-content').addClass('hidden');
				showContentChoose.removeClass('hidden');
			}

			filteredData = sortBy === 'Recommended' ? sortDataCar('order', dataAfterCheckbox) : sortDataCar('price', dataAfterCheckbox);

			if(JSON.stringify(filteredData) !== JSON.stringify(currentCarData)) {
				renderListCar(filteredData);
			}

		});

	};


	var convertDateObj = function(obj) {
		var date = $.datepicker.formatDate('dd/mm/yy', obj),
				hour = obj.getHours(),
				minute = (obj.getMinutes() < 10) ? '0' + obj.getMinutes() : obj.getMinutes(),
				time = hour + ' : ' + minute,
				obj = {
					dateFormat : date,
					timeFormat : time
				};
		return obj;
	}

	var sliderLandingSales = function(){
		var totalMobileSlide = 2,
				totalLandscapeSlide = 2;
		$('[data-mobile-slider]').each(function() {
			var slider = $(this);
			slider.find('img').each(function() {
				var self = $(this),
						newImg = new Image();

				newImg.onload = function() {
					slider.css('visibility', 'visible');
					slider.find('.slides')
						.slick({
							siaCustomisations: true,
							dots: true,
							speed: 300,
							draggable: true,
							slidesToShow: totalLandscapeSlide,
							slidesToScroll: totalLandscapeSlide,
							accessibility: false,
							arrows: false,
							responsive: [
								{
									breakpoint: 480,
									settings: {
										slidesToShow: totalMobileSlide,
										slidesToScroll: totalMobileSlide
									}
								}
							]
						});
				};
				newImg.src = self.attr('src');
			});
		});
		$('.item-inner').css('padding-right', '5px');
	};
	// $('.addon-item').find('.accordion-car').on('click', function() {
	// 	sliderLandingSales();
	// });

	var url = $('.addons-landing-content .accordion__content--car').data('carjson');

	getCarData(url);

	var resetFilter = function(){
		var btnReset = $('.addon-no-result .reset');

		btnReset.off('click.resetFilter').on('click.resetFilter', function(e){
			e.preventDefault();
			handleResetFilter()
		});

	}

	var handleResetFilter = function(){
		arrSupplierChecked = [];
		arrLocalChecked = [];
		arrFuelChecked = [];
		arrAutomaticChecked = [];
		AirChecked = null;
		DoorChecked = null;
		arrCarSizeChecked = null;
		partySize = "";

		$('[data-select-partysize]').customSelect('updateSync');

		$('#car-avaliable-slider .slide-car-item').removeClass('active');
		$('.block-apply input[type="checkbox"]').removeAttr('checked');
		$('.addon-car-editfilter .block-result').empty();
		renderListCar(initCarData);
	}

	resetFilter();

  var selectCars = function(url, dataVehicleid){
    var tplBookingCar;
    var appendAfterDiv = $('.accordion__content--car');
    var tplBlockCar = function(data1){
      $.get(global.config.url.addOnCarSelected, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateBlockCar = $(template);
        appendAfterDiv.find('.block-search-filter').addClass('hidden');
        appendAfterDiv.append(templateBlockCar);
        $('#form-selected-car').validate({
					focusInvalid: true,
					errorPlacement: global.vars.validateErrorPlacement,
					success: global.vars.validateSuccess
				});
				$('#form-selected-car').find('[data-customselect]').each(function(){
					$(this).defaultSelect('refresh');
				});
        SIA.forceInput();
				enableConfirmBtn();
        win.scrollTop(appendAfterDiv.offset().top);
        addedCar(data1);
        $('.cancellation [data-tooltip]').kTooltip();
        $('.editor [data-tooltip]').kTooltip();
      });
    };
    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data2 = reponse.response.details;
        var vehicle = data2.vehicles;
        for(var i = 0,length = vehicle.length ; i < length; i++) {
        	if(dataVehicleid == parseInt(vehicle[i].vehicleId)){
        		var vehicleItem = vehicle[i];
        	}
        }
        tplBlockCar(vehicleItem);
      }
    });
  };

  var selectTermAndonditions = function(url){
    var TermAndonditions;
    var appendBeforeDiv = $('.popup--add-ons-car-term-condition-1');
    var tplTermAndonditions = function(data1){
      $.get(global.config.url.addOnCarTermAndonditions, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateTermAndonditions = $(template);
        appendBeforeDiv.append(templateTermAndonditions);
      });
    };
    $.ajax({
      url: url,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data2 = reponse.response.details;
        tplTermAndonditions(data2);
      }
    });
  };
  selectTermAndonditions(url);

  var enableConfirmBtn = function(){
  	var firstName = $('#form-selected-car').find('input[name="first-name"]'),
  			surName = $('#form-selected-car').find('input[name="sur-name"]'),
  			confirm = $('#form-selected-car').find('input[name="confirm"]');
  	$('#form-selected-car').find('input[name="first-name"], input[name="sur-name"]').each(function(){
  		$(this).off('keyup.enableConfirm').on('keyup.enableConfirm', function(){
  			if(firstName.valid() && surName.valid() && $('#title').valid() === true) {
  				confirm.removeAttr('disabled').removeClass('disabled');
  			} else {
  				confirm.attr('disabled', true);
  				confirm.addClass('disabled');
  			}
  		});
  	});

  	$('#title').off('change.enableConfirm').on('change.enableConfirm', function(){
  		if(firstName.valid() && surName.valid() && $('#title').valid() === true) {
  			confirm.removeAttr('disabled').removeClass('disabled');
  		} else {
  			confirm.attr('disabled', true);
  			confirm.addClass('disabled');
  		}
  	});
  }

  var addedCar = function(data){
  	$('.block-extras input[name="confirm"]').off('click.addedCar').on('click.addedCar', function(){
  			var addonItem = $(this).closest('.addon-item'),
  		 			parentAccordionControll = addonItem.find('[data-accordion-trigger]'),
  		 			desc = addonItem.find('.description'),
  					listLinkEl = $('<ul class="list-link">' +
												'<li><a href="#" class="trigger-accordion-added"><em class="ico-point-r--addon"></em>Edit add-on</a>'+
												'</li><li><a href="#" class="trigger-accordion-remove"><em class="ico-point-r--addon"></em>Cancel</a>' +
												'</li></ul>');

  		parentAccordionControll.find('.ico-point-r').addClass('hidden');

  		if(parentAccordionControll.find('.list-link').length === 0) {
  			parentAccordionControll.append(listLinkEl);
  		}

  		parentAccordionControll.find('.list-link').removeClass('hidden');

  		desc.addClass('hidden');

  		if(addonItem.find('.add-ons-item-added').length <= 0) {
  			renderCarAddonAdded(addonItem, data);
  		}

  		addonItem.find('.addon-added').removeClass('hidden');

  		$('.car-extra .btn-back').trigger('click.backResultList');

  		// htmlBody.animate({ scrollTop: addonItem.offset().top}, 'slow');

			var editAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-added'),
					cancelAddonTriger = parentAccordionControll.find('.list-link').find('.trigger-accordion-remove');

			cancelAddonTriger.off('click.cancelAddon').on('click.cancelAddon', function(e){
				e.preventDefault();
				e.stopPropagation();
				addonItem.find('.addon-added').addClass('hidden');
				desc.removeClass('hidden')
				addonItem.find('.add-ons-item-added').remove();
				parentAccordionControll.find('.list-link').addClass('hidden');
				parentAccordionControll.children('.ico-point-r').removeClass('hidden');
				htmlBody.animate({ scrollTop: addonItem.offset().top}, 'slow');
				if(parentAccordionControll.is('.active')){
					parentAccordionControll.trigger('click.accordion');
				}

				$('.accordion__content--car').find('.car-extra').remove();
				$('.accordion__content--car .block-search-filter').removeClass('hidden');
				$('.accordion__content--car').find('.list-cars-result input[name="remove-car"]').addClass('hidden');
    		$('.accordion__content--car').find('.list-cars-result input[name="select-car"]').removeClass('hidden');

    		handleResetFilter();

			});
  	});
  }

  var renderCarAddonAdded = function(el, dataCarAdded) {
  	var pickupDate = covertDateToRender(pickupDateObj),
  			dropoffDate = covertDateToRender(dropoffDateObj);

  	$.get(global.config.url.addons.car.templateCarAdded, function (data) {
  		var template = window._.template(data, {
  			data: dataCarAdded,
  			pickupDate: pickupDate,
  			dropoffDate: dropoffDate
  		});
  		$(template).insertBefore(el.find('[data-accordion-trigger]'));
  	}, 'html');

  }

  var covertDateToRender = function(objdate){
  	var hour = objdate.getHours(),
  			minute = (objdate.getMinutes() < 10) ? '0' + objdate.getMinutes() : objdate.getMinutes(),
  			date = $.datepicker.formatDate( "yy MM dd (D)", objdate),
  			dateAfter;

  	dateAfter = hour < 12 ? (hour + ':' + minute + ' AM ' + date) : (hour + ':' + minute + ' PM ' + date);

  	return dateAfter;
  }

  $(document).on('click','input[name="select-car"]', function(){
    var dataVehicleid = $(this).closest('.item-result').data('vehicleid'),
    		selectBtn = $(this);

    setTimeout(function(){
    	removeCarAdded();
    	selectCars(url, dataVehicleid);
    	selectBtn.closest('.list-cars-result').find('input[name="select-car"]').removeClass('hidden');
    	selectBtn.closest('.list-cars-result').find('input[name="remove-car"]').addClass('hidden');
    	selectBtn.addClass('hidden');
    	selectBtn.next().removeClass('hidden');
    	$(document).ready(function(){
        var mySetInterval = setInterval(function(){
        	var dataID = $('.accordion__content--car').find('.car-extra').attr('data-id', dataVehicleid);
	        clearInterval(mySetInterval);
	    	},300);
	    });
    }, 100);

  });

  var removeCarAdded = function() {
  	var addonItem = $('.accordion__content--car').closest('.addon-item'),
  			desc = addonItem.find('.description');

  		addonItem.find('.addon-added').addClass('hidden');
  		addonItem.find('.ico-point-r').removeClass('hidden');
  		addonItem.find('.list-link').addClass('hidden');
  		desc.removeClass('hidden');
			addonItem.find('.add-ons-item-added').remove();

  }

  $(document).on('click.backResultList', '#remove-car, .car-extra > .btn-back', function(e) {
  	e.preventDefault();
    $('.accordion__content--car').find('.car-extra').remove();
    $('.accordion__content--car .block-search-filter').removeClass('hidden');
    setTimeout(function(){
			htmlBody.animate({ scrollTop: $('.list-cars-result').offset().top}, 'slow');
    },300);
  });

  $(document).on('click', 'input[name="remove-car"]', function(e) {
  	e.preventDefault();
    $('.accordion__content--car').find('.list-cars-result input[name="remove-car"]').addClass('hidden');
    $('.accordion__content--car').find('.list-cars-result input[name="select-car"]').removeClass('hidden');
    removeCarAdded();
  });

  $(document).on('click', '#remove-protection', function() {
 		var groupButton = $('.button-group-2');
  	$(this).addClass('hidden');
  	$('.check-full-protection').find('#full-protection').prop('checked', false);
  	groupButton.find('#add-protection').removeClass('hidden');
  	groupButton.children().eq(0).addClass('hidden');
		groupButton.children().eq(1).removeClass('hidden');
  });

  $(document).on('click', '#add-protection', function() {
  	var groupButton = $('.button-group-2');
  	$(this).addClass('hidden');
  	$('.check-full-protection').find('#full-protection').prop('checked', true);
  	groupButton.find('#remove-protection').removeClass('hidden');
  	groupButton.children().eq(0).removeClass('hidden');
		groupButton.children().eq(1).addClass('hidden');
  });

  $('.accordion__content--car').off('click.handleSeemoreCar').on('click.handleSeemoreCar', '[data-seemore-car]', function(){
  	 var listCar = $(this).closest('.list-cars-result'),
  	 		 listCarItem = listCar.find('.item-result'),
  	 		 count = 2,
  	 		 noHiddenIdx = listCarItem.not('.hidden').length;
  	 for(var i = noHiddenIdx; i < noHiddenIdx + count; i++) {
  	 	listCarItem.eq(i).removeClass('hidden');
  	 }
  	 if(listCarItem.not('.hidden').length === listCarItem.length) {
  	 	$(this).addClass('hidden');
  	 }
  });

  $('.block-search-filter').on('click','.custom-checkbox > input', function() {
  	var agedDriver = $('.driver-field').find('#drive-age-1');
  	var validateAged =  agedDriver.find('.input-5__text');
  	var checkboxNoError = $('.driver-field').find('.custom-checkbox');
  	if($(this).is(':checked')){
  		agedDriver.addClass('hidden');
  		validateAged.find('input').attr('data-rule-required', false);
  		agedDriver.next('.text-error').addClass('hidden');
  		checkboxNoError.find('label').addClass('style-checkbox-error');
  	}else{
  		validateAged.find('input').attr('data-rule-required', true);
  		agedDriver.removeClass('hidden');
  		checkboxNoError.find('label').removeClass('style-checkbox-error');
  	}
  });

  var popup1 = $('.popup--add-ons-car-popup-excess-explained');
  var popup2 = $('.popup--add-ons-car-term-condition-1');
  $(document).on('click', '.trigger-popup-term-condition', function() {
  	$(this).trigger('dblclick');
    popup2.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('dblclick', '.trigger-popup-term-condition', function() {
  	popup2.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  });
  $(document).on('click', '.trigger-popup-excess-explained', function() {
  	popup1.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
  	$(this).trigger('dblclick');
  });
  $(document).on('dblclick', '.trigger-popup-excess-explained',function(){
	  popup1.Popup('show');
    $('body').find('.fadeInOverlay').addClass('overlay');
	});
  $(document).on('click', '.popup__close', function(event) {
    popup1.Popup('hide');
    popup2.Popup('hide');
    event.preventDefault();
  });

};
