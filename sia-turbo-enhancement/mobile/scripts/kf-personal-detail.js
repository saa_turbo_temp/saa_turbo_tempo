/**
 * @name SIA
 * @description Define global kf-personal-detail function
 * @version 1.0
 */
SIA.KFPersonalDetail = function(){
	var global = SIA.global;
	var config = global.config;
	var formPersonalDetail = $('[data-form-non-editable]'),
			btnCancel = $('[data-button-cancel]'),
			btnDelete = $('[data-button-delete]'),
			btnShowPIN = $('[data-btn-show-pin]'),
			lnkServiceFee = $('[data-link-service-fee]'),
			popupDeleteKf = $('[data-popup-delete-nominee]'),
			popupServiceFees = $('[data-popup-service-fees]'),
			btnFirstSubmit = $('#btn-sumit-1'),
			btnLastSubmit = $('#btn-sumit-2'),
			hiddenRowEmpty = $('[data-check-empty-value]'),
			currentPinEl = $('[data-current-pin]'),
			redemtionNomineeEl = $('[data-redemtion-nominee]'),
			btnAddNominee = $('[data-add-nominee]'),
			checkPinEl = $('[data-check-pin]'),
			rowQuestionDisabled = $('[data-row-question]'),
			resGender = $('[data-gender-resource]'),
			refGender = $('[data-gender-reference]');

	var correctDate = function(){
		var target = $('[data-rule-validatedate]');
		var detectDate = function(d, m, y, el){
			var nd = new Date();
			var getLastDate = new Date((y ? y : nd.getFullYear()), (m ? m : '01'), 0);
			if(d > getLastDate.getDate()){
				el.val(getLastDate.getDate());
				el.defaultSelect('refresh');
			}
		};
		target.each(function(){
			var self = $(this);
			var data = self.data('rule-validatedate');
			var date = $(data[0]);
			var month = $(data[1]);
			var year = $(data[2]);
			// console.log(date, month, year);
			date.off('change.correctDate').on('change.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
			month.off('change.correctDate').on('change.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
			year.off('change.correctDate').on('change.correctDate', function(){
				detectDate(date.val(), month.find(':selected').index(), year.val(), date);
			});
		});
	};

	correctDate();

	var resetFieldData = function(el) {
		var parentEl = el.closest('.autocomplete');
		if(parentEl.data('autocomplete')) {
			parentEl.find('[autocomplete]').data('uiAutocomplete')._value(
				// el.find(':selected').data('text')
				el.find('[selected]').length ? el.find(':selected').data('text') : ''
			);
		} else {
			el.defaultSelect('refresh');
		}
	};

	var foreachField = function(isDisabled, formEl, isResetData) {
		isDisabled = isDisabled || false;
		formEl = formEl || formPersonalDetail;
		isResetData = isResetData || false;
		formEl.find(':input').each(function() {
			var that = $(this);
			if(this.type !== 'submit') {
				if(isResetData && that.is('select')) {
					resetFieldData(that);
				}
				if(!that.is(btnDelete) && !that.hasClass('non-active')) {
					that.prop('disabled', isDisabled);
				}
			} else {
				that.val(isDisabled ? L10n.kfProfiles.edit : L10n.kfProfiles.save);
			}
		});
	};

	var initAjax = function(url, data, type, callback) {
		type = type || 'json';
		callback = callback || $.noop();
		if(url && data) {
			$.ajax({
				url: url,
				type: global.config.ajaxMethod,
				dataType: type,
				data: data,
				success: function(res) {
					if(res) {
						callback();
					}
				},
				error: function(jqXHR, textStatus) {
					console.log(textStatus);
				}
			});
		}
	};

	var hiddenRowInfo = function() {
		if(hiddenRowEmpty.length) {
			hiddenRowEmpty.each(function() {
				var that = $(this);
				if(!that.find(':input').val()) {
					that.addClass('hidden');
				} else {
					that.removeClass('hidden');
				}
			});
		}
	};

	var detectGender = function(resourceElement, referenceElement) {
		if(!resourceElement.length || !referenceElement.length) {
			return;
		}
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	// var convertMonth = function(n, isGetIndex){
	// 	var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	// 	return isGetIndex ? m.indexOf(n) : m[n];
	// };

	// $.validator.addMethod('checkpassport', function(value, el, param) {
	// 	if($(el).is('.hidden')) {
	// 		return true;
	// 	}
	// 	var arr = param[1].split(',');
	// 	var cd = new Date();
	// 	var optDate = new Date($(arr[2]).val(), convertMonth($(arr[1]).val(), true), $(arr[0]).val());
	// 	// ignore validate
	// 	if($(param[3]).is(':checked')){
	// 		return true;
	// 	}
	// 	// detect validate with condition
	// 	if(param[2]){
	// 		if((optDate - cd)/(30*24*60*60*1000) < param[0] && $(param[2]).is(':checked')){
	// 			return false;
	// 		}
	// 		else{
	// 			return true;
	// 		}
	// 	}
	// 	else{
	// 		if((optDate - cd)/(30*24*60*60*1000) < param[0]){
	// 			return false;
	// 		}
	// 	}
	// 	return true;
	// }, L10n.validator.checkpassport);

	detectGender(resGender, refGender);

	if(!popupDeleteKf.data('Popup')){
		popupDeleteKf.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}

	if(!popupServiceFees.data('Popup')){
		popupServiceFees.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]'
		});
	}

	formPersonalDetail.each(function() {
		$(this).validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			invalidHandler: global.vars.invalidHandler,
			onfocusout: global.vars.validateOnfocusout,
			submitHandler: function() {
				var that = $(this.currentForm);
				if(that.hasClass('non-editable')) {
					var screenH;
					if(btnLastSubmit.length) {
						screenH = btnLastSubmit.offset().top - global.vars.win.scrollTop();
					}
					if(hiddenRowEmpty.length) {
						hiddenRowEmpty.removeClass('hidden');
					}
					if(currentPinEl.length) {
						if(that.find(currentPinEl).length) {
							currentPinEl.val('');
						}
					}
					that.removeClass('non-editable');
					checkPostcode();
					foreachField(false, that);
					detectGender(resGender, refGender);
					that.data('validatedOnce', false);
					if(!that.data('initTitleDropdown')) {
						SIA.initPersonTitles();
						that.data('initTitleDropdown', true);
					}
					if(btnLastSubmit.length && btnFirstSubmit.length) {
						if(global.vars.win.scrollTop() > btnFirstSubmit.offset().top){
							// global.vars.win.scrollTop(btnLastSubmit.offset().top - screenH);
							// global.vars.win.scrollTop(that.offset().top);
						}
					}
				} else {
					var validator = that.data('validator');
					if(validator.checkForm()) {
						if(that.data('formSubmitAjax')) {
							initAjax(config.url.kfMessageMarkJSON, that.find(':input').serialize(), null, function() {
								that.addClass('non-editable');
								foreachField(true, that);
								that.find('[data-pin]').each(function() {
									var pinEle = $(this);
									if(pinEle.data('pin')) {
										pinEle.attr('type', 'password');
										btnShowPIN.removeClass('active').text(L10n.kfProfiles.showPin);
									}
								});
							});
							return false;
						}
						return true;
					}
				}
				return false;
			}
		});
	});

	if(redemtionNomineeEl.length && redemtionNomineeEl.length >= 5) {
		btnAddNominee.addClass('disabled').on('click', function(e){ e.preventDefault(); });
	}

	if(formPersonalDetail.hasClass('non-editable')) {
		hiddenRowInfo();
		foreachField(true);
	}

	if(checkPinEl.length && rowQuestionDisabled.length) {
		var isDelay = false,
				minLength = checkPinEl.data('ruleMinlength');
		checkPinEl.off('keypress.checkSecurityPin').on('keypress.checkSecurityPin', function() {
			if(!isDelay) {
				isDelay = true;
				setTimeout(function() {
					if(!checkPinEl.hasClass('error') && checkPinEl.val().length >= minLength) {
						initAjax(config.url.sqcCheckPIN, {pin: checkPinEl.val()}, 'json', function() {
							rowQuestionDisabled.find('.grid-disabled').removeClass('disabled');
							rowQuestionDisabled.find(':input').removeAttr('disabled');
						});
					}
					isDelay = false;
				}, 1000);
			}
		});
	}

	$('#country').off('blur.editForm').on('blur.editForm', function(){
		checkPostcode();
	});

	function checkPostcode(){
		var countryVal = $('#country').val();
		var postCode = $('#postcode');
		if(countryVal === 'Singapore'){
			postCode.attr('data-rule-required', 'true');
			postCode.attr('data-msg-required', 'Singapore must have a postcode!');
		}else{
			postCode.removeAttr('data-rule-required');
			postCode.removeAttr('data-msg-required');
		}
	}

	// formPersonalDetail.off('submit.editForm').on('submit.editForm', function() {
	// 	var that = $(this);
	// 	if(that.hasClass('non-editable')) {
	// 		var screenH;
	// 		if(btnLastSubmit.length) {
	// 			screenH = btnLastSubmit.offset().top - global.vars.win.scrollTop();
	// 		}
	// 		if(hiddenRowEmpty.length) {
	// 			hiddenRowEmpty.removeClass('hidden');
	// 		}
	// 		if(currentPinEl.length) {
	// 			if(that.find(currentPinEl).length) {
	// 				currentPinEl.val('');
	// 			}
	// 		}
	// 		that.removeClass('non-editable');
	// 		checkPostcode();
	// 		foreachField(false, that);
	// 		that.data('validatedOnce', false);
	// 		if(!that.data('initTitleDropdown')) {
	// 			SIA.initPersonTitles();
	// 			that.data('initTitleDropdown', true);
	// 		}
	// 		if(btnLastSubmit.length && btnFirstSubmit.length) {
	// 			if(global.vars.win.scrollTop() > btnFirstSubmit.offset().top){
	// 				// global.vars.win.scrollTop(btnLastSubmit.offset().top - screenH);
	// 				// global.vars.win.scrollTop(that.offset().top);
	// 			}
	// 		}
	// 	} else {
	// 		var validator = that.data('validator');
	// 		if(validator.checkForm()) {
	// 			if(that.data('formSubmitAjax')) {
	// 				initAjax(config.url.kfMessageMarkJSON, that.find(':input').serialize(), null, function() {
	// 					that.addClass('non-editable');
	// 					foreachField(true, that);
	// 					that.find('[data-pin]').each(function() {
	// 						var pinEle = $(this);
	// 						if(pinEle.data('pin')) {
	// 							pinEle.attr('type', 'password');
	// 							btnShowPIN.removeClass('active').text(L10n.kfProfiles.showPin);
	// 						}
	// 					});
	// 				});
	// 				return false;
	// 			}
	// 			return true;
	// 		}
	// 	}
	// 	return false;
	// });

	btnCancel.off('click.cancelEdit').on('click.cancelEdit', function() {
		var currentForm = $(this).closest('form'),
				pinField = currentForm.find('[data-pin]'),
				autocompleteField = currentForm.find('[data-autocomplete]'),
				screenH;
		if(btnLastSubmit.length) {
			screenH = btnLastSubmit.offset().top - global.vars.win.scrollTop();
		}
		hiddenRowInfo();
		pinField.attr('type','password');
		if(currentForm.find(btnShowPIN).length) {
			btnShowPIN.removeClass('active').text(L10n.kfProfiles.showPin);
		}
		currentForm.get(0).reset();
		currentForm.data('validator').resetForm();
		foreachField(true, currentForm, true);
		currentForm.addClass('non-editable');
		if(btnLastSubmit.length && btnFirstSubmit.length) {
			if(global.vars.win.scrollTop() > btnFirstSubmit.offset().top){
				// global.vars.win.scrollTop(btnLastSubmit.offset().top - screenH);
				// global.vars.win.scrollTop(currentForm.offset().top);
			}
		}
		if(autocompleteField.length) {
			autocompleteField.removeClass('default');
		}
		if(currentForm.find(checkPinEl).length && currentForm.find(rowQuestionDisabled).length) {
			rowQuestionDisabled.find('.grid-disabled').addClass('disabled');
			rowQuestionDisabled.find(':input').attr('disabled', 'disabled');
		}
	});

	btnShowPIN.each(function(){
		var self = $(this),
			isShow = false;

		self.off('click.showPIN').on('click.showPIN', function(e) {
			if(!self.hasClass('active')) {
				self.addClass('active').text(L10n.kfProfiles.hidePin);
				isShow = true;
			} else {
				self.removeClass('active').text(L10n.kfProfiles.showPin);
				isShow = false;
			}
			self.closest('form').find('[data-pin]').each(function() {
				var that = $(this);
				if(that.data('pin')) {
					that.attr('type', isShow ? 'text' : 'password');
				}
			});
			e.preventDefault();
		});
	});

	btnDelete.off('click.deleteNominee').on('click.deleteNominee', function() {
		popupDeleteKf.Popup('show');
	});

	lnkServiceFee.off('click.deleteNominee').on('click.deleteNominee', function(e) {
		e.preventDefault();
		popupServiceFees.Popup('show');
	});
};
