'use strict';
/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
var SIA = SIA || {};

SIA.global = (function () {

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	var paramUrl = sURLVariables[1];
	var sName = paramUrl ? paramUrl.split('=')[1] : '';
	var getURLParams = function(sParam) {
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1];
			}
		}
	};

	// configuration
	var config = {
		width: {
			docScroll: window.Modernizr.touch || window.navigator.msMaxTouchPoints ? 0 : 20,
			datepicker: 14
		},
		mobile: 768,
		tablet: 988,
		formatDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		template: {
			overlay: '<div class="overlay"></div>',
			error: '<em class="ico-error">error</em>',
			// labelError: '<span class="text-error">Lorum ipsum lorum ipsum</span>',
			labelError: '<p class="text-error"><span>Lorum ipsum lorum ipsum</span></p>',
			success: '<em class="ico-success">success</em>',
			flags: '<img src="images/transparent.png" alt="" class="flags {0}">',
			addEmail: '<div data-validate-row="true" class="table-row"><div data-validate-col="true" class="table-col table-col-1"><span>{0}.</span></div><div data-validate-col="true" class="table-col table-col-2"><div class="table-inner"><label for="email-address-{0}" class="hidden">&nbsp;</label><span class="input-1"><input type="email" name="email-address-{0}" id="email-address-{0}" placeholder="Email address" value="" data-rule-email="true" data-msg-email="{1}"></span></div></div></div>',
			addEmailConfirm: '<div class="table-row"><div class="table-col table-col-1"><span>{0}.</span></div><div class="table-col table-col-2"><div class="table-inner"><label for="email-confirm-input-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"><input type="email" name="email-confirm-input-01" value="{1}" id="email-confirm-input-{0}" readonly="readonly"></span></div></div></div>',
			AddSMSSuccessfullySent: '<div class="table-row"> <div class="table-col table-col-1"><span>{0}.</span></div> <div class="table-col table-col-2"> <div class="table-inner"> <label for="country-name-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="text" name="country-name-{0}" value="{1}" id="country-name-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-3"> <div class="table-inner"> <label for="area-code-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="tel" name="area-code-{0}" value="{2}" id="area-code-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-4"> <div class="table-inner"> <label for="phone-number-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="text" name="phone-number-{0}" value="{3}" id="phone-number-{0}" readonly="readonly"></span> </div> </div> </div>',
			loadingStatus: '<div class="loading-block"><img src="images/ajax-loader-ie.gif" alt="loading" longdesc="img-desc.html"></div>',
			loadingMedium: '<div class="loading loading--medium">Loading</div>',
			loadingSmall: '<div class="loading loading--small">Loading</div>'
		},
		zIndex: {
			datepicker: 1004,
			ppLanguage: 100,
			tabContentOverlay: 14,
			overlayPopup: 14
		},
		duration: {
			overlay: 400,
			menu: 400,
			languagePopup: 400,
			searchPopup: 400,
			newsTicker: {
				auto: 2000,
				animate: 1000
			},
			clearTimeout: 100,
			bookingWidget: 400,
			popupSearch: 400,
			flightStatus: 400,
			popupGesture: 400
		},
		imgSrc: {
			transparent: 'images/transparent.png'
		},
		url: {
			yourVoucherTpl: 'template/your-vouchers.tpl',
			aboutVoucher: 'template/vouchers-about.tpl',
			voucherAccountSummary: 'template/vouchers-account-summary.tpl',
			voucherFlightSegmentType: 'template/vouchers-flight-segment-type.tpl',
			voucherFlightSegmentTypeItems: 'template/vouchers-flight-segment-item-selection.tpl',
			voucherFlightSegmentTemplate: 'template/vouchers-flight-segment-item.tpl',
			checkVoucherAvailUrlFailue: 'ajax/voucher-selected-segment-list-json-response-failue.json',
			checkVoucherAvailUrl: 'ajax/voucher-selected-segment-list-json-response.json',
			paxFakeData: 'ajax/pax-fake-data.json',
			simpleStickyTemplate: 'template/simple-sticky.tpl',
			stickyTemplate: 'template/sticky.tpl',
			barTemplate: 'template/bar.tpl',
			flightHistoryTemplate: 'template/flight-history.tpl',
			addOnMbTemplate: 'template/add-on-mb.tpl',
			addOnMbTemplateSales: 'template/add-on-mb-sales.tpl',
			bookingDetailCarResponse : 'template/booking-detail-car-response.tpl',
			addOnCarSelected : 'template/add-on-car-selected.tpl',
			addOnCarTermAndonditions : 'template/add-on-car-term-and-conditions.tpl',
			addOnMbHotelTemplateSales: 'template/add-on-mb-hotel-sales.tpl',
			addOnAddedTemplateSales: 'template/add-on-added-sales.tpl',
			jsonAutocomplete: 'ajax/destinations-data.json',
			fareDealsJSON: 'ajax/fare-deals.json',
			templateFareDeal: 'template/fare-deals.tpl',
			cabinSelectJSON : 'ajax/cabin-data.json',
			flightStatusJSON : 'ajax/ice-flight-status.json',
			flightStatusTemplate : 'template/flight-status.tpl',
			flightScheduleJSON : 'ajax/flight-schedule.json',
			flightScheduleTemplate : 'template/flight-schedule.tpl',
			promotionPageKrisflyerTemplate : 'template/promotion-krisflyer-list.tpl',
			promotionPageKrisflyerJSON : 'ajax/promotion-krisflyer-list.json',
			promotionPageTemplate : 'template/promotion-list.tpl',
			promotionEnhancePageTemplate: 'template/promotion-enhance-list.tpl',
			promotionPageJSON : 'ajax/promotion-fare-list.json',
			promotionPageJSONSeaMore : 'ajax/promotion-fare-list-1.json',
			promotionPageJSONSearch : 'ajax/promotion-fare-list-search.json',
			languageJSON : 'ajax/autocomplete-language.json',
			social: {
				facebookSharing : 'https://www.facebook.com/sharer.php?u=',
				twitterSharing : 'https://twitter.com/share?url=',
				gplusSharing : 'https://plus.google.com/share?url='
			},
			CIBFlightSearch1Template: 'template/cib-flight-search.tpl',
			CIBFlightSearch1UpgradeTemplate: 'template/flight-upgrade.tpl',
			bookingDetailResponse: 'ajax/booking-details-response.json',
			flightSearchFare1JSON: 'ajax/cib-flight-search-fare-1.json',
			flightSearchFare2JSON: 'ajax/cib-flight-search-fare-2.json',
			yourVoucher: 'ajax/AccountSummarySampleJson_updated.json',
			flightSearchFareFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-search-fare-flight-info.json',
			flightSearchFare1UpgradeJSON: 'ajax/cib-flight-search-fare-1-upgrade.json',
			flightSearchFare2UpgradeJSON: 'ajax/cib-flight-search-fare-2-upgrade.json',
			kfTableTemplate: 'template/how-to-earn-kf-table.tpl',
			kfTableJSON: 'ajax/how-to-earn-kf-table.json',
			kfUseTableTemplate: 'template/how-to-use-kf-table.tpl',
			kfUseTableJSON: 'ajax/how-to-use-kf-table.json',
			kfAtAGlanceTemplate: 'template/kf-at-a-glance.tpl',
			kfAtAGlanceTemplate01: 'template/kf-at-a-glance-01.tpl',
			kfAtAGlanceTemplateButton: 'template/kf-at-a-glance-button.tpl',
			kfAtAGlanceTemplateSf: 'template/kf-at-a-glance-sf.tpl',
			// kfAtAGlanceJSON: 'ajax/kf-at-a-glance.json',
			kfMessageMarkJSON : 'ajax/kf-message-mark.json',
			kfMessageDeleteJSON : 'ajax/kf-message-delete.json',
			kfMessageItemsJSON: 'ajax/kf-message-items-mobile.json',
			kfMessageItemTemplate: 'template/kf-message-item.tpl',
			kfBookingUpcomingJSON: 'ajax/kf-booking-upcoming-flights.json',
			kfBookingUpcomingTemplate: 'template/kf-booking-upcoming-flights.tpl',
			kfFlightHistoryJSON: 'ajax/kf-booking-flight-history.json',
			kfFlightHistoryTemplate: 'template/kf-booking-flight-history.tpl',
			kfPastFlightHistoryTemplate: 'template/kf-booking-past-flight.tpl',
			kfCheckInJSON: 'ajax/kf-check-in.json',
			kfCheckInTemplate: 'template/kf-check-in.tpl',
			langToolbarTemplate: 'template/language-toolbar.tpl',
			orbFlightResultTemplate: 'template/orb-flight-result.tpl',
			orbFlightResultJSON: 'ajax/orb-flight-result.json',
			orbFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-search-flight-info.json',
			orbWaitlistedFlightJSON: 'ajax/orb-waitlisted-flight.json',
			cibFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select.json',
			cibFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select-on-change.json',
			orbFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select.json',
			orbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select-on-change.json',
			mbFlightSelect: $('body').hasClass('mb-payments-excess-baggage-page') ? 'ajax/mb-flight-select-excess-baggage.json' : getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select.json',
			mbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select-on-change.json',
			kfNumberNomineeJSON: getURLParams('nominne') ? ('ajax/JSONS/' + sName) : 'ajax/kf-number-nominne.json',
			mb: {
				selectMeal: 'template/mb-select-meals.tpl',
				selectMealSidebar: 'template/mb-select-meals-sidebar.tpl',
				selectFlightMeal: 'template/mb-flight-select-meals.tpl',
				selectFlightMealSidebar: 'template/mb-flight-select-meals-sidebar.tpl',
				selectFlightMealPopupMenu: 'template/mb-flight-select-meals-popup.tpl'
			},
			cibBookingSummaryDetailsPopupTemplate: 'template/booking-summary-details-popup.tpl',
			cibBookingSummarySfDetailsPopupTemplate: 'template/booking-summary-details-popup-sf.tpl',
			orbBookingSummaryDetailsPopupTemplate: 'template/orb-booking-summary-details-popup.tpl',
			cibBookingSummaryDetailsPopupTemplateAddOn: 'template/booking-summary-details-popup-addon.tpl',
			bookingFlightConfirm: 'ajax/booking-flight-confirm.json',
			kfMilesTemplate: 'template/kf-miles.tpl',
			kfFavorite: 'template/kf-favourite.tpl',
			kfStatement: 'template/kf-statement.tpl',
			preferSeatContent: 'template/prefer-seat-content.tpl',
			kfSubBookingUpcomingFlights: 'template/kf-booking-upcoming-flights-01.tpl',
			kfExtendMileJSON: 'ajax/kf-extend-miles.json',
			orbFlightSchedule: 'template/orb-flight-schedule.tpl',
			sqcUpcomingFlightTemplate: 'template/sqc-upcoming-flight.tpl',
			sqcUpcomingFlightDetailTemplate: 'template/sqc-upcoming-flight-detail.tpl',
			sqcUserDelete: 'ajax/sqc-user-delete.json',
			sqcSalePointDelete: 'ajax/sqc-sale-point-delete.json',
			sqcCheckPIN: 'ajax/sqc-check-PIN.json',
			sqcSavedTripsTemplate: 'template/sqc-saved-trips.tpl',
			sqc: {
				atGlanceTemplate: 'template/sqc-at-a-glance.tpl',
				atGlanceDetailTemplate: 'template/sqc-at-a-glance-detail.tpl',
				atGlanceJSON: 'ajax/sqc-at-a-glance.json'
			},
			ssh: {
				hotel: {
					json: 'ajax/ssh-hotel.json',
					template: 'template/ssh-hotel.tpl'
				}
			},
			bookingDetailTemplate: 'template/booking-detail.tpl',
			bookingDetailMealTemplate: 'template/booking-detail-meal.tpl',
			bookingDetailBagTemplate: 'template/booking-detail-baggage.tpl',
			excessBaggageTemplate: 'template/booking-detail-excess-baggage.tpl',
			mealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-meal.json',
			mealFlightJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-meal.json',
			mealFlightPopupJSON: getURLParams('mealMenu') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-popup-meal.json',
			manageBookingMealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/manage-booking-meal.json',
			excessMealJSON: 'ajax/booking-detail-excess-meal.json',
			baggageJSON: getURLParams('baggage') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-baggage.json',
			excessBaggageJSON: 'ajax/booking-detail-excess-baggage.json',
			success: 'ajax/success.json',
			desEntryTemplate: 'template/des-entry.tpl',
			desEntryJSON: 'ajax/des-entry.json',
			staticMoviesTemplate: 'template/static-content-movies.tpl',
			staticMoviesJSON: 'ajax/static-content-movies.json',
			staticMusicTemplate: 'template/static-content-music.tpl',
			staticMusicJSON: 'ajax/static-content-music.json',
			kfVoucherRedemptionJSON: 'ajax/kf-voucher-redemption.json',
			kfVoucherSummaryJSON: 'ajax/how-to-use-kf-voucher.json',
			kfVoucherRedemptionTemplate: 'template/kf-voucher-redemption.tpl',
			kfVoucherRedemptionReviewTemplate: 'template/kf-voucher-redemption-review.tpl',
			videoLightbox: {
				youtube: 'template/youtube-template-lightbox.tpl',
				flowplayer: 'template/flowplayer-template-lightbox.tpl'
			},
			addBaggagePopup: {
				template: 'template/add-baggage-popup.tpl',
				json: 'ajax/add-baggage-popup.json'
			},
			promotionsPackages: {
				json: 'ajax/pormotions-packages.json',
				template: 'template/pormotions-packages.tpl'
			},
			countryCityAutocomplete: 'ajax/country-city.json',
			faredealPromotion: {
				// faredealMultiPriceJson: 'ajax/faredeal-multiple-price.json',
				// faredealMultiPriceSeemoreJson: 'ajax/faredeal-multiple-price-seemore.json',
				// pricePointsJson: 'ajax/pormotions-price-points.json',
				bookingWidgetJson: 'ajax/booking-widget.json'
			},
			addons: {
				hotel: {
					json: 'ajax/Hotel_Availability_Response.json',
                    jsonmb: 'ajax/Hotel_Availability_Response.json',
					template: 'template/add-on-hotel-list.tpl'
				},
				car: {
					templateSlider: 'template/add-on-car-slider.tpl',
					templateCar: 'template/add-on-car-list.tpl',
					templateCarAdded: 'template/add-on-car-added.tpl'
				}
			},
		},
		highlightSlider: {
			desktop: 4,
			tablet: 3,
			mobile: 1
		},
		datepicker: {
			numberOfMonths: ((window.Modernizr.touch || window.navigator.msMaxTouchPoints) && ($(window).width() < 768)) ? 1 : 2
		},
		seatMap: {
			template: {
				cabin: '<div class="seatmap-cabin"></div>',
				space: '<div class="seatmap-cabin-separate"></div>',
				sLabelWrapper: '<div class="seatmap-cabin-row seatmap-toprow"></div>',
				seatWrapper: '<div class="seatmap-cabin-wrapper"></div>',
				sLabel: '<div data-sia-rowblock="{0}" class="seatmap-columnletter">{1}</div>',
				blk: '<div class="seatmap-row-block"></div>',
				aisle: '<div class="seat-aisle"></div>',
				seatRow: '<div class="seatmap-cabin-row"><span class="seatmap-rownum left">{0}</span><span class="seatmap-rownum right">{0}</span></div>',
				seatColumn: '<div data-sia-rowblock="{0}" class="seatmap-row-block"></div>',
				inforSeat: '<span class="passenger-info__seat"></span>'
			},
			flightClass: {
				economy: 'Economy Class',
				business: 'Business Class'
			},
			seat: {
				available: 'ico-seat-available',
				empty: 'ico-seat-empty',
				occupied: 'ico-seat-occupied',
				bassinet: 'ico-1-sm-bassinet',
				blocked: 'ico-seat-blocked'
			},
			url: {
				seatMap: 'template/seatmap-foundation.html',
				seatMapJSON: 'ajax/seatmap_sample.json',
				seatMapJSON1: 'ajax/seatmap_sample-1.json'
			},
			passenger: 2
		},
		ajaxMethod: 'get',
		map: {
			googlemap: {
				apiKey: 'AIzaSyA8UdAEjReJYw6GTXdTqBsIxHwRA738xTo',
				lib: 'places',
				zoom: 14,
				radius: 1000
			},
			baidumap: {
				apiKey: '11ipY4UjrfPkwsHPhpqWzeHvaWkzGyAL',
				zoom: 18,
				radius: 1000
			}
		}
	};

	if (!String.prototype.format) {
		String.prototype.format = function () {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function (match, number) {
				return typeof args[number] !== 'undefined' ? args[number] : match;
			});
		};
	}
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		};
	}

	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) {
				if (this[i] === obj) { return i; }
			}
			return -1;
		};
	}


	// global variable
	var vars = {};
	var doc = $(document);
	var win = $(window);
	var body = $(document.body);
	var html = $('html');
	var menuHolder = $();
	var container = $('#container');

	var header = $('.header');
	var menuT = $('.menu');
	var menuBar = $('.menu-bar');
	var mainMenu = $('.menu-main');
	var popupSearch = $('.popup--search');
	var popupPromo = $('.popup--promo');
	var popupLogin = $('.popup--login');
	var ppLanguage = $('.popup--language');
	var popupLoggedProfile = $('.popup--logged-profile');

	var languageJSON = {
		'data': [
			{
				'value': 'Global',
				'flag':'global',
				'language': 'en_UK, zh_CN, fr_FR, pt_BR, de_DE, zh_TW, ja_JP, ko_KR, ru_RU, es_ES',
				'order': 1
			},
			{
				'value': 'Australia',
				'flag':'australia',
				'language': 'en_UK',
				'order': 2
			},
			{
				'value': 'Austria',
				'flag':'austria',
				'language': 'en_UK',
				'order': 3
			},
			{
				'value': 'Bangladesh',
				'flag':'bangladesh',
				'language': 'en_UK',
				'order': 4
			},
			{
				'value': 'Belgium',
				'flag':'belgium',
				'language': 'en_UK, fr_FR',
				'order': 5
			},
			{
				'value': 'Brazil',
				'flag':'brazil',
				'language': 'pt_BR, en_UK',
				'order': 6
			},
			{
				'value': 'Brunei',
				'flag':'brunei',
				'language': 'en_UK',
				'order': 7
			},
			{
				'value': 'Cambodia',
				'flag':'cambodia',
				'language': 'en_UK',
				'order': 8
			},
			{
				'value': 'Canada',
				'flag':'canada',
				'language': 'en_UK',
				'order': 9
			},
			{
				'value': 'People\'s Republic Of China',
				'flag':'people_republic_of_china',
				'language': 'zh_CN, en_UK',
				'order': 10
			},
			{
				'value': 'Denmark',
				'flag':'denmark',
				'language': 'en_UK',
				'order': 11
			},
			{
				'value': 'Egypt',
				'flag':'egypt',
				'language': 'en_UK',
				'order': 12
			},
			{
				'value': 'France',
				'flag':'france',
				'language': 'fr_FR, en_UK',
				'order': 13
			},
			{
				'value': 'Germany',
				'flag':'germany',
				'language': 'de_DE, en_UK',
				'order': 14
			},
			{
				'value': 'Greece',
				'flag':'greece',
				'language': 'en_UK',
				'order': 15
			},
			{
				'value': 'Hong Kong',
				'flag':'hong_kong',
				'language': 'en_UK, zh_TW',
				'order': 16
			},
			{
				'value': 'India',
				'flag':'india',
				'language': 'en_UK',
				'order': 17
			},
			{
				'value': 'Indonesia',
				'flag':'indonesia',
				'language': 'en_UK',
				'order': 18
			},
			{
				'value': 'Ireland',
				'flag':'ireland',
				'language': 'en_UK',
				'order': 19
			},
			{
				'value': 'Italy',
				'flag':'italy',
				'language': 'en_UK',
				'order': 20
			},
			{
				'value': 'Japan',
				'flag':'japan',
				'language': 'ja_JP, en_UK',
				'order': 21
			},
			{
				'value': 'Kuwait',
				'flag':'kuwait',
				'language': 'en_UK',
				'order': 22
			}, {
				'value': 'Laos',
				'flag': 'laos',
				'language': 'en_UK',
				'order': 23
			},{
				'value': 'Luxembourg',
				'flag': 'luxembourg',
				'language': 'en_UK',
				'order': 24
			}, {
				'value': 'Malaysia',
				'flag': 'malaysia',
				'language': 'en_UK',
				'order': 25
			}, {
				'value': 'Maldives',
				'flag': 'maldives',
				'language': 'en_UK',
				'order': 26
			}, {
				'value': 'Nepal',
				'flag': 'nepal',
				'language': 'en_UK',
				'order': 27
			}, {
				'value': 'Netherlands',
				'flag': 'netherlands',
				'language': 'en_UK',
				'order': 28
			}, {
				'value': 'New Zealand',
				'flag': 'new_zealand',
				'language': 'en_UK',
				'order': 29
			}, {
				'value': 'Norway',
				'flag': 'norway',
				'language': 'en_UK',
				'order': 30
			}, {
				'value': 'Philippines',
				'flag': 'philippines',
				'language': 'en_UK',
				'order': 31
			}, {
				'value': 'Republic of Korea',
				'flag': 'south_korea',
				'language': 'ko_KR, en_UK',
				'order': 32
			}, {
				'value': 'Russia',
				'flag': 'russia',
				'language': 'ru_RU, en_UK',
				'order': 33
			}, {
				'value': 'Saudi Arabia',
				'flag': 'saudia_arabia',
				'language': 'en_UK',
				'order': 34
			}, {
				'value': 'Singapore',
				'flag': 'singapore',
				'language': 'en_UK, zh_CN',
				'order': 35
			}, {
				'value': 'South Africa',
				'flag': 'south_africa',
				'language': 'en_UK',
				'order': 36
			}, {
				'value': 'Spain',
				'flag': 'spain',
				'language': 'es_ES, en_UK',
				'order': 37
			}, {
				'value': 'Sri Lanka',
				'flag': 'sri_lanka',
				'language': 'en_UK',
				'order': 38
			}, {
				'value': 'Sweden',
				'flag': 'sweden',
				'language': 'en_UK',
				'order': 39
			}, {
				'value': 'Switzerland',
				'flag': 'switzerland',
				'language': 'en_UK, de_DE',
				'order': 40
			}, {
				'value': 'Taiwan',
				'flag': 'taiwan',
				'language': 'zh_TW, en_UK',
				'order': 41
			}, {
				'value': 'Thailand',
				'flag': 'thailand',
				'language': 'en_UK',
				'order': 42
			}, {
				'value': 'Turkey',
				'flag': 'turkey',
				'language': 'en_UK',
				'order': 43
			}, {
				'value': 'United Arab Emirates',
				'flag': 'united_arab_emirates',
				'language': 'en_UK',
				'order': 44
			}, {
				'value': 'United Kingdom',
				'flag': 'united_kingdom',
				'language': 'en_UK',
				'order': 45
			}, {
				'value': 'United States',
				'flag': 'united_states',
				'language': 'en_UK',
				'order': 46
			}, {
				'value': 'Vietnam',
				'flag': 'vietnam',
				'language': 'en_UK',
				'order': 47
			}
		]
	};

	vars = {
		doc: doc,
		win: win,
		body: body,
		html: html,
		container: container,
		header: header,
		popupPromo: popupPromo,
		ppLanguage: ppLanguage,
		languageJSON: languageJSON,
		validationRuleByGroup: [
			'validateDate',
			'required_issue_place',
			'checkofadult',
			'checkofchild',
			'checkofinfant',
			'checkCurrentDate',
			'checkpassport',
			'validateAtLeastOne',
			'notEqualTo'
		]
	};

	if(body.is('#template')){
		return;
	}

	vars.isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};

	vars.isIEMobile = function() {
		return navigator.userAgent.toLowerCase().match(/iemobile/i) ? true : false;
	};

	vars.detectDevice = {
		isMobile: (function(){return function(){ return ($(window).width() < 768);};})(),
		isTablet: (function(){return function(){ return ((win.width() <= config.tablet && win.width() >= config.mobile));};})()
	};

	vars.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

	vars.isIos = (navigator.userAgent.match(/(iPhone)/g) ? true : false);

	vars.addClassOnIE = function(els,con, n){
		switch(con) {
			case 'both':
				els.each(function(idx){
					if(idx){
						$(this).addClass('nth-child-' + (idx + 1));
					}
					if($(this).is(':last')){
						$(this).addClass('last-child');
					}
				});
				break;
			case 'last':
				els.each(function(){
					if($(this).is(':last')){
						$(this).addClass('last-child');
					}
				});
				break;
			case '2':
				els.each(function(idx){
					if(idx === (con - 1)){
						$(this).addClass('nth-child-' + con);
					}
				});
				break;
			case 'option':
				els.each(function(idx){
					if(idx < n){
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
				break;
			case 'all':
				els.each(function(idx){
					$(this).addClass('nth-child-' + (idx + 1));
				});
				break;
			default:
				els.each(function(idx){
					if(idx){
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
		}
	};

	var addClassForSafari = function(){
		// if(isSafari && (!window.Modernizr.touch && !window.navigator.msMaxTouchPoints)){
		if(vars.isSafari){
			body.addClass('safari');
		}
	};

	var addClassForIosWindowPhoneAndroid = function(){
		if(vars.isIos){
			body.addClass('ios');
		}
		else if(window.navigator.msMaxTouchPoints){
			body.addClass('windows-phone');
			html.removeClass('no-touch').addClass('touch');
		}
		else{
			body.addClass('android');
		}
	};

	var centerPopup = function(el, visible){
		el.css({
			top: Math.max(0, (win.height() - el.innerHeight()) / 2) + win.scrollTop(),
			display: (visible ? 'block' : 'none'),
			//left: detectDevice.isMobile ? 0 : Math.max(0, (win.width() - el.innerWidth()) / 2)
			left: Math.max(0, (win.width() - el.innerWidth()) / 2)
		});
	};

	var detectHidePopup = function(){
		if(menuHolder.length){
			if(menuHolder.is('.popup--language')){
				menuHolder.find('.custom-select input').blur();
				menuHolder.data('triggerLanguage').trigger('click.showLanguage');
				win.off('resize.popupLanguage');
			}
			else if(!menuHolder.is('.popup--language')){
				menuHolder.data('menu').children('a').trigger('click.showSubMenu');
			}
		}
	};

	// if(!vars.detectDevice.isMobile()){
	// 	if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
	// 		doc.off('scroll.hideOverlay mousewheel.hideOverlay').on('scroll.hideOverlay mousewheel.hideOverlay', function(){
	// 			detectHidePopup();
	// 			hidePopupSearch();
	// 		});
	// 	}
	// 	else{
	// 		doc.off('touchmove.hideOverlay').on('touchmove.hideOverlay', function(){
	// 			if(vars.detectDevice.isTablet()){
	// 				detectHidePopup();
	// 				hidePopupSearch();
	// 			}
	// 		});
	// 	}
	// }

	var hideLoggedProfile = function(){
		if(popupLoggedProfile.length){
			if(!popupLoggedProfile.hasClass('hidden')){
				if(popupLoggedProfile.triggerPopup) {
					popupLoggedProfile.triggerPopup.removeClass('active');
				}
				popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
				if(popupLoggedProfile.overlay) {
					popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				}
				popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
					if(popupLoggedProfile.overlay) {
						popupLoggedProfile.overlay.remove();
						popupLoggedProfile.overlay = $();
					}
					popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
				win.off('resize.LoggedProfile');
			}
		}
	};

	var ieMobileSearchFix = function() {
		if(vars.isIE()) {
			popupSearch.off('keydown.ieMobileSearchFix').on('keydown.ieMobileSearchFix', function() {
				menuT.find('.menu-inner').scrollTop(0);
			});
		}
	};
	ieMobileSearchFix();

	/*var hidePopupSearch = function(disableHideProfile){
		if(!popupSearch.hasClass('hidden')){
			popupSearch.triggerPopup.removeClass('active');
			popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
			popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
			popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				popupSearch.removeClass('animated fadeOut').addClass('hidden');
				popupSearch.overlay.remove();
				popupSearch.overlay = $();
				popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
			win.off('resize.inputSearch');
		}
		if(!disableHideProfile){
			hideLoggedProfile();
		}
	};*/

	// create a gesture for popup on M and T
	var popupGesture = function(popup, trigger, pClose, pContent, notTablet, callback){
		var scrollTopHolder = 0;
		var overlayPopup = null;
		var timerPopupGesture = null;
		var winW = win.width();

		var popupOnMobile = function(){
			// var langToolbarEl = $('.toolbar--language');
			popup.show();
			popup.css({
				'position': 'fixed',
				'top': 0
			});
			if(popup.outerHeight(true) < win.height()){
				popup.css('height', win.height());
			}
			popup.addClass('animated slideInRight');
			popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				if(popup.outerHeight(true) > win.height()){
					container.css('height', popup.outerHeight(true)/* - (langToolbarEl.length ? langToolbarEl.height() : 0)*/);
				}
				else{
					container.css('height', win.height()/* - (langToolbarEl.length ? langToolbarEl.height() : 0)*/);
					popup.height(win.height());
				}
				container.css('overflow', 'hidden');
				popup.css('top', '');
				popup.css('position', '');
				win.scrollTop(0);
				// popup.removeClass('animated slideInRight');
				popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		};

		var onResize = function(){
			// if(winW === win.width()){
			// 	return;
			// }
			// else {
			// 	popup.find(pClose).trigger('click.closePopup');
			// }
			// var langToolbarEl = $('.toolbar--language');
			winW = win.width();
			popup.css('height', '');
			popup.find(pContent).css('height', '');
			if(popup.outerHeight(true) > win.height()){
				container.css('height', popup.outerHeight(true)/* - (langToolbarEl.length ? langToolbarEl.height() : 0)*/);
				container.css('overflow', 'hidden');
			}
			else{
				container.css('height', win.height()/* - (langToolbarEl.length ? langToolbarEl.height() : 0)*/);
				popup.height(win.height());
				container.css('overflow', 'hidden');
			}
			popup.show();
		};
		trigger.off('click.showPopup').on('click.showPopup', function(e){
			e.preventDefault();
			winW = win.width();
			if(callback){
				callback($(this));
			}
			var langToolbarEl = $('.toolbar--language');
			if(langToolbarEl.length) {
				langToolbarEl.hide();
			}
			scrollTopHolder = win.scrollTop();
			overlayPopup = $(config.template.overlay).appendTo(body).show().css({ 'zIndex': config.zIndex.overlayPopup}).addClass('animated fadeInOverlay');
			popupOnMobile();
			win.off('resize.popupGesture').on('resize.popupGesture', function(){
				clearTimeout(timerPopupGesture);
				timerPopupGesture = setTimeout(function(){
					//Fix iOS
					$(window).scrollTop(10);
					onResize();
				}, 150);
			});
			overlayPopup.off('click.closePopup').on('click.closePopup', function(e){
				e.preventDefault();
				popup.find(pClose).trigger('click.closePopup');
			});
		});
		popup.find(pClose).off('click.closePopup').on('click.closePopup', function(e){
			e.preventDefault();
			var langToolbarEl = $('.toolbar--language');
			if(langToolbarEl.length) {
				langToolbarEl.show();
			}
			win.off('resize.popupGesture');
			overlayPopup.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
			popup.css({
				'position': 'fixed',
				'top': 0
			});
			container.removeAttr('style');
			// if(langToolbarEl.length) {
			// 	container.css('paddingTop', langToolbarEl.height());
			// }
			win.scrollTop(scrollTopHolder);
			scrollTopHolder = 0;
			popup.addClass('animated slideOutRight');
			popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				popup.removeClass('animated slideOutRight slideInRight');
				overlayPopup.off('click.closePopup').remove();
				popup.removeAttr('style');
				popup.find(pContent).removeAttr('style');
				popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		});
	};
	//public popupGesture
	vars.popupGesture = popupGesture;

	// click document to hide popup
	container.off('click.hideSubMenu').on('click.hideSubMenu', function(e){
		if(!$(e.target).closest('.menu').length){
			if(typeof window.resetMenuT !=='undefined'){
				window.resetMenuT();
			}
		}
		// if(!$(e.target).closest('.popup--search').length){
		// 	hidePopupSearch();
		// }
	});

	var animationNavigationOnDesktop = function(){
		var menu = mainMenu.find('.menu-item');
		mainMenu.preventClick = false;
		menu.each(function(){
			var self = $(this);
			var realSub = self.children('.menu-sub'); // real sub

			if(realSub.length){

				if(!realSub.find('.sub-item').length && !realSub.children('.menu-sub__join').length) {
					self.children('a').find('.ico-point-r').addClass('hidden');
					return;
				}

				var menuAnimation = function(){
					if(realSub.hasClass('hidden')){
						menu.children('.menu-sub').not('.hidden').addClass('hidden').parent().removeClass('active');
						realSub.removeClass('hidden');
						menuBar.css('top', mainMenu.outerHeight(true));
						mainMenu.addClass('active');
						mainMenu.parent().scrollTop(0);
						if(mainMenu.height() < realSub.height()){
							mainMenu.height(realSub.height() + parseInt(mainMenu.parent().css('paddingTop'), 10));
						}
						menuBar.addClass('active');
						//fix scroll in sub-menu
						// self.siblings().addClass('hidden');
						// menuBar.addClass('hidden');
					}
					else{
						realSub.addClass('hidden');
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
						// self.siblings().removeClass('hidden');
						// menuBar.removeClass('hidden');
					}
				};

				// self.children('a').off('click.showSubMenu').on('click.showSubMenu', function(e){
				// 	e.preventDefault();
				// 	if(mainMenu.preventClick){
				// 		return;
				// 	}
				// 	menuAnimation();
				// });

				self.find('> a em.ico-point-r').off('click.showSubMenu').on('click.showSubMenu', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(mainMenu.preventClick){
						return;
					}
					menuAnimation();
				});

				realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
					e.preventDefault();
					menu.siblings().removeClass('hidden');
					mainMenu.css('height', '').removeClass('active');
					realSub.addClass('hidden');
					setTimeout(function() {
						menuBar.removeClass('active');
						menuBar.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							realSub.addClass('hidden');
							realSub.closest('.menu-item').removeClass('active');
							menuBar.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}, 5);
				});
			}
		});
	};

	var selectLanguage = function(){
		var triggerLanguage = menuBar.find('ul > li:first > a');
		var triggerCountry = $('#text-country');
		// var timeoutppLanguage = null;

		ppLanguage = ppLanguage.appendTo(body).css('zIndex', config.zIndex.ppLanguage);
		ppLanguage.data('triggerLanguage', triggerLanguage);

		triggerLanguage.off('click.showLanguage').on('click.showLanguage', function(e){
			e.preventDefault();
			$('html, body').scrollTop(0);
			var wW = win.width();
			var langToolbarEl = $('.toolbar--language');
			var closePPLanguage = function() {
				if(wW === win.width()) {
					win.off('resize.popupLanguage');
					triggerCountry.off('blur.reset');
				}
				ppLanguage.addClass('animated slideOutRight');
				triggerLanguage.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay').css('zIndex', '');
				ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
					triggerLanguage.removeClass('active');
					menuHolder = $();
					triggerLanguage.overlay.remove();
					ppLanguage.removeClass('animated slideOutRight slideInRight').addClass('hidden');
					ppLanguage.css('height', '');
					ppLanguage.find('.popup__content').removeAttr('style');
					// if(langToolbarEl.length) {
					// 	container.css('padding-top', langToolbarEl.height());
					// }
					triggerLanguage.overlay.remove();
					win.trigger('resize.openMenuT');
					win.trigger('orientationchange.openMenuT');
					ppLanguage.css('right', '');
					ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					ppLanguage.parent().css('overflow-y', 'auto');
					ppLanguage.parent().css('height', 'auto');
					triggerCountry.off('blur.reset').on('blur.reset', function(){
						$('html, body').scrollTop(0);
						ppLanguage.find('.popup__inner').height(win.height());
					});
				});
			};
			// open and close popup language

			if(ppLanguage.hasClass('hidden')){ //open
				triggerLanguage.overlay = $(config.template.overlay).appendTo(body);
				triggerLanguage.overlay.hide();
				triggerLanguage.overlay.off('click.hideOverlay').on('click.hideOverlay', function(){
					triggerLanguage.trigger('click.showLanguage');
				});

				// show
				ppLanguage.removeClass('animated');
				ppLanguage.removeClass('slideOutRight');
				ppLanguage.removeClass('hidden');
				// ppLanguage.parent().css('overflow-y', 'hidden');
				centerPopup(ppLanguage, true);
				ppLanguage.css('top', 0);
				ppLanguage.addClass('animated slideInRight');
				if(ppLanguage.outerHeight(true) > win.height()){
					container.css('height', ppLanguage.outerHeight(true));
				}
				else{
					var isIphone = /(iPhone)/g.test(navigator.userAgent);
					var h = isIphone ? ((win.innerHeight() > win.innerWidth()) ? window.screen.height : window.screen.width) : window.screen.height;
					container.css('height', h - (langToolbarEl.length ? langToolbarEl.height() : 0));
					ppLanguage.height(h);
					ppLanguage.parent().css('height', h);
				}
				container.css('overflow', 'hidden');
				ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
					ppLanguage.css('right', 0);
					ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});

				// fix bug for lumina automatically close modal
				ppLanguage.off('click.doNothing').on('click.doNothing', function(){});

				triggerLanguage.addClass('active');
				triggerLanguage.overlay.css({
					'zIndex': config.zIndex.ppLanguage
				});
				triggerLanguage.overlay.show().addClass('animated fadeInOverlay').css('zIndex', (win.width() < config.tablet - config.width.docScroll) ? (config.zIndex.ppLanguage - 1) : '');

				win.off('resize.popupLanguage').on('resize.popupLanguage', function(){
					if(wW !== win.width()) {
						closePPLanguage();
					}

					// clearTimeout(timeoutppLanguage);
					// timeoutppLanguage = setTimeout(function(){
					// 	if(wW !== win.width()){
					// 		if(ppLanguage.find('.ui-autocomplete-input').autocomplete('widget').is(':visible')){
					// 			ppLanguage.find('.ui-autocomplete-input').blur();
					// 		}
					// 		wW = win.width();
					// 	}
					// 	ppLanguage.css('height', '');
					// 	ppLanguage.find('.popup__content').css('height', '');
					// 	if(ppLanguage.outerHeight(true) > win.height()){
					// 		container.css('height', '');
					// 		container.css('height', ppLanguage.height());
					// 		ppLanguage.height(ppLanguage.height());
					// 	}
					// 	else{
					// 		container.css('height', '');
					// 		container.css('height', win.height());
					// 		ppLanguage.height(win.height());
					// 	}
					// },100);
				});
			}
			else{ // close
				closePPLanguage();
			}
		});

		// close popup language
		ppLanguage.find('.popup__close').off('click.hideLanguage').on('click.hideLanguage', function(e){
			e.preventDefault();
			//e.stopPropagation()
			triggerLanguage.trigger('click.showLanguage');
		});
	};

	var loginPopup = function(){
		var triggerLoginPoup = menuBar.find('ul a.login');
		var flyingFocus = $('#flying-focus');
		var travelWidget = $('#travel-widget');

		// init login popup
		popupLogin.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			beforeHide: function() {
				if(popupLogin.find('[data-tooltip]').length) {
					popupLogin.find('[data-tooltip]').data('kTooltip').closeTooltip();
				}
			},
			afterHide: function(){
				win.trigger('resize.openMenuT');
				win.trigger('orientationchange.openMenuT');
				if(travelWidget.length && travelWidget.find('.tab-content.active').is(':visible')){
					travelWidget.tabMenu('onResize');
				}
			}
		});
		triggerLoginPoup.off('click.showLoginPopup').on('click.showLoginPopup', function(e){
			e.preventDefault();

			// popupLogin.data('Popup').options.beforeShow = function() {};
			// popupLogin.data('Popup').options.afterHide = function() {};
			popupLogin.Popup('show');
		});

		// highlight input
		popupLogin.find('input:text, input:password').off('focus.highlight').on('focus.highlight', function(e){
			e.stopPropagation();
			$(this).closest('.input-1').addClass('focus');
		});
		popupLogin.find('input:text, input:password').off('blur.highlight').on('blur.highlight', function(){
			$(this).closest('.input-1').removeClass('focus');
		});
	};

	var menuTablet = function(){
		var navbar = $('.ico-nav');
		var menuBarItems = menuBar.find('li');
		var login = $();
		// var login = menuBarItems.eq(2).clone().addClass('login-item').prependTo(mainMenu.children('ul'));
		var search = menuBarItems.eq(1).clone(true).addClass('search-item').prependTo(mainMenu.children('ul'));
		var resetMenuTimer = null;
		var isAnimate = false;

		menuT = $('.menu');
		// menuT.find('.logged-in .menu-sub').css('top', -win.height());

		search.find('.popup--search').removeClass('hidden');
		login.hide();
		search.hide();

		// comment to fix input search autocomplete

		// search.find('input').off('focus.fixedOnIOS').on('focus.fixedOnIOS', function(){
		// 	var self = $(this);
		// 	if('ontouchstart' in window){
		// 		win.off('touchstart.removeCaret').on('touchstart.removeCaret', function(){
		// 			self.trigger('blur.removeEvent');
		// 		});
		// 		self.off('blur.removeEvent').on('blur.removeEvent', function(){
		// 			self.off('touchstart.removeCaret');
		// 		});
		// 	}
		// });

		var resetMenuT = function(){
			if(container.hasClass('show-menu')){
				var langToolbarEl = $('.toolbar--language');
				container.removeClass('show-menu');
				// reset menu
				menuT.addClass('fadeOutLeft animated');
				menuT.menuOverlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					container.removeAttr('style');
					if(langToolbarEl.length) {
						// container.css('padding-top', langToolbarEl.height());
						menuT.removeAttr('style');
					}
					win.off('resize.openMenuT');
					win.off('orientationchange.openMenuT');
					menuBarItems.eq(1).show();
					// menuBarItems.eq(2).show();
					login.hide();
					search.hide();
					menuT.removeClass('fadeOutLeft animated active').css('display', '').css('height', '');
					isAnimate = false;
					menuT.menuOverlay.remove();
					menuT.find('.menu-inner').removeAttr('style');
					mainMenu.removeClass('active').removeAttr('style');
					menuBar.removeClass('active');
					mainMenu.find('.menu-item').removeClass('active hidden').siblings().removeClass('hidden');
					mainMenu.find('.menu-sub').addClass('hidden');
					menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					popupLoggedProfile.addClass('hidden').css('margin-top', '').closest('.logged').removeClass('no-transition').css('margin-top', '');
					menuT.find('.menu-bar').removeClass('hidden');
				});
				menuT.find('input').blur();
			}
		};
		// freeze body to slide
		var freezeBody = function(){
			var getMaxHeight = window.screen.height || win.height();
			var langToolbarEl = $('.toolbar--language'),
					containerH = getMaxHeight,
					menuInnerEl = menuT.find('.menu-inner');

			if(langToolbarEl.length) {
				containerH -= langToolbarEl.height();
			}

			// var menuInnerHeight = containerH - parseInt(menuInnerEl.css('padding-top')) - parseInt(menuInnerEl.css('padding-bottom')) - parseInt(mainMenu.css('padding-top'));

			if(menuT.hasClass('active')){
				menuInnerEl.height(containerH - parseInt(menuInnerEl.css('padding-top')) - parseInt(menuInnerEl.css('padding-bottom')));

				container.css('height', '');
				container.css({
					'overflow': 'hidden',
					'height': containerH
				});
				return;
			}
			menuT.menuOverlay = $(config.template.overlay).appendTo(body).show().addClass('animated fadeInOverlay');
			// menuT.css('height', '');
			menuT.addClass('fadeInLeft animated active').show();
			menuT.find('.menu-inner').height(getMaxHeight - parseInt($('.menu-inner').css('padding-top')));
			menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				var menuInnerEl = menuT.find('.menu-inner');
				menuT.removeClass('fadeInLeft animated');
				isAnimate = false;
				container.css({
					'overflow': 'hidden',
					'height': containerH
				});

				menuInnerEl.height(containerH - parseInt(menuInnerEl.css('padding-top')) - parseInt(menuInnerEl.css('padding-bottom')));

				menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		};

		var openMenuT = function(){
			container.addClass('show-menu');
			freezeBody();

			menuT.menuOverlay.off('click.hideMenu').on('click.hideMenu', function(e){
				e.preventDefault();
				resetMenuT();
			});
			menuBarItems.eq(1).hide();
			// menuBarItems.eq(2).hide();
			login.show();
			search.show();
			// popupLoggedProfile.css('top', -mainMenu.height());
			var originalWidth = win.width();
			if(window.navigator.msMaxTouchPoints){
				win.off('orientationchange.openMenuT').on('orientationchange.openMenuT',function(){
					clearTimeout(resetMenuTimer);
					resetMenuT();
					// if(window.screen.width !== originalWidth) {
					// 	return;
					// }
					// resetMenuTimer = setTimeout(function(){
					// 	freezeBody();
					// }, config.duration.clearTimeout);
				});
			}
			else{
				win.off('resize.openMenuT').on('resize.openMenuT', function(){
					clearTimeout(resetMenuTimer);
					if(win.width() !== originalWidth) {
						resetMenuT();
						return;
					}
					resetMenuTimer = setTimeout(function(){
						freezeBody();
					}, config.duration.clearTimeout);
				});
			}

		};
		window.resetMenuT = resetMenuT;

		var popupSearchPosition = function(){
			popupSearch.removeClass('hidden').css({
				left: menuBarItems.eq(1).offset().left - popupSearch.width()/2 + menuBarItems.eq(1).width()/2 - 30,
				top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + 21
			});
		};

		menuBarItems.eq(1).children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e){
			e.preventDefault();
			e.stopPropagation();
			detectHidePopup();
			hideLoggedProfile();

			var self = $(this);
			if(popupSearch.hasClass('hidden')){
				self.addClass('active');
				popupSearchPosition();
				popupSearch.addClass('animated fadeIn');
				popupSearch.overlay = $(config.template.overlay).appendTo(body).addClass('animated fadeInOverlay').show();
				popupSearch.triggerPopup = self;

				popupSearch.find('.popup__close').off('click.closeSearch').on('click.closeSearch', function(e){
					e.preventDefault();
					e.stopPropagation();
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.overlay.off('click.closeSearch').on('click.closeSearch', function(){
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
			}
			else{
				self.removeClass('active');
				popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
				popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					popupSearch.removeClass('animated fadeOut').addClass('hidden');
					popupSearch.overlay.remove();
					popupSearch.overlay = $();
					popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
				win.off('resize.inputSearch');
			}
		});

		login.children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e){
			e.preventDefault();
			menuBarItems.eq(2).children('a').trigger('click.showLoginPopup');
		});
		navbar.off('click.showMenuT').on('click.showMenuT', function(e){
			e.preventDefault();
			e.stopPropagation();
			var popupSeatSelect = $('[data-infomations-1]'),
					popupSeatChange = $('[data-infomations-2]');
			if(popupSeatSelect.length) {
				popupSeatSelect.find('.tooltip__close').trigger('click');
			}
			if(popupSeatChange.length) {
				popupSeatChange.find('.tooltip__close').trigger('click');
			}
			if(isAnimate){
				return;
			}
			var langToolbarEl = $('.toolbar--language');
			isAnimate = true;
			if(menuT.hasClass('active')){
				resetMenuT();
			}
			else{
				var winH = window.screen.height + 1 || $(window).height();
				menuT.css({
					// 'top': langToolbarEl.height(),
					'height': winH - langToolbarEl.height()
				});
				openMenuT();
			}
		});
	};


	var loggedProfile = function(){
		if(popupLoggedProfile.length){
			var triggerLoggedProfile = menuBar.find('ul a.status');
			// var timerLoggedProfileResize = null;
			popupLoggedProfile.removeClass().addClass('menu-sub hidden');

			popupLoggedProfile.css('top', -menuBar.find('.logged-in').offset().top);

			triggerLoggedProfile.off('click.showLoggedProfile').on('click.showLoggedProfile', function(e){
				e.preventDefault();
				e.stopPropagation();
				detectHidePopup();
				// hidePopupSearch(true);

				//var self = $(this);
				// if(popupLoggedProfile.hasClass('hidden')){
				// 	self.addClass('active');
				// 	var popupLoggedProfilePosition = function(){
				// 		popupLoggedProfile.removeClass('hidden').css({
				// 			left: triggerLoggedProfile.offset().left - popupLoggedProfile.width()/2 + popupLoggedProfile.width()/2 - 30,
				// 			top: triggerLoggedProfile.offset().top + triggerLoggedProfile.innerHeight() + 21
				// 		});
				// 	};
				// 	popupLoggedProfilePosition();
				// 	popupLoggedProfile.addClass('animated fadeIn');
				// 	popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body).css('opacity', 0.8).addClass('animated fadeInOverlay').show();
				// 	popupLoggedProfile.triggerPopup = self;

				// 	// win.off('resize.LoggedProfile').on('resize.LoggedProfile', function(){
				// 	// 	clearTimeout(timerLoggedProfileResize);
				// 	// 	timerLoggedProfileResize = setTimeout(function(){
				// 	// 		popupLoggedProfilePosition();
				// 	// 		if(win.width() < config.tablet - config.width.docScroll){
				// 	// 			self.removeClass('active');
				// 	// 			popupLoggedProfile.addClass('hidden');
				// 	// 			popupLoggedProfile.overlay.remove();
				// 	// 			win.off('resize.LoggedProfile');
				// 	// 		}
				// 	// 	}, 100);
				// 	// });
				// 	popupLoggedProfile.overlay.off('click.closeLoggedProfile').on('click.closeLoggedProfile', function(e){
				// 		e.preventDefault();
				// 		e.stopPropagation();
				// 		triggerLoggedProfile.trigger('click.showLoggedProfile');
				// 	});
				// }
				// else{
				// 	self.removeClass('active');
				// 	popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
				// 	popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				// 	popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				// 		popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
				// 		popupLoggedProfile.overlay.remove();
				// 		popupLoggedProfile.overlay = $();
				// 		popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				// 	});
				// 	win.off('resize.LoggedProfile');
				// }

				var langToolbarH = 0;

				if(popupLoggedProfile.hasClass('hidden')){
					var langToolbarEl = $('.toolbar--language');
							// [old code] langToolbarH = 0;
					if(langToolbarEl.length) {
						langToolbarH = langToolbarEl.height();
					}
					popupLoggedProfile.removeClass('hidden');
					mainMenu.addClass('active');
					mainMenu.parent().scrollTop(0);
					// popupLoggedProfile.css('top', - mainMenu.height());
					// popupLoggedProfile.css('top', '').closest('.logged').css('margin-top', -mainMenu.height());
					menuBar.addClass('active');
					popupLoggedProfile.css('top', 18 + langToolbarH - menuBar.find('.logged-in').offset().top);
				}
				else{
					// popupLoggedProfile.addClass('hidden').closest('.logged').css('margin-top', '');
					mainMenu.removeClass('active').removeClass('hidden');
					menuBar.removeClass('active');
				}

				// [old code] menuT.find('.menu-inner').css('overflowY', 'hidden');

				var menuHeight = win.height() - langToolbarH;
				if ($('.logged-in').find('.menu-sub-outer').height() < menuHeight) {
					menuT.find('.menu-inner').css('overflow', 'hidden');
				}
			});

			popupLoggedProfile.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
				e.preventDefault();
				if (window.Modernizr.cssanimations) {
					// popupLoggedProfile.closest('.logged').addClass('no-transition').css('margin-top', 0);
					// popupLoggedProfile.addClass('no-transition').css('margin-top', -popupLoggedProfile.closest('.logged').offset().top);
					setTimeout(function(){
						popupLoggedProfile.addClass('hidden').removeClass('no-transition').css('margin-top', '');
						popupLoggedProfile.closest('.menu-item').removeClass('active');
						popupLoggedProfile.closest('.logged').removeClass('no-transition');
					}, 5 /*500*/);
					mainMenu.removeClass('active');
					menuBar.removeClass('active');
					menuT.find('.menu-inner').css('overflowY', 'auto');
				}
			});
		}
	};

	// checkAllList
	var checkAllList = function (trigger, wrapper, callback) {
		var checkAll = function () {
			var checkall = true;
			if(callback){
				callback(wrapper.find(':checkbox:checked').length);
			}
			wrapper.find(':checkbox').not('[disabled]').each(function () {
				if (!$(this).is(':checked')) {
					checkall = false;
					return;
				}
			});
			if(!wrapper.find(':checkbox').not('[disabled]').length){
				checkall = false;
			}
			return checkall;
		};

		trigger.off('change.checkAllList').on('change.checkAllList', function () {
			if ($(this).is(':checked')) {
				wrapper.find(':checkbox').not('[disabled]').each(function () {
					$(this).prop('checked', true);
				});
			} else {
				wrapper.find(':checkbox').not('[disabled]').each(function () {
					$(this).prop('checked', false);
				});
			}
			if(callback){
				callback($(this).is(':checked'));
			}
		});

		// wrapper.find(':checkbox').each(function () {
		// 	var self = $(this);
		// 	self.off('change.checkAllList').on('change.checkAllList', function () {
		// 		if (checkAll()) {
		// 			trigger.prop('checked', true);
		// 		} else {
		// 			trigger.prop('checked', false);
		// 		}
		// 	});
		// });
		wrapper.on('change.checkAllList', ':checkbox', function() {
			if (checkAll()) {
				trigger.prop('checked', true);
			} else {
				trigger.prop('checked', false);
			}
		});
	};
	//publick checkAllList function
	vars.checkAllList = checkAllList;

	// var loader = function(){
	// 	var imgs = $('body img').not('.img-loading'),
	// 		totalImage = imgs.length,
	// 		count = 0;
	// 	imgs.each(function(){
	// 		var img = new Image();
	// 		img.onload = function(){
	// 			count++;
	// 			if(count >= totalImage){
	// 				SIA.preloader.hide();
	// 			}
	// 		};
	// 		img.src = this.src;
	// 	});
	// };

	var initTooltip = function(){
		if($('[data-tooltip]')){
			$('[data-tooltip]').each(function(){
				if(!$(this).data('kTooltip')){
					$(this).kTooltip();
				}
			});
		}
	};

	// var addClassForWindowPhone = function(){
		// if(window.navigator.msMaxTouchPoints){
		// 	body.addClass('windows-phone');
		// }
	// };

	var initClearText = function(){
		$('input:text, input[type="tel"]').not('[data-start-date], [data-return-date], [data-oneway], [readonly]').addClear();
	};

	var triggerValidateByGroup = function() {
		// $('[data-rule-require_from_group]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group');
		// 	var elements = $(data[1]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });
		// $('[data-rule-require_from_group_if_check]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group_if_check');
		// 	var elements = $(data[3]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });

		// $('form').on('submit.validatedOnce', function() {
		// 	$(this).data('validatedOnce', true);
		// });

		$('[data-rule-validatedate]').each(function() {
			var self = $(this);
			var data = self.data('rule-validatedate');
			var elements = $(data.join(','));
			elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
				var el = $(this);
				var validator = el.closest('form').data('validator');
				// var validatedOnce = el.closest('form').data('validatedOnce');
				if(validator) {
					// validator.element(self);
					if(!el.is(':hidden') && !el.hasClass('ignore-validate')){
						validator.element(el);
					}
				}
			});
		});
		$('[data-rule-requiredignorable]').each(function(i, it) {
			var self = $(it);
			var form = self.closest('form');
			var dataRule = self.data('rule-requiredignorable');
			var ignoreCheckbox = dataRule[1].replace(':checked', '');
			$(ignoreCheckbox).off('change.ignore-' + i).on('change.ignore-' + i, function () {
				// if(form.data('validator') && form.data('validatedOnce')) {
				if(form.data('validator')) {
					form.data('validator').element(self);
				}
			});
		});

		var vldDate = $('[data-rule-validatedate]');
		if(vldDate.length){
			vldDate.each(function(){
				var self = $(this);
				var ar = self.data('rule-validatedate');
				// var formValidated = self.closest('form');
				$(ar[0]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function(){
					$(ar[1]).valid();
					$(ar[2]).valid();
					// if(formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[1]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function(){
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if(formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function(){
					$(ar[0]).valid();
					$(ar[1]).valid();
					/*if(formValidated.data('validatedOnce')) {
					}*/
				});
			});
		}

		var vldPp = $('[data-rule-checkpassport]');
		if(vldPp.length){
			vldPp.each(function(){
				var self = $(this);
				var ar = self.data('rule-checkpassport')[1].split(',');
				// var formValidated = self.closest('form');
				$(ar[1]).off('change.triggerByGroupValidate').on('change.triggerByGroupValidate', function(){
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if(formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('change.triggerByGroupValidate').on('change.triggerByGroupValidate', function(){
					$(ar[0]).valid();
					$(ar[1]).valid();
					// if(formValidated.data('validatedOnce')) {
					// }
				});
			});
		}
	};

	var triggerAutocomplete = function() {
		$('[data-autocomplete] input').not(':submit').off('click.autocomplete').on('click.autocomplete', function() {
			if($(this).is(':focus')) {
				$(this).autocomplete('search', '');
			}
		});
	};

	var autoCompleteFieldDealKeyboard = function() {
		var originalWidth = $(window).width();
		var setPosition = function() {
			var focusAutoCompleteField = $('[data-autocomplete] input:focus:visible').not(':submit');
			if(focusAutoCompleteField.length) {
				var dropdown = focusAutoCompleteField.autocomplete('widget');
				window.setTimeout(function() {
					dropdown.position({
						my: 'left top',
						at: 'left bottom',
						of: focusAutoCompleteField
					});
				}, 200);
			}

			var focusDatepickerField = $('.hasDatepicker:focus:visible');
			if(focusDatepickerField.length) {
				var datepickerDrop = focusDatepickerField.datepicker('widget');
				window.setTimeout(function() {
					datepickerDrop.position({
						my: 'left top',
						at: 'left bottom',
						of: focusDatepickerField
					});
				}, 200);
			}
		};
		$(window).on('resize.dealWithKeyboard', function() {
			if($(window).width() === originalWidth) {
				//open keyboard
				setPosition();
			}
			else {
				//rotation
				originalWidth = $(window).width();
			}
		}).on('scroll.dealWithKeyboard', function() {
			setPosition();
		});
	};

	var iOsKeyboardTabIndexDealing = function() {
		var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
		if(!iOS) {
			return;
		}
		var allInputs = $(':input');
		var numInputs = allInputs.length;

		// Update the tab indexes when an input is focused.
		$('form').on('focus', ':input', function() {
			var activeForm = $(this).closest('form');
			var activeFormInputs = activeForm.find(':input');

			// Make the inputs on all inactive forms negative.
			$.each(allInputs, function(i) {
				var parentForm = $(this).closest('form');

				if (parentForm !== activeForm) {
					$(this).attr('tabindex', -(numInputs - i));
				}
			});

			// This form is active; use positive tab indexes.
			$.each(activeFormInputs, function(i) {
				$(this).attr('tabindex', ++i);
			});
		});

	};

	var initTriggerPopup = function(){
		var triggerPopup = $('[data-trigger-popup]');
		var flyingFocus = $('#flying-focus');

		triggerPopup.each(function(){
			var self = $(this);
			if(typeof self.data('trigger-popup') === 'boolean'){
				return;
			}
			var popup = $(self.data('trigger-popup'));
			if(!popup.data('Popup')){
				popup.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]',
					afterShow: function(){
						flyingFocus = $('#flying-focus');
						if(flyingFocus.length){
							flyingFocus.remove();
						}
					}
				});
			}
			self.off('click.showPopup').on('click.showPopup', function(e){
				e.preventDefault();
				if(!self.data('isDisabledStopIm')) {
					e.stopImmediatePropagation();
				}
				if(!self.hasClass('disabled')){
					popup.Popup('show');
				}
			});
		});
	};

	var loginFormValidation = function() {
		$('.form--login').each(function(){
			$(this).validate({
				focusInvalid: true,
				errorPlacement: vars.validateErrorPlacement,
				success: vars.validateSuccess
			});
		});
	};

	var globalFormValidation = function() {
		$('[data-global-form-validate]').validate({
			focusInvalid: true,
			errorPlacement: vars.validateErrorPlacement,
			success: vars.validateSuccess,
			onfocusout: vars.validateOnfocusout,
			invalidHandler: vars.invalidHandler
		});
	};

	var orientationChangeTweak = function() {
		var originalWidth = $(window).width();
		$(window)
		.off('resize.tweak-orientation-change')
		.on('resize.tweak-orientation-change', function() {
			if($(this).width() !== originalWidth) {
				originalWidth = $(this).width();
				// $('.hasDatepicker').datepicker('hide');
				$('#ui-datepicker-div').hide();
				$('[data-autocomplete] input').not(':submit').autocomplete('close');
				// var focusedInput = $('input:focus');
				// focusedInput.blur();
				$('body').prepend('<input type="text" readonly style="position: fixed; z-index: -1; top: 50%; left: 0; width: 0; height: 0; opacity:0;" id="fake-focusable-element" />');
				$('#fake-focusable-element').focus();
				setTimeout(function() {
					$('#fake-focusable-element').remove();
				}, 3000);
			}
		});
	};

	var ieMobileReadonlyInputs = function() {
		if(/IEMobile/.test(window.navigator.userAgent)) {
			$(document)
			.off('focus.readonly')
			.on('focus.readonly', 'input[readonly][placeholder]', function() {
				if(!$(this).is('[type="password"]')) {
					var val = $.trim($(this).val());
					if(!val.length) {
						$(this).val($(this).attr('placeholder'));
					}
				}
			})
			.off('blur.readonly')
			.on('blur.readonly', 'input[readonly][placeholder]', function() {
				if(!$(this).is('[type="password"]')) {
					var val = $(this).val();
					if(val === $(this).attr('placeholder')) {
						$(this).val('');
					}
				}
			});
		}
	};

	// init infor KF
	var intMastheadSlider = function(){
		var masthead = $('.masthead');
		if(masthead.length){
			var flexslider1 = $('.flexslider-1', masthead);
			flexslider1.find('.slides').slick({
				siaCustomisations: true,
				dots: true,
				speed: 300,
				draggable: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				accessibility: false,
				arrows: false,
				infinite: false,
				customPaging: function() {
					return '<a href="#" class="ico-point-r">&nbsp;</a>';
				}
			});
			var findMaxHeight = function(listElm){
				var max = 0;
				listElm.each(function(){
					if(max < $(this).outerHeight()){
						max = $(this).outerHeight();
					}
				});
				return max;
			};
			win.off('resize.masthead').on('resize.masthead', function(){
				var slideItems = flexslider1.find('.slide-item');
				var slideItemMaxHeight = findMaxHeight(slideItems);
				slideItems.css('height',slideItemMaxHeight);
			}).trigger('resize.masthead');

		}
	};

	var initDisabledLink = function() {
		var disabledLink = $('[data-disabled-link]');
		if(disabledLink.length) {
			disabledLink.off().on('click', function(e) {
				if($(this).hasClass('disabled')) {
					e.preventDefault();
					e.stopImmediatePropagation();
				}
			});
		}
	};

	// var initAutocompleteSearch = function() {
	// 	var term = '';
	// 	$('.form-search')
	// 		.off('blur.closeSuggest').on('blur.closeSuggest', 'input:text', function() {
	// 			$(this).data('ui-autocomplete').menu.element.hide();
	// 		})
	// 		.each(function() {
	// 			$(this).find('input:text').autocomplete({
	// 				source: function(request, response) {
	// 					term = request.term;
	// 					$.get('ajax/autocomplete-search.json', function(data) {
	// 						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
	// 						var source = $.grep(data.searchSuggests, function(item){
	// 							return matcher.test(item);
	// 						});
	// 						response(source);
	// 					});
	// 				},
	// 				focus: function() {
	// 					var ulMenu = $(this).autocomplete('widget');
	// 					ulMenu.children('li').removeClass('active');
	// 					ulMenu.find('.ui-state-focus').parent().addClass('active');
	// 				}
	// 			}).data('ui-autocomplete')._renderItem = function(ul, item) {
	// 				if (!ul.hasClass('auto-suggest')) {
	// 					ul.addClass('auto-suggest');
	// 				}
	// 				var termLength = term.length;
	// 				var label = item.label;
	// 				var liContent = '<span class="text-suggest">' + label.substr(0, termLength) +
	// 					'</span>' + label.substr(termLength, label.length - 1);
	// 				return $('<li>').append($('<a>').html(liContent)).appendTo(ul);
	// 			};
	// 		});
	// };


	var globalFun = function(){
		animationNavigationOnDesktop();
		selectLanguage();
		loginPopup();
		loggedProfile();
		menuTablet();
		// loader();
		initTooltip();
		addClassForSafari();
		addClassForIosWindowPhoneAndroid();
		initClearText();
		// addClassForWindowPhone();
		triggerValidateByGroup();
		triggerAutocomplete();
		autoCompleteFieldDealKeyboard();
		iOsKeyboardTabIndexDealing();
		initTriggerPopup();
		loginFormValidation();
		orientationChangeTweak();
		ieMobileReadonlyInputs();
		intMastheadSlider();
		globalFormValidation();
		initDisabledLink();

		// initAutocompleteSearch();
	};

	// it is used on mobile
	var backgroundImage = function(){
		var prImgs = $('#banner-slider .slide-item');

		if (!(vars.isIEMobile() && window.darkSite)) {
			prImgs = prImgs.add($('[data-assign-background] .full-banner--img'));
		}

		prImgs.each(function() {
			var prImg = $(this);
			prImg.css({
				'background-image': 'url(' + prImg.find('img').attr('src') + ')'
			});

			prImg.find('img').attr('src', config.imgSrc.transparent);

			if(vars.detectDevice.isTablet() && prImg.data('tablet-bg')){
				prImg.css({
					'background-position': prImg.data('tablet-bg')
				});
			}

			// if(vars.detectDevice.isMobile() && prImg.data('mobile-bg')){
			if(prImg.data('mobile-bg')){
				prImg.css({
					'background-position': prImg.data('mobile-bg')
				});
			}

			if (prImg.is('.full-banner--img') && prImg.hasClass('visibility-hidden')) {
				prImg.removeClass('visibility-hidden');
			}
		});
	};

	$.fn.resetForm = function() {
		return this.each(function() {
			// $(this).data('validatedOnce', null);
			$(this).find('.error:input').parents('.error').removeClass('error');
			$(this).find('.text-error').remove();
		});
	};
	// vars.validationRuleByGroup = [];
	vars.validateErrorPlacement = function(error, element) {
		// if(!$(element).closest('form').data('validatedOnce')) { return; }
		if (element.is(':disabled')) { return; }
		// if(!$(element).closest('form').data('validatedOnce')) { return; }
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		var inputsInGroup = group.find(':input:visible').filter(function() {
			return (jQuery.isEmptyObject($(this).rules()) === false);
		});
		var validator = $(element).closest('form').data('validator');
		var validationRuleByGroup = vars.validationRuleByGroup;
		var errorList = validator.errorList;

		var appendError = function(container) {
			container.addClass('error');

			// fix issue: elements are not required validation in form-group
			var ignoreEl = container.find('[data-ignore]');
			if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
				ignoreEl.closest('.grid-col').addClass('ignore');
			}

			if(!container.children('.text-error').length && error.text()) {
				$(config.template.labelError)
				.appendTo(container)
				.find('span')
				.text(error.text());

				$(element).data('errorMessage', container.children('.text-error'));
			}
			else {
				if (error.text()) {
					container.children('.text-error').find('span').text(error.text());
				}
				else {
					container.children('.text-error').remove();
				}
			}
		};

		var theRestIsOk = function() {
			return inputsInGroup.not($(element)).filter(function() {
				return (validator.check($(this)) === false);
			}).length === 0;
		};

		//If it's not validating by group
		if(!isValidateByGroup) {
			appendError(gridInner);

			//check if the current rule is a rule-by-group
			var isRuleByGroup = false;
			if(validationRuleByGroup) {
				for (var i = errorList.length - 1; i >= 0; i--) {
					if($(element).is(errorList[i].element) && validationRuleByGroup.indexOf(errorList[i].method) >= 0) {
						isRuleByGroup = true;
						break;
					}
				}
			}

			//if current rule is a rule-by-group and it's fail
			if(isRuleByGroup) {
				//and the rest input in group are ok
				if(theRestIsOk()) {
					//remove old error message in group if exists
					if(group.children('.text-error').length) {
						group.children('.text-error').remove();
					}
					gridInner.removeClass('error');
					//append new error message to group
					group.append($(element).data('errorMessage')).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}
				}
				else {
					//if the rest input are not ok
					//just show separate error and temporarily hide group error
					gridInner.removeClass('error').children('.text-error').remove();
				}
			}
			else {
				//if it's another separate error
				if(error.text() !== '') {
					//remove group error
					if(inputsInGroup.length > 1) {
						group.removeClass('error').children('.text-error').remove();

						// fix issue: elements are not required validation in form-group
						var ignoreEl = group.find('[data-ignore]');
						if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
							ignoreEl.closest('.grid-col').removeClass('ignore');
						}
					}
				}
				else {
					//if it's ok, need to detect it's ok for rule-group or rule-element
					for (var z = validationRuleByGroup.length - 1; z >= 0; z--) {
						if($(element).rules()[validationRuleByGroup[z]]) {
							group.removeClass('error').children('.text-error').remove();

							// fix issue: elements are not required validation in form-group
							var ignoreEl = group.find('[data-ignore]');
							if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
								ignoreEl.closest('.grid-col').removeClass('ignore');
							}

							break;
						}
					}
				}
			}
		}
		else {
			//validating by group
			appendError(gridInner);
			if(inputsInGroup.index($(element)) === 0) {
				if(!$(element).data('elementValidator')) {
					$(element).data('elementValidator', $.extend({}, validator));
				}
			}
			if(!$(element).data('being-validated')) {
				inputsInGroup.data('being-validated', true);
				inputsInGroup.each(function() {
					inputsInGroup.eq(0).data('elementValidator') && inputsInGroup.eq(0).data('elementValidator').element($(this));
				});
				inputsInGroup.data('being-validated', false);
				group.children('.text-error').remove();
				var firstError = group.find('.text-error').filter(function() {
					return ($(this).find('span').text() !== '');
				}).first();

				if(firstError.length) {
					group.append(firstError).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}

					group.find('.text-error').not(firstError).remove();
				}
				else {
					group.removeClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').removeClass('ignore');
					}

					group.find('.error:not(:input)').removeClass('error');
				}
			}
		}
	};

	vars.validateSuccess = function(label, element) {
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		// var inputsInGroup = group.find(':input');
		// var validator = $(element).closest('form').data('validator');

		var removeMessage = function(container) {
			container.removeClass('error');
		};

		//If it's not validating by group
		if(!isValidateByGroup) {
			removeMessage(gridInner);
		}
	};

	vars.validateOnfocusout = function(element) {
		var formEl = $(element).closest('form'),
				validator = formEl.data('validator');
		var container = $(element).closest('.error');
		if(container.data('validate-by-group')) {
			if(container.find('.error:input').not($(element)).length) {
				return;
			}
		}
		// if(formEl.data('validatedOnce')) {
		// }

		if(vars.validationRuleByGroup) {
			var group = $(element).closest('.form-group, [data-validate-row]'),
					strRules = '';
			for(var i = vars.validationRuleByGroup.length - 1; i >= 0; i--) {
				strRules += '[data-rule-' + vars.validationRuleByGroup[i] + ']' + (i !== 0 ? ',' : '');
			}
			if(strRules) {
				var ruleEl = group.find(strRules);
				if(ruleEl.length) {
					// ruleEl.trigger('change');
					validator.element(element);
					validator.element(ruleEl);
					return;
				}
			}
		}

		if(this.element(element) && !this.checkable(element)) {
			$(element)
			.closest('.error').removeClass('error').addClass('success')
			.children('.text-error').remove();
		}
		if(this.element(element) && !this.checkable(element)) {
			$(element)
			.closest('.error').removeClass('error').addClass('success')
			.children('.text-error').remove();
		}
	};

	vars.invalidHandler = function(form, validator) {
		var errors = validator.numberOfInvalids();
		if (errors) {
			var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
			if(errorColEl.length) {
				win.scrollTop(errorColEl.offset().top - 40);
			}
		}
	};

	return {
		vars: vars,
		config: config,
		globalFun: globalFun,
		backgroundImage: backgroundImage
	};
})();

SIA.preloader = (function () {
	var preload = $('.overlay-loading');
	var show = function(callback) {
		preload.removeClass('hidden');
		if(typeof(callback) === 'function') {
			callback();
		}
	};

	var hide = function(callback) {
		if (!preload.data('never-stop')) {
			if(preload.data('planloading') !== undefined) {
				SIA.planLoading.goEnd();
			} else {
				preload.addClass('hidden');
			}
			preload.trigger('afterHide.ready');
		}
		if (typeof(callback) === 'function') {
			callback();
		}
	};

	var isVisible = function() {
		return preload.is(':visible');
	};

	return {
		show: show,
		hide: hide,
		isVisible: isVisible
	};
})();

SIA.planLoading = (function() {

	var interBlock = $('.interstitial-block'),
			interEl = $('.interstitial-2'),
	    startPoint = interEl.data('start'),
	    endPoint = interEl.data('end'),
	    listCircle = $('.rotate-circle'),
      planDeg = 23, // plus a deg og planet
	    staDeviation = 11,
	    cungDeg = 2.75, // minus a deg of the point
	    originalDeg = (180 - (staDeviation * 2)) / 5 + cungDeg,
	    startDeg = 90 + planDeg + (originalDeg * startPoint),
	    endDeg = 90 + (originalDeg * endPoint ) + cungDeg,
	    endDeg1 = endDeg - 0.2 * (endDeg - startDeg),
	    win = $(window);

	var activeCircle = function(index, callback) {

		listCircle.eq(index).addClass('active').css('z-index', 2);

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var init = function(callback) {


		listCircle.each(function(index){
			  var _self = $(this),
			      _selfDeg = 90 + (_self.data('step') * (originalDeg + cungDeg));
			  _self.css({
			  	'transform' : 'rotate('+_selfDeg+'deg)',
				});

				_self.attr('data-degree', _selfDeg);

				if(endPoint - startPoint < 2) {
				  if(index < startPoint) {
				  	_self.addClass('active');
				  }
				}
		})

		if(endPoint - startPoint < 2) {

			interEl.css({'transform' : 'rotate('+ startDeg +'deg)'});

			setTimeout(function(){
			  interEl.css({
			  'transition': 'transform 1s linear'
			  });
			},1);

			setTimeout(function(){
			  interEl.css({
			  	'transform' : 'rotate('+ endDeg1 +'deg)'});
			},2);

		} else {
			listCircle.css('z-index', 0);
		  interEl.animate({
		  	deg: startDeg
	  	}).animate({
	    	deg: endDeg1
	  	}, {
		  duration: 3000,
		  easing: "swing",
	  	step: function(now){
	  		interEl.css({
          'transform':'rotate('+now+'deg)'
        });

        if(now > $('[data-step="1"]').data('degree') + 13) {
        	activeCircle(0);
        }
        if(now > $('[data-step="2"]').data('degree') + 13) {
        	activeCircle(1);
        }
        if(now > $('[data-step="3"]').data('degree') + 13) {
        	activeCircle(2);
        }
        if(now > $('[data-step="4"]').data('degree') + 13) {
        	activeCircle(3);
        }
	  	 }
		 })
		}

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var goEnd = function(callback) {

		if(endPoint - startPoint < 2) {
			interEl.css({'transform' : 'rotate('+endDeg+'deg)'});
			setTimeout(function(){
				 	$('.overlay-loading').addClass('hidden');
			},1000);
		} else {
			interEl.animate({
			  	deg: endDeg1
		  	}).animate({
		    	deg: endDeg
		  	}, {
			  duration: 1000,
			  easing: "swing",
		  	step: function(now){
		  		interEl.css({
	          'transform':'rotate('+now+'deg)'
	        });
	        if(now > $('[data-step="1"]').data('degree') + 13) {
	        	activeCircle(0);
	        }
	        if(now > $('[data-step="2"]').data('degree') + 13) {
	        	activeCircle(1);
	        }
	        if(now > $('[data-step="3"]').data('degree') + 13) {
	        	activeCircle(2);
	        }
	        if(now > $('[data-step="4"]').data('degree') + 13) {
	        	activeCircle(3);
	        }
		  	 },
		  	 complete: function(){
		  	 	$('.overlay-loading').addClass('hidden');
		  	 }
			 })
		}

		if (typeof(callback) === 'function') {
			callback();
		};

	};

	var rePosition = function(callback) {

		if (typeof(callback) === 'function') {
			callback();
		};

	}

	return {
		init: init,
		goEnd: goEnd,
		rePosition: rePosition
	};

})();

(function($) {

	SIA.planLoading.init();

	$(document).ready(function() {
		// SIA.global = global();
		// SIA.preloader = preloader();
		var body = $(document.body);

		var loadScript = function (url,callback,location){
			if(!url || (typeof url !== 'string')){
				return;
			}
			var script = document.createElement('script');
			//if this is IE8 and below, handle onload differently
			if(typeof document.attachEvent === 'object'){
				script.onreadystatechange = function(){
					//once the script is loaded, run the callback
					if(script.readyState === 'loaded' || script.readyState === 'complete'){
						if(callback){
							callback();
						}
					}
				};
			} else {
				//this is not IE8 and below, so we can actually use onload
				script.onload = function(){
					//once the script is loaded, run the callback
					if (callback){
						callback();
					}
				};
			}
			//create the script and add it to the DOM
			script.src = url;
			document.getElementsByTagName(location ? location : 'head')[0].appendChild(script);
		};

		var urlScript = {
			scripts: {
				'underscore': 'scripts/underscore.js',
				'jqueryui': 'scripts/jquery-ui-1.10.4.custom.js',
				'validate': 'scripts/jquery.validate.js',
				'slick': 'scripts/slick.js',
				'mousewheel': 'scripts/jquery.mousewheel.js',
				'jscrollpane': 'scripts/jquery.jscrollpane.js',
				'inputmask': 'scripts/jquery.inputmask.js',
				'accounting': 'scripts/accounting.min.js',
				'placeholder': 'scripts/jquery.placeholder.js',
				'transit': 'scripts/jquery.transit.js',
				'cleartext': 'scripts/clear-text.js',
				'seatmap': 'scripts/seat-map.js',
				'chart': 'scripts/chart.js',
				'highcharts': 'scripts/highcharts.js',
				'grayscale': 'scripts/grayscale.js',
				'isotope': 'scripts/isotope.js',
				'shuffle': 'scripts/jquery.shuffle.js',
				'youtubeAPI': 'https://www.youtube.com/iframe_api',
				'flowplayerAPI': '//releases.flowplayer.org/6.0.1/flowplayer.min.js',
				'tiff': 'scripts/tiff.min.js',
				'recaptcha': 'https://www.google.com/recaptcha/api.js'
			},
			modules: {
				'flightTableBorder': 'scripts/flight-table-border.js',
				'forceInput': 'scripts/force-input.js',
				'multipleSubmit': 'scripts/multiple-submit.js',
				'contact': 'scripts/contact.js',
				'simpleSticky': 'scripts/simple-sticky.js',
				'coachBeta': 'scripts/coach-beta.js',
				'sticky': 'scripts/sticky.js',
				'bar': 'scripts/bar.js',
				'filterEntertainment': 'scripts/filter-entertainment.js',
				'tab': 'scripts/tab.js',
				'accordion' : 'scripts/accordion.js',
				'countCharsLeft' : 'scripts/count-chars-left.js',
				'addon' : 'scripts/add-on.js',
				'atAGlance' : 'scripts/at-a-glance.js',
				'initAutocompleteCity' : 'scripts/autocomplete-city.js',
				'autocompleteAirport': 'scripts/autocomplete-airport.js',
				'autocompleteCar': 'scripts/autocomplete-car.js',
				'bookingSummnary' : 'scripts/booking-summary.js',
				'CIBBookingSummary' : 'scripts/cib-booking-summary.js',
				'cibConfirmation' : 'scripts/cib-confirmation.js',
				'KFClaimMissingMiles' : 'scripts/claim-missing-miles.js',
				'KFMileExpiring' : 'scripts/expiring-miles.js',
				'KFFavourite' : 'scripts/favourites.js',
				'flightCalendar' : 'scripts/flight-calendar.js',
				'flightSchedule' : 'scripts/flight-schedule.js',
				'flightSearchCalendar' : 'scripts/flight-search-calendar.js',
				'flightSelect' : 'scripts/flight-select.js',
				'flightStatus' : 'scripts/flight-status.js',
				'home' : 'scripts/home.js',
				'darkSiteHome': 'scripts/dark-site-home.js',
				'KFMileHowToEarn' : 'scripts/how-to-earn.js',
				'KFMileHowToUse' : 'scripts/how-to-use.js',
				'flightScheduleTrip' : 'scripts/init-flight-schedule-trip.js',
				'initPersonTitles' : 'scripts/init-personTitles.js',
				'initTabMenu' : 'scripts/init-tabMenu.js',
				'KFCheckIns' : 'scripts/kf-check-ins.js',
				'KFFlightHistory' : 'scripts/kf-flight-history.js',
				'KFMessageForward' : 'scripts/kf-message-forward.js',
				'KFMessage' : 'scripts/kf-message.js',
				'KFPartnerProgramme' : 'scripts/kf-partner-programme.js',
				'KFPersonalDetail' : 'scripts/kf-personal-detail.js',
				'KFRedemptionNominee' : 'scripts/kf-redemption-nominee-add.js',
				'KFTicketReceipt' : 'scripts/kf-tickets-and-receipts.js',
				'KFUpcomingFlights' : 'scripts/kf-upcoming-flights.js',
				'multicity' : 'scripts/multicity.js',
				'newsTickerContent' : 'scripts/newsTickerContent.js',
				'orbConfirmation' : 'scripts/orb-confirmation.js',
				'ORBFlightSelect' : 'scripts/orb-flight-search.js',
				'passengerDetail' : 'scripts/passenger-detail.js',
				'payment' : 'scripts/payment.js',
				'promotionKrisflyer' : 'scripts/promotion-krisflyer.js',
				'promotion' : 'scripts/promotion.js',
				'KFRedeemMiles' : 'scripts/redeem-miles.js',
				'roundTrip' : 'scripts/round-trip.js',
				'selectCabin' : 'scripts/select-cabin.js',
				'selectReturnDate' : 'scripts/select-return-date.js',
				'KFStatement' : 'scripts/statements.js',
				'stickySidebar' : 'scripts/sticky-sidebar.js',
				// 'FormCheckChange' : 'scripts/form-check-change.js',
				'ORBBookingSummary': 'scripts/orb-booking-summary.js',
				'initAutoFilledPassenger': 'scripts/init-auto-filled-passenger.js',
				'initCorrectDate': 'scripts/init-corect-date.js',
				'kfRegistration': 'scripts/kf-registration.js',
				'sqcRegistration': 'scripts/sqc-registration.js',
				'specialAssistance': 'scripts/special-assistance.js',
				'manageBooking': 'scripts/manage-booking.js',
				'excessBaggage': 'scripts/excess-baggage.js',
				'mbConfirmation': 'scripts/mb-confirmation.js',
				'mbSelectMeal': 'scripts/mb-select-meals.js',
				'MBBookingSummary': 'scripts/mb-booking-summary.js',
				'SQCExpenditure': 'scripts/sqc-expenditure.js',
				'sqcUpcomingFlight': 'scripts/sqc-upcoming-flight.js',
				'SQCUser': 'scripts/sqc-user.js',
				'SQCAddUser': 'scripts/sqc-add-user.js',
				'sqcAtAGlance': 'scripts/sqc-at-a-glance.js',
				'SQCFlightHistory': 'scripts/sqc-flight-history.js',
				'sqcSavedTrips': 'scripts/sqc-saved-trips.js',
				'swipeDatepicker' : 'scripts/swipe-datepicker.js',
				'desWhereTo': 'scripts/des-where-to-stay.js',
				'desEntry': 'scripts/des-entry.js',
				'milestonesRewards': 'scripts/milestones-rewards.js',
				'DESCityGuide': 'scripts/des-city-guide.js',
				'staticContentKrisflyer': 'scripts/static-content-krisflyer.js',
				'staticContentGeneric': 'scripts/static-content-generic.js',
				'sshHotel': 'scripts/ssh-hotel.js',
				'sshSelection': 'scripts/ssh-selection.js',
				'sshAdditional': 'scripts/ssh-additional.js',
				'mbChangeFlight': 'scripts/mb-change-flight.js',
				'multiTabsWithLongText': 'scripts/multitabs-with-long-text.js',
				'staticContentHeritage': 'scripts/static-content-heritage.js',
				'moreInTheSection': 'scripts/more-in-the-section.js',
				'kfUnsubscribe': 'scripts/kf-unsubscribe.js',
				'feedBack': 'scripts/feed-back.js',
				'kfVoucherRedemption': 'scripts/kf-voucher-redemption.js',
				'landingSearchFlight': 'scripts/landing-search-flight.js',
				'youtubeApp': 'scripts/youtube-app.js',
				'flowPlayerApp': 'scripts/flowplayer-app.js',
				'darkSiteStatements': 'scripts/dark-site-statements.js',
				'grayscaleModule': 'scripts/grayscale-module.js',
				'autocompleteSearch': 'scripts/autocomplete-search.js',
				'faqs': 'scripts/faqs.js',
				'plusOrMinusNumber': 'scripts/plus-or-minus-number.js',
				'radioTabsChange': 'scripts/radio-tabs-change.js',
				'addBaggage': 'scripts/add-baggage.js',
				'scrollTop': 'scripts/scroll-top.js',
				'promotionsPackages': 'scripts/promotions-packages.js',
				'autocompleteCountryCity': 'scripts/autocomplete-country-city.js',
				'disableValue': 'scripts/disable-value.js',
				'togglePrivacy': 'scripts/toggle-privacy.js',
				'carouselFade': 'scripts/carousel-fade.js',
				'carouselSlide': 'scripts/carousel-slide.js',
				'pricePoints': 'scripts/price-points.js',
				'checkImage': 'scripts/check-image.js',
				'hotelListing': 'scripts/hotel-listing.js',
				'promotionComponent': 'scripts/promotion-component.js',
				'bookingWidget': 'scripts/booking-widget.js',
				'redemptionNominee': 'scripts/redemption-nominee.js',
				'bookingMileClaim': 'scripts/kf-retroactive-mile-claim.js',
				'googleMap': 'scripts/googlemap.js',
				'baiduMap': 'scripts/baidumap.js',
				'selectMeals': 'scripts/mb-flight-select-meal.js',
				'donateMiles': 'scripts/donate-miles.js',
				'voucherRewards': 'scripts/voucher-rewards.js',
				'voucherStored': 'scripts/voucher-cookies.js'
			},
			getScript: function(script) {
				return this.scripts[script];
			}
		};

		var comment = [
			SIA.global.globalFun,
			SIA.footer,
			SIA.cookiesUse,
			SIA.autoCompleteLanguage,
			SIA.highlightInput,
			SIA.fixPlaceholder,
			SIA.initCustomSelect,

			// set backgroundImage
			SIA.global.backgroundImage,
			// social
			SIA.socialNetworkAction,
			// sub menu on device for KF
			SIA.initSubMenu,
			// language toolbar
			SIA.initLangToolbar,
			// checkbox confirm
			SIA.initConfirmCheckbox,
			SIA.initToggleButtonValue
		];


		var initFunction = comment;
		var requireScript = ['jqueryui','mousewheel', 'validate', 'jscrollpane', 'cleartext', 'placeholder'];
		var concatInitFunctionDone = false;
		var totalLoadedModules = 0;
		var loadedModules = 0;
		var modulesToLoad = [];
		var dashboard = {
			page: {
				'flightTableBorder': [
					'flightTableBorder'
				],
				'forceInput': [
					'forceInput'
				],
				'multipleSubmit': [
					'multipleSubmit'
				],
				'contact': [
					'contact',
					'initAutocompleteCity'
				],
				'simpleSticky': [
					'simpleSticky'
				],
				'coachBeta': [
					'coachBeta'
				],
				'sticky': [
					'sticky'
				],
				'bar': [
					'bar'
				],
				'grayscaleModule': [
					'grayscaleModule'
				],
				'darkSiteStatements': [
					'darkSiteStatements'
				],
				'home': [
					'home',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					'autocompleteAirport',
					'autocompleteCar',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip',
					'swipeDatepicker'
				],
				'darkSiteHome': [
					'darkSiteHome',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip'
				],
				'passengerDetail': [
					'initPersonTitles',
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initAutocompleteCity',
					'initAutoFilledPassenger',
					'CIBBookingSummary'
				],
				'orbPassengerDetail': [
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initPersonTitles',
					'initAutocompleteCity',
					'ORBBookingSummary',
					'initAutoFilledPassenger'
				],
				'seatMap': [
					'stickySidebar',
					// 'initTabMenu',
					'CIBBookingSummary'
				],
				'flightStatus': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'flightStatus',
					// 'initTabMenu',
					'swipeDatepicker'
				],
				'flightSchedule': [
					'flightSchedule',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'initAutocompleteCity',
					'selectCabin',
					// 'initTabMenu',
					'swipeDatepicker'
				],
				'flightCalendar': [
					'flightCalendar',
					'CIBBookingSummary'
				],
				'promotion': [
					'promotion'
				],
				'faresDetailsPage': [
					'roundTrip',
					'swipeDatepicker',
					'selectCabin',
					'promotion'
				],
				'promotionKrisflyer': [
					'promotionKrisflyer'
				],
				'bookingSummnary': [
					'bookingSummnary',
					'initAutocompleteCity'
					// 'initTabMenu'
				],
				'flightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'flightSelect',
					'CIBBookingSummary',
					'swipeDatepicker'
				],
				'orbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'ORBBookingSummary',
					'swipeDatepicker'
				],
				'mbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'MBBookingSummary',
					'swipeDatepicker'
				],
				'mbFlightSelectSf': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'CIBBookingSummary',
					'swipeDatepicker'
				],
				'flightSearchCalendar': [
					'flightSearchCalendar'
				],
				'multicity': [
					'initAutocompleteCity',
					'selectReturnDate',
					'selectCabin',
					'multicity',
					'swipeDatepicker'
				],
				'payment': [
					'payment',
					'initAutocompleteCity',
					'CIBBookingSummary'
				],
				'orbPayment': [
					'payment',
					'initAutocompleteCity',
					'ORBBookingSummary'
				],
				'mbPayment': [
					'payment'
				],
				'addon': [
					'addon',
					'CIBBookingSummary',
					'googleMap',
					'baiduMap',
					'multiTabsWithLongText',
					'initTabMenu',
					'plusOrMinusNumber',
					'roundTrip',
					'autocompleteCar'
				],
				'orbAddOns': [
					'addon',
					'ORBBookingSummary'
				],
				'cibConfirmation': [
					'cibConfirmation',
					'CIBBookingSummary'
				],
				'atAGlance': [
					'atAGlance'
				],
				'kfRedemptionNominee': [
					'KFRedemptionNominee',
					'initPersonTitles',
					'initAutocompleteCity'
					// 'initCorectDate'
				],
				'kfPartnerProgramme': [
					'KFPartnerProgramme'
				],
				'accordion': [
					'accordion'
				],
				'countCharsLeft': [
					'countCharsLeft'
				],
				'howToEarn': [
					'initAutocompleteCity',
					'KFMileHowToEarn'
				],
				'howToUse': [
					'initAutocompleteCity',
					'KFMileHowToUse',
					'kfVoucherRedemption'
				],
				'redeemMiles': [
					'initAutocompleteCity',
					'KFRedeemMiles'
				],
				'kfExpiringMiles': [
					'KFMileExpiring'
				],
				'kfStatement': [
					'KFStatement'
				],
				'KFFavourite': [
					'initAutocompleteCity',
					'KFFavourite'
				],
				// 'milesPage': [
				// 	'initAutocompleteCity'
				// ],
				'kfMessage': [
					'KFMessage'
				],
				'kfMessageForward': [
					'KFMessageForward'
				],
				'kfPersonalDetail': [
					'KFPersonalDetail',
					'initAutocompleteCity',
					'initPersonTitles'
				],
				'kfUpcomingFlights': [
					'KFUpcomingFlights'
				],
				'kfClaimMissingMiles': [
					'KFClaimMissingMiles',
					'initAutocompleteCity'
				],
				'kfFlightHistory': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'swipeDatepicker',
					'KFFlightHistory'
				],
				'KFTicketReceipt': [
					'KFTicketReceipt'
				],
				'KFCheckIns' : [
					'KFCheckIns'
				],
				'ORBConfirmation': [
					'orbConfirmation',
					'ORBBookingSummary'
				],
				// 'FormCheckChange': [
				// 	'FormCheckChange'
				// ],
				'ORBFlightSchedule': [
					'flightSearchCalendar'
				],
				// 'KFCreateNewPin': [
				// 	'initCorectDate'
				// ],
				'KFRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'kfRegistration'
				],
				'SQCRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'sqcRegistration'
				],
				'specialAssistance': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'specialAssistance'
				],
				'cancelWaitlistBooking': [
					'initAutocompleteCity',
					'selectReturnDate',
					'swipeDatepicker'
				],
				'ManageBooking': [
					'manageBooking'
				],
				'ExcessBaggage': [
					'excessBaggage'
				],
				'MBConfirmation': [
					'mbConfirmation',
					'manageBooking'
				],
				'MBReview': [
					'manageBooking',
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'MBBookingSummary',
					'swipeDatepicker'
				],
				'MBSelectMeal': [
					'mbSelectMeal'
				],
				'MBSelectMealLand': [
					'accordion',
					'selectMeals',
					'multiTabsWithLongText'
				],
				'SQCExpenditure': [
					'SQCExpenditure'
				],
				'sqcUpcomingFlight': [
					'sqcUpcomingFlight'
				],
				'SQCUser': [
					'SQCUser'
				],
				'SQCAddUser': [
					'SQCAddUser',
					'initAutocompleteCity',
					'initPersonTitles'
					// 'initCorectDate'
				],
				'SQCAtAGlance': [
					'sqcAtAGlance'
				],
				'SQCFlightHistory': [
					'initAutocompleteCity',
					'SQCFlightHistory'
				],
				'SQCSavedTrips': [
					'sqcSavedTrips'
				],
				'initCorrectDate': [
					'initCorrectDate'
				],
				'desWhereTo': [
					'desWhereTo',
					'roundTrip',
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'desEntry': [
					'desEntry',
					'initAutocompleteCity'
				],
				'milestonesRewards': [
					'milestonesRewards'
				],
				'DESCityGuide': [
					'DESCityGuide',
					'roundTrip',
					'initAutocompleteCity'
				],
				'staticContentKrisflyer': [
					'staticContentKrisflyer'
				],
				'staticContentGeneric': [
					'staticContentGeneric'
				],
				'sshSelection': [
					'CIBBookingSummary',
					'roundTrip',
					'sshSelection'
				],
				'sshHotel': [
					'sshHotel',
					'CIBBookingSummary',
					'roundTrip',
					'sshSelection'//,
					// 'accordion'
				],
				'sshAdditional': [
					'CIBBookingSummary',
					'sshAdditional'
				],
				'mbChangeFlight': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'mbChangeFlight'
				],
				'staticContentComponents': [
					// 'initTabMenu'
				],
				'multiTabsWithLongText': [
					'multiTabsWithLongText'
				],
				'staticContentMeals': [
					// 'initTabMenu'
				],
				'staticContentSpecMeals': [
					// 'initTabMenu'
				],
				'initTabMenu': [
					'initTabMenu'
				],
				'staticContentHeritage': [
					'staticContentHeritage'
				],
				'moreInTheSection':[
					'moreInTheSection'
				],
				'kfUnsubscribe': [
					'kfUnsubscribe'
				],
				'feedBack': [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack'
				],
				'bestFare' : [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack',
					'selectReturnDate',
					'roundTrip'
				],
				'sQupgrade' : [
          // 'roundTrip'
        ],
				'checkRoom': [
					'sshSelection'
				],
				'youtubeApp': [
					'youtubeApp'
				],
				'flowPlayerApp': [
					'flowPlayerApp'
				],
				'landingSearchFlight': [
					'initAutocompleteCity',
					'landingSearchFlight',
					'roundTrip',
					'selectCabin',
					'selectReturnDate',
					'flightScheduleTrip'
				],
				'tab': [
					'tab'
				],
				'filterEntertainment': [
					'filterEntertainment'
				],
				'autocompleteSearch': [
					'autocompleteSearch'
				],
				'faqs': [
					'faqs'
				],
				'enewsSubscribe': [
					'initPersonTitles'
				],
				'contactUs': [
					'initAutocompleteCity'
				],
				'plusOrMinusNumber': [
					'plusOrMinusNumber'
				],
				'addBaggage': [
					'addBaggage'
				],
				'scrollTop': [
					'scrollTop'
				],
				'promotionsPackages': [
					'promotionsPackages',
					'initAutocompleteCity'
				],
				'radioTabsChange': [
					'radioTabsChange'
				],
				'hotelListing': [
					'hotelListing'
				],
				'autocompleteCountryCity': [
					'autocompleteCountryCity'
				],
				'disableValue': [
					'disableValue'
				],
				'togglePrivacy': [
					'togglePrivacy'
				],
				'carouselFade': [
					'carouselFade'
				],
				'carouselSlide': [
					'carouselSlide'
				],
				'promotionComponent': [
					'promotionComponent'
				],
				'pricePoints': [
					'pricePoints'
				],
				'checkImage': [
					'checkImage'
				],
				'bookingWidget': [
					'bookingWidget',
					'initAutocompleteCity',
					'roundTrip',
					'selectReturnDate',
					'selectCabin'
				],
				'redemptionNominee': [
					'initAutocompleteCity',
					'redemptionNominee'
				],
				'bookingMileClaim': [
					'bookingMileClaim'
				],
				'donateMiles': [
					'donateMiles'
				],
				'voucher': [
					'voucherRewards',
					'manageBooking',
					'voucherStored'
				]
			},
			getPage: function(page) {
				//return this.page[page];
				var functions = this.page[page];
				totalLoadedModules += functions.length;
				$.each(functions, function(index) {
					modulesToLoad.push(functions[index]);
					loadScript(urlScript.modules[functions[index]], function() {
						var moduleIndex = modulesToLoad.indexOf(functions[index]);
						modulesToLoad[moduleIndex] = SIA[functions[index]];
						loadedModules++;
						if(totalLoadedModules === loadedModules) {
							initFunction = initFunction.concat(modulesToLoad);
							concatInitFunctionDone = true;
						}
					}, 'body');
				});
			}
		};

		if($('[data-flight]').length){
			dashboard.getPage('flightTableBorder');
		}

		if($('[data-rule-onlycharacter]').length || $('[data-rule-digits]').length){
			dashboard.getPage('forceInput');
		}

		if($('[data-multiple-submit]').length){
			dashboard.getPage('multipleSubmit');
		}

		if ($('[data-social]').length) {
			dashboard.getPage('contact');
		}

		if (window.simpleSticky) {
			dashboard.getPage('simpleSticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-sticky]').length) {
			dashboard.getPage('sticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-bar]').length) {
			dashboard.getPage('bar');
			requireScript = requireScript.concat(['underscore']);
		}

		if($('[data-coach-beta]').length){
			dashboard.getPage('coachBeta');
		}

		if ($('[data-autocomplete-search]').length) {
			dashboard.getPage('autocompleteSearch');
		}

		if($('[data-tab]').length){
			dashboard.getPage('initTabMenu');
		}

		if($('[data-check-room]').length){
			dashboard.getPage('checkRoom');
		}

		if ($('[data-carousel-fade]').length) {
			dashboard.getPage('carouselFade');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-carousel-slide]').length) {
			dashboard.getPage('carouselSlide');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-price-points]').length) {
			dashboard.getPage('pricePoints');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-check-image]').length) {
			dashboard.getPage('checkImage');
		}

		if (body.hasClass('landing-search-flights-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('landing-fl-status-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-manage-booking-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-checkin-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-flight-schedules-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('statement-dark-site-page')) {
			dashboard.getPage('darkSiteStatements');
			requireScript = requireScript.concat(['slick', 'youtubeAPI']);
		}

		if (body.hasClass('home-page') && !window.darkSite) {
			dashboard.getPage('home');
			requireScript = requireScript.concat(['underscore', 'slick', 'inputmask', 'accounting']);
		}

		if(window.darkSite){
			dashboard.getPage('grayscaleModule');
		}
		if (body.hasClass('home-page') && window.darkSite) {
			dashboard.getPage('darkSiteHome');
			requireScript = requireScript.concat(['underscore', 'slick', 'grayscale', 'inputmask', 'accounting']);
		}
		if(body.hasClass('passenger-details-page')){
			if(body.hasClass('orb-passenger-details-page')) {
				dashboard.getPage('orbPassengerDetail');
			}
			else {
				dashboard.getPage('passengerDetail');
			}
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if(body.hasClass('seatsmap-page')){
			dashboard.getPage('seatMap');
			requireScript = requireScript.concat(['underscore', 'accounting', 'seatmap']);
		}
		if(body.hasClass('flight-status-page')){
			dashboard.getPage('flightStatus');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('flight-schedules-page')){
			dashboard.getPage('flightSchedule');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}
		if(body.hasClass('fare-calendar-page')){
			dashboard.getPage('flightCalendar');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if(body.hasClass('fares-list-page')){
			dashboard.getPage('promotion');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if(body.hasClass('fares-details-page')){
			dashboard.getPage('faresDetailsPage');
			requireScript = requireScript.concat(['inputmask']);
		}
		if(body.hasClass('krisflyer-list-page')){
			dashboard.getPage('promotionKrisflyer');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('booking-sumary-page') || body.hasClass('checkin-complete') || body.hasClass('relaunch-page')){
			dashboard.getPage('bookingSummnary');
			requireScript = requireScript.concat(['inputmask']);
		}
		if (body.hasClass('flight-select-page')) {
			if (body.hasClass('orb-flight-select-page')) {
				dashboard.getPage('orbFlightSelect');
			} else if (body.hasClass('mb-flight-select-page')) {
				if (body.hasClass('mb-flight-select-page-sf')) {
					dashboard.getPage('mbFlightSelectSf');
				} else {
					dashboard.getPage('mbFlightSelect');
				}
			} else {
				dashboard.getPage('flightSelect');
			}
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}
		if(body.hasClass('flight-search-calendar-page')){
			dashboard.getPage('flightSearchCalendar');
		}
		if (body.hasClass('milestones-at-a-glance-page')) {
			dashboard.getPage('milestonesRewards');
		}
		if(body.hasClass('multi-city-page')){
			dashboard.getPage('multicity');
			requireScript = requireScript.concat(['inputmask']);
		}
		if(body.hasClass('payments-page')){
			if(body.hasClass('orb-payments-page')) {
				dashboard.getPage('orbPayment');
			}
			else if(body.hasClass('mb-payments-page')) {
				dashboard.getPage('mbPayment');
			}
			else {
				dashboard.getPage('payment');
			}
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}

		// if(body.hasClass('mb-payments-page')) {
		// 	dashboard.getPage('mbPayment');
		// }

		if(body.hasClass('add-ons-page')){
			if(body.hasClass('orb-add-ons')) {
				dashboard.getPage('orbAddOns');
			}
			else {
				dashboard.getPage('addon');
			}
			requireScript = requireScript.concat(['inputmask', 'slick', 'accounting', 'underscore']);
		}
		if(body.hasClass('cib-confirmation-page')){
			dashboard.getPage('cibConfirmation');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if(body.hasClass('at-a-glance-page')){
			dashboard.getPage('atAGlance');
			requireScript = requireScript.concat(['transit','accounting','chart', 'underscore']);
		}
		if(body.hasClass('add-redemption-nominee-page')){
			dashboard.getPage('kfRedemptionNominee');
		}
		if(body.hasClass('partner-programme-page')){
			dashboard.getPage('kfPartnerProgramme');
		}
		// if(body.hasClass('miles-page')){
		// 	dashboard.getPage('milesPage');
		// }
		if(body.hasClass('messages-inbox-page')){
			dashboard.getPage('kfMessage');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('message-forward-page')){
			dashboard.getPage('kfMessageForward');
		}
		if(body.hasClass('personal-details-page') || body.hasClass('kf-profile-security') || body.hasClass('kf-preferences') /*|| body.hasClass('redemption-nominee-page')*/){
			dashboard.getPage('kfPersonalDetail');
		}
		if (body.hasClass('redemption-nominee-page')) {
			dashboard.getPage('redemptionNominee');
		}
		if(body.hasClass('claim-missing-miles-page')){
			dashboard.getPage('kfClaimMissingMiles');
		}
		if(body.hasClass('booking-upcoming-flights-page')) {
			dashboard.getPage('kfUpcomingFlights');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('booking-page')) {
			dashboard.getPage('kfFlightHistory');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}
		if(body.hasClass('ticket-receipt-page')) {
			dashboard.getPage('KFTicketReceipt');
		}
		if(body.hasClass('booking-check-ins-page')) {
			dashboard.getPage('KFCheckIns');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('how-to-earn-page')){
			dashboard.getPage('howToEarn');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('how-to-use-page')){
			dashboard.getPage('howToUse');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if(body.hasClass('redeem-miles')){
			dashboard.getPage('redeemMiles');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('expiring-miles-page')){
			dashboard.getPage('kfExpiringMiles');
		}
		if(body.hasClass('favourites-page')) {
			dashboard.getPage('KFFavourite');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if(body.hasClass('statements-page')){
			dashboard.getPage('kfStatement');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.find('.slides').length){
			requireScript = requireScript.concat(['slick']);
		}
		if(body.hasClass('orb-confirmation-page')) {
			dashboard.getPage('ORBConfirmation');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if(body.hasClass('orb-flight-schedule')) {
			dashboard.getPage('ORBFlightSchedule');
			requireScript = requireScript.concat(['underscore']);
		}
		if($('[data-accordion-wrapper]').length){
			dashboard.getPage('accordion');
		}

		if($('[data-count-chars-left]').length){
			dashboard.getPage('countCharsLeft');
			requireScript = requireScript.concat(['accounting']);
		}

		if($('[data-rule-validatedate]').length){
			dashboard.getPage('initCorrectDate');
		}
		// if($('[data-check-change]').length){
			// dashboard.getPage('FormCheckChange');
		// }
		// if(body.hasClass('create-new-pin-page')) {
		// 	dashboard.getPage('KFCreateNewPin');
		// }

		if(body.hasClass('registration-page')) {
			dashboard.getPage('KFRegistration');
		}

		if(body.hasClass('sqc-registration-page')) {
			dashboard.getPage('SQCRegistration');
		}

		if (body.hasClass('special-assistance-page')) {
			dashboard.getPage('specialAssistance');
			requireScript = requireScript.concat(['tiff']);
		}

		if(body.hasClass('cancel-waitlist-booking-page')) {
			dashboard.getPage('cancelWaitlistBooking');
			requireScript = requireScript.concat(['inputmask']);
		}
		if(body.hasClass('mb-main-page')) {
			dashboard.getPage('ManageBooking');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('excess-baggage-page')) {
			dashboard.getPage('ExcessBaggage');
			requireScript = requireScript.concat(['underscore']);
		}

		if(body.hasClass('mb-confirmation-page') || body.hasClass('mb-change-booking-confirmation-page')) {
			dashboard.getPage('MBConfirmation');
		}
		if(body.hasClass('review-cancellation-page')) {
			dashboard.getPage('ManageBooking');
		}
		if(body.hasClass('mb-waitlisted-flight-orc-reserved-page')) {
			dashboard.getPage('ManageBooking');
		}
		if(body.hasClass('mb-select-flights-page')) {
			dashboard.getPage('ManageBooking');
		}
		if(body.hasClass('review-page') || body.hasClass('review-booking-insuff-page')) {
			dashboard.getPage('MBReview');
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}
		if(body.hasClass('select-meals-page')) {
			dashboard.getPage('MBSelectMeal');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('select-meals-land-page')) {
			dashboard.getPage('MBSelectMealLand');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('expenditure-page')) {
			dashboard.getPage('SQCExpenditure');
		}
		if(body.hasClass('sqc-upcoming-flights-page')) {
			dashboard.getPage('sqcUpcomingFlight');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('sqc-user')) {
			dashboard.getPage('SQCUser');
		}
		if(body.hasClass('sqc-add-user')) {
			dashboard.getPage('SQCAddUser');
		}
		if(body.hasClass('sqc-at-a-glance-page')) {
			dashboard.getPage('SQCAtAGlance');
			requireScript = requireScript.concat(['highcharts', 'chart', 'transit', 'underscore', 'accounting']);
		}
		if(body.hasClass('sqc-bookings-flight-history-page')) {
			dashboard.getPage('SQCFlightHistory');
			requireScript = requireScript.concat(['jqueryui', 'validate', 'underscore']);
		}
		if(body.hasClass('sqc-saved-trips-page')) {
			dashboard.getPage('SQCSavedTrips');
			requireScript = requireScript.concat(['underscore']);
		}
		if(body.hasClass('change-flight-page')) {
			dashboard.getPage('mbChangeFlight');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if(body.hasClass('utilities-page')) {
			dashboard.getPage('desWhereTo');
			// requireScript = requireScript.concat(['isotope']);
			requireScript = requireScript.concat(['shuffle', 'underscore', 'inputmask']);
		}
		if(body.hasClass('destination-list-page')) {
			dashboard.getPage('desEntry');
			requireScript = requireScript.concat(['shuffle']);
		}
		if(body.hasClass('city-guide-page')) {
			dashboard.getPage('DESCityGuide');
			requireScript = requireScript.concat(['slick', 'inputmask']);
		}
		if(body.hasClass('des-krisFlyer-1-page')){
			dashboard.getPage('staticContentKrisflyer');
			requireScript = requireScript.concat(['slick']);
		}
		if(body.hasClass('static-content-generic-page')){
			dashboard.getPage('staticContentGeneric');
			requireScript = requireScript.concat(['slick']);
		}

		if(body.hasClass('ssh-selection-page')) {
			dashboard.getPage('sshSelection');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if(body.hasClass('hotel-page')) {
			dashboard.getPage('sshHotel');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if(body.hasClass('ssh-additional-page')) {
			dashboard.getPage('sshAdditional');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if(body.hasClass('static-content-components')) {
			dashboard.getPage('staticContentComponents');
		}

		if($('[data-multi-tab]').length){
			dashboard.getPage('multiTabsWithLongText');
		}

		if(body.hasClass('static-content-meals-page')) {
			dashboard.getPage('staticContentMeals');
		}

		if(body.hasClass('static-content-spec-meal-page')) {
			dashboard.getPage('staticContentSpecMeals');
		}

		if(body.hasClass('static-content-heritage')){
			dashboard.getPage('staticContentHeritage');
		}

		if (body.hasClass('contact-us-page')) {
			dashboard.getPage('contactUs');
		}

		if ($('[data-disable-value]').length) {
			dashboard.getPage('disableValue');
		}

		if ($('[data-toggle-privacy]').length) {
			dashboard.getPage('togglePrivacy');
		}

		if(body.hasClass('feedback-concern-baggage-delayed-page')) {
			dashboard.getPage('feedBack');
			requireScript = requireScript.concat(['accounting', 'tiff', 'recaptcha']);
		}
		if (body.hasClass('static-bestfare')) {
			dashboard.getPage('bestFare');
			requireScript = requireScript.concat(['accounting', 'inputmask', 'tiff', 'recaptcha']);
		}
		if (body.hasClass('sq-upgrade-marketing-page')) {
      dashboard.getPage('sQupgrade');
      requireScript = requireScript.concat(['inputmask']);
    }

		if($('[data-tablet-slider]').length){
			dashboard.getPage('moreInTheSection');
			if(requireScript.indexOf('slick') === - 1){
				requireScript = requireScript.concat(['slick']);
			}
		}
		if ($('[data-radio-tab]').length) {
			dashboard.getPage('tab');
		}

		if ($('[data-entertaiment]').length) {
			dashboard.getPage('filterEntertainment');
		}

		if($('[data-youtube-url]').length){
			dashboard.getPage('youtubeApp');
			requireScript = requireScript.concat(['youtubeAPI']);
		}
		if($('[data-flow-url]').length){
			dashboard.getPage('flowPlayerApp');
			requireScript = requireScript.concat(['flowplayerAPI']);
		}

		if(body.hasClass('unsubscribe-page')){
			dashboard.getPage('kfUnsubscribe');
		}

		if ($(['data-open-accordion']).length) {
			dashboard.getPage('faqs');
		}

		if ($('[data-plus-or-minus-number]').length) {
			dashboard.getPage('plusOrMinusNumber');
		}

		if ($('[data-radio-tabs-change]').length) {
			dashboard.getPage('radioTabsChange');
		}

		if (body.hasClass('add-baggage-page')) {
			dashboard.getPage('addBaggage');
		}

		if($('[data-scroll-top]').length){
			dashboard.getPage('scrollTop');
		}

		if (body.hasClass('enews-subscribe-page')) {
			dashboard.getPage('enewsSubscribe');
		}

		if (body.hasClass('promotions-packages-page')) {
			dashboard.getPage('promotionsPackages');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('hotel-listing-page')) {
			dashboard.getPage('hotelListing');
			requireScript = requireScript.concat(['shuffle']);
		}

		if (body.hasClass('booking-mile-claim-page')) {
			dashboard.getPage('bookingMileClaim');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}

		if ($('[data-promotion-component]').length) {
			dashboard.getPage('promotionComponent');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-booking-widget]').length) {
			dashboard.getPage('bookingWidget');
			requireScript = requireScript.concat(['inputmask']);
		}

		if ($('[data-component-country-city]').length) {
			dashboard.getPage('autocompleteCountryCity');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('donate-miles-page')) {
			dashboard.getPage('donateMiles');
		}

		if (body.hasClass('voucher-page')) {
			dashboard.getPage('voucher');
			requireScript = requireScript.concat(['underscore']);
		}
		if(totalLoadedModules === 0) {
			concatInitFunctionDone = true;
		}

		var countLoadScript = 0;
		var loadSIAScript = function(){
			var loadFunctionDuration = 0;
			var count = 0;
			var loadFunction = function(c){
				setTimeout(function(){
					if(initFunction[c]){
						initFunction[c]();
						count ++;
						if(count !== initFunction.length){
							loadFunction(count);
						}
						else{
							// SIA.preloader.hide();
							var unloadTimer = window.noJsonHandler ? 700 : 0;
							setTimeout(function(){ SIA.preloader.hide(); }, unloadTimer);
						}
					}
					else{
						SIA.preloader.hide();
					}
				}, loadFunctionDuration);
			};
			var waitInitFunction = setInterval(function() {
				if(concatInitFunctionDone) {
					loadFunction(count);
					clearInterval(waitInitFunction);
				}
			}, 100);
			if(typeof triggerModal !=='undefined'){
				window.setCookie = function(cname, cvalue, exdays) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays*24*60*60*1000));
					var expires = 'expires='+d.toGMTString();
					document.cookie = cname + '=' + cvalue + ';' + expires;
				};

				window.getCookie = function(cname) {
					var name = cname + '=';
					var ca = document.cookie.split(';');
					for(var i=0; i<ca.length; i++) {
						var c = ca[i].trim();
						if (c.indexOf(name) === 0){
							return c.substring(name.length, c.length);
						}
					}
					return '';
				};

				var initPopupTriggerModal = function(triggerModal, introLightboxEl) {
					// var trM = $(triggerModal);
					var trM = $();

					if(typeof introLightboxEl !== 'undefined'){
						trM = introLightboxEl;
					}else{
						trM = $(triggerModal);
					}

					var openLogin = trM.find('[data-open-popup-login]');
					var isOpenLogin = false;
					trM.Popup({
						overlayBGTemplate: SIA.global.config.template.overlay,
						modalShowClass: '',
						triggerCloseModal: '.popup__close, [data-close]',
						afterHide: function(popup){
							if($(popup).find('[data-remember-cookie] input').is(':checked')){
								window.setCookie(triggerModal, true, 7);
							}
							if(openLogin.length && isOpenLogin){
								$(openLogin.data('open-popup-login')).Popup('show');
								isOpenLogin = false;
							}
						}
					});

					if(openLogin.length){
						openLogin.off('click.openLogin').on('click.openLogin', function(){
							trM.Popup('hide');
							isOpenLogin = true;
						});
					}

					// if(typeof(Storage) !== 'undefined') {
					// 	if(localStorage.getItem('translateTo')) {
					// 		if(!window.getCookie(triggerModal)){
					// 			trM.Popup('show');
					// 		}
					// 	}
					// 	else{
					// 		if(!window.getCookie(triggerModal)){
					// 			trM.Popup('show');
					// 		}
					// 	}
					// }

					if(typeof(Storage) !== 'undefined') {
						if(localStorage.getItem('translateTo')) {
							if(!window.getCookie(triggerModal)){
								trM.Popup('show');
							}
						}
						else{
							if(window.translateTo){
								if(!window.getCookie(triggerModal)){
									trM.Popup('show');
								}
							}else{
								body.off('finishAppended.openLB').on('finishAppended.openLB', function(){
									if(!window.getCookie(triggerModal)){
										trM.Popup('show');
									}
								});
							}
						}
					}
				};

				if (/\//.test(triggerModal)) {
					var getLbTemplate = function() {
						return $.ajax({
							url: triggerModal,
						});
					};
					getLbTemplate().done(function(data) {
						var introLightboxEl = $(data);
						var totalImg = introLightboxEl.find('img');
						var countImage = 0;
						body.append(introLightboxEl);
						if(totalImg.length){
							totalImg.each(function() {
								var that = $(this);
								var img = new Image();
								img.onload = function() {
									countImage++;
									if (countImage === totalImg.length) {
										// initPopupTriggerModal(introLightboxEl);
										initPopupTriggerModal(triggerModal, introLightboxEl);
									}
								};
								img.src = that.attr('src');
							});
						}
						else{
							// initPopupTriggerModal(introLightboxEl);
							initPopupTriggerModal(triggerModal, introLightboxEl);
						}
					});
				} else {
					initPopupTriggerModal(triggerModal);
				}
			}
		};
		var excuteScript = function(url){
			loadScript(url, function(){
				countLoadScript++;
				if(countLoadScript === requireScript.length){
					loadSIAScript();
				}
			});
		};
		// if(requireScript.length){
		// 	for(var i = 0; i < requireScript.length; i++ ){
		// 		excuteScript(urlScript.getScript(requireScript[i]));
		// 	}
		// }
		// else{
		// 	loadSIAScript();
		// }
		var initLoadPage = function(){
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			var paramUrl = sURLVariables[1];
			var getURLParams = function(sParam) {
				for (var i = 0; i < sURLVariables.length; i++) {
					var sParameterName = sURLVariables[i].split('=');
					if (sParameterName[0] === sParam) {
						return sParameterName[1];
					}
				}
			};
			var run = function(){
				if(requireScript.length){
					for(var i = 0; i < requireScript.length; i++ ){
						excuteScript(urlScript.getScript(requireScript[i]));
					}
				}
				else{
					loadSIAScript();
				}
			};
			if(getURLParams('debug') && $.parseJSON(getURLParams('debug'))){
				var sName = paramUrl.split('=');
				$.get('./ajax/JSONS/' + sName[1], function(res){
					globalJson[sName[0]] = res;
					run();
				});
			}
			else{
				run();
			}
		};

		initLoadPage();

		var preloadImages = function() {
			if (typeof imageSrcs !== 'undefined' && imageSrcs.mobile.length) {
				$.each(imageSrcs.mobile, function(i, src){
					var img = new Image();
					img.src = src;
				});
			}
		};

		preloadImages();
	});

	$(window).off('resize.updatePosLoading').on('resize.updatePosLoading', function(){
			SIA.planLoading.rePosition();
	});

})(jQuery);

SIA.footer = function(){
	var footer = $('footer');
	footer.accordion({
		wrapper: 'div.footer-top',
		triggerAccordion: 'h3.footer-block-heading',
		contentAccordion: 'div.footer-block-inner > ul',
		activeClass: 'active',
		duration: 600,
		css3:true
	});
};

/**
 *  @name accordion
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'accordion';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}


	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			that.isOpenAll = false;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);

			that.wrapper.each(function(){
				var self = $(this);
				var triggerAccordion = self.find(that.options.triggerAccordion);
				var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');
				self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;

				triggerAccordion.each(function(idx){
					var trigger = $(this);
					var preventDefault = false;
					if(supportCss){
						trigger.data('height', contentAccordion.eq(idx).height());
					}
					if(!trigger.hasClass(that.options.activeClass)){
						if(supportCss){
							contentAccordion.eq(idx).css('height', 0);
						}else{
							contentAccordion.eq(idx).hide();
						}
					}
					trigger.off('click.accordion').on('click.accordion', function(e) {
						if($(e.target).is('label') || $(e.target).is(':checkbox')){
							return;
						}
						e.preventDefault();
						if(trigger.closest('.disable').length || preventDefault){
							return;
						}
						if(that.isOpenAll){
							that.isOpenAll = false;
							triggerAccordion.not(trigger).trigger('click.accordion');
							trigger.trigger('stayAcc');
							return;
						}
						preventDefault = true;
						if($.isFunction(that.options.beforeAccordion)){
							that.options.beforeAccordion.call(self, trigger, contentAccordion.eq(idx));
						}
						trigger.trigger('beforeAccordion');

						if(trigger.hasClass(that.options.activeClass)){
							if(supportCss){
								contentAccordion.eq(idx).css('height', 0);
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideUp(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.removeClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
							self.activeAccordion = $();
						}
						else{

							// if(self.activeAccordion.length){
							// 	self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							// 	self.activeAccordion.trigger('click.accordion');
							// 	self.activeAccordion = $();
							// }

							var curActiveAccor = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							if (curActiveAccor && curActiveAccor.length) {
								curActiveAccor.trigger('click.accordion');
							}

							self.activeAccordion = trigger;


							if(supportCss){
								contentAccordion.eq(idx).css('height', trigger.data('height'));
								contentAccordion.eq(idx).off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideDown(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.addClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
						}
					});
				});

				// this resize to support on mobile
				// win.off('resize.accordion').on('resize.accordion', function(){
				// 	if(supportCss && $(this).width() < 768){
				// 		contentAccordion.css('height', 0);
				// 		triggerAccordion.removeClass('active');
				// 	}else if(supportCss && $(this).width() >= 768){
				// 		contentAccordion.css('height', '');
				// 		triggerAccordion.removeClass('active');
				// 	}
				// }).trigger('resize.accordion');
			});
		},
		refresh: function(){
			this.init();
		},
		openAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(!that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', triggerAccordion.data('height'));
					}else{
						contentAccordion.slideDown(that.options.duration);
					}
					triggerAccordion.addClass(that.options.activeClass);
					that.trigger('openAll');
					that.isOpenAll = true;
				});
			}
		},
		closeAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', 0);
					}else{
						contentAccordion.slideUp(that.options.duration);
					}
					triggerAccordion.removeClass(that.options.activeClass);
					that.trigger('closeAll');
					that.isOpenAll = false;
				});
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		wrapper: '',
		triggerAccordion: '',
		contentAccordion: '',
		activeClass: 'active',
		duration: 400,
		css3: false,
		afterAccordion: function() {},
		beforeAccordion: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global autoCompleteLanguage functions
 * @version 1.0
 */

SIA.autoCompleteLanguage = function(){
	var global = SIA.global;
	var win = global.vars.win;
	// var	config = global.config;
	var body = global.vars.body;
	var languageJSON = global.vars.languageJSON;
	// var timerTriggerSearch;

	var getNamespace = (function(){
		var id = 0;
		return function(){
			return 'reposatcl-' + id++;
		};
	})();

	var autoCompleteCustom = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.airportData = that.options.airportData;
		that.timer = null;
		that.timerResize = null;
		that.winW = win.width();

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('div');
			var namespace = getNamespace();
			var bookingAutoComplete = field.autocomplete({
					minLength : 0,
					open: that.options.open,
					change: that.options.change,
					select: that.options.select,
					close: that.options.close,
					response: that.options.response,
					search: that.options.search,
					// source: that.airportData,
					source: function(request, response) {
						// create a regex from ui.autocomplete for 'safe returns'
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
						// match the user's request against each destination's keys,

						var match = $.grep(that.airportData, function(airport) {
							var flag = airport.flag;
							var value = airport.value;

							return (matcher.test(value) || matcher.test(flag)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
						});

						// ... return if ANY of the keys are matched
						response(match);
					},
					appendTo : that.options.autoCompleteAppendTo
				}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function (ul, item) {
				// customising our suggestion dropdowns here
				return $('<li class="autocomplete-item">')
				.attr('data-value', item.order)
				.attr('data-language', item.language)
				.attr('data-flag', item.flag)
				.append('<a class="autocomplete-link" href="javascript:void(0);"><img src="images/transparent.png" alt="" class="flags '+ item.flag +'">'+ item.value + '</a>')
				.appendTo(ul);
			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			// if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
			// 	bookingAutoComplete._move = function( direction ) {
			// 		var item, previousItem,
			// 		last = false,
			// 		api = this.menu.element.data('jsp'),
			// 		li = $(),
			// 		minus = null,
			// 		currentPosition = api.getContentPositionY();

			// 		switch(direction){
			// 			case 'next':
			// 				if(this.element.val() === ''){
			// 					api.scrollToY(0);
			// 					li = this.menu.element.find('li:first');
			// 					item = li.addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				else{
			// 					previousItem = this.menu.element.find('li.active').removeClass('active');
			// 					li = previousItem.next();
			// 					item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				if(!item){
			// 					last = true;
			// 					li = this.menu.element.find('li').removeClass('active').first();
			// 					item = li.addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				this.term = item.value;
			// 				this.element.val(this.term);
			// 				if(last){
			// 					api.scrollToY(0);
			// 					last = false;
			// 				}
			// 				else{
			// 					if(li.index() > that.options.itemShow - 1){
			// 						minus = Math.abs(previousItem.position().top - (this.menu.element.find('li:eq('+ (that.options.itemShow - 1) +')')).position().top);
			// 						if(Math.abs(currentPosition - minus) > 3){
			// 							api.scrollToY(minus);
			// 							currentPosition = api.getContentPositionY();
			// 						}
			// 						api.scrollToY(currentPosition + $(previousItem).outerHeight(true));
			// 					}
			// 				}
			// 				break;
			// 			case 'previous':
			// 				if(this.element.val() === ''){
			// 					last = true;
			// 					item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				else{
			// 					previousItem = this.menu.element.find('li.active').removeClass('active');
			// 					item = previousItem.prev().removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				if(!item){
			// 					last = true;
			// 					item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
			// 				}
			// 				this.term = item.value;
			// 				this.element.val(this.term);
			// 				if(last){
			// 					api.scrollToY(this.menu.element.find('.jspPane').height());
			// 					last = false;
			// 				}
			// 				else{
			// 					api.scrollToY(previousItem.position().top);
			// 					currentPosition = api.getContentPositionY();
			// 					api.scrollToY(currentPosition - $(previousItem).outerHeight(true));
			// 				}
			// 				break;
			// 		}
			// 	};
			// }

			field.autocomplete('widget').addClass('autocomplete-menu');
			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				var self = $(this);
				that.winW = win.width();
				self.closest('.custom-select').addClass('focus');
				win.off('resize.' + namespace).on('resize.' + namespace, function(){
					clearTimeout(that.timerResize);
					that.timerResize = setTimeout(function(){
						if(field.autocomplete('widget').is(':visible')){
							// field.autocomplete('widget').css({
							// 	'left': field.closest('.custom-select').offset().left,
							// 	'top': field.closest('.custom-select').offset().top + field.closest('.custom-select').outerHeight(true)
							// });
							if(window.navigator.msMaxTouchPoints){
								self.autocomplete('widget').show().css({
									'top': self.closest('[data-autocomplete]').offset().top + self.closest('[data-autocomplete]').outerHeight(true) - win.scrollTop()
								});
							}
							if(that.winW !== win.width()){
								field.trigger('blur.highlight');
								that.winW = win.width();
							}
						}
					}, 100);
				});

				// var dataAutocomplete = self.data('uiAutocomplete');
				// if(dataAutocomplete && self.val()) {
				// 	if(!dataAutocomplete.selectedItem) {
				// 		clearTimeout(timerTriggerSearch);
				// 		timerTriggerSearch = setTimeout(function() {
				// 			dataAutocomplete.search();
				// 		}, 10);
				// 	}
				// }

			});
			field.off('blur.highlight').on('blur.highlight', function(){
				that.timer = setTimeout(function(){
					field.closest('.custom-select').removeClass('focus');
					field.autocomplete('close');
				}, 200);

				win.off('resize.' + namespace);

			});
			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
				clearTimeout(that.timer);
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					field.autocomplete('widget').find('li.active').trigger('click');
					e.preventDefault();
				}
			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
			// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
	};


	var autoCompleteLanguage = function(){
		var ppLanguage = global.vars.ppLanguage;
		if (!ppLanguage.length) {
			return;
		}
		var optionLanguage = ppLanguage.find('.custom-radio');
		var parentOptionLanguage = optionLanguage.parent();
		var defaultFlag = ppLanguage.find('.custom-select img').attr('class').split(' ')[1];
		parentOptionLanguage.children().not(':eq(0)').remove();
		// optionLanguage.not(':eq(0)').hide();
		var dataLanguage = ['en_UK', 'zh_CN', 'fr_FR', 'pt_BR', 'de_DE', 'zh_TW', 'ja_JP', 'ko_KR', 'ru_RU', 'es_ES'];
		var detectLanguge = function(lng){
			var lngs = lng.split(',');
			var isLng = [];

			for(var i = 0; i < lngs.length; i ++){
				for(var ii = 0; ii < dataLanguage.length; ii ++){
					if($.trim(lngs[i]) === $.trim(dataLanguage[ii])){
						isLng.push(ii);
					}
				}
			}

			return isLng;
		};

		var _selectLanguage = function(arr){
			parentOptionLanguage.empty();
			optionLanguage.find(':radio').removeAttr('checked');
			for(var i = 0; i< arr.length; i ++){
				var t = optionLanguage.eq(arr[i]).clone().appendTo(parentOptionLanguage);
				if(i === 0){
					t.find(':radio').prop('checked', true);
				}
				else{
					t.find(':radio').prop('checked', false);
				}
			}
		};
		var timerAutocompleteLang = null;
		var timerAutocompleteLangOpen = null;

		var getFlags = function(value){
			var a = {
				idx : 0,
				flag : false
			};
			for(var i = 0; i < languageJSON.data.length; i ++){
				if(languageJSON.data[i].value === value){
					a = {
						idx : i,
						flag : true
					};
				}
			}
			return a;
		};

		autoCompleteCustom({
			containerAutocomplete : ppLanguage,
			autocompleteFields : 'input#text-country',
			autoCompleteAppendTo: body,
			airportData : languageJSON.data,
			open: function(){
				var self = $(this);
				self.autocomplete('widget').hide();
				clearTimeout(timerAutocompleteLangOpen);
				timerAutocompleteLangOpen = setTimeout(function(){
					self.autocomplete('widget').show();
					// self.autocomplete('widget').show().css({
					// 	'left': self.closest('.custom-select--2').offset().left,
					// 	'top': self.closest('.custom-select--2').offset().top + self.closest('.custom-select--2').outerHeight(true)
					// });
					if(window.navigator.msMaxTouchPoints){
						self.autocomplete('widget').show().css({
							'top': self.closest('[data-autocomplete]').offset().top + self.closest('[data-autocomplete]').outerHeight(true) - win.scrollTop()
						});
					}
				}, 100);
			},
			select: function(event, ui){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				flag.removeClass().addClass('flags ' + ui.item.flag);
				_selectLanguage(detectLanguge(ui.item.language));
				win.trigger('resize.popupLanguage');
				setTimeout(function(){
					self.blur();
				}, 100);
			},
			response: function(event, ui){
				if(ui.content.length ===1){
					// $(this).val(ui.content[0].value);
					// $(this).select();
				}
			},
			search: function(){
				// var self = $(this);
				clearTimeout(timerAutocompleteLang);
				timerAutocompleteLang = setTimeout(function(){
					// if(self.autocomplete('widget').find('li').length === 1){
					// 	self.autocomplete('widget').find('li').addClass('active');
					// }
				}, 100);
			},
			close: function(){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				var item = getFlags(self.val());
				if(item.flag){
					flag.removeClass().addClass('flags ' + languageJSON.data[item.idx].flag);
					_selectLanguage(detectLanguge(languageJSON.data[item.idx].language));
				}
				if(!$.trim(self.val())){
					flag.removeClass().addClass('flags ' + defaultFlag);
					var defaultLanguage = 'en_UK, zh_CN';
					_selectLanguage(detectLanguge($.trim(defaultLanguage)));
				}
				$(this).closest('[data-autocomplete]').removeClass('focus');
			},
			setWidth: 0
		});
	};
	// init
	autoCompleteLanguage();
};

/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'checkboxAll';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var that = this;
			var chbMaster = that.element.find('[data-checkbox-master]').first();
			var chbItems = that.element.find('[data-checkbox-item]');

			chbMaster.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checked = $(this).is(':checked');
				chbItems.filter(':visible').prop('checked', checked);
			});

			chbItems.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checkedAll = (chbItems.filter(':visible').length === chbItems.filter(':checked').length);
				chbMaster.prop('checked', checkedAll);
			});
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
	};

	// $(function() {
	// 	$('[data-checkbox-all]')[pluginName]();
	// });

}(jQuery, window));

/**
 * @name SIA
 * @description Define global cookiesUse functions
 * @version 1.0
 */
SIA.cookiesUse = function(){
	var popupCookies = $('.popup--cookie').appendTo(SIA.global.vars.container);
	// cookies

	var setCookie = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+d.toGMTString();
		document.cookie = cname + '=' + cvalue + ';' + expires;
	};

	var getCookie = function(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i].trim();
			if (c.indexOf(name) === 0){
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	var checkCookie = function () {
		var user = getCookie('seen');
		if (user !== '') {
			popupCookies.addClass('hidden');
		} else {
			popupCookies.removeClass('hidden');
		}
	};
	// end cookies
	checkCookie();
	popupCookies.find('.popup__close').off('click.closeCookie').on('click.closeCookie', function(e){
		e.preventDefault();
		setCookie('seen', true, 7);
		popupCookies.addClass('hidden');
	});
};

/**
 * @name defaultSelect
 **/
(function($, window, undefined) {
	var pluginName = 'defaultSelect';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	function setValue(el) {
		var valueSelect = '';
		el.each(function(idx) {
			if (idx === 0) {
				valueSelect += $(this).text();
			} else {
				valueSelect += ',' + $(this).text();
			}
		});
		return valueSelect;
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			that.wrapper = that.closest(that.options.wrapper);
			that.textCustom = that.wrapper.find(that.options.textCustom);
			that.valueContainer = that.wrapper.find(that.options.valueContainer);
			that.valueContainer.val(that.find('option:selected').val());
			that.textCustom.val(that.find('option:selected').text());

			if(that.wrapper.hasClass('disabled')){
				that.attr('disabled', true);
			}

			that.off('change.changeTestCustom').on('change.changeTestCustom', function() {
				if (that.options.isInput) {
					that.textCustom.val(setValue($(this).find('option:selected')));
				} else {
					that.textCustom.text(setValue($(this).find('option:selected')));
				}
				if($(this).find('option:selected').val()){
					that.wrapper.removeClass('default');
				}
				else{
					that.wrapper.addClass('default');
				}
				if($.isFunction(that.options.afterSelect)){
					that.options.afterSelect.call(that, $(this).find('option:selected'));
				}
			});
			if (that.options.isInput) {
				that.textCustom.val(setValue(that.find('option:selected')));
			} else {
				that.textCustom.text(setValue(that.find('option:selected')));
			}
			that.off('focus.hightlight').on('focus.hightlight', function(){
				that.wrapper.addClass('focus');
			});
			that.off('blur.hightlight').on('blur.hightlight', function(){
				that.wrapper.removeClass('focus');
			});
		},
		refresh: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			if (that.options.isInput) {
				that.textCustom.val(setValue(that.find('option:selected')));
			} else {
				that.textCustom.text(setValue(that.find('option:selected')));
			}
			that.valueContainer.val(setValue(that.find('option:selected')));
			if($.trim(that.find('option:selected').val())){
				that.wrapper.removeClass('default');
			}
			else{
				that.wrapper.addClass('default');
			}
		},
		update: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			if (that.options.isInput) {
				that.textCustom.val(setValue(that.find('[selected]')));
			} else {
				that.textCustom.text(setValue(that.find('[selected]')));
			}
			that.valueContainer.val(setValue(that.find('[selected]')));
			if($.trim(that.find('[selected]').val())){
				that.wrapper.removeClass('default');
			}
			else{
				that.wrapper.addClass('default');
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				// window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		wrapper: '.custom-select',
		textCustom: '.select__text',
		isInput: false,
		valueContainer: 'input.valueContainer',
		afterSelect: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global highlightInput functions
 * @version 1.0
 */
SIA.highlightInput = function(){
	var win = SIA.global.vars.win;
	var doc = SIA.global.vars.doc;
	var el = null;
	var autocompleteEl = null;

	// fix issue autocomplete when clear the text and focus other input, the autocomplete doesn't close
	doc.off('focus.highlightInput').on('focus.highlightInput', 'input, select', function(e) {
		if (el && el.length && el.closest('[data-autocomplete]').length && el.get(0) !== e.target) {
			setTimeout(function() {
				if(el.data('uiAutocomplete')){
					el.autocomplete('close');
					el = $(e.target);
				}
			}, 500);
		}
		else {
			el = $(e.target);
		}
	});

	doc
		.off('touchstart.highlightInput touchend.highlightInput touchmove.highlightInput')
		.on('touchstart.highlightInput touchend.highlightInput touchmove.highlightInput', function(e) {
			if (autocompleteEl && autocompleteEl.closest('[data-autocomplete]').length &&
				autocompleteEl.get(0) !== e.target && !$(e.target).closest('.ui-autocomplete').length) {
				autocompleteEl.autocomplete('close');
				autocompleteEl = $(e.target);
			}
			else if (!$(e.target).closest('.ui-autocomplete').length) {
				autocompleteEl = $(e.target);
			}
		});

	var ip = $('.input-3,.input-1,.textarea-1, .textarea-2').not('.disabled').filter(function(idx, el){
		if(!$(el).closest('[data-return-flight]').length){
			return el;
		}
	});
	ip.off('focusin.highlightInput').on('focusin.highlightInput', function(){
		var self = $(this);
		var wW = win.width();

		if (!self.hasClass('disabled')) {
			self.addClass('focus');
			self.removeClass('default');
		}

		win.off('resize.blurfocusin').on('resize.blurfocusin', function(){
			if(wW !== win.width()){
				win.focus();
				win.off('resize.blurfocusin');
			}
		});
	}).off('focusout.highlightInput').on('focusout.highlightInput', function(){
		var self = $(this);
		var child = self.children();
		self.removeClass('focus');
		// fix issue for alphanumeric if there is no rule-required
		if(child.data('rule-alphanumeric') && !child.data('rule-required')){
			self.closest('.grid-col').removeClass('error').find('.text-error').remove();
		}
	}).find('input').off('change.removeClassDefault').on('change.removeClassDefault', function(){
		var self = $(this);
		if(!self.val() || self.val() === self.attr('placeholder')){
			self.addClass('default');
		}
		else{
			self.removeClass('default');
		}
	}).trigger('change.removeClassDefault');

};

'use strict';
/**
 * @name SIA
 * @description Define global initCustomSelect functions
 * @version 1.0
 */
SIA.initCustomSelect = function(){
	if($('.passenger-details-page').length){
		return;
	}
	$('select').each(function(){
		if(!$(this).closest('[data-autocomplete]').length){
			if(!$(this).data('defaultSelect')){
				$(this).defaultSelect({
					wrapper: '.custom-select',
					textCustom: '.select__text',
					afterSelect: function(){
						var self = $(this);
						if(self.is('#country-1')){
							var beforeFlyItem = self.closest('.before-fly-choose').next().children('.before-fly-item');
							beforeFlyItem.hide().eq(self.prop('selectedIndex')).show();
						}
						if(self.closest('[data-fare-deal]').length){
							$('#fare-deals').children('.manualFareDeals').hide().eq(self.prop('selectedIndex')).show();
						}
						if(self.is('#economy-5')){
							// var tab = $('#travel-widget');
							var outerWraper = self.closest('.form-group');
							var byRoute = outerWraper.siblings('.by-route');
							var byFlightNo = outerWraper.siblings('.by-flight-no');
							if(self.prop('selectedIndex')){
								byRoute.removeClass('hidden');
								byFlightNo.addClass('hidden');
							}
							else{
								byRoute.addClass('hidden');
								byFlightNo.removeClass('hidden');
							}
							// tab.tabMenu('onResize');
						}
					},
					isInput: false
				});
			}
		}
	});
};

/**
 * @name SIA
 * @description Define global initConfirmCheckbox functions
 * @version 1.0
 */
SIA.initConfirmCheckbox = function() {
	var wrapperFieldConfirm = $('[data-confirm-tc]');
	var btnSubmit = $('[data-button-submit]');

	if(wrapperFieldConfirm.length) {
		var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

		confirmCheckbox.off('change.confirm').on('change.confirm', function(e) {
			e.preventDefault();
			var btn = btnSubmit.length ? btnSubmit : confirmCheckbox.closest('.form-group-full').next().find(':submit');
			if(btn.length) {
				if(confirmCheckbox.is(':checked')) {
					btn.removeClass('disabled').prop('disabled', false);
				}
				else {
					btn.addClass('disabled').prop('disabled', true);
				}
			}
		}).trigger('change.confirm');
	}
};

/**
 * @name SIA
 * @description Define global initLangToolbar functions
 * @version 1.0
 */
SIA.initLangToolbar = function() {
	var body = SIA.global.vars.body;

	if(typeof(Storage) !== 'undefined' && !body.hasClass('popup-window-login-page') && !body.hasClass('popup-window-logout-page')) {
		// if(JSON.parse(localStorage.getItem('translateTo'))) {
		// condition alway to return true value
		// if(localStorage.getItem('translateTo')) {
		// 	SIA.global.vars.body.data('showTBar',false);
		// 	return;
		// }
		if(localStorage.getItem('translateTo')) {
			window.translateTo = true;
			SIA.global.vars.body.data('showTBar',false);
			return;
		}

		var global = SIA.global;
		var body = global.vars.body;
		body.data('showTBar',true);
		// var container = $('#container');
		var hideTbar = function(tpl) {
			// tpl.css('margin-top', -tpl.height());
			body.data('showTBar',false);
			// container.css('padding-top', 0);
			// body.removeClass('translateOn');
			// tpl.off('webkitTransitionEnd.toolbar transitionend.toolbar msTransitionEnd.toolbar oTransitionEnd.toolbar').on('webkitTransitionEnd.toolbar transitionend.toolbar msTransitionEnd.toolbar oTransitionEnd.toolbar', function() {
				// body.removeClass('transition-all');
			tpl.remove();
			// });
		};

		var showTBar = function(tpl) {
			var delayTime = 200;
			var tBarHeight;
			body.prepend(tpl);
			tBarHeight = tpl.height();
			// tpl.css('margin-top', -tBarHeight);
			/*if($('.at-a-glance-page').length){
				delayTime += 1000;
			}*/
			setTimeout(function() {
				// container.css('padding-top', tBarHeight);
				// body.addClass('transition-all');
				// tpl.removeAttr('style');
				// body.addClass('translateOn');

				// Trigger show popup
				if(typeof triggerModal !== 'undefined' && triggerModal.indexOf('/') === -1){
					if(!window.getCookie(triggerModal) && triggerModal.indexOf('/') === -1){
						$(triggerModal).Popup('show');
					}
				}
				body.trigger('finishAppended');
			}, delayTime);
		};
		if(!body.is('.interstitial-page')) {
			$.get(global.config.url.langToolbarTemplate, function(templateStr) {
				var tpl = $(templateStr);
				tpl.find('.toolbar__close').off('click.closeToolbar').on('click.closeToolbar', function(e) {
					e.preventDefault();
					hideTbar(tpl);
				});
				tpl.find('[data-lang-toolbar]').off('click.translateLang').on('click.translateLang', function(e) {
					e.stopPropagation();
					hideTbar(tpl);
					localStorage.setItem('translateTo', $(this).data('langToolbar'));
				});
				// global.vars.win.off('resize.langToolbar').on('resize.langToolbar', function() {
				// 	if(tpl.length) {
				// 		container.css('padding-top', tpl.height());
				// 	}
				// });
				showTBar(tpl);
			}, 'html');
		}
	}
};

/**
 * @name SIA
 * @description Define global fixPlaceholder functions
 * @version 1.0
 */
SIA.fixPlaceholder = function(){
	// creating a fake placeholder for not supporting placeholder
	var placeholderInput = $('[placeholder]');
	if(!window.Modernizr.input.placeholder){
		placeholderInput.placeholder();
	}
};

/**
 * @name SIA
 * @description Define global initSubMenu functions
 * @version 1.0
 */
SIA.initSubMenu = function() {
	var subMenu = $('[data-device-submenu]');

	if(subMenu.length) {
		subMenu.find('option[selected]').prop('selected', true);
		subMenu.off('change.submenu').on('change.submenu', function() {
			var url = $(this).find('option:selected').data('url');
			if(url) {
				window.location.href = url;
			}
		});
	}
};

/**
 * @name SIA
 * @description Define global initToggleButtonValue functions
 * @version 1.0
 */
SIA.initToggleButtonValue = function() {
	var buttonToggle = $('[data-toggle-value]');
	buttonToggle.off('click.toggleLabel').on('click.toggleLabel',function() {
		var toggleValue = $(this).data('toggle-value');
		var currentValue = $(this).is('input') ? $(this).val() : $(this).html();
		var slideItem = $(this).closest('.slide-item');
		var index = slideItem.attr('index');
		$(this).data('toggle-value', currentValue);
		if($(this).is('input')) {
			$(this).val(toggleValue);
		}
		else {
			$(this).html(toggleValue);
		}
		if(slideItem.length){
			slideItem.siblings('[index="'+ index +'"]').find('[data-toggle-value]').html(toggleValue);
		}
	});
};

/**
 *  @name Popup
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'Popup';
	var win = $(window);
	var body = $(document.body);

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	var detectS3 = function(){
		var modal = ['GT-I9300', 'GT-I9305', 'SHV-E210K/L/S', 'SGH-T999/L','SGH-I747', 'SGH-N064', 'SGH-N035', 'SCH-J021', 'SCH-R530', 'SCH-I535', 'SCH-S960L', 'SCH-S968C','GT-I9308'];
		var isS3 = false;
		for(var i = 0; i < modal.length; i++){
			if(window.navigator.userAgent.indexOf(modal[i]) !==-1){
				isS3 = true;
				break;
			}
		}
		return isS3;
	};

	function onIpad() {
		// var userAgent = navigator.userAgent.toLowerCase();
		// var isIOS = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || ('ontouchstart' in window);
		var mobile = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
		if (!mobile) {
			return false;
		} else {
			return true;
		}
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			that.modal = that.attr('data-modal-popup') ? $(that.attr('data-modal-popup')) : that;
			that.modal = that.modal.appendTo(body);
			that.detectIpad = onIpad();
			that.isShow = false;
			that.isAnimate = false;
			that.timer = null;
			that.isTablet = false;
			that.isMobile = false;
			that.scrollTopHolder = 0;
			that.winW = win.width();

			that.modal.addClass('hidden');
		},
		freeze: function(){
			var plugin = this,
					that = plugin.element,
					h = win.height() + that.scrollTopHolder,
					langToolbarEl = $('.toolbar--language');
			if(langToolbarEl.length) {
				var langH = langToolbarEl.height();
				h -= langToolbarEl.is(':hidden') ? 0 : langH;
				if(win.scrollTop() > langH) {
					body.removeClass('transition-all').removeClass('translateOn');
				}
			}
			if(!that.data('parentContainerStyle')) {
				$(plugin.options.container).css({
					'height': h
				});
			}
		},
		showTablet: function() {
			var plugin = this,
					that = plugin.element;
			plugin.reposition();
			if(!$(plugin.options.container)){
				that.freezeBody = true;
				plugin.freeze();
				$(plugin.options.container);
			}
			that.modal.removeClass('hidden').addClass('animated fadeIn').css('zIndex', (that.options.zIndex + 1));
			that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
					win.scrollTop(0);
				}
				if(that.options.afterShow){
					that.options.afterShow(that);
				}

				that.trigger('afterShow');
				that.isAnimate = false;
				that.isTablet = true;
				plugin.addResize();
				that.modal.removeClass('fadeIn');
				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		},
		showMobile: function() {
			var plugin = this,
					that = plugin.element;

			plugin.reposition();
			// that.modal.css({'position': 'fixed', 'overflow-y': 'auto'});
			that.modal.css({'position': 'fixed'});
			if(that.modal.find('.popup__content').outerHeight(true) < window.screen.height){
				that.modal.height(win.height());
			}
			// freeze
			that.modal.css('zIndex', (that.options.zIndex + 1));
			that.modal.addClass('animated slideInRight');
			that.isMobile = true;
			that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				var winH = win.height(),
						langToolbarEl = $('.toolbar--language');
				if(langToolbarEl.length) {
					winH -= langToolbarEl.is(':hidden') ? 0 : langToolbarEl.height();
				}
				$(plugin.options.container).css('height', winH).data({'winHeight': win.height(), 'currentHeight': winH});

				if(that.modal.find('.popup__content').outerHeight(true) < window.screen.height) {
					that.modal.height(win.height());
				}

				$(plugin.options.container).css('overflow', 'hidden');
				win.scrollTop(0);
				if(that.options.afterShow){
					that.options.afterShow(that);
				}
				// callback function
				that.trigger('afterShow');
				that.modal.css({
					'right': 0
				});
				that.isAnimate = false;

				that.modal.removeClass('slideInRight');
				if(detectS3()){
					that.modal.removeAttr('style');
					setTimeout(function(){
						plugin.reposition();
						if(that.modal.outerHeight(true) < window.screen.height){
							that.modal.height(win.height());
						}
						that.modal.css('zIndex', (that.options.zIndex + 1));
					}, 100);
				}
				plugin.addResize();
				// fix bug on iphone 5 of pax-details-name-mismatch.html
				setTimeout(function(){
					that.modal.css('position', '');
				}, 100);
				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
			// that.modal.on('touchstart.avc', function(){
			// });
		},
		reset: function(){
			var plugin = this,
				that = plugin.element,
				winH = win.height(),
				langToolbarEl = $('.toolbar--language');
			if(langToolbarEl.length) {
				winH -= langToolbarEl.is(':hidden') ? 0 : langToolbarEl.height();
			}
			that.modal.css('height', '');
			that.modal.find('.popup__content').css('height', '');

			// $(plugin.options.container).css('height', $(window).height());
			$(plugin.options.container).css('height', winH).data({'winHeight': win.height(), 'currentHeight': winH});
			$(plugin.options.container).css('overflow', 'hidden');

			if(that.modal.outerHeight(true) < win.height()){
				that.modal.css('height', $(window).height());
			}
			// else {
			// 	$(plugin.options.container).css('height', that.modal.outerHeight(true));
			// }
			// if(that.modal.outerHeight(true) > win.height()){
			// 	$(plugin.options.container).css('height', that.modal.outerHeight(true));
			// 	$(plugin.options.container).css('overflow', 'hidden');
			// }
			// else{
			// 	var container = $(plugin.options.container),
			// 			currentHeight = container.data('currentHeight'),
			// 			winHeight = container.data('winHeight');
			// 	container.css('height', currentHeight);
			// 	that.modal.height(winHeight);
			// 	container.css('overflow', 'hidden');
			// }
		},
		show: function() {
			var plugin = this,
					that = plugin.element;
			if(that.isShow){
				return;
			}
			that.winW = win.width();
			that.winH = win.height();
			that.trigger('beforeShow');
			if(that.options.beforeShow){
				that.options.beforeShow(that);
			}

			that.isShow = true;
			that.isAnimate = true;
			that.overlay = $(that.options.overlayBGTemplate).appendTo(body);
			that.scrollTopHolder = win.scrollTop();
			that.overlay.show().css({
				'zIndex': that.options.zIndex
			}).addClass('animated fadeInOverlay');
			that.modal.removeClass('hidden');
			plugin.showMobile();

			that.modal.find(that.options.triggerCloseModal).on({
				'click.hideModal': function(e) {
					e.preventDefault();
					if(!that.isAnimate){
						plugin.hide();
					}
				}
			});
			that.modal.off('click.doNothing').on('click.doNothing', function(){
				// fix for lumina 820 and 1020
			});
			if(that.options.closeViaOverlay){
				that.overlay.on({
					'click.hideModal': function(e) {
						e.preventDefault();
						if(!that.isAnimate){
							plugin.hide();
						}
					}
				});
			}
			that.modal.on({'click.hideModal': function(e) {
					if(!that.isAnimate && !$(e.target).closest('.popup__inner').length){
						// e.preventDefault();
						plugin.hide();
					}
				}
			});
			// that.modal.find('.popup__inner').off('click.hideModal').on('click.hideModal', function(e) {
			// 	e.stopPropagation();
			// });

			body.addClass('no-flow');

			that.isShow = true;
		},
		addResize: function(){
			var plugin = this,
				that = plugin.element;
			// resize
			win.on({
				'resize.reposition': function() {
					// if(that.isShow && win.width() !== that.winW){
					// 	plugin.hide();
					// 	return;
					// }
					// if(!that.isShow || (win.width() === that.winW)){
					// 	return;
					// }
					// that.winW = win.width();
					if(that.timer){
						clearTimeout(that.timer);
					}
					that.timer = setTimeout(function(){
						if(that.isShow){
							plugin.reposition();
							plugin.reset();
						}
					}, 600);
				},
				'keyup.escape': function(e) {
					if (e.which === 27) {
						plugin.hide();
						if(that.find('[data-customselect]').length){
							$(document).trigger('click');
						}
					}
				}//,
				// 'orientationchange.hide': function() {
				// 	plugin.hide();
				// }
			});
		},
		hide: function() {
			var plugin = this,
					that = plugin.element,
					langToolbarEl = $('.toolbar--language');

			if(that.overlay){
				that.isShow = false;
				that.trigger('beforeHide');
				that.modal.find(that.options.triggerCloseModal).off('click.hideModal');
				that.overlay.off('click.hideModal');
				if(langToolbarEl.length) {
					// langToolbarEl.removeClass('hidden');
					// langToolbarEl.removeAttr('style');
					body.addClass('transition-all translateOn');
				}
				if(that.options.beforeHide){
					that.options.beforeHide(that);
				}

				that.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				that.modal.css('position', 'fixed');
				that.modal.addClass('animated slideOutRight');
				win.off('resize.reposition keyup.escape orientationchange.hide');
				// unfreeze
				if(!that.data('parentContainerStyle')) {
					$(plugin.options.container).removeAttr('style');
				}

				// if(langToolbarEl.length) {
				// 	$(plugin.options.container).css('padding-top', langToolbarEl.height());
				// }

				win.scrollTop(that.scrollTopHolder);
				that.scrollTopHolder = 0;


				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
					that.modal.css('height', '').find('.popup__content').removeAttr('style');

					that.modal.removeAttr('style').addClass('hidden').removeClass('animated slideOutRight fadeIn slideInRight');
					that.overlay.off('click.hideModal').remove();
					that.overlay = null;

					that.isAnimate = false;

					// callback function
					if(that.options.afterHide){
						that.options.afterHide(that);
					}
					that.trigger('afterHide');
					that.modal.css('right', '');
					that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});

				body.removeClass('no-flow');
			}
		},
		reposition: function() {
			var plugin = this,
					that = plugin.element;
			that.modal.removeClass('hidden').css({
				top: 0,
				position : 'absolute',
				left: 0
			});
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		overlayBGTemplate: '<div class="bg-modal radial" id="overlay"></div>',
		modalShowClass: 'modal-show',
		triggerCloseModal: '.modal_close, a.btn-gray, [data-close]',
		duration: 500,
		container: '#container',
		mobile: 768,
		tablet: 988,
		zIndex: 20,
		closeViaOverlay: true,
		beforeShow: function() {},
		beforeHide: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define social network functions
 * @version 1.0
 */
SIA.socialNetworkAction = function(){
	var global = SIA.global;
	var config = global.config;
	var social = $('[data-social]');
	var cocialSharing = function (element, link) {
		$(element).attr('href', link);
		$(element).attr('target', '_blank');
	};
	social.each(function(){
		var fb = $('[data-facebook]', this);
		var tw = $('[data-twitter]', this);
		var gplus = $('[data-gplus]', this);
		cocialSharing(fb, config.url.social.facebookSharing + window.location.href);
		cocialSharing(tw, config.url.social.twitterSharing + window.location.href);
		cocialSharing(gplus, config.url.social.gplusSharing + window.location.href);
	});
};

(function($, window, undefined) {
  var pluginName = 'stickyWidget',
      win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          el = that.element,
          elPos = el.offset().top,
          openStickyEl = el.find('[data-sticky-open]'),
          stickyBlock = el.find('.booking-widget-block'),
          closeStickyEl = el.find('.sticky__close'),
          curPos = 0 ;
      win.off('scroll.' + pluginName).on('scroll.' + pluginName,
        function(){
        if(win.scrollTop() >= elPos) {
          el.addClass('sticky');
          if(stickyBlock.is('.hidden')) {
            openStickyEl.removeClass('hidden');
          }
          el.find('input').blur();
          $('.ui-datepicker').hide();
        } else {
          if(stickyBlock.is('.hidden')) {
              el.removeClass('sticky');
              openStickyEl.addClass('hidden');
            }
        }
      })
      openStickyEl.off('click.openSticky').on('click.openSticky', function(){
          // $.when(stickyBlock.slideDown('fast')).done(function(){
            stickyBlock.removeClass('hidden')
            // var stickyHeight;
            // if(stickyBlock.outerHeight() > win.height()) {
            //   stickyHeight = stickyBlock.outerHeight();
            //   el.css('overflow', 'scroll');
            // } else {
            //   stickyHeight = win.height();
            // }
            stickyBlock.css({
              'height': win.height() + 'px'
            });
            $('body').css({
              'height': win.height() + 'px',
              'overflow': 'hidden',
              'position': 'fixed'
            });
          // });
          openStickyEl.addClass('hidden');
          closeStickyEl.css('display', 'block');
          curPos = win.scrollTop();

      });
      closeStickyEl.off('click.closeSticky').on('click.closeSticky', function(){
        el.css('overflow', 'auto');
        stickyBlock.css({
          'height': 'auto'
        });
        $('body').css({
          'height': 'auto',
          'overflow': 'auto',
          'position': 'static'
        });
        closeStickyEl.css('display', 'none');
        stickyBlock.addClass('hidden');
        // $.when(stickyBlock.slideUp('fast')).done(function(){
            openStickyEl.removeClass('hidden');
            win.scrollTop(curPos);
        // });
      });
      win.off('orientationchange.sticky').on('orientationchange.sticky', function(){
        closeStickyEl.trigger('click.closeSticky');
      })
      this.checkRadio();
    },

    checkRadio: function() {
      var that = this,
          el   = that.element,
          radioEl = el.find('input:radio'),
          target = el.find('[data-target]');
      radioEl.each(function(idx){
        var self = $(this);
        if(self.is(':checked')){
          if(idx){
            target.eq(1).removeClass('hidden');
            target.eq(0).addClass('hidden');
          }
          else{
            target.eq(0).removeClass('hidden');
            target.eq(1).addClass('hidden');
          }
        }
        self.off('change.stickyWidget').on('change.stickyWidget', function(){
          if(self.is(':checked')){
            if(idx){
              target.eq(1).removeClass('hidden');
              target.eq(0).addClass('hidden');
            }
            else{
              target.eq(0).removeClass('hidden');
              target.eq(1).addClass('hidden');
            }
          }
        });
      });
    },

    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
  };

  $(function() {
   $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));

/**
 *  @name tabMenu
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
  var pluginName = 'tabMenu';
  var win = $(window);
  var body = $(document.body);
  var isOpen = false;
  var itemTab = $('.tab-nav-item');
  var customRadio = $('.custom-radio');
  var container = $('#container');
  var tabContentOverlay;
  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.selectOnMobile = that.find(that.options.selectOnMobile);
      that.freezeBody = false;
      that.scrollTopHolder = 0;
      that.winW = win.width();
      that.winH = win.height();

      var onResize = null;
      if(that.options.isPopup){
        onResize = function(){
          if(that.winW === win.width() && that.winH === win.height()){
            return;
          }

          that.winW = win.width();
          that.winH = win.height();
          plugin.onResize();
        };
      }

      $('[data-checked-show]').on('click.toggle', function() {
        if(!$('body').hasClass('add-ons-1-landing-page')){
          var target = $(this).data('checked-show');
          if(!$(this).is(':checked')) {
            $(target).removeClass('hidden');
            plugin.onResize();
          } else {
            $(target).addClass('hidden');
            plugin.onResize();
          }
        }
      });

      itemTab.on('click', function() {
        var $this = $(this),
            itemClick = $(this).index(),
            itemActive = that.find('[data-tab-nav-item]');
        itemTab.removeClass('active');
        $(this).addClass('active');
        var tabItem =  itemActive.eq(itemClick);
        var tabItemIndex = itemActive.eq(itemClick).index();
        itemActive.removeClass('active');
        if(itemClick === tabItemIndex){
          tabItem.addClass('active');
          plugin.onResize();
        }
        return false;
      });
      var customRadioTab = function(){
        var radioBook = that.find('#travel-radio-1'),
          radioRedeem = that.find('#travel-radio-2'),
          bookFlight = that.find('#book-flight'),
          bookRedeem = that.find('#book-redem');
          if (radioBook.is(':checked')) {
            bookFlight.addClass('active');
          }else{
             bookFlight.removeClass('active');
          }if (radioRedeem.is(':checked')){
            bookRedeem.addClass('active');
          }else{
            bookRedeem.removeClass('active');
          }
      };
      customRadio.on('click',function() {
        if(that.data('widget-v1')){
          customRadioTab();
        }
      });

      that.selectOnMobile.prop('selectedIndex', that.index);
      that.selectOnMobile.off('change.triggerTab').on('change.triggerTab', function(){
        if(that.data('tab')){
          that.tab.eq(that.selectOnMobile.prop('selectedIndex')).trigger('click.show');
        }
        else{
          if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).is('a')){
            window.location.href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).attr('href');
          }
          else{
            if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').length){
              var href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').attr('href');
              if(!href.match(':void')){
                window.location.href = href;
              }
            }
          }
        }
      }).triggerHandler('blur.resetValue');

      if(!that.data('tab')){
        return;
      }
      that.tab.off('click.show').on('click.show', function(e) {
        e.preventDefault();
        plugin.changeTab(this);
      });

      if(that.options.isPopup){
        that.find(that.options.triggerClosePopup).off('click.hideTabPopup').on('click.hideTabPopup', function(e){
          e.preventDefault();
          var parent = $(this).closest('.tab-wrapper');
          var langToolbarEl = $('.toolbar--language');
          if(isOpen){
            return;
          }
          if(langToolbarEl.length) {
            langToolbarEl.show();
          }
          isOpen = true;
          parent.addClass('animated slideOutRight');
          tabContentOverlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
          win.off('resize.resetTabMenu orientationchange.hide');
          parent.css('position', 'fixed');
          $(that.options.container).removeAttr('style');
          win.scrollTop(that.scrollTopHolder);
          parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
            parent.removeClass('animated slideOutRight slideInRight');
            tabContentOverlay.off('click.closeTabContent').remove();
            that.scrollTopHolder = 0;
            isOpen = false;
            parent.removeAttr('style');
            parent.find('.tab-content.active').removeAttr('style');
            parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
          });
          win.off('resize.resetTabMenu orientationchange.hide');
          if($.isFunction(that.options.beforeClosePopup)){
            that.options.beforeClosePopup.call(that, that.tabContent, $(this));
            parent.find(':input:focus').blur();
          }
        });
      }
    },
    onResize: function(){
      var plugin = this,
        that = plugin.element;
      var parent = that.tabContent.parent();
      var langToolbarEl = $('.toolbar--language');
      if(langToolbarEl.length) {
        langToolbarEl.hide();
      }

      parent.css('height', '');
      that.tabContent.eq(that.index).css('height', '');

      if(that.tabContent.eq(that.index).outerHeight(true) > win.height()){
        $(that.options.container).css({
          'height': that.tabContent.eq(that.index).outerHeight(true),
          'overflow': 'hidden'
        });
      }
      else{
        that.tabContent.eq(that.index).removeAttr('style');
        $(plugin.options.container).css({
          'height': win.height(),
          'overflow': 'hidden'
        });
        that.tabContent.eq(that.index).height(win.height());
      }
      parent.height(that.tabContent.eq(that.index).outerHeight(true)).show();
    },
    update: function(){
      var plugin = this,
        that = plugin.element;
      that.options = plugin.options;
      that.tab = that.find(that.options.tab);
      that.tabContent = that.find(that.options.tabContent);
      that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
      that.freezeBody = false;
    },
    option: function(options) {
      this.element.options = $.extend({}, this.options, options);
    },
    changeTab: function(e) {
      var that = this.element,
          plugin = this;

      if($.isFunction(that.options.beforeChange)){
        that.options.beforeChange.call(that, that.tabContent, $(e));
      }

      that.tab.eq(that.index).removeClass(that.options.activeClass);
      if(that.options.animation){
        that.tabContent.eq(that.index).fadeOut(that.options.duration);
      }
      else{
        that.tabContent.eq(that.index).removeClass(that.options.activeClass);
      }

      that.index = that.tab.index($(e).addClass(that.options.activeClass));
      if(that.options.animation){
        that.tabContent.eq(that.index).css({
          'position': 'absolute',
          top: 80
        }).fadeIn(that.options.duration, function(){
          $(e).css('position', '');
        });
      }
      else{
        that.tabContent
          .removeClass(that.options.activeClass)
          .eq(that.index).addClass(that.options.activeClass);
        that.selectOnMobile.prop('selectedIndex', that.index);
      }
      // show as popup for tabMenu

      if(that.options.isPopup){
        var parent = that.tabContent.parent();
        var self = $(e);
        var langToolbarEl = $('.toolbar--language');
        that.winW = win.width();
        that.winH = win.height();
        isOpen = true;
        that.scrollTopHolder = win.scrollTop();

        if(langToolbarEl.length) {
          langToolbarEl.hide();
        }

        parent.show();
        tabContentOverlay = $(that.options.templateOverlay).appendTo(body).show().css({
          'opacity': 0.5,
          'zIndex': that.options.zIndex,
          'visibility': 'hidden'
        }).addClass('animated fadeInOverlay');
        // parent.css('position', 'fixed');
        parent.addClass('animated slideInRight');
        if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) < win.height()){
          parent.css('top', '').height(win.height());
        }
        parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
          if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) > win.height()){
            container.css('height', that.tabContent.eq(that.tab.index(self)).outerHeight(true));
          }
          else{
            that.tabContent.eq(that.tab.index(self)).height(win.height());
            container.css('height', win.height());
          }
          parent.css('top', '').height(that.tabContent.eq(that.tab.index(self)).outerHeight(true));
          $(plugin.options.container).css('overflow', 'hidden');
          isOpen = false;
          win.scrollTop(0);
          parent.css('position', '');
          parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
          if($.isFunction(that.options.afterChange)){
            that.options.afterChange.call(that, that.tabContent, $(e));
          }
          that.trigger('afterChange');
        });
        tabContentOverlay.off('click.closeTabContent').on('click.closeTabContent', function(e){
          e.preventDefault();
          that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
        });
        // var originalWidth = win.width();
        // win.off('resize.resetTabMenu').on('resize.resetTabMenu', function(){
        //  clearTimeout(tabMenuTimer);
        //  tabMenuTimer = setTimeout(function(){
        //    if(win.width() !== originalWidth) {
        //      originalWidth = win.width();
        //      that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
        //      return;
        //    }
        //    onResize();
        //  }, 10);
        // }).off('orientationchange.hide').on('orientationchange.hide', function() {
        //  that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
        // });
      }

      if($.isFunction(that.options.afterChange)){
        that.options.afterChange.call(that, that.tabContent, $(e));
      }
      that.trigger('afterChange');
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      } else {
        // console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
      }
    });
  };

  $.fn[pluginName].defaults = {
    tab: '',
    tabContent: '',
    activeClass: 'active',
    animation: false,
    duration: 500,
    isPopup: false,
    tablet: 988,
    container: '#container',
    mobile: 768,
    zIndex: 14,
    templateOverlay: '<div class="overlay"></div>',
    triggerClosePopup: '.tab-wrapper .popup__close',
    selectOnMobile: '> select',
    docScroll: window.Modernizr.touch ? 0 : 20,
    beforeClosePopup: function(){},
    afterChange: function() {},
    beforeChange: function(){}
  };

}(jQuery, window));

/**
 *  @name tooltip
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'kTooltip';
	var win = $(window);
	var doc = $(document);
	var tooltipOnDevice = null;

	var toggleDocScrollHandler = function(isSet, plugin) {
		if (isSet) {
			doc
				.off('scroll.hideTooltip mousewheel.hideTooltip')
				.on('scroll.hideTooltip mousewheel.hideTooltip', function(){
					plugin.closeTooltip();
				});
		}
		else {
			doc.off('scroll.hideTooltip mousewheel.hideTooltip');
		}
	};

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
			that = plugin.element;
			that.options = plugin.options;
			that.isShow = false;
			that.timer = 0;
			that.tooltip = $();
			that.maxWidth = plugin.options.maxWidth;
			if(true){
				that.bind('click.showTooltip', function(e){
					e.preventDefault();
					if(that.hasClass('disabled') && !that.data('open-tooltip')){
						return;
					}
					e.stopPropagation();
					if(tooltipOnDevice){
						tooltipOnDevice.tooltip.remove();
						tooltipOnDevice.tooltip = $();
						tooltipOnDevice.isShow = false;
						win.off('resize.hideTooltip');
						win.off('click.hideTooltip');
						// $(document).off('scroll.hideTooltip mousewheel.hideTooltip');
						toggleDocScrollHandler(false);
						tooltipOnDevice = null;
					}
					if(that.isShow){
						return;
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
			}
			else{
				that.off('click.showTooltip').on('click.showTooltip', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(that.isShow){
						return;
					}
					if(that.timer){
						clearTimeout(that.timer);
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
				// that.off('mouseleave.hideTooltip').on('mouseleave.hideTooltip', function(){
				// 	that.timer = setTimeout(function(){
				// 			that.tooltip.remove();
				// 			that.tooltip = $();
				// 			that.isShow = false;
				// 			win.off('resize.hideTooltip');
				// 		}, 200);
				// });
				// that.hover(
				// 	function(){
				// 		if(that.timer){
				// 			clearTimeout(that.timer);
				// 		}
				// 		plugin.generateTooltip();
				// 	},
				// 	function(){
				// 		that.timer = setTimeout(function(){
				// 			that.tooltip.remove();
				// 			that.tooltip = $();
				// 		}, 200);
				// 	}
				// );
			}
		},
		generateTooltip: function(){
			var plugin = this,
			that = plugin.element;
			tooltipOnDevice = that;
			if(that.tooltip.length){
				return;
			}
			// if(that.attr('data-type') === 2){
			// 	that.tooltip = $(that.options.template2).appendTo(document.body);
			// }
			// else if(that.attr('data-type') === 3){
			// 	that.tooltip = $(that.options.template3).appendTo(document.body);
			// }
			// else{
			// 	that.tooltip = $(that.options.template1).appendTo(document.body);
			// 	that.tooltip.find('a.popup_close').unbind('click.close').bind('click.close', function(e){
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		that.tooltip.remove();
			// 		that.tooltip = $();
			// 	});
			// }
			if(that.data('type') === 2){
				that.tooltip = $(that.options.template2).appendTo(document.body);
				that.tooltip.find('a.popup__close').off('click.close').on('click.close', function(e){
					e.preventDefault();
					e.stopPropagation();
					that.tooltip.remove();
					that.tooltip = $();
					that.isShow = false;
					win.off('resize.hideTooltip');
					// $(document).off('scroll.hideTooltip mousewheel.hideTooltip');
					toggleDocScrollHandler(false);
				});
			}
			else if(that.data('type') === 3){
				that.tooltip = $(that.options.template3).appendTo(document.body);
			}
			else{
				that.tooltip = $(that.options.template1).appendTo(document.body);
			}
			// that.tooltip = $(that.options.template1).appendTo(document.body);
			that.tooltip.show().find('.tooltip__content').html(that.attr('data-content'));
			that.tooltip
				.off('click.preventDefault')
				.on('click.preventDefault', function(e){
					e.stopPropagation();
				})
				.off('touchstart.hideTooltip')
				.on('touchstart.hideTooltip', function(){
					// e.stopImmediatePropagation();
					// doc.off('scroll.hideTooltip mousewheel.hideTooltip');
					toggleDocScrollHandler(false);
				})
				.off('touchend.hideTooltip')
				.on('touchend.hideTooltip', function(){
					toggleDocScrollHandler(true, plugin);
				})
				.find('a.tooltip__close').unbind('click.close').bind('click.close', function(e){
					e.preventDefault();
					/*e.stopPropagation();
					that.tooltip.remove();
					that.tooltip = $();*/
					plugin.closeTooltip();
				});

			var actualWidth = that.tooltip.outerWidth(true);
			if(that.data('max-width')){
				that.maxWidth = that.data('max-width');
			}
			if(actualWidth > that.maxWidth){
				actualWidth = that.maxWidth;
			}
			that.tooltip.innerWidth(that.maxWidth);
			that.tooltip.css({
				'max-width': that.tooltip.width()
			}).css('width', '');

			var left = Math.max(0, that.offset().left - that.tooltip.outerWidth(true)/2 + that.outerWidth(true)/2 + 5);

			var top = (that.data('type') === 3) ? that.offset().top  + that.innerHeight() : that.offset().top - that.tooltip.outerHeight() - 10;

			if((left + that.tooltip.outerWidth()) > $(window).width()){
				left = $(window).width() - that.tooltip.outerWidth() - 3;
			}

			that.tooltip.css({
				position: 'absolute',
				'z-index': 999999,
				// top: that.offset().top - that.tooltip.outerHeight() - that.height()/2,
				top: top,
				left: left
			});
			that.tooltip.find('.tooltip__arrow').css({
				left: that.offset().left - left + that.width()/2
			});

			// if(that.tooltip.find('img').length){
			// 	that.tooltip.css({
			// 		position: 'absolute',
			// 		'z-index': 999999,
			// 		// top: that.offset().top - that.tooltip.outerHeight() - that.height() - 50,
			// 		top: top,
			// 		left: left
			// 	});
			// 	that.tooltip.find('img').each(function(){
			// 		var newImg = new Image();
			// 		newImg.onload = function(){
			// 			left = that.offset().left - that.tooltip.width()/2 + that.width()/2;
			// 			if((left + that.tooltip.width()) > $(window).width()){
			// 				left = $(window).width() - that.tooltip.width();
			// 			}
			// 			that.tooltip.stop().animate({
			// 				width: that.tooltip.width(),
			// 				height: that.tooltip.outerHeight(),
			// 				// top: that.offset().top - that.tooltip.outerHeight() - that.height() - 50,
			// 				// top: that.offset().top - that.tooltip.outerHeight() - that.height(),
			// 				top: top,
			// 				left: left
			// 			}, 500);
			// 		};
			// 		newImg.src = $(this).attr('src');
			// 	});
			// }
			// else{
			// 	that.tooltip.css({
			// 		position: 'absolute',
			// 		'z-index': 999999,
			// 		// top: that.offset().top - that.tooltip.outerHeight() - that.height()/2,
			// 		top: top,
			// 		left: left
			// 	});
			// }

			win.off('click.hideTooltip').on('click.hideTooltip', function(){
				plugin.closeTooltip();
			});
			$('.popup .popup__inner').off('touchstart.hideTooltip').on('touchstart.hideTooltip', function(){
				plugin.closeTooltip();
			});
			win.off('resize.hideTooltip').on('resize.hideTooltip', function(){
				plugin.closeTooltip();
			});

			// $(document).off('scroll.hideTooltip mousewheel.hideTooltip').on('scroll.hideTooltip mousewheel.hideTooltip', function(){
			// 	plugin.closeTooltip();
			// });

			toggleDocScrollHandler(true, plugin);

			// that.tooltip.unbind('mouseenter mouseleave').bind({
			// 	'mouseenter': function(){
			// 		if(that.timer){
			// 			clearTimeout(that.timer);
			// 		}
			// 	},
			// 	'mouseleave': function(){
			// 		that.timer = setTimeout(function(){
			// 			that.tooltip.remove();
			// 			that.tooltip = $();
			// 		}, 100);
			// 	}
			// });
			if($.isFunction(that.options.afterShow)){
				that.options.afterShow.call(that, that.tooltip);
			}
		},
		closeTooltip: function(){
			var plugin = this,
				that = plugin.element;
			if(that.tooltip.length){
				that.isShow = false;
				that.tooltip.remove();
				that.tooltip = $();
				win.unbind('click.hideTooltip');
				// $(document).off('scroll.hideTooltip mousewheel.hideTooltip');
				toggleDocScrollHandler(false);
				tooltipOnDevice = null;
			}
			win.off('resize.hideTooltip');

			if($.isFunction(that.options.afterClose)){
				that.options.afterClose.call(that, that.tooltip);
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		afterShow: function(){},
		afterClose: function(){},
		maxWidth: 240,
		template1: '<div class="question-tooltips"><span class="tooltip-cont"></span><em class="ico-question-tooltips"></em></div>',
		template2: '<aside class="tooltip tooltip--info"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close">&#xe60d;</a><div class="tooltip__content"></div></aside>',
		template3: '<aside class="tooltip tooltip--conditions-1"><em class="tooltip__arrow type-top"></em><a href="#" class="tooltip__close">&#xe60d;</a><div class="tooltip__content"></div></aside>'
	};

}(jQuery, window));
