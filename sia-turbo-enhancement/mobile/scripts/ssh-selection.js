/**
 * @name SIA
 * @description Define global sshSelection functions
 * @version 1.0
 */
SIA.sshSelection = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var htmlOption = {
		'adult': {
			'single': '<option value="1">1</option>',
			'double': '<option value="1">1</option><option value="2">2</option>',
			'triple share': '<option value="1">1</option><option value="2">2</option><option value="3">3</option>'
		},
		'child': {
			'0': '<option value="0">0</option>',
			'1': '<option value="0">0</option><option value="1">1</option>',
			'2': '<option value="0">0</option><option value="1">1</option><option value="2">2</option>'
		},
		'error': '<p class="text-error" data-selection-error="true"><span>' + L10n.sshSelection.error + '</span></p>'
	};

	var initSelection = function() {
		var formCheckRoom = $('[data-check-room]');

		if (globalJson && globalJson.bookingSummary) {
			formCheckRoom.each(function() {
				var form = $(this);
				var accordionTriggers = form.find('[data-accordion-trigger]');
				var btnAddMore = form.find('[data-btn-addmore]');
				var wrapperRoom = form.find('[data-wrapper-room]');
				var bsJson = globalJson.bookingSummary.bookingSummary;
				var adult = parseInt(bsJson.adultCount);
				var child = parseInt(bsJson.childCount);
				var rowClone = form.find('[data-room-block]').first().clone();
				var paxNum = adult + child;
				var selectedpaxNum = 0;

				if (adult === 1) {
					btnAddMore.addClass('disabled');
				}

				var initSelectRoomAndPax = function(roomBlock) {
					selectedpaxNum = 0;
					if (roomBlock && roomBlock.length) {
						var customSelects = roomBlock.find('[data-customSelect]');
						var adultEl = customSelects.eq(0);
						var childEl = customSelects.eq(1);
						var roomEl = customSelects.eq(2);

						roomEl
							.off('change.selectRoom')
							.on('change.selectRoom', function() {
								var roomType = roomEl.find('select').val().toLowerCase();
								var roomPaxNum = roomType === 'single' ? 1: roomType === 'double'? 2 : 3;

								adultEl
									.find('select').empty().append(htmlOption.adult[roomType])
									.defaultSelect('refresh')
									.off('change.selectAdult')
									.on('change.selectAdult', function() {
										var adultNum = parseInt(adultEl.find('select').val());
										var childNum = roomPaxNum - adultNum;

										childEl
											.find('select').empty().append(htmlOption.child[childNum])
											.defaultSelect('refresh');
									})
									.trigger('change.selectAdult');
							})
							.trigger('change.selectRoom');

						if (roomBlock.closest('[data-wrapper-room]').siblings('[data-selection-error]').length) {
							roomBlock
								.closest('[data-wrapper-room]')
								.siblings('[data-selection-error]')
								.remove();
						}
					}
				};

				var addNewRow = function(el, newRowNumber) {
					var customSelectEl = el.find('[data-customselect]');
					el.find('[data-room-order]').text(L10n.ssh.room + ' ' + newRowNumber);
					customSelectEl.each(function() {
						var self = $(this);
						replaceFieldInfo(self, newRowNumber);
						// self.find('a').remove();
						// self.find('select').defaultSelect('refresh');
						self.find('select').defaultSelect({
							wrapper: '.custom-select',
							textCustom: '.select__text',
							isInput: false
						});
					});
					return el;
				};

				var removeRow = function(el) {
					var blockEl = el.parents('[data-room-block]'),
							wrapperRow = blockEl.parent(),
							listNextRows = blockEl.nextAll(),
							totalNextRows = listNextRows.length,
							totalRows;

					blockEl.remove();
					totalRows = wrapperRow.find('[data-room-block]').length;

					if(totalNextRows) {
						var j = totalRows;
						for(var i = totalNextRows - 1; i >= 0; i--) {
							var currentEl = listNextRows.eq(i);
							currentEl.find('[data-room-order]').text(L10n.ssh.room + ' ' + j);
							replaceFieldInfo(currentEl, j);
							j--;
						}
					}

					if(totalRows <= 1) {
						wrapperRow.find('[data-remove-block]').addClass('hidden');
					}
					if(totalRows < paxNum) {
						wrapperRow.siblings('[data-btn-addmore]').removeClass('disabled');
					}

					if (wrapperRow.siblings('[data-selection-error]').length) {
						wrapperRow.siblings('[data-selection-error]').remove();
					}
				};

				var replaceFieldInfo = function(el, newRowNumber) {
					var selectEl = el.find('select'),
							selectNewId = selectEl.attr('id').slice(0, -1) + newRowNumber;

					el.find('label').attr('for', selectNewId);
					selectEl.attr('id', selectNewId).attr('name', selectNewId);
				};

				btnAddMore.off('click.addSelection').on('click.addSelection', function(e) {
					e.preventDefault();
					var self = $(this),
						popupEl = self.parents('.popup'),
						wrapperRow = self.siblings('[data-wrapper-room]'),
						newRowNumber = wrapperRow.find('[data-room-block]').length + 1;

					if(self.hasClass('disabled')) {
						return;
					}

					if(newRowNumber >= adult) {
						self.addClass('disabled');
					}

					wrapperRow.append(addNewRow(rowClone.clone(), newRowNumber));
					wrapperRow.find('[data-remove-block]').removeClass('hidden');

					initSelectRoomAndPax(wrapperRow.find('[data-room-block]:last-child'));

					if(popupEl.length) {
						popupEl.data('Popup').reposition();
					}
				});

				wrapperRoom
					.off('click.removeSelection')
					.on('click.removeSelection', '[data-remove-block]', function(e) {
						e.stopPropagation();
						e.preventDefault();
						var self = $(this),
								popupEl = self.parents('.popup');
						removeRow(self);
						if(popupEl.length) {
							popupEl.data('Popup').reposition();
						}
					});

				var getSelectedPax = function(wrapper) {
					var selectedPax = 0,
						selectedAdult = 0,
						selectedChild = 0;

					wrapper.find('[data-room-block]').each(function() {
						var room = $(this);
						var roomSelects = room.find('[data-customSelect]');
						var adult = parseInt(roomSelects.eq(0).find('select').val());
						var child = parseInt(roomSelects.eq(1).find('select').val());
						selectedAdult += adult;
						selectedChild += child;
					});

					selectedPax += selectedAdult + selectedChild;

					return {
						selectedPax: selectedPax,
						selectedAdult: selectedAdult,
						selectedChild: selectedChild
					};
				};

				var checkFormSelectedValid = function(e) {
					var isValid = true;
					var errorIndex = -1;

					wrapperRoom.each(function(idx, wrapper) {
						var selectedPax = 0,
							selectedAdult = 0,
							selectedChild = 0;
						wrapper = $(wrapper);

						var selectedPaxObj = getSelectedPax(wrapper);
						selectedPax = selectedPaxObj.selectedPax;
						selectedAdult = selectedPaxObj.selectedAdult;
						selectedChild = selectedPaxObj.selectedChild;

						if (selectedPax !== paxNum || selectedAdult !== adult || selectedChild !== child) {
							if(!wrapper.siblings('[data-selection-error]').length) {
								$(htmlOption.error).insertBefore(wrapper.siblings('[data-btn-addmore]'));
							}
							isValid = false;
							errorIndex = errorIndex === -1 ? idx : errorIndex;
						}
						else {
							wrapper.siblings('[data-selection-error]').remove();
						}
					});

					if (!isValid) {
						e.preventDefault();
						if(errorIndex !== -1 && !accordionTriggers.eq(errorIndex).hasClass('active')) {
							accordionTriggers.eq(errorIndex).trigger('click.accordion');
						}
					}
				};

				form.off('submit.checkRoom').on('submit.checkRoom', function(e) {
					if (form.valid()) {
						checkFormSelectedValid(e);
					}
				});

				wrapperRoom.each(function() {
					var self = $(this);
					initSelectRoomAndPax(self.find('[data-room-block]').eq(0));
				});


				jQuery.validator.addMethod('checkBookingDate', function(value, element, param) {
					var bDateArr = $(param[1]).val().split('/');
					var cDateArr = value.split('/');
					var greaterDay = parseInt(param[0]);
					var bDate = new Date(bDateArr[2], parseInt(bDateArr[1]) - 1, bDateArr[0]);
					var cDate = new Date(cDateArr[2], parseInt(cDateArr[1]) - 1, cDateArr[0]);

					return cDate - bDate > greaterDay * 24 * 60 * 60 * 1000;
				}, L10n.validator.checkBookingDate);

				jQuery.validator.addMethod('checkCheckoutDate', function(value, element, param) {
					var bDateArr = $(param[2]).val().split('/');
					var cDateArr = value.split('/');
					var greaterDay = parseInt(param[0]);
					var lessDay = parseInt(param[1]);
					var bDate = new Date(bDateArr[2], parseInt(bDateArr[1]) - 1, bDateArr[0]);
					var cDate = new Date(cDateArr[2], parseInt(cDateArr[1]) - 1, cDateArr[0]);
					var nightTime = cDate - bDate;
					var ts = 24 * 60 * 60 * 1000;

					return nightTime > greaterDay * ts && nightTime <= lessDay * ts;
				}, L10n.validator.checkCheckoutDate);

				form.validate({
					focusInvalid: true,
					errorPlacement: global.vars.validateErrorPlacement,
					success: global.vars.validateSuccess
				});
			});
		}
	};

	var initModule = function() {
		win.off('finishLoadingBS.initSelection').on('finishLoadingBS.initSelection', function(){
			initSelection();
		});
	};

	initModule();
};
