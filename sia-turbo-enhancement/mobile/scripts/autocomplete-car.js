/**
 * @name SIA
 * @description Define global autocomplete search functions
 * @version 1.0
 */
SIA.autocompleteCar = function(){
  var vars = SIA.global.vars;
  var win = vars.win;
  var doc = vars.doc;
  var searchEl = $('[data-autocomplete-car]');
  var term = '';
  var timerResize = null;
  var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  var timerAutocompleteLangOpen = null;
  var source;

  // Init autocomplele search
  var initAutocompleteCar = function() {
    $('.overlay-loading').removeClass('hidden');
    searchEl.each(function(index, el) {
      var field = $(el).find('input'),
          dataJson,
          solrIndex = $(el).data('solrindex'),
          solrRows = $(el).data('solrrows'),
          sourceUrl = 'http://www.rentalcars.com/FTSAutocomplete.do';
      var searchAutoComplete = field.autocomplete({
        minLength: 1,
        open: function() {
          var self = $(this);
          self.autocomplete('widget').hide();
          clearTimeout(timerAutocompleteLangOpen);
          timerAutocompleteLangOpen = setTimeout(function(){
            self.autocomplete('widget').show().css({
              'left': self.parent().offset().left,
              'top': self.parent().offset().top + self.parent().outerHeight(true)
            });
            self.autocomplete('widget')
              .jScrollPane({
                scrollPagePercent: 10
              }).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
                e.preventDefault();
                e.stopPropagation();
              });
          }, 100);
        },
        source: function(request, response) {
          term = request.term;
          var getData;
            clearTimeout(getData);
            getData = setTimeout(function(){
             $.get(sourceUrl + '?solrIndex=' + solrIndex + '&solrRows=' + solrRows + '&solrTerm=' + term, function(data) {
               response(data.results.docs);
             });
             },1000);
        },
        search: function() {
          var self = $(this);
          self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
        },
        close: function() {
          var self = $(this);
          self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
        },
        select: function(evt, ui) {
          var menu = $(this).data("uiAutocomplete").menu.element;
          var itemLi = menu.find("li:has(a.ui-state-focus)");
          ui.item.value = $.trim(itemLi.text());
          var inputs = $(this).closest('form').find(':input');
          inputs.eq( inputs.index(this)+ 1 ).focus();
        }
      }).data('ui-autocomplete');
      searchAutoComplete._renderItem = function(ul, item) {
        if(item.name === L10n.globalSearch.noMatches) {
          return $('<li class="autocomplete-item"><a href="javascript:void(0);">No result found.</a></li>').appendTo(ul);
        } else {
          if($('.accordion__content--car') && $('.accordion__content--car').length) {
            if(item.placeType === "A") {
              return $('<li class="autocomplete-item" data-value="' + item.name + '"><a href="javascript:void(0);"><span class="icon ico-airplane"></span><span class="name">' + item.name + '</span><span class="air-iata">(' + item.iata + ')</span><span class="sub-desc">'+ (+item.city ? item.city + ', ': '<span class="hidden">, </span>') + (item.region ? (item.region + ', ') : '') + (item.country ? (item.country) : '') +'</span></a></li>')
              .appendTo(ul);
            } else if(item.placeType === "C" || item.placeType === "D" || item.placeType === "T") {
              return $('<li class="autocomplete-item" data-value="' + item.name + '"><a href="javascript:void(0);"><span class="icon ico-building-2"></span><span class="name">' + item.name  + '</span><span class="sub-desc">'+ (item.city ? item.city + ', ': '<span class="hidden">, </span>') + (item.region ? (item.region + ', ') : ' ') + (item.country ? (item.country) : '') +'</span></a></li>')
              .appendTo(ul);
            } else {
              return $('li');
            }
          } else {
            if(item.placeType === "A") {
              return $('<li class="autocomplete-item" data-value="' + item.name + '"><a href="javascript:void(0);"><span class="icon ico-airplane"></span><span class="name">' + item.name + '</span><span class="air-iata">(' + item.iata + ')</span><span class="sub-desc">'+ (+item.city ? item.city + ', ': '<span class="hidden">, </span>') + (item.region ? (item.region + ', ') : '') + (item.country ? (item.country) : '') +'</span></a></li>')
              .appendTo(ul);
            } else {
              return $('<li class="autocomplete-item" data-value="' + item.name + '"><a href="javascript:void(0);"><span class="icon ico-building-2"></span><span class="name">' + item.name  + '</span><span class="sub-desc">'+ (item.city ? item.city + ', ': '<span class="hidden">, </span>') + (item.region ? (item.region + ', ') : ' ') + (item.country ? (item.country) : '') +'</span></a></li>')
              .appendTo(ul);
            }
          }

        }
      };
      $('.overlay-loading').addClass('hidden');
      searchAutoComplete._resizeMenu = function () {
        this.menu.element.outerWidth(field.parent().outerWidth(true));
      };
      searchAutoComplete._move = function( direction ) {
        var item, previousItem,
          last = false,
          li = $(),
          minus = null,
          api = this.menu.element.data('jsp');

        if (!api) {
          if (!this.menu.element.is(':visible')) {
            this.search(null, event);
            return;
          }
          if (this.menu.isFirstItem() && /^previous/.test(direction) ||
              this.menu.isLastItem() && /^next/.test(direction) ) {
            this._value( this.term );
            this.menu.blur();
            return;
          }
          this.menu[direction](event);
        }
        else {
          var currentPosition = api.getContentPositionY();
          switch(direction){
            case 'next':
              if(this.element.val() === ''){
                api.scrollToY(0);
                li = this.menu.element.find('li:first');
                item = li.addClass('active').data( 'ui-autocomplete-item' );
              }
              else{
                previousItem = this.menu.element.find('li.active').removeClass('active');
                li = previousItem.next();
                item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
              }
              if(!item){
                last = true;
                li = this.menu.element.find('li').removeClass('active').first();
                item = li.addClass('active').data( 'ui-autocomplete-item' );
              }
              this.term = li.find('span.name').text() + li.find('span.sub-desc').text();
              this.element.val(this.term);
              if(last){
                api.scrollToY(0);
                last = false;
              }
              else{
                currentPosition = api.getContentPositionY();
                minus = li.position().top + li.innerHeight();
                if(minus - this.menu.element.height() > currentPosition){
                  api.scrollToY(Math.max(0, minus - this.menu.element.height()));
                }
              }
              break;
            case 'previous':
              if(this.element.val() === ''){
                last = true;
                li = this.menu.element.find('li:last');
                item = this.menu.element.find(li).addClass('active').data( 'ui-autocomplete-item' );
              }
              else{
                previousItem = this.menu.element.find('li.active').removeClass('active');
                li = previousItem.prev();
                item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
              }
              if(!item){
                last = true;
                item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
              }
              this.term = li.find('span.name').text() + li.find('span.sub-desc').text();
              this.element.val(this.term);
              if(last){
                api.scrollToY(this.menu.element.find('.jspPane').height());
                last = false;
              }
              else{
                currentPosition = api.getContentPositionY();
                if(li.position().top <= currentPosition){
                  api.scrollToY(li.position().top);
                }
              }
              break;
          }
        }
      };

      field.autocomplete('widget').addClass('autocomplete-menu');

      field.off('blur.autocomplete');
      field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll').on('scroll.preventScroll mousewheel.preventScroll', function(e){
        e.stopPropagation();
      });

      field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
        e.stopPropagation();
      });

      field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
       if(e.which === 13){
         e.preventDefault();
         $($('li.ui-menu-item.active').data('autocomplete')).trigger('click');
         $('ul.ui-autocomplete').css('display', 'none');
       }else if(e.keyCode === 9){
        $($('li.ui-menu-item.active').data('autocomplete')).trigger('click');
         $('ul.ui-autocomplete').css('display', 'none');
       }
      });

      field.off('blur.highlight').on('blur.highlight', function(){
        setTimeout(function(){
          field.closest('.custom-select').removeClass('focus');
          field.autocomplete('close');
        }, 200);
      });
    });
  };

  win.off('resize.blurSearch').on('resize.blurSearch', function() {
    clearTimeout(timerResize);
    timerResize = setTimeout(function() {
      var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if (winW !== width) {
        winW = width;
        var currentEl = searchEl.not(':hidden');
        if (currentEl.length) {
          var currentMenu = currentEl.autocomplete('widget');
          if (currentMenu.length && currentMenu.is(':visible')) {
            currentEl.autocomplete('close');
          }
        }
      }
    }, 50);
  });

  $('[data-autocomplete-car] input').each(function(){
    $(this).off('click.autocomplete').on('click.autocomplete', function() {
      if ($(this).is(':focus')) {
        var value = $(this).val();
        if(value !== '') {
          $(this).autocomplete('search', value);
        }
      }
    });
  });

  var initModule = function() {
    initAutocompleteCar();
  };

  initModule();
};
