﻿{  
   "sshList":[  
      {  
         "from":"伦敦 (LHR)",
         "to":"新加坡 (SIN)",
         "eligible":"false",
         "hotelName":"Carlton Hotel",
         "segmentID":"1",
         "checkInDate":"Sep 21, 2015 00:09:00 AM",
         "checkOutDate":"Sep 22, 2015 00:09:00 AM",
         "roomTypes":[  
            "SINGLE 1A SUPERIOR ROOM"
         ],
         "noOfRooms":"1",
         "packageInfo":"SSHSIN127987",
         "hotelImage":"/images/Promotions/SSH/Cat-C/carltonreceptionsmall.jpg"
      },
      {  
         "from":"新加坡 (SIN)",
         "to":"伦敦 (LHR)",
         "eligible":"false",
         "segmentID":"2",
         "roomTypes":[  

         ]
      },
      {  
         "from":"伦敦 (LHR)",
         "to":"新加坡 (SIN)",
         "eligible":"false",
         "hotelName":"Carlton Hotel",
         "segmentID":"3",
         "checkInDate":"Oct 04, 2015 00:10:00 AM",
         "checkOutDate":"Oct 05, 2015 00:10:00 AM",
         "roomTypes":[  
            "SINGLE 1A SUPERIOR ROOM"
         ],
         "noOfRooms":"1",
         "packageInfo":"SSHSIN127988",
         "hotelImage":"/images/Promotions/SSH/Cat-C/carltonreceptionsmall.jpg"
      },
      {  
         "from":"新加坡 (SIN)",
         "to":"伦敦 (LHR)",
         "eligible":"false",
         "segmentID":"4",
         "roomTypes":[  

         ]
      }
   ],
   "insuranceList":[  

   ],
   "locale":"zh_CN"
}