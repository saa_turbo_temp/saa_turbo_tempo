<% _.each( data.favouriteItems, function( city, idx ){ %>
  <div class="flight-item">
    <div class="flight-item__inner <%= city.expire ? "flight-item--expired" : "" %>">
      <a href="<%- city.shareurl %>"><img src="<%- city.imgUrl %>" alt="" longdesc="img-desc.html">
        <span class="flight-item__vignette">&nbsp;</span>
        <div class="flight-item__info-1">
          <p class="info-promotions"><%- city.city %>
            <%if (city.flighttype){%>
              <span><%- city.flighttype %></span>
            <%}%>
          </p>
        </div>
        <%if (city.price){%>
          <div class="flight-item__info-2">
            <p class="info-promotions"><span>From <%- city.currency %> </span><%- city.priceText %><sup>*</sup></p>
          </div>
        <%}%>
      </a>
      <% if(city.type === 'promo'){%>
        <div class="flight-item__status-1">
          <% if(city.expire) {%>
            <em class="bg-status bg-status--2"><%=city.type%> expired</em>
          <%} else {%>
            <em class="bg-status bg-status--1"><%=city.type%></em>
          <%}%>
        </div>
      <%} else if(city.expire){%>
        <div class="flight-item__status-1">
          <em class="bg-status bg-status--2"><%=city.type%> expired</em>
        </div>
      <%}%>
      <a href="javascript:;" class="flight-item__favourite <%=(city.favourite) ? 'favourited' : '' %>" data-id="<%= city.promoFareId %>"><em class="ico-star"><span class="ui-helper-hidden-accessible"><% if(city.favourite){ %>Click to unfavourite<% }else{ %>Click to mark as favourite<% } %></span></em><span class="loading loading--small hidden">Loading...</span></a>
    </div>
  </div>
<% })%>