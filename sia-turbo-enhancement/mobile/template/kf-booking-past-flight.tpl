<% _.each(data, function(flight, idx) { %>
  <tr class="<%- idx % 2 == 0 ? 'odd' : 'even' %> <%- showNumber && idx < showNumber ? '' : 'hidden' %>">
    <td>
      <div class="data-title"></div>
      <div class="custom-checkbox custom-checkbox--1 table-width-4">
        <input name="<%-flight.id%>" id="<%- flight.id %>" type="checkbox" <% if(flight.checked) { %>checked<%}%>>
        <label for="<%-flight.id%>">&nbsp;</label>
      </div>
    </td>
    <td class="date-of-travel">
      <div class="data-title">Date of travel</div>
      <span><%- flight.shortDate %></span>
    </td>
    <td>
      <div class="data-title">Airline</div>
      <span><%- flight.airline %></span>
    </td>
    <td>
      <div class="data-title">Flight number</div>
      <span><%- flight.number %></span>
    </td>
    <td>
      <div class="data-title">From</div>
      <span><%- flight.from %></span>
    </td>
    <td>
      <div class="data-title">To</div>
      <span><%- flight.to %></span>
    </td>
  </tr>
<% }); %>
