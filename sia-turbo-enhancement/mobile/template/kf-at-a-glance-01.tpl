<div class="booking-info">
  <div class="booking-info-item booking-info--width-1">
    <div class="booking-desc">
      <% if(data.flightInfor.flightNumber) { %>
        <span class="hour"><%- data.flightInfor.flightNumber %></span>
      <% } %>
      <div class="booking-content"><span><%- data.aircraft %></span>
      </div>
    </div>
  </div>
  <div class="booking-info-item booking-info--width-2">
    <div class="booking-desc">
      <span class="hour"><%- data.flightInfor.from.code %> <%- data.from.time %></span>
      <div class="booking-content"><span><%- data.from.date %>, <%- data.from.airport %></span>
      </div><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
    </div>
  </div>
  <div class="booking-info-item booking-info--width-2">
    <div class="booking-desc">
      <span class="hour"><%- data.flightInfor.to.code %> <%- data.to.time %></span>
      <div class="booking-content"><span><%- data.to.date %>, <%- data.to.airport %></span>
      </div>
    </div>
  </div>
  <div class="booking-info-item booking-info--width-3">
    <div class="booking-desc">
      <div class="booking-content"><span><%- data.checkinMessage %></span>
      </div>
    </div>
  </div>
</div>
