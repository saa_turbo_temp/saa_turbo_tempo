<% if (data && data.length) { %>
  <% if (data.isMobile) { %>
    <% _.each(data, function(city, idx) { %>
      <div class="static-item col-mb-3">
        <div class="static-item__info"><img src="<%- city.src %>" alt="" longdesc="<%- city.longdesc %>">
          <a href="<%- city.link %>">
            <div class="static-item__vignette-light">&nbsp;</div>
            <div class="static-item__detail">
              <span class="static-item__name"><%- city.name %></span><span class="static-item__link"><em class="ico-point-r"></em></span>
            </div>
          </a>
        </div>
      </div>
    <% }); %>
  <% } else { %>
    <% var startIdx = data.startIdx %>
    <% if (!data.isTablet) { %>
      <% var setNumber = Math.ceil((data.length + startIdx) / 5) %>
      <% var i = 0, j = 0, idx = 0 %>
      <% for (i = startIdx / 5 + 1; i <= setNumber; i++) { %>
        <% for (j = 0; j < 5; j++) { %>
          <% idx = (i - 1) * 5 + j %>
          <% if (idx < data.length + startIdx) { %>
            <% if (j === (i - 1) % 3) { %>
              <div class="static-item static-item--large col-mb-6">
                <div class="static-item__info">
                  <a href="<%- data[idx - startIdx].link %>">
                    <img src="<%- data[idx - startIdx].src %>" alt="" longdesc="<%- data[idx - startIdx].longdesc %>">
                    <div class="static-item__vignette-light">&nbsp;</div>
                    <div class="static-item__detail">
                      <span class="static-item__name"><%- data[idx - startIdx].name %></span><span class="static-item__link"><em class="ico-point-r"></em></span>
                    </div>
                  </a>
                </div>
              </div>
            <% } else { %>
              <div class="static-item col-mb-3">
                <div class="static-item__info">
                  <a href="<%- data[idx - startIdx].link %>">
                    <img src="<%- data[idx - startIdx].src %>" alt="" longdesc="<%- data[idx - startIdx].longdesc %>">
                    <div class="static-item__vignette-light">&nbsp;</div>
                    <div class="static-item__detail">
                      <span class="static-item__name"><%- data[idx - startIdx].name %></span><span class="static-item__link"><em class="ico-point-r"></em></span>
                    </div>
                  </a>
                </div>
              </div>
            <% } %>
          <% } %>
        <% } %>
      <% } %>
    <% } else { %>
      <% var setNumber = Math.ceil((data.length + startIdx) / 3) %>
      <% var i = 0, j = 0, idx = 0 %>
      <% for (i = startIdx / 3 + 1; i <= setNumber; i++) { %>
        <% for (j = 0; j < 3; j++) { %>
          <% idx = (i - 1) * 3 + j %>
          <% if (idx < data.length + startIdx) { %>
            <% if (j === (i - 1) % 2) { %>
              <div class="static-item static-item--large col-mb-6">
                <div class="static-item__info">
                  <a href="<%- data[idx - startIdx].link %>">
                    <img src="<%- data[idx - startIdx].src %>" alt="" longdesc="<%- data[idx - startIdx].longdesc %>">
                    <div class="static-item__vignette-light">&nbsp;</div>
                    <div class="static-item__detail">
                      <span class="static-item__name"><%- data[idx - startIdx].name %></span><span class="static-item__link"><em class="ico-point-r"></em></span>
                    </div>
                  </a>
                </div>
              </div>
            <% } else { %>
              <div class="static-item col-mb-3">
                <div class="static-item__info">
                  <a href="<%- data[idx - startIdx].link %>">
                    <img src="<%- data[idx - startIdx].src %>" alt="" longdesc="<%- data[idx - startIdx].longdesc %>">
                    <div class="static-item__vignette-light">&nbsp;</div>
                    <div class="static-item__detail">
                      <span class="static-item__name"><%- data[idx - startIdx].name %></span><span class="static-item__link"><em class="ico-point-r"></em></span>
                    </div>
                  </a>
                </div>
              </div>
            <% } %>
          <% } %>
        <% } %>
      <% } %>
    <% } %>
  <% } %>
<% } %>
