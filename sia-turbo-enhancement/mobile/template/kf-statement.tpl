<% _.each( data.statement, function( statement, idx ){ %>
  <tr class="<%= idx%2 === 0 ? 'odd' : 'even'  %>">
    <td>
      <div class="data-title"><%- data.headerMobile[0] %></div><span><%- statement.date %></span>
    </td>
    <td>
      <div class="data-title"><%- data.headerMobile[1] %></div><%- statement.type %>
    </td>
    <td>
      <div class="data-title"><%- data.headerMobile[2] %></div><%- statement.detail %>
    </td>
    <td>
      <div class="data-title"><%- data.headerMobile[3] %></div><%= statement.miles ? statement.miles : '-'  %>
    </td>
  </tr>
<%})%>

