<div class="block-2 add-ons-item block-travel">
    <h5 class="sub-heading-3--dark sub-title-travel">Travel insurance</h5>
    <div class="add-ons-item__content item-row">
        <div class="item-content-head item-content-head--top0 style">
            <div class="item-col-1 item-col-1--25" ><figure><img src="images/aig-logo.png" data-travel alt="Travel insurance"
                                                     longdesc="img-desc.html"></figure></div>
            <div class="item-col-2">
                <div class="content">
                    <div class="desc desc-travel" data-tabindex="true">
                        <p>Travel Insurance has been added to all passengers for this booking</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="add-ons-footer">
        <p>Contact&nbsp;<a href="#">AIG&nbsp;</a>for any changes</p><a href="#" class="link"><em
        class="ico-point-r"></em>Cancel</a>
    </div>
</div>

<% if(data.agodaBookingList !== void 0 && data.agodaBookingList.length > 0){ %>
    <% _.each(data.agodaBookingList, function(agodaBookingList, agodaBookingListIdx) { %>
        <div class="block-2 add-ons-item block-hotel">
            <h5 class="sub-heading-3--dark hotel-title" data-tabindex="true">Hotel</h5>
            <div class="hotel-infor addon-hotel-sales">
                <div class="item-content-head">
                    <div class="item-col-1">
                        <figure>
                            <img src="<%- agodaBookingList.hotelImage %>" data-hotel alt="<%- agodaBookingList.hotelName %>" longdesc="">
                        </figure>
                    </div>
                    <div class="item-col-2">
                        <div class="content">
                            <h3 data-tabindex="true" class="title-5--blue hotel-name"><%- agodaBookingList.hotelName %></h3>
                            <ul class="rating-block" aria-label="<%- agodaBookingList.starRating %> Star" data-tabindex=true>
                                <% if (agodaBookingList.starRating === "0.5" || agodaBookingList.starRating === 0.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "1" || agodaBookingList.starRating === 1) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "1.5" || agodaBookingList.starRating === 1.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "2" || agodaBookingList.starRating === 2) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "2.5" || agodaBookingList.starRating === 2.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "3" || agodaBookingList.starRating === 3) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "3.5" || agodaBookingList.starRating === 3.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "4" || agodaBookingList.starRating === 4) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "4.5" || agodaBookingList.starRating === 4.5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } else if (agodaBookingList.starRating === "5" || agodaBookingList.starRating === 5) { %>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                    <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                                    </li>
                                <% } %>
                            </ul>
                            <div data-tabindex="true" class="desc">
                                <p><%=agodaBookingList.addresLineOne%>, <%=agodaBookingList.city%>, <%=agodaBookingList.country%> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="add-ons-item__content item-row">
                <div class="item">
                    <ul class="info-details-3">
                        <li data-tabindex="true"> <span>Check-in date:</span><p class="checkin-date"><%=agodaBookingList.checkInDate%></p></li>
                        <li data-tabindex="true"><span>Check-out date:</span><p class="checkout-date"><%=agodaBookingList.checkOutDate%></p></li>
                        <li data-tabindex="true">
                            <span>Guests:</span>
                            <% if(parseInt(agodaBookingList.numberOfGuests) <= 1)  { %>
                                <%=agodaBookingList.numberOfGuests%> Adult
                            <% } else { %>
                                 <%=agodaBookingList.numberOfGuests%> Adults
                            <% } %>

                        </li>
                        <li data-tabindex="true"><span>Room types:</span><%=agodaBookingList.noOfRooms%> <%=agodaBookingList.roomTypes%></li>
                        <li data-tabindex="true"><span>No. of nights:</span><%=agodaBookingList.numberOfNights%></li>
                        <li data-tabindex="true"><span>Breakfast:</span>Included for all rooms</li>
                    </ul>
                </div>
                <div class="item">
                    <div class="price-room" data-tabindex="true">
                        <div class="total-title">Amount charged by Agoda</div>
                        <div class="total-cost">SGD 800.90</div>
                    </div>
                    <ul class="info-details-2" >
                        <li data-tabindex="true"><span>Agoda Reference ID:</span>12345678</li>
                        <li data-tabindex="true"><span>Booking status:</span>Booking received</li>
                    </ul>
                </div>
            </div>
            <div class="add-on-cantact">
                <figure class="img-block"><img src="images/agoda-logo.png" alt="photo-car" longdesc="img-desc.html">
                </figure>
                <div class="add-on__content">
                    <div data-tabindex="true" class="desc">
                        <p data-tabindex="true">To make changes to your booking, contact</p><a href="<%=agodaBookingList.selfServiceURL%>">Agoda Customer Support</a>
                        <p data-tabindex="true">Tel:&nbsp;<strong>+65 61234567</strong></p>
                    </div>
                </div>
            </div>
        </div>
    <% }) %>
<% } %>
<% if(data) { %>
    <div class="block-2 add-ons-item block-car">
        <h5 class="sub-heading-3--dark sub-title-car">Car rental</h5>
        <div class="car-infor">
            <div class="item-content-head">
                <div class="item-col-1">
                    <figure>
                        <img src="<%- data.response.vehicleDetails.imageURL %>" data-car alt="Car rental" longdesc="img-desc.html">
                    </figure>
                </div>
                <div class="item-col-2">
                    <div class="content">
                        <h6 class="title-5--blue name-car"><%- data.response.vehicleDetails.vehicleName %></h6>
                        <div class="desc">
                            <p class="sub-head">Premium</p>
                        </div>
                        <ul class="list-furnished">
                            <li><%- data.response.vehicleDetails.seats %> Seats</li>
                            <li><%- data.response.vehicleDetails.doors %> Doors</li>
                            <li><%- data.response.vehicleDetails.bigSuitcase %> Large bags</li>
                        </ul>
                        <ul class="list-furnished">
                            <li><% if(data.response.vehicleDetails.aircon === "Yes"){ %>Air Conditioning<% } else { %>
                            <% } %></li>
                            <li><%- data.response.vehicleDetails.automatic %> Tranmission</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="add-ons-item__content item-row">
            <div class="item">
                <ul class="info-details-1 style">
                    <li><span>Pick up:</span><p class="pick-up"><%- data.response.pickUp.dateAndTime %></p></li>
                    <li><span>Location:</span><%- data.response.pickUp.locationDetails.locationName %>, <%- data.response.pickUp.locationDetails.country %></li>
                </ul>
                <ul class="info-details-1 style">
                    <li><span>Drop off:</span><p class="drop-off"><%- data.response.dropOff.dateAndTime %></p></li>
                    <li><span>Location:</span><%- data.response.dropOff.locationDetails.locationName %>, <%- data.response.dropOff.locationDetails.country %></li>
                </ul>
            </div>
            <div class="item">
                <div class="price-room">
                    <div class="total-title">Amount charged by Rentalcars.com</div>
                    <div class="total-cost"><%- data.response.price.currencyCode %> <%- data.response.price.amount %></div>
                </div>
                <ul class="info-details-2">
                    <li><span>Transaction ID:</span><%- data.response.bookingId %></li>
                    <li><span>Booking status:</span><%- data.response.statusMsg %></li>
                </ul>
            </div>
        </div>
        <div class="add-on-cantact">
            <figure class="img-block"><img src="images/addon-rentalcars-logo.png" alt="photo-car" longdesc="img-desc.html">
            </figure>
            <div class="add-on__content">
                <div class="desc">
                    <p>To make changes to your booking, go to Rentalcars.com.</p>
                    <span><a href="#" class="link-4"><em class="ico-point-r"></em>Manage booking</a></span>
                </div>
            </div>
        </div>
    </div>
<% } %>
