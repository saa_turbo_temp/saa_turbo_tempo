<% _.each( data.list, function( list, idx ){ %>
  <article class="promotion-item promotion-item--2 fadeIn animated">
    <div class="promotion-item__inner">
        <a href="<%- list.shareurl %>" class="flight-item">
          <img src="<%- list.imgUrl %>" alt="Lorem ipsum dolor sit amet" longdesc="img-desc.html">
          <div class="flight-item__vignette">&nbsp;</div>
          <div class="flight-item__wrapper">
            <div class="flight-item__info-1">
              <p class="info-promotions"><%- list.destinationCityName %></p>
              <span class="text-from">From</span>
              <p class="info-promotions"><%- list.currency %> <%- list.priceFormat %><sup>*</sup>
              </p>
              <span><%- list.cabin %></span>
              <span><%- list.tripType %></span>
            </div>
          </div>
        </a>
      <div class="promotion-item__content">
        <div class="promotion-item__desc">
          <p class="promotion_text-dark">Travel period <%- list.durationStart %> - <%- list.durationEnd %></p>
          <p class="promotion_text-dark">Book by <%- list.bookBy %></p>
          <a href="<%- list.shareurl %>" class="link-2"><em class="ico-point-r">&nbsp;</em>Fare conditions</a>
        </div>
      </div>
    </div>
  </article>
<% }); %>
