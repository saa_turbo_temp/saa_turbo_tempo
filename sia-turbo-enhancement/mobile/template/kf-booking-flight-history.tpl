<% _.each(data, function(flight, idx) { %>
	<tr class="<%- idx % 2 == 0 ? 'odd' : 'even' %> <%- showNumber && idx < showNumber ? '' : 'hidden' %>">
		<td>
			<div class="data-title">Departure date</div>
			<span><%- flight.longDate %></span>
		</td>
		<td>
			<div class="data-title">Details</div>
			<%- flight.from %> to <%- flight.to %>
			<br />
			<%- flight.number %>
			<br />
			<%- flight.classType %>
		</td>
		<td class="align-right">
			<div class="data-title">KrisFlyer miles earned</div>
			<% if(flight.earned) { %>
				<%- flight.earned %>
			<% } else { %>
				-
			<% } %>
		</td>
		<td class="align-right">
			<div class="data-title">KrisFlyer miles redeemed</div>
			<% if(flight.redeemed) { %>
				<%- flight.redeemed %>
			<% } else { %>
				-
			<% } %>
		</td>
	</tr>
<% }); %>
