<aside class="popup popup--intro-lightbox hidden" >
  <div class="popup__inner">
    <div class="popup__content"><a href="#" class="popup__close" data-close="true"></a>
      <figure>
        <div class="img-wrapper"><img src="images/enhanced-light-box-1.png" alt="" longdesc="img-desc.html">
        </div>
        <div class="intro-block">
          <h2 class="popup__heading"><span>Welcome to the enhanced singaporeair.com</span></h2>
          <p class="sub-text-4">With a new look to improve your experience</p>
          <p class="sub-text-1--dark">Bringing you enhanced features progressively, starting with:</p>
          <ul class="intro-list">
            <li>Check-in</li>
            <li>Seat selection</li>
            <li>Promotions</li>
            <li>Flight status</li>
            <li>Flight schedules</li>
          </ul>
          <div class="button-group"><a href="#" class="btn-1" data-close="true">Continue</a>
          </div>
          <div class="custom-checkbox custom-checkbox--1" data-remember-cookie="true">
            <input name="remember-cb" id="remember-cb" type="checkbox">
            <label for="remember-cb">Don't show this to me again</label>
          </div>
        </div>
      </figure>
    </div>
  </div>
</aside>
