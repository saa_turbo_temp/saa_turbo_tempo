<span class="title-6--dark">About this voucher</span>
<p>Request a complimentary cabin class upgrade when you check in at the airport.</p>
<div class="voucher-list">
  <div class="item-row">
  <div class="item-cell">Airport upgrade voucher</div>
  <div class="item-cell">Voucher code XXXXXXXXX1</div>
  <div class="item-cell">Expires 30 Dec 2017</div>
  <div class="item-cell"><span class="status">Available</span></div>
  </div>
  <div class="item-row">
  <div class="item-cell">Airport upgrade voucher</div>
  <div class="item-cell">Voucher code XXXXXXXXX1</div>
  <div class="item-cell">Expires 30 Dec 2017</div>
  <div class="item-cell"><span class="status">Available</span></div>
  </div>
</div><span class="title-6--dark">Voucher usage</span>
<p>Airport upgrade vouchers can be used by both the principal PPS Club member and redemption nominees.</p>
