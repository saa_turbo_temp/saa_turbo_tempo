<% if(data.ticketStatus == 0) { %>
  <form method="post" class="hidden" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1 btn--block" data-checkin="0">Manage booking</button>
  </form>
<% } else if(data.ticketStatus == 1) { %>
  <form method="post" class="hidden" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1 btn--block" data-checkin="0">Check-in</button>
  </form>
<% } else if(data.ticketStatus == 2) { %>
  <form method="post" class="hidden" action="/checkIN-flow.form?pnr=<%-data.referenceNumber%>&lastName=<%-data.lastName%>">
    <button type="submit" class="btn-1 btn--block" data-checkin="0">Manage Check in</button>
  </form>
<% } else { %>
  <span><%- data.checkinMessage %></span>
<% } %>