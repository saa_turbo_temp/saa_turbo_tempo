/**
 * @name SIA
 * @description Define global CIB Booking Summary functions
 * @version 1.0
 */
SIA.CIBBookingSummary = function(){
	var global = SIA.global;
	var config = global.config;
	var body = global.vars.body;
	var flightSearch = $('.flights__searchs');
	var flightUpgrades = $('.flights-upgrade');
	var bookingSummaryWidget = $('.booking-summary');
	var collapseBsp = $('[data-collapse-bsp]');

	if(!bookingSummaryWidget.length){
		return;
	}

	var bspFirstTop = bookingSummaryWidget.offset().top;
	var bookingSummaryControl = bookingSummaryWidget.find('.booking-summary__heading .booking-summary__control');
	var bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');
	// var bookingSummaryOffset = bookingSummaryWidget.offset();
	var bookingGroupContent = bookingSummaryContent.find('[data-flight-cost]');

	// var fare = 0, taxes = 0, carrier = 0;
	var addonSubtotal = 0;
	var grandTotal = 0;
	var addons = [];
	var kkMiles = 0;
	var kkMilesRest = 0;
  var agodaTotal = 0;
	var infoFare = bookingSummaryWidget.find('[data-fare] span').last();
	var infoTaxes = bookingSummaryWidget.find('[data-taxes] span').last();
	var infoCarrier = bookingSummaryWidget.find('[data-carrier] span').last();
	var infoFlightSubtotal = bookingSummaryWidget.find('[data-subtotal] span').last();
	var infoGrandTotal = bookingSummaryWidget.find('[data-grandtotal]');
	var infoAddonList = bookingSummaryWidget.find('[data-addons]');
	var infoKKMilesRest = bookingSummaryWidget.find('[data-krisflyer-miles-rest]');
	// var formAddon = $('.form--addons');
	// var tableBaggages = $('.table-baggage');
	var tooltipPopup = $('.add-ons-booking-tooltip');
	var passengerCount = bookingSummaryWidget.find('.number-passengers');
	var infoPayableWithMiles = bookingSummaryWidget.find('[data-krisflyer-miles]');
	var totalToBePaid = $('[data-headtotal], [data-tobepaid]');
  var infoAgodaTotal = bookingSummaryWidget.find('[data-agoda-total]');

	// This function uses remove format Number
	var unformatNumber = function(number) {
		number = window.accounting.unformat(number);
		return parseFloat(number);
	};

	// This function uses format Number
	var formatNumber = function(number, fraction) {
		return window.accounting.formatNumber(number, (typeof(fraction) !== 'undefined') ? fraction : 2, ',', '.');
	};

	// Set the number of people booking
	var setPassengerCount = function() {
		if(globalJson.bookingSummary) {
			var bsinfo = globalJson.bookingSummary.bookingSummary;
			var html = '';
			if(bsinfo.adultCount) {
				html += bsinfo.adultCount + ' Adult' + (bsinfo.adultCount > 1 ? 's' : '');
			}
			if(bsinfo.childCount) {
				html += (html.length ? ', ' : '') + bsinfo.childCount + (bsinfo.childCount > 1 ? ' Children' : ' Child');
			}
			if(bsinfo.infantCount) {
				html += (html.length ? ', ' : '') + bsinfo.infantCount + ' Infant' + (bsinfo.infantCount > 1 ? 's' : '');
			}
			passengerCount.html(html);
		}
	};

	// Set state for radio button
	var preselectFlights = function() {
		if(globalJson.bookingSummary) {
			var flightInfo = globalJson.bookingSummary.fareAvailablityVO;
			if(flightInfo.dafaults) {
				for (var i = flightInfo.dafaults.length - 1; i >= 0; i--) {
					if(flightInfo.dafaults[i] !== null) {
						flightSearch.filter('[data-flight="' + (i + 1) + '"]').find('input[value="' + flightInfo.dafaults[i] + '"]').prop('checked', true).trigger('change.select-flight');
						$('[name="selectedFlightIdDetails[' + i + ']"]').val(flightInfo.dafaults[i]);
					}
				}
			}

			var radioEls = flightSearch.find('input:radio');

			//Check for enable flights
			radioEls.each(function() {
				if(!$.isEmptyObject(flightInfo.messages)) {
					var flightId = $(this).val();
					if(typeof(flightInfo.messages[flightId]) === 'undefined') {
						$(this).prop('disabled', true);
						$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
					}
					else {
						$(this).prop('disabled', false);
						$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
					}
				}
				else {
					$(this).prop('disabled', false);
					$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').removeClass('disabled');
				}
			});

			if($.isEmptyObject(flightInfo.messages)) {
				var firstFare = flightSearch.eq(0).find('input:checked:first');
				var isWaitlisted = firstFare.data('waitlisted');

				flightSearch.not(':first').find(isWaitlisted ? 'input[data-waitlisted="true"]' : 'input[data-waitlisted="false"]').each(function() {
					$(this).prop('disabled', true);
					$(this).closest('td.hidden-mb, .package--price').find('[data-tooltip]').addClass('disabled');
				});
			}

			radioEls.trigger('change.flightTableBorder');
		}
	};

	// Set booking sumary flight infomation
	var setBookingSummaryFlightInfo = function() {
		var bsinfo = globalJson.bookingSummary.bookingSummary;
		var flightsInfo = bookingSummaryWidget.find('[data-flight-info]');
		var secureFareInfo = globalJson.bookingSummary.tttVO;

		flightsInfo.empty();
		var flightsInfoHtml = '';
		for (var i = 0; i < bsinfo.flight.length; i++) {
			flightsInfoHtml += '<div class="flights-info">';
			flightsInfoHtml += '	<div class="flights-info-heading">';
			flightsInfoHtml += '		<h4>Flight ' + (i + 1) + '</h4>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.date + ' - ' + bsinfo.flight[i].flightSegments[0].deparure.time + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '	<div class="flights-info__country">';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[0].deparure.airportCode + '</span>';
			flightsInfoHtml += '		<span>' + bsinfo.flight[i].flightSegments[bsinfo.flight[i].flightSegments.length - 1].arrival.airportCode + '</span>';
			flightsInfoHtml += '	</div>';
			flightsInfoHtml += '</div>';
		}
		flightsInfo.html(flightsInfoHtml);

		infoFare.text(formatNumber(bsinfo.fareTotal));
		infoTaxes.text(formatNumber(bsinfo.taxTotal));
		infoCarrier.text(formatNumber(bsinfo.surchargeTotal));
		infoFlightSubtotal.text('Sgd ' + formatNumber(bsinfo.fareSubTotal));

		infoAddonList.empty();
		for (var i = 0; i < addons.length; i++) {
			var html = '';
			html += '<li class="addon--item"><span>' + addons[i].title + '</span>';
			html += '<span class="price">' + formatNumber(addons[i].price) + '</span>';
			// html += '<a href="javascript:void(0);" class="delete-btn"><em class="ico-close-round-fill"></em></a>';
			html += '</li>';
			var li = $(html);
			// li.find('.delete-btn').data('element', addons[i].element);
			infoAddonList.append(li);
		}

		var htmlEnd = '<li class="sub-total"><span>Sub-total</span><span class="price">Sgd ' + formatNumber(addonSubtotal) + '</span></li>';
		var liTotal = $(htmlEnd);
		var stringTotalPaid = '';
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		var payWithKfMiles = true;

		var totalPaidWithConvert = '';
		var convertPriceChb = $('.payment-currency').find('[data-toggler] input');
		var isConvertPriceChecked = convertPriceChb.length && convertPriceChb.is(':checked');
		var isPaymentPage = body.hasClass('payments-page');

		infoAddonList.append(liTotal);
    infoAgodaTotal.text('Sgd ' + formatNumber(agodaTotal));
		infoGrandTotal.text('Sgd ' + formatNumber(grandTotal));
		infoPayableWithMiles.text('Sgd ' + formatNumber(kkMiles));
		infoKKMilesRest.text('Sgd ' + formatNumber(kkMilesRest));

		if (kkMilesRest > 0) {
			stringTotalPaid += '<span class="unit">Sgd ' + formatNumber(kkMilesRest) + '</span>';

			if (isPaymentPage) {
				totalPaidWithConvert += '<span class="unit">Sgd ' + formatNumber(kkMilesRest) +
					'</span><span class="payment-currency-text' +
					(isConvertPriceChecked ? '' : ' hidden') + '">' +
					L10n.payment.convertText.format(formatNumber(kkMilesRest)) + '</span>';
			}
		}

		if(payWithKfMilesCheckbox.length && !payWithKfMilesCheckbox.is(':checked') &&
			!globalJson.bookingSummary.bookingSummary.milesAllocated) {
			payWithKfMiles = false;
		}

		if(kkMiles > 0 && payWithKfMiles) {
			// stringTotalPaid += '&nbsp;<small>+</small></span>';
			// stringTotalPaid += '<span class="miles">' + formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';

			if (!isPaymentPage) {
				stringTotalPaid += '<span class="miles">' +
					(kkMilesRest > 0 ? '<small>+</small>&nbsp;' : '') +
					formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';
			}
			else {
				totalPaidWithConvert += '<span class="miles">' +
					(kkMilesRest > 0 ? '<small>+</small>&nbsp;' : '') +
					formatNumber(kkMiles, 0) + ' KrisFlyer miles</span>';
			}
		}

		// totalToBePaid.html(stringTotalPaid);
		totalToBePaid.eq(0).html(stringTotalPaid);
		totalToBePaid.eq(1).html(isPaymentPage ? totalPaidWithConvert : stringTotalPaid);

		//secure fare
    var secureFare = '';
    var totalSecureFare = '';
    if(secureFareInfo.tttFare) {
      secureFare += '<div class="flights-cost" data-tabindex="true">' +
                    '<h4 class="flights-cost-title">' +
                      '<span class="text-left">SECURE FARE</span>' +
                      '<span class="text-right">&nbsp;</span>' +
                    '</h4>';

      secureFare += '<ul class="flights-cost__details">' +
                      '<li data-hold-fare="true">' +
                        '<span>Hold this fare</span>' +
                        '<span class="values">' +
                          formatNumber(secureFareInfo.tttFare, 2) +
                        '</span>' +
                      '</li>' +
                    '</ul></div>';

      $(secureFare).appendTo(bookingGroupContent);

      totalSecureFare += '<div class="grand-total">' +
                            '<p class="total-title" data-tabindex="true" data-aria-text="Secure this fare at">SECURE THIS FARE AT</p>' +
                            '<p class="total-info" data-tabindex="true" >' +
                              '<span class="unit">' + secureFareInfo.tttCurrency + ' ' + formatNumber(secureFareInfo.tttFare, 2) + '</span>' +
                            '</p>' +
                            '<p class="info-charge" data-tabindex="true">Includes taxes and surcharges</p' +
                          '</div>';

      $(totalSecureFare).appendTo(bookingGroupContent);
    }

		if (body.hasClass('payments-page') && isConvertPriceChecked) {
			convertPriceChb.trigger('change.exchange');
		}
	};

	// Attach a onchange to checkbox
	var payWithKfMilesChange = function() {
		var payWithKfMilesCheckbox = $('[data-pay-with-kfmile]');
		payWithKfMilesCheckbox.off('change.pay-with-miles').on('change.pay-with-miles', function() {
			setBookingSummaryFlightInfo();
		});
	};

	payWithKfMilesChange();

	// Caculate flight prices
	var calculateFlightPrices = function() {
		var bsinfo = globalJson.bookingSummary.bookingSummary;

		// if($('.add-ons-page').length) {
		// 	addonSubtotal = 0;
		// 	for (var i = 0; i < addons.length; i++) {
		// 		addonSubtotal += addons[i].price;
		// 	}
		// 	grandTotal = bsinfo.fareSubTotal + addonSubtotal;
		// 	kkMilesRest = grandTotal;
		// }
		// else {
		addonSubtotal = bsinfo.addonSubTotal;
		grandTotal = bsinfo.grandTotal;
		kkMilesRest = bsinfo.costPayableByCash;
		// }

		kkMiles = bsinfo.costPayableByMiles;
    var agodaTotal1, agodaTotal2;
    _.forEach(bsinfo.commonAddons, function(value, i){
      if(value.type === "Hotel") {
        if(value.hotelTaxAmount){
          agodaTotal1 = value.amount + value.hotelTaxAmount;
        }else{
          agodaTotal1 = value.amount;
        }
      }
      if(value.type === "Car Rental") {
        if(value.hotelTaxAmount){
          agodaTotal2 = value.amount + value.hotelTaxAmount;
        }else{
          agodaTotal2 = value.amount;
        }
      }
    });
    agodaTotal = agodaTotal1 + agodaTotal2;
	};

	// Print summary of fare conditions
	var printFareCondition = function(res) {
		if($('.flight-select-page').length) {
			var fareCondition = $('.summary-fare__conditions');
			var html = '';
			for(var i = 0; i < res.fareFamilyCondition.length; i++) {
				html += '<li>';
				if(res.fareFamilyCondition[i].isAllowed) {
					html += '<em class="ico-check-thick"></em>';
				}
				else {
					html += '<em class="ico-close"></em>';
				}
				html += res.fareFamilyCondition[i].description;
				html += '</li>';
			}
			fareCondition.html(html);
		}
	};

	// Print addons
	var printAddons = function(res) {
		if(res.bookingSummary.commonAddons) {
			for(var i = 0; i < res.bookingSummary.commonAddons.length; i++) {
				if(!checkExistAddon(res.bookingSummary.commonAddons[i].type)) {
					addons.push({
						title: res.bookingSummary.commonAddons[i].type,
						price: res.bookingSummary.commonAddons[i].amount,
						element: $()
					});
				}
			}
		}
	};

	// Render popup details
	var renderPopupDetails = function(res) {
		var isAddonPage = body.is('.add-ons-payment-page'),
        templateUrl = '';

    templateUrl = isAddonPage === true ? config.url.cibBookingSummaryDetailsPopupTemplateAddOn : config.url.cibBookingSummaryDetailsPopupTemplate;
    $.get(templateUrl, function(data) {
			if(!$('.add-ons-page, .payments-page').length) {
				res.bookingSummary.commonAddons = [];
			}
			var template = window._.template(data, {
				data: res,
				confirmationPage: $('.cib-confirmation-page').length
			});
			var popupContent = $('.popup--flights-details .popup__content');
			popupContent.children(':not(.popup__close)').remove();
			popupContent.append(template);
			popupContent.find('[data-need-format]').each(function() {
				var number = unformatNumber($(this).text());
				$(this).text(formatNumber(number, $(this).data('need-format')));
			});
		});
	};

	var renderSecurePopupDetails = function(res) {
    $.get(config.url.cibBookingSummarySfDetailsPopupTemplate, function(data) {
      if(!$('.add-ons-page, .payments-page').length) {
        res.bookingSummary.commonAddons = [];
      }
      var template = _.template(data, {
        data: res,
        confirmationPage: $('.cib-confirmation-page').length
      });
      var popupContent = $('.popup--flights-details-sf .popup__content');
      popupContent.children(':not(.popup__close)').remove();
      popupContent.append(template);
      popupContent.find('[data-need-format]').each(function() {
        var number = unformatNumber($(this).text());
        $(this).text(formatNumber(number, $(this).data('need-format')));
      });

      popupContent
        .find('.flights--detail span')
        .off('click.getFlightInfo')
        .on('click.getFlightInfo', function() {
          var self = $(this);
          var details = self.siblings('.details');
          if (details.is(':empty')) {
            $.ajax({
              url: config.url.flightSearchFareFlightInfoJSON,
              type: config.ajaxMethod,
              dataType: 'json',
              data: {
                flightNumber: self.parent().data('flight-number'),
                carrierCode: self.parent().data('carrier-code'),
                date: self.parent().data('date'),
                origin: self.parent().data('origin')
              },
              success: function(res) {
                self.children('em').toggleClass('ico-point-d ico-point-u');
                details.toggleClass('hidden');
                var html = '';
                html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
                for (var ft in res.flyingTimes) {
                  html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
                }
                details.html(html);
              },
              error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                if (textStatus !== 'abort') {
                  window.alert(errorThrown);
                }
              },
              beforeSend: function() {
                self.children('em').addClass('hidden');
                self.children('.loading').removeClass('hidden');
              },
              complete: function() {
                self.children('.loading').addClass('hidden');
                self.children('em').removeClass('hidden');
              }
            });
          } else {
            self.children('em').toggleClass('ico-point-d ico-point-u');
            if (details.is('.hidden')) {
              details.hide().removeClass('hidden').slideDown(400);
            }
            else {
              details.slideUp(400, function() {
                details.addClass('hidden');
              });
            }
          }
        });
    });
  };

	// Show message
	var showPeyMessage = function(res) {
		if($('.flight-select-page').length && res.bookingSummary.flight.length) {
			var flights = res.bookingSummary.flight;
			$.each(flights, function(idx, flight) {
				var fs = flightSearch.eq(idx);
				var packageMsg = fs.find('.package-message');
				var pkm = fs.find('input:checked').closest('div').find('.package-message');
				packageMsg.addClass('hidden');
				if (pkm.length && flight.cabinMismatch && flight.cabinMismatch === 'true') {
					pkm.removeClass('hidden');
				}
			});
		}
	};

	var BSPAjax;
	// Call Ajax for Booking sumary
	var callBSPAjax = function(onchange, extData, callback, radioEl) {
		var data = {
		};
		$.extend(data, extData);
		if(onchange) {
			flightSearch.each(function(i, it) {
				var selectedFlightId = $(it).find('input:radio:checked').first().val();
				if(selectedFlightId) {
					selectedFlightId = i + '|' + selectedFlightId.substring(2);
					data['selectedFlightIdDetails[' + i + ']'] = selectedFlightId;
				}
			});
		}
		if(BSPAjax) {
			BSPAjax.abort();
		}
		BSPAjax = $.ajax({
			url: onchange ? config.url.cibFlightSelectOnChange : config.url.cibFlightSelect,
			type: config.ajaxMethod,
			data: data,
			dataType: 'json',
			success: function(res) {
				globalJson.bookingSummary = res;

				if($('.payments-page').length) {
					var triggerCostPayableByCash = new jQuery.Event('change.costPayableByCash');
					triggerCostPayableByCash.cash = res.bookingSummary.costPayableByCash;
					bookingSummaryWidget.trigger(triggerCostPayableByCash);
				}

				printAddons(res);
				setPassengerCount();
				preselectFlights();
				calculateFlightPrices();
				setBookingSummaryFlightInfo();
				printFareCondition(res);
				renderPopupDetails(res);
				showPeyMessage(res);
				renderSecurePopupDetails(res);

				if ($('.ssh-additional-page').length && SIA.sshAdditional &&
					SIA.sshAdditional.setAdditional) {
					SIA.sshAdditional.setAdditional();
				}

				$(window).trigger('finishLoadingBS.initSelection');

				if(typeof(callback) === 'function') {
					callback();
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
				if(textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			},
			beforeSend: function() {
				passengerCount
				.add(totalToBePaid)
				.add(infoFare)
				.add(infoTaxes)
				.add(infoCarrier)
				.add(infoFlightSubtotal)
				.add(infoGrandTotal)
				.add(infoKKMilesRest)
				.add(infoPayableWithMiles)
				.text('...');
				infoAddonList.empty();

				if(body.hasClass('flight-select-page') && onchange && radioEl) {
					var bspInfo = $('.booking-summary').find('.booking-summary__info');
					bspInfo.find('.total-cost').addClass('hidden');
					bspInfo.find('.fare-notice').addClass('hidden');
					bspInfo.siblings('.loading--medium-2').removeClass('hidden');
					flightSearch.find('input:radio').not(radioEl).not(':checked').prop('disabled', true);
				}
				else {
					SIA.preloader.show();
				}
			},
			complete: function() {
				if (body.hasClass('flight-select-page')){
					var summaryFareP = $('.summary-fare').find('p').eq(0);
					var cloneBsp = $();

					summaryFareP.siblings('.booking-summary').remove();
					cloneBsp = bookingSummaryWidget.clone(true, true).insertAfter(summaryFareP);
					cloneBsp.addClass('visible-mb').find('[data-tooltip]').removeData('kTooltip').kTooltip();

					if(onchange) {
						var bspInfo = $('.booking-summary').find('.booking-summary__info');

						bspInfo.find('.total-cost').removeClass('hidden');
						bspInfo.find('.fare-notice').removeClass('hidden');
						bspInfo.siblings('.loading--medium-2').addClass('hidden');
            SIA.preloader.hide();
					}
				}
				else {
					SIA.preloader.hide();
				}
			}
		});
	};

	callBSPAjax(false, {}, function() {
		$('[data-flight]').each(function() {
			$(this).find('input:radio:checked').eq(0).trigger('change.select-flight');
		});
	});

	var paymentKFMiles = function() {
		bookingSummaryWidget.off('change.KfMiles').on('change.KfMiles', function(e) {
			callBSPAjax(true, {
				selectedMiles: e.miles
			}, e.callback);
		});
	};

	paymentKFMiles();

	// Check exist Addon
	var checkExistAddon = function(elm) {
		var exists = false;
		for (var i = addons.length - 1; i >= 0; i--) {
			if(typeof(elm) === 'object' && addons[i].element.is(elm)) {
				exists = true;
				return exists;
			}
			else if(typeof(elm) === 'string' && addons[i].title === elm) {
				exists = true;
				return exists;
			}
		}
		return exists;
	};

	// Fill data to table flight search
	var fillData = function() {
		flightSearch
		.off('change.fillData')
		.on('change.fillData', 'input[type="radio"]', function(e) {
			var radio = $(this);
			var tableIndex = flightSearch.index($(this).closest('.flights__searchs'));
			$('[name="selectedFlightIdDetails[' + tableIndex + ']"]').val($(this).val());
			// calculateFlightPrices();
			// setBookingSummaryFlightInfo();
			// changeTitleFare(this);
			callBSPAjax(true, {
				tripType: $(this).val()[0]
			}, function() {
				if(e.originalEvent) {
					toogleTooltip(tooltipPopup, L10n.bookingSummary.fare, /*this.dataset.fare*/ radio.next('label').find('strong.package--price-number').text());
				}
			}, radio);
		});

		flightUpgrades
		.off('change.fillData')
		.on('change.fillData', 'input[type="checkbox"]', function(e) {
			var checkbox = $(this);
			var isChecked = $(this).is(':checked');
			if(isChecked) {
				if(!checkExistAddon(checkbox)) {
					addons.push({
						title : checkbox.data('upgrade-title'),
						price : unformatNumber(checkbox.data('upgrade-price')),
						element : checkbox
					});
				}
			}
			else {
				for (var i = addons.length - 1; i >= 0; i--) {
					if(addons[i].element.is(checkbox)) {
						addons.splice(i, 1);
					}
				}
			}

			calculateFlightPrices();
			setBookingSummaryFlightInfo();
			if(e.originalEvent && isChecked) {
				toogleTooltip(tooltipPopup, checkbox.data('upgrade-title'), checkbox.data('upgrade-price'));
			}
		});

		flightUpgrades.off('clearAddOn').on('clearAddOn', function() {
			var radio = $(this).find('input[type="checkbox"]');
			for (var i = addons.length - 1; i >= 0; i--) {
				if(addons[i].element.is(radio)) {
					addons.splice(i, 1);
				}
			}

			calculateFlightPrices();
			setBookingSummaryFlightInfo();
		});

		// bookingSummaryWidget
		// .off('click.deleteAddon')
		// .on('click.deleteAddon', '.delete-btn', function(e) {
		// 	e.preventDefault();
		// 	var btn = $(this);
		// 	var elm = $(btn.data('element'));
		// 	if(elm.is('[type="radio"]') || elm.is('[type="checkbox"]')) {
		// 		elm.prop('checked', false).parent().removeClass('checked');
		// 		if(elm.closest('.baggage-1').length) {
		// 			elm.closest('.baggage-1').siblings('.baggage-3').find(':checkbox').prop({
		// 				'checked': false,
		// 				'disabled': true
		// 			}).parent().removeClass('checked').addClass('disabled');
		// 			elm.trigger('change');
		// 		}
		// 	}
		// 	$(this).closest('li').remove();

		// 	for (var i = addons.length - 1; i >= 0; i--) {
		// 		if(addons[i].element.is(elm)) {
		// 			addons.splice(i, 1);
		// 		}
		// 	}

		// 	calculateFlightPrices();
		// 	setBookingSummaryFlightInfo();
		// });
	};

	fillData();

	// Scroll popup
	var popupScroll = function() {
		var trigger = bookingSummaryWidget.find('[data-popup-anchor]');
		trigger.off('click.setAnchor').on('click.setAnchor', function() {
			var anchor = $(this).data('popup-anchor');
			var popup = $($(this).data('trigger-popup'));
			popup.data('anchor', anchor);
		});
		trigger.each(function() {
			var popup = $($(this).data('trigger-popup'));
			if(!popup.data('boundScroll')) {
				popup.data('boundScroll', true);
				popup.off('afterShow.scrollToAnchor').on('afterShow.scrollToAnchor', function() {
					var pop = $(this);
					window.setTimeout(function() {
						/*var content = pop.find('.popup__content');
						var paddingTop = parseInt(content.css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						content.scrollTop(0);
						if(Modernizr.mq('(max-width: 767px)')) {
							$('body').scrollTop(anchorElement.position().top - paddingTop);
						}
						else {
							content.scrollTop(anchorElement.position().top + content.scrollTop() - paddingTop);
						}*/
						var paddingTop = parseInt(popup.find('.popup__content').css('padding-top'), 10);
						var anchorElement = pop.find('[data-anchor="' + pop.data('anchor') + '"]');
						popup.scrollTop(0);
						// if(Modernizr.mq('(max-width: 767px)')) {
						// 	$('body').scrollTop(anchorElement.position().top - paddingTop);
						// }
						// else {
						if(anchorElement.length) {
							pop.scrollTop(anchorElement.position().top - paddingTop);
						}
						// }
						// Fixed bug hidden popup for iPad
						window.setTimeout(function() {
							pop.scrollTop(pop.scrollTop() + 1);
						}, 100);
					}, 1);
				});
			}
		});
	};

	popupScroll();

	var stickyWidget = function() {

		// Set position for sticky
		var setStickyPosition = function() {
			if($('.passenger-details-page, .seatsmap-page, .cib-confirmation-page').length) {
				return;
			}

			var fixedClassName = 'booking-summary--fixed';
			var footer = $('footer.footer');
			var bspRelative = $('.blk-heading');
			var bspRelativeOffset = bspRelative.offset();
			var bspRelativeTop = bspRelativeOffset.top;

			if($(window).scrollTop() >= bspFirstTop && Modernizr.mq('(min-width: ' + SIA.global.config.tablet + 'px)')) {
				var winHeight = $(window).height();
				var winTop = $(window).scrollTop();
				var bmHeight = bookingSummaryWidget.height();
				var footTop = footer.offset().top;
				var bspRelativeBottom = bspRelativeTop + bspRelative.outerHeight(true);
				var right = $(window).width() - $('.main-inner').width();
				right = right / 2;
				bookingSummaryWidget.addClass(fixedClassName).css('right', right);

				if(bmHeight <= winHeight) {
					if(winTop + bmHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: (bspRelativeBottom - footTop) + 'px'
						});
					}
					else {
						bookingSummaryWidget.css({
							top: '5px',
							bottom: ''
						});
					}
				}
				else {
					if(winTop + winHeight >= footTop) {
						bookingSummaryWidget.css({
							top: 'auto',
							bottom: (bspRelativeBottom - footTop) + 'px'
						});
					}
					else {
						bookingSummaryWidget.css({
							top: '5px',
							bottom: ''
						});
					}
				}
			}
			else {
				bookingSummaryWidget.removeClass(fixedClassName).css({
					right: '',
					top: '',
					bottom: ''
				});
			}
		};
		var originalWidth = $(window).width();
		$(window).on('resize.sticky-booking-summary', function() {
			if($(this).width() !== originalWidth) {
				setTimeout(function() {
					setStickyPosition();
				}, 200);
			}
		}).on('scroll.sticky-booking-summary', function() {
			setStickyPosition();
		}).trigger('scroll.sticky-booking-summary');

		return {
			setStickyPosition: setStickyPosition
		};
	};

	var sticky = stickyWidget();

	// Toggle Booking sumary
	var toggleBookingSummary = function() {
		bookingSummaryControl.off('click.openBS').on('click.openBS', function(e){
			e.preventDefault();
			if($('.orb-flight-select-page').length) {
				if($('[data-flight] input[type=radio]:checked').length === 0) {
					return;
				}
			}

			var bookingSummaryWidget = $(this).closest('.booking-summary');
			var bookingSummaryContent = bookingSummaryWidget.find('.booking-summary__content');

			if(window.innerWidth < SIA.global.config.tablet) {
				var popupSeatSelect = $('[data-infomations-1]'),
						popupSeatChange = $('[data-infomations-2]');
				if(popupSeatSelect.length) {
					popupSeatSelect.find('.tooltip__close').trigger('click');
				}
				if(popupSeatChange.length) {
					popupSeatChange.find('.tooltip__close').trigger('click');
				}
			}

			bookingSummaryWidget.toggleClass('active');
			bookingSummaryContent.toggle(0, function(){
				if(bookingSummaryWidget.hasClass('active') &&
					bookingSummaryWidget.closest('body').hasClass('add-ons-page')) {
					bookingSummaryWidget.closest('.main-inner').css('min-height',bookingSummaryWidget.outerHeight(true) + 'px');
				} else {
					bookingSummaryWidget.closest('.main-inner').css('min-height','');
				}
				$(window).trigger('scroll.sticky-booking-summary');
			});
			sticky.setStickyPosition();
		});

		if(collapseBsp.length){
			collapseBsp.off('click.openBS').on('click.openBS', function(e){
				e.preventDefault();
				$(this).closest('.booking-summary').find('.booking-summary__control').trigger('click.openBS');
			});
		}

		if($('.payments-page').length) {
			bookingSummaryControl.trigger('click.openBS');
		}

	};

	toggleBookingSummary();

	// var formAddonBS = function() {
	// 	formAddon.off('click.addon').on('click.addon', '[data-add-booking-summary]', function(e) {
	// 		var btn = $(this);
	// 		var pkg = {
	// 			title: btn.data('addon-title') ? btn.data('addon-title') : '...',
	// 			price: btn.data('addon-price') ? btn.data('addon-price') : 0,
	// 			element: btn
	// 		};

	// 		if(!checkExistAddon(btn)) {
	// 			addons.push(pkg);
	// 			calculateFlightPrices();
	// 			setBookingSummaryFlightInfo();
	// 			if(e.originalEvent) {
	// 				toogleTooltip(tooltipPopup, pkg.title, pkg.price);
	// 			}
	// 		}
	// 	});
	// };

	// formAddonBS();

	// var tableBaggageAddon = function() {
	// 	tableBaggages
	// 	.off('change.addBaggage')
	// 	.on('change.addBaggage', '.baggage-1 input[type="checkbox"]', function() {
	// 		var chb = $(this);
	// 		if(chb.is(':checked')) {
	// 			var add = {
	// 				title: chb.data('title') ? chb.data('title') : '...',
	// 				price: chb.data('price') ? unformatNumber(chb.data('price')) : 0,
	// 				element: chb
	// 			};

	// 			if(!checkExistAddon(chb)) {
	// 				addons.push(add);
	// 				calculateFlightPrices();
	// 				setBookingSummaryFlightInfo();
	// 			}
	// 		}
	// 		else {
	// 			for (var i = addons.length - 1; i >= 0; i--) {
	// 				if(addons[i].element.is(chb)) {
	// 					addons.splice(i, 1);
	// 				}
	// 			}
	// 			calculateFlightPrices();
	// 			setBookingSummaryFlightInfo();
	// 		}
	// 	})
	// 	.off('change.addBaggageOversize')
	// 	.on('change.addBaggageOversize', '.baggage-3 input[type="checkbox"]', function() {
	// 		var chbOversize = $(this);
	// 		var chb = $(this).closest('tr').find('.baggage-1 input[type="checkbox"]');
	// 		var oversizePrice = chbOversize.data('price') ? unformatNumber(chbOversize.data('price')) : 0;
	// 		for (var i = addons.length - 1; i >= 0; i--) {
	// 			if(addons[i].element.is(chb)) {
	// 				if(chbOversize.is(':checked')) {
	// 					if(!addons[i].addedBonus) {
	// 						addons[i].price += oversizePrice;
	// 						addons[i].addedBonus = true;
	// 					}
	// 				}
	// 				else {
	// 					if(addons[i].addedBonus) {
	// 						addons[i].price -= oversizePrice;
	// 						addons[i].addedBonus = false;
	// 					}
	// 				}
	// 			}
	// 		}

	// 		calculateFlightPrices();
	// 		setBookingSummaryFlightInfo();
	// 	});
	// };

	// tableBaggageAddon();

	// Toggle tooltip
	var toogleTooltip = function(tooltipElement, upperText, priceAdded) {
		if(!tooltipElement.hasClass('active') && !bookingSummaryWidget.hasClass('active')) {
			var overwriteTxtTooltip = tooltipElement.find('.tooltip__content');
			var position = bookingSummaryWidget.offset(),
					scrollTop = $(window).scrollTop(),
					posTop = Math.max(position.top - scrollTop, 0),
					/*posRight = window.innerWidth - position.left*/
					posRight = (document.body.clientWidth || $(window).width()) - position.left + 15;

			overwriteTxtTooltip.html(
				upperText +
				'<span class="text-1">+SGD ' + formatNumber(priceAdded) + '</span>'
			);
			if(window.innerWidth < SIA.global.config.tablet) {
				var posBottomBooking = position.top + bookingSummaryWidget.height();
				if(scrollTop > posBottomBooking) {
					posTop = 15;
				} else {
					posTop = posBottomBooking - scrollTop + 15;
				}
				posRight = position.left;
			}
			/*else if(window.innerWidth === SIA.global.config.tablet){
				posRight += 15;
			}*/

			tooltipElement.addClass('active').stop().css({
				position: 'fixed',
				top: posTop,
				right: posRight
			}).fadeIn(400).delay(2000).fadeOut(400, function() {
				$(this).removeClass('active');
			});
		}

		$(window).on('scroll.sticky-booking-summary', function() {
			if(bookingSummaryWidget.hasClass('booking-summary--fixed')) {
				tooltipElement.css('top', '5px');
			} else {
				tooltipElement.css('top', bookingSummaryWidget.offset().top - $(window).scrollTop());
			}

			if(window.innerWidth < SIA.global.config.tablet) {
				var posBottomBooking = bookingSummaryWidget.offset().top + bookingSummaryWidget.height();
				if($(window).scrollTop() > posBottomBooking) {
					tooltipElement.css('top', '15px');
				} else {
					tooltipElement.css('top', posBottomBooking - $(window).scrollTop() + 15);
				}
			}
		});
	};
};
