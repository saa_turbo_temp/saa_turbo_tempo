/**
 * @name SIA
 * @description Define function to init contact popup
 * @version 1.0
 */
SIA.contact = function() {
	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;
	var sms = $('.trigger-sms');
	var popupSMS = $('#sendPhoneNumber');
	var popupSMSContent = popupSMS.find('.popup__content > div');
	var formSMS = popupSMS.find('.form--phone-number');
	var sendFormSMS = $('.popup--phone-number .form--phone-number');
	var popupReSMS = $('.popup--phone-number');
	var isReSMS = false;

	popupSMS.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		closeViaOverlay: false
	}).find('#enter-phone-email-submit-1').off('click.confirm').on('click.confirm', function() {
		// e.preventDefault();
		if (formSMS.valid()) {
			isReSMS = true;
			popupSMSContent.eq(0).addClass('hidden');
			popupSMSContent.eq(1).removeClass('hidden');

			setTimeout(function() {
				popupSMS.Popup('reset');
				popupSMS.Popup('reposition');
			}, 100);
		}
	});
	popupReSMS.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		closeViaOverlay: false
	});

	sms.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
		e.preventDefault();
		if (isReSMS) {
			popupReSMS.Popup('show');
		} else {
			popupSMS.Popup('show');
		}
	});

	// email
	var emailTrigger = $('.trigger-email');
	var popupEmailAddress = $('#sendEmail');
	var popupEmailAddressContent = popupEmailAddress.find('.popup__content > div');
	var emailForm = popupEmailAddress.find('form');
	// var popupEmailAddress = $('.popup--email-address1');
	// var popupEmailConfirm = $('.popup--email-confirm1');

	popupEmailAddress.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterHide: function() {
			popupEmailAddressContent.eq(1).addClass('hidden');
			popupEmailAddressContent.eq(0).removeClass('hidden');
		},
		closeViaOverlay: false
	}).find('#email-confirm-submit-2').off('click.confirm').on('click.confirm', function() {
		// e.preventDefault();
		if (emailForm.valid()) {
			popupEmailAddressContent.eq(0).addClass('hidden');
			popupEmailAddressContent.eq(1).removeClass('hidden');

			setTimeout(function() {
				popupEmailAddress.Popup('reset');
				popupEmailAddress.Popup('reposition');
			}, 100);
		}
	});
	popupEmailAddress.find('#email-address-submit-3').off('click.addEmail').on('click.addEmail', function(e) {
		e.preventDefault();
		var newInput = $(config.template.addEmail.format(($(this).closest('.table-row').prev().children().length + 1), L10n.validation.email)).appendTo($(this).closest('.table-row').prev());
		if (!window.Modernizr.input.placeholder) {
			newInput.find('input').placeholder();
			newInput.find('input').addClear();
		}
	});

	emailTrigger.off('click.showCancelPopup').on('click.showCancelPopup', function(e) {
		e.preventDefault();
		popupEmailAddress.Popup('show');
	});
	var validateFormGroup = function(formGroup) {
		formGroup.each(function() {
			var self = $(this);
			var doValidate = function(els) {
				var pass = true;
				els.each(function() {
					if (!pass) {
						return;
					}
					pass = $(this).valid();
					// fix for checkin- sms
					if (els.closest('[data-validate-col]').length && pass) {
						els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
					}
				});
			};
			self.off('click.triggerValidate').on('click.triggerValidate', function() {
				formGroup.each(function() {
					if ($(this).data('change')) {
						doValidate($(this).find('select, input'));
					}
				});
			});

			self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function() {
				formGroup.not(self).each(function() {
					if ($(this).data('change')) {
						doValidate($(this).find('select, input'));
					}
				});
			}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function() {
				self.data('change', true);
			});

			self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function() {
				self.data('change', true);
				$(this).valid();
			}).off('blur.passengerDetail').on('blur.passengerDetail', function() {
				if ($(this).val()) {
					self.data('change', true);
				} else {
					// fix for checkin- sms
					if ($(this).closest('[data-validate-col]').length) {
						doValidate($(this).closest('[data-validate-row]').find('input'));
					}
				}
			});

		});
	};

	var initValidateSMS = function() {
		formSMS.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				return false;
			}
		});
		validateFormGroup(formSMS.find('.table-row'));
	};

	initValidateSMS();

	var initValidateSendSMS = function() {
		sendFormSMS.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
		});
		validateFormGroup(sendFormSMS.find('.table-row'));
	};

	initValidateSendSMS();

	var initValidateEmail = function() {
		emailForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				return false;
			},
			invalidHandler: function(form, validator) {
				if (validator.numberOfInvalids()) {
					var divScroll = win.width() > global.config.mobile ? emailForm.find('[data-scroll-content]') : emailForm.closest('.popup'),
						scrollTo = 0;
					divScroll.scrollTop(scrollTo);
					scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top;
					divScroll.scrollTop(scrollTo);
				}
			}
		});
	};

	initValidateEmail();
};
