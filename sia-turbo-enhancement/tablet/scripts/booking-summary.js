/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
SIA.bookingSummnary = function(){
	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;

	$.validator.addMethod('validateEmail', function(value) {
		if(!value.length){
			return true;
		}
		// return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
		var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return emailReg.test(value);
	}, L10n.validator.validateEmail);

	$.validator.addMethod('validateAtLeastOne', function(value, el, param) {
		var form = $(el).closest('form');
		var allField = form.find(param);
		var valid = false;
		allField.find('input:text, input[type="tel"]').each(function(){
			if($.trim($(this).val()) !== ''){
				valid = true;
				return false;
			}
		});
		return valid;
	}, L10n.validator.validateAtLeastOne);

	//var mobileDevice = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
	if(!$('.sia-breadcrumb').length || $('.passenger-details-page').length){
		return;
	}
	// init checkAllList function
	var booking = $('.booking');
	booking.each(function(){
		var bookingPassengerHeading = $('.booking-passenger-heading', this);
		var btn = $('.btn-1', this);
		bookingPassengerHeading.each(function(i){
			var self = $(this);
			global.vars.checkAllList(self.find(':checkbox'), self.siblings('.booking-passenger-list'), function(con){
				if(!i){
					if(con){
						btn.removeClass('disabled').prop('disabled', false);
					}
					else{
						btn.addClass('disabled').prop('disabled', true);
					}
				}
			});
		});
	});

	// $('.booking-control a.btn-1').off('click.disabled').on('click.disabled', function(e){
	// 	if($(this).hasClass('disabled')){
	// 		e.preventDefault();
	// 	}
	// });

	// // init uncheck checkin procedure
	// var uncheckProcudure = function(wrap){
	// 	wrap.each(function(i){
	// 		var self = $(this);
	// 		if(i === 0){
	// 			self.find('input:checkbox').each(function(ii){
	// 				var chbox = $(this);
	// 				$(this).off('change.uncheck').on('change.uncheck', function(){

	// 				});
	// 			});
	// 		}
	// 		else{

	// 		}
	// 	});
	// };

	// Init accordion
	var initAccordion = function(){
		if($('.main-inner .booking').length){
			$('.main-inner').accordion({
				wrapper: '.booking .booking-item',
				triggerAccordion: 'a.booking-passenger__control',
				contentAccordion: 'div.booking-passenger-content',
				activeClass: 'active',
				duration: 600
			});
		}
	};

	/*var enableCheckboxes = function(trigger, trigger2, nextFlight, passenger1, passenger2, openNextFlight){
		trigger.off('change.enableCheckboxes').on('change.enableCheckboxes', function(){
			if(trigger.is(':checked')){
				nextFlight.removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': false
				}).parent().removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', false).parent().removeClass('disabled');
				if(!openNextFlight.hasClass('active')){
					openNextFlight.trigger('click.accordion');
				}
			}
			else{
				nextFlight.addClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': true,
					'checked': false
				}).parent().addClass('disabled').eq(0).closest('ul').prev().find(':checkbox').prop({
					'checked': false
				});
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', true).parent().addClass('disabled');
			}
		});
		if(trigger.is(':checked')){
			trigger.trigger('change.checkAllList').trigger('change.enableCheckboxes');
		}

		passenger1.each(function(idx){
			var self = $(this);
			self.off('change.selectPassenger').on('change.selectPassenger', function(){
				if(self.is(':checked')){
					trigger2.prop('disabled', false).parent().removeClass('disabled');
					passenger2.eq(idx).prop({
						'disabled': false
					}).parent().removeClass('disabled');
					passenger2.eq(idx).trigger('change.checkAllList');
					if(!openNextFlight.hasClass('active')){
						openNextFlight.trigger('click.accordion');
					}
					if(nextFlight.hasClass('disabled')){
						nextFlight.removeClass('disabled');
					}
				}
				else{
					passenger2.eq(idx).prop({
						'disabled': true,
						'checked': false
					}).parent().addClass('disabled');
					if(!passenger1.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).is(':checked')){
						nextFlight.addClass('disabled');
						trigger2.prop('disabled', true).parent().addClass('disabled');
					}
					passenger2.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).trigger('change.checkAllList');
				}
			});
		});
	};

	var bookingCheckboxAllFirstFlight = $('#booking-checkbox-all-1');
	var bookingCheckboxAllSecondFlight = $('#booking-checkbox-all-2');
	var wrapperFirstFlight = bookingCheckboxAllFirstFlight.closest('.booking-item');
	var nextFlight = wrapperFirstFlight.siblings('.booking-item');
	initAccordion();
	enableCheckboxes(bookingCheckboxAllFirstFlight, bookingCheckboxAllSecondFlight, nextFlight.children('.sub-heading-3--dark'), bookingCheckboxAllFirstFlight.closest('.booking-passenger-heading').next().find(':checkbox'), nextFlight.find(':checkbox').not('#booking-checkbox-all-2'), nextFlight.find('a.booking-passenger__control'));*/

	// Enable checkbox
	var enableCheckboxes = function(trigger, trigger2, nextFlight, passenger1, passenger2, openNextFlight){
		var trigger2Length = trigger2.length,
				passenger2Length = passenger2.length,
				loopCheckbox = passenger2Length / trigger2Length;

		trigger.off('change.enableCheckboxes').on('change.enableCheckboxes', function(){
			if(trigger.is(':checked')){
				nextFlight.removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': false
				}).parent().removeClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', false).parent().removeClass('disabled');
				if(!openNextFlight.hasClass('active')){
					openNextFlight.trigger('click.accordion');
				}
			}
			else{
				nextFlight.addClass('disabled');
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).prop({
					'disabled': true,
					'checked': false
				}).parent().addClass('disabled').eq(0).closest('ul').prev().find(':checkbox').prop({
					'checked': false
				});
				passenger2.filter(function(){
					return !$(this).parent('.custom-checkbox--checked-in').length;
				}).trigger('change.checkAllList');
				trigger2.prop('disabled', true).parent().addClass('disabled');
			}
		});

		if(trigger.is(':checked')){
			trigger.trigger('change.checkAllList').trigger('change.enableCheckboxes');
		}
		passenger1.each(function(idx){
			var self = $(this);
			self.off('change.selectPassenger').on('change.selectPassenger', function(){
				if(self.is(':checked')){
					trigger2.prop('disabled', false).parent().removeClass('disabled');

					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).prop({
						'disabled': false
					}).parent().removeClass('disabled');

					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).trigger('change.checkAllList');

					// if(!openNextFlight.hasClass('active')){
					// 	openNextFlight.trigger('click.accordion');
					// }

					openNextFlight.each(function(){
						var self = $(this);
						if(!self.hasClass('active')){
							self.addClass('active').siblings('.booking-passenger-content').slideDown(400);
						}
					});

					if(nextFlight.hasClass('disabled')){
						nextFlight.removeClass('disabled');
					}
				}
				else{
					passenger2.filter(function(index) {
						return index % loopCheckbox === idx;
					}).prop({
						'disabled': true,
						'checked': false
					}).parent().addClass('disabled');

					if(!passenger1.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).is(':checked')){
						nextFlight.addClass('disabled');
						trigger2.prop('disabled', true).parent().addClass('disabled');
					}

					passenger2.filter(function(){
						return !$(this).parent('.custom-checkbox--checked-in').length;
					}).trigger('change.checkAllList');
				}
			});
		});
	};

	var bookingCheckboxAllFirstFlight = $('#booking-checkbox-all-1');
	var bookingCheckboxAllOtherFlight = $('[data-checkbox-all]').not(bookingCheckboxAllFirstFlight);
	var wrapperFirstFlight = bookingCheckboxAllFirstFlight.closest('.booking-item');
	var nextFlight = wrapperFirstFlight.siblings('.booking-item');
	initAccordion();

	enableCheckboxes(
		bookingCheckboxAllFirstFlight,
		bookingCheckboxAllOtherFlight,
		nextFlight.children('.sub-heading-3--dark'),
		bookingCheckboxAllFirstFlight.closest('.booking-passenger-heading').next().find(':checkbox'),
		nextFlight.find(':checkbox').not(bookingCheckboxAllOtherFlight),
		nextFlight.find('a.booking-passenger__control')
	);

	var formTwo = $('#form-booking-2');
	formTwo.find(':checkbox').each(function() {
		if($(this).is(':checked')) {
			formTwo.find('input[type="submit"]').removeClass('disabled').prop('disabled', false);
			formTwo.find('.booking-passenger__control').trigger('click.accordion');
			return;
		}
	});

	var flyingFocus = $('#flying-focus');

	// cancel popup
	var cancelCheckIn = $('.cancel-flight');
	var cancelAllFlight = $('.cancel-all-flight');
	var popupCancelFlight = $('.popup--checkin-cancel');
	var popupCancelFlightConfirm = $('.popup--checkin-cancel-confirm');
	var popupCancelAllFlight = $('.popup--checkin-cancel-all');
	var popupCancelAllFlightConfirm = $('.popup--checkin-cancel-all-confirm');
	var popupCheckinErrorMessage = $('.popup--checkin-error-message');

	popupCancelFlight.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#success-flight-submit').off('click.confirm').on('click.confirm', function(e){
		e.preventDefault();
		popupCancelFlight.Popup('hide');
		// fix for Safari
		setTimeout(function(){
			popupCancelFlightConfirm.Popup('show');
		}, 200);
	});

	popupCheckinErrorMessage.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: true
	});

	popupCancelAllFlight.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#cancel-checkbox-cancel-submit').off('click.confirm').on('click.confirm', function(e){
		e.preventDefault();
		popupCancelAllFlight.Popup('hide');
		// fix for Safari
		setTimeout(function(){
			popupCancelAllFlightConfirm.Popup('show');
		}, 200);
	});

	popupCancelFlightConfirm.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	});

	popupCancelAllFlightConfirm.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	});

	cancelCheckIn.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(!$(this).hasClass('disabled')){
				if(self.hasClass('first-flight')){
					popupCheckinErrorMessage.Popup('show');
				}
				else{
					popupCancelFlight.Popup('show');
				}
			}
		});
		self.closest('li').prev().find('a').off('click.changeSeat').on('click.changeSeat', function(e){
			if($(this).hasClass('disable')){
				e.preventDefault();
			}
		});
	});

	cancelAllFlight.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(self.hasClass('disabled')){
				return;
			}
			if(self.hasClass('first-flight')){
				popupCheckinErrorMessage.Popup('show');
			}
			else{
				popupCancelAllFlight.Popup('show');
			}
		});
	});

	popupCancelFlight.find('.table-default > .table-row--heading').each(function(){
		global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
	});

	popupCancelAllFlight.find('.table-default > .table-row--heading').each(function(){
		global.vars.checkAllList($(this).find(':checkbox'), $(this).next());
	});

	// sms popup
	// var sms = $('.trigger-sms');
	// var popupSMS = $('#sendPhoneNumber');
	// var popupSMSContent = popupSMS.find('.popup__content > div');
	// var formSMS = popupSMS.find('.form--phone-number');
	// var sendFormSMS = $('.popup--phone-number .form--phone-number');
	// // var popupSMS = $('.popup--enter-phone-number1');
	// var popupReSMS = $('.popup--phone-number');
	// // var popupSuccessSent = $('.popup--successfully-sent1');
	// var isReSMS = false;

	// popupSMS.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	closeViaOverlay: false
	// }).find('#enter-phone-email-submit-1').off('click.confirm').on('click.confirm', function(){
	// 	// e.preventDefault();
	// 	if(formSMS.valid()) {
	// 		isReSMS = true;
	// 		popupSMSContent.eq(0).addClass('hidden');
	// 		popupSMSContent.eq(1).removeClass('hidden');

	// 		setTimeout(function(){
	// 			popupSMS.Popup('reset');
	// 			popupSMS.Popup('reposition');
	// 		}, 100);
	// 	}
	// });
	// popupReSMS.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	closeViaOverlay: false
	// });

	// sms.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
	// 	e.preventDefault();
	// 	if(isReSMS){
	// 		popupReSMS.Popup('show');
	// 	}
	// 	else{
	// 		popupSMS.Popup('show');
	// 	}
	// });

	// // email
	// var emailTrigger = $('.trigger-email');
	// var popupEmailAddress = $('#sendEmail');
	// var popupEmailAddressContent = popupEmailAddress.find('.popup__content > div');
	// var emailForm = popupEmailAddress.find('form');
	// // var popupEmailAddress = $('.popup--email-address1');
	// // var popupEmailConfirm = $('.popup--email-confirm1');

	// popupEmailAddress.Popup({
	// 	overlayBGTemplate: config.template.overlay,
	// 	modalShowClass: '',
	// 	triggerCloseModal: '.popup__close, .btn-back-booking',
	// 	afterShow: function(){
	// 		flyingFocus = $('#flying-focus');
	// 		if(flyingFocus.length){
	// 			flyingFocus.remove();
	// 		}
	// 	},
	// 	afterHide: function(){
	// 		popupEmailAddressContent.eq(1).addClass('hidden');
	// 		popupEmailAddressContent.eq(0).removeClass('hidden');
	// 	},
	// 	closeViaOverlay: false
	// }).find('#email-confirm-submit-2').off('click.confirm').on('click.confirm', function(){
	// 	// e.preventDefault();
	// 	if(emailForm.valid()) {
	// 		popupEmailAddressContent.eq(0).addClass('hidden');
	// 		popupEmailAddressContent.eq(1).removeClass('hidden');

	// 		setTimeout(function(){
	// 			popupEmailAddress.Popup('reset');
	// 			popupEmailAddress.Popup('reposition');
	// 		}, 100);
	// 	}
	// });
	// popupEmailAddress.find('#email-address-submit-3').off('click.addEmail').on('click.addEmail', function(e){
	// 	e.preventDefault();
	// 	var newInput = $(config.template.addEmail.format(($(this).closest('.table-row').prev().children().length + 1), L10n.validation.email)).appendTo($(this).closest('.table-row').prev());
	// 	if(!window.Modernizr.input.placeholder){
	// 		newInput.find('input').placeholder();
	// 		newInput.find('input').addClear();
	// 	}
	// });

	// emailTrigger.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
	// 	e.preventDefault();
	// 	popupEmailAddress.Popup('show');
	// });

	// boarding-pass
	var boardingPassTrigger =  $('.boarding-pass');
	var popupBoarding = $('.popup--boarding-3');
	var popupBoardingContent = popupBoarding.find('.popup__content > div:first');
	var popupBoardingEmail = popupBoarding.find('.boarding-1');
	var popupBoardingSMS = popupBoarding.find('.boarding-2');
	var boardingForm = popupBoardingContent.find('form');

	var show = function(popup){
		popupBoarding.css({
			'overflow': 'hidden'
		});
		popup.css({
			'display':'block'
		});
		popup.css({
			'position': 'absolute',
			'top': 0,
			'left': 0,
			'z-index': 1,
			'opacity': 0
		}).animate({
			'opacity': 1
		},300);
		popupBoardingContent.css('visibility', 'hidden').animate({
			'opacity': 0
		},200, function(){
			// popupBoarding.css({
			// 	// 'height': height
			// });
		});
	};

	// set value for Boarding SMS
	var setValueForBoardingSMS = function(){
		var boardingSMS = popupBoarding.find('.tab-content.sms'),
				smsRow = boardingSMS.find('.table-row');
		var countryVal, areaVal, phoneVal;
		smsRow.each(function(idx, ele){
			countryVal= $(ele).find('[data-country]').val();
			areaVal= $(ele).find('[data-area]').val();
			phoneVal= $(ele).find('[data-phone]').val();

			popupBoardingSMS.find('.table-row').eq(idx).find('[data-country] .select__text').text(countryVal);
			popupBoardingSMS.find('.table-row').eq(idx).find('[data-area] input').val(areaVal);
			popupBoardingSMS.find('.table-row').eq(idx).find('[data-phone] input').val(phoneVal);
		});
	};

	// set value for Boarding Email
	var setValueForBoardingEmail = function(){
		var boardingEmail = popupBoarding.find('.tab-content.email'),
				emailRow = boardingEmail.find('.table-row');
		var emailVal;
		emailRow.each(function(idx, ele){
			emailVal= $(ele).find('[data-email]').val();
			popupBoardingEmail.find('.table-row').eq(idx).find('[data-email] input').val(emailVal);
		});
	};

	popupBoarding.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, .btn-back-booking',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		closeViaOverlay: false
	}).find('#boarding-10-submit').off('click.confirm').on('click.confirm', function(){
		// e.preventDefault();
		if(boardingForm.valid()) {
			var tab = $(this).closest('form').find('.tabs--1').find('.tab-item.active');
			if(tab.index()){
				setValueForBoardingSMS();
				show(popupBoardingSMS);
			}
			else{
				setValueForBoardingEmail();
				show(popupBoardingEmail);
			}
		}
	});

	popupBoarding.find('[data-tab]').off('afterChange.resizeModal').on('afterChange.resizeModal', function(){
		if(win.width() < config.mobile){
			popupBoarding.Popup('reset');
		}
	});

	boardingPassTrigger.each(function(){
		var self = $(this);
		self.off('click.showCancelPopup').on('click.showCancelPopup', function(e){
			e.preventDefault();
			if(!self.hasClass('disable')){
				popupBoardingContent.show();
				popupBoardingContent.css({
					'opacity': 1,
					'visibility': 'visible'
				});
				popupBoardingSMS.hide();
				popupBoardingEmail.hide();
				popupBoarding.Popup('show');
			}
			else{
				// popupCheckinErrorMessage.Popup('show');
			}
		});
	});

	// dynamic height
	/*var bkif = $('.booking-info');
	var timerSetHeight = null;
	if(bkif.length){
		var setHeightChecked = function(){
			if(global.vars.detectDevice.isMobile()){
				bkif.each(function(){
					var h = 0;
					var ch = $(this).children();
					ch.css('height', '');
					ch.each(function(){
						var self = $(this);
						if(h < self.height()){
							h = self.height();
						}
						// if(idx !== ch.length - 1){
						// 	if(self.outerHeight(true) > h){
						// 		h = self.outerHeight(true);
						// 	}
						// }
						// else if(idx === ch.length - 1){
						// 	$(this).height(h);
						// }
					});
					ch.height(h);
				});
			}
		};
		setHeightChecked();
		win.off('resize.setHeightChecked').on('resize.setHeightChecked', function(){
			clearTimeout(timerSetHeight);
			timerSetHeight = setTimeout(function(){
				setHeightChecked();
			}, 200);
		});
	}*/

	var tableDefault = $('.popup--checkin-cancel-all .table-default');
	tableDefault.each(function(){
		var table = $(this);
		$(this).find(':checkbox').each(function(colIndex){
			$(this).off('change.checkProcedure').on('change.checkProcedure',function(){
				var checkState = $(this).prop('checked');
				checkboxBackwardEffect(table,colIndex,checkState);
			});
		});
	});

	var checkboxBackwardEffect = function(currentArea,currentCol,currentState){
		currentArea.nextAll('div.table-default').each(function(){
			$(this).find(':checkbox').not('[disabled]').each(function(index){
				if(currentCol === index){
					if(currentState){
						$(this).prop('checked', true);
					}
					else{
						$(this).prop('checked', false);
					}
					$(this).trigger('change.checkAllList');
				}
			});
		});
	};
	// $('[data-printed-boarding-pass="true"]').find('a.cancel-flight').addClass('disabled');

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		var doValidate = function(els){
	// 			var pass = true;
	// 			els.each(function(){
	// 				if(!pass){
	// 					return;
	// 				}
	// 				pass = $(this).valid();
	// 				// fix for checkin- sms
	// 				if(els.closest('[data-validate-col]').length && pass){
	// 					els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
	// 				}
	// 			});
	// 		};
	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 			formGroup.each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		});

	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 			self.data('change', true);
	// 		});

	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
	// 			self.data('change', true);
	// 			$(this).valid();
	// 		}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
	// 			if($(this).val()){
	// 				self.data('change', true);
	// 			}
	// 			else{
	// 				// fix for checkin- sms
	// 				if($(this).closest('[data-validate-col]').length){
	// 					doValidate($(this).closest('[data-validate-row]').find('input'));
	// 				}
	// 			}
	// 		});

	// 	});
	// };

	// Validate
	var initValidateBoarding = function() {
		boardingForm.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function(){
				return false;
			}
		});
	};

	initValidateBoarding();

	// var initValidateSMS = function() {
	// 	formSMS.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess,
	// 		submitHandler: function(){
	// 			return false;
	// 		}
	// 	});
	// 	validateFormGroup(formSMS.find('.table-row'));
	// };

	// initValidateSMS();

	// var initValidateSendSMS = function() {
	// 	sendFormSMS.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess,
	// 	});
	// 	validateFormGroup(sendFormSMS.find('.table-row'));
	// };

	// initValidateSendSMS();

	// var initValidateEmail = function() {
	// 	emailForm.validate({
	// 		focusInvalid: true,
	// 		errorPlacement: global.vars.validateErrorPlacement,
	// 		success: global.vars.validateSuccess,
	// 		submitHandler: function() {
	// 			return false;
	// 		},
	// 		invalidHandler: function(form, validator) {
	// 			if(validator.numberOfInvalids()) {
	// 				var divScroll = win.width() > global.config.mobile ? emailForm.find('[data-scroll-content]') : emailForm.closest('.popup'),
	// 						scrollTo = 0;
	// 				divScroll.scrollTop(scrollTo);
	// 				scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top;
	// 				divScroll.scrollTop(scrollTo);
	// 			}
	// 		}
	// 	});
	// };

	// initValidateEmail();

	// Get data from JSON append to Flight info.
	var getFlightInfo = function() {
		var bookingInfoGroup = $('.booking-info-group');
		bookingInfoGroup.find('.flights--detail span').off('click.showInfo').on('click.showInfo', function() {
			var self = $(this);
			if(self.next('.details').is(':not(:visible)')) {
				self.children('em').addClass('hidden');
				self.children('.loading').removeClass('hidden');

				$.ajax({
					url: config.url.flightSearchFareFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						self.toggleClass('active');
						var html = '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						for(var ft in res.flyingTimes) {
							html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
						}
						self.next('.details').html(html).hide().removeClass('hidden').stop().slideToggle(400);
					},
					error: function(jqXHR, textStatus) {
						// console.log(jqXHR);
						if(textStatus === 'timeout') {
							window.alert(L10n.flightSelect.timeoutGettingData);
						}
						else {
							window.alert(L10n.flightSelect.errorGettingData);
						}
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				self.toggleClass('active');
				self.next('.details').stop().slideToggle(400);
			}
		});
	};

	getFlightInfo();

	var emailAndSMS = function(){
		var triggerEmailSMS = $('[data-trigger-email-sms]');
		var sendCheckinConfirmationPopup = $('.popup--send-checkin-confirmation');
		var sendCheckinForm = $('#send-checkin-confirmation');
		var popupEmailConfirm = sendCheckinConfirmationPopup.find('.popup--email-confirm');
		var PopupSuccessfullySent = sendCheckinConfirmationPopup.find('.popup--successfully-sent');
		var emailSMS = sendCheckinConfirmationPopup.find('.popup--email-sms');
		var tabEmailSMS = sendCheckinConfirmationPopup.find('.tabs--1');
		var addEmailAddress = sendCheckinConfirmationPopup.find('#email-address-submit-3');
		var formEmailConfirm = sendCheckinConfirmationPopup.find('.form--email-confirm');
		var formPhoneNumber = sendCheckinConfirmationPopup.find('.form--phone-number');

		var setValueEmailConfirmPopup = function(){
			var arr = [];
			var renderData = function(){
				sendCheckinForm.find('.form--email-address input').each(function() {
					var self = $(this);
					if (self.val()){
						arr.push(self.val());
					}
				});
			};

			renderData();

			formEmailConfirm.find('.table-default').empty();

			for(var i = 0; i< arr.length; i++){
				$(config.template.addEmailConfirm.format(i + 1, arr[i])).appendTo(formEmailConfirm.find('.table-default'));
			}

		};

		// set value SMS success sent Popup
		var setValueSMSSuccessfullySentPopup = function(){
			var arr = [];
			var countryVal, areaVal, phoneVal;
			var renderData = function(){
				formPhoneNumber.find('.table-row').each(function(idx, el){
					countryVal = $(el).find('[data-country]').val();
					areaVal = $(el).find('[data-area]').val();
					phoneVal = $(el).find('[data-phone]').val();

					if(countryVal){
						arr.push({
							country: countryVal,
							area: areaVal,
							phone: phoneVal
						});
					}
				});
			};

			renderData();

			PopupSuccessfullySent.find('.table-default').empty();

			for(var i = 0; i < arr.length; i++){
				$(config.template.AddSMSSuccessfullySent.format(i + 1, arr[i].country, arr[i].area, arr[i].phone)).appendTo(PopupSuccessfullySent.find('.table-default'));

			}
		};

		sendCheckinConfirmationPopup.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, .btn-back-booking',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			afterHide: function(){
				PopupSuccessfullySent.addClass('hidden');
				popupEmailConfirm.addClass('hidden');
				emailSMS.removeClass('hidden');
			},
			closeViaOverlay: false
		});

		// Validate form group
		var validateFormGroup = function(formGroup){
			formGroup.each(function(){
				var self = $(this);
				var doValidate = function(els){
					var pass = true;
					els.each(function(){
						if(!pass){
							return;
						}
						pass = $(this).valid();
						// fix for checkin- sms
						if(els.closest('[data-validate-col]').length && pass){
							els.closest('[data-validate-col]').removeClass('error').find('.text-error').remove();
						}
					});
				};
				self.off('click.triggerValidate').on('click.triggerValidate', function(){
					formGroup.each(function(){
						if($(this).data('change')){
							doValidate($(this).find('input'));
						}
					});
				});

				self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
					formGroup.not(self).each(function(){
						if($(this).data('change')){
							doValidate($(this).find('input'));
						}
					});
				}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
					self.data('change', true);
				});

				self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
					self.data('change', true);
					$(this).valid();
				}).off('blur.passengerDetail').on('blur.passengerDetail', function(){
					if($(this).val()){
						self.data('change', true);
					}
					else{
						// fix for checkin- sms
						if($(this).closest('[data-validate-col]').length){
							doValidate($(this).closest('[data-validate-row]').find('input'));
						}
					}
				});
			});
		};

		// Validate email and SMS
		var initValidateEmailAndSMS = function(){
			sendCheckinForm.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess,
				submitHandler: function() {
					var currentTab = sendCheckinForm.find('.tabs--1').find('.tab-item.active');

					SIA.preloader.show();

					emailSMS.addClass('hidden');

					if(currentTab.index()){
						setValueSMSSuccessfullySentPopup();
						PopupSuccessfullySent.removeClass('hidden');
					}
					else{
						setValueEmailConfirmPopup();
						popupEmailConfirm.removeClass('hidden');
					}

					setTimeout(function(){
						sendCheckinConfirmationPopup.Popup('reset');
						sendCheckinConfirmationPopup.Popup('reposition');
						SIA.preloader.hide();
					}, 100);

					return false;
				},
				invalidHandler: function(form, validator) {
					if(validator.numberOfInvalids()) {
						var divScroll = sendCheckinForm.find('[data-scroll-content]'),
								scrollTo = 0;
						divScroll.scrollTop(scrollTo);
						scrollTo = $(validator.errorList[0].element).closest('[data-validate-col]').offset().top - divScroll.offset().top;
						divScroll.scrollTop(scrollTo);
					}
				}
			});

			validateFormGroup(sendCheckinForm.find('[data-validate-row]'));
		};

		// start validate after blur for validateAtLeastOne
		// var setRuleForValidate = function(){
		// 	if(formPhoneNumber.find('[data-rule-validateatleastone]').length){
		// 		formPhoneNumber.find('[data-validate-row]').find('input').on('blur.blurValidate', function(){
		// 			var that = $(this);
		// 			var inputAtLeastOne = that.closest('[data-validate-row]').find('input');
		// 			inputAtLeastOne.not(that).valid();
		// 		});
		// 	}
		// };

		// setRuleForValidate();
		// end validate after blur for validateAtLeastOne

		initValidateEmailAndSMS();

		triggerEmailSMS.off('click.showEmailSMSPopup').on('click.showEmailSMSPopup', function(e){
			e.preventDefault();
			sendCheckinConfirmationPopup.Popup('show');
		});

		// tab afterChange event
		tabEmailSMS.off('afterChange.handleBtnES').on('afterChange.handleBtnES', function(){
			var self = $(this);
			var form = self.closest('form');
			var currentTab = form.find('.tabs--1').find('.tab-item.active');

			if(currentTab.index()){
				form.find('#email-address-submit-3').addClass('hidden');
			}else{
				form.find('#email-address-submit-3').removeClass('hidden');
			}
		});

		addEmailAddress.off('click.addEmailAddress').on('click.addEmailAddress', function(e){
			e.preventDefault();
			// var self = $(this);
			var newRow = $(config.template.addEmail.format(($(this).closest('form').find('.form--email-address .table-row').length + 1), L10n.validation.email)).appendTo($(this).closest('form').find('.form--email-address .table-row').parent());
			if(!window.Modernizr.input.placeholder){
				newRow.find('input').placeholder();
				newRow.find('input').addClear();
			}
		});
	};

	emailAndSMS();
};
