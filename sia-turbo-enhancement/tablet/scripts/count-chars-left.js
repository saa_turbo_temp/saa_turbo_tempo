/**
 * @name SIA
 * @description Define CountCharLeft functions
 * @version 1.0
 */
SIA.countCharsLeft = function() {
	var countCharsLeftEls = $('[data-count-chars-left]');

	var countCharsLeft = function(countEl, limitEl, preventSetText, e) {
		limitEl = limitEl || countEl.parent().siblings('.limit-character');
		var maxLength = parseInt(countEl.attr('maxlength'));

		var vlue = countEl.val();
		var breakLine = (vlue.match(/\n/g)||[]).length;
		var length = vlue.length;
		var charLeft = maxLength - (length + breakLine);

		var charText = '';
		charText = charLeft <= 0 ?
			'0 character left' : charLeft === 1 ?
			'1 character left' : window.accounting.formatNumber(charLeft, false) + ' characters left.';

		if (!preventSetText) {
			if(length + breakLine >= maxLength) {
				countEl.val(vlue.slice(0, maxLength - breakLine));
				limitEl.text('0 character left.');
			}
			else {
				limitEl.text(charText);
			}
		}
		else {
			if(e && length + breakLine > maxLength) {
				var key = e.which || e.keyCode;
				if (e.ctrlKey || key === 13 || key === 32 || (key >= 48 && key <= 90) ||
					(key >= 96 && key <= 111) || key >= 186) {

					countEl.val(vlue.slice(0, maxLength - breakLine));

					if(key === 13 && charLeft === -1) {
						limitEl.text('1 character left');
					}
					else {
						limitEl.text('0 character left');
					}
				}
			}
			else {
				limitEl.text(charText);
			}
		}
		return charText;
	};

	countCharsLeftEls
		.off('keyup.feedBack')
		.on('keyup.feedBack', function(e) {
			var countEl = $(this);
			var limitEl = countEl.parent().siblings('.limit-character');
			countCharsLeft(countEl, limitEl, true, e);
		})
		.off('keydown.feedBack')
		.on('keydown.feedBack', function(e) {
			var countEl = $(this);
			var key = e.keyCode || e.which;
			if (e.ctrlKey || key === 13 || key === 32 || (key >= 48 && key <= 90) ||
				(key >= 96 && key <= 111) || key >= 186) {
				var vlue = countEl.val();
				var breakLine = (vlue.match(/\n/g)||[]).length;
				var length = vlue.length;
				var maxLength = parseInt(countEl.attr('maxlength'));
				if(!e.ctrlKey && length + breakLine >= maxLength) {
					e.preventDefault();
				}
			}
		})
		.off('change.feedBack blur.feedBack focus.feedBack')
		.on('change.feedBack blur.feedBack focus.feedBack', function() {
			countCharsLeft($(this));
		})
		.off('paste.feedBack')
		.on('paste.feedBack', function() {
			setTimeout(function() {
				countCharsLeft($(this));
			}, 200);
		})
		.trigger('keyup.feedBack');
};
