/**
 * @name SIA
 * @description Define global flightStatus functions
 * @version 1.0
 */
SIA.multicity = function(){
	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;
	var radioTab = function(wp, r, t){
		wp.each(function(){
			var radios = wp.find(r);
			var tabs = wp.find(t);
			radios.each(function(i){
				var self = $(this);
				self.off('change.selectTabs').on('change.selectTabs', function(){
					tabs.removeClass('active').eq(i).addClass('active');
				});
			});
		});
	};
	var popupStopover = $('.popup--stopover');
	var flyingFocus = $('#flying-focus');
	var tbr = popupStopover.find('.table-row');

	if(!popupStopover.data('Popup')){
		popupStopover.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			}
		});
	}
	radioTab($('.main-inner'), '[data-search-flights]', '.form--toggle-search');

	var multiFlightEl = $('.multi-flight');
	var formSearch = $('.form--toggle-search');

	// var validateGreaterDate = function(form){
	// 	var validProcess = true;
	// 	var tmpDate = null;
	// 	form.find('[data-oneway]').each(function(){
	// 		var self = $(this);
	// 		var dataValidateCol = self.closest('[data-validate-col]').removeClass('error');
	// 		var getDate = self.datepicker( 'getDate' );
	// 		dataValidateCol.find('.text-error').remove();
	// 		if(!tmpDate){
	// 			tmpDate = getDate;
	// 		}
	// 		else if(tmpDate < getDate){
	// 			tmpDate = getDate;
	// 		}
	// 		else if(tmpDate > getDate){
	// 			dataValidateCol.addClass('error');
	// 			validProcess = false;
	// 			$(L10n.multiCity.errorTemplate).appendTo(dataValidateCol);
	// 			return;
	// 		}
	// 	});
	// 	return validProcess;
	// };

	// var validateFormGroup = function(formGroup){
	// 	formGroup.each(function(){
	// 		var self = $(this);
	// 		var doValidate = function(els){
	// 			var pass = true;
	// 			els.each(function(){
	// 				if(!pass){
	// 					return;
	// 				}
	// 				pass = $(this).valid();
	// 			});
	// 		};
	// 		self.off('click.triggerValidate').on('click.triggerValidate', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		});

	// 		self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
	// 			formGroup.not(self).each(function(){
	// 				if($(this).data('change')){
	// 					doValidate($(this).find('select, input'));
	// 				}
	// 			});
	// 		}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
	// 			self.data('change', true);
	// 		});

	// 		self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
	// 			self.data('change', true);
	// 		});
	// 	});
	// };

	var updateRuleValidateGreaterDate = function(form){
		var datesCollection = form.find('[data-oneway]');
		var updateContent = '';
		datesCollection.each(function(idx){
			if(!idx){
				$(this).attr('data-rule-validdateGreaterDate','[]');
				updateContent +='"#'+ $(this).attr('id') +'"';
			}
			else{
				updateContent +=',"#'+ $(this).attr('id') +'"';
				$(this).attr('data-rule-validdateGreaterDate','['+ updateContent+']');
			}
		});
		// datesCollection.not(':eq(0)').attr('data-rule-validdateGreaterDate', updateContent);
	};

	var removeErrorValidate = function(el) {
		el.find('.grid-col, :input').removeClass('error');
		el.find('.text-error').remove();
	};

	formSearch.each(function() {
		var that = $(this);
		that.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				if(that.find(multiFlightEl).children().length) {
					popupStopover.Popup('show');
				}
				return false;
			},
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					win.scrollTop($(validator.errorList[0].element).closest('.grid-col').offset().top - 40);
				}
			}
		});

		// validateFormGroup(that.find('.form-group'));
		that.off('click');
	});

	multiFlightEl.each(function(){
		var st = $(this);
		var pattern = st.children().eq(0);
		var clone = pattern.clone();
		var idFromText = 'clone-from-flight-';
		var idToText = 'clone-to-flight-';
		var idDateText = 'clone-date-flight-';
		var order = 0;
		var addFlight = st.next('.form-group').find('input');
		var tempEl = $();
		var checkMinFlight = function(flightForm) {
			if(flightForm.find('.multi-flight .form-wrapper').length > 1) {
				flightForm.find('.ico-close-rounded').show();
			} else {
				flightForm.find('.ico-close-rounded').hide();
			}
		};

		checkMinFlight(st.closest('form'));

		pattern.find('input:not(:eq(2))').off('touchstart.fixonIpad').on('touchstart.fixonIpad', function(e){
			e.stopPropagation();
		});

		// st.sortable({
		// 	stop: function( event, ui ){
		// 		$(ui.item[0]).find('[data-oneway]').val('');
		// 	}
		// });
		clone.find('.hasDatepicker').removeClass('hasDatepicker');
		clone.find('input').removeAttr('id');
		st.delegate('.ico-close-rounded', 'click.delete', function(e){
			e.preventDefault();
			var that = $(this),
					currentRow = that.closest('.form-wrapper'),
					rows = currentRow.siblings('.form-wrapper'),
					totalRows = rows.length,
					parentForm = currentRow.closest('form');
			currentRow.remove();
			// st.sortable('refresh');
			if(totalRows < 6) {
				addFlight.removeClass('disabled').prop('disabled', false);
			}
			checkMinFlight(parentForm);
		});
		if(addFlight.length){
			addFlight.off('click.addFlight').on('click.addFlight', function(e){
				e.preventDefault();
				var that = $(this),
						totalRows = st.children().length,
						flightTypeIndex = multiFlightEl.index(multiFlightEl.not(':hidden'));
				idFromText = idFromText + flightTypeIndex + '-';
				idToText = idToText + flightTypeIndex + '-';
				idDateText = idDateText + flightTypeIndex + '-';

				if(totalRows < 6){
					// var dataRequireFromGroup;
					order++;
					tempEl = clone.clone();
					// dataRequireFromGroup = '[3, "#' + idFromText + order + ',#' + idToText + order + ',#' + idDateText + order + '"]';
					tempEl.find('input:eq(0)').attr({
						'id' : idFromText + order,
						'name' : idFromText + order
					}).parent().siblings('label').attr('for', idFromText + order);
					tempEl.find('input:eq(1)').attr({
						'id' : idToText + order,
						'name' : idToText + order
					}).parent().siblings('label').attr('for', idToText + order);
					tempEl.find('input:eq(2)').attr({
						'id' : idDateText + order,
						'name' : idDateText + order
					}).parent().siblings('label').attr('for', idDateText + order);
					// validateFormGroup(tempEl);
					tempEl.appendTo(st);
					// validateFormGroup(st.find('.form-group'));
					SIA.initAutocompleteCity();
					SIA.selectReturnDate();
					removeErrorValidate(tempEl);

					if(totalRows === 5) {
						that.addClass('disabled').prop('disabled', true);
					}
				}
				checkMinFlight(that.closest('form'));
				updateRuleValidateGreaterDate(that.closest('form'));
			});
		}
	});

	// enable and diable select
	tbr.each(function(){
		var self = $(this);
		var rd = self.find(':checkbox');
		var sl = self.find('select');
		var addStopover = function(){
			if(rd.is(':checked')){
				sl.prop('disabled', false);
				sl.closest('[data-customselect]').removeClass('disabled');
			}
			else{
				sl.prop('disabled', true);
				sl.closest('[data-customselect]').addClass('disabled');
			}
		};
		addStopover();
		rd.off('change.addStopover').on('change.addStopover', addStopover);
	});
};
