SIA.baiduMap = function(bMapsLoaded) {
  var global = SIA.global,
      config = global.config,
      win = $(window),
      maps = $('.hotel-block .hotel-list');
      baidumapKey = config.map.baidumap.apiKey;
      baidumapZoom = config.map.baidumap.zoom;
      baidumapRadius = config.map.baidumap.radius;

  bMapsLoaded = bMapsLoaded !== undefined ? bMapsLoaded : false;
  window.BMapsCallback = function(){

      maps.each(function(i, e) {
        var mapEl = $(e),
          lat = mapEl.data('latitude'),
          long = mapEl.data('longtitude'),
          mapId = mapEl.data('map-id'),
          hotelImg = mapEl.data('hotel-mainimg'),
          hotelName = mapEl.data('hotel-name'),
          rate = mapEl.data('rate'),
          star;
          triggerMapEl = mapEl.find('[data-hotel-map-trigger]');

        if(rate === 0.5){
          star = "<em class='ico-3-hotel-star-half'></em>";
        } else if(rate === 1) {
          star = "<em class='ico-3-star-1'></em>";
        } else if(rate === 1.5) {
          star = "<em class='ico-3-star-15'></em>";
        } else if(rate === 2) {
          star = "<em class='ico-3-star-2'></em>";
        } else if(rate === 2.5) {
          star = "<em class='ico-3-star-25'></em>";
        } else if(rate === 3) {
          star = "<em class='ico-3-star-3'></em>";
        } else if(rate === 3.5) {
          star = "<em class='ico-3-star-35'></em>";
        } else if(rate === 4) {
          star = "<em class='ico-3-star-4'></em>";
        } else if(rate === 4.5) {
          star = "<em class='ico-3-star-45'></em>";
        } else if(rate === 5) {
          star = "<em class='ico-3-star-5'></em>";
        }

        triggerMapEl.off('click.loadHotelMap').on('click.loadHotelMap', function() {
          if($('#map_canvas_' + mapId).children().length <= 0) {
            initialize(lat, long, mapId, hotelImg, hotelName, star);
          }
        });

      });



  }

  function loadBaiduMaps() {
    if(bMapsLoaded) return window.BMapsCallback();
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src","http://api.map.baidu.com/api?v=2.0&ak="+ baidumapKey +"&callback=BMapsCallback");
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
  }

  function initialize(lat, long, mapId, hotelImg, hotelName, star) {
    var map, point, markerMain, icon, sContent, infoWindow, local;
    map = new BMap.Map("map_canvas_" + mapId);
    point = new BMap.Point(long,lat);
    map.centerAndZoom(point, baidumapZoom);

    local = new BMap.LocalSearch(map, {
      renderOptions: {map: map, selectFirstResult: false, autoViewport: true}
    });
    local.searchNearby(["bank","restaurant","hotel"], point, baidumapRadius);

    icon = new BMap.Icon('images/main-location.png', new BMap.Size(34, 44), {
        anchor: new BMap.Size(10, 30),
        infoWindowAnchor: new BMap.Size(10, 0)
    });
    markerMain = new BMap.Marker(point, {
      icon: icon
    });
    map.addOverlay(markerMain);


    sContent = '<div class="mainWindow"><div class="image"><img src="'+hotelImg+'" /></div><div class="desc"><h5>'+hotelName+'</h5>'+star+'</div></div>';

    infoWindow = new BMap.InfoWindow(sContent);
    markerMain.addEventListener("click", function(){
      map.openInfoWindow(infoWindow,point);
    });

    map.enableScrollWheelZoom(true);
  }

  if($('.hotel-block').data('baidu-map') !== undefined) {
    loadBaiduMaps();
  }

}
