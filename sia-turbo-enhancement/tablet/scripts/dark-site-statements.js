/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteStatements = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var detectDevice = global.vars.detectDevice;
	var showedSlides = detectDevice.isMobile() ? 1 : (detectDevice.isTablet() ? 5 : 4);
	var youtubeList = $('.watch-list-1');
	var orgTemplate = youtubeList.html();
	var players = [];
	var player;
	var timer = null;
	var currentUrl = '';
	var urlArr = [];
	var idxAfterResize = 0;

	win.off('resize.grayscaleStatements').on('resize.grayscaleStatements', function() {
		clearTimeout(timer);
		timer = setTimeout(function() {
			var lastShowedSlides = showedSlides;
			showedSlides = detectDevice.isMobile() ? 1 : (detectDevice.isTablet() ? 5 : 4);
			var tabletAndDesktop = Math.abs(showedSlides - lastShowedSlides) === 1;
			if (Math.abs(showedSlides - lastShowedSlides) === 0) {
				return;
			}
			if (tabletAndDesktop) {
				youtubeList.slick('unslick');
			} else {
				if (player) {
					player.stopVideo();
				}
				youtubeList.off().html(orgTemplate).removeClass('slick-initialized slick-slider');
			}
			initModule();
			players = [];
			if (!player) {
				window.onYouTubeIframeAPIReady();
			}
		}, 100);
	});

	var initYoutubePlayer = function() {
		var thumbnails = $('[data-statement-url]');
		var url = currentUrl ? currentUrl : thumbnails.eq(0).data('statement-url');
		thumbnails.each(function() {
			var thumbnail = $(this);
			thumbnail.off('click.playYoutube').on('click.playYoutube', function(e) {
				e.preventDefault();
				url = thumbnail.data('statement-url');
				if (!player) {
					window.onYouTubeIframeAPIReady();
				} else {
					player.loadVideoById(url);
					currentUrl = url;
				}
			});
		});
		window.onYouTubeIframeAPIReady = function() {
			var playerEl = $('#player');
			player = new window.YT.Player('player', {
				height: playerEl.height(),
				width: playerEl.width(),
				videoId: url
			});
		};
	};

	var initYoutubePlayerMobile = function(playerInfoList) {
		var createPlayer = function(playerInfo) {
			return new window.YT.Player(playerInfo.id, {
				height: playerInfo.h,
				width: playerInfo.w,
				videoId: playerInfo.url
			});
		};

		for (var i = 0; i < playerInfoList.length; i++) {
			var curplayer = createPlayer(playerInfoList[i]);
			players.push(curplayer);
		}
		window.players = players;
	};

	var initSlider = function() {
		var slideEl = $('.slick-slide');
		var activeEl = $('.slick-active');
		var listPlayers = [];

		var updateElement = function() {
			urlArr = [];
			slideEl = $('.slick-slide').find('.info-watch-1');
			activeEl = $('.slick-active').find('.info-watch-1');
			slideEl.each(function(idx) {
				var item = $(this);
				var playerOpt = {};
				item.attr('id', 'player-mb-' + idx);
				playerOpt.id = 'player-mb-' + idx;
				playerOpt.url = item.closest('a').data('statement-url');
				urlArr.push(playerOpt.url);
				playerOpt.w = item.width();
				playerOpt.h = item.height();
				listPlayers.push(playerOpt);
			});
		};

		var handleArrows = function() {
			var slides = $('.slick-slide');
			var len = slides.length;
			var prevBtn = $('.flexslider-prev');
			var nextBtn = $('.flexslider-next');
			nextBtn.css({
				'display': slides.eq(len - 1).hasClass('slick-active') ? 'none' : 'block'
			});
			prevBtn.css({
				'display': slides.eq(0).hasClass('slick-active') ? 'none' : 'block'
			});
		};

		idxAfterResize = (urlArr.indexOf(currentUrl) >= 0) ? urlArr.indexOf(currentUrl) : idxAfterResize;
		youtubeList.on('init', function() {
			handleArrows();
			if (!detectDevice.isMobile()) {
				return;
			}
			updateElement();
			initYoutubePlayerMobile(listPlayers);
		}).on('afterChange', function() {
			handleArrows();
			activeEl = $('.slick-active').find('.info-watch-1');
			currentUrl = activeEl.closest('a').data('statement-url');
		}).on('beforeChange', function() {
			if (!detectDevice.isMobile()) {
				return;
			}
			activeEl = $('.slick-active').find('.info-watch-1');
			var id = activeEl.attr('id');
			var currentPlayer = players.filter(function(a) {
				return $(a.f).attr('id') === id;
			});
			if (currentPlayer[0]) {
				currentPlayer[0].stopVideo();
			}
		}).slick({
			siaCustomisations: true,
			slidesToShow: showedSlides,
			slidesToScroll: 1,
			draggable: false,
			initialSlide: idxAfterResize,
			slide: 'div',
			infinite: false,
			useCSS: false,
			touchMove: detectDevice.isMobile() ? false : true,
			nextArrow: '<a href="#" class="slick-next flexslider-next">Next</a>',
			prevArrow: '<a href="#" class="slick-prev flexslider-prev">Prev</a>'
		});
	};

	var initModule = function() {
		if (!detectDevice.isMobile()) {
			initYoutubePlayer();
		}
		initSlider();
	};

	initModule();
	if (!detectDevice.isMobile()) {
		window.onYouTubeIframeAPIReady();
	}
};
