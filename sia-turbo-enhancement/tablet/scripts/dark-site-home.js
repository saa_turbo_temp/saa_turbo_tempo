/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.darkSiteHome = function(){
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var popupPromo = global.vars.popupPromo;
	var travelWidget = $('#travel-widget');
	var formManageBooking = travelWidget.find('#form-manage-booking');
	var formCheckIn = travelWidget.find('#form-check-in');
	var formPackage = travelWidget.find('#form-packages');
	var formFlightStatus = travelWidget.find('#form-flight-status');
	var formFlightStatus1 = travelWidget.find('#form-flight-status-1');
	var travelWidgetVisibleInput = 'input[type="text"]';
	var popupPromoMember = $('.popup--promo-code-kf-member');
	var popupPromoKF = $('[data-popup-promokf]');
	var loginBtn = $('[data-trigger-popup]');

	$.validator.addMethod('bookingEticket', function(value) {
		if(value.length === 6 || value.length === 13){
			if(value.length === 6){
				return /^[a-zA-Z0-9]+$/.test(value) && !/(0|1)/.test(value) && !/[-_\s]/g.test(value);
			}
			if(value.length === 13){
				return /[^-_\s]+$/.test(value) && /^\d+$/.test(value);
			}
		}
		else{
			return false;
		}
		return true;
	}, L10n.validator.bookingEticket);

	var _formPromotionValidation = function(){
		popupPromo.find('.form--promo').validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var validateFormGroup = function(formGroup){
		formGroup.each(function(){
			var self = $(this);
			self.off('click.triggerValidate').on('click.triggerValidate', function(){
				formGroup.not(self).each(function(){
					if($(this).data('change')){
						$(this).find('select, input').valid();
					}
				});
			});

			self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
				formGroup.not(self).each(function(){
					if($(this).data('change')){
						$(this).find('select, input').valid();
					}
				});
			}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
				self.data('change', true);
			});
			self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
				self.data('change', true);
			});
		});
	};


	var _manageBookingValidation = function(){
		var formGroup = formManageBooking.find('.form-group');
		validateFormGroup(formGroup);
		formManageBooking.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _checkInValidation = function(){
		var formGroup = formCheckIn.find('.form-group');
		validateFormGroup(formGroup);
		formCheckIn.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatusValidation = function() {
		var formGroup = formFlightStatus.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatus1Validation = function() {
		var formGroup = formFlightStatus1.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus1.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var formPromoKFValidation = function(){
		popupPromoKF.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _formPackageValidation = function() {
		var ppSearchLeaving = $('.popup--search-leaving');
		var btnContinue = ppSearchLeaving.find('[data-continue]');

		ppSearchLeaving.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			beforeShow: function(){
				if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()){
					ppSearchLeaving.data('parentContainerStyle', $('#container').attr('style'));
				}
			},
			beforeHide: function(){
				if(global.vars.detectDevice.isMobile()){
				}
			},
			afterHide: function(){
				if(global.vars.detectDevice.isMobile()){
					travelWidget.tabMenu('onResize');
				}
				if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()) {
					if(ppSearchLeaving.data('parentContainerStyle')) {
						ppSearchLeaving.removeData('parentContainerStyle');
					}
				}
			}
		});

		btnContinue.off('click.searchPackage').on('click.searchPackage', function() {
			ppSearchLeaving.Popup('hide');
			formPackage[0].submit();
		});

		var formGroup = formPackage.find('.form-group');
		validateFormGroup(formGroup);

		formPackage.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				ppSearchLeaving.Popup('show');
				return false;
			}
		});
	};

	var initBookingTab = function(){
		travelWidget.tabMenu({
			tab: 'ul.tab .tab-item',
			tabContent: 'div.tab-content',
			activeClass: 'active',
			templateOverlay: config.template.overlay,
			zIndex: config.zIndex.tabContentOverlay,
			isPopup: true
		});
	};
	formPromoKFValidation();
	initBookingTab();

	var initHighlightSlider = function() {
		var highlightSlider = $('#highlight-slider');
		var wrapperHLS = highlightSlider.parent();
		var imgHighlightLength = highlightSlider.find('img').length - 1;
		var isTabletMobile = global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile();

		var changeBgToTablet = function() {
			highlightSlider.find('img').each(function() {
				var self = $(this);
				var parentSelt = self.parent();
				parentSelt.css({
					'background-image': 'url(' + self.attr('data-img-src') + ')'
				});
				self.attr('src', config.imgSrc.transparent);
			});
		};

		var changeBgToDesktop = function() {
			highlightSlider.find('img').each(function() {
				var self = $(this);
				var parentSelt = self.parent();

				parentSelt.css('background-image', '');
				self.attr('src', self.attr('data-img-src'));
			});
		};

		var loadBackgroundHighlight = function(self, parentSelt, idx){
			if(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile()){
				parentSelt.css({
					'background-image': 'url(' + self.attr('data-img-src') + ')'
				});
				self.attr('src', config.imgSrc.transparent);
			}

			if(idx === imgHighlightLength){
				if(window.innerWidth > 480){
					highlightSlider.width(wrapperHLS.width() + 22);
				}
				else{
					highlightSlider.width(wrapperHLS.width());
				}

				highlightSlider.css('visibility', 'visible');
				highlightSlider.find('.slides')
					.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: 4,
						slidesToScroll: 4,
						accessibility: false,
						autoplay: false,
						pauseOnHover: false,
						responsive: [
							{
								breakpoint: 988,
								settings: {
									slidesToShow: 3,
									slidesToScroll: 3
								}
							},
							{
								breakpoint: 768,
								settings: {
									slidesToShow: 2,
									slidesToScroll: 2
								}
							},
							{
								breakpoint: 480,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1
								}
							}
						]
					});

				win.off('resize.highlightSlider').on('resize.highlightSlider',function() {
					if(window.innerWidth > 480){
						highlightSlider.width(wrapperHLS.width() + 22);
					}
					else{
						highlightSlider.width(wrapperHLS.width());
					}

					if ((global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && !isTabletMobile) {
						isTabletMobile = true;
						changeBgToTablet();
					}
					else if (!(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && isTabletMobile) {
						isTabletMobile = false;
						changeBgToDesktop();
					}
				}).trigger('resize.highlightSlider');
			}
		};

		highlightSlider.find('img').each(function(idx) {
			var self = $(this);
			var parentSelt = self.parent();
			var nI = new Image();

			self.attr('data-img-src', self.attr('src'));

			nI.onload = function(){
				loadBackgroundHighlight(self, parentSelt, idx);
			};
			nI.src = self.attr('src');
		});
	};

	initHighlightSlider();

	var triggerProCode = $('[data-promo-code-popup]');
	var flyingFocus = $('#flying-focus');

	if(globalJson.loggedUser){
		popupPromo = popupPromoMember;
		global.vars.popupPromo = popupPromoMember;
	}

	popupPromo.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		beforeShow: function(){
			if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()){
				popupPromo.data('parentContainerStyle', $('#container').attr('style'));
			}
		},
		beforeHide: function(){
			if(global.vars.detectDevice.isMobile()){
			}
		},
		afterHide: function(){
			if(global.vars.detectDevice.isMobile()){
				travelWidget.tabMenu('onResize');
			}
			if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()) {
				if(popupPromo.data('parentContainerStyle')) {
					popupPromo.removeData('parentContainerStyle');
				}
			}
		},
		triggerCloseModal: '.popup__close'
	});
	triggerProCode.off('click.showPromo').on('click.showPromo', function(e){
		e.preventDefault();
		popupPromo.Popup('show');
	});

	_formPromotionValidation();
	_manageBookingValidation();
	_checkInValidation();
	_flightStatusValidation();
	_flightStatus1Validation();
	_formPackageValidation();
	loginBtn.off('click.triggerLoginPopup').on('click.triggerLoginPopup', function(e){
		e.preventDefault();
		var loginPopupEl = $(loginBtn.data('popup'));
		if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()) {
			if(loginBtn.data('keepContainerStyle')) {
				loginPopupEl.data('Popup').options.beforeShow = function() {
					loginPopupEl.data('parentContainerStyle', true);
				};
				loginPopupEl.data('Popup').options.afterHide = function() {
					if(global.vars.detectDevice.isMobile()) {
						travelWidget.tabMenu('onResize');
					}
					if(loginPopupEl.data('parentContainerStyle')) {
						loginPopupEl.removeData('parentContainerStyle');
					}
				};
			}
		}
		loginPopupEl.Popup('show');
	});

	var checkEmptyInput = function(input){
		var isEmpty = false;
		input.each(function(){
			if(!$(this).val()){
				isEmpty = true;
			}
		});
		return isEmpty;
	};

	var changeText = function(form, input, btn){
		var inputs = form.find(input);
		var b = form.find(btn);
		inputs.each(function(){
			var self = $(this);
			self.off('change.checkEmptyInput').on('change.checkEmptyInput', function(){
				if(!checkEmptyInput(inputs)){
					b.val(L10n.home.proceed);
				}
				else{
					b.val(L10n.home.retrive);
				}
			});
		});
	};

	changeText(formManageBooking, travelWidgetVisibleInput, '#retrieve-1');
	changeText(formCheckIn, travelWidgetVisibleInput, '#retrieve-2');

	var bookingWidgetSwitch = function() {
		var manageBookingTabs = $('[data-manage-booking]');
		var manageBookingForms = $('[data-manage-booking-form]');

		var checkinTabs = $('[data-checkin]');
		var checkinForms = $('[data-checkin-form]');

		var flightStatusTabs = $('[data-flight-status]');
		var flightStatusForms = $('[data-flight-status-form]');

		var apply = function(tabs, form, dataTab, dataForm) {
			tabs
			.off('change.switch-tab')
			.on('change.switch-tab', function() {
				var data = $(this).data(dataTab);
				if(!dataForm) {
					dataForm = dataTab + '-form';
				}
				form.removeClass('active').filter('[data-' + dataForm + '="' + data + '"]').addClass('active');
			});
		};

		apply(manageBookingTabs, manageBookingForms, 'manage-booking');
		apply(checkinTabs, checkinForms, 'checkin');
		apply(flightStatusTabs, flightStatusForms, 'flight-status');

		var flightStatusFormSecond = flightStatusForms.filter('[data-flight-status-form="by-number"]');
		var optionDepartingArriving = flightStatusFormSecond.find('[data-option] input');
		var departingArriving = flightStatusFormSecond.find('[data-target]');

		optionDepartingArriving
		.off('change.changeDepartingArriving')
		.on('change.changeDepartingArriving', function() {
			var index = optionDepartingArriving.index($(this));
			departingArriving.removeClass('hidden').eq(index === 0 ? 1 : 0).addClass('hidden');
		});
	};

	var hideElement = function() {
		var bannerSlider = $('#banner-slider');
		bannerSlider.removeClass('flexslider');
		bannerSlider.find('img').remove();
		$('.wrapper.first').remove();
		$('.beta-footer').addClass('hidden');
		$('.footer-top').addClass('hidden');
		$('.footer .social').addClass('hidden');
	};

	var initPackagesSlider = function(){
		var packages = $('.packages');
		var packagesSlider = packages.find('[data-slideshow]');

		var startSlider = function(self, idx){
			var imgBannerLength = packagesSlider.find('.slide-item img').length - 1;
			var option = packagesSlider.data('option') ? $.parseJSON(packagesSlider.data('option').replace(/\'/gi, '"')) : {};
			option.pauseOnHover = false;
			option.siaCustomisations = true;

			if(idx === imgBannerLength){
				packagesSlider.css('visibility', 'visible');
				packagesSlider.find('.slides').slick(option);
			}
		};

		packagesSlider.find('.slide-item img').each(function(idx) {
			var self = $(this);
			var nI = new Image();
			nI.onload = function() {
				startSlider(self, idx);
			};

			nI.src = self.attr('src');
		});
	};

	hideElement();
	bookingWidgetSwitch();
	initPackagesSlider();
};
