/**
 * @name SIA
 * @description Define function to carousel slide
 * @version 1.0
 */
SIA.carouselSlide = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var isTabletMobile = global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile();
	var carouselSlide = $('[data-carousel-slide]');
	var paddingRight = 22;

	if(carouselSlide.length){
		carouselSlide.each(function(){
			var carousel = $(this);
			var numberSlide = carousel.data('carousel-slide');
			var wrapperHLS = carousel.parent();
			var imgHighlightLength = carousel.find('img').length - 1;

			var loadBackgroundHighlight = function(self, parentSelt, idx){
				if(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile()){
					parentSelt.css({
						'backgroundImage': 'url(' + self.attr('data-img-src') + ')'
					});
					self.attr('src', config.imgSrc.transparent);
				}

				if(idx === imgHighlightLength){
					if(window.innerWidth > 480){
						carousel.width(wrapperHLS.width() + paddingRight);
					}
					else{
						carousel.width(wrapperHLS.width());
					}

					carousel.css('visibility', 'visible');

					carousel.find('.slides')
						.slick({
							siaCustomisations: true,
							dots: true,
							speed: 300,
							useCSS: false,
							draggable: true,
							slidesToShow: numberSlide,
							slidesToScroll: numberSlide,
							accessibility: false,
							autoplay: false,
							pauseOnHover: false,
							responsive: [
								{
									breakpoint: 988,
									settings: {
										slidesToShow: 3,
										slidesToScroll: 3
									}
								},
								{
									breakpoint: 768,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 2
									}
								},
								{
									breakpoint: 480,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1
									}
								}
							]
						});
				}
			};

			carousel.find('img').each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				self.attr('data-img-src', self.attr('src'));

				nI.onload = function(){
					loadBackgroundHighlight(self, parentSelt, idx);
				};

				nI.src = self.attr('src');
			});
		});

		win.off('resize.carouselSlide').on('resize.carouselSlide',function() {
			carouselSlide.each(function(){
				var carousel = $(this);
				var wrapperHLS = carousel.parent();

				var changeBgToTablet = function() {
					carousel.find('img').each(function() {
						var self = $(this);
						var parentSelt = self.parent();

						parentSelt.css({
							'backgroundImage': 'url(' + self.attr('data-img-src') + ')'
						});

						self.attr('src', config.imgSrc.transparent);
					});
				};

				var changeBgToDesktop = function() {
					carousel.find('img').each(function() {
						var self = $(this);
						var parentSelt = self.parent();

						parentSelt.css('backgroundImage', '');
						self.attr('src', self.attr('data-img-src'));
					});
				};

				if(window.innerWidth > 480){
					carousel.width(wrapperHLS.width() + paddingRight);
				}
				else{
					carousel.width(wrapperHLS.width());
				}

				if ((global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && !isTabletMobile) {
					isTabletMobile = true;
					changeBgToTablet();
				}
				else if (!(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && isTabletMobile) {
					isTabletMobile = false;
					changeBgToDesktop();
				}

			});
		}).trigger('resize.carouselSlide');
	}
};
