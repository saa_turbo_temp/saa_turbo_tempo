SIA.sqcSavedTrips = function() {
	var config = SIA.global.config;
	var container = $('[data-fill-flights]');
	var upcomingInfoTemplate;
	var btnMore = $('[data-see-more]');
	var btnDelete = $('[data-message-delete]');
	var listTrips = $('.block--bookings-list');

	function getFlightInto(block) {
		block.find('.flights--detail > span')
		.off('click.getFlightInfo')
		.on('click.getFlightInfo', function() {
			var self = $(this);
			var details = self.siblings('.details');
			if(details.is('.hidden')) {
				$.ajax({
					url: config.url.orbFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						self.children('em').toggleClass('ico-point-d ico-point-u');
						details.toggleClass('hidden');
						var html = '';
						html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						for(var ft in res.flyingTimes) {
							html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
						}
						details.html(html);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						if(textStatus !== 'abort') {
							window.alert(errorThrown);
						}
					},
					beforeSend: function() {
						self.children('em').addClass('hidden');
						self.children('.loading').removeClass('hidden');
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				self.children('em').toggleClass('ico-point-d ico-point-u');
				details.toggleClass('hidden');
			}
		});
	}

	function showListPassengers(block) {
		block.find('[data-toggle-passengers-list]')
		.off('click.toggle-list-passengers')
		.on('click.toggle-list-passengers', function(e) {
			e.preventDefault();
			var listPassengers = $(this).siblings('[data-passengers]');
			$(this).toggleClass('active');
			var showLess = $(this).is('.active');
			var passengers = listPassengers.data('passengers').split(',');
			var lis = '';
			for(var i = 0; i < (showLess ? passengers.length : 3); i++) {
				lis += '<li>' + passengers[i];
				if(i < (showLess ? passengers.length : 3) - 1) {
					lis += ',';
				}
				lis += '</li>';
			}
			listPassengers.html(lis);
			listPassengers.children().last().addClass('last');
		});
	}

	function getUpcomingInfo(block) {
		var url = block.data('url');
		$.ajax({
			url: url,
			type: config.ajaxMethod,
			dataType: 'json',
			data: {},
			success: function(res) {
				var template = window._.template(upcomingInfoTemplate, {
					data: res
				});
				block.find('.loading').addClass('hidden');
				if(res.btnLabel) {
					block.find('.booking-form').removeClass('hidden').find('.btn-1').text(res.btnLabel);
				}
				block.find('.ico-point-d').removeClass('hidden');
				block.find('.booking-info-group').html(template);

				getFlightInto(block);
				showListPassengers(block);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if(jqXHR && textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			}
		});
	}

	function fillContent() {
		$.get(config.url.sqcUpcomingFlightDetailTemplate, function(html) {
			upcomingInfoTemplate = html;
		});
		$.get(config.url.sqcSavedTripsTemplate, function(html) {
			var template = window._.template(html, {
				data: globalJson.sqcSavedTrips
			});
			container.html(template);

			container.find('[data-saved-trip]').each(function() {
				getUpcomingInfo($(this));
			});

			SIA.accordion.initAccordion();
			btnDelete.attr('disabled', 'disabled');

			listTrips
			.checkboxAll()
			.off('change.checkbox')
			.on('change.checkbox', '[data-checkbox-master], [data-checkbox-item]', function() {
				var checkedBoxes = listTrips.find('[data-checkbox-item]:checked');
				if(checkedBoxes.length) {
					btnDelete.addClass('active').removeAttr('disabled');
				}
				else {
					btnDelete.removeClass('active').attr('disabled', 'disabled');
				}
			});
		});
		if(globalJson.sqcSavedTrips.length > 3) {
			btnMore.show();
		}
		else {
			btnMore.hide();
		}
		btnMore.off('click.show-more').on('click.show-more', function(e) {
			e.preventDefault();
			$('.hidden[data-saved-trip]').removeClass('hidden');
			listTrips.find('[data-checkbox-master]').prop('checked', false);
			$(this).hide(0);
		});
	}

	fillContent();

	function popupDeleteTrips() {
		var popup = $('.popup--saved-trips-delete').Popup({
			triggerCloseModal: '.popup__close, [data-close]',
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			container: '#container'
		});
		var renderDeletedTrip = function(){
			var lis = '';
			$('[data-fill-flights] [data-checkbox-item]:checked').not(':hidden').each(function() {
				lis += '<li>' + $(this).next('label').text() + '</li>';
			});
			return lis;
		};
		btnDelete.off('click.open-popup').on('click.open-popup', function(e) {
			e.preventDefault();
			popup.find('[data-trip-list]').html(renderDeletedTrip());
			popup.Popup('show');
		});
	}

	popupDeleteTrips();
};
