/**
 * @name SIA
 * @description Define global promotion functions
 * @version 1.0
 */
SIA.promotion = function(){
	var global = SIA.global,
			config = global.config,
			doc = global.vars.doc,
			win = global.vars.win,
			body = global.vars.body,
			htmlBody = $('html,body'),
			isPromotionEnhance = body.is('.promotion-enhancement'),
			enhanceData = [];
	//start promotions page
	var promotionPage = function(){
		if($('.fares-list-page').length){
			var wrapper = $('.promotion-result');
			var seeMoreBtn = $('.promotion-btn [data-see-more]');
			var updatebtn = $('#promotion-fare--detail-btn');
			// var btnClosePopup = $('.form-promo-filter-content .popup__close');
			// var formFareFilterCity = $('#form-fare-filter-city');

			if(isPromotionEnhance) {
				wrapper = $('.promotion-result--enhance').find('.promotion-list');
				enhanceData.push({
									city: 'All',
									lable: 'All',
									parent: 'All',
									value: 'All'
								});
				_.forEach(globalJson.listAllCountry, function(elem){
					enhanceData.push({
						city: elem.dataText,
						group: elem.dataText,
						lable: elem.dataText,
						value: elem.dataText
					});
					if(elem.children){
						var parent = elem;
						_.forEach(elem.children, function(elem){
							enhanceData.push({
								city: elem.key,
								lable: elem.key,
								parent: parent.dataText,
								value: elem.dataText
							})
						});
					}
				});
			}

			var _autoComplete = function (opt) {
				// create a variable for management
				var that = {};

				// declare booking widget defaults
				var defaults = {
					// containerAutocomplete : '',
					autocompleteFields : '',
					autoCompleteAppendTo: '',
					airportData : [],
					open: function(){},
					change: function(){},
					select: function(){},
					close: function(){},
					search: function(){},
					response: function(){},
					itemShow: 6,
					setWidth: 30
				};

				// extend passed options and defaults
				that.options = $.extend({}, defaults, opt);

				// that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
				that.autocompleteFields = that.options.autocompleteFields;
				that.airportData = that.options.airportData;
				that.timer = null;
				that.timerResize = null;
				that.winW = win.width();

				that.autocompleteFields.each(function (index, value) {
					var field = $(value);
					var wp = field.closest('[data-autocomplete]');
					var bookingAutoComplete = field.autocomplete({
							minLength : 0,
							open: that.options.open,
							change: that.options.change,
							select: that.options.select,
							close: that.options.close,
							search: that.options.search,
							response: that.options.response,
							source: that.airportData,
							appendTo : that.options.autoCompleteAppendTo
						}).data('ui-autocomplete');
					bookingAutoComplete._renderItem = function (ul, item) {
						if(isPromotionEnhance){
							if(item.group){
								that.group = item.group;
								return $('<li class="group-item autocomplete-item">')
								.attr('data-value', item.group)
								.append('<a class="autocomplete-link">'+ item.group +'</a>')
								.appendTo(ul);
							}else {
								// since all cities are under a country/group, if item.parent does not belong to that.options.group, you can just prepend to ul
								if(item.parent === that.group){
									return $('<li class="autocomplete-item">')
									.attr('data-value', item.city)
									.append('<a class="autocomplete-link">' + item.city +'</a>')
									.appendTo(ul);
								}else if(item.parent){
									return $('<li class="autocomplete-item redundancy">')
									.attr('data-value', item.city)
									.append('<a class="autocomplete-link">' + item.city +'</a>')
									.prependTo(ul);
								}
								return $('<li class="autocomplete-item">')
									.attr('data-value', item.city)
									.append('<a class="autocomplete-link">' + item.city +'</a>')
									.appendTo(ul);
							}
						}else{
							return $('<li class="autocomplete-item">')
							.attr('data-value', item.value)
							.append('<a class="autocomplete-link" href="javascript:void(0);">' + item.city +'</a>')
							.appendTo(ul);
						}
					};

					bookingAutoComplete._resizeMenu = function () {
						this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
					};

					field.autocomplete('widget').addClass('autocomplete-menu');

					field.off('blur.autocomplete');
					field.off('focus.highlight').on('focus.highlight', function () {
						var self = $(this);
						that.winW = win.width();
						self.closest('.custom-select').addClass('focus');
						win.off('resize.repositionAutocompleteCity').on('resize.repositionAutocompleteCity', function(){
							clearTimeout(that.timerResize);
							that.timerResize = setTimeout(function(){
								// if(field.autocomplete('widget').is(':visible')){
								// 	field.autocomplete('widget').css({
								// 		'left': field.closest('[data-autocomplete]').offset().left,
								// 		'top': field.closest('[data-autocomplete]').offset().top + field.closest('[data-autocomplete]').outerHeight(true)
								// 	});
								// }
								if(that.winW !== win.width()){
									field.trigger('blur.highlight');
									that.winW = win.width();
								}
							}, 100);
						});
					});
					field.off('blur.highlight').on('blur.highlight', function(){
						that.timer = setTimeout(function(){
							field.closest('.custom-select').removeClass('focus');
							field.autocomplete('close');
						}, 200);
						win.off('resize.repositionAutocompleteCity');
					});
					field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll').on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
						e.stopPropagation();
					});
					field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
						clearTimeout(that.timer);
					});
					field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
						if(e.which === 13){
							field.autocomplete('widget').find('li.active').trigger('click');
							e.preventDefault();
						}
					});
					wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
						e.preventDefault();
						clearTimeout(that.timer);
						if(field.closest('.custom-select').hasClass('focus')){
							field.trigger('blur.highlight');
						}
						else{
							field.trigger('focus.highlight');
						}
					});
				});
			};

			var transferData = function(json){
				var allData = json;
				var dataJSON = allData.promoVO[0];
				var index = 0;

				var dataJSONFilter = {
					list: []
				};
				var page = 1;
				var currentItem = 0;
				var limitItem = 12;
				var itemsLength;
				var price ={
					from: 0,
					to: 0
				};

				var preventClick = false;

				var dataAutocompleteFareDeals = [];
				var dataAutocompleteFareDealsTo = [];

				// create data for autocomplete
				var createData = function(){
					dataAutocompleteFareDeals = [];
					for(var i = 0; i < allData.promos.city.length; i++){
						dataAutocompleteFareDeals.push({
							city: allData.promos.city[i].description,
							value: allData.promos.city[i].description,
							codeValue: allData.promos.city[i].code + '-' +allData.promos.city[i].description
						});
					}
				};
				var createDataFareDealsTo = function(){
					dataAutocompleteFareDealsTo = [];
					dataAutocompleteFareDealsTo.push({
						city: L10n.promotionList.to.defaultOption,
						value: L10n.promotionList.to.defaultOption,
						codeValue: L10n.promotionList.to.defaultOption
					});
					for(var i = 0; i < allData.promos.destCountry.length; i++){
						dataAutocompleteFareDealsTo.push({
							city: allData.promos.destCountry[i].description,
							value: allData.promos.destCountry[i].description,
							codeValue: allData.promos.destCountry[i].code + '-' +allData.promos.destCountry[i].description
						});
					}
				};

				var timerAutocompleteOpen = null;
				var timerAutocompleteOpenTo = null;

				var getIndex = function(value){
					var cl = {
						idx: 0,
						con: false
					};
					for(var i = 0; i < allData.promoVO.length; i ++){
						if(allData.promoVO[i].city === value){
							cl = {
								idx: i,
								con: true
							};
						}
					}
					return cl;
				};
				// var timer;
				var txtFrom = $('#fare-filter-1');
				var txtTo = $('#fare-filter-2');
				var cabin = $('#fare-filter-3');
				var tripType = $('#trip-type');
				var selCity = $('[data-faredealcity] input');
				var selCityTo = $('[data-faredealcity-to] input');

				// $('.fares-list-page').find('[data-return-flight]').off('selectDate.datepicker').on('selectDate.datepicker', function(){
				// 	filter();
				// });

				var buildHtml = function(isReset, items){
					var limit = page * limitItem;
					itemsLength = items.list.length;
					var dataFilter = {
						list:[]
					};
					if(itemsLength < limit){
						limit = itemsLength;
					}
					if(itemsLength <= (page * limitItem)){
						seeMoreBtn.hide();
					}else{
						seeMoreBtn.show();
					}
					for(currentItem; currentItem < limit; currentItem++){
						dataFilter.list.push(items.list[currentItem]);
						if(currentItem === limit - 1){
							page++;
						}
					}
					if(isReset){
						wrapper.empty();
					}

					for (var k in dataFilter.list) {
						// dataFilter.list[k].price = accounting.formatMoney(dataFilter.list[k].price, '', 0, ',', '.');
						dataFilter.list[k].priceFormat = accounting.formatMoney(dataFilter.list[k].price, '', 0, ',', '.');
					}

					$.get(config.url.promotionPageTemplate, function (data) {
						var template = window._.template(data, {
							'data': dataFilter
						});
						if(dataFilter.list.length){
							wrapper.append(template);
						}
						else{
							if(!items.list.length){
								wrapper.html('<h2 class="empty-data">' + L10n.emptyData + '</h2>');
							}
						}
						preventClick = false;
						// wrapper.append(template);
					}, 'html');
				};

				var buildHtmlEnhance = function(isReset, items){
					var limit = page * limitItem;

					//group list
					var newItemList = _.groupBy(items.list, function(i){
						return i.destinationCityName;
					});

					//get the min price list
					var minPriceList = [];
					_.each(newItemList, function(value, key, obj){
						var l = _.min(obj[key], function(i){
							return i.price;
						});
						minPriceList.push(l);
					});
					// itemsLength = items.list.length;
					itemsLength = minPriceList.length;
					var dataFilter = {
						list: items.list,
						minPriceList: []
					};
					if(itemsLength < limit){
						limit = itemsLength;
					}
					if(itemsLength <= (page * limitItem)){
						seeMoreBtn.hide();
					}else{
						seeMoreBtn.show();
					}
					for(currentItem; currentItem < limit; currentItem++){
						//dataFilter.list.push(items.list[currentItem]);
						dataFilter.minPriceList.push(minPriceList[currentItem]);
						if(currentItem === limit - 1){
							page++;
						}
					}
					if(isReset){
						wrapper.empty();
					}
					for (var k in dataFilter.list) {
						dataFilter.list[k].priceFormat = accounting.formatMoney(dataFilter.list[k].price, '', 0, ',', '.');
					}
					for (var k in dataFilter.minPriceList) {
						dataFilter.minPriceList[k].priceFormat = accounting.formatMoney(dataFilter.minPriceList[k].price, '', 0, ',', '.');
					}

					$.get(config.url.promotionEnhancePageTemplate, function (data) {

						var template = window._.template(data, {
							'data': dataFilter
						});
						if(dataFilter.list.length){
							wrapper.append(template);
						}
						else{
							if(!items.list.length){
								wrapper.html('<h2 class="empty-data">' + L10n.emptyData + '</h2>');
							}
						}
						if(SIA.accordion.initAccordion) {
							SIA.accordion.initAccordion();
						}
						renderItemDetail();

					}, 'html');
				};

				var slider = $('#slider-range'),
						priceRangeCurrency = slider.find('.ui-slider_title'),
						labelFrom = slider.find('.ui-slider_from'),
						labelTo = slider.find('.ui-slider_to'),
						inputFrom = slider.find('[name="from-price"]'),
						inputTo = slider.find('[name="to-price"]');

				var initValue = [slider.data('current-min'), slider.data('current-max')];
				var holderValue = [];
				var triggerPopup = $('.promo-filter-results a');

				var fareCondition = $('.promotions-popup');
				var flyingFocus = $('#flying-focus');
				var enableSlider = true;
				var stepPercent = slider.data('step');

				createData();
				createDataFareDealsTo();

				// init triger condition
				$('.fares-list-page').delegate('[data-trigger-popup]','click.triggerConditionPopup', function(e){
					e.preventDefault();
					$($(this).data('popup')).Popup('show');
				});


				// init condition
				fareCondition.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					afterShow: function(){
						flyingFocus = $('#flying-focus');
						if(flyingFocus.length){
							flyingFocus.remove();
						}
					},
					triggerCloseModal: '.popup__close'
				});

				if(parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare) <= 0){
					initValue = [parseInt(dataJSON.minFare), parseInt(dataJSON.maxFare)];
					enableSlider = false;
				}
				else{
					initValue = [parseInt(dataJSON.minFare), parseInt(dataJSON.maxFare)];
					enableSlider = true;
				}

				priceRangeCurrency.text(L10n.promotion.priceRange.format($.trim(dataJSON.currency)));
				slider.slider({
					range: true,
					min: parseInt(dataJSON.minFare),
					max: (parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare) <= 0) ? parseInt(dataJSON.maxFare) + 1 : parseInt(dataJSON.maxFare),
					step: (parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare))*stepPercent/100,
					values: initValue,
					create: function() {
						labelFrom.text(accounting.formatMoney(initValue[0], '', 0, ',', '.'));
						labelTo.text(accounting.formatMoney(initValue[1], '', 0, ',', '.'));
						holderValue = [initValue[0], initValue[1]];
					},
					slide: function( event, ui ) {
						if(!enableSlider){
							return false;
						}
						// if((ui.values[0] + 500) >= ui.values[1]) {
						// 	return false;
						// }
						var from = ui.values[0],
								to = ui.values[1];
						labelFrom.text(accounting.formatMoney(from, '', 0, ',', '.'));
						labelTo.text(accounting.formatMoney(to, '', 0, ',', '.'));
						inputFrom.val(from);
						inputTo.val(to);
						holderValue = [from, to];
					},
					stop: function(event, ui) {
						if(isPromotionEnhance) {
							price.from = parseInt(holderValue[0]);
							price.to = parseInt(holderValue[1]);
							filter(dataJSON);
						}
					}
				});

				price.from = parseInt(slider.slider( 'values', 0 ));
				price.to = parseInt(slider.slider( 'values', 1 ));

				// if(!window.Modernizr.touch){
				// 	cabin.closest('[data-customSelect]').off('afterSelect.changeCabin').on('afterSelect.changeCabin', function(){
				// 		filter();
				// 	});
				// }else{
				// 	cabin.off('change.changeCabin').on('change.changeCabin', function(){
				// 		filter();
				// 	});
				// }

				/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
					selCity.closest('[data-customSelect]').off('afterSelect.changeCabin').on('afterSelect.changeCabin', function(){
						// formFareFilterCity.attr('action', 'http://google.com.vn');
						formFareFilterCity.submit();
						// reload();
					});
				}
				else{
					selCity.off('change.changeCabin').on('change.changeCabin', function(){
						formFareFilterCity.submit();
						// reload();
					});
				}*/
				// selCity.off('change.changeCabin').on('change.changeCabin', function(){
				// 	formFareFilterCity.submit();
				// 	// reload();
				// });

				seeMoreBtn.off('click.seemore').on('click.seemore', function(e){
					e.preventDefault();
					isPromotionEnhance === true ? buildHtmlEnhance(false, dataJSONFilter)
																			: buildHtml(false, dataJSONFilter);
				});

				updatebtn.off('click.update').on('click.update', function(e){
					e.preventDefault();
					if (!preventClick) {
						preventClick = true;
						price.from = parseInt(holderValue[0]);
						price.to = parseInt(holderValue[1]);
						filter(dataJSON);
					}
				});

				cabin
				.off('change.filterPromotion')
				.on('change.filterPromotion', function(e){
					e.preventDefault();
					price.from = parseInt(holderValue[0]);
					price.to = parseInt(holderValue[1]);
					if(isPromotionEnhance) {
						filter(dataJSON);
					}
				});

				tripType
				.off('change.filterPromotion')
				.on('change.filterPromotion', function(e){
					e.preventDefault();
					price.from = parseInt(holderValue[0]);
					price.to = parseInt(holderValue[1]);
					if(isPromotionEnhance) {
						filter(dataJSON);
					}
				});

				var testPrice = function(value){
					return price.from <= value && value <= price.to;
				};

				var testCabin = function(value){
					if(cabin.val() === ''){
						return true;
					}else{
						return value === cabin.val();
					}
				};

				var testTriptype = function(value){
					if(tripType.val() === ''){
						return true;
					}else{
						return value === tripType.val();
					}
				};

				// var convertToStandardDate = function(val){
				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				// 	return (months.indexOf(val.substr(0, 3)) + 1) + '/01/' + val.substr(3, val.length);
				// };

				var testDate = function(valueF, valueT){
					var from  = txtFrom.val();
					var to = txtTo.val();
					// var fromTime = from !== '' ? new Date(convertToStandardDate(from)).getTime() : '';
					// var toTime = to !== '' ?  new Date(convertToStandardDate(to)).getTime() : '';
					var fromTime = from !== '' ? new Date(from).getTime() : '';
					var toTime = to !== '' ?  new Date(to).getTime() : '';

					// var fromTime = from !== '' ? new Date(from.split('/').reverse().join('/')).getTime() : '';
					// var toTime = to !== '' ?  new Date(to.split('/').reverse().join('/')).getTime() : '';
					var valueFTime = new Date(valueF).getTime();
					var valueTTime = new Date(valueT).getTime();
					if(from === '' && to === ''){
						return true;
					}else if(from !== '' && to !== ''){
						return  valueFTime <= toTime && valueFTime >= fromTime && valueTTime >= fromTime && valueTTime <= toTime;
						// return  valueFTime <= toTime && valueTTime >= fromTime;
					}
					return true;
				};

				var testTo = function(value){
					return selCityTo.val() === value;
				};

				var testToEnhancePage = function(value){
					var selValue = selCityTo.val();
					return selValue === 'All' ? true : selValue.split('-')[1].trim() === value;
				}

				// window.alert(new Date(convertToStandardDate('Dec2014')));
				// convertToStandardDate('Dec2014');

				var renderMonTemplate = function(el, json, last){
					el.empty();
					var options = [];
					for(var i = 0; i < json.length; i++){
						var option,
								arrDate = json[i].split(' '),
								newFirstDate = arrDate[0] + ' 01 ' + arrDate[1],
								lastDate = new Date(arrDate[1], parseInt(months.indexOf(arrDate[0])) + 1, 0).getDate(),
								newLastDate = arrDate[0] + ' ' + lastDate + ' ' + arrDate[1];
						if(last){
							option = '<option '+ ((i === json.length - 1) ? 'selected' : '') +' value="' + newLastDate +'"' + '>' + json[i] + '</option>';
						}
						else{
							option = '<option '+ ((i === 0) ? 'selected' : '') +' value="' + newFirstDate +'"' + '>' + json[i] + '</option>';
						}
						options.push(option);
					}
					el.html(options.join(''));
					el.defaultSelect('refresh');
				};

				var renderCabinTemplate = function(json){
					cabin.empty();
					var options = [];
					for(var i = 0; i < json.availableClass.length; i++){
						var option;
						option = '<option value="' + json.availableClass[i] +'"' + '>' + json.availableClass[i] + '</option>';
						options.push(option);
					}
					cabin.html(options.join(''));
					cabin.defaultSelect('refresh');
					// if(cabin.closest('[data-customSelect]').data('customSelect')){
					// 	cabin.closest('[data-customSelect]').customSelect('_createTemplate');
					// }
				};

				var filter = function(json){
					var list = json.cityVO;
					// renderCabinTemplate(json);
					// renderMonTemplate(txtFrom, json.availableRange);
					// renderMonTemplate(txtTo, json.availableRange);
					isSeemore = false;
					page = 1;
					currentItem = 0;
					dataJSONFilter.list = [];
					for(var i = 0; i < list.length; i++){
						var item = list[i];
						var isValid = true;
						var start = item.durationStart;
						var end = item.durationEnd;
						if(start === 'Not available'){
							start = new Date();
						}
						if(end === 'Not available'){
							end = new Date(start.getFullYear() +1, start.getMonth(), start.getDate());
						}
						if(!testPrice(parseInt(item.price))){
							isValid = false;
						}
						if(!testCabin(item.cabin)){
							isValid = false;
						}

						if(isPromotionEnhance){

							if(selCityTo.data('isCountrySelected') === true) {
								if(!testTo(item.destCountryDescription) && selCityTo.val() !== L10n.promotionList.to.defaultOption){
									isValid = false;
								}
							} else {
								if(!testToEnhancePage(item.destCityCode) && selCityTo.val() !== L10n.promotionList.to.defaultOption){
									isValid = false;
								}
							}
						}else{
							if(!testTriptype(item.tripType)){
								isValid = false;
							}
							if(!testTo(item.destCountryDescription) && selCityTo.val() !== L10n.promotionList.to.defaultOption){
								isValid = false;
							}
						}
						// if(!testDate(item.durationstart, item.durationend)){

						if(!testDate(start, end) && !isPromotionEnhance){
							isValid = false;
						}
						if(isValid){
							dataJSONFilter.list.push(item);
						}
					}

					isPromotionEnhance === true ? buildHtmlEnhance(true, dataJSONFilter)
																			: buildHtml(true, dataJSONFilter);
				};

				var generateTemplateFromJSON = function(json, idx){
					selCity.val(allData.promos.city[idx].description);
					renderCabinTemplate(json);
					renderMonTemplate(txtFrom, json.availableFromRange);
					renderMonTemplate(txtTo, json.availableToRange, true);
					filter(json);
				};

				_autoComplete({
					autocompleteFields : selCity,
					autoCompleteAppendTo: body,
					airportData : dataAutocompleteFareDeals,
					open: function(){
						var self = $(this);
						self.autocomplete('widget').hide();
						clearTimeout(timerAutocompleteOpen);
						timerAutocompleteOpen = setTimeout(function(){
							self.autocomplete('widget').show();
						}, 100);
					},
					select: function(event, ui){
						var getInfo = getIndex(ui.item.codeValue);
						if(getInfo.con){
							dataJSON = allData.promoVO[getInfo.idx];
							index = getInfo.idx;
							if(parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare) <= 0){
								initValue = [parseInt(dataJSON.minFare), parseInt(dataJSON.maxFare) ];
								enableSlider = false;
							}
							else{
								initValue = [parseInt(dataJSON.minFare), parseInt(dataJSON.maxFare) ];
								enableSlider = true;
							}

							priceRangeCurrency.text(L10n.promotion.priceRange.format($.trim(dataJSON.currency)));
							slider.slider( 'option', {
								min: parseInt(dataJSON.minFare),
								max: (parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare) <= 0) ? parseInt(dataJSON.maxFare) + 1 : parseInt(dataJSON.maxFare),
								step: (parseInt(dataJSON.maxFare) - parseInt(dataJSON.minFare))*stepPercent/100,
								values: initValue
							});
							labelFrom.text(accounting.formatMoney(slider.slider( 'values', 0 ), '', 0, ',', '.'));
							labelTo.text(accounting.formatMoney(slider.slider( 'values', 1 ), '', 0, ',', '.'));
							price.from = parseInt(slider.slider( 'values', 0 ));
							price.to = parseInt(slider.slider( 'values', 1 ));
							holderValue = [price.from, price.to];
							generateTemplateFromJSON(dataJSON, index);
						}
						else{
							dataJSON = {
								'cityVO': []
							};
							filter(dataJSON);
						}
					},
					// response: function(event, ui){
					// 	if(ui.content.length ===1){
					// 		// $(this).val(ui.content[0].value);
					// 		// $(this).select();
					// 	}
					// },
					// search: function(){
					// 	var self = $(this);
					// 	self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
					// },
					close: function(){
						$(this).closest('[data-autocomplete]').removeClass('focus');
					},
					setWidth: 0
				});
				_autoComplete({
					autocompleteFields : selCityTo,
					autoCompleteAppendTo: body,
					airportData : isPromotionEnhance ? enhanceData : dataAutocompleteFareDealsTo,
					open: function(){
						var self = $(this);
						self.autocomplete('widget').hide();
						clearTimeout(timerAutocompleteOpenTo);
						timerAutocompleteOpenTo = setTimeout(function(){
							self.autocomplete('widget').show();
						}, 100);
					},
					select: function(event, ui){
						if(isPromotionEnhance) {
							var getInfo = getIndex(ui.item.value);
							selCityTo.val(ui.item.value);
							if(ui.item.parent) {
								selCityTo.data('isCountrySelected', false);
							} else {
								selCityTo.data('isCountrySelected', true);
							}
							price.from = parseInt(slider.slider( 'values', 0 ));
							price.to = parseInt(slider.slider( 'values', 1 ));
							holderValue = [price.from, price.to];

							generateTemplateFromJSON(dataJSON, getInfo.idx);
						}
					}
					// response: function(event, ui){
					// 	if(ui.content.length ===1){
					// 		// $(this).val(ui.content[0].value);
					// 		// $(this).select();
					// 	}
					// },
					// search: function(){
					// 	var self = $(this);
					// 	self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
					// },
					// close: function(){
					// 	$(this).closest('[data-autocomplete]').removeClass('focus');
					// },
					// setWidth: 0
				});

				index = getIndex(dataJSON.city).idx;
				selCityTo.val(L10n.promotionList.to.defaultOption);
				generateTemplateFromJSON(dataJSON, index);

				SIA.global.vars.popupGesture($('.form-promo-filter'), triggerPopup, '.popup__close,#promotion-fare--detail-btn', '.form-promo-filter-content');
			};

			transferData(globalJson.promotionFareDeals);

			// $.ajax({
			// 	url: 'ajax/Fare_Deal_India.json',
			// 	dataType: 'json',
			// 	type: global.config.ajaxMethod,
			// 	success: function(data) {
			// 		transferData(data);
			// 	},
			// 	error: function(xhr, status) {
			// 		if(status !== 'abort') {
			// 			window.alert(L10n.flightSelect.errorGettingData);
			// 		}
			// 	}
			// });
		}
	};

	promotionPage();
	// if($('.fares-list-page').length){
	// 	var slider = $('#slider-range'),
	// 		labelFrom = slider.find('.ui-slider_from'),
	// 		labelTo = slider.find('.ui-slider_to');
	// 	var triggerPopup = $('.promo-filter-results a');
	// 	var initValue = [ 75, 300 ];
	// 	slider.slider({
	// 		range: true,
	// 		min: 0,
	// 		max: 500,
	// 		values: initValue,
	// 		create: function() {
	// 			labelFrom.text('SGD ' + initValue[0]);
	// 			labelTo.text('SGD ' + initValue[1]);
	// 		},
	// 		slide: function( event, ui ) {
	// 			labelFrom.text('SGD ' + ui.values[0]);
	// 			labelTo.text('SGD ' + ui.values[1]);
	// 		}
	// 	});

	// 	// load JSON
	// 	var wrapper = $('.promotion-result');
	// 	var seeMoreBtn = $('.promotion-btn [data-see-more]');
	// 	var promotionBtnSearch = $('#promotion-fare--detail-btn');

	// 	var loadJSON = function(url, noempty){
	// 		$.ajax({
	// 			url: url,
	// 			dataType : 'json',
	//			type: global.config.ajaxMethod,
	// 			success: function(res){
	// 				if(!noempty){
	// 					wrapper.empty();
	// 				}
	// 				$.get(config.url.promotionPageTemplate, function (data) {
	// 					var template = window._.template(data, {
	// 						'data': res
	// 					});
	// 					wrapper.append(template);
	// 				}, 'html');
	// 			}
	// 		});
	// 	};

	// 	promotionBtnSearch.off('click.reloadJSON').on('click.reloadJSON',function(e){
	// 		e.preventDefault();
	// 		loadJSON(config.url.promotionPageJSONSearch);
	// 	});
	// 	loadJSON(config.url.promotionPageJSON);
	// 	seeMoreBtn.off('click.seemore').on('click.seemore', function(e){
	// 		e.preventDefault();
	// 		loadJSON(config.url.promotionPageJSONSeaMore, true);
	// 	});
	// 	window.popupGesture($('.form-promo-filter'), triggerPopup, '.popup__close', '.form-promo-filter-content');
	// }
	var formValidation = function() {
		var formPromotionBookFlights = $('[data-promotion-book-flight]');
		// var config = SIA.global.config;
		var global = SIA.global;
		formPromotionBookFlights.each(function() {
			var form = $(this);
			form.validate({
				focusInvalid: true,
				errorPlacement: global.vars.validateErrorPlacement,
				success: global.vars.validateSuccess
			});
		});
	};

	formValidation();

	var renderItemDetail = function() {
		var accordionWrapContent = $('[data-accordion-wrapper-content="1"]'),
				accordion = accordionWrapContent.find('[data-accordion="1"]');

		accordionWrapContent.each(function(i,v){

			accordion.each(function(index, value){
				var _self = $(this),
						parrentWrapper = _self.closest('[data-accordion-wrapper-content="1"]')
						trigger = _self.find('[data-accordion-trigger]'),
						closeBtn = _self.find('.close-btn'),
						imageItem = _self.find('.flight-item');

				imageItem.off('click.showItem').on('click.showItem', function(e){
					e.preventDefault();
					$(this).siblings('[data-accordion-trigger]').trigger('click.showItem');
				});

				trigger.off('click.showItem').on('click.showItem', function(e){
					var _this = $(this),
							padding = 65,
							wrapperW = parrentWrapper.width(),
							content = _this.parent().siblings('[data-accordion-content]'),
							acc = _this.closest('[data-accordion]');

					accordionWrapContent.find('[data-accordion="1"]').removeClass('active');
					_this.trigger('click.accordion');

					setTimeout(function(){
						if(_this.is('.active')){
							_this.closest('[data-accordion]').addClass('active');
						}
						htmlBody.animate({ scrollTop: acc.offset().top}, 'slow');
					}, 50);
				});

				closeBtn.off('click.closeAccordiion').on('click.closeAccordiion', function(e){
					var _this = $(this),
							acc = _this.closest('[data-accordion]'),
							triggerEl = acc.find('[data-accordion-trigger]');

					e.preventDefault();
					accordionWrapContent.find('[data-accordion="1"]').removeClass('active');
					triggerEl.trigger('click.accordion');
				});

			});
		});

		var listAddOns = $('.promotion-list');
		if(listAddOns.length){
			var totalImg = listAddOns.find('img');
			var countImage = 0;
			if(totalImg.length){
				totalImg.each(function(){
					var that = $(this);
					var img = new Image();
					img.onload = function(){
						countImage ++;
						if(countImage === totalImg.length){
							sortItem();
						}
					};
					img.onerror = function(){
						sortItem();
					};
					img.src = that.attr('src');
				});
			}
		}

	};


	// This function uses for Sort item
	var sortItem = function(){
		var addOnList = $('.promotion-list');
		var itemAddOn = addOnList.children('.promotion-item');
		var colCount = 3;
		var margin = 20;
		var realColWidth = 0;
		var windowWidth = 0;
		var blocks = [];
		Array.max = function(array) {
			return Math.max.apply(Math, array);
		};

		function setupBlocks() {
			if(window.innerWidth < global.config.tablet){
				colCount = 2
			}
			else{
				colCount = 3;
			}

			blocks = [];
			windowWidth = addOnList.outerWidth();
			realColWidth = Math.floor((windowWidth - margin*(colCount-1))/colCount);
			for(var i=0; i < colCount; i++) {
				blocks.push({
					top: 0,
					left: 0,
					item: $()
				});
			}
			positionBlocks();
		}

		var getIndex = function(){
			var min = 10000000;
			var index = 0;
			for(var i=0; i < blocks.length; i++) {
				if(min > blocks[i].top){
					min = blocks[i].top;
					index = i;
				}
			}
			return index;
		};

		function positionBlocks(){
			itemAddOn.each(function(idx){
				var self = $(this),
						innerH = self.find('.promotion-item__inner').outerHeight(true);
						innerW = self.find('.promotion-item__inner').innerWidth();
						content = self.find('[data-accordion-content]'),
						wrapW = addOnList.outerWidth(),
						idxBlk = getIndex(),
						padding = 20,
						margin = 20
						w = innerW*colCount + margin*(colCount - 1) - padding*2,
						l = 0,
						t = 0;

				if(self.is('.reposition')) {
					self.removeClass('reposition');
				}

				if(blocks[idxBlk].item.length){
					l = - blocks[idxBlk].item.position().left;
					t = blocks[idxBlk].item.position().top + innerH;
					// float left to right
					if(t + 200 > blocks[0].top && (idx === itemAddOn.length - 1)){
						l = - blocks[0].item.position().left;
						t = blocks[0].item.position().top + blocks[0].item.outerHeight(true);
						idxBlk = 0;
					}

					if(idxBlk === 0 && blocks[idxBlk].left === 0) {
						self.addClass('reposition');
					}
				}
				else{
					l = idxBlk === 0 ? idx : -(self.position().left);
					t = 0;
				}

				blocks[idxBlk].item = self;
				blocks[idxBlk].top = t + innerH;
				blocks[idxBlk].left = l;

				w = colCount === 3 ? w - 1 : w;
				content.css({
					'left': l,
					'width': w
				});
			});
		}

		win.off('resize.sortItemPromotion').on('resize.sortItemPromotion', function(){
			setupBlocks();
		});
		setupBlocks();
	};

	var initDate = function() {
		var startDate = $('[data-start-date]'),
			returnDate = $('[data-return-date]');
		if (!startDate.data('start-date')) {
			startDate.closest('.grid-col').hide();
		}
		if (!returnDate.data('return-date')) {
			returnDate.closest('.grid-col').hide();
		}

		renderItemDetail();
	};
	initDate();
};
