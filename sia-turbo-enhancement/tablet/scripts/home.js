/**
 * @name SIA
 * @description Define global home functions
 * @version 1.0
 */
SIA.home = function(){
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;
	var header = global.vars.header;
	// var container = global.vars.container;
	var popupPromo = global.vars.popupPromo;
	var travelWidget = $('#travel-widget');
	var formManageBooking = travelWidget.find('#form-manage-booking');
	var formCheckIn = travelWidget.find('#form-check-in');
	// var formBookTravel = travelWidget.find('#form-book-travel');
	var formPackage = travelWidget.find('#form-packages');
	var formFlightStatus = travelWidget.find('#form-flight-status');
	var formFlightStatus1 = travelWidget.find('#form-flight-status-1');
	var travelWidgetVisibleInput = 'input[type="text"]';
	var popupPromoMember = $('.popup--promo-code-kf-member');
	var popupPromoKF = $('[data-popup-promokf]');
	var loginBtn = $('[data-trigger-popup]');

	$.validator.addMethod('bookingEticket', function(value) {
		if(value.length === 6 || value.length === 13){
			if(value.length === 6){
				return /^[a-zA-Z0-9]+$/.test(value) && !/(0|1)/.test(value) && !/[-_\s]/g.test(value);
			}
			if(value.length === 13){
				return /[^-_\s]+$/.test(value) && /^\d+$/.test(value);
			}
		}
		else{
			return false;
		}
		return true;
	}, L10n.validator.bookingEticket);

	// $.validator.addMethod('eTicketNumber', function(value, el, param) {
	// 	var matcher = new RegExp((typeof param === 'string' || typeof param === 'number') ? '^(' + param + ')[0-9]+$' : '^[0-9]+$');
	// 	if(value.length === 13) {
	// 		return matcher.test(value);
	// 	}
	// 	return false;
	// }, L10n.validator.eTicketNumber);

	// Select depart or arrive
	var _fareDeals = function(){
		var fareDeals = $('.fare-deals .content');
		var fareDealsAutocomplete = $('.fare-deals #fare-deal-city');
		var customSelectFareDeal = fareDealsAutocomplete.closest('[data-customSelect]');

		var renderTemplate = function(json){
			var allData = json;
			// console.log(allData);
			var dataJSON = allData ? allData.promoVO[0] : null;
			var options = [];

			// create data for autocomplete
			var createSelect = function(){
				options = [];
				for(var i = 0; i < allData.promos.city.length; i++){
					var option = '<option data-code="'+ allData.promos.city[i].code +'-'+ allData.promos.city[i].description +'" value="' + allData.promos.city[i].code + '" >' + allData.promos.city[i].description + '</option>';
					options.push(option);
				}
				fareDealsAutocomplete.html(options.join(''));
				fareDealsAutocomplete.defaultSelect('refresh');
				fareDealsAutocomplete.off('change.faredeals').on('change.faredeals', function(){
					var index = getIndex(fareDealsAutocomplete.find(':selected').data('code'));
					if(index !== -1){
						dataJSON = allData.promoVO[index];
						generateTemplateFromJSON(dataJSON);
					}
					else {
						generateTemplateFromJSON(null);
					}
				});
			};


			// create date for faredeals
			var createDataFaredeals = function(json, numberOfItem, limit){
				var obj = {
					'listFares': [],
					'listMobiles': []
				};
				for(var i = 0; i < json.length; i++){
					if(i < limit){
						if(i%2 === 0){
							if(typeof obj.listFares[0] === 'undefined'){
								obj.listFares[0] = {
									'listDeals': []
								};
							}
							obj.listFares[0].listDeals.push(json[i]);
						}
						else if(i%2 === 1){
							if(typeof obj.listFares[1] === 'undefined'){
								obj.listFares[1] = {
									'listDeals': []
								};
							}
							obj.listFares[1].listDeals.push(json[i]);
						}
					}
					if(i < numberOfItem){
						obj.listMobiles.push(json[i]);
					}
				}
				return obj;
			};

			var getIndex = function(value){
				var idx = -1;
				for(var i = 0; i < allData.promoVO.length; i ++){
					if(allData.promoVO[i].city === value){
						idx = i;
					}
				}
				return idx;
			};

			var generateTemplateFromJSON = function(json){
				if(json){
					for(var k in json.cityVO) {
						json.cityVO[k].price = window.accounting.formatMoney(json.cityVO[k].price, ' ', 0, ',', '.');
					}
					$.get(config.url.templateFareDeal, function (data) {
						var template = window._.template(data, {
							data: createDataFaredeals(json.cityVO, 5, 10)
						});
						fareDeals.html(template);
					}, 'html');
				}
				else{
					fareDeals.html('<p class="fare-deals-note">'+ L10n.fareDeal.nodata +'</p>');
				}
			};
			if(fareDeals.length){
				if(dataJSON){
					createSelect();
					generateTemplateFromJSON(dataJSON);
				}
				else{
					var mainHeading = customSelectFareDeal.prev();
					var mainHeadingText = mainHeading.text().split(' ');
					customSelectFareDeal.hide();
					mainHeading.text(mainHeadingText.slice(0, mainHeadingText.length - 1).join(' '));
					fareDeals.html('<p class="fare-deals-note">'+ L10n.fareDeal.nofare +'</p>');
				}
			}
		};

		renderTemplate(globalJson.promotionFareDeals);

		// $.ajax({
		// 	url: 'ajax/Fare_Deal_India.json',
		// 	dataType: 'json',
		// 	type: global.config.ajaxMethod,
		// 	success: function(data) {
		// 		renderTemplate(data);
		// 	},
		// 	error: function(xhr, status) {
		// 		if(status !== 'abort') {
		// 			window.alert(L10n.flightSelect.errorGettingData);
		// 		}
		// 	}
		// });
	};

	/*var _bookingWidget = function(travelWidget){
		var formTravel = travelWidget.find('#form-book-travel');
		var radioFilter = formTravel.find('.form-group--tooltips input[type="radio"]');
		var radioTooltips = formTravel.find('.radio-tooltips');
		//var cityFrom = formTravel.find('#city-1');
		//var cityTo = formTravel.find('#city-2');

		radioFilter.each(function(index, el){
			$(el).off('change.showTooltip').on('change.showTooltip', function(){
				radioTooltips.removeClass('active');
				radioTooltips.eq(index).addClass('active');
			});
			// detect if IE and safari
			if(global.vars.isSafari){
				$(el).off('afterTicked.showTooltip').on('afterTicked.showTooltip', function(){
					$(el).trigger('change.showTooltip');
				});
			}
		});
	};*/

	var _formPromotionValidation = function(){
		popupPromo.find('.form--promo').validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var validateFormGroup = function(formGroup){
		formGroup.each(function(){
			var self = $(this);
			self.off('click.triggerValidate').on('click.triggerValidate', function(){
				formGroup.not(self).each(function(){
					if($(this).data('change')){
						$(this).find('select, input').valid();
					}
				});
			});

			self.find('select').closest('[data-customselect]').off('beforeSelect.passengerDetail').on('beforeSelect.passengerDetail', function(){
				formGroup.not(self).each(function(){
					if($(this).data('change')){
						$(this).find('select, input').valid();
					}
				});
			}).off('afterSelect.validateFormGroup').on('afterSelect.validateFormGroup', function(){
				self.data('change', true);
			});
			self.find('input, select').off('change.passengerDetail').on('change.passengerDetail', function(){
				self.data('change', true);
			});
		});
	};

	// var _bookingTravelValidation = function(){
	// 	var formGroup = formBookTravel.find('.form-group');
	// 	validateFormGroup(formGroup);
	// 	formBookTravel.each(function() {
	// 		$(this).validate({
	// 			focusInvalid: true,
	// 			errorPlacement: function(error, element) {
	// 				var containerForm = $(element).closest('.form-group');
	// 				containerForm.removeClass('success').addClass('error');
	// 				if(error.text().length) {
	// 					if(!containerForm.find('.text-error').length){
	// 						$(config.template.labelError).appendTo(containerForm).find('span').text(error.text());
	// 					}else{
	// 						containerForm.find('.text-error').find('span').text(error.text());
	// 					}
	// 				}
	// 			},
	// 			success: function(label, element) {
	// 				var containerForm = $(element).closest('.form-group');
	// 				if(containerForm.find('input.error').length > 0) {
	// 					return;
	// 				}
	// 				containerForm.removeClass('error').addClass('success');
	// 				containerForm.data('change', false);
	// 				containerForm.find('.text-error').find('span').text('');
	// 			}
	// 		});
	// 	});
	// };

	var _manageBookingValidation = function(){
		var formGroup = formManageBooking.find('.form-group');
		validateFormGroup(formGroup);
		formManageBooking.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _checkInValidation = function(){
		var formGroup = formCheckIn.find('.form-group');
		validateFormGroup(formGroup);
		formCheckIn.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatusValidation = function() {
		var formGroup = formFlightStatus.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _flightStatus1Validation = function() {
		var formGroup = formFlightStatus1.find('.form-group');
		validateFormGroup(formGroup);
		formFlightStatus1.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	var _formPackageValidation = function() {
		var ppSearchLeaving = $('.popup--search-leaving');
		var btnContinue = ppSearchLeaving.find('[data-continue]');

		ppSearchLeaving.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			beforeShow: function() {
				if (global.vars.detectDevice.isTablet()) {
					ppSearchLeaving.data('parentContainerStyle', $('#container').attr('style'));
				}
			},
			afterShow: function() {
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
			},
			afterHide: function() {
				win.trigger('resize.resetTabMenu');
				if (global.vars.detectDevice.isTablet()) {
					if (ppSearchLeaving.data('parentContainerStyle')) {
						ppSearchLeaving.removeData('parentContainerStyle');
					}
				}
			}
		});

		btnContinue.off('click.searchPackage').on('click.searchPackage', function() {
			ppSearchLeaving.Popup('hide');
			formPackage[0].submit();
		});

		var formGroup = formPackage.find('.form-group');
		validateFormGroup(formGroup);

		formPackage.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess,
			submitHandler: function() {
				ppSearchLeaving.Popup('show');
				return false;
			}
		});
	};

	var formPromoKFValidation = function(){
		popupPromoKF.validate({
			focusInvalid: true,
			errorPlacement: global.vars.validateErrorPlacement,
			success: global.vars.validateSuccess
		});
	};

	// init tab book widget

	var initBookingTab = function(){
		travelWidget.tabMenu({
			tab: 'ul.tab .tab-item',
			tabContent: 'div.tab-content',
			activeClass: 'active',
			templateOverlay: config.template.overlay,
			zIndex: config.zIndex.tabContentOverlay,
			isPopup: true
			// afterChange: function(tabs){
			// 	var tab = tabs.filter('.active');
			// 	var isFirstFocus = tab.data('focus');
			// 	if(isFirstFocus){
			// 		tab.find('form input[type=text]:first').focus();
			// 	}
			// }
		});
	};
	formPromoKFValidation();
	initBookingTab();

	// banner slider
	var bannerSlider = $('#banner-slider');
	var detectMaxHeight = function(){
		return win.width() > win.height() ? win.width() : win.height();
	};

	var setHeightSlider = function() {
		if(global.vars.detectDevice.isMobile()){
			bannerSlider.height(detectMaxHeight() - header.height() - $('#travel-widget').height() > 310 ? detectMaxHeight() - header.height() - $('#travel-widget').height() : 310);
		} else {
			bannerSlider.height('');
		}
	};

	setHeightSlider();

	var timerResizeSlider;
	win/*.off('resize')*/.on('resize', function() {
		clearTimeout(timerResizeSlider);
		timerResizeSlider = setTimeout(function() {
			setHeightSlider();
		}, 400);
	});

	var imgBannerLength = bannerSlider.find('img.img-main').length - 1;
	var loadBackgroundBanner = function(self, idx){
		// self.closest('.slide-item').css({
		// 	'background-image': 'url(' + self.attr('src') + ')'
		// });
		// if(global.vars.detectDevice.isTablet()){
		// 	self.closest('.slide-item').css({
		// 		'background-position': self.closest('.slide-item').data('tablet-bg')
		// 	});
		// }
		// if(global.vars.detectDevice.isMobile()){
		// 	self.closest('.slide-item').css({
		// 		'background-position': self.closest('.slide-item').data('mobile-bg')
		// 	});
		// }
		// self.attr('src', config.imgSrc.transparent);
		if(idx === imgBannerLength){
			bannerSlider.find('.loading').hide();
			bannerSlider.css('visibility', 'visible');
			bannerSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					draggable: true,
					infinite: true,
					arrows: false,
					speed: 500,
					fade: true,
					autoplay: false,
					pauseOnHover: false,
					accessibility: false,
					slide: 'div',
					cssEase: 'linear'
				});
		}
	};
	bannerSlider.find('img.img-main').each(function(idx) {
		var self = $(this);
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundBanner(self, idx);
		};
		nI.src = self.attr('src');
	});

	// Promotions and Packages sliders
	var promotionSlider = $('[data-slideshow]');
	var option = $.parseJSON(promotionSlider.data('option').replace(/\'/gi, '"'));
	option.autoplay = false;
	option.siaCustomisations = true;
	var imgPromotionLength = promotionSlider.find('img').length - 1;
	var loadBackgroundPromotion = function(self, parentSelt, idx){
		// if(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile()){
		// 	parentSelt.css({
		// 		'background-image': 'url(' + self.attr('src') + ')'
		// 	});
		// 	self.attr('src', config.imgSrc.transparent);
		// }
		// parentSelt.css({
		// 	'background-image': 'url(' + self.attr('src') + ')'
		// });
		// self.attr('src', config.imgSrc.transparent);
		if(idx === imgPromotionLength){
			promotionSlider.css('visibility', 'visible');
			promotionSlider.find('.slides')
				.slick(option);
		}
	};
	promotionSlider.find('img').each(function(idx) {
		var self = $(this);
		var parentSelt = self.parent();
		var nI = new Image();
		nI.onload = function(){
			loadBackgroundPromotion(self, parentSelt, idx);
		};
		nI.src = self.attr('src');
	});

	var initHighlightSlider = function() {
		//highlight slider
		var highlightSlider = $('#highlight-slider');
		var wrapperHLS = highlightSlider.parent();
		var imgHighlightLength = highlightSlider.find('img').length - 1;
		var isTabletMobile = global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile();

		var changeBgToTablet = function() {
			highlightSlider.find('img').each(function() {
				var self = $(this);
				var parentSelt = self.parent();
				parentSelt.css({
					'background-image': 'url(' + self.attr('data-img-src') + ')'
				});
				self.attr('src', config.imgSrc.transparent);
			});
		};

		var changeBgToDesktop = function() {
			highlightSlider.find('img').each(function() {
				var self = $(this);
				var parentSelt = self.parent();

				parentSelt.css('background-image', '');
				self.attr('src', self.attr('data-img-src'));
			});
		};

		var loadBackgroundHighlight = function(self, parentSelt, idx){
			if(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile()){
				parentSelt.css({
					'background-image': 'url(' + self.attr('data-img-src') + ')'
				});
				self.attr('src', config.imgSrc.transparent);
			}

			if(idx === imgHighlightLength){
				if(window.innerWidth > 480){
					highlightSlider.width(wrapperHLS.width() + 22);
				}
				else{
					highlightSlider.width(wrapperHLS.width());
				}

				highlightSlider.css('visibility', 'visible');
				highlightSlider.find('.slides')
					.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						useCSS: false,
						draggable: true,
						slidesToShow: 4,
						slidesToScroll: 4,
						accessibility: false,
						autoplay: false,
						pauseOnHover: false,
						responsive: [
							{
								breakpoint: 988,
								settings: {
									slidesToShow: 3,
									slidesToScroll: 3
								}
							},
							{
								breakpoint: 768,
								settings: {
									slidesToShow: 2,
									slidesToScroll: 2
								}
							},
							{
								breakpoint: 480,
								settings: {
									slidesToShow: 1,
									slidesToScroll: 1
								}
							}
						]
					});

				win.off('resize.highlightSlider').on('resize.highlightSlider',function() {
					if(window.innerWidth > 480){
						highlightSlider.width(wrapperHLS.width() + 22);
					}
					else{
						highlightSlider.width(wrapperHLS.width());
					}

					if ((global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && !isTabletMobile) {
						isTabletMobile = true;
						changeBgToTablet();
					}
					else if (!(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isTablet()) && isTabletMobile) {
						isTabletMobile = false;
						changeBgToDesktop();
					}
				}).trigger('resize.highlightSlider');
			}
		};

		highlightSlider.find('img').each(function(idx) {
			var self = $(this);
			var parentSelt = self.parent();
			var nI = new Image();

			self.attr('data-img-src', self.attr('src'));

			nI.onload = function(){
				loadBackgroundHighlight(self, parentSelt, idx);
			};
			nI.src = self.attr('src');
		});
	};

	initHighlightSlider();

	//init popup
	var triggerProCode = $('[data-promo-code-popup]');
	// var holderHeightContainer = 0;
	// var popupPromoTimer = null;
	var flyingFocus = $('#flying-focus');

	if(globalJson.loggedUser){
		popupPromo = popupPromoMember;
		global.vars.popupPromo = popupPromoMember;
	}

	popupPromo.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		afterShow: function(){
			flyingFocus = $('#flying-focus');
			if(flyingFocus.length){
				flyingFocus.remove();
			}
		},
		beforeShow: function(){
			if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()){
				// travelWidget.hide();
				popupPromo.data('parentContainerStyle', $('#container').attr('style'));
			}
		},
		beforeHide: function(){
			if(global.vars.detectDevice.isMobile()){
				// travelWidget.show();
			}
		},
		afterHide: function(){
			if(global.vars.detectDevice.isMobile()){
				travelWidget.tabMenu('onResize');
			}
			if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()) {
				if(popupPromo.data('parentContainerStyle')) {
					popupPromo.removeData('parentContainerStyle');
				}
			}
		},
		triggerCloseModal: '.popup__close'
	});
	triggerProCode.off('click.showPromo').on('click.showPromo', function(e){
		e.preventDefault();
		popupPromo.Popup('show');
	});

	// init tooltip via radio button
	/*if(travelWidget.length){
		_bookingWidget(travelWidget);
	}*/

	// validate form
	_formPromotionValidation();
	// _bookingTravelValidation();
	_manageBookingValidation();
	_checkInValidation();
	_flightStatusValidation();
	_flightStatus1Validation();
	_formPackageValidation();
	// fare-deals
	_fareDeals();

	// init triger login
	loginBtn.off('click.triggerLoginPopup').on('click.triggerLoginPopup', function(e){
		e.preventDefault();
		var loginPopupEl = $(loginBtn.data('popup'));
		if(global.vars.detectDevice.isMobile() || global.vars.detectDevice.isTablet()) {
			if(loginBtn.data('keepContainerStyle')) {
				loginPopupEl.data('Popup').options.beforeShow = function() {
					loginPopupEl.data('parentContainerStyle', true);
				};
				loginPopupEl.data('Popup').options.afterHide = function() {
					if(global.vars.detectDevice.isMobile()) {
						travelWidget.tabMenu('onResize');
					}
					if(loginPopupEl.data('parentContainerStyle')) {
						loginPopupEl.removeData('parentContainerStyle');
					}
				};
			}
		}
		loginPopupEl.Popup('show');
	});

	var checkEmptyInput = function(input){
		var isEmpty = false;
		input.each(function(){
			if(!$(this).val()){
				isEmpty = true;
			}
		});
		return isEmpty;
	};

	var changeText = function(form, input, btn){
		var inputs = form.find(input);
		var b = form.find(btn);
		inputs.each(function(){
			var self = $(this);
			self.off('change.checkEmptyInput').on('change.checkEmptyInput', function(){
				if(!checkEmptyInput(inputs)){
					b.val(L10n.home.proceed);
				}
				else{
					b.val(L10n.home.retrive);
				}
			});
		});
	};

	changeText(formManageBooking, travelWidgetVisibleInput, '#retrieve-1');
	changeText(formCheckIn, travelWidgetVisibleInput, '#retrieve-2');

	var bookingWidgetSwitch = function() {
		var manageBookingTabs = $('[data-manage-booking]');
		var manageBookingForms = $('[data-manage-booking-form]');

		var checkinTabs = $('[data-checkin]');
		var checkinForms = $('[data-checkin-form]');

		var flightStatusTabs = $('[data-flight-status]');
		var flightStatusForms = $('[data-flight-status-form]');

		var apply = function(tabs, form, dataTab, dataForm) {
			tabs
			.off('change.switch-tab')
			.on('change.switch-tab', function() {
				var data = $(this).data(dataTab);
				if(!dataForm) {
					dataForm = dataTab + '-form';
				}
				form.removeClass('active').filter('[data-' + dataForm + '="' + data + '"]').addClass('active');
			});
		};

		apply(manageBookingTabs, manageBookingForms, 'manage-booking');
		apply(checkinTabs, checkinForms, 'checkin');
		apply(flightStatusTabs, flightStatusForms, 'flight-status');

		var flightStatusFormSecond = flightStatusForms.filter('[data-flight-status-form="by-number"]');
		var optionDepartingArriving = flightStatusFormSecond.find('[data-option] input');
		var departingArriving = flightStatusFormSecond.find('[data-target]');

		optionDepartingArriving
		.off('change.changeDepartingArriving')
		.on('change.changeDepartingArriving', function() {
			var index = optionDepartingArriving.index($(this));
			departingArriving.removeClass('hidden').eq(index === 0 ? 1 : 0).addClass('hidden');
		});
	};

	bookingWidgetSwitch();

	var initFormStorage = function() {
		var classCustomEl = $('[data-class]'),
				optionClassEl,
				selectClassEl = $('[data-class] select'),
				adultCustomEl = $('[data-adult]'),
				selectAdultEl = $('[data-adult] select'),
				optionAdultEl,
				childCustomEl = $('[data-child]'),
				selectChildEl = $('[data-child] select'),
				optionChildEl,
				infantCustomEl = $('[data-infant]'),
				selectInfantEl = $('[data-infant] select'),
				optionInfantEl,
				infantDisable = $('#book-redem').find('[data-infant]');


				selectClassEl.each(function(){
					$(this).off('change.saveClassData').on('change.saveClassData',function(){

						var classData = $(this).val(),
								selfClassCustom = $(this).parent();


							classCustomEl.not(selfClassCustom).each(function(){
								var selectEl = $(this).find('select'),
									  optionClassEl = $(this).find('option');

								optionClassEl.removeAttr('selected');
								if(parseInt(optionClassEl.last().val()) < parseInt(classData)) {
									optionClassEl.first().attr('selected', 'selected');
								} else {
									optionClassEl.each(function(){
										var selfVal = $(this).attr('value'),
												self = $(this);
										if(selfVal === classData) {
											self.attr('selected', 'selected');
										};
									});
								};
								selectEl.trigger('change.changeCabin', true);
								selectEl['defaultSelect']('update');
							});
					});
				});

				selectAdultEl.each(function(index){
					$(this).off('change.saveAdultData').on('change.saveAdultData',function(event, flag){

						var adultData = $(this).val(),
								selfAdultCustom = $(this).parent();

							adultCustomEl.not(selfAdultCustom).each(function(){
								var selectEl = $(this).find('select'),
								 		optionAdultEl = $(this).find('option');

								optionAdultEl.removeAttr('selected');
								if(parseInt(optionAdultEl.last().val()) < parseInt(adultData)) {
									optionAdultEl.first().attr('selected', 'selected');
								} else {
									optionAdultEl.each(function(){
										var selfVal = $(this).attr('value'),
												self = $(this);
										if(selfVal === adultData) {
											self.attr('selected', 'selected');
										};
									});
								};

								if($(this).closest('form').data('form') !== 'hotel') {
									selectEl.trigger('change.changeCabin');
								}
								selectEl['defaultSelect']('update');
							});
					});
				});

				selectChildEl.each(function(index){
					$(this).off('change.saveChildData').on('change.saveChildData',function(event, flag){

						var childData = $(this).val(),
								selfChildCustom = $(this).parent();

							childCustomEl.not(selfChildCustom).each(function(){
								var selectEl = $(this).find('select'),
										optionChildEl = $(this).find('option');

								optionChildEl.removeAttr('selected');
								if(parseInt(optionChildEl.last().val()) < parseInt(childData)) {
									optionChildEl.first().attr('selected', 'selected');
								} else {
									optionChildEl.each(function(){
										var selfVal = $(this).attr('value'),
												self = $(this);
										if(selfVal === childData) {
											self.attr('selected', 'selected');
										};
									});
								};
								selectEl.trigger('change.changeCabin');
								selectEl['defaultSelect']('update');
							});
					});
				});

				selectInfantEl.each(function(index){
					$(this).off('change.saveInfantData').on('change.saveInfantData',function(event, flag){

						var infantData = $(this).val(),
								selfInfantCustom = $(this).parent();

							infantCustomEl.not(selfInfantCustom).not(infantDisable).each(function(){
								var selectEl = $(this).find('select'),
										optionInfantEl = $(this).find('option');
								optionInfantEl.removeAttr('selected');

								if(parseInt(infantCustomEl.last().val()) < parseInt(infantData)) {
									infantCustomEl.first().attr('selected', 'selected');
								} else {
									optionInfantEl.each(function(){
										var selfVal = $(this).attr('value'),
												self = $(this);
										if(selfVal === infantData) {
											self.attr('selected', 'selected');
										};
									});
								};
								selectEl.trigger('change.changeCabin');
								selectEl['defaultSelect']('update');
							});
					});
				});

			};
	if($('[data-widget-v1="true"]').length > 0 || $('[data-widget-v2="true"]').length > 0) {
		initFormStorage();
	}


		var animationExpandForm = function () {

			var collapseWidth = $('[data-widget-v2]').outerWidth()
			$('[data-widget-v2] .tab-wrapper').css({
				'min-width': collapseWidth + 'px'
			});

			$('[data-trigger-expand]').each(function(){

				$(this).off('click.expandFrom').on('click.expandFrom', function(event, flag){
					event.preventDefault();
					var parContent = $(this).closest('.tab-content'),
							childContent = parContent.find('.tab-nav-content.active'),
							childWrapper = parContent.find('.tab-nav-wrapper'),
							parWrapper = $(this).closest('.tab-wrapper'),
							expandContent = parContent.find('.col-expand--right'),
							tooltip = childWrapper.find('.form-group--tooltips');

					if(!childWrapper.is('.tab-form-expand')) {
						parWrapper.css({
							'min-width': collapseWidth * 2 + 'px'
						});
						if(win.width() >= 1024) {
							tooltip.addClass('expand-form');
						}
						childWrapper.addClass('tab-form-expand');
						expandContent.css('display','block');
						$(this).css('display', 'none');
						$(this).siblings('[data-trigger-collapsed]').css('display','block');
						if(flag !== true) {
							$('[data-trigger-collapsed]').focus();
						}
					}
				});
			});

			$('[data-trigger-collapsed]').each(function(){
				$(this).off('click.collapseFrom').on('click.collapseFrom', function(e){
					e.preventDefault();
					var parContent = $(this).closest('.tab-content'),
							childContent = parContent.find('.tab-nav-content.active'),
							parWrapper = $(this).closest('.tab-wrapper'),
							childWrapper = parContent.find('.tab-nav-wrapper'),
							expandContent = parContent.find('.col-expand--right'),
							tooltip = childWrapper.find('.form-group--tooltips');

					if(childWrapper.is('.tab-form-expand')) {
						parWrapper.css({
							'min-width': collapseWidth + 'px'
						});
						if(win.width() >= 1024) {
							tooltip.removeClass('expand-form');
						}
						childWrapper.removeClass('tab-form-expand');
						expandContent.css('display','none');
						$(this).css('display', 'none');
						$(this).siblings('[data-trigger-expand]').css('display','block');
						$('[data-trigger-expand]').focus();
					}
				});
			});

			$('[data-widget-v2="true"] input').each(function(){
				$(this).off('focus.triggerExpandForm, click.triggerExpandForm').on('focus.triggerExpandForm, click.triggerExpandForm', function(){
					if(win.width() >= 1024) {
						var parTab = $(this).closest('.tab-content'),
								navWrapper = parTab.find('.tab-nav-wrapper'),
								btnExpand = parTab.find('[data-trigger-expand]');
						if(parTab.is('[data-bookatrip]') === true && !navWrapper.is('.tab-form-expand')) {
							setTimeout(function(){
								btnExpand.trigger('click.expandFrom', true);
							}, 500);
						};
				  }
				});
			});

		}

	if($('[data-widget-v2="true"]').length > 0 && !SIA.global.vars.detectDevice.isTablet()) {
			animationExpandForm();
	}


	win.off('orientationchange.animationForm').on('orientationchange.animationForm', function(){
		setTimeout(function(){
			var widget = $('[data-widget-v2]'),
			    collapseWidth = $('[data-widget-v2]').outerWidth(),
					tabWrapper = widget.find('.tab-wrapper'),
					tabNavWrapper = tabWrapper.find('.tab-nav-wrapper');

			tabWrapper.css('min-width', collapseWidth + 'px');
			tabNavWrapper.removeClass('tab-form-expand');

			if(win.width() >= 1024) {
				$('.col-expand--right').css('display', 'none');
				$('[data-trigger-expand]').css('display', 'block');
				animationExpandForm();
			} else {
				$('[data-trigger-expand], [data-trigger-collapsed]').css('display', 'none');
				$('.col-expand--right').css('display', 'block');
			}
		}, 500);

	});

	var initUncheckedToShow = function() {
		$('[data-checked-show]').on('click.toggle', function() {
			var target = $(this).data('checked-show');
			if(!$(this).is(':checked')) {
				$(target).removeClass('hidden');
			} else {
				$(target).addClass('hidden');
			}
		});
	}
	initUncheckedToShow();
};
