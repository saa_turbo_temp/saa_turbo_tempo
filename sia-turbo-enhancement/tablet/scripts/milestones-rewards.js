/**
 * @name SIA
 * @description Define global donatemiles functions
 * @version 1.0
 */
SIA.milestonesRewards = function() {
  var global = SIA.global,
      config = global.config;

  var progressBar = $('[data-progress-animation]'),
      currentPPS = progressBar.data('kf-points'),
      leftPPS = progressBar.data('silver'),
      milestones = progressBar.find('.milestones'),
      milestonesLeft = milestones.find('.milestones-bar--silver'),
      milestonesRight = milestones.find('.milestones-bar--gold'),
      milestonesAnimate = progressBar.find('.milestones-animate'),
      itemMilestones = milestones.find('.milestones-item'),
      tooltipEl = progressBar.find('.tooltip-progress'),
      totalPPS = progressBar.data('total-points'),
      currentPer = currentPPS*100/totalPPS,
      count = currentPer / 20,
      leftPer = leftPPS*100/totalPPS,
      remainPer = 100 - leftPer,
      pixelProgressBar,
      milestonesLeftLength,
      item,
      tabItem = $('.dials-progress-tab').find('.tab-item'),
      tabContent = $('.dials-progress-tab').find('.tab-content'),
      heightBar = milestones.outerHeight(),
      widthBar = milestones.outerWidth(),
      isMobile;

  isMobile = ($(window).width() < 768) ? true : false;

  pixelProgressBar = isMobile ? progressBar.outerHeight() : progressBar.outerWidth();

  milestonesLeftLength = pixelProgressBar*(leftPPS/totalPPS);

  var resetAnimation = function() {
    // setTimeout(function(){
      itemMilestones.removeClass('active');
      tooltipEl.css('display', 'none');
      if(isMobile) {
        milestonesAnimate.css('height', 0);
      } else {
        milestonesAnimate.css('width', 0);
      }
  }


  var animationProgressBar = function(){

    var startWidth, startHeight;

    tooltipEl.find('.current-number').text(currentPPS);

    var initProgressBar = function() {

      if(isMobile) {
        milestonesLeft.css({
          'height': leftPer + '%'
        });

        milestonesRight.css({
          'top': leftPer + '%',
          'height': remainPer + '%'
        });
      } else {
        milestonesLeft.css({
          'width': leftPer + '%'
        });

        milestonesRight.css({
          'left': leftPer + '%',
          'width': remainPer + '%'
        });
      }

      for(var i = 0; i < itemMilestones.length; i++) {
        var position,
            positionValue,
            selfIcon = $(itemMilestones[i]).find('.icon-milestones');

        if(isMobile) {
          position = $(itemMilestones[i]).css('top').split('%')[0];
          positionValue = position.split('px')[0];
          if(i === 0) {
            startHeight = $(itemMilestones[i]).css('top').split('%')[0]*100/heightBar;
          }
        } else {
          position = $(itemMilestones[i]).css('left').split('%')[0];
          positionValue = position.split('px')[0];
          if(i === 0) {
            startWidth = positionValue*100/widthBar;
          }
        }


        if(positionValue > milestonesLeftLength) {
          selfIcon.css('border-color', '#b69b5a');
        }

      }
    };

    var startAnimation = function() {
      var arr = [],
          countAnimateEl,
          lastIdx = itemMilestones.length - 1;

      milestonesAnimate.each(function(){
        arr.push(parseInt($(this).data('point')));
      });

      for(var i = 0; i < arr.length; i++) {
        if(currentPPS <= arr[i]) {
          countAnimateEl = i + 1;
          break;
        }
      }
      for(var i = 0; i < countAnimateEl; i++) {
        var self = $(milestonesAnimate[i]),
            selfPoint = parseInt(self.data('point')),
            prePoint = 0,
            preEl,
            point,
            currentPoint = currentPPS,
            startWidth = 0;

        if(i >= 1) {
          preEl = $(milestonesAnimate[i - 1]);
          prePoint = parseInt(preEl.data('point'));
        }
        point = selfPoint - prePoint;
        if(isMobile) {
           if(currentPPS > selfPoint) {
             self.delay(i*1000).animate({
             height: startWidth + '%'
           }).animate({
             height: 100 + '%'
           }, {
             duration: 300,
             easing: "linear",
             complete: function(){
               var index = parseInt($(this).data('index'));
               $(itemMilestones[index]).addClass('active');
             }
           });
          } else {
           self.delay(i*1000).animate({
             height: startWidth + '%'
           }).animate({
             height: ((point - (selfPoint - currentPPS))*100/point) + '%'
           }, {
             duration: 300,
             easing: "linear",
             step: function(now, fx) {
              if(now === 100) {
               $(itemMilestones[countAnimateEl - 1]).addClass('active');
              }
              if(now === fx.end) {
               $(tooltipEl[countAnimateEl - 1]).css('top', now + '%');
              }
             },
             complete: function(){
               setTimeout(function(){
                 $(tooltipEl[countAnimateEl - 1]).fadeIn('fast').css('display','table');
               },500);
             }
           });
          }
        } else {
           if(currentPPS > selfPoint) {
             self.delay(i*1000).animate({
             width: startWidth + '%'
           }).animate({
             width: 100 + '%'
           }, {
             duration: 300,
             easing: "linear",
             complete: function(){
               var index = parseInt($(this).data('index'));
               $(itemMilestones[index]).addClass('active');
             }
           });
          } else {
           self.delay(i*1000).animate({
             width: startWidth + '%'
           }).animate({
             width: ((point - (selfPoint - currentPPS))*100/point) + '%'
           }, {
             duration: 300,
             easing: "linear",
             step: function(now, fx) {
              if(now === 100) {
               $(itemMilestones[countAnimateEl - 1]).addClass('active');
              }
              if(now === fx.end) {
               $(tooltipEl[countAnimateEl - 1]).css('left', now + '%');
              }
             },
             complete: function(){
               setTimeout(function(){
                 $(tooltipEl[countAnimateEl - 1]).fadeIn('fast').css('display','table');
               },500);
             }
           });
          }
        }

       }

    }

    startAnimation();

  };

  tabItem.each(function(idx){
    $(this).off('click.initAnimation').on('click.initAnimation', function(e){
      if($(this).is('[data-tabprogressbar]') && !$(tabContent[idx]).is('.active')) {
        setTimeout(function(){
          animationProgressBar();
        },200);
      } else {
        milestonesAnimate.stop(true,true);
        resetAnimation();
      }
    });
  });

  $(window).off('orientationchange.resetAnimation').on('orientationchange.resetAnimation', function(){
    milestonesAnimate.stop(true,true);
    resetAnimation();
    setTimeout(function(){
      animationProgressBar();
    },200);
  });

  animationProgressBar();
  var yourVoucher = function(){
    var yourVoucherAccordion = $('.container-accordion'),
     redemptionVoucher = yourVoucherAccordion.find('[data-50000miles-redemption]'),
      doubleMilesAccrual = yourVoucherAccordion.find('[data-double-miles-accrual]'),
      exclusiveUpgrade = yourVoucherAccordion.find('[data-exclusive-upgrade]'),
      airportUpgrade = yourVoucherAccordion.find('[data-airport-upgrade]');

    var initVoucher = function(voucherEl, data1, progressVoucherCountSelect) {
      var templateVoucher;
      $.get(global.config.url.yourVoucherTpl, function (data) {
        var template = window._.template(data, {
          data: data1
        });
        templateVoucher = $(template);
        var length = 0,
            totalVoucher = voucherEl.find('.accordion-link').find('.number').removeClass('hidden'),
            appendAfterDiv = voucherEl.find('.earn-mile');

        for(var i = 0; i < data1.length; i++) {
          if(data1[i].rewardStatus === 'Available') {
            length++;
          }
        };

        totalVoucher.text(length);
        voucherEl.find('.voucher-list').remove();
        appendAfterDiv.after(templateVoucher);
        // UPdate number of voucher
        if(length>1 && progressVoucherCountSelect){
          progressVoucherCountSelect.removeClass('hidden').text(length + ' x ' );
        }
        $('.item-row [data-tooltip]').kTooltip();
      });
    };

    $.ajax({
      url: global.config.url.yourVoucher,
      type: SIA.global.config.ajaxMethod,
      dataType: 'json',
      success: function(reponse) {
        var data0 = reponse.SAARewardInfo[0].MilesRedemptionVouchers,
            data2 = reponse.SAARewardInfo[1].DoubleMilesAccrualVoucher,
            data3 = reponse.SAARewardInfo[2].ExclusiveUpgradeVoucher,
            data4 = reponse.SAARewardInfo[3].AirportUpgradeVoucher,
            total50000MilesRedemption = $('.milestones-item__info').find('[50000miles-redemption]').find('span'),
            totalDoubleMiles = $('.milestones-item__info').find('[data-double-miles]').find('span'),
            totalAirportUpgrade = $('.milestones-item__info').find('[data-airport-upgrade]').find('span'),
            totalExclusiveUpgrade = $('.milestones-item__info').find('[data-exclusive-upgrade]').find('span');
        initVoucher(redemptionVoucher, data0, total50000MilesRedemption );
        initVoucher(doubleMilesAccrual, data2, totalDoubleMiles );
        initVoucher(airportUpgrade, data4, totalAirportUpgrade);
        initVoucher(exclusiveUpgrade, data3, totalExclusiveUpgrade);
      }
    });
    $(document).off('click.ico-tooltips').on('keydown','.ico-tooltips',function(e) {
      switch(e.keyCode) {
        case 13:
          $(this).trigger('click');
        break;
      }
    });
  };
  yourVoucher();

};
