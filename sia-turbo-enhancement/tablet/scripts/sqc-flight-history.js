SIA.SQCFlightHistory = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var config = global.config;

	var formSearch = $('.form-search-flight-history');
	var FilterFlightHistory = $('.filter-flight-history');
	var btnSeeMore = $('[data-see-more]');
	var tableFlights = $('.flight-history-table table');
	var jsonFlightHistory = globalJson.jsonFlightHistory;
	var	template = '';

	if(!template){
		$.get(SIA.global.config.url.flightHistoryTemplate, function (tpl) {
			template = tpl;
		}, 'html');

		var filterData = function(data) {
			var getValueByName = function(name) {
				return $.grep(data, function(a) {
					return a.name === name;
				})[0].value;
			};

			var dataFilter = {
				from: getValueByName('from-select'),
				to: getValueByName('to-select'),
				className: getValueByName('class-select'),
				departureDate: getValueByName('month-select'),
				bookedBy: getValueByName('bookedBy-select')
			};

			var filteredData =  jsonFlightHistory.flightHistory.filter(function(item) {
				var toMatched = !dataFilter.to ? true : (item.to !== '' ? (dataFilter.to.indexOf(item.to) >= 0) : false);
				var fromMatched = !dataFilter.from ? true : (item.from !== '' ? (dataFilter.from.indexOf(item.from) >= 0) : false);
				var classMatched = !dataFilter.className ? true : (item.className !== '' ? (dataFilter.className.indexOf(item.className) >= 0) : false);
				var bookedByMatched = !dataFilter.bookedBy ? true : (item.bookedBy !== '' ? (dataFilter.bookedBy.indexOf(item.bookedBy) >= 0) : false);
				var dateMatched = !dataFilter.departureDate ? true : (dataFilter.departureDate !== '' ? (item.departureDate.indexOf(dataFilter.departureDate) >= 0) : false);

				var isMatched = toMatched && fromMatched && classMatched && bookedByMatched && dateMatched;
				return isMatched;
			});
			return filteredData;
		};

		var renderflightHistory = function(filteredData) {
			var fhTable = $('.sqc-flight-table').find('tbody');

			if (fhTable.length) {
				// $.get(SIA.global.config.url.flightHistoryTemplate, function(data) {
				// 	var template = window._.template(data, {
				// 		data: filteredData
				// 	});
				// 	fhTable.html(template);
				// }, 'html');

				var html = window._.template(template, {
					data: filteredData
				});

				fhTable.html(html);
			}
		};

		formSearch.off('submit.SQCFlightHistory').on('submit.SQCFlightHistory', function(e) {
			e.preventDefault();
			var data = $(this).serializeArray();
			var filteredData = filterData(data);
			if(filteredData.length <= 10){
				btnSeeMore.hide();
			}else{
				btnSeeMore.show();
			}
			renderflightHistory(filteredData);

			if(win.width() < config.mobile){
				FilterFlightHistory.find('.popup__close').trigger('click.closePopup');
			}
		});

		var seeMore = function() {
			btnSeeMore.off('click.see-more').on('click.see-more', function() {
				tableFlights.find('tr.hidden:lt(5)').removeClass('hidden');
				if(tableFlights.find('tr.hidden').length === 0){
					$(this).hide();
				}
			});

			if (tableFlights.find('tr.hidden').length === 0) {
				btnSeeMore.hide();
			}
		};

		seeMore();
	}


	/*function sortTable() {
		var sortDate = $('[data-sort-date]');
		var sortBookedBy = $('[data-booked-by]');

		var sortFunc = function() {
			var rows = jQuery.makeArray(tableFlights.find('tbody tr'));
			var numberOfVisible = tableFlights.find('tbody tr').filter(':visible').length;
			rows = rows.sort(function(a, b) {
				a = $(a);
				b = $(b);
				var date1 = new Date(a.find('td').first().find('span').text());
				var date2 = new Date(b.find('td').first().find('span').text());

				if(date1 === date2) {
					var bookedByA = a.find('td').last().find('span').text();
					var bookedByB = b.find('td').last().find('span').text();

					if(bookedByA === bookedByB) {
						return 0;
					}
					if(bookedByA < bookedByB) {
						return (sortBookedBy.data('booked-by') === 'asc' ? -1 : 1);
					}
					else {
						return (sortBookedBy.data('booked-by') === 'asc' ? 1 : -1);
					}
				}
				if(date1 < date2) {
					return (sortDate.data('sort-date') === 'asc') ? -1 : 1;
				}
				else {
					return (sortDate.data('sort-date') === 'asc') ? 1 : -1;
				}
			});

			tableFlights.find('tbody').html(rows);

			tableFlights.find('tbody tr').removeClass('hidden').filter(':gt(' + (numberOfVisible - 1) + ')').addClass('hidden');

			if(tableFlights.find('tr.hidden').length) {
				btnSeeMore.show(0);
			}
			else {
				btnSeeMore.hide(0);
			}
		};

		sortDate.off('click.sort').on('click.sort', function() {
			$(this).find('em').toggleClass('ico-point-u ico-point-d');
			var isSorting = $(this).data('sort-date');
			$(this)
			.data('sort-date', (isSorting === 'asc' ? 'desc' : 'asc'))
			.attr('data-sort-date', (isSorting === 'asc' ? 'desc' : 'asc'));

			sortFunc();
		});

		sortBookedBy.off('click.sort').on('click.sort', function() {
			$(this).find('em').toggleClass('ico-point-u ico-point-d');
			var isSorting = $(this).data('booked-by');
			$(this)
			.data('booked-by', (isSorting === 'asc' ? 'desc' : 'asc'))
			.attr('data-booked-by', (isSorting === 'asc' ? 'desc' : 'asc'));

			sortFunc();
		});

		sortFunc();
	}

	sortTable();*/

	SIA.global.vars.popupGesture(FilterFlightHistory, $('.check-flight-details'), '.popup__close', '.filter-flight-history-content');
};
