/**
 * @name SIA
 * @description
 * @version 1.0
 */
SIA.DESCityGuide = function() {
	var global = SIA.global,
			config = global.config,
			win = global.vars.win,
			winW = win.width(),
			blockSlider = $('#where-to-stay-slider'),
			wrapperSlider = blockSlider.parent(),
			imgSliderLength = blockSlider.find('img').length - 1,
			blockTruncate = $('[data-truncate-height]'),
			totalDesktopSlide = 2,
			totalTabletSlide = 3,
			totalMobileSlide = 1,
			totalLandscapeSlide = 2,
			slideMarginRight = 20,
			timerDetectTruncate,
			timerDetectSlider;

	var detectTruncate = function(el) {
		var blockMaxHeight = el.data('truncateHeight'),
				originContent = el.data('originContent');

		var truncateText = function(maxHeight, strTxt) {
			while(maxHeight < el.outerHeight()) {
				strTxt = strTxt.substr(0, strTxt.lastIndexOf(' '));
				el.html(strTxt + '...' + '<a class="read-more-link" href="#" title="' + L10n.truncateBlock.readMore + '">' + L10n.truncateBlock.readMore + '<a>');
			}
		};

		var showContent = function() {
			el.removeClass('block-truncate').html(originContent).data('isShowContent', true);
		};

		if(!originContent) {
			originContent = el.html();
			el.data('originContent', originContent);
		}

		if(el.height() >= blockMaxHeight && win.width() < global.config.mobile) {
			el.addClass('block-truncate');
			truncateText(blockMaxHeight, originContent);
			el.find('.read-more-link').off('click.showContent').on('click.showContent', function(e) {
				e.preventDefault();
				showContent();
			});
		} else if(winW !== win.width()) {
			showContent();
			winW = win.width();
		}
	};

	var loadBackgroundSlider = function(self, parentSelf, idx) {
		if(global.vars.detectDevice.isTablet() || global.vars.detectDevice.isMobile()) {
			parentSelf.css({
				'background-image': 'url(' + self.attr('src') + ')'
			});
			self.attr('src', config.imgSrc.transparent);
		}

		if(idx === imgSliderLength) {
			blockSlider.width(wrapperSlider.width() + (win.width() < 480 ? 0 : slideMarginRight));
			blockSlider.css('visibility', 'visible');
			blockSlider.find('.slides').slick({
				siaCustomisations: true,
				dots: true,
				speed: 300,
				draggable: true,
				slidesToShow: totalDesktopSlide,
				slidesToScroll: totalDesktopSlide,
				accessibility: false,
				responsive: [
					{
						breakpoint: config.tablet,
						settings: {
							slidesToShow: totalTabletSlide,
							slidesToScroll: totalTabletSlide
						}
					},
					{
						breakpoint: config.mobile,
						settings: {
							slidesToShow: totalLandscapeSlide,
							slidesToScroll: totalLandscapeSlide
						}
					},
					{
						breakpoint: 480,
						settings: {
							slidesToShow: totalMobileSlide,
							slidesToScroll: totalMobileSlide
						}
					}
				]
			});

			win.off('resize.blockSlider').on('resize.blockSlider',function() {
				blockSlider.width(wrapperSlider.width() + (win.width() < 480 ? 0 : slideMarginRight));
			}).trigger('resize.blockSlider');
		}
	};

	blockSlider.find('img').each(function(idx) {
		var self = $(this),
				parentSelf = self.parent(),
				newImg = new Image();

		newImg.onload = function() {
			loadBackgroundSlider(self, parentSelf, idx);
		};
		newImg.src = self.attr('src');
	});

	blockTruncate.each(function() {
		var self = $(this);
		if(!self.data('isShowContent')) {
			detectTruncate(self);
		}
	});

	win.off('resize.detectTruncate').on('resize.detectTruncate', function() {
		clearTimeout(timerDetectTruncate);
		timerDetectTruncate = setTimeout(function() {
			blockTruncate.each(function() {
				var self = $(this);
				if(!self.data('isShowContent')) {
					detectTruncate(self);
				}
			});
		}, 400);
	});

	win.off('resize.mobileSlider').on('resize.mobileSlider',function() {
		clearTimeout(timerDetectSlider);
		timerDetectSlider = setTimeout(function() {
			$('[data-mobile-slider] .slides').each(function() {
				var slider = $(this);
				if(global.vars.detectDevice.isMobile()) {
					if(!slider.hasClass('slick-initialized')) {
						slider
							.slick({
								siaCustomisations: true,
								dots: true,
								speed: 300,
								draggable: true,
								slidesToShow: totalLandscapeSlide,
								slidesToScroll: totalLandscapeSlide,
								accessibility: false,
								arrows: false,
								responsive: [
									{
										breakpoint: 480,
										settings: {
											slidesToShow: totalMobileSlide,
											slidesToScroll: totalMobileSlide
										}
									}
								]
							});
					}
				} else {
					if(slider.hasClass('slick-initialized')) {
						slider.slick('unslick');
					}
				}
			});
		}, 400);
	}).trigger('resize.mobileSlider');
};
