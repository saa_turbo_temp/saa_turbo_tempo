/**
 *  @name tabMenu
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'chart';
	//var win = $(window);
	//var body = $(document.body);
	//var html = $('html');

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
			that = plugin.element;
			that.cirmask = that.find('.circle-mask.circle-full');
			that.circleFill = that.find('.circle__fill:not(.circle--fix)');
			that.circleFix = that.find('.circle__fill.circle--fix');
			that.itemDesc = that.find('.item-desc__info__heading');
			that.dialsChartCircle = that.find('.dials-chart-circle');
			that.kfPoints = this.element.find('[data-kf-points]');
			this.options.endVal = that.kfPoints.data('kfPoints');
			that.kfRequired = this.element.find('[data-kf-required]').data('kfRequired');

			if (that.kfRequired < this.options.endVal && that.dialsChartCircle.hasClass('dials-large-kf-points')) {
				that.dialsChartCircle.removeClass('dials-large-kf-points');
			}

			that.iconDials = that.kfPoints.siblings('.ico-dials');
			that.colorCircle = this.element.data('color');
			that.circleFill.css('background-color',that.colorCircle);
			that.circleFix.css('background-color',that.colorCircle);
			that.iconDials.css({'background-color': that.colorCircle, 'border-color': that.colorCircle});
			this.options.incrementVal = (this.options.endVal * 100)/ (this.options.endVal + that.kfRequired);

		},
		show: function(){
			var plugin = this,
			that = plugin.element;
			that.options = plugin.options;
			if(!that.options.endVal){
				return;
			}
			var incrementAnimate;
			incrementAnimate = (that.options.increment * that.options.incrementVal);
			that.cirmask.transition({
				rotate: incrementAnimate
			},plugin.options.duration);

			if (incrementAnimate < 90) {
				that.circleFill.hide();
			}
			else{
				that.circleFill.transition({
					rotate: incrementAnimate
				},plugin.options.duration);
			}

			// that.circleFix.transition({
			// 	rotate: incrementAnimate*2
			// },plugin.options.duration);

			setTimeout(function(){
				that.circleFix.transition({
					rotate: incrementAnimate*2
				},plugin.options.duration);
				if (that.kfRequired < that.options.endVal) {
					that.dialsChartCircle.addClass('dials-large-kf-points');
				}
			}, plugin.options.duration);


			$({kfpoints: that.options.startVal}).stop().animate({kfpoints: that.options.endVal}, {
					duration: plugin.options.duration,
					step: function () {
						that.itemDesc.text(accounting.formatNumber(Math.ceil(this.kfpoints)));
					},
					complete: function() {
						that.itemDesc.text(accounting.formatNumber(that.options.endVal));
					}
				});

			// setTimeout(function() {
			// 	if (that.kfRequired < that.options.endVal) {
			// 		that.dialsChartCircle.addClass('dials-large-kf-points');
			// 	}
			// }, plugin.options.delay);
		},
		reset: function() {
			var plugin = this,
			that = plugin.element;

			if (that.dialsChartCircle.hasClass('dials-large-kf-points')) {
				that.dialsChartCircle.removeClass('dials-large-kf-points');
			}

			that.itemDesc.text(accounting.formatNumber(0));
			that.cirmask.css('transform', '');
			that.circleFill.css('transform', '');
			that.circleFix.css('transform', '');
		},
		refresh: function(){
			var plugin = this;
			plugin.reset();
			setTimeout(function(){
				plugin.show();
			}, plugin.options.delay);
		},
		option: function(options) {
			this.element.options = $.extend({}, this.options, options);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		startVal: 0,
		endVal: 0,
		increment: 0,
		incrementVal: 0,
		delay: 50,
		duration: 600
	};

}(jQuery, window));
