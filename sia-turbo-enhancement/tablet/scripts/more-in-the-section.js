/**
 * @name SIA
 * @description Define global moreInTheSection functions
 * @version 1.0
 */
SIA.moreInTheSection = function(){
	var global = SIA.global;
	var config = global.config;
	var	win = global.vars.win;
	var	totalMobileSlide = 1;
	var	totalLandscapeSlide = 2;
	var	totalTabletSlide = 3;
	var	timerDetectSlider;

	var initSliderMoreIn = function(){
		var blockMoreIn = $('[data-tablet-slider] .slides');

		win.off('resize.tabletSlider').on('resize.tabletSlider',function() {
			clearTimeout(timerDetectSlider);
			timerDetectSlider = setTimeout(function() {
				if(window.innerWidth < config.tablet) {
					blockMoreIn.each(function() {
						var slider = $(this);
						if(!slider.hasClass('slick-initialized')) {
							slider
								.slick({
									siaCustomisations: true,
									dots: true,
									speed: 300,
									draggable: true,
									slidesToShow: totalTabletSlide,
									slidesToScroll: totalTabletSlide,
									accessibility: false,
									arrows: false,
									responsive: [
										{
											breakpoint: 768,
											settings: {
												slidesToShow: totalLandscapeSlide,
												slidesToScroll: totalLandscapeSlide
											}
										},
										{
											breakpoint: 480,
											settings: {
												slidesToShow: totalMobileSlide,
												slidesToScroll: totalMobileSlide
											}
										}
									]
								});
						}
					});
				}
				else{
					blockMoreIn.each(function(){
						var slider = $(this);
						if(slider.hasClass('slick-initialized')) {
							slider.slick('unslick');
						}
					});
				}
			}, 50);
			// Correct timeout from 400 into 50 for resize window
		}).trigger('resize.tabletSlider');
	};

	var initModule = function(){
		initSliderMoreIn();
	};

	initModule();
};
