SIA.sqcUpcomingFlight = function() {
	//sqcUpcomingFlightTemplate
	//sqcUpcomingFlightDetailTemplate
	var config = SIA.global.config;
	var container = $('[data-fill-flights]');
	var upcomingInfoTemplate;
	var btnMore = $('[data-see-more]');
	var upcomingForm = $('.form-upcoming-flight');
	var sqcUpcomingFlights = globalJson.sqcUpcomingFlights;

	upcomingForm.validate({
			focusInvalid: true,
			errorPlacement: SIA.global.vars.validateErrorPlacement,
			success: SIA.global.vars.validateSuccess,
			submitHandler: function(){
				var upcomingDetail = $('input[name="upcoming-details"]').val();
				var upcomingName = $('input[name="upcoming-name"]').val();

				sqcUpcomingFlights = globalJson.sqcUpcomingFlights;
				sqcUpcomingFlights = getDataAfterSearch(upcomingDetail, upcomingName);
				container.empty();
				fillContent();
				return false;
			}
		});

	function getDataAfterSearch(upcomingDetail, upcomingName){
		var searchData = [];
		var sqcUpcomingFlights = globalJson.sqcUpcomingFlights;
		var len = sqcUpcomingFlights.length;

		for(var i = 0; i < len; i++){
			if(sqcUpcomingFlights[i].referenceNumber.toLowerCase().indexOf(upcomingDetail.toLowerCase()) >= 0  &&
				 isPassengerExist(sqcUpcomingFlights[i].passengers, upcomingName)){
				searchData.push(sqcUpcomingFlights[i]);
			}
		}
		return searchData;
	}

	function isPassengerExist(data, searchString){
		var len = data.length;
		for(var i = 0; i < len; i++){
			var testString = data[i].toLowerCase();
			if(testString.indexOf(searchString.toLowerCase()) > 0){
				return true;
			}
		}
		return false;
	}

	function getFlightInto(block) {
		block.find('.flights--detail > span')
		.off('click.getFlightInfo')
		.on('click.getFlightInfo', function() {
			var self = $(this);
			var details = self.siblings('.details');
			if(details.is('.hidden')) {
				$.ajax({
					url: config.url.orbFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						self.children('em').toggleClass('ico-point-d ico-point-u');
						details.toggleClass('hidden');
						var html = '';
						html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						for(var ft in res.flyingTimes) {
							html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
						}
						details.html(html);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
						if(textStatus !== 'abort') {
							window.alert(errorThrown);
						}
					},
					beforeSend: function() {
						self.children('em').addClass('hidden');
						self.children('.loading').removeClass('hidden');
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				self.children('em').toggleClass('ico-point-d ico-point-u');
				details.toggleClass('hidden');
			}
		});
	}

	function showListPassengers(block) {
		block.find('[data-toggle-passengers-list]')
		.off('click.toggle-list-passengers')
		.on('click.toggle-list-passengers', function(e) {
			e.preventDefault();
			var listPassengers = $(this).siblings('[data-passengers]');
			$(this).toggleClass('active');
			var showLess = $(this).is('.active');
			var passengers = listPassengers.data('passengers').split(',');
			var lis = '';
			for(var i = 0; i < (showLess ? passengers.length : 3); i++) {
				lis += '<li>' + passengers[i];
				if(i < (showLess ? passengers.length : 3) - 1) {
					lis += ',';
				}
				lis += '</li>';
			}
			listPassengers.html(lis);
			listPassengers.children().last().addClass('last');
		});
	}

	function getUpcomingInfo(block) {
		var url = block.data('url');
		$.ajax({
			url: url,
			type: config.ajaxMethod,
			dataType: 'json',
			data: {},
			success: function(res) {
				var template = window._.template(upcomingInfoTemplate, {
					data: res
				});
				block.find('.loading').addClass('hidden');
				if(res.btnLabel) {
					block.find('.booking-form').removeClass('hidden').find('.btn-1').text(res.btnLabel);
				}
				block.find('.ico-point-d').removeClass('hidden');
				block.find('.booking-info-group').html(template);

				getFlightInto(block);
				showListPassengers(block);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if(jqXHR && textStatus !== 'abort') {
					window.alert(errorThrown);
				}
			}
		});
	}

	function fillContent() {
		$.get(config.url.sqcUpcomingFlightDetailTemplate, function(html) {
			upcomingInfoTemplate = html;
		});
		$.get(config.url.sqcUpcomingFlightTemplate, function(html) {
			sqcUpcomingFlights = sqcUpcomingFlights.sort(function(a,b){
				return Date.parse(b.departureDate) - Date.parse(a.departureDate);
			});
			var template = window._.template(html, {
				data: sqcUpcomingFlights
			});
			container.html(template);

			container.find('[data-upcoming-flight]').each(function() {
				getUpcomingInfo($(this));
			});

			SIA.accordion.initAccordion();
		});
		if(sqcUpcomingFlights.length > 3) {
			btnMore.show();
		}
		else {
			btnMore.hide();
		}
		btnMore.off('click.show-more').on('click.show-more', function(e) {
			e.preventDefault();
			$('.hidden[data-upcoming-flight], .hidden[data-upcoming-flight-alert]').removeClass('hidden');
			$(this).hide(0);
		});
	}

	fillContent();
};
