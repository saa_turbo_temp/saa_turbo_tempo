/**
 * @name SIA
 * @description Define global roundTrip functions
 * @version 1.0
 */
SIA.roundTrip = function(){
	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;
	var globalJson = window.globalJson || {};

	var outPromotionPopup = $('.popup--promotion-outpromotionday'),
			outProReturnPopup = $('.popup--promotion-return-day');
	outPromotionPopup.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close]'
	});
	outProReturnPopup.Popup({
		overlayBGTemplate: config.template.overlay,
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close]'
	});

	$('[data-return-flight]').each(function(){
		var wrapper = $(this);
		var returnDatepicker = wrapper.find('[data-start-date]');
		var returnDay = wrapper.find('[data-return-date]');
		var wpReturnDatepicker = returnDatepicker.closest('.input-3');
		var wpReturnDay = returnDay.closest('.input-3');
		var timerDatepicker = null;
		//var timerOnKeyValidation = null;
		var date2Holder = null;
		var storeDayInst = null;
		var wW = win.width();
		var timerHideDP = null;

		var checkPromotionDate = function(promoDateJSON, dateTime) {
			if(promoDateJSON) {
				var i = 0,
						maxLength = promoDateJSON.length;
				while(i < maxLength) {
					var startTime = promoDateJSON[i],
							endTime = promoDateJSON[i + 1];
					if(startTime && endTime) {
						if(dateTime >= new Date(startTime).getTime() && dateTime <= new Date(endTime).getTime()) {
							return 'promotion-day';
						}
					}
					i += 2;
				}
			}
			return '';
		};

		var returnShowDate = function(d1, d2, inst){
			var a = new Date(d1);
			var b = new Date(d2);
			if(global.config.datepicker.numberOfMonths === 1){
				inst.settings.showCurrentAtPos = 0;
				return;
			}
			if((b - a)/(30*24*60*60*1000) > 1){
				inst.settings.showCurrentAtPos = 1;
				return;
			}
			else{
				if(a.getMonth() === b.getMonth() && a.getFullYear() === b.getFullYear()){
					inst.settings.showCurrentAtPos = 0;
					return;
				}
				else{
					inst.settings.showCurrentAtPos = 1;
					return;
				}
			}
		};

		var beforeShowDayDepart = function(date, other, month) {
			var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
			var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
			var promotionClass = '';
			if(globalJson.promotionDate) {
				promotionClass = checkPromotionDate(globalJson.promotionDate, date.getTime());
			}
			return classGeneratorDepart(date, date1, date2, other, month, promotionClass);
		};

		var classGeneratorDepart = function(date, date1, date2, other, month, promotionClass){
			if(!other && date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))){
				if(date2 && (date.getDate() === date1.getDate() && date.getMonth() === date1.getMonth() && date.getFullYear() === date1.getFullYear())){
					if(date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear()){
						return [true, 'dp-start-highlight dp-end-highlight dp-highlight ' + promotionClass];
					}
					return [true, 'dp-start-highlight dp-highlight ' + promotionClass];
				}
				if(date2 && (date.getDate() === date2.getDate() && date.getMonth() === date2.getMonth() && date.getFullYear() === date2.getFullYear())){
					return [true, 'dp-end-highlight dp-highlight ' + promotionClass];
				}
				if(date1 && !date2){
					return [true, promotionClass];
				}
				return [true, 'dp-highlight ' + promotionClass];
			}
			else if(other && date1 && date2){
				date2Holder = new Date(date2);
				if(date1 && date2 && date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear()){
					return [true, promotionClass];
				}
				if(date1 && date2 && date1.getTime() < date2.getTime() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear()){
					return [true, promotionClass];
				}
				if(date1.getMonth() - 1 === month && date1.getFullYear() === date.getFullYear()){
					return [true, promotionClass];
				}
				if(date2.getMonth() === month - 1 && date2.getFullYear() === date.getFullYear()){
					return [true, promotionClass];
				}
				if(date1 && date2 && date1.getTime() < date2.getTime() && date1.getFullYear() < date2.getFullYear() && date2Holder.setDate(date2Holder.getDate() + 12) > date.getTime() && date.getTime() > date1.getTime()){
					// set class when return year is greater than depart year
					if(date2.getMonth() === month && date2.getTime() < date.getTime()){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date2.getTime() < date.getTime() && date2.getMonth() - 1 < month){
					return [true, promotionClass];
				}
				if(date1 && date2 && date.getMonth() !== date1.getMonth() && date.getFullYear() === date1.getFullYear()  && date.getTime() >= date1.getTime() && date.getTime() <= date2Holder.setDate(date2Holder.getDate() + 12) && date1.getTime() < date2Holder.getTime() && date1.getMonth() < date2.getMonth()){
					// if there is unwanted row
					if(!month){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date.getTime() <= date2.getTime() && date.getMonth() === date1.getMonth() && date2.getMonth() - date1.getMonth() >1){
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date.getTime() < date2.getTime() && date.getMonth() === date1.getMonth() && date.getFullYear() === date1.getFullYear() && date2.getMonth() === month){
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1.getFullYear() === date.getFullYear() && date1.getMonth() === date.getMonth() && date1.getDate() - date.getDate() <= 6 ){
					if(month - date1.getMonth() > 1){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight' + promotionClass];
				}
			}
			return [true, promotionClass];
		};

		var beforeShowDayReturn = function(date, other, month) {
			var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
			var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
			var promotionClass = '';
			if(globalJson.promotionDate) {
				promotionClass = checkPromotionDate(globalJson.promotionDate, date.getTime());
			}
			return classGeneratorReturn(date, date1, date2, other, month, promotionClass);
		};

		var classGeneratorReturn = function(date, date1, date2, other, month, promotionClass){
			if(!other && date1 && ((date.getTime() === date1.getTime()) || (date2 && date >= date1 && date <= date2))){
				if(date1 && !date2){
					return [true, 'ui-datepicker-current-day ' + promotionClass];
				}
				else if(date1 && date2){
					if((date.getDate() === date1.getDate() && date.getMonth() === date1.getMonth() && date.getFullYear() === date1.getFullYear())){
						if(date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear()){
							return [true, 'dp-start-highlight dp-end-highlight dp-highlight ' + promotionClass];
						}
						return [true, 'dp-start-highlight dp-highlight ' + promotionClass];
					}
					if((date.getDate() === date2.getDate() && date.getMonth() === date2.getMonth() && date.getFullYear() === date2.getFullYear())){
						return [true, 'dp-end-highlight dp-highlight ' + promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
			}
			else if(other && date1 && date2){
				date2Holder = new Date(date2);
				if(date1 && date2 && date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear()){
					return [true, promotionClass];
				}
				if(date1 && date2 && date1.getTime() < date2.getTime() && date1.getMonth() === date2.getMonth() && date2.getFullYear() === date1.getFullYear()){
					return [true, promotionClass];
				}
				if(date1.getMonth() - 1 === month && date1.getFullYear() === date.getFullYear()){
					return [true, promotionClass];
				}
				if(date2.getMonth() === month - 1 && date2.getFullYear() === date.getFullYear()){
					// if(date2.getMonth() - date1.getMonth() === 1 && date2.getFullYear() === date1.getFullYear()){
					// 	return [true, 'dp-highlight'];
					// }
					return [true, promotionClass];
				}
				if(date1 && date2 && date1.getTime() < date2.getTime() && date1.getFullYear() < date2.getFullYear() && date2Holder.setDate(date2Holder.getDate() + 12) > date.getTime() && date.getTime() > date1.getTime()){
					// set class when return year is greater than depart year
					if(date2.getMonth() === month && date2.getTime() < date.getTime()){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date2.getTime() < date.getTime() && date2.getMonth() - 1 < month){
					return [true, promotionClass];
				}
				if(date1 && date2 && date.getMonth() !== date1.getMonth() && date.getFullYear() === date1.getFullYear()  && date.getTime() >= date1.getTime() && date.getTime() <= date2Holder.setDate(date2Holder.getDate() + 12) && date1.getTime() < date2Holder.getTime() && date1.getMonth() < date2.getMonth()){
					// if there is unwanted row
					if(!month){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date.getTime() <= date2.getTime() && date.getMonth() === date1.getMonth() && date2.getMonth() - date1.getMonth() >1){
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1 && date2 && date.getTime() < date2.getTime() && date.getMonth() === date1.getMonth() && date.getFullYear() === date1.getFullYear() && date2.getMonth() === month){
					return [true, 'dp-highlight ' + promotionClass];
				}
				if(date1.getFullYear() === date.getFullYear() && date1.getMonth() === date.getMonth() && date1.getDate() - date.getDate() <= 6 ){
					if(month - date1.getMonth() > 1){
						return [true, promotionClass];
					}
					return [true, 'dp-highlight ' + promotionClass];
				}
			}
			return [true, promotionClass];
		};
		var updateDepartDate = function(fromDate, destinareDate, specialDateStr) {
			if(destinareDate.data('plus-date')) {
				var plusNumber = parseInt(destinareDate.data('plus-date')) || 0;
				var startDateTmp = fromDate.datepicker('getDate');
				if(startDateTmp != null) {
					var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate() + plusNumber));
					destinareDate.datepicker('option', 'minDate',
						returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
					);
				}
			} else if (specialDateStr) {
				destinareDate.datepicker('option', 'minDate', specialDateStr);
			} else {
				destinareDate.datepicker('option', 'minDate', fromDate.data('date'));
			}
		};
		var onSelectDepart = function(dateText, objDate) {
			var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
			var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
			if(returnDatepicker.closest('#travel-widget').data('widget-v1') || returnDatepicker.closest('#travel-widget').data('widget-v2')) {
				$('#travel-widget [data-start-date], #travel-widget [data-oneway]').not(returnDatepicker).closest('.input-3').removeClass('default');
				$('#travel-widget [data-oneway]').val(dateText);
				$('#travel-widget [data-start-date]').not(returnDatepicker).each(function(){
					$(this).val(dateText);
					var selfReturn = $(this).closest('[data-return-flight]').find('[data-return-date]');
					if(selfReturn.val() !== "") {
						var pselfReturn = selfReturn.datepicker('getDate').getTime();
						var pDepartDate = returnDatepicker.datepicker('getDate').getTime();
						if(selfReturn.is('[data-plus-date]')) {
							if(pselfReturn <= pDepartDate) {
								selfReturn.val(null);
							}
						} else {
							if(pselfReturn < pDepartDate) {
								selfReturn.val(null);
							}
						}
					}
					var startDateTmp = returnDatepicker.datepicker('getDate');
					var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate()));
					selfReturn.datepicker('option', 'minDate',
						returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
					);
				});
			};
			returnDatepicker.closest('.form-group').data('change', true);
			wpReturnDatepicker.removeClass('default');
			if (date1 && date2) {
				storeDayInst.settings.showCurrentAtPos = 0;
				returnDay.val('');
				returnDay.data('date', '');
			}
			// Check if need to plus date to return Date
			if(returnDay.data('plus-date')) {
				var plusNumber = parseInt(returnDay.data('plus-date')) || 0;
				var startDateTmp = returnDatepicker.datepicker('getDate');
				var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate() + plusNumber));
				returnDay.datepicker('change', 'minDate',
					returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
				);
			}
			var selectedDate = new Date(objDate.selectedYear, objDate.selectedMonth, objDate.selectedDay);
			var validator = returnDatepicker.closest('form').validate();

			if(globalJson.promotionDate && !checkPromotionDate(globalJson.promotionDate, selectedDate.getTime())) {
				returnDatepicker.datepicker('hide');
				validator.element(returnDatepicker);
				outPromotionPopup.Popup('show');
			} else {
				var timeInput = returnDatepicker.data('time-input');
				var updateDepartDateTimeout = null;
				updateDepartDateTimeout && clearTimeout(updateDepartDateTimeout);
				if (timeInput) {
					updateDepartDateTimeout = setTimeout(function(){
						wrapper.find(timeInput).focus();
					}, 100);
				} else if (returnDay.is(':visible')) {
					updateDepartDateTimeout = setTimeout(function(){
						updateDepartDate(returnDatepicker, returnDay);
						returnDay.datepicker('show');
					}, 100);
					validator.element(returnDatepicker);

				}
				// validator.element(returnDatepicker);
			}
		};
		var onSelectReturn = function(dateText, objDate) {
			var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
			// var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
			var cd = new Date();
			var c = $.datepicker.parseDate('dd/mm/yy', dateText);
			wpReturnDay.removeClass('default');
			returnDay.data('date', dateText);
			returnDay.val(dateText);
			if(returnDay.closest('#travel-widget').data('widget-v1') || returnDay.closest('#travel-widget').data('widget-v2')) {
				var syncReturnDate = $('#travel-widget [data-return-date]').not(returnDay);
				var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
				syncReturnDate.each(function(){
					if($(this).closest('form').is('#book-hotel')) {
						if(date1 < date2 || date1 > date2) {
							$(this).closest('.input-3').removeClass('default');
							$(this).val(dateText);
						} else {
							$(this).val(null);
						}
						if($(this).data('plus-date')) {
							var plusNumber = parseInt($(this).data('plus-date')) || 0;
							var fromDate = $(this).closest('form').find('[data-start-date]');
							var startDateTmp = fromDate.datepicker('getDate');
							var returnDate = new Date(startDateTmp.setDate(startDateTmp.getDate() + plusNumber));
							$(this).datepicker('option', 'minDate',
								returnDate.getDate() + '/' + (returnDate.getMonth() + 1) + '/' + returnDate.getFullYear()
							);
						}
					} else {
						$(this).closest('.input-3').removeClass('default');
						$(this).val(dateText);
					}
				});
			};
			if(date1 && date1.getTime() > c.getTime()){
				if(c.getTime() < cd.getTime()){
					returnDatepicker.val($.datepicker.formatDate('dd/mm/yy', cd)).data('date', $.datepicker.formatDate('dd/mm/yy', cd));
				}
				else{
					returnDatepicker.val(dateText).data('date', dateText);
				}
				returnDay.val('').data('date', '');
				setTimeout(function(){
					returnDay.datepicker('show');
				}, 100);
			} else {
				var selectedDate = new Date(objDate.selectedYear, objDate.selectedMonth, objDate.selectedDay);
				if(globalJson.promotionDate && !checkPromotionDate(globalJson.promotionDate, selectedDate.getTime())) {
					returnDay.datepicker('hide');
					outProReturnPopup.Popup('show');
				}
				var validator = returnDay.closest('form').validate();
				validator.element(returnDay);
			}
		};

		var processChangeMonthYear = function(year, month, inst) {
			$(inst.dpDiv).addClass('ui-datepicker-fullwidth').find('.ui-state-default').wrap('<div class="wrap-ui-state-default"></div>');
			clearTimeout(timerDatepicker);
			timerDatepicker = setTimeout(function() {
				$(inst.dpDiv).removeClass('ui-datepicker-fullwidth').find('.ui-state-default').wrap('<div class="wrap-ui-state-default"></div>');
			}, 1000);
		};

		returnDatepicker.datepicker({
			numberOfMonths: global.config.datepicker.numberOfMonths,
			showOtherMonths: true,
			dateFormat: 'dd/mm/yy',
			showAnim: '',
			dayNamesMin: global.config.formatDays,
			beforeShowDay: beforeShowDayDepart,
			onSelect: onSelectDepart,
			beforeShow: function(input, inst){
				if(input.value && input.value !== $(input).prop('placeholder') && input.value !== 'dd/mm/yyyy') {
					returnDatepicker.data('date', input.value);
				}
				storeDayInst = inst;

				wW = win.width();
				var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
				var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
				if(date1 && date2 && date1.getTime() > date2.getTime() || !date1){
					returnDay.val('').data('date', '');
				}
				wpReturnDatepicker.addClass('focus');
				clearTimeout(timerDatepicker);
				timerDatepicker = setTimeout(function() {
					var dpEl = $(inst.dpDiv),
							leftDp = wpReturnDatepicker.offset().left;
					if(global.vars.detectDevice.isTablet()){
						leftDp = 'auto';
					}
					dpEl.css({
						'zIndex': global.config.zIndex.datepicker,
						'right': leftDp,
						// 'left': wpReturnDatepicker.offset().left,
						// 'top': wpReturnDatepicker.offset().top + wpReturnDatepicker.height() + 10,
						'display': 'block'
					}).off('click.doNothing').on('click.doNothing', function(){
						// fix for lumina
					});
					// if(global.vars.detectDevice.isMobile()){
					// 	$(inst.dpDiv).width(187);
					// }
					if(global.vars.detectDevice.isTablet()){
						$(inst.dpDiv).css('width','');
					}
					dpEl.find('.ui-state-default').wrap('<div class="wrap-ui-state-default"></div>');
				},100);
				win.off('resize.hideDatepickerDepart').on('resize.hideDatepickerDepart', function(){
					clearTimeout(timerHideDP);
					setTimeout(function(){
						if(wW !== win.width()){
							returnDatepicker.blur().datepicker('hide');
						}
					}, 100);
				});
			},
			onClose: function(input, inst) {
				var currentReturnDatepicker = $(this).val();
				var isValidDay = $.inputmask.isValid(currentReturnDatepicker, { alias: 'dd/mm/yyyy'});
				if(!isValidDay){
					returnDatepicker.val('');
					returnDatepicker.data('date', '');
				}
				wpReturnDatepicker.removeClass('focus');
				wpReturnDay.removeClass('focus');
				win.off('resize.hideDatepickerDepart');
				$(inst.dpDiv).off('click.doNothing');
				$(this).closest('[data-target]').next().find('input[data-oneway]').val(returnDatepicker.val()).next().val(returnDatepicker.val());
				if(returnDatepicker.val() === '' || returnDatepicker.val() === 'dd/mm/yyyy' || returnDatepicker.val() === returnDatepicker.attr('placeholder')){
					wpReturnDatepicker.addClass('default');
				}
				updateDepartDate(returnDatepicker, returnDay, input);
			},
			onChangeMonthYear: processChangeMonthYear,
			minDate: new Date(),
			maxDate: '+355d'
		}).prop('readonly', true).click(function() {
			$(this).datepicker('show');
			if(/SAMSUNG/.test(window.navigator.userAgent)){
				$('#ui-datepicker-div').css('overflow','hidden');
			}
		});

		returnDay.datepicker({
			numberOfMonths: global.config.datepicker.numberOfMonths,
			showOtherMonths: true,
			dateFormat: 'dd/mm/yy',
			showAnim: '',
			dayNamesMin: global.config.formatDays,
			onSelect: onSelectReturn,
			showCurrentAtPos: 0,
			beforeShowDay: beforeShowDayReturn,
			beforeShow: function(input, inst){
				if(input.value && input.value !== $(input).prop('placeholder') && input.value !== 'dd/mm/yyyy') {
					returnDay.data('date', input.value);
				}

				storeDayInst = inst;
				wW = win.width();
				// if(global.vars.detectDevice.isMobile()){
				// 	inst.settings.numberOfMonths = 1;
				// }
				// if(global.vars.detectDevice.isTablet()){
				// 	inst.settings.numberOfMonths = 2;
				// }
				if(!returnDatepicker.val()){
					returnDay.datepicker('hide');
					setTimeout(function(){
						returnDay.val('').data('date', '');
						returnDatepicker.datepicker('show');
					}, 100);
					return;
				} else {
					returnDatepicker.data('date', returnDatepicker.val());
				}

				storeDayInst = inst;
				var date1 = $.datepicker.parseDate('dd/mm/yy', returnDatepicker.data('date'));
				var date2 = $.datepicker.parseDate('dd/mm/yy', returnDay.data('date'));
				if(date1 && date2){
					if(date1.getTime() > date2.getTime()){
						returnDay.val('').data('date', '');
					}
					else{
						returnShowDate(date1, date2, inst);
					}
				}

				// if(globalJson.promotionDate) {
				// 	if(date2 || (input.value && input.value !== 'dd/mm/yyyy')) {
				// 		var checkDate = date2 || $.datepicker.parseDate('dd/mm/yy', input.value);
				// 		if(!checkPromotionDate(globalJson.promotionDate, checkDate.getTime())) {
				// 			$(input).data('legendMsgClass', 'msg-invalid').data('legendMsg', L10n.promotion.msgInvalidPromo);
				// 		} else {
				// 			$(input).data('legendMsgClass', 'msg-valid').data('legendMsg', L10n.promotion.msgValidPromo);
				// 		}
				// 	}
				// }

				wpReturnDay.addClass('focus');
				clearTimeout(timerDatepicker);
				timerDatepicker = setTimeout(function() {
					var dpEl = $(inst.dpDiv),
							leftDp = wpReturnDay.offset().left;
					if(global.vars.detectDevice.isTablet()){
						leftDp = 'auto';
					}
					dpEl.css({
						'zIndex': global.config.zIndex.datepicker,
						'right': leftDp,
						// 'left': detectPos($(inst.dpDiv)),
						// 'top': wpReturnDay.offset().top + wpReturnDay.height() + 10,
						'display': 'block'
					}).off('click.doNothing').on('click.doNothing', function(){
						// fix for lumina
					});
					// if(global.vars.detectDevice.isMobile()){
					// 	$(inst.dpDiv).width(187);
					// }
					if(global.vars.detectDevice.isTablet()){
						$(inst.dpDiv).css('width','');
					}
					dpEl.find('.ui-state-default').wrap('<div class="wrap-ui-state-default"></div>');
				},100);
				win.off('resize.hideDatepickerReturn').on('resize.hideDatepickerReturn', function(){
					clearTimeout(timerHideDP);
					setTimeout(function(){
						if(wW !== win.width()){
							// returnDatepicker.blur().datepicker('hide');
							returnDay.blur().datepicker('hide');
						}
					}, 100);
				});
			},
			onClose: function(input, inst) {
				var currentReturnDay = $(this).val();
				var isValidDay = $.inputmask.isValid(currentReturnDay, { alias: 'dd/mm/yyyy'});
				if(!isValidDay){
					returnDay.val('');
					returnDay.data('date', '');
				}
				wpReturnDatepicker.removeClass('focus');
				wpReturnDay.removeClass('focus');
				win.off('resize.hideDatepickerReturn');
				$(inst.dpDiv).off('click.doNothing');
				if(returnDay.val() === '' || returnDay.val() === 'dd/mm/yyyy' || returnDay.val() === returnDay.attr('placeholder')){
					wpReturnDay.addClass('default');
				}
			},
			onChangeMonthYear: processChangeMonthYear,
			minDate: new Date(),
			maxDate: '+355d'
		}).prop('readonly', true).click(function() {
			$(this).datepicker('show');
		});

		wpReturnDatepicker.find('.ico-date').parent().off('click.showDatepicker').on('click.showDatepicker', function(){
			returnDatepicker.datepicker('show');
		});
		wpReturnDay.find('.ico-date').parent().off('click.showDatepicker').on('click.showDatepicker', function(){
			returnDay.datepicker('show');
		});
		returnDatepicker.data('date', '');
		returnDay.data('date', '');

		// setTimeout(function() {
		// 	returnDatepicker.datepicker('setDate', 'today');
		// 	returnDatepicker.data('date', returnDatepicker.val()).closest('.input-3').removeClass('default');
		// }, 4000);
	});
};
