/**
 * @name SIA
 * @description Define global autocomplete search functions
 * @version 1.0
 */
SIA.autocompleteSearch = function(){
	var win = SIA.global.vars.win;
	var searchEl = $('[data-autocomplete-search]');

	var getNamespace = (function(){
		var id = 0;
		return function(){
			return 'reposatcl-' + id++;
		};
	})();
	
	// Init autocomplele search
	var initAutocompleteSearch = function() {
		searchEl.each(function(index, el) {
			var field = $(el);
			var sourceUrl = field.data('autocomplete-source') || 'ajax/autocomplete-search.json';
			var term = '';
			var timerResize = null;
			var timer = null;
			var winW = win.width();
			var namespace = getNamespace();
			var searchAutoComplete = field.autocomplete({
				source: function(request, response) {
					term = request.term;
					$.get(sourceUrl + '?term=' + term, function(data) {
						response(data.searchSuggests);
					});
				},
				focus: function() {
					var ulMenu = $(this).autocomplete('widget');
					ulMenu.children('li').removeClass('active');
					ulMenu.find('.ui-state-focus').parent().addClass('active');
				},
				select: function(evt, ui) {
					$(this).val(ui.item.value).closest('form').trigger('submit');
				},
				appendTo: field.closest('.menu-inner').length ? field.closest('.menu-inner') : $('body')
			}).data('ui-autocomplete');

			searchAutoComplete._renderItem = function(ul, item) {
				if (!ul.hasClass('auto-suggest')) {
					ul.addClass('auto-suggest');
				}
				return $('<li class="autocomplete-item" data-value="' + (item.value ? item.value : item.label) + '"><a href="javascript:void(0);">' + (item.label ? item.label : '') + '</a></li>')
					.appendTo(ul);
			};

			searchAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(field.outerWidth());
			};

			field.autocomplete('widget').addClass('autocomplete-menu');
			field.off('blur.autocomplete');

			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				winW = win.width();

				win.off('resize.' + namespace).on('resize.' + namespace, function(){
					clearTimeout(timerResize);
					timerResize = setTimeout(function(){
						if(field.autocomplete('widget').is(':visible')){
							if(winW !== win.width()){
								field.trigger('blur.highlight');
								winW = win.width();
							}
						}
					}, 100);
				});
			});

			field.off('blur.highlight').on('blur.highlight', function(){
				timer = setTimeout(function(){
					field.autocomplete('close');
				}, 100);
				win.off('resize.' + namespace);
			});

			field.autocomplete('widget').off('click.clearTimeout').on('click.clearTimeout', function(){
				clearTimeout(timer);
			});

			field.autocomplete('widget')
				.off('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll')
				.on('scroll.preventScroll mousewheel.preventScroll touchmove.preventScroll', function(e){
					e.stopPropagation();
				});

			// field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
			// 	if(e.which === 13){
			// 		field.autocomplete('widget').find('li.active').trigger('click');
			// 		e.preventDefault();
			// 	}
			// });

			field.off('touchend.fixCrashissue').on('touchend.fixCrashissue', function(e){
				e.stopPropagation();
			});
		});
	};

	var initModule = function() {
		initAutocompleteSearch();
	};

	initModule();
};
