/**
 * @name SIA
 * @description sqc-add-user
 * @version 1.0
 */
SIA.SQCAddUser = function() {
	var global = SIA.global,
			config = global.config,
			popupAddPointSale = $('.popup--add-point-sale'),
			btnAddMore = $('[data-btn-addmore]'),
			wrapperRows = $('[data-table-rows]'),
			addPointInner = $('.add-point-inner'),
			cloneCityRow = popupAddPointSale.find('[data-table-row]').first().clone(),
			errorTpl = '<p class="text-error"><span>Select at least one country.</span></p>',
			posSize = window.posSize || 3,
			saleNum = 0,
			listCountry = $();

//	var ajaxSuccess = function(res) {
//		if(res.salePoint) {
//			if($.parseJSON(res.salePoint.status)) {
//				var pointIsRemoved = $('[data-point-id="' + res.salePoint.id + '"]');
//
//				if(pointIsRemoved.is(':last-child')) {
//					pointIsRemoved.prev('.separate-comma').remove();
//				}
//
//				pointIsRemoved.next('.separate-comma').remove();
//				pointIsRemoved.remove();
//			}
//		}
//	};
//
//	var ajaxFail = function(jqXHR, textStatus) {
//		console.log(textStatus);
//	};
//
//	var initAjax = function(url, data, type, successFunc, errorFunc) {
//		var el = this;
//		type = type || 'json';
//		successFunc = successFunc || ajaxSuccess;
//		errorFunc = errorFunc || ajaxFail;
//		$.ajax({
//			url: url,
//			type: config.ajaxMethod,
//			dataType: type,
//			data: data,
//			success: successFunc,
//			error: errorFunc
//		}).then(function(){
//			el.removeClass('loading loading--small');
//		});
//	};

	var detectGender = function(resourceElement, referenceElement) {
		if(!resourceElement.length || !referenceElement.length) { return; }
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	var generateOptions = function(curCountry, allCountries, initDefaultSelect) {
		var cloneList = listCountry.clone();
		allCountries.not(curCountry).each(function() {
			var vlue = $(this).val();
			if (vlue) {
				cloneList = cloneList.not('option[value="' + vlue + '"]');
			}
		});
		cloneList.filter('option[value="' + curCountry.val() + '"]').attr('selected', 'selected');
		curCountry.empty().append(cloneList);
		if (!initDefaultSelect) {
			curCountry.defaultSelect('refresh');
		}
		else {
			curCountry.defaultSelect({
				wrapper: '.custom-select',
				textCustom: '.select__text',
				isInput: false
			});
		}
	};

	var addNewPoint = function(el) {
		var newRowNumber = popupAddPointSale.find('[data-table-row]').length + 1,
				selectEl = el.find('select'),
				selectNewId = selectEl.attr('id').slice(0, -1) + newRowNumber;
		el.find('[data-row-order]').text(newRowNumber + '.');
		el.find('label').attr('for', selectNewId);
		selectEl.attr('id', selectNewId).attr('name', selectNewId);
		generateOptions(selectEl, wrapperRows.find('select'), true);
		return el;
	};

	var addMorePoint = function(){
		var addPointSubmit = popupAddPointSale.find('#add-point-submit');

		var renderPointTpl = function(){
			var tpl = '';
			popupAddPointSale.find('select').each(function(){
				if($(this).val()){
					if(!tpl && !addPointInner.children().length){
						tpl+=L10n.sqcUser.addTpl.format($(this).val(), $(this).children(':selected').text());
					}
					else{
						tpl+='<span class="separate-comma">,&nbsp;</span>' + L10n.sqcUser.addTpl.format($(this).val(), $(this).children(':selected').text());
					}
				}
			});
			if(tpl){
				$(tpl).appendTo(addPointInner);
			}
		};

		var checkCountry = function() {
			var valid = false;
			wrapperRows.find('select').each(function() {
				if ($(this).val()) {
					valid = true;
					return false;
				}
			});
			return valid;
		};

		addPointSubmit.off('click.addMorePoint').on('click.addMorePoint', function(e){
			e.preventDefault();
			var errorEl = wrapperRows.siblings('p.text-error');

			if (checkCountry()) {
				renderPointTpl();
				popupAddPointSale.Popup('hide');
				if (errorEl.length) {
					errorEl.remove();
				}
			}
			else {
				if (!errorEl.length) {
					$(errorTpl).insertAfter(wrapperRows);
				}
			}
		});
	};

	btnAddMore.off('click.addNewPoint').on('click.addNewPoint', function(e) {
		e.preventDefault();
		wrapperRows.append(addNewPoint(cloneCityRow.clone()));
		wrapperRows.scrollTop(wrapperRows.prop('scrollHeight') - wrapperRows.height());
		popupAddPointSale.Popup('reposition');

		saleNum++;

		if (saleNum === posSize) {
			btnAddMore.attr('disabled', 'disabled').addClass('disabled');
		}
	});

	addPointInner.on('click.removePoint','.remove-point',  function(e) {
		e.preventDefault();
		var el = $(this),
			salePoint = $(this).closest('[data-point-id]');

		el.addClass('loading loading--small');

		$.ajax({
			url: config.url.success,
			type: config.ajaxMethod,
			dataType: 'json',
			data: salePoint.data('pointId'),
			success: function(data) {
				if(data.success){
					if(salePoint.is(':last-child')) {
						salePoint.prev('.separate-comma').remove();
					}
					salePoint.next('.separate-comma').remove();
					salePoint.remove();
				}
				el.removeClass('loading loading--small');
			},
			error: function(){
				window.alert(L10n.sqcUser.salePointError);
				el.removeClass('loading loading--small');
			}
		});
	});

	var initPosPopup = function() {
		listCountry = wrapperRows.find('select').eq(0).find('option').removeAttr('selected');
		if (posSize > 3) {
			btnAddMore.removeAttr('disabled').removeClass('disabled');
			saleNum = 3;
		}
		else {
			wrapperRows.find('[data-table-row]').not(':lt(' + posSize + ')').remove();
			btnAddMore.attr('disabled', 'disabled').addClass('disabled');
			saleNum = wrapperRows.find('[data-table-row]').length;
		}

		wrapperRows
			.off('change.validDropDown')
			.on('change.validDropDown', 'select', function() {
				var self = $(this);
				var all = wrapperRows.find('select');
				var others = all.not(self);
				others.each(function() {
					generateOptions($(this), all);
				});
			});
	};

	var initModule = function() {
		initPosPopup();
		addMorePoint();
		detectGender($('[data-gender-resource]'), $('[data-gender-reference]'));
	};

	initModule();
};
