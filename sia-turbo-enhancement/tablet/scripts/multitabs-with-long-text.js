/**
 * @name SIA
 * @description Define global multiTabsWithLongText functions
 * @version 1.0
 */
SIA.multiTabsWithLongText = function(){
	var global = SIA.global;
	var win = global.vars.win;
	var timerResize;

	var buildSelectForLongText = function(){
		var tabWrapper = $('[data-multi-tab]');
		var tabs = tabWrapper.find('ul.tab');
		// var select = multiTab.find('[data-select-tab]');
		// var tabWrapper = tabs.parent();
		var indexLimitTab = 0;
		var totalwid = 0;
		var tabItem = tabs.children();

		var multiTab1 = $();
		var selectTabs = $();
		var selectTabsText = $();
		var totalwidOneTab = 0;

		tabItem.each(function(i){
			var self = $(this);
			// var ctselect = self.find('select');
			// ctselect.empty().append(select.children(':eq('+i+'),:gt('+i+')').clone());
			totalwidOneTab+=self.outerWidth();
			if(totalwid + self.outerWidth() <= global.config.tablet && !tabItem.eq(indexLimitTab).hasClass('limit-item')){
				totalwid +=(self.outerWidth() - (self.find('em.ico-dropdown').is(':hidden') ? 0 : self.find('em.ico-dropdown').outerWidth(true)));
				indexLimitTab = i;
			}
			else{
				if(!tabItem.eq(indexLimitTab).hasClass('limit-item')){
					// tabWrapper.addClass('multi-tabs');
					tabItem.eq(indexLimitTab).addClass('limit-item').children('[data-customselect]').removeClass('hidden');
				}
			}
		});

		tabWrapper.addClass('multi-tabs');
		tabItem.filter(':gt('+indexLimitTab+')').addClass('hidden');

		var initMultiTabsOne = function(){
			if(totalwidOneTab < tabWrapper.outerWidth()){
				if(win.outerWidth() >= global.config.tablet){
					tabWrapper.removeClass('multi-tabs--1');
				}else{
					tabWrapper.addClass('multi-tabs--1');
				}
			}

			multiTab1 = $('.multi-tabs--1');
			selectTabs = multiTab1.find('[data-select-tab]');
			selectTabsText = selectTabs.find('.select__text');

			if(multiTab1.length){
				multiTab1.find('li').removeClass('hidden limit-item');
			}

			var setWidthSelect = function(){
				if(selectTabs.length){
					var selectTabsDefault = selectTabs.find('select');
					var li = tabWrapper.find('li');
					var txt;
					var idx;

					selectTabs.width(tabWrapper.find('li.active').width());

					selectTabsDefault.off('change.textTabs').on('change.textTabs', function(){
						txt = selectTabsDefault.find('option:selected').text();
						idx = selectTabsDefault.prop('selectedIndex');

						li.removeClass('active');
						li.eq(idx).addClass('active').trigger('click.show');
						selectTabsText.html(txt);
						tabWrapper.find('li.active').children('a').html(txt + '<em class="ico-dropdown"></em>');

						selectTabs.width(tabWrapper.find('li.active').width());

					});
				}
			};

			setWidthSelect();
		};

		initMultiTabsOne();

		if(tabItem.eq(indexLimitTab).hasClass('limit-item')){
			// tabItem.eq(indexLimitTab).addClass('limit-item').children('[data-customselect]').removeClass('hidden');
			var multiSelect = tabItem.eq(indexLimitTab).find('select');
			var customSelectEl = multiSelect.closest('[data-customselect]');
			var limitItem = customSelectEl.closest('.limit-item');
			var indexOfFakeTab = limitItem.index();
			var indexTab = limitItem.is('.active') ? indexOfFakeTab : limitItem.siblings('li.active').index();
			var indexHolder = 0;

			multiSelect.defaultSelect({
				wrapper: '.custom-select',
				textCustom: '.select__text',
				isInput: false
			});
			// var changeIcon = function(){
			// 	var displayTxtEl = customSelectEl.find(multiSelect.data('defaultSelect').options.textCustom),
			// 		txt = multiSelect.find('option:selected').text(),
			// 		txtReplace = customSelectEl.data('replaceTextByPlane'),
			// 		regx = new RegExp(txtReplace, 'gi');
			// 	displayTxtEl.html(txt.replace(regx, '<em class="ico-plane"></em>'));
			// 	customSelectEl.siblings('.mark-desktop').html(txt.replace(regx, '<em class="ico-plane"></em>') + '<em class="ico-dropdown"></em>');
			// };
			if(tabs.data('click-through')){
				customSelectEl.addClass('click-through');
				customSelectEl.closest('li').find('> a').trigger('click.switch-flight');
			}
			customSelectEl.off('click.triggerTab').on('click.triggerTab', function(){
				if(tabs.data('click-through')){
					customSelectEl.closest('li').find('> a').trigger('click.switch-flight');
				}
			});
			multiSelect.off('change.triggerTab').on('change.triggerTab', function(){
				// changeIcon();
				var curIndex = multiSelect.prop('selectedIndex');
				tabs.find('> li > a').eq(curIndex + indexOfFakeTab).trigger('click.switch-flight');
				if(indexTab >= indexLimitTab){
					multiSelect.prop('selectedIndex', indexHolder);
					multiSelect.defaultSelect('refresh');
					// changeIcon();
				}
				// $('#form-seatmap').submit();
			});
			if(indexTab > indexLimitTab){
				multiSelect.prop('selectedIndex', indexTab - limitItem.index());
				indexHolder = indexTab - limitItem.index();
				multiSelect.defaultSelect('refresh');
				limitItem.siblings('li.active').removeClass('active').end().addClass('active');
			}
			// changeIcon();
		}

		win.off('resize.tabMenuOne').on('resize.tabMenuOne', function() {
			clearTimeout(timerResize);
			timerResize = setTimeout(function() {
				initMultiTabsOne();
			}, 400);
		}).trigger('resize.tabMenuOne');

	};

	var initModule = function(){
		buildSelectForLongText();
	};

	initModule();
};
