SIA.ORBFlightSelect = function() {
	var flightTables = $('[data-flight]');
	var global = SIA.global;
	var config = global.config;
	var formFlightSearch = $('[data-form-flight-search]');
	var buttonShowMore = $('[data-see-more]');
	var addStopoverLnk = $('[data-add-stopover]');
	var stopoverContent = $('[data-stopover-content]');

	// var getFlightClassFromId = function(id) {
	// 	var classes = {
	// 		'FF32' : 'Flexi Saver 2 to go',
	// 		'FF42' : 'Sweet Deals 2 to go',
	// 		'FF44' : 'Sweet Deals 4 to go',
	// 		'FF1' : 'Fully Flexi',
	// 		'FF2' : 'Flexi',
	// 		'FF3' : 'Flexi Saver',
	// 		'FF4' : 'Sweet Deals',
	// 		'FF5' : 'Super Deals',
	// 		'FF6' : 'Business',
	// 		'FF8' : 'First/Suites',
	// 		'FC1' : 'Corporate Economy',
	// 		'FC2' : 'Corporate Business',
	// 		'FC3' : 'Corporate First',
	// 		'FD1' : 'Economy Class - Special Fares',
	// 		'FD2' : 'Business Class - Special Fares',
	// 		'FD3' : 'First Class - Special Fares',
	// 		'FD4' : 'Credit Card Promotion - Economy',
	// 		'FD5' : 'Credit Card Promotion - Business',
	// 		'FD6' : 'Credit Card Promotion - First'
	// 	};
	// 	var id = id.substring(2, 5);
	// 	if(classes[id]) {
	// 		return classes[id];
	// 	}
	// 	else {
	// 		id = id.substring(0, 2);
	// 		return classes[id] ? classes[id] : '';
	// 	}
	// };

	var previousBtn = $('a.wi-icon.wi-icon-previous');
	var nextBtn = $('a.wi-icon.wi-icon-next');
	var flightsSearch = $('div.flights__searchs');
	var flightsTable = flightsSearch.find('table.flights__table');
	var titleHead = flightsTable.find('thead .title-head');
	var marginLeftPreBtn = Math.round(previousBtn.width()/2);
	var marginTopPreBtn = Math.round(previousBtn.height()/2);
	var marginTopNexBtn = Math.round(nextBtn.height()/2);
	var posLeftNextBtn = 0,
			posRightNextBtn = 0,
			widthPackage3,
			heightPackage;

	var loopTableTitleHead = function(){
		posLeftNextBtn = 0;
		posRightNextBtn = 0;
		widthPackage3 = 0;
		heightPackage = 0;
		titleHead.each(function( index ) {
			if(index<1){
				posLeftNextBtn += $(this).outerWidth();
				posRightNextBtn += $(this).outerWidth();
			}else if(index<5){
				widthPackage3 = $(this).outerWidth()/2;
				posRightNextBtn += $(this).outerWidth();
			}

			if($(this).hasClass('package-3')){
				heightPackage = $(this).outerHeight()/2;
				posLeftNextBtn += 2;
				posRightNextBtn -= marginLeftPreBtn + 4;
				return false;
			}
		});
	};

	loopTableTitleHead();

	previousBtn.css({'left':posLeftNextBtn,'top':heightPackage,'margin-left':-marginLeftPreBtn,'margin-top':-marginTopPreBtn}).hide();
	previousBtn.off('click.showHidenFare').on('click.showHidenFare',function(){
		flightsTable.removeClass('previous-package');
		flightsTable.addClass('next-package');
		nextBtn.show();
		previousBtn.hide();
		loopTableTitleHead();
		nextBtn.css({'top':$('thead',flightsTable).outerHeight()/2,'left':posRightNextBtn});
	});
	nextBtn.css({'left':posRightNextBtn,'top':heightPackage,'margin-top':-marginTopNexBtn});
	nextBtn.off('click.showHidenFare').on('click.showHidenFare',function(){
		flightsTable.removeClass('next-package');
		flightsTable.addClass('previous-package');
		previousBtn.show();
		nextBtn.hide();
		previousBtn.css({'top':$('thead',flightsTable).outerHeight()/2});
	});
	previousBtn.css('visibility','visible');
	nextBtn.css('visibility','visible');

	var isDelay = false;
	global.vars.win.off('resize.calcBtnControl').on('resize.calcBtnControl', function() {
		if(!isDelay) {
			isDelay = true;
			setTimeout(function() {
				loopTableTitleHead();
				marginLeftPreBtn = marginLeftPreBtn ? marginLeftPreBtn : Math.round(previousBtn.width()/2);
				marginTopPreBtn = marginTopPreBtn ? marginTopPreBtn : Math.round(previousBtn.height()/2);
				marginTopNexBtn = marginTopNexBtn ? marginTopNexBtn : Math.round(nextBtn.height()/2);
				previousBtn.css({'left':posLeftNextBtn,'top':heightPackage,'margin-left':-marginLeftPreBtn,'margin-top':-marginTopPreBtn});
				nextBtn.css({'left':posRightNextBtn,'top':heightPackage,'margin-top':-marginTopNexBtn});
				isDelay = false;
			}, config.duration.clearTimeout);
		}
	});

	// flightsTable.off('click.showHidenFares').on('click.showHidenFares', '.flights__info a.link-5', function() {
	// 	var showFlightInfoMD = $(this).parent().parent().find('.flights__info--mb');
	// 	showFlightInfoMD.toggleClass('visible-mb');
	// });

	$.validator.addMethod('checkdaterange', function(value, el, param) {
		var minDateEl = $(param[0]),
				maxDateEl = $(param[1]),
				minDateValue,
				maxDateValue,
				currentDateValue;

		var convertTimeString = function(dateString) {
			var arrDate = dateString.split('/');
			if(arrDate.length === 3) {
				return new Date(arrDate[2], parseInt(arrDate[1]) - 1, arrDate[0]).getTime();
			}
			return 0;
		};

		if(value) {
			currentDateValue = convertTimeString(value);
			minDateValue = minDateEl.length ? convertTimeString(minDateEl.val()) : 0;
			maxDateValue = maxDateEl.length ? convertTimeString(maxDateEl.val()) : 0;

			if(maxDateValue) {
				return currentDateValue >= minDateValue && currentDateValue <= maxDateValue;
			}
			return currentDateValue >= minDateValue;
		}

		return true;
	}, L10n.validator.checkdaterange);

	var selectFlight = function() {
		var applyToTable = function(table, afterChangeCallback) {
			if(!formFlightSearch.data('form-flight-search')){
				return;
			}
			table.find('.message-waitlisted').parent().parent().addClass('hidden');

			table
			.off('change.select-flight')
			.on('change.select-flight', 'input', function() {
				var input = $(this);
				var tableRow = input.parents('tr').last();
				tableRow.siblings().removeClass('active');
				tableRow.addClass('active');

				// table.find('.class-flight').text('');
				var classSpan = tableRow.find('.class-flight');
				var flightClass = input.data('flight-class');
				classSpan.text(flightClass);

				var related = input.data('related');
				var relatedRdo = tableRow.find('[data-related="' + related + '"]').not(input);
				relatedRdo.prop('checked', true);

				var waitlistedMessage = '<em class="ico-checkbox"></em>';
				var isWaitlisted = false;
				if(globalJson.bookingSummary.pwmMessage) {
					var pwmMessage = globalJson.bookingSummary.pwmMessage[0];
					if(pwmMessage[input.val()]) {
						waitlistedMessage += pwmMessage[input.val()];
						isWaitlisted = true;
					}
				}

				var waitlistedCell = table.find('.message-waitlisted').parent().parent();
				waitlistedCell.toggleClass('hidden', !isWaitlisted);
				if(isWaitlisted) {
					waitlistedCell.find('.message-waitlisted').html(waitlistedMessage);
				}

				input.closest('[data-flight]').find('.flights__info--price').removeClass('active');
				if (relatedRdo.closest('.flights__info--price').length) {
					relatedRdo.closest('.flights__info--price').addClass('active');
				}
				else {
					input.closest('.flights__info--price').addClass('active');
				}

				if(typeof(afterChangeCallback) === 'function') {
					afterChangeCallback();
				}
			});
		};

		var checkSubmitAble = function() {
			if(flightTables.length === (flightTables.find('input:radio:checked').length / 2)) {
				formFlightSearch.find('#btn-next').prop('disabled', false).removeClass('disabled');
			}
			else {
				formFlightSearch.find('#btn-next').prop('disabled', true).addClass('disabled');
			}
		};

		flightTables.each(function() {
			applyToTable($(this), checkSubmitAble);
		});
	};

	selectFlight();

	var mobileToggleFlightSelection = function() {
		flightTables
		.off('click.toggle-mobile-flights')
		.on('click.toggle-mobile-flights', '.flights__info a.link-5', function() {
			$(this).closest('td').find('.flights__info--mb').toggleClass('visible-mb');
		});
	};

	mobileToggleFlightSelection();

	var viewMoreTableFlight = function() {
		var applyToTable = function(btn, table) {
			var rows = table.find('.flights__table > tbody > tr');
			if(!rows.filter('.hidden').length) {
				btn.hide(0);
			}

			btn
			.off('click.show-more')
			.on('click.show-more', function() {
				rows = table.find('.flights__table > tbody > tr');
				rows.filter('.hidden:lt(6)').removeClass('hidden');
				if(!rows.filter('.hidden').length) {
					$(this).hide(0);
				}
			});

			if(table.find('.flights__table > tbody > tr.hidden').length === 0) {
				btn.addClass('hidden');
			}
		};

		flightTables.each(function() {
			var t = $(this).data('flight');
			var button = buttonShowMore.filter('[data-see-more="' + t + '"]');
			var table = flightTables.filter('[data-flight="' + t + '"]');
			applyToTable(button, table);
		});
	};

	viewMoreTableFlight();

	var getFlightInfo = function() {
		flightTables
		.off('click.showInfo')
		.on('click.showInfo', '.flights--detail span', function() {
			var self = $(this);
			if(self.next('.details').is(':not(:visible)')) {
				self.children('em').addClass('hidden');
				self.children('.loading').removeClass('hidden');

				$.ajax({
					url: config.url.orbFlightInfoJSON,
					type: config.ajaxMethod,
					dataType: 'json',
					data: {
						flightNumber: self.parent().data('flight-number'),
						carrierCode: self.parent().data('carrier-code'),
						date: self.parent().data('date'),
						origin: self.parent().data('origin')
					},
					success: function(res) {
						// self.children('em').toggleClass('ico-point-d ico-point-u');
						self.toggleClass('active');
						var flyingTime = '';
						for(var ft in res.flyingTimes) {
							flyingTime = res.flyingTimes[ft];
						}
						var html = '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
						html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime + '</p>';
						self.next('.details').html(html).hide().removeClass('hidden').stop().slideToggle(400);
					},
					error: function(jqXHR, textStatus) {
						// console.log(jqXHR);
						if(textStatus === 'timeout') {
							window.alert(L10n.flightSelect.timeoutGettingData);
						}
						else {
							window.alert(L10n.flightSelect.errorGettingData);
						}
					},
					complete: function() {
						self.children('em').removeClass('hidden');
						self.children('.loading').addClass('hidden');
					}
				});
			}
			else {
				// self.children('em').toggleClass('ico-point-d ico-point-u');
				self.toggleClass('active');
				self.next('.details').stop().slideToggle(400);
			}
		});
	};

	getFlightInfo();

	var popupSearchEdit = function() {
		var popupEditSearch = $('.popup--edit-search');
		var flyingFocus = $('#flying-focus');
		var triggerEditSearch = $('.flights__target a.search-link');
		if(!popupEditSearch.data('Popup')){
			popupEditSearch.Popup({
				overlayBGTemplate: config.template.overlay,
				modalShowClass: '',
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function(){
					flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				},
				beforeHide: function() {
					// popupEditSearch.find('[data-customselect]').customSelect('hide');
				},
				closeViaOverlay: false
			});
		}

		triggerEditSearch.off('click.triggerCheckAvailable').on('click.triggerCheckAvailable', function(e){
			e.preventDefault();
			popupEditSearch.Popup('show');
		});

		var radioTab = function(wp, r, t){
			wp.each(function(){
				var radios = wp.find(r);
				var tabs = wp.find(t);
				radios.each(function(i){
					var self = $(this);
					self.off('change.selectTabs').on('change.selectTabs', function(){
						tabs.removeClass('active').eq(i).addClass('active');
					});
				});
			});
		};
		radioTab(popupEditSearch, '[data-search-flights]', '.widget-edit-search');
	};

	popupSearchEdit();

	var getWailistedFlight = function() {
		$('[data-waitlisted-flight]').each(function() {
			var self = $(this);
			$.ajax({
				url: config.url.orbWaitlistedFlightJSON,
				type: config.ajaxMethod,
				data: { },
				dataType: 'json',
				success: function() {
				},
				error: function() {
					self.children('.preferred-flight').children('h3').text(L10n.orbWaitlistedFlight.failMessage);
					self.children('.preferred-flight').children('.content').empty();
				},
				complete: function() {
					self.children('.preferred-flight').removeClass('hidden');
					self.children('.loading').addClass('hidden');
				}
			});
		});
	};

	getWailistedFlight();

	var dataSort = function() {
		var filter = $('[data-filter]');
		filter.find('select').off('change.sort').on('change.sort', function() {
			var self = $(this);
			flightTables.each(function() {
				var table = $(this).find('.flights__table');
				var rows = table.find('> tbody > tr');
				var visibleRowsNumber = rows.filter(':visible').length;
				var sortBy = self.val();
				switch(sortBy) {
					case 'recommended':
						rows.sort(function(a, b) {
							if($(a).data('recommended') > $(b).data('recommended')) {
								return 1;
							}
							if($(a).data('recommended') < $(b).data('recommended')) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'price-asc':
						rows.sort(function(a, b) {
							var priceA = window.accounting.unformat($(a).data('price'));
							var priceB = window.accounting.unformat($(b).data('price'));
							if(priceA > priceB) {
								return 1;
							}
							else if (priceA < priceB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'travel-asc':
						rows.sort(function(a, b) {
							if($(a).data('travel-time') > $(b).data('travel-time')) {
								return 1;
							}
							else if($(a).data('travel-time') < $(b).data('travel-time')) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'departure-asc':
						rows.sort(function(a, b) {
							var departA = new Date($(a).data('departure-time'));
							var departB = new Date($(b).data('departure-time'));
							if(departA > departB) {
								return 1;
							}
							else if(departA < departB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'departure-desc':
						rows.sort(function(a, b) {
							var departA = new Date($(a).data('departure-time'));
							var departB = new Date($(b).data('departure-time'));
							if(departA < departB) {
								return 1;
							}
							else if(departA > departB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'arrival-asc':
						rows.sort(function(a, b) {
							var arrivalA = new Date($(a).data('arrival-time'));
							var arrivalB = new Date($(b).data('arrival-time'));
							if(arrivalA > arrivalB) {
								return 1;
							}
							else if(arrivalA < arrivalB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					case 'arrival-desc':
						rows.sort(function(a, b) {
							var arrivalA = new Date($(a).data('arrival-time'));
							var arrivalB = new Date($(b).data('arrival-time'));
							if(arrivalA < arrivalB) {
								return 1;
							}
							else if(arrivalA > arrivalB) {
								return -1;
							}
							else {
								return 0;
							}
						});
						break;
					default:
						break;
				}
				rows.removeClass('hidden even-item').each(function(rowIdx, row) {
					if(rowIdx % 2 !== 0) {
						$(row).addClass('even-item');
					}
					if(rowIdx > visibleRowsNumber - 1) {
						$(row).addClass('hidden');
					}
				});
				rows.detach().appendTo(table.children('tbody'));
			});
		});
	};

	dataSort();

	var popupTopUpSubmit = function() {
		var popupTopup = $(formFlightSearch.find('#btn-next').data('popup'));
		if(!popupTopup.data('Popup')) {
			popupTopup.Popup({
				overlayBGTemplate: config.template.overlay,
				triggerCloseModal: '.popup__close, [data-close]',
				afterShow: function(){
					var flyingFocus = $('#flying-focus');
					if(flyingFocus.length){
						flyingFocus.remove();
					}
				},
				closeViaOverlay: true
			});
		}

		formFlightSearch
		.find('#btn-next')
		.off('click.topup')
		.on('click.topup', function() {
			popupTopup.Popup('show');
		});

		popupTopup.find('form').off('submit.topup').on('submit.topup', function (e) {
			e.preventDefault();
			formFlightSearch.submit();
		});

		// formFlightSearch.off('submit.topup').on('submit.topup', function(e) {
		// 	e.preventDefault();
		// 	popupTopup.Popup('show');
		// });
	};

	popupTopUpSubmit();

	var controlStopover = function() {
		if(!addStopoverLnk.length || !stopoverContent.length) {
			return;
		}

		addStopoverLnk.off('click.showStopover').on('click.showStopover', function() {
			var that = $(this);
			if(!that.hasClass('disabled')) {
				that.closest('[data-group-stopover]').find(stopoverContent).removeClass('hidden');
				that.addClass('disabled');
			}
		});

		stopoverContent.find('[data-close-stopover]').each(function() {
			var that = $(this);
			that.off('click.hideStopover').on('click.hideStopover', function() {
				that.closest(stopoverContent).addClass('hidden');
				that.closest('[data-group-stopover]').find(addStopoverLnk).removeClass('disabled');
			});
		});
	};

	controlStopover();
};
