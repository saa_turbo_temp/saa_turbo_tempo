/**
 * @name Rewards voucher
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.voucherRewards = function() {
  var global = SIA.global;
  var body = $('body');
  var yourVoucherSection = body.find('[data-your-voucher]');
  var voucherSelect = body.find('#voucher-select');
  var selectVoucherForm = body.find('#form-select-voucher');
  var status = {
    selectVoucher: false,
    selectFlightSegment: false
  };
  var flightSegmentElements = $('[data-flight-segment]');
  var selectTarget = voucherSelect.data('result-target');
  var ajaxUrl = voucherSelect.data('url');
  var voucherData = {};
  var selectedIndex = 0;
  var segment = [];
  var doublemiles,
      airport_upgrade,
      bookable_upgrade,
      voucherCount,
      voucherType,
      selectFlightIdx = 0,
      flag = false,
      voucherNotCheckbox = 0,
      totalSegment = 0,
      totalVoucher = 0,
      currOptFlight = [],
      errorMessageElement = body.find('[data-error]'),
      isFirst = true,
      flightDataDefault = [],
      flightData = [],
      flightSelected = {},
      initSegment;

  const DOUBLE_MILES = 'Double_Miles_Accrual_Voucher';
  const BOOKABLE_UPGRADE = 'Bookable_Upgrade_Voucher';
  const AIRPORT_UPGRADE = 'Airport_Upgrade_Voucher';


  var getFlightInfo = function(wrapper) {
    var flightSq =  $('.flights--detail').find('.flight-sq');
    flightSq.off('click.get-flight-info').on('click.get-flight-info', function() {
      var self = $(this);
      self.toggleClass('active');
      if(self.siblings('.details').is('.hidden')) {
        $.ajax({
          url: global.config.url.flightSearchFareFlightInfoJSON,
          data: {
            flightNumber: self.parent().data('flight-number'),
            carrierCode: self.parent().data('carrier-code'),
            date: self.parent().data('data-date'),
            origin: self.parent().data('origin'),
          },
          type: SIA.global.config.ajaxMethod,
          dataType: 'json',
          beforeSend: function() {
            self.children('em').addClass('hidden');
            self.children('.loading').removeClass('hidden');
          },
          success: function(data) {
            var flyingTime = '';
            for(var ft in data.flyingTimes){
              flyingTime = data.flyingTimes[ft];
            }

            var html = '';
            html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + data.aircraftType + '</p>';
            html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + flyingTime + '</p>';

            self.siblings('.details').html(html).hide().removeClass('hidden').slideToggle(400);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            window.alert(errorThrown);
          },
          complete: function() {
            self.children('em').removeClass('hidden');
            self.children('.loading').addClass('hidden');
          }
        });
      }
      else {
        self.siblings('.details').slideToggle(400, function() {
          $(this).addClass('hidden');
        });
        // self.children('em').toggleClass('ico-point-u ico-point-d');
      }
    });
  };

  var setNextStep = function(status) {
    if (status) {
      selectVoucherForm.find('[data-btn-next]').removeClass('disabled').removeAttr('disabled');
    } else {
      selectVoucherForm.find('[data-btn-next]').addClass('disabled').attr('disabled', 'disabled');
    }
  };

  var initVoucherSection = function() {
    $.get(global.config.url.aboutVoucherSummary, function (data) {
      var template = window._.template(data, {
        data: {}
      });
      var templateEl = $(template);
      yourVoucherSection.html(templateEl);
    });
  };

  var ajaxCall = function(url, data, onSuccess) {
    $.ajax({
      url: url,
      method: 'GET',
      data: data,
      beforeSend : function() {
        SIA.preloader.show();
      },
      success: function(data) {
        if (onSuccess && typeof onSuccess === 'function') {
          onSuccess(data);
        }
      },
      error: function(err) {
        console.log(err);
      },
      complete: function(){
        SIA.preloader.hide();
      }
    });
  };

  var onErrorHandler = function(error) {
    if (!error) {
      errorMessageElement.addClass('hidden');
    } else {
      errorMessageElement.find('alert__message').text(error.errorMessage);
      errorMessageElement.removeClass('hidden');
    }
  };

  var checkAvailableSegment = function(ajaxUrl, selectedData, callback) {
    if (!selectedData || !selectedData.itinerary) {
      return false;
    }
    var voucherAvailable = false;
    var flight = selectedData.itinerary.flight;
    var departureTime = selectedData.itinerary.listSegment && selectedData.itinerary.listSegment[0].fromCity && selectedData.itinerary.listSegment[0].fromCity.departureTime;
    var departuteDate = selectedData.itinerary.listSegment && selectedData.itinerary.listSegment[0].fromCity && selectedData.itinerary.listSegment[0].fromCity.departuteDate;
    var checkVoucherAvailable = {
      fromCity: {
        segmentId: selectedIndex,
        flight : flight,
        departureTime: departureTime,
        departureDate: departuteDate
      }
    };
    ajaxCall(ajaxUrl, checkVoucherAvailable, function(data) {
      if (data.selectedSegmentListJson && data.selectedSegmentListJson.length &&
          data.selectedSegmentListJson[0].selectFlightSegment &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment.length &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment[0] &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment[0].itinerary &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment[0].itinerary.upgradeTo &&
          data.selectedSegmentListJson[0].selectFlightSegment.segment[0].itinerary.upgradeTo.upgradeCabinClass !== '') {
        voucherAvailable = true;
        onErrorHandler(false);
        // remove error message
      } else {
        voucherAvailable = false;
        var error = data.selectedSegmentListJson[0].selectFlightSegment.segment[0].itinerary.upgradeTo;
        onErrorHandler(error);
        // Show error message
      }
      if (callback) {
        callback(voucherAvailable);
      }
    });
  };
  var getVoucherCount = function(voucherType) {
    var count = 0;
    if(voucherType === DOUBLE_MILES) {
      count = doublemiles;
    } else if(voucherType === AIRPORT_UPGRADE) {
      count = airport_upgrade;
    } else if(voucherType === BOOKABLE_UPGRADE){
      count = bookable_upgrade;
    }
    return count;
  };
  var onSelectFlightSegment = function(that, value, cb) {
    var selfBlock = $(that).closest('.block__flight--segment');
    var ajaxUrl = $(that).data('url');
    var selectedItem = {};
    var tmpData;

    tmpData = flightData.filter(function(item, index) {
      if(item.segmentId === value + "") {
        selectedIndex = index;
        return true;
      }
    });
    flightSelected[value] = true;
    refreshFlightSegmentSelect();
    $(that).data('selectelidx') ? selectFlightIdx = $(that).data('selectelidx') : selectFlightIdx = selectedIndex;
    selectedItem = tmpData.length && tmpData[0];
    if(voucherType === DOUBLE_MILES) {
      $.get(global.config.url.voucherFlightSegmentTemplate, function (data) {
        var template = window._.template(data, {
          data: selectedItem,
          index: selectedIndex,
          voucherType: voucherType
        });
        var templateEl = $(template);

        var totalChecked = 0;

        if(voucherType === BOOKABLE_UPGRADE) {
          var listBlock = $('.block__flight--segment').not(selfBlock);

          listBlock.each(function(){
            var listCheckboxBook = $(this).find('input[type="checkbox"]');
            if(listCheckboxBook.not(':checked').length === 0 && listCheckboxBook.not('[data-checkedvoucher="true"]').length === listCheckboxBook.length) {
              totalChecked = totalChecked + 1;
            }
          })
        } else {
          $('.block__flight--segment').not(selfBlock).find('input[type="checkbox"]').each(function(){
            if($(this).is(':checked') && !$(this).is('[data-checkedvoucher="true"]')) {
              totalChecked = totalChecked + 1;
            }
          });
        }

        voucherNotCheckbox = totalVoucher - totalChecked ;


        var flightSegmentEl = selfBlock.find('.flight-segment-detail');
        flightSegmentEl.html(templateEl);
        if (cb) {
          cb(flightSegmentEl, selectFlightIdx, true);
        }
      });
    } else {
      checkAvailableSegment(ajaxUrl, selectedItem, function(isAvailable) {
        $.get(global.config.url.voucherFlightSegmentTemplate, function (data) {
          var template = window._.template(data, {
            data: isAvailable ? selectedItem : [],
            index: selectedIndex,
            voucherType: voucherType
          });
          var templateEl = $(template);

          var totalChecked = 0;

          if(voucherType === BOOKABLE_UPGRADE) {
            var listBlock = $('.block__flight--segment').not(selfBlock);

            listBlock.each(function(){
              var listCheckboxBook = $(this).find('input[type="checkbox"]');
              if(listCheckboxBook.not(':checked').length === 0 && listCheckboxBook.not('[data-checkedvoucher="true"]').length === listCheckboxBook.length) {
                totalChecked = totalChecked + 1;
              }
            })
          } else {
            $('.block__flight--segment').not(selfBlock).find('input[type="checkbox"]').each(function(){
              if($(this).is(':checked') && !$(this).is('[data-checkedvoucher="true"]')) {
                totalChecked = totalChecked + 1;
              }
            });
          }

          voucherNotCheckbox = totalVoucher - totalChecked ;


          var flightSegmentEl = selfBlock.find('.flight-segment-detail');
          flightSegmentEl.html(templateEl);
          if(voucherType === BOOKABLE_UPGRADE) {
            $('.wrap-bg').find('.upgrade-to-premium-economy').addClass('hidden');
          }else{
            $('.wrap-bg').find('.upgrade-to-premium-economy').removeClass('hidden');
          }
          if (cb) {
            cb(flightSegmentEl, selectFlightIdx, isAvailable);
          }
        });
      });
    }
  };

  var updateRemovedFlightToData = function(dataIdToCheck) {
    if (flightData.length && flightDataDefault.length) {
      var hasData = false;
      var dataToKeep = null;
      for(var i = 0, len = flightDataDefault.length; i < len; i++) {
        if (flightDataDefault[i].segmentId === dataIdToCheck) {
          dataToKeep = flightDataDefault[i];
        }
      }
      for(var i2 = 0, len2 = flightData.length; i2 < len2; i2++) {
        if (flightData[i2].segmentId === dataIdToCheck) {
          hasData = true;
        }
      }
      if (hasData === false && dataToKeep) {
        flightData.push(dataToKeep);
      }
      if (flightSelected[dataIdToCheck]) {
        flightSelected[dataIdToCheck] = false;
      }
      return flightData;
    }
  };
  var refreshFlightSegmentSelect = function(target) {
    $.get(global.config.url.voucherFlightSegmentTypeItems, function (tpldata) {
      if($('body').hasClass('voucher-fight-failue')){
        checkAvailUrl = global.config.url.checkVoucherAvailUrlFailue;
      }else{
        checkAvailUrl = global.config.url.checkVoucherAvailUrl;
      }
      $('[data-customselect]').each(function() {
        var flightSelectBox = $(this).find('select[name="select-flight-segment"]');
        if (!flightSelectBox.length) {
          return;
        }
        var flightSelectValue = flightSelectBox.val();
        var tmpFlight = flightData.filter(function(data) {
          return data && data.segmentId === ""
            || data.segmentId === flightSelectValue
            || flightSelected[data.segmentId] !== true;
        });
        var template = window._.template(tpldata, {
          data: tmpFlight,
          value: flightSelectValue
        });
        var templateEl = $(template);
        if (target) {
          target.html(templateEl);
        }
        else {
          flightSelectBox.html(templateEl);
        }
        $('[data-customselect]').defaultSelect('refresh');
      });
    });
  };


  var showFlightSegmentSelect = function(voucherType, voucherList, wrapperId) {
    if (!flightData || voucherType === false) {
      flightSegmentElements.empty();
      setNextStep(false);
      return;
    }

    var tmpFlight = flightData.filter(function(data) {
      return data && data.segmentId === ""
        || flightSelected[data.segmentId] !== true;
    });

    $.get(global.config.url.voucherFlightSegmentType, function (data) {
      if($('body').hasClass('voucher-fight-failue')){
        checkAvailUrl = global.config.url.checkVoucherAvailUrlFailue;
      }else{
        checkAvailUrl = global.config.url.checkVoucherAvailUrl;
      }
      var template = window._.template(data, {
        data: tmpFlight,
        checkAvailUrl: checkAvailUrl,
        id: wrapperId
      });
      var templateEl = $(template);
      var flightSegmentSelectPlugin;
      if(flag === true) {
        templateEl.find('option').each(function(){
          for(var i = 0; i < currOptFlight.length; i++) {
            if(parseInt($(this).val()) === parseInt(currOptFlight[i].value)) {
              $(this).remove();
            }
          }
        });
      }
      flightSegmentElements.append(templateEl);
      if (wrapperId) {
        flightSegmentSelectPlugin = flightSegmentElements.find('#' + wrapperId + ' [data-customselect]').defaultSelect('refresh');
      } else {
        flightSegmentSelectPlugin = flightSegmentElements.find('[data-customselect]').defaultSelect('refresh');
      }
      // Re init select flight segment
      flightSegmentSelectPlugin.each(function(){
        var lastValue = null;
         $(this).off('click.beforeSelect').on('click.beforeSelect', function(){
          lastValue = $(this).find('select').val();
        });
        $(this).data('defaultSelect').options.afterSelect = function(selectedElement) {
          var value = selectedElement.val();
          if (value && value !== '') {

            if(flag === true) {
              $('.voucher-flight-segment').find('[data-customselect]').not($(this)).each(function(){
                var optionEl = $(this).find('option');
                optionEl.each(function(){
                  if(parseInt($(this).val()) === parseInt(value)) {
                    $(this).remove();
                  }
                });
                $(this).defaultSelect('refresh');
              })
            }
            if (lastValue !== null) {
              updateRemovedFlightToData(lastValue);
            }

            onSelectFlightSegment(this, value, function(flightSegmentEl,selectFlightIdx, isAvailable) {
              // Init get the flight info after show flightSegment
              var checkOptionSelected = false;
              var selfBlock = $(flightSegmentEl).closest('.block__flight--segment');
              var btnRemove = $('input[name="remove-flight"]');
              // Init get the flight info after show flightSegment
              getFlightInfo($('#' + wrapperId));
              isAvailable ? setNextStep(true) : setNextStep(false);
              // init check logic passenger
              // initVoucherCount(voucherType, voucherList);
              for(var i = 0; i < currOptFlight.length; i++) {
                if(currOptFlight[i].index === selectFlightIdx) {
                  checkOptionSelected = true;
                  currOptFlight[i].value = value;
                  break;
                }
              }
              if(!checkOptionSelected && flag) {
                currOptFlight.push({index : selectFlightIdx, value: parseInt(value)});
                selfBlock.find('[data-customselect]').attr('data-selectElIdx', selectFlightIdx);
              }

              voucherCount = voucherNotCheckbox;
              if(voucherType === DOUBLE_MILES) {
                doublemiles = voucherCount;
              } else if(voucherType === AIRPORT_UPGRADE) {
                airport_upgrade = voucherCount;
              } else if(voucherType === BOOKABLE_UPGRADE){
                bookable_upgrade = voucherCount;
              }

              if(selfBlock.is(':last-child') && voucherCount > 0 && totalSegment > 0
                && voucherType !== BOOKABLE_UPGRADE) {
                var selfBtnAdd = selfBlock.find('.add-more-cta');
                selfBtnAdd.removeClass('hidden');
              }
              if(isAvailable === false) {
                selfBlock.find('.form-group').addClass('error');
                selfBlock.find('.add-more-cta').addClass('hidden');
              } else {
                if(!isFirst) {
                  $('.block__flight--segment').each(function(){
                    if($(this).find('select').val() && $(this).find('select').val() !== '') {
                      $(this).find('input[name="remove-flight"]').removeClass('hidden');
                    }
                  });
                }
              }

              initLogicCheckbox(flightSegmentEl.find('[data-vouchertype]'));

            });
          } else {
            var selectElIdx = $(this).data('selectelidx')
            if(selectElIdx) {
              for(var i = 0; i < currOptFlight.length; i++) {
                if(currOptFlight[i].index === selectElIdx) {
                  checkOptionSelected = true;
                  currOptFlight[i].value = value;
                  break;
                }
              }
            }
          }
        };
      });
    });
  };

  var renderVoucherItems = function(voucherData) {
    $.get(global.config.url.aboutVoucher, function (data) {
      var template = window._.template(data, {
        data: voucherData
      });
      var templateEl = $(template);
      $(selectTarget).removeClass('hidden').append(templateEl);
      if(voucherData.displayName === AIRPORT_UPGRADE){
        $('.about-voucher').find('.usage-airport-upgrade').removeClass('hidden');
      }else if(voucherData.displayName === DOUBLE_MILES){
        $('.about-voucher').find('.usage-double-miles-accrual').removeClass('hidden');

      }else if(voucherData.displayName === BOOKABLE_UPGRADE){
        $('.about-voucher').find('.usage-bookable-upgrade').removeClass('hidden');
      }
    });
  };
  var getVoucherData = function(value, ajaxUrl, callback) {
    var voucherData = {};
    var voucherName = value;
    if(typeof SIA.voucherStored === 'function' && SIA.voucherStored().getLocalStorage(voucherName) !== null) {
        voucherData = SIA.voucherStored().getLocalStorage(voucherName);
        if (callback) {
          callback(voucherData);
        }
      } else if (ajaxUrl) {
        ajaxCall(ajaxUrl, {type: value}, function(data) {
          voucherData = data;
          if (callback) {
            callback(voucherData);
          }
          // store cookie
          if(typeof SIA.voucherStored === 'function'){
            SIA.voucherStored().setLocalStorage(voucherName, voucherData);
          }
        });
      }
  };

  var addKeyToListObj = function(obj, key, val) {
    $.each(obj, function(index) {
      if (key === 'index' && !val) {
        obj[index][key] = index;
      } else {
        obj[index][key] = val;
      }
    });
    return obj;
  };

  var initSelectVoucher = function() {
    var selectTarget = voucherSelect.data('result-target');
    var ajaxUrl = voucherSelect.data('url');
    var segment = [];

    var voucherSelectPlugin = voucherSelect.closest('[data-customselect] > select').data('defaultSelect');
    // var selectedElement = voucherSelect.closest('[data-customselect] > select');
    if (voucherSelectPlugin) {
      voucherSelectPlugin.options.afterSelect = function(selectedElement) {
        showFlightSegmentSelect(false);
        $(selectTarget).empty().addClass('hidden');
        voucherCount = 0;
        var value = selectedElement.val();
        if (value && value !== '') {
          status.selectVoucher = true;
          flag = false;
          currOptFlight = [];

          if($('body').hasClass('voucher-page-1')){
            if(value === 'Double_Miles_Accrual_Voucher' ) {
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample2Test.json';
            } else if(value === AIRPORT_UPGRADE) {
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample1Test.json';
            } else if(value === BOOKABLE_UPGRADE){
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample3Test.json';
            }

          }else{
            if(value === 'Double_Miles_Accrual_Voucher' ) {
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample2.json';
            } else if(value === AIRPORT_UPGRADE) {
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample1.json';
            } else if(value === BOOKABLE_UPGRADE){
              ajaxUrl = 'ajax/voucher-SAAVoucherSelectionDropDownJsonSample3.json';
            }
          }

          getVoucherData(value, ajaxUrl, function(voucherData) {
            renderVoucherItems(voucherData);
            if (voucherData.SAARewardInfo && voucherData.SAARewardInfo.voucherList) {
              voucherList = voucherData.SAARewardInfo.voucherList;
              initVoucherCount(value, voucherList);
            }
            if (voucherData.eligibleSegmentList &&
                voucherData.eligibleSegmentList.length &&
                voucherData.eligibleSegmentList[0].selectFlightSegment &&
                voucherData.eligibleSegmentList[0].selectFlightSegment.segment) {
                  segment = voucherData.eligibleSegmentList[0].selectFlightSegment.segment.slice(0);
                }
            voucherCount  = getVoucherCount(value);
            voucherType = value;
            flightSelected = {};
            flightData = addKeyToListObj(segment.slice(0), 'index');
            flightDataDefault = segment.slice(0);
            totalSegment = segment.length - 1;
            initSegment = segment.length - 1;
            isFirst = true;
            //show flight segment selector
            errorMessageElement.addClass('hidden');
            flag = true;
            showFlightSegmentSelect(voucherType, voucherList, null, 'select-flight-segment');
            totalSegment = totalSegment - 1;
          });
        }
      }; // END afterSelect function
    }
  };

  var initVoucherCount = function(value, voucherList) {
    if(value === 'Double_Miles_Accrual_Voucher') {
      doublemiles = voucherList.length;
      totalVoucher = voucherList.length;
      voucherCount = doublemiles;
    } else if(value === AIRPORT_UPGRADE) {
      airport_upgrade = voucherList.length;
      totalVoucher = voucherList.length;
      voucherCount = airport_upgrade;
    } else if(value === BOOKABLE_UPGRADE){
      bookable_upgrade = voucherList.length;
      totalVoucher = voucherList.length;
      voucherCount = bookable_upgrade;
    }
  }

  var tooltipClick = function() {
      $(document).on('click', '.custom-checkbox', function(){
        var tooltipDisabled = selectVoucherForm.find('.custom-checkbox');
        var inputDisabled = tooltipDisabled.find('input');
        if($(this).find('input').is('[data-tooltipvoucher]')){
          selectVoucherForm.find('.tooltip--use-voucher').addClass('hidden');
          $(this).find('.tooltip--use-voucher').removeClass('hidden');
        }

      });
      $(document).on('click', '.tooltip__close', function(e){
          e.preventDefault();
          $(this).parent().addClass('hidden');
          return false;
      });
      $('html').on('click.tooltip--use-voucher', function() {
        selectVoucherForm.find('.tooltip--use-voucher').addClass('hidden');
      });
  };

  var handleLogicVoucher = function() {
    var formSelectVoucher = $('#form-select-voucher');
    selectVoucherForm
    .off('click.addFlightSegment', 'input[name="more-flight"]')
    .on('click.addFlightSegment', 'input[name="more-flight"]', function(){
      if(voucherCount > 0 && totalSegment > 0) {
        flag = true;
        showFlightSegmentSelect(voucherType, null, 'more-flight-segment-' + voucherCount);
        totalSegment = totalSegment - 1;
        // Add new Flight Segment
      }
      isFirst = false;
      $(this).closest('.add-more-cta').addClass('hidden');
    });

    selectVoucherForm
    .off('click.removeFlightSegment', 'input[name="remove-flight"]')
    .on('click.removeFlightSegment', 'input[name="remove-flight"]', function(){

      var selfFlightSegment = $(this).closest('.block__flight--segment'),
        flightSegmentValue = selfFlightSegment.find('select[name="select-flight-segment"]').val(),
        listCheckbox = selfFlightSegment.find('input[type="checkbox"]');

      if (flightSegmentValue === '') {
        return;
      }

      var value = selfFlightSegment.find('option:selected').val() === '' ? '' : parseInt(selfFlightSegment.find('option:selected').val());

      for (var i = 0; i < currOptFlight.length; i++)
       if (currOptFlight[i].value === value) {
          currOptFlight.splice(i,1);
          break;
       }

      listCheckbox.each(function(){
        if($(this).is(':checked') && !$(this).is('[data-checkedvoucher="true"]')) {
          airport_upgrade = airport_upgrade + 1;
          voucherCount = airport_upgrade;
        }
      });

      if(voucherCount > 0 && totalSegment > 0) {
        formSelectVoucher.find('input[type="checkbox"]').each(function(){
          if($(this).data('disabledvoucher')) {
            $(this).removeAttr('disabled').removeAttr('data-disabledvoucher');
            $(this).closest('.custom-checkbox').removeClass('disbled');
          }
        });
      }

      selfFlightSegment.remove();
      updateRemovedFlightToData(flightSegmentValue);
      refreshFlightSegmentSelect();

      totalSegment = totalSegment + 1;

      if(voucherCount >= 0 && totalSegment > 0) {
        $('[data-flight-segment]').find('.add-more-cta').last().removeClass('hidden');
      }

      if(totalSegment === initSegment - 1) {
        $('[data-flight-segment]').find('input[name="remove-flight"]').first().addClass('hidden');
        isFirst = true;
      }

    });

  };


  var initLogicCheckbox = function(listCheckbox) {

    console.log('orginal voucher:' + voucherCount);


    var checkboxItem = listCheckbox.find('input[type="checkbox"]');

    if(listCheckbox.data('vouchertype') === AIRPORT_UPGRADE) {
      checkboxItem.each(function(){
        if($(this).is(':checked') && !$(this).is(':disabled') && !$(this).is('[data-checkedvoucher="true"]')) {
          airport_upgrade = airport_upgrade >= 0 ? airport_upgrade - 1 : airport_upgrade;
          voucherCount = airport_upgrade;
        }
      });
    } else if(listCheckbox.data('vouchertype') === BOOKABLE_UPGRADE) {
      if(checkboxItem.not(':checked').length === 0 && checkboxItem.not('[data-checkedvoucher="true"]').length === checkboxItem.length) {
        bookable_upgrade = bookable_upgrade >= 0 ? bookable_upgrade - 1 : bookable_upgrade;
        voucherCount = bookable_upgrade;
      }
    } else if(listCheckbox.data('vouchertype') === 'Double_Miles_Accrual_Voucher') {
      checkboxItem.each(function(){
        if($(this).is('[data-principlekf="true"]') && !$(this).is('[data-checkedvoucher="true"]')) {
          doublemiles = doublemiles >= 0 ? doublemiles - 1 : doublemiles;
          voucherCount = doublemiles;
        }
      });
    }

    console.log('After apply checkbox logic, voucher: ' + voucherCount);

  }

  var handleLogicCheckbox = function () {
    var formSelectVoucher = $('#form-select-voucher');
      formSelectVoucher
      .off('change.logicCheckbox', 'input[type="checkbox"]')
      .on('change.logicCheckbox', 'input[type="checkbox"]', function(){
        var _self = $(this),
            voucherCheckbox = formSelectVoucher.find('[data-vouchertype]'),
            listCheckbox = voucherCheckbox.find('input[type="checkbox"]');
        if(voucherCheckbox.data('vouchertype') === AIRPORT_UPGRADE) {
          if(_self.is(':checked')) {
            airport_upgrade = airport_upgrade - 1;
            voucherCount = airport_upgrade;
          } else {
            airport_upgrade = airport_upgrade + 1;
            voucherCount = airport_upgrade;
          }
        }

        console.log('Airport_Upgrade_Voucher, after click on checkbox, voucher:' + voucherCount);


        if(voucherCount > 0) {
          listCheckbox.each(function(){
            if($(this).data('disabledvoucher')) {
              $(this).removeAttr('disabled').removeAttr('data-disabledvoucher');
              $(this).closest('.custom-checkbox').removeClass('disbled');
            }
          });
        } else {
          listCheckbox.not(_self).each(function(){
            if(!$(this).is(':disabled')) {
              $(this).attr({
                'disabled': true,
                'data-disabledvoucher': true
              });
              $(this).closest('.custom-checkbox').addClass('disbled');
            }
          });

          $('.add-more-cta').addClass('hidden');
        }
      });
  };

  if(localStorage && localStorage.length > 0) {
    localStorage.clear();
  }

  initVoucherSection();
  initSelectVoucher();
  tooltipClick();
  handleLogicVoucher();
  handleLogicCheckbox();
};
