/**
 * @name SIA
 * @description Define global staticContentHeritage functions
 * @version 1.0
 */
SIA.staticContentHeritage = function() {
	var global = SIA.global,
			win = global.vars.win;

	var initNavMenu = function() {
		var bodyEl = $('html,body'),
				// navBlockEl = $('[data-nav-block]'),
				listBlockEl = $('[data-list-block]'),
				navEl = $('[data-desktop-submenu]'),
				menuLink = navEl.find('a'),
				selectMenu = $('[data-device-submenu]').find('select'),
				navTop = listBlockEl.length ? listBlockEl.offset().top : 0,
				scrollTimer,
				risizeTimer,
				footer = $('footer'),
				navElHeight = navEl.height(),
				wW = win.width();

		var calcOffsetNav = function(currentTop, navTop) {
			if(navEl.length) {
				if(currentTop > navTop) {
					var leftNav = listBlockEl.width() + listBlockEl.offset().left;
					var top = Math.min(footer.offset().top - (currentTop + navElHeight + 80) , 0);
					navEl.css({
						'position' : 'fixed',
						'right' : 'auto',
						'left' : leftNav,
						'top': top
					});
				} else {
					navEl.removeAttr('style');
				}
			}
		};

		menuLink.off('click.scrollToBlock').on('click.scrollToBlock', function(e) {
			e.preventDefault();
			var self = $(this),
					idBlock = self.attr('href');

			menuLink.removeClass('active');
			self.addClass('active');
			if(/(#[a-z0-9][a-z0-9\-_]*)/ig.test(idBlock)) {
				selectMenu.val(idBlock.slice(1)).defaultSelect('refresh');
				bodyEl.animate({scrollTop: $(idBlock).offset().top}, 400);
			}
		});

		selectMenu.off('change.scrollToBlock').on('change.scrollToBlock', function() {
			var self = $(this),
					idBlock = self.val();
			menuLink.removeClass('active');
			navEl.find('a[href="#' + idBlock + '"]').addClass('active');
			self.siblings('.heritage-nav__selected').text(self.find('option:selected').text());
			bodyEl.animate({scrollTop: $('#' + idBlock).offset().top}, 400);
		});

		win.off('scroll.navMenu').on('scroll.navMenu', function() {
			navTop = listBlockEl.length ? listBlockEl.offset().top : 0;
			clearTimeout(scrollTimer);
			scrollTimer = setTimeout(function() {
				calcOffsetNav(win.scrollTop(), navTop);
			}, 5);
		}).trigger('scroll.navMenu');

		win.off('resize.navMenu').on('resize.navMenu', function() {
			clearTimeout(risizeTimer);
			if(wW !== win.width()){
				wW = win.width();
				risizeTimer = setTimeout(function() {
					win.trigger('scroll.navMenu');
					selectMenu.trigger('change.scrollToBlock');
				}, 100);
			}
		});

		listBlockEl.children().last().addClass('last');
	};

	var initModule = function() {
		initNavMenu();
	};

	initModule();
};
