/**
 * @name SIA
 * @description Define global stickySidebar functions
 * @version 1.0
 */
SIA.stickySidebar = function(){
	/*var dropdownsOnMobile = function(wrapper, ch, sl){
		var chs = wrapper.find(ch);
		var sls = wrapper.find(sl);
		sls.off('change.direct').on('change.direct', function(){
			window.location.href = chs.eq(sls.prop('selectedIndex')).attr('href');
		});
	};*/
	var bookingNav = $('[data-fixed].sidebar');
	if(bookingNav.length){
		// reset scroll Top
		SIA.global.vars.win.scrollTop(0);

		var wrapperBookingNav = bookingNav.parent();
		var maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;
		var offsetBookingNavInWrapper = parseFloat(bookingNav.css('top'));
		var bookingNavOffsetTop = wrapperBookingNav.offset().top;
		offsetBookingNavInWrapper = window.isNaN(offsetBookingNavInWrapper) ? 0 : offsetBookingNavInWrapper;
		SIA.global.vars.win.off('scroll.sticky mousewheel.sticky').on('scroll.sticky mousewheel.sticky', function(){
			maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;
			bookingNavOffsetTop = wrapperBookingNav.offset().top;
			if((bookingNavOffsetTop +offsetBookingNavInWrapper) <= SIA.global.vars.win.scrollTop()){
				bookingNav.css({
					top: Math.min(maxScroll, SIA.global.vars.win.scrollTop() - bookingNavOffsetTop)
				});
			}
			else{
				bookingNav.css('top', '');
			}
		});
		/*if(SIA.global.vars.win.width() <= SIA.global.config.tablet){
			// seat map and passenger detail
			dropdownsOnMobile(bookingNav, 'a', 'select');
		}*/
	}
};
