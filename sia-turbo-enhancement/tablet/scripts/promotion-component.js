/**
 * @name SIA
 * @description Define global promotionComponent functions
 * @version 1.0
 */
SIA.promotionComponent = function(){
	var componentEls = $('[data-promotion-component]');

	var initShuffle = function(resultWrapperEl) {
		var images = resultWrapperEl.find('img');
		var loadedImgNum = 0;

		var startShuffle = function(){
			resultWrapperEl.shuffle({
				speed: 0,
				isLeftSort: false,
				itemSelector: '.promotion-item--2:not(":hidden")',
				sizer: resultWrapperEl.find('.promotion-item--2:not(":hidden")').eq(0)
			});
		};

		images.each(function(){
			var that = $(this);
			var img = new Image();

			img.onload = function(){
				loadedImgNum ++;
				if (loadedImgNum === images.length) {
					startShuffle();
				}
			};

			img.onerror = function(){
				startShuffle();
			};

			img.src = that.attr('src');
		});
	};

	var searchItems = function(item, from, to) {
		return item
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!from || self.data('from') === from) &&
					(!to || self.data('to') === to);
			});
	};

	componentEls.each(function() {
		var componentEl = $(this);
		var resultWrapperEl = componentEl.find('.promotion-result');
		var item = resultWrapperEl.children();
		var from = componentEl.find('[data-filter-destination-from] select');
		var to = componentEl.find('[data-filter-destination-to] select');
		var seemoreEl = componentEl.find('[data-see-more]');
		var promotionForm = componentEl.find('.form-station');
		var loadingItem = componentEl.data('loading-view');
		var itemview = item.filter(':not(":hidden")').length;
		var resultItem = $();
		var haveShuffle = !!componentEl.data('have-shuffle');
		var appendedItem = $();
		var numberOfClick = 0;

		var upload = function(){
			resultItem = searchItems(item, from.val(), to.val());
			itemview = loadingItem;
			numberOfClick = 0;
			seemoreEl.text(L10n.kfSeemore.seeMore);
			if(itemview >= resultItem.length){
				seemoreEl.addClass('hidden');
			}
			else{
				seemoreEl.removeClass('hidden');
			}
			resultItem.filter(':lt('+ itemview  +')').removeClass('hidden');
		};

		upload();

		if (!!haveShuffle) {
			initShuffle(resultWrapperEl);
		}

		seemoreEl
			.off('click.seemore')
			.on('click.seemore', function(e) {
				e.preventDefault();
				var maxI = itemview + loadingItem;
				numberOfClick ++;
				if(numberOfClick === 2){
					seemoreEl.text(L10n.kfSeemore.seeAll);
				}
				else if(numberOfClick > 2){
					maxI = resultItem.length;
				}
				appendedItem = resultItem.filter(function(idx){
					return idx > (itemview - 1 ) && idx < maxI;
				});
				itemview = maxI;
				resultItem.filter(':lt('+ itemview  +')').removeClass('hidden');
				if (!!haveShuffle) {
					resultWrapperEl.shuffle('appended', appendedItem);
				}
				if(itemview >= resultItem.length){
					seemoreEl.addClass('hidden');
				}
			});

		promotionForm
			.off('submit.updateContent')
			.on('submit.updateContent', function(e) {
				e.preventDefault();
				upload();
				if (!!haveShuffle) {
					resultWrapperEl.shuffle('destroy');
					initShuffle(resultWrapperEl);
				}
			});
	});

	var componentCarousel = function(){
		var componentCarousel = $('.component-carousel');
		var sliders = $('.slick-slider', componentCarousel);

		var autoPlay = function(slider){
			slider.slick('slickPlay', true);
		};

		sliders.each(function(){
			var self = $(this);
			autoPlay(self);
		});
	};

	var componentBodyCopy = function(){
		var componentBodyCopy = $('.component-body-copy');

		var characterLimit = function(para){
			var maxLength = 905;
			var readMoreTpl = '<span>....</span><a href="#" class="read-more">Read More</a>';
			if(para.html().length > maxLength){
				var shortContent = para.html().substr(0,maxLength);
				var longContent	= para.html().substr(maxLength);
				para.html(shortContent + readMoreTpl +'<span style="display:none;">'+longContent+'</span>');
			}
			para.find('.read-more').off('click').on('click', function(e){
				var self = $(this);
				e.preventDefault();
				self.hide();
				self.prev().hide();
				self.next().fadeIn();
			});
		};

		componentBodyCopy.each(function(){
			var self = $(this);
			var selfCopy = $('.component-body-copy__text p', self);

			characterLimit(selfCopy);
		});

	};

	componentCarousel();
	componentBodyCopy();

};
