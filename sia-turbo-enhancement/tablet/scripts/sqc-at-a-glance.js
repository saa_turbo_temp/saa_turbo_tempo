/**
 * @name SIA
 * @description Define global sqcAtAGlance functions
 * @version 1.0
 */
SIA.sqcAtAGlance = function(){
	var global = SIA.global;
	var config = global.config;
	var win = global.vars.win;
	var dialsChart = $('.dials-chart__item');
	var duration = 600;
	var initChart = function(){
		var blockChart = $('.block--account-summary-chart');
		var chartContent = $('.chart-content', blockChart);
		if(!chartContent.length){
			return;
		}
		var currency = 'SGD';
		var categories = globalJson.sqcAtAGlanceChartData ? globalJson.sqcAtAGlanceChartData.categories : {
			m: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
			t: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
		};
		var data =  globalJson.sqcAtAGlanceChartData ? globalJson.sqcAtAGlanceChartData.data : [107.0, 160.9, 109.5, 140.5, 180.2, 160.0, 210.5, 260.5, 230.3, 180.3, 130.9, 190.6];

		var responsive = function(){
			var api = chartContent.highcharts();
			var timer = null;
			var update = false;
			win.off('resize.sqcChart').on('resize.sqcChart', function(){
				clearTimeout(timer);
				timer = setTimeout(function(){
					if(window.innerWidth < config.mobile){
						if(update){
							api.xAxis[0].update({
								categories: categories.m
							});
						}
						update = false;
					}
					else{
						if(!update){
							api.xAxis[0].update({
								categories: categories.t
							});
						}
						update = true;
					}
				},200);
			}).triggerHandler('resize.sqcChart');
		};

		chartContent.highcharts({
			chart: {
				backgroundColor: '#F7F7F7',
				events: {
					load: function(){
						responsive();
					}
				}
			},
			colors: ['#000000'],
			title: {
				text: ''
			},
			subtitle: {
				text: ''
			},
			exporting: {
				enabled: false
			},
			credits: {
				enabled: false
			},
			xAxis: {
				categories: window.innerWidth < config.mobile ? categories.m : categories.t,
				tickColor: '#F7F7F7',
				lineColor: '#F7F7F7',
				labels: {
					style: {
						color: '#999999',
						fontSize: '10px',
						'font-family': '"proxima-nova", "Open Sans", "Arial", "Helvetica", "sans-serif"'
					}
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 1,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 2,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 3,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 4,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 5,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 6,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 7,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 8,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 9,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 10,
					width: 1,
					color: '#FFFFFF'
				},{
					value: 11,
					width: 1,
					color: '#FFFFFF'
				}]
			},
			yAxis: {
				title: {
					text: ''
				},
				gridLineWidth: 0,
				labels: {
					enabled: false
				}
			},
			plotOptions: {
				series: {
					connectNulls: true
				}
			},
			tooltip: {
				shadow: false,
				borderWidth: 0,
				backgroundColor: '#00266B',
				formatter: function () {
					return currency + ' ' + this.y;
				},
				style: {
					color: '#FFFFFF',
					fontSize: '13px',
					'font-family': '"proxima-nova", "Open Sans", "Arial", "Helvetica", "sans-serif"'
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			series: [{
				showInLegend: false,
				lineWidth: 0.5,
				shadow: false,
				marker: {
					fillColor: '#F7F7F7',
					lineWidth: 0.5,
					radius: 3,
					lineColor: null,
					states: {
						hover: {
							fillColor: '#00266B',
							radius: 5,
							lineWidthPlus: 0,
							radiusPlus: 0
						}
					}
				},
				states: {
					hover: {
						halo: {
							size: 2,
							opacity: 1
						}
					}
				},
				data: data
			}]
		});
	};

	var renderYourBookings = function(){
		var json = null;
		var loadTemplate = function(html){
			var wrapper = $('[data-accordion-wrapper-content]');
			wrapper.empty();
			var template = window._.template(html, {
				data: json
			});
			wrapper.html(template);
			$.get(config.url.sqc.atGlanceDetailTemplate, function(html) {
				wrapper.find('[data-flight]').each(function() {
					renderBtn($(this), html);
				});
				SIA.accordion.initAccordion();
			});
		};

		var renderBtn = function(block, temp){
			var url = block.data('url');
			$.ajax({
				url: url,
				type: config.ajaxMethod,
				dataType: 'json',
				data: {},
				success: function(res) {
					var template = window._.template(temp, {
						data: res
					});
					block.find('.loading').addClass('hidden');
					if(res.btnLabel) {
						block.find('.booking-form').removeClass('hidden').find('.btn-1').text(res.btnLabel);
					}
					block.find('.ico-point-d').removeClass('hidden');
					block.find('.booking-info-group').html(template);
					getFlightInto(block);
					showListPassengers(block);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if(jqXHR && textStatus !== 'abort') {
						window.alert(errorThrown);
					}
				}
			});
		};

		function getFlightInto(block) {
			block.find('.flights--detail > span')
			.off('click.getFlightInfo')
			.on('click.getFlightInfo', function() {
				var self = $(this);
				var details = self.siblings('.details');
				if(details.is('.hidden')) {
					$.ajax({
						url: config.url.orbFlightInfoJSON,
						type: config.ajaxMethod,
						dataType: 'json',
						data: {
							flightNumber: self.parent().data('flight-number'),
							carrierCode: self.parent().data('carrier-code'),
							date: self.parent().data('date'),
							origin: self.parent().data('origin')
						},
						success: function(res) {
							self.children('em').toggleClass('ico-point-d ico-point-u');
							details.toggleClass('hidden');
							var html = '';
							html += '<p>' + L10n.flightSelect.aircraftTypeLabel + ': ' + res.aircraftType + '</p>';
							for(var ft in res.flyingTimes) {
								html += '<p>' + L10n.flightSelect.flyingTimeLabel + ': ' + res.flyingTimes[ft] + '</p>';
							}
							details.html(html);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							// console.log(jqXHR);
							if(textStatus !== 'abort') {
								window.alert(errorThrown);
							}
						},
						beforeSend: function() {
							self.children('em').addClass('hidden');
							self.children('.loading').removeClass('hidden');
						},
						complete: function() {
							self.children('.loading').addClass('hidden');
							self.children('em').removeClass('hidden');
						}
					});
				}
				else {
					self.children('em').toggleClass('ico-point-d ico-point-u');
					details.toggleClass('hidden');
				}
			});
		}

		function showListPassengers(block) {
			var toggle = block.find('[data-toggle-passengers-list]');
			var more = $('.more', toggle);
			var less = $('.less', toggle);
			var listPassengers = toggle.siblings('[data-passengers]');
			var passengers = listPassengers.data('passengers').split(',');


			var moreLess = function(length){
				var lis = '';
				for(var i = 0; i < length; i++) {
					if(i < length - 1) {
						lis += '<li>' + passengers[i];
						lis += ',';
					}
					else{
						lis += '<li class="last">' + passengers[i];
					}
					lis += '</li>';
				}
				return lis;
			};

			more
			.off('click.toggle-list-passengers')
			.on('click.toggle-list-passengers', function(e) {
				e.preventDefault();
				listPassengers.html(moreLess(passengers.length));
				toggle.addClass('active');
			});
			less
			.off('click.toggle-list-passengers')
			.on('click.toggle-list-passengers', function(e) {
				e.preventDefault();
				listPassengers.html(moreLess(3));
				toggle.removeClass('active');
			});
		}


		var init = function(){
			$.ajax({
				url: config.url.sqc.atGlanceJSON,
				type: config.ajaxMethod,
				dataType: 'json',
				data: {},
				success: function(res) {
					json = res;
					$.get(config.url.sqc.atGlanceTemplate, function(html) {
						loadTemplate(html);
					});
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if(jqXHR && textStatus !== 'abort') {
						window.alert(errorThrown);
					}
				}
			});
		};
		init();
	};

	var popupQuickLink = function(){
		var customiseLinkForm = $('.popup--sqc-customise-link form');
		var customiseLinkChkbs = customiseLinkForm.find('input:checkbox');
		var totalCustome = customiseLinkForm.data('totalCustome');

		customiseLinkChkbs.off('change.customiseLinkChkbs').on('change.customiseLinkChkbs',function(){
			var unCheckedChkbxs = customiseLinkForm.find('input:checkbox:not(:checked)');
			if(customiseLinkForm.find('input:checkbox:checked').length >= totalCustome){
				unCheckedChkbxs.prop('disabled', true);
				unCheckedChkbxs.parent('.custom-checkbox').addClass('disabled');
			}
			else {
				unCheckedChkbxs.prop('disabled', false);
				unCheckedChkbxs.parent('.custom-checkbox').removeClass('disabled');
			}
		}).trigger('change.customiseLinkChkbs');
	};

	var highlightsSlide = function(){
		var highlight = $('#highlight-slider');
		highlight.find('.slides').slick({
			siaCustomisations: true,
			dots: true,
			draggable: true,
			infinite: true,
			arrows: false,
			speed: 500,
			slidesToShow: 3,
			slidesToScroll: 3,
			accessibility: false,
			responsive: [
				{
					breakpoint: 988,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			],
			slide: 'div',
			cssEase: 'linear'
		});
	};

	// Show all Dials
	var showAllDials = function(self){
		self.addClass('animated fadeIn');
		self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn').on('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn' , function(){
			setTimeout(function(){
				dialsChart = $('.dials-chart__item');
				dialsChart.each(function(){
					if(!$.data(this, 'chart')){
						dialsChart.chart({
							startVal: 0,
							endVal: 4000,
							increment: 180 / 100,
							incrementVal: 70,
							duration: duration
						}).chart('show');
					}
				});
				self.removeClass('animated fadeIn');
				self.off('animationend.fadeIn webkitAnimationEnd.fadeIn MSAnimationEnd.fadeIn oanimationend.fadeIn');
			},1000);
		});
	};

	var initModule = function(){
		initChart();
		renderYourBookings();
		popupQuickLink();
		highlightsSlide();
		showAllDials($('.block--your-status'));
	};

	initModule();
};
