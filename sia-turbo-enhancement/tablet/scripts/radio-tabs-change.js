
/**
 * @name SIA
 * @description Define global radioTabsChange functions
 * @version 1.0
 */
SIA.radioTabsChange = function(){
	var placeLevel = [];

	var apply = function(controlTabs, contentTabs) {
		controlTabs
			.off('change.switch-tab')
			.on('change.switch-tab', function() {
				var self = $(this);
				var idx = controlTabs.index(self);
				if(self.is('select')){
					idx = self.prop('selectedIndex');
				}
				// contentTabs.addClass('hidden').eq(idx).removeClass('hidden');
				contentTabs.removeClass('active').eq(idx).addClass('active');
			});
	};

	var detectLevel = function(controlTabs){
		controlTabs.each(function(){
			var level = $(this).data('change-control');
			if(placeLevel.indexOf(level) ===-1){
				placeLevel.push(level);
			}
		});
	};

	var filterTab = function(controlTabs, contentTabs){
		for(var i = 1; i <= placeLevel.length; i ++){
			var controlSampleLevel = controlTabs.filter('[data-change-control="'+ i +'"]');
			var contentSampleLevel = contentTabs.filter('[data-content-control="'+ i +'"]');
			apply(controlSampleLevel, contentSampleLevel);
		}
	};

	var initRadioTabsChange = function(){
		var radioTabsChange = $('[data-radio-tabs-change]');
		radioTabsChange.each(function(){
			var self = $(this);
			var controlTabs = self.find('[data-change-control]');
			var contentTabs = self.find('[data-content-control]');
			detectLevel(controlTabs);
			filterTab(controlTabs, contentTabs);
		});
	};

	var initModule = function(){
		initRadioTabsChange();
	};

	initModule();
};
