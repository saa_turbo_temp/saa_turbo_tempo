/**
 * @name SIA
 * @description Define global kf-personal-detail function
 * @version 1.0
 */
SIA.KFPartnerProgramme = function(){
	if(!$('.partner-programme-page').length){
		return;
	}
	var global = SIA.global;
	var config = global.config;
	var jsonUrl = 'ajax/kf-partner-programme.json';
	var jsonUrl2 = 'ajax/kf-partner-programme-2.json';
	var pendingDelay = 2000;
	var tableAddPartner =$('.table-add-partner');
	var linkBtn = tableAddPartner.find('[data-link]');
	var registerBtn = tableAddPartner.find('[data-register]');
	var registerBtnPending = tableAddPartner.find('[data-register-status="pending"]');
	var deleteBtn = tableAddPartner.find('[data-delete]');
	var boxTierMatch = $('.tier-match-steps');
	var formProfileLink = $('.form-profile-link');
	var formPartnerAdd = $('.form-partner-add');
	var profileLinkChkb = formProfileLink.find('input:checkbox');
	var triggerTermPopup = formProfileLink.find('[data-trigger-term-popup]');
	var modal = $('.popup--link');
	var term = $('.popup--tcs');
	var flyingFocus = null;
	var popupDelete = $('.popup--delete-confirmation');
	var submitDelete = $('#submit');
	var btnDeleted;

	popupDelete.Popup({
		overlayBGTemplate: config.template.overlay
	});

	if(!modal.data('Popup')){
		modal.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			}
		});
	}

	if(!term.data('Popup')){
		term.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close, [data-close]',
			beforeShow: function(){
				modal.Popup('hide');
			},
			afterShow: function(){
				flyingFocus = $('#flying-focus');
				if(flyingFocus.length){
					flyingFocus.remove();
				}
			},
			afterHide: function(){
				modal.Popup('show');
			}
		});
	}

	formPartnerAdd.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess,
		submitHandler: function(){
			modal.Popup('show');
			return false;
		}
	});

	var pendingAction = function(clickbtn){
		var parentTr = clickbtn.parents('tr');
		if(clickbtn.is('[data-register]')){
			parentTr.find('[data-register-status]').text('Pending');
			parentTr.find('[data-register],[data-delete]').addClass('disable');
		}else{
			parentTr.find('[data-status]').text('Pending');
			parentTr.find('[data-link],[data-delete]').addClass('disable');
		}

		clickbtn.find('.wrapper-text').addClass('hidden');
		clickbtn.find('.loading').removeClass('hidden');
	};
	var ajaxSuccessLink = function(linkBtn,data){
		setTimeout(function(){
			if(data.link.statusCode === '200'){
				var parentTr = linkBtn.parents('tr');
				var status = parentTr.find('[data-status]');
				parentTr.find('[data-link],[data-delete]').removeClass('disable');
				if(linkBtn.find('.wrapper-text').text() === 'Link'){
					linkBtn.find('.wrapper-text').text('UnLink');
					status.text('Linked');
				}else{
					linkBtn.find('.wrapper-text').text('Link');
					status.text('UnLinked');
				}
				linkBtn.find('.wrapper-text').removeClass('hidden');
				linkBtn.find('.loading').addClass('hidden');
			}
			var tt = $('.tooltip');
			if(tt.length){
				tt.find('a.tooltip__close').trigger('click.close');
			}
		},pendingDelay);
	};

	var ajaxSuccessRegister = function(registerBtn,data){
		setTimeout(function(){

			var parentTr = registerBtn.parents('tr');
			var status = parentTr.find('[data-register-status]');
			parentTr.find('[data-register],[data-delete]').removeClass('disable');

			if(registerBtn.find('.wrapper-text').text() === 'Register'){
				registerBtn.find('.wrapper-text').text('Registered');
			}else{
				registerBtn.find('.wrapper-text').text('Register');
				status.text('');
			}
			if(data.register.statusCode === '400'){
				status.text('Pending');
				status.attr('data-register-status', 'pending');
			}
			if(data.register.statusCode === '200'){
				status.text('Successful');
				status.attr('data-register-status', 'successful');
				boxTierMatch.hide();
			}
			registerBtn.find('.wrapper-text').removeClass('hidden');
			registerBtn.find('.loading').addClass('hidden');


			var tt = $('.tooltip');
			if(tt.length){
				tt.find('a.tooltip__close').trigger('click.close');
			}
		},pendingDelay);
	};

	var ajaxCheckPending = function(){
		var registerBtn = $('[data-register-status="pending"]');
		var parentTr = registerBtn.parents('tr');
		var status = parentTr.find('[data-register-status]');
		registerBtn.each(function(){
			$.ajax({
				url: jsonUrl2,
				async: false,
				type: global.config.ajaxMethod,
				dataType: 'json',
				success: function(data){
					if(data.register.statusCode === '200'){
						status.text('Successful');
						status.attr('data-register-status', 'successful');
						boxTierMatch.hide();
					}
				},
				error: function(){
				}
			});
		});
	};

	if(registerBtnPending.length){
		ajaxCheckPending();
	}

	var ajaxSuccessDelete = function(delBtn){
		setTimeout(function(){
			delBtn.parents('tr').find('[data-link],[data-delete]').removeClass('disable');
			delBtn.parents('tr').remove();
			var tt = $('.tooltip');
			if(tt.length){
				tt.find('a.tooltip__close').trigger('click.close');
			}
		},pendingDelay);
	};
	var getAjax = function(jsonUrl,pendingAct,ajaxSuccess,clBtn){
		$.ajax({
			url: jsonUrl,
			async: false,
			type: global.config.ajaxMethod,
			dataType: 'json',
			beforeSend: pendingAct,
			success: function(data){
				ajaxSuccess(clBtn,data);
			},
			error: function(){
			}
		});
	};

	var ajaxDelete = function(){
		if(btnDeleted){
			getAjax(jsonUrl, pendingAction(btnDeleted), ajaxSuccessDelete, btnDeleted);
		}
	};
	linkBtn.off('click.linkBtn').on('click.linkBtn',function(){
		if(!$(this).hasClass('disable')){
			getAjax(jsonUrl,pendingAction($(this)),ajaxSuccessLink,$(this));
		}
	});

	registerBtn.off('click.registerBtn').on('click.registerBtn', function(){
		if(!$(this).hasClass('disable')){
			getAjax(jsonUrl2, pendingAction($(this)), ajaxSuccessRegister, $(this));
		}
	});

	deleteBtn.off('click.linkBtn').on('click.linkBtn',function(){
		if(!$(this).hasClass('disable')){
			popupDelete.Popup('show');
			btnDeleted = $(this);
		}
	});

	submitDelete.off('click.submitDelete').on('click.submitDelete', function(){
		popupDelete.Popup('hide');
		ajaxDelete();
	});
	profileLinkChkb.off('change.profileLinkChkb').on('change.profileLinkChkb',function(){
		if($(this).prop('checked')){
			$(this).parents('form').find('input:submit').prop('disabled', false).removeClass('disabled');
		}
		else{
			$(this).parents('form').find('input:submit').prop('disabled', true).addClass('disabled');
		}
	}).trigger('change.profileLinkChkb');
	triggerTermPopup.off('click.triggerTermPopup').on('click.triggerTermPopup',function(){
		term.Popup('show');
	});
};
