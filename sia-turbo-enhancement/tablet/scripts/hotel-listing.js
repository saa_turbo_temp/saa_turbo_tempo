
/**
 * @name SIA
 * @description Define global hotelListing functions
 * @version 1.0
 */
SIA.hotelListing = function(){
	var hotelListing = $('[data-filter-hotel-listing]');
	var hotelResult = hotelListing.find('.hotel-result');

	var categorySL = hotelListing.find('[data-filter-category]');
	var categorySLDefault = categorySL.find('select');
	var categorySLValue = categorySLDefault.val();

	var locationSL = hotelListing.find('[data-filter-location]');
	var locationSLDefault = locationSL.find('select');
	var locationSLValue = locationSLDefault.val();

	var items = hotelListing.find('.where-destination__item');
	var startRate = hotelListing.find('[data-start-rate]');
	var startRateValue = startRate.data('start-rate');


	var starRating = function() {
		var rate = startRate.children();
		var chooseRate = function() {
			rate.removeClass('rated');
			rate.each(function(i) {
				var self = $(this);
				if (i < startRateValue) {
					self.addClass('rated');
				}
			});
		};

		rate.each(function(i) {
			$(this).off('click.rate').on('click.rate', function(e) {
				e.preventDefault();
				startRateValue = i + 1;
				chooseRate();
				filterShuffle();
			});
		});

		chooseRate();
	};

	var initShuffle = function() {
		var images = hotelResult.find('img');
		var loadedImgNum = 0;

		var startShuffle = function(){
			hotelResult.shuffle({
				speed: 0,
				itemSelector: '.where-destination__item',
				sizer: $('.where-destination__item').eq(0)
			});

			hotelResult.on('done.shuffle', function() {
				filterShuffle();
			});
		};

		images.each(function(){
			var that = $(this);
			var img = new Image();

			img.onload = function(){
				loadedImgNum ++;
				if (loadedImgNum === images.length) {
					startShuffle();
				}
			};

			img.onerror = function(){
				startShuffle();
			};

			img.src = that.attr('src');

		});
	};


	var filterShuffle = function(){

		items.addClass('hidden');
		var itemsShowCurrent = items.filter(function(){
			var self = $(this);
			return (self.data('category') === categorySLValue || categorySLValue === '') &&
				(self.data('location') === locationSLValue || locationSLValue === '') &&
				self.data('rating') <= startRateValue;
		});

		itemsShowCurrent.removeClass('hidden');
		hotelResult.prepend(itemsShowCurrent);

		hotelResult.data('shuffle').sizer = $('.where-destination__item').eq(0)[0];

		hotelResult.shuffle('shuffle', function($el){
			return !$el.hasClass('hidden');
		});
	};

	var initFilter = function(){
		categorySL.on('afterSelect.category', function(){
			categorySLValue = categorySLDefault.val();
			filterShuffle();
		});

		locationSL.on('afterSelect.location', function(){
			locationSLValue = locationSLDefault.val();
			filterShuffle();
		});
	};

	var initModule = function(){
		initShuffle();
		starRating();
		initFilter();
	};

	initModule();
};
