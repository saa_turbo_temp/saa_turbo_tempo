/**
 * @name SIA
 * @description manage change flight functions
 * @version 1.0
 */
SIA.mbChangeFlight = function(){
	var cbChangeSegmentEl = $('[data-enable-element]'),
			linkAddStopoverEl = $('[data-add-stopover]'),
			removeStopoverEl = $('[data-remove-stopover]'),
			flightCb1 = $('#flight-checkbox-1'),
			flightCb2 = $('#flight-checkbox-2'),
			cabinCb = $('#cabin-1').parent();

	var isAllChecked = function(){
		if (flightCb2.length === 0) {
			return flightCb1.is(':checked');
		}
		return (flightCb1.is(':checked') && flightCb2.is(':checked'));
	};

	var checkTheCabinCbStatus = function(){
		cabinCb[isAllChecked() ? 'removeClass' : 'addClass']('disabled');
	};

	flightCb1.off('change.mbChangeFlight').on('change.mbChangeFlight', function(){
		checkTheCabinCbStatus();
	});

	flightCb2.off('change.mbChangeFlight').on('change.mbChangeFlight', function(){
		checkTheCabinCbStatus();
	});

	checkTheCabinCbStatus();

	var resetFields = function(el) {
		if(el.hasClass('ui-autocomplete-input')) {
			var parentEl = el.closest('[data-autocomplete]');
			if(parentEl.data('autocomplete')) {
				parentEl.addClass('default').find('[autocomplete]').data('uiAutocomplete')._value(
					el.find('[selected]').length ? el.find(':selected').data('text') : ''
				);
			} else {
				el.defaultSelect('refresh');
			}
		} else {
			el.datepicker('setDate', '').parents('.input-3').addClass('default');
		}
	};

	cbChangeSegmentEl.off('change.segment').on('change.segment', function() {
		var self = $(this),
				len = self.data('enableElement').length;

		for(var i = 0; i < len; i++){
			var enableEl = $(self.data('enableElement')[i]);
			if(self.is(':checked')) {
				enableEl.datepicker('option', 'disabled', false);
				enableEl.parents('.input-3').removeClass('disabled');
			} else {
				enableEl.datepicker('option', 'disabled', true);
				enableEl.parents('.input-3').addClass('disabled');
			}
		}
	}).trigger('change.segment');

	linkAddStopoverEl.off('click.addStopover').on('click.addStopover', function(e) {
		e.preventDefault();
		var self = $(this),
				wrapperFlightEl = self.parents('[data-wrapper-flight]'),
				wrapperStopoverEl = wrapperFlightEl.find('[data-wrap-stopover]');

		if(!self.hasClass('disabled')) {
			self.addClass('disabled');
			wrapperStopoverEl.find('.input-3')[isSegmentChecked(wrapperStopoverEl) ? 'removeClass' : 'addClass']('disabled');
			wrapperStopoverEl.removeClass('hidden');
		}
	});

	var isSegmentChecked = function(wrapperStopoverEl){
		return wrapperStopoverEl.parents('.multi-flight--change').find('input:checkbox').is(':checked');
	};

	removeStopoverEl.off('click.removeStopover').on('click.removeStopover', function(e) {
		e.preventDefault();
		var wrapperStopoverEl = $(this).parents('[data-wrap-stopover]'),
				addStopverEl = wrapperStopoverEl.parents('[data-wrapper-flight]').find('[data-add-stopover]');

		if(!wrapperStopoverEl.hasClass('hidden')) {
			wrapperStopoverEl.addClass('hidden').find('input').each(function() {
				resetFields($(this));
			});
			addStopverEl.removeClass('disabled');
		}
	});
};
