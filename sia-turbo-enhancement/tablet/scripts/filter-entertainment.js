SIA.filterEntertainment = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var container = $('[data-entertaiment]');
	var items = container.find('.ent-item');
	var filterSelect = container.find('[data-entertaiment-filter]');
	var btnSeemore = container.find('.see-more-btn');
	var additionalItemsDisplay = container.data('item') * 1;
	var currentSet = (container.data('set') * 1);
	var minItemsDisplay = additionalItemsDisplay * currentSet;
	var filteredItems = null;

	var filterByCategory = function() {
		win.off('resize.filterEntertainment').on('resize.filterEntertainment', function() {
			updateNumberOfItemAdded();
			minItemsDisplay = additionalItemsDisplay * currentSet;
			filterHandler();
		}).trigger('resize.filterEntertainment');

		filterSelect.off('change.staticContentMusic').on('change.staticContentMusic', function() {
			filterHandler();
			currentSet = (container.data('set') * 1);
			updateNumberOfItemAdded();
			minItemsDisplay = additionalItemsDisplay * currentSet;
		});
	};

	var filterHandler = function() {
		items.addClass('hidden');
		filteredItems = getFilteredItem();
		var len = (filteredItems.length > minItemsDisplay) ? minItemsDisplay : filteredItems.length;
		for (var i = 0; i < len; i++) {
			if (filteredItems.eq(i).length) {
				filteredItems.eq(i).removeClass('hidden');
			}
		}
		toggleBtnSeeMore();
	};

	var updateNumberOfItemAdded = function() {
		if (global.vars.detectDevice.isTablet()) {
			additionalItemsDisplay = 3;
		} else if (global.vars.detectDevice.isMobile()) {
			additionalItemsDisplay = 1;
		} else {
			additionalItemsDisplay = container.data('item') * 1;
		}
	};

	var getFilteredItem = function() {
		var val = filterSelect.find('option').filter(':selected').attr('value');
		if (val === '') {
			return items;
		}
		return items.filter(function() {
			return $(this).data('category') === val;
		});
	};

	var toggleBtnSeeMore = function() {
		var len = filteredItems.length;
		var visibleElLen = filteredItems.not('.hidden').length;
		btnSeemore[(len === visibleElLen) ? 'addClass' : 'removeClass']('hidden');
	};

	var handleSeeMore = function() {
		btnSeemore.off('click.staticContentMusic').on('click.staticContentMusic', function(e) {
			e.preventDefault();
			filteredItems = getFilteredItem();
			var visibleItems = filteredItems.not('.hidden');
			var len = (filteredItems.length > minItemsDisplay) ?
				(minItemsDisplay < visibleItems.length ? visibleItems.length : minItemsDisplay) :
				filteredItems.length;
			currentSet++;
			len = len + additionalItemsDisplay;
			for (var i = 0; i < len; i++) {
				if (filteredItems.eq(i).length) {
					filteredItems.eq(i).removeClass('hidden');
				}
			}
			toggleBtnSeeMore();
		});
	};

	var initModule = function() {
		filterByCategory();
		handleSeeMore();
	};

	initModule();
};
