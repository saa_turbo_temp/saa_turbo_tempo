<% _.each(data, function(flight, index) { %>
  <div class="booking-item item-available">
  	<% if(flight.delay) { %>
      <div class="alert-block checkin-alert hidden">
        <div class="inner">
          <div class="alert__icon"><em class="ico-alert">&nbsp;</em></div>
          <div class="alert__message"><%= flight.delay %></div>
        </div>
      </div>
    <% } %>
    <div data-accordion="1" class="block-2 accordion accordion--1 booking--style-1" data-url="<%- flight.url %>">
    	<div class="accordion__control-inner">
  		  <a href="#" data-accordion-trigger="1" class="accordion__control accordion__control--1">
          <% if(flight.flag) { %>
            <span class="flag"><%- flight.flag %></span>
          <% } %>
  		    <h4 class="sub-heading-2--dark">Booking Reference <%- flight.referenceNumber %></h4>
  		    <h5 class="sub-heading-3--dark"><%- flight.from.name %> to <%- flight.to.name %>
            <span>– <%- flight.departureDate %></span>
            <% if(flight.confirm) { %>
              <span class="confirm"> <%= flight.confirm %> </span>
            <% }%>
          </h5>
          <em class="ico-point-d hidden"></em>
  		  </a>
  		  <span class="loading loading--small">Loading...</span>
  		</div>
      <div data-accordion-content="1" class="accordion__content" style="display: none;"></div>
    </div>
  </div>
<% }); %>
