<% if(data && data.length) {%>
  <% _.each(data, function(segment, idx) { %>
    <% if (segment.segmentId) {%>
      <% var selected = (value == segment.segmentId) %>
      <% if (segment.itinerary.listSegment.length) {%>
        <option value="<%- segment.segmentId %>" <%= selected ? "selected" : ""%>><%- (segment.index || idx) + '. ' +segment.segmentDropDownValue + ' - ' + segment.itinerary.listSegment[0].fromCity.departuteDate%></option>
      <% }%>
    <% } else { %>
      <option value="" disabled selected><%- segment.segmentDropDownValue %></option>
    <% }%>
  <% }); %>
<% } else {%>
  <option value="" disabled selected>Select</option>
<% }%>