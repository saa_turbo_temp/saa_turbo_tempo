<div class="flights__info--group">
  <div class="flights--detail" data-flight-number="<%= data.referenceNumber %>" data-carrier="<%= data.carrierCode %>" data-date="<%= data.date %>" data-origin="<%= data.origin %>">
    <span>
      Flight <%= data.carrierCode %> <%= data.flightNumber %>
      <em class="ico-point-d"></em>
      <span class="loading loading--small hidden">Loading...</span>
    </span>
    <div class="details hidden">
      <p>Aircraft type: <%= data.aircraft %></p>
      <p>Flying time: <%= data.flyingTime %></p>
    </div>
  </div>
  <span class="flights-type"><%= data.flightClass %></span>
</div>
<div class="booking-info no-border">
  <div class="booking-info-item ">
    <div class="booking-desc">
      <span class="hour"><%= data.from.code %> <%= data.from.time %></h4>
      <span class="country-name"><%= data.from.cityName %></span>
      <div class="booking-content">
        <span><%= data.from.date %>, <br><%= data.from.airport %> Terminal <%= data.from.terminal %></span>
      </div>
      <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
    </div>
  </div>
  <div class="booking-info-item ">
    <div class="booking-desc">
      <span class="hour"><%= data.to.code %> <%= data.to.time %></h4>
      <span class="country-name"><%= data.to.cityName %></span>
      <div class="booking-content">
        <span><%= data.to.date %>, <br><%= data.to.airport %> Terminal <%= data.to.terminal %></span>
      </div>
    </div>
  </div>
</div>
<div class="booking-info booking-info-row">
  <div class="booking-content">
    <span><%= data.layoverMessage %></span>
  </div>
</div>