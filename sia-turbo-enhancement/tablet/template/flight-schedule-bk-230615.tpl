<!-- flight detail -->
<div class="blk-heading">
	<h3 class="sub-heading-1--dark left"><%= data.depart %> to <%= data.destination %></h2>
	<a href="#" class="btn-2 schedule-check-availability"><em class="ico-date"></em>Book now</a>
</div>

<div class="flight-schedule">
	<table class="flight-schedule__table">
		<thead>
			<tr>
				<th class="title-head hidden-mb"><%= data.headingTitle.title %></th>
				<% _.each( data.headingTitle.dateFlight, function( date, i ){ %>
					<th class="title-head hidden-mb<% if(i == 0) { %> title-head-date--first<% } %><% if(i == data.headingTitle.dateFlight.length - 1) { %> title-head-date--last<% } %>"><%= date.day %><span><%= date.date %></span></th>
				<% }); %>
			</tr>
		</thead>
		<tbody>
			<% _.each( data.flights, function( flights, i ){ %>
				<tr>
					<td class="flights__info--group">
						<div class="flights__info none-border">
							<div data-flight-number="<%= flights.flightInfor.flightNumber%>" data-carrier-code="<%= flights.flightInfor.carrierCode%>" data-date="<%= flights.flightInfor.date%>" data-origin="<%= flights.flightInfor.origin%>" class="flights--detail left" <%= (flights.flightInfor.mean && flights.flightInfor.flyTime) ? 'data-infor="true"' : ''%> ><span>Flight <%= flights.flightInfor.number%><em class="ico-point-d"></em><span class="loading loading--small hidden">Loading...</span></span>
								<div class="details hidden">
									<p>Aircraft type: <%= flights.flightInfor.mean%></p>
									<p>Flying time: <%= flights.flightInfor.flyTime%></p>
								</div>
							</div><span class="class-flight"></span>
						</div>
						<% _.each( flights.plane, function( plane, idx ){ %>
							<div class="flights__info">
								<div class="flights__info--detail"><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em><span class="hour"><%= plane.depart.shortForm%>&nbsp;<%= plane.depart.time%></span><span class="country-name"><%= plane.depart.place%></span><span class="date"><%= plane.depart.airport%></span></div>
								<div class="flights__info--detail"><span class="hour"><%= plane.arrive.shortForm%>&nbsp;<%= plane.arrive.time%></span><span class="country-name"><%= plane.arrive.place%></span><span class="date"><%= plane.arrive.airport%></span></div>
							</div>

							<% if(plane.notice.layover){%>
								<div class="flights__info"><span><strong><%= plane.notice.layover.title%></strong>: <%= plane.notice.layover.time%></span></div>
							<%}%>

							<% if(idx == 0 && i == 0){%>
								<div class="flights__info none-border flights__info-schedule">
									<div data-flight-number="<%= data.flights[1].flightInfor.flightNumber%>" data-carrier-code="<%= data.flights[1].flightInfor.carrierCode%>" data-date="<%= data.flights[1].flightInfor.date%>" data-origin="<%= flights.flightInfor.origin%>" class="flights--detail left" <%= (flights.flightInfor.mean && flights.flightInfor.flyTime) ? 'data-infor="true"' : ''%> ><span>Flight <%= data.flights[1].flightInfor.number%><em class="ico-point-d"></em><span class="loading loading--small hidden">Loading...</span></span>
										<div class="details hidden">
											<p>Aircraft type: <%= flights.flightInfor.mean%></p>
											<p>Flying time: <%= flights.flightInfor.flyTime%></p>
										</div>
									</div><span class="class-flight"></span>
								</div>
							<%}%>

							<% if(plane.notice.total){%>
								<div class="flights__info none-border"><span><strong><%= plane.notice.total.title%></strong>: <%= plane.notice.total.time%></span></div>
							<%}%>

							<% if(idx === flights.plane.length - 1){%>
								<% _.each( data.headingTitle.dateFlight, function( date, idhead ){ %>
									<%
										var selected = flights.flightDate.selected.match(/[a-zA-Z]{3}\s?\d{1,2}/)[0];
										var dateFlight = data.headingTitle.dateFlight[idhead].month + ' ' + parseInt(data.headingTitle.dateFlight[idhead].date, 10);
										var isSelected = (selected == dateFlight);
									%>
									<div class="flights__info flights__info--mb visible-mb">
										<div class="flights__info--schedule"><div class="package--name title-head<%= idhead%2 !== 0 ? ' even' : '' %>"><%= date.date %>&nbsp;<%= date.month %>&nbsp;(<%= date.day %>)</div>
										<div class="package--flight<%= isSelected ? ' date-selected' : '' %>">
											<% if(flights.flightDate.date[idhead] == 1) { %><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying today</span></em><% } else { %><span class="dash">-</span><% } %>
											<span class="package-border"></span></div>
										</div>
									</div>
								<% }); %>
							<%}%>
						<% }); %>
					</td>
					<% _.each( flights.flightDate.date, function( date, idxDate ){ %>
						<%
							var selected = flights.flightDate.selected.match(/[a-zA-Z]{3}\s?\d{1,2}/)[0];
							var dateFlight = data.headingTitle.dateFlight[idxDate].month + ' ' + parseInt(data.headingTitle.dateFlight[idxDate].date, 10);
							var isSelected = (selected == dateFlight);
						%>
						<td class="hidden-mb col-date<%= (idxDate ==0) ? ' col-date--first' : ''%><%= (idxDate ==flights.flightDate.date.length-1) ? ' col-date--last' : ''%><%= isSelected ? ' date-selected' : '' %>">
							<% if(flights.flightDate.date[idxDate] == 1) { %><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying today</span></em><% } else { %><span class="dash">-</span><% } %>
							<span class="package-border"></span>
						</td>
					<% }); %>
				</tr>
			<% }); %>
		</tbody>
	</table>
</div>
<ul class="button-group">
	<li><a href="#" class="link-4 btn--prev" data-url="ajax/flight-schedule-prev.json"><em class="ico-point-l"></em>Previous week</a>
	</li>
	<li><a href="#" class="link-4 btn--next" data-url="ajax/flight-schedule-next.json">Next week<em class="ico-point-r"></em></a>
	</li>
</ul>
