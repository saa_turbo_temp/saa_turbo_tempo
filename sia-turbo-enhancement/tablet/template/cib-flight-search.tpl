<% _.each(data.results, function(res, resIdx) { %>
  <tr class="<%- resIdx === 0 ? 'active' : '' %> <%- (resIdx % 2) === 0 ? 'even-item' : '' %> <%- resIdx > 4 ? 'hidden' : '' %>">
    <td class="first flights__info--group">
      <% _.each(res.stops, function(stop, stopIdx) { %>
        <% if(typeof stop.flightName != 'undefined') { %>
          <div class="flights__info none-border">
            <div class="flights--detail left">
              <span>
                <%- stop.flightName %>
                <em class="ico-point-d"></em>
                <span class="loading loading--small hidden">Loading...</span>
              </span>
              <div class="details hidden"><p>Aircraft type: <%- stop.aircraftType %></p><p>Flying time: <%- stop.flyingTime %><p></div>
            </div><span class="class-flight"><%- stop.flightClass %></span>
          </div>
        <% } %>
        <div class="flights__info">
          <div class="flights__info--detail">
            <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
            <span class="hour"><%- stop.from.country %> <%- stop.from.time %></span>
            <span class="date"><%- stop.from.date %>,<br> <%- stop.from.port %></span>
          </div>
          <div class="flights__info--detail"><span class="hour"><%- stop.to.country %> <%- stop.to.time %></span><span class="date"><%- stop.to.date %>,<br> <%- stop.to.port %></span></div>
          <% if(stop.operatedBy) { %>
            <p class="operated">Operated by <%- stop.operatedBy %></p>
            <% } %>
        </div>
        <div class="flights__info <%- (stopIdx === (res.stops.length - 1)) ? 'last' : '' %>">
          <span><%- stop.totalTime %></span>
          <% if(stopIdx === (res.stops.length - 1)) { %>
            <a href="javascript:;" class="link-5 right">Fares<em class="ico-point-d"></em></a>
          <% } %>
        </div>
      <% }); %>

      <div class="flights__info asdasd flights__info--mb <%- resIdx == 0 ? 'visible-mb' : '' %>">
        <% _.each(res.packages, function(pkg, pkgIdx) { %>
          <div class="flights__info--price">
            <% if(pkgIdx === 0) { %>
              <div class="package--name bgd-green-7-66">Economy Sweet <br>Deals 2 To Go</div>
            <% } %>
            <% if(pkgIdx === 1) { %>
              <div class="package--name bgd-green-6">Economy Sweet <br>Deals</div>
            <% } %>
            <% if(pkgIdx === 2) { %>
              <div class="package--name bgd-green-4">Economy Flexi <br>Saver</div>
            <% } %>
            <% if(pkgIdx === 3) { %>
              <div class="package--name bgd-green-2">Economy Flexi</div>
            <% } %>
            <% if(pkgIdx === 4) { %>
              <div class="package--name bgd-green-1">Economy Flexi</div>
            <% } %>
            <% if(pkgIdx === 5) { %>
              <div class="package--name bgd-green-5">Economy Flexi</div>
            <% } %>
            <% if(pkgIdx === 6) { %>
              <div class="package--name bgd-green-4">Economy Flexi</div>
            <% } %>
            <% if(pkgIdx === 7) { %>
              <div class="package--name bgd-green-gray">Premium <br>Economy</div>
            <% } %>
            <div class="package--price">
              <% if(pkg.price) { %>
                <div class="custom-radio custom-radio--1">
                  <input name="flight-select-mb-<%- data.flight %>" id="flight-select-mb-<%- data.flight %>-<%- resIdx %>-<%- pkgIdx %>" type="radio" value="SGD <%- pkg.price %>" data-related="<%- pkgIdx %>" data-fare="<%- pkg.price %>" data-taxes="<%- pkg.taxes %>" data-carrier="<%- pkg.carrier %>" data-flight-class="<%- pkg.flightClass%>" data-datetime="<%- res.stops[0].from.date %> - <%- res.stops[0].from.time %>"/>
                  <label for="flight-select-mb-<%- data.flight %>-<%- resIdx %>-<%- pkgIdx %>">SGD <%- pkg.price %></label>
                </div><a href="javascript: void(0)" data-type="3" data-tooltip="true", data-max-width="310", data-content="&lt;div class=&quot;summary-fare&quot;&gt; &lt;h3 class=&quot;title-conditions&quot;&gt;Summary of fare conditions&lt;/h3&gt; &lt;ul class=&quot;summary-fare__conditions&quot;&gt; &lt;li&gt;&lt;em class=&quot;ico-check-thick&quot;&gt;&lt;/em&gt;Booking change (USD 50)&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-check-thick&quot;&gt;&lt;/em&gt;Earn KrisFlyer miles (50%)&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-close&quot;&gt;&lt;/em&gt;Refund/Cancellation&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-close&quot;&gt;&lt;/em&gt;Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;&lt;a href=&quot;cib-blank-page.html&quot;&gt;terms and conditions&lt;/a&gt;) &lt;/li&gt; &lt;/ul&gt; &lt;/div&gt;" class="ico-info-round-fill">&nbsp;</a>
              <% } else { %>
                <span>Not available</span>
              <% } %>
            </div>
          </div>
        <% }); %>
      </div>
    </td>
    <% _.each(res.packages, function(pkg, pkgIdx) { %>
      <td class="hidden-mb <%- (pkgIdx === (res.packages.length - 1)) ? 'last' : ('package-' + (pkgIdx + 1)) %>">
        <% if(pkg.fewLeft) { %>
          <div class="seat-left"><span>A few seats left</span></div>
        <% } %>
        <% if(pkg.price) { %>
          <div class="custom-radio custom-radio--1">
            <input name="flight-select-<%- data.flight %>" id="flight-select-<%- data.flight %>-<%- resIdx %>-<%- pkgIdx %>" type="radio" value="SGD <%- pkg.price %>" data-related="<%- pkgIdx %>" data-fare="<%- pkg.price %>" data-flight-class="<%- pkg.flightClass%>" data-taxes="<%- pkg.taxes %>" data-carrier="<%- pkg.carrier %>" data-datetime="<%- res.stops[0].from.date %> - <%- res.stops[0].from.time %>"/>
            <label for="flight-select-<%- data.flight %>-<%- resIdx %>-<%- pkgIdx %>">SGD <%- pkg.price %></label>
          </div><a href="javascript: void(0)" data-type="3" data-tooltip="true", data-max-width="310", data-content="&lt;div class=&quot;summary-fare&quot;&gt; &lt;h3 class=&quot;title-conditions&quot;&gt;Summary of fare conditions&lt;/h3&gt; &lt;ul class=&quot;summary-fare__conditions&quot;&gt; &lt;li&gt;&lt;em class=&quot;ico-check-thick&quot;&gt;&lt;/em&gt;Booking change (USD 50)&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-check-thick&quot;&gt;&lt;/em&gt;Earn KrisFlyer miles (50%)&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-close&quot;&gt;&lt;/em&gt;Refund/Cancellation&lt;/li&gt; &lt;li&gt;&lt;em class=&quot;ico-close&quot;&gt;&lt;/em&gt;Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;&lt;a href=&quot;cib-blank-page.html&quot;&gt;terms and conditions&lt;/a&gt;) &lt;/li&gt; &lt;/ul&gt; &lt;/div&gt;" class="ico-info-round-fill">&nbsp;</a>
        <% } else { %>
          <span>Not available</span>
        <% } %>
      </td>
    <% }); %>
  </tr>
<% }); %>
