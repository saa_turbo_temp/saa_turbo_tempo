<% _.each( items, function( item, idx ){ %>
	<div class="messages-list__item">
		<div class="custom-checkbox custom-checkbox--1">
			<input type="checkbox" id="<%- item.id %>" name="<%- item.id %>">
			<label for="<%- item.id %>">&nbsp;</label>
		</div>
		<div class="messages-list__content">
			<h3 class="sub-heading-2--dark"><a class="messages-list__unread" href="<%- item.link %>"><%- item.title %></a>
			</h3>
			<p><%- item.shortDescription %></p>
		</div>
		<div class="messages-list__time" data-time="<%- item.date.day %> <%- item.date.month %> <%- item.date.year %>">
			<p><%- item.date.day %> <%- item.date.month %><span class="hidden-mb"><%- item.date.year %></span></p>
		</div>
	</div>
<% }); %>
