<fieldset>
  <!--OUTBOUND FLIGHTS-->
  <% _.each(data.outBoundFlight.flights, function(flight, flightIdx) { %>
    <div class="wrapper">
      <% _.each(flight.info, function(info, infoIdx) { %>
        <% if(infoIdx < flight.info.length - 1) { %>
          <section class="block-1 fare-calendar">
            <div class="blk-heading">
              <h3 class="main-heading">
                <span style="<%= infoIdx > 0 ? 'visibility : hidden;' : '' %>">
                  <%= (flightIdx + 1) + '. ' %>
                </span>
                <%= flight.info[infoIdx].replace(/\s?\(.*\)/g, '') %> to <%= flight.info[infoIdx + 1].replace(/\s?\(.*\)/g, '') %>
              </h3>
            </div>
            <div data-name="search-calendar-<%= flightIdx %>" class="flight-search-calendar">
              <div class="flight-search-calendar__content">
                <% _.each(data.outBoundFlight.headingTitle.dateFlight, function(date, dateIdx) { %>
                  <div class="flight-search-calendar__item">
                    <div class="search-calendar search-calendar--schedule custom-radio">
                      <input name="search-calendar-out-<%= flightIdx + '-' + infoIdx %>" id="calendar-<%= flightIdx + '-' + infoIdx + '-' + dateIdx %>" type="radio" value="<%= date.date + ' ' + date.month %>" <%= flight.flightDate.selected.indexOf(date.month + ' ' + date.date) >= 0 ? 'checked="checked"' : '' %>/>
                      <label for="calendar-<%= flightIdx + '-' + infoIdx + '-' + dateIdx %>">
                        <span class="search-calendar__date"><%= date.day %></span>
                        <span class="search-calendar__day"><%= date.date + ' ' + date.month %></span>
                        <% if(flight.flightDate.date[dateIdx] === 1) { %>
                          <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                        <% } else { %>
                          <span class="search-calendar__available">-</span>
                        <% } %>
                      </label>
                    </div>
                  </div>
                <% }); %>
              </div>
              <% if(infoIdx === flight.info.length - 2) { %>
                <div class="flight-search-calendar__control">
                  <a href="../desktop/orb-flight-schedule-1.html" class="slick-prev"
                    data-url-desktop="../desktop/orb-flight-schedule-1.html"
                    data-url-tablet="../tablet/orb-flight-schedule-1.html"
                    data-url-mobile="../mobile/orb-flight-schedule-1.html"
                    data-url-old="../old-browser/orb-flight-schedule-1.html">
                    <em class="ico-point-l"></em> Prev <span class="day">7</span> days
                  </a>
                  <a href="../desktop/orb-flight-schedule-2.html" class="slick-next"
                    data-url-desktop="../desktop/orb-flight-schedule-2.html"
                    data-url-tablet="../tablet/orb-flight-schedule-2.html"
                    data-url-mobile="../mobile/orb-flight-schedule-2.html"
                    data-url-old="../old-browser/orb-flight-schedule-2.html">
                    Next <span class="day">7</span> days<em class="ico-point-r"></em>
                  </a>
                </div>
              <% } %>
            </div>
          </section>
        <% } %>
      <% }); %>
    </div>
  <% }); %>


  <!--INBOUND FLIGHTS-->
<% if(data.inBoundFlight) { %>
  <% _.each(data.inBoundFlight.flights, function(flight, flightIdx) { %>
    <div class="wrapper">
      <% _.each(flight.info, function(info, infoIdx) { %>
        <% if(infoIdx < flight.info.length - 1) { %>
          <section class="block-1 fare-calendar">
            <div class="blk-heading">
              <h3 class="main-heading">
                <span style="<%= infoIdx > 0 ? 'visibility : hidden;' : '' %>">
                  <%= (data.outBoundFlight.flights.length + flightIdx + 1) + '. ' %>
                </span>
                <%= flight.info[infoIdx].replace(/\s?\(.*\)/g, '') %> to <%= flight.info[infoIdx + 1].replace(/\s?\(.*\)/g, '') %>
              </h3>
            </div>
            <div data-name="search-calendar-<%= flightIdx %>" class="flight-search-calendar">
              <div class="flight-search-calendar__content">
                <% _.each(data.inBoundFlight.headingTitle.dateFlight, function(date, dateIdx) { %>
                  <div class="flight-search-calendar__item">
                    <div class="search-calendar search-calendar--schedule custom-radio">
                      <input name="search-calendar-in-<%= flightIdx + '-' + infoIdx %>" id="calendar-<%= flightIdx + '-' + infoIdx + '-' + dateIdx %>" type="radio" value="<%= date.date + ' ' + date.month %>" <%= flight.flightDate.selected.indexOf(date.month + ' ' + date.date) >= 0 ? 'checked="checked"' : '' %>/>
                      <label for="calendar-1">
                        <span class="search-calendar__date"><%= date.day %></span>
                        <span class="search-calendar__day"><%= date.date + ' ' + date.month %></span>
                        <% if(flight.flightDate.date[dateIdx] === 1) { %>
                          <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                        <% } else { %>
                          <span class="search-calendar__available">-</span>
                        <% } %>
                      </label>
                    </div>
                  </div>
                <% }); %>
              </div>
              <% if(infoIdx === flight.info.length - 2) { %>
                <div class="flight-search-calendar__control">
                  <a href="../desktop/orb-flight-schedule-1.html" class="slick-prev"
                    data-url-desktop="../desktop/orb-flight-schedule-1.html"
                    data-url-tablet="../tablet/orb-flight-schedule-1.html"
                    data-url-mobile="../mobile/orb-flight-schedule-1.html"
                    data-url-old="../old-browser/orb-flight-schedule-1.html">
                    <em class="ico-point-l"></em> Prev <span class="day">7</span> days
                  </a>
                  <a href="../desktop/orb-flight-schedule-2.html" class="slick-next"
                    data-url-desktop="../desktop/orb-flight-schedule-2.html"
                    data-url-tablet="../tablet/orb-flight-schedule-2.html"
                    data-url-mobile="../mobile/orb-flight-schedule-2.html"
                    data-url-old="../old-browser/orb-flight-schedule-2.html">
                    Next <span class="day">7</span> days<em class="ico-point-r"></em>
                  </a>
                </div>
              <% } %>
            </div>
          </section>
        <% } %>
      <% }); %>
    </div>
  <% }); %>
<% } %>

  <!--SUBMIT-->
  <div class="button-group-1">
    <input type="submit" name="search-btn" id="search-btn" value="Search" class="btn-1" data-btn-search="true"/>
  </div>
</fieldset>
