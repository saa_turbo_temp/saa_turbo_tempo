<figure><img src="<%- data.upgrade.image %>" alt="<%- data.upgrade.title %>" longdesc="img-desc.html">
</figure>
<div class="flights-upgrade__content">
  <div class="wrap-upgrade-cb">
    <div class="custom-checkbox custom-checkbox--1">
      <input name="upgrade-checkbox-3-<%- data.upgrade.flight %>" id="upgrade-checkbox-3-<%- data.upgrade.flight %>" type="checkbox" data-upgrade-title="<%- data.upgrade.title %>" data-upgrade-price="<%- data.upgrade.price %>">
      <label for="upgrade-checkbox-3-<%- data.upgrade.flight %>"><%- data.upgrade.title %></label>
    </div><span class="total-cost">Sgd <%- data.upgrade.price %></span>
  </div>
  <p><%- data.upgrade.desc %></p>
  <ul>
    <% _.each(data.upgrade.details, function(item, i) { %>
      <li><%- item %></li>
    <% }); %>
  </ul>
</div>