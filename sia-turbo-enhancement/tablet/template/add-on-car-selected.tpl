<% if(data) { %>
  <div class="list-cars-result car-extra" data-id=''><a href="#" class="btn-back"><em class="ico-point-l"><span class="ui-helper-hidden-accessible"></span></em>Back to search result</a>
    <form action="#" mehtod="post" id="form-selected-car">
        <div class="item-result">
          <figure><img src="<%- data.largeImageURL %>" alt="<%- data.vehicleName %>" longdesc="img-desc.html" />
          </figure>
          <div class="content-result">
              <div class="title-content"><span class="title-5--blue"><%- data.vehicleName %></span><span>&nbsp;or similar</span></div>
              <ul class="seat-car">
                  <li><%- data.seats %> Seats</li>
                  <li><%- data.doors %> Doors</li>
                  <li><%- data.bigSuitcase %> Large bags</li>
              </ul>
              <ul class="condition">
                  <li>
                    <% if(data.aircon === "Yes"){ %>
                      Air Conditioning
                    <% } else { %>
                    <% } %>
                  </li>
                  <li><%- data.automatic %></li>
              </ul>
              <ul class="location">
                  <li><em class="ico-airplane"></em><span>Pick up location</span><span class="text"><%- data.route.pickUp.locationName %></span></li>
                  <li><em class="ico-2-petro-vector"></em><span>Fuel policy</span><span class="text"><%- data.fuelPolicy.description %></span></li>
              </ul>
              <ul class="cancellation">
                <% if(data.freeCancellation) { %>
                  <span class="free-tooltip"><em class="ico-check-thick"></em><span>Free cancellation</span><em data-tooltip="true" data-type="2" data-max-width="255" data-content="&lt;p class=&quot;tooltip__text-2&quot;&gt;Free cancellation up to 48 hours before your pick-up (deposit payments excluded)&lt;/p&gt;" class="ico-tooltips" tabindex="0"></em></span>
                <% } %>
                <% if(data.theftProtection) { %>
                  <li><em class="ico-check-thick"></em><span>Theft protection</span></li>
                <% } %>
                <% if(data.freeAmendments) { %>
                  <li><em class="ico-check-thick"></em><span>Free ammendments</span></li>
                <% } %>
                <% if(data.theftProtection) { %>
                  <li><em class="ico-check-thick"></em><span>Collision damage waiver</span></li>
                <% } %>
              </ul>
              <div class="supplied-by"><span>Supplied by</span><img src="<%- data.supplier.smallLogo %>" alt="<%- data.supplier.supplierName %>" longdesc="img-desc.html" />
              </div>
              <div class="price-car"><span class="note">Price for 3 days</span>
              <div class="sub-heading-2--blue"><%- data.price.baseCurrency %> <%- data.price.basePrice %></div><span class="miles">EARN 2000 MILES</span>
              <div class="button-group-3">
                  <input type="button" name="remove-car" id="remove-car" value="Remove car" class="btn-4" />
              </div>
              </div>
          </div>
        </div>
        <div class="full-protection block-2__main">
          <% if(data.insurancePackage.code === "I"){ %>
            <span class="title-5--blue">Full Protection</span>
            <div class="wrap-bg">
              <span>With Full Protection, you’re completely covered against damage or theft of your car. Get extra peace of mind now, and pay less for cover at the car hire counter.&nbsp;
              <span>
                <a href="javascript:void(0)" data-trigger-popup=".popup--add-ons-car-popup-excess-explained" class="trigger-popup-excess-explained">
                  <span class="text">Your excess, explained</span>
                </a>
              <div class="car-protection-table">
                  <div class="editor">
                      <table data-sort-content="true" data-room-table="true">
                          <thead class="hidden-mb">
                              <tr role="row">
                                  <th class="th-5">What is covered</th>
                                  <th class="th-5">No Protection</th>
                                  <th class="th-5">Full Protection<span>Only <%- data.extras.price.baseCurrency %> <%- data.extras.price.basePrice %> per day</span></th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr role="row" class="odd content-tbody">
                                  <td><span class="title item-hidden">What is covered</span>The car's excess</td>
                                  <td><span class="title item-hidden">No Protection</span><em class="ico-close"><span class="ui-helper-hidden-accessible">Not covered</span></em></td>
                                  <td><span class="title item-hidden">Full Protection<span>Only <%- data.extras.price.baseCurrency %> <%- data.extras.price.basePrice %> per day</span></span><em class="ico-check-thick"><span class="ui-helper-hidden-accessible">Covered</span></em></td>
                              </tr>
                              <tr role="row" class="even content-tbody">
                                  <td><span class="title item-hidden">What is covered</span>Windows, mirrors, wheels &amp; tyres</td>
                                  <td><span class="title item-hidden">No Protection</span><em class="ico-close"><span class="ui-helper-hidden-accessible">Not covered</span></em></td>
                                  <td><span class="title item-hidden">Full Protection<span>Only <%- data.extras.price.baseCurrency %> <%- data.extras.price.basePrice %> per day</span></span><em class="ico-check-thick"><span class="ui-helper-hidden-accessible">Covered</span></em></td>
                              </tr>
                              <tr role="row" class="odd content-tbody">
                                  <td><span class="title item-hidden">What is covered</span>Administration and breakdown charges</td>
                                  <td><span class="title item-hidden">No Protection</span><em class="ico-close"><span class="ui-helper-hidden-accessible">Not covered</span></em></td>
                                  <td><span class="item-hidden">Full Protection<span>Only <%- data.extras.price.baseCurrency %> <%- data.extras.price.basePrice %> per day</span></span><em class="ico-check-thick"><span class="ui-helper-hidden-accessible">Covered</span></em></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
              <div class="button-group-2">
                  <span class="title-5">Full Protection has been added to your booking</span>
                  <span class="title-5 text-add-full-protection hidden">Full Protection has been removed from your booking</span>
                  <ul data-check-full-protection="" class="check-full-protection hidden">
                      <li>
                          <div class="custom-checkbox custom-checkbox--1">
                              <input name="Full Protection" id="full-protection" aria-labelledby="Full Protection" type="checkbox" value="Full Protection" aria-label="full-protection" checked="true">
                              <label for="full-protection">full-protection</label>
                          </div>
                      </li>
                  </ul>
                  <input type="button" name="remove-protection" id="remove-protection" value="Remove full protection " class="btn-4" aria-label="Remove full protection"/>
                  <input type="button" name="add-protection" id="add-protection" value="Add full protection " class="btn-1 hidden" aria-label="Add full protection" />
              </div>
            </div>
          <% } %>
          <div class="block-extras">
            <p class="title-5--blue">Extras</p><span>If you wish to include additional drivers or rent a GPS or child seat, you may make these requests in the&nbsp;<a href="#" target="_blank"><span class="text">Manage Booking</span></a>&nbsp;after payment has been made.</span>
            <div class="wrap-bg">
              <span class="title-5--grey">Driver Details</span>
              <div class="form-group grid-row">
                <div class="grid-col one-third">
                    <label for="title">Title*</label>
                    <div class="grid-inner">
                      <div data-customselect="true" class="custom-select custom-select--2 default salutation">
                        <label for="title" class="select__label"><span class="ui-helper-hidden-accessible">Title</span>
                        </label><span class="select__text">Select</span><span class="ico-dropdown">Select</span>
                        <select id="title" name="title" data-rule-required="true" data-msg-required="Select Title." data-person-title="true">
                            <option value="">Select</option>
                            <option value="1" selected="selected">Mrs</option>
                            <option value="2">Mr</option>
                            <option value="3">Ms</option>
                            <option value="4">Miss</option>
                            <option value="5">Others</option>
                        </select>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="form-group grid-row">
                  <div class="grid-col one-half">
                    <label for="first-name">First name*</label>
                    <div class="grid-inner"><span class="input-1 capitalize-input">
                      <input type="text" name="first-name" id="first-name" placeholder="" value="" aria-labelledby="first-name-error" data-rule-required="true" data-msg-required="Enter first name." data-rule-onlyCharacter="true" data-msg-onlyCharacter="Enter letters of the English alphabet, and if required, any of these symbols /’@()”-"/></span>
                      </div>
                  </div>
              </div>
              <div class="form-group grid-row">
                  <div class="grid-col one-half">
                    <label for="sur-name">Surname*</label>
                    <div class="grid-inner"><span class="input-1 capitalize-input">
                      <input type="text" name="sur-name" id="sur-name" placeholder="" value="" aria-labelledby="sur-name-error" data-rule-required="true" data-msg-required="Enter surname." data-rule-onlyCharacter="true" data-msg-onlyCharacter="Enter letters of the English alphabet, and if required, any of these symbols /’@()”-"/></span>
                    </div>
                  </div>
              </div>
            </div>
            <p>Prices for additional extras are controlled by the local rental company and may be subject to change. The prices listed are Guide Prices only, payable direct to the rental company. If you have any additional requirements please include them in your booking request, however Rentalcars.com cannot guarantee their availability.</p>
            <div class="terms-conditions">Read&nbsp;<a href="javascript:void(0)" data-trigger-popup=".popup--add-ons-car-term-condition" class="trigger-popup-term-condition"><span class="text">terms and conditions</span></a>&nbsp;of your car rental.
            </div>
            <div class="button-group-1">
              <input type="button" name="confirm" id="confirm" value="Confirm" disabled class="btn-1 disabled" role="button"/>
            </div>
          </div>
        </div>
    </form>
  </div>
<% } %>
