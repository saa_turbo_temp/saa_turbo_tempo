<% _.each(data.calendar, function( calendar, idx ){ %>
  <div class="flight-search-calendar__item">
    <div class="search-calendar">
      <input name="<%- data.name %>" id="<%- data.name %>-<%-idx%>" type="radio" value="<%= (calendar.notAvailable) ? "" : calendar.price %>">
      <label for="<%- data.name %>-<%-idx%>">
        <% if(calendar.lowfare) {%>
          <em class="label-status"></em>
        <% } %>
        <p class="search-calendar__date"><%- calendar.date %></p>
        <p class="search-calendar__day"><%- calendar.day %></p>
        <% if(calendar.notAvailable) {%>
          <p class="search-calendar__available"><%- calendar.price %></p>
        <% } else { %>
          <p class="search-calendar__price"><%- calendar.currency %> <%- calendar.price %></p>
        <% } %>
      </label>
    </div>
  </div>
<% }); %>
