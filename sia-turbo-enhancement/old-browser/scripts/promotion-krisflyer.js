/**
 * @name SIA
 * @description Define global promotionKrisflyer functions
 * @version 1.0
 */

SIA.promotionKrisflyer = function(){
	//start promotions page
	var global = SIA.global;
	var config = global.config;
	var promotionPage = function(){
		if($('.krisflyer-list-page').length){
			var wrapper = $('[data-result]');
			var seeMoreBtn = $('.promotion-btn [data-see-more]');
			var allData = globalJson.promotionKri;
			var city = $('#krisflyer-filter-1');
			var promotion = $('#krisflyer-filter-2');
			// var createNewData = function(json){
			// 	var a = {
			// 		list: []
			// 	};
			// 	var b = {
			// 		list: []
			// 	};
			// 	for(var i = 0; i < json.promoVO.length; i++){
			// 		a.list.push(json.promoVO[i].cityVO);
			// 	}
			// 	for(var ii = 0; ii < a.list.length; ii++){
			// 		b.list = b.list.concat(a.list[ii]);
			// 	}
			// 	return b;
			// };
			var dataJSON = {
				list: []
			};
			var dataJSONFilter = {
				list: []
			};
			var page = 1;
			var itemsFirstLoad = 12;
			var limitItem = 6;

			var buildHtml = function(isFirst, items){
				if(isFirst){
					wrapper.empty();
					wrapper.html(config.template.loadingMedium);
				}
				$.get(config.url.promotionPageKrisflyerTemplate, function (data) {
					wrapper.find('.loading').addClass('hidden');
					data = data.replace(/td>\s+<td/g,'td><td');
					var template = window._.template(data, {
						'data': items
					});
					if(items.list.length){
						wrapper.append(template);
					}
					else{
						wrapper.html('<h2 class="empty-data">' + L10n.emptyData + '</h2>');
					}
				}, 'html');
			};

			var getIndex = function(value){
				var cl = {
					idx: 0,
					con: false
				};
				for(var i = 0; i < allData.promoVO.length; i ++){
					if(allData.promoVO[i].city === value){
						cl = {
							idx: i,
							con: true
						};
					}
				}
				return cl;
			};

			var filterCity = function(data){
				var dt = [];
				for(var i = 0; i < data.length; i++){
					if(data[i].type === promotion.val()){
						dt.push(data[i]);
					}
				}
				return dt;
			};

			var createSelectCity = function(ct){
				var options = [];
				for(var i = 0; i < allData.promos.city.length; i++){
					var option = '<option value="' + allData.promos.city[i].description + '" >' + allData.promos.city[i].description + '</option>';
					options.push(option);
				}
				ct.html(options.join(''));
				ct.closest('[data-customselect]').customSelect('_createTemplate');
				ct.closest('[data-customselect]').off('afterSelect.filterCity').on('afterSelect.filterCity', function(){
					if(getIndex(city.val()).con){
						dataJSON.list = allData.promoVO[getIndex(city.val()).idx].cityVO;
						if(promotion.val() !=='All'){
							dataJSON.list = filterCity(dataJSON.list);
						}
					}
					else{
						dataJSON.list = [];
					}
					page = 1;
					dataJSONFilter.list = dataJSON.list.slice(0, itemsFirstLoad);
					buildHtml(true, dataJSONFilter);
					if(itemsFirstLoad >= dataJSON.list.length){
						seeMoreBtn.hide();
					}
					else{
						seeMoreBtn.show();
					}
				});
			};

			var createSelectPromotion = function(ct){
				var options = [];
				for(var i = 0; i < allData.promos.promotion.length; i++){
					var option = '<option value="' + allData.promos.promotion[i] + '" >' + allData.promos.promotion[i] + '</option>';
					options.push(option);
				}
				ct.html(options.join(''));
				ct.closest('[data-customselect]').customSelect('_createTemplate');
				ct.closest('[data-customselect]').off('afterSelect.filterPromotion').on('afterSelect.filterPromotion', function(){
					if(getIndex(city.val()).con){
						dataJSON.list = allData.promoVO[getIndex(city.val()).idx].cityVO;
						if(promotion.val() !=='All'){
							dataJSON.list = filterCity(dataJSON.list);
						}
					}
					else{
						dataJSON.list = [];
					}
					page = 1;
					dataJSONFilter.list = dataJSON.list.slice(0, itemsFirstLoad);
					buildHtml(true, dataJSONFilter);
					if(itemsFirstLoad >= dataJSON.list.length){
						seeMoreBtn.hide();
					}
					else{
						seeMoreBtn.show();
					}
				});
			};


			seeMoreBtn.off('click.seemore').on('click.seemore', function(e){
				e.preventDefault();
				dataJSONFilter.list = dataJSON.list.slice(itemsFirstLoad + ((page - 1 )*limitItem), (itemsFirstLoad + (page*limitItem)));
				if(itemsFirstLoad + (page*limitItem) >= dataJSON.list.length){
					seeMoreBtn.hide();
				}
				page++;
				buildHtml(false, dataJSONFilter);
			});

			var reload = function(){
				createSelectCity(city);
				createSelectPromotion(promotion);
				/*$.ajax({
					url: config.url.promotionPageKrisflyerJSON,
					dataType : 'json',
					type: global.config.ajaxMethod,
					success: function(res){

					}
				});*/
				// dataJSON = globalJson.promotionKrisflyerList;
				// dataJSON = createNewData(allData);
				if(getIndex(city.val()).con){
					dataJSON.list = allData.promoVO[getIndex(city.val()).idx].cityVO;
					if(promotion.val() !=='All'){
						dataJSON.list = filterCity(dataJSON.list);
					}
				}
				else{
					dataJSON.list = [];
				}
				dataJSONFilter.list = dataJSON.list.slice(0, itemsFirstLoad);
				buildHtml(true, dataJSONFilter);
			};
			reload();
		}
	};
	promotionPage();
};
