/**
 * @name SIA
 * @description Define global specialAssistance functions
 * @version 1.0
 */
SIA.specialAssistance = function() {
	// var disabledValueEls = $('[data-disable-value]');
	var requiredChbGroup = $('[data-rule-chbsrequired]');

	requiredChbGroup.off('change.validateChb').on('change.validateChb', function() {
		$(this).valid();
	});

	$.validator.addMethod('chbsRequired', function(value, el, param) {
		var chbs = $(param);
		return chbs.length && chbs.is(':checked');
	}, L10n.validator.selectOneOpt);

// -------------------- BEGIN UPLOAD FILE MODULE -------------------------------

	var attachList = $('.list-attachment');
	var attachEl = $('.attachment');
	var attachError = attachEl.find('.text-error');
	var chooseFileEl = $('.choose-file');
	var unsupportHtml5Tpl = '<li>{0}<a href="#"> delete</a></li>';
	var errorTpl = '<p class="text-error">{0}</p>';
	var pattern = /(jpg|jpeg|png|gif|tif|tiff|doc|docx|pdf|txt|zip)$/i;

	// $('[data-support-html5="false"]').removeClass('hidden');
	chooseFileEl.find('.chose-img-name').text('');

	var attachFunction = function(){
		attachEl
			.off('change.chooseFile')
			.on('change.chooseFile', 'input:file', function() {
				attachError.remove();

				if (this.value && this.value.match(/[^\/\\]+$/).length) {
					var fileName = this.value.match(/[^\/\\]+$/)[0];
					var fileExtension = fileName.split('.').pop().toLowerCase();

					if (pattern.test(fileExtension)) {
						attachList.prepend(unsupportHtml5Tpl.format(fileName));
						chooseFileEl.addClass('hidden');
						attachEl.off('change.chooseFile');
					}
					else {
						attachError = $(errorTpl.format('Not supported file format')).insertAfter(attachList);
						this.value = '';
					}
				}
			});
	};

	attachEl
		.off('click.removeFile')
		.on('click.removeFile', 'a, .attachment-close', function(e) {
			e.preventDefault();
			$(this).parent().remove();
			attachError.remove();

			chooseFileEl.find('input:file').val('');
			chooseFileEl.removeClass('hidden');
			attachFunction();
		});
		// .off('change.chooseFile')
		// .on('change.chooseFile', 'input:file', function() {
		// 	attachError.remove();

		// 	if (this.value && this.value.match(/[^\/\\]+$/).length) {
		// 		var fileName = this.value.match(/[^\/\\]+$/)[0];
		// 		var fileExtension = fileName.split('.').pop().toLowerCase();

		// 		if (pattern.test(fileExtension)) {
		// 			attachList.prepend(unsupportHtml5Tpl.format(fileName));
		// 			chooseFileEl.addClass('hidden');
		// 		}
		// 		else {
		// 			attachError = $(errorTpl.format('Not supported file format')).insertAfter(attachList);
		// 			this.value = '';
		// 		}
		// 	}
		// });
	attachFunction();

// ---------------------- END UPLOAD FILE MODULE -------------------------------
};
