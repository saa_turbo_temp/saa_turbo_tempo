SIA.staticContentKrisflyer = function(){
	var global = SIA.global,
			win = global.vars.win;
			// config = global.config;
			// totalTabletSlide = 3,
			// timerDetectSlider;

	// banner slider
	var bannerSlider = $('#banner-slider');
	if(bannerSlider.length){
		var imgs = bannerSlider.find('img.img-main');
		var loadBackgroundBanner = function(self, idx){
			// self.closest('.slide-item').css({
			// 	'background-image': 'url(' + self.attr('src') + ')'
			// });
			// self.attr('src', config.imgSrc.transparent);
			if(idx === imgs.length - 1){
				bannerSlider.find('.loading').hide();
				bannerSlider.css('visibility', 'visible');
				bannerSlider.find('.slides')
					.slick({
						siaCustomisations: true,
						dots: true,
						draggable: true,
						infinite: true,
						arrows: false,
						speed: 500,
						fade: true,
						autoplay: false,
						pauseOnHover: false,
						accessibility: false,
						slide: 'div',
						cssEase: 'linear'
					});
			}
		};
		imgs.each(function(idx) {
			var self = $(this);
			var nI = new Image();
			nI.onload = function(){
				loadBackgroundBanner(self, idx);
			};
			nI.src = self.attr('src');
		});
	}

	//highlight slider
	var highlightSlider = $('#highlight-slider');
	if(highlightSlider.length){
		var wrapperHLS = highlightSlider.parent();
		var imgsHl = highlightSlider.find('img');
		var loadBackgroundHighlight = function(self, parentSelt, idx){
			if(idx === imgsHl.length - 1){
				highlightSlider.width(wrapperHLS.width() + 22);

				highlightSlider.css('visibility', 'visible');
				highlightSlider.find('.slides')
					.slick({
						siaCustomisations: true,
						dots: true,
						speed: 300,
						draggable: true,
						slidesToShow: 4,
						slidesToScroll: 4,
						accessibility: false,
						autoplay: false,
						pauseOnHover: false
					});
				win.off('resize.highlightSlider').on('resize.highlightSlider',function() {
					highlightSlider.width(wrapperHLS.width() + 22);
				}).trigger('resize.highlightSlider');
			}
		};

		imgsHl.each(function(idx) {
			var self = $(this);
			var parentSelt = self.parent();
			var nI = new Image();
			nI.onload = function(){
				loadBackgroundHighlight(self, parentSelt, idx);
			};
			nI.src = self.attr('src');
		});
	}

	// win.off('resize.tabletSlider').on('resize.tabletSlider',function() {
	// 	clearTimeout(timerDetectSlider);
	// 	timerDetectSlider = setTimeout(function() {
	// 		if(window.innerWidth <= config.tablet) {
	// 			$('[data-tablet-slider] .slides').each(function() {
	// 				var slider = $(this);
	// 				if(slider.hasClass('slick-initialized')) {
	// 					slider.slick('unslick');
	// 				}

	// 				slider.slick({
	// 					dots: true,
	// 					speed: 300,
	// 					draggable: true,
	// 					slidesToShow: totalTabletSlide,
	// 					slidesToScroll: totalTabletSlide,
	// 					accessibility: false,
	// 					arrows: false,
	// 				});
	// 			});
	// 		}
	// 	}, 400);
	// }).trigger('resize.tabletSlider');
};
