/**
 * @name SIA
 * @description Define global accordion functions
 * @version 1.0
 */

SIA.accordion = function(){
	var vars = SIA.global.vars;
	var win = vars.win;
	var body = vars.body;
	var htmlBody = $('html, body');
	var timeAnimate = 400;
	var flyingFocus = $('#flying-focus');
	// This function use for init accordion
	var initAccordion = function(){
		var bookingNav = $('[data-fixed].sidebar');
		var contentWrapper = $('[data-accordion-wrapper=1]');
		var	firstTriggerAccordion = '[data-accordion-trigger=1]',
			secondTriggerAccordion = '[data-accordion-trigger=2]',
			firstContentAccordion = '[data-accordion-content=1]',
			secondContentAccordion = '[data-accordion-content=2]';

		if(contentWrapper.find('[data-accordion=1]').length){
			contentWrapper.each(function(){
				var self = $(this);
				var openAllAccordion = $('.open-all-btn', self);
				var $firstTriggerAccordion = $(firstTriggerAccordion, self),
					$secondTriggerAccordion = $(secondTriggerAccordion, self);
				var animateHtmlBody = function(trigger){
					if(trigger.offset().top === win.scrollTop()){
						return;
					}
					htmlBody.stop().animate({
						scrollTop: trigger.offset().top
					}, timeAnimate, function(){
						flyingFocus = $('#flying-focus');
						if(flyingFocus.length){
							flyingFocus.remove();
						}

						// fix issue for set window.location.hash faqs pages
						if (body.hasClass('faqs-pages')) {
							if (trigger.hasClass('open-all-btn')) {
								window.location.hash = '';
							}
							else {
								trigger.trigger('setHash.faqsPages');
							}
						}
					});
				};
				var scrollTop = -1;
				var clearTimerUpdate = null;
				var dT = 300;

				var autoUpdateScrollTop = function(trigger){
					clearTimerUpdate = setTimeout(function(){
						if(scrollTop === trigger.offset().top){
							animateHtmlBody(trigger);
							scrollTop = -1;
							clearTimeout(clearTimerUpdate);
						}
						else {
							scrollTop = trigger.offset().top;
							autoUpdateScrollTop(trigger);
						}
					}, dT);
				};

				openAllAccordion.off('click.openAllAccordion').on('click.openAllAccordion', function(e){
					e.preventDefault();

					if(openAllAccordion.hasClass('open')){
						if($firstTriggerAccordion.length){
							$firstTriggerAccordion.siblings(firstContentAccordion).stop().slideUp(400);
						}
						if($secondTriggerAccordion.length){
							$secondTriggerAccordion.filter('.active').removeClass('active').siblings(secondContentAccordion).slideUp(400);
						}
						openAllAccordion.removeClass('open active').html(L10n.accordion.open);
					}
					else{
						if($firstTriggerAccordion.length){
							$firstTriggerAccordion.not('.active').siblings(firstContentAccordion).stop().slideDown(400);
							$firstTriggerAccordion.filter('.active').removeClass('active');
						}
						if($secondTriggerAccordion.length){
							$secondTriggerAccordion.not('.active').addClass('active').siblings(secondContentAccordion).slideDown(400);
						}
						animateHtmlBody(openAllAccordion);
						openAllAccordion.addClass('open active').html(L10n.accordion.collapse);
					}
				});

				$firstTriggerAccordion.off('beforeAccordion.refeshOpenAll').on('beforeAccordion.refeshOpenAll', function(){
					var self = $(this);
					if(openAllAccordion.hasClass('open')){
						$firstTriggerAccordion.not(self).addClass('active');
						openAllAccordion.removeClass('open').html(L10n.accordion.open);
						autoUpdateScrollTop(self);
					}
				});

				if(self.data('accordion')){
					self.accordion('refresh');
				}
				else{
					self.accordion({
						wrapper: '[data-accordion-wrapper-content=1]',
						triggerAccordion: firstTriggerAccordion,
						contentAccordion: firstContentAccordion,
						activeClass: 'active',
						duration: timeAnimate,
						beforeAccordion: function(trigger, content){
							var sld = content.find('.slides');
							if(sld.length){
								if(!sld.find('.slide-item').width()){
									var slideWidth = (sld.closest('.accordion__content').parent().width() / 2) - 50;
									sld.find('.slick-track').width(100000);
									sld.find('.slide-item').width(slideWidth);
								}
								// sld.slickGoTo(sld.slickCurrentSlide());
								if($('body').hasClass('add-ons-1-landing-page')){
									// sld.slick('slickGoTo', sld.slick(''));
								}else{
									sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));
								}
							}

							// if (trigger.hasClass('active') && openAllAccordion.hasClass('open')){
							// 	openAllAccordion.removeClass('open').text(L10n.accordion.open);
							// 	$firstTriggerAccordion.not(trigger).trigger('click.accordion');
							// }
						},
						afterAccordion: function(trigger){
							if(trigger.hasClass('active') && !body.is('.promotion-enhancement')){
								animateHtmlBody(trigger);

								// fix issue for set window.location.hash faqs pages
								trigger.trigger('setHash.faqsPages');
							}
							// fix issue for block scroll of faqs-pages
							win.trigger('scroll.blockScroll');

							if(bookingNav.length){
								win.trigger('scroll.sticky');
							}
						}
					});
				}
			});

		}
		var accordionContentInfo = $('[data-accordion-wrapper=2]');
		if(accordionContentInfo.find('[data-accordion=2]').length){

			var animateHtmlBody = function(trigger){
				if(trigger.offset().top === win.scrollTop()){
					return;
				}
				htmlBody.stop().animate({
					scrollTop: trigger.offset().top
				}, timeAnimate);
			};

			if(accordionContentInfo.data('accordion')){
				accordionContentInfo.accordion('refresh');
			}
			else{
				accordionContentInfo.accordion({
					wrapper: '[data-accordion-wrapper-content=2]',
					triggerAccordion: secondTriggerAccordion,
					contentAccordion: secondContentAccordion,
					activeClass: 'active',
					duration: timeAnimate,
					beforeAccordion: function(trigger, content){
						var sld = trigger.find('.slides');
						if(sld.length){
							if(sld.find('.slide-item').width() < 100){
								var slideWidth = (sld.closest('.accordion__control').width() / 2) - 44;
								sld.find('.slick-track').width(100000);
								sld.find('.slide-item').width(slideWidth);

							}
							sld.slick('slickGoTo', sld.slick('slickCurrentSlide'));

						}

					},
					afterAccordion: function(trigger){
						if(trigger.hasClass('active')){
							animateHtmlBody(trigger);
						}

					}
				});
			}
		}
	};

	initAccordion();
	SIA.accordion.initAccordion = initAccordion;
};
