SIA.KFClaimMissingMiles = function(){
	var global = SIA.global,
			selectflight = $('[data-change-flight]'),
			claimForm = $('[data-claim-form]'),
			btnAddFlight = $('[data-btn-add-flight]'),
			claimPopup = $('.popup--success-claim'),
			confirmPopup = $('.popup--confirm-email-address'),
			formConfirm = confirmPopup.find('form'),
			arrFlight = [],
			activeFlight;

	var replaceFieldInfo = function(el, newRowNumber) {
		var newId = el.attr('id') + '-' + newRowNumber,
				arrRule = el.data('ruleValidatedate');
		el.siblings('label').attr('for', newId);
		el.attr('id', newId).attr('name', newId);
		if(arrRule) {
			var newArrRule = [];
			for(var i = 0, maxLength = arrRule.length; i < maxLength; i++) {
				newArrRule.push(arrRule[i] + '-' + newRowNumber);
			}
			el.data('ruleValidatedate', newArrRule);
		}
	};

	var addNewRow = function(el, newRowNumber) {
		var fieldEl = el.find(':input');
		fieldEl.each(function() {
			var self = $(this),
					customSelectEl = self.parent('[data-customselect]');
			replaceFieldInfo(self, newRowNumber);
			if(self.is('select') && customSelectEl.length) {
				customSelectEl.find('a').remove();
				customSelectEl.customSelect();
			}
		});
		return el.removeClass('hidden');
	};

	var controlCloseButton = function(el) {
		if(el.length <= 1) {
			el.find('[data-remove-flight]').addClass('hidden');
		} else {
			el.find('[data-remove-flight]').removeClass('hidden');
		}
	};

	claimPopup.Popup({
		overlayBGTemplate: '<div class="overlay"></div>',
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close]'
	});

	confirmPopup.Popup({
		overlayBGTemplate: '<div class="overlay"></div>',
		modalShowClass: '',
		triggerCloseModal: '.popup__close, [data-close]'
	});

	formConfirm.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess,
		invalidHandler: global.vars.invalidHandler,
		submitHandler: function() {
			confirmPopup.Popup('hide');
			claimPopup.Popup('show');
			return false;
		}
	});

	claimForm.data('validator').settings.submitHandler = function() {
		confirmPopup.Popup('show');
		return false;
	};

	$('[data-field-type]').each(function(index) {
		var flightNumber = index + 1;
		arrFlight[flightNumber] = $('[data-field-type="' + flightNumber + '"]').clone();
	});

	selectflight.off('change.selectFlight').on('change.selectFlight', function() {
		var listFlight = $('[data-field-type]');
		activeFlight = $('[data-field-type="' + this.value + '"]');
		activeFlight.data('minFlightId', activeFlight.data('minFlightId') || activeFlight.length);
		listFlight.addClass('hidden');
		activeFlight.removeClass('hidden');
		controlCloseButton(activeFlight);
	}).trigger('change.selectFlight');

	claimForm.off('click.removeFlight').on('click.removeFlight', '[data-remove-flight]', function(e) {
		e.preventDefault();
		var removeEl = $(this).parents('[data-field-type]'),
				currentIndex = removeEl.data('fieldType'),
				listNextRows = removeEl.nextAll().not(':hidden'),
				totalNextRows = listNextRows.length,
				totalRows;
		removeEl.remove();
		activeFlight = $('[data-field-type="' + currentIndex + '"]');
		controlCloseButton(activeFlight);
		totalRows = activeFlight.length;
		if(totalNextRows) {
			var j = totalRows;
			for(var i = totalNextRows - 1; i >= 0; i--) {
				listNextRows.eq(i).find('[data-title-order]').text(j);
				j--;
			}
		}
	});

	btnAddFlight.off('click.addFlight').on('click.addFlight', function() {
		var currentIndex = activeFlight.data('fieldType'),
				newRow;
		if(arrFlight[currentIndex]) {
			newRow = addNewRow(arrFlight[currentIndex].clone(), activeFlight.data('minFlightId'));
			newRow.find('[data-title-order]').text(activeFlight.length + 1);
			activeFlight.last().after(newRow);
			activeFlight = $('[data-field-type="' + currentIndex + '"]');
			activeFlight.data('minFlightId', activeFlight.data('minFlightId') + 1);
			controlCloseButton(activeFlight);
			SIA.initCorrectDate();
			SIA.autocompleteCountryCity();
		}
	});
};
