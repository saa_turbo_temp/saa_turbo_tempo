/**
 * @name SIA
 * @description Define global landingSearchFlight functions
 * @version 1.0
 */

SIA.tab = function() {
	var radioEls = $('[data-radio-tab]');
	var tabEls = $('[data-radio-tab-content]');
	var labelEls = radioEls.siblings('label');

	radioEls.off('change.tab').on('change.tab', function() {
		var checkedEl = radioEls.filter(':checked');
		var dataTabVal = checkedEl.data('radio-tab');
		var activeTab = tabEls.filter(function() {
			var tab = $(this);
			return tab.data('radio-tab-content') === dataTabVal;
		});

		tabEls.removeClass('active');
		activeTab.addClass('active');
	});

	labelEls.off('click.tab').on('click.tab', function() {
		radioEls.trigger('change.tab');
	});
};
