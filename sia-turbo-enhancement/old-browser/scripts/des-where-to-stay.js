/**
 * @name SIA
 * @description Define global desWhereTo functions
 * @version 1.0
 */
SIA.desWhereTo = function(){
	/*var initIsotope = function(){
		var destinationBlock = $('.where-destination .where-destination__inner');
		var item = destinationBlock.children('.where-destination__item');
		var thumb = item.find('.place-3__thumb img');
		var colCount = 0;
		var colWidth = 0;
		var windowWidth = 0;
		var blocks = [];
		var resizeTimer = null;
		var delayResize = 200;
		if(!item.length){
			return;
		}
		var setupBlocks = function() {
			blocks = [];
			windowWidth = destinationBlock.outerWidth();
			colWidth = item.outerWidth();
			colCount = Math.round(windowWidth/(colWidth));
			for(var i=0; i < colCount; i++) {
				blocks.push({
					top: 0,
					item: $()
				});
			}
			positionBlocks();
		};
		var getIndex = function(){
			var min = 10000000;
			var index = 0;
			for(var i=0; i < blocks.length; i++) {
				if(min > blocks[i].top){
					min = blocks[i].top;
					index = i;
				}
			}
			return index;
		};
		// var getMax = function(){
		// 	var min = 0;
		// 	var index = 0;
		// 	for(var i=0; i < blocks.length; i++) {
		// 		if(min < blocks[i].top){
		// 			min = blocks[i].top;
		// 			index = i;
		// 		}
		// 	}
		// 	return index;
		// };

		var getHeighest = function(){
			var h = 0;
			for(var i=0; i < blocks.length; i++) {
				if(blocks[i].top > h){
					h = blocks[i].top;
				}
			}
			return h;
		};

		var positionBlocks = function() {
			item.each(function(idx){
				var self = $(this);
				// var row = parseInt(idx/colCount);
				var idxBlk = getIndex();
				// var max = getMax();
				var l = 0;
				var t = 0;
				if(blocks[idxBlk].item.length){
					l = blocks[idxBlk].item.position().left;
					t = blocks[idxBlk].item.position().top + blocks[idxBlk].item.outerHeight(true);
					// float left to right
					if(t + 200 > blocks[0].top && (idx === item.length - 1)){
						l = blocks[0].item.position().left;
						t = blocks[0].item.position().top + blocks[0].item.outerHeight(true);
						idxBlk = 0;
					}
				}
				else{
					l = (idx*(colWidth));
					t = 0;
				}
				blocks[idxBlk].item = self;
				blocks[idxBlk].top = t + self.outerHeight(true);
				self.css({
					'position': 'absolute',
					'left': l,
					'top': t
				});
			});
			destinationBlock.css({
				'position': 'relative',
				'height': getHeighest()
			});
		};
		win.off('resize.sortItemWT').on('resize.sortItemWT', function(){
			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function(){
				setupBlocks();
			},delayResize);
		});
		thumb.each(function(idxI){
			var nI = new Image();
			nI.onload = function(){
				if(idxI === thumb.length - 1){
					setupBlocks();
					win.triggerHandler('resize.sortItemWT');
				}
			};
			nI.src = $(this).attr('src');
		});
	};*/

	// var initIsotope = function(){
	// 	var $container = $('.where-destination .where-destination__inner');
	// 	$container.isotope({
	// 		// options
	// 		itemSelector: '.place-3',
	// 		// layoutMode: 'fitRows',
	// 		animationEngine: 'css',
	// 		transitionDuration: 0
	// 	});
	// };

	var initShuffle = function() {
		var destinationBlock = $('.where-destination .where-destination__inner');
		// window.onload = function() {
		destinationBlock.shuffle({
			speed: 0,
			itemSelector: '.where-destination__item',
			sizer: destinationBlock.find('.where-destination__item').eq(1)
		});
		// };
	};

	var convertPhoneLink = function() {
		var phoneLinkEl = $('[data-phone-link]');

		if(phoneLinkEl.length) {
			phoneLinkEl.each(function() {
				var self = $(this),
					phoneUrl = self.prop('href'),
					skypeUrl = phoneUrl.replace(/tel/gi, 'callto');
				self.attr('href', skypeUrl);
			});
		}
	};

	var checkRadio = function(){
		var el   = $('.form-search-flight-des'),
		    radioEl = el.find('input:radio'),
		    target = el.find('[data-target]');
		radioEl.each(function(idx){
		  var self = $(this);
		  if(self.is(':checked')){
		    if(idx){
		      target.eq(1).removeClass('hidden');
		      target.eq(0).addClass('hidden');
		    }
		    else{
		      target.eq(0).removeClass('hidden');
		      target.eq(1).addClass('hidden');
		    }
		  }
		  self.off('change.staticWidget').on('change.staticWidget', function(){
		    if(self.is(':checked')){
		      if(idx){
		        target.eq(1).removeClass('hidden');
		        target.eq(0).addClass('hidden');
		      }
		      else{
		        target.eq(0).removeClass('hidden');
		        target.eq(1).addClass('hidden');
		      }
		    }
		  });
		});
	};

	var initModule = function(){
		// initIsotope();
		initShuffle();
		convertPhoneLink();
		checkRadio();
	};

	initModule();
};
