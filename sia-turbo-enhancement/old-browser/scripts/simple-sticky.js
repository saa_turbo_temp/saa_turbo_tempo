/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.simpleSticky = function() {
	var global = SIA.global;
	var body = $('body');

	var appendTemplate = function() {
		$.get(global.config.url.simpleStickyTemplate, function(data) {
			var template = window._.template(data, {
				data: {}
			});
			var templateEl = $(template);
			body.append(templateEl);
		});
	};
	appendTemplate();
};
