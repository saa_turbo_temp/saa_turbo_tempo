/**
 * @name SIA
 * @description Define global sshHotel functions
 * @version 1.0
 */
SIA.sshHotel = function(){
	var global = SIA.global;
	var config = global.config;

	// var hotelSortFilter = function(){
	// 	var getData = function(){
	// 		$.ajax({
	// 			url: config.url.ssh.hotel.json,
	// 			dataType: 'json',
	// 			type: global.config.ajaxMethod,
	// 			beforeSend: function() {
	// 			},
	// 			success: function(data) {
	// 				if(data){
	// 					getTemplate(data);
	// 				}
	// 			},
	// 			error: function(xhr, status) {
	// 				if(status !== 'abort') {
	// 					window.alert(L10n.flightSelect.errorGettingData);
	// 				}
	// 			}
	// 		});
	// 	};
	// 	var getTemplate = function(data){
	// 		$.get(config.url.ssh.hotel.template, function (tpl) {
	// 			hotelTemplateView(tpl, data);
	// 		});
	// 	};

	// 	var hotelTemplateView = function(html, data){
	// 		var slider = $('#slider-range');
	// 		var labelFrom = slider.find('.ui-slider_from');
	// 		var labelTo = slider.find('.ui-slider_to');
	// 		var inputFrom = slider.find('[name="from-price"]');
	// 		var	inputTo = slider.find('[name="to-price"]');
	// 		var initValue = [slider.data('current-min'), slider.data('current-max')];
	// 		var listHotel = $('.list-hotel');
	// 		var seaMoreBtn = listHotel.find('.hotel-item-btn [data-see-more]');
	// 		var seaMoreBtnWrapper = seaMoreBtn.parent();
	// 		var location = $('#location');
	// 		var customLocation = location.closest('[data-customselect]');
	// 		var sort = $('#custom-select--sort-1');
	// 		var customSort = sort.closest('[data-customselect]');
	// 		var firstView = 5;
	// 		var nextView = 3;
	// 		var filteredData = [];
	// 		var newData = [];
	// 		var holderUI = [];
	// 		var ratingBlock = $('.rating-block');
	// 		var rate = ratingBlock.children();
	// 		var startRating = ratingBlock.data('start-rate');
	// 		var slideFrom = initValue[0];
	// 		var slideTo = initValue[1];

	// 		var renderTemplate = function(data){
	// 			var template = window._.template(html, {
	// 				data: data
	// 			});
	// 			if (!$(template).length) {
	// 				listHotel.addClass('hidden');
	// 				customSort.addClass('hidden');
	// 			}
	// 			else {
	// 				$(template).insertBefore(seaMoreBtnWrapper);

	// 				if (listHotel.hasClass('hidden')) {
	// 					listHotel.removeClass('hidden');
	// 				}
	// 				if (customSort.hasClass('hidden')) {
	// 					customSort.removeClass('hidden');
	// 				}
	// 			}
	// 		};

	// 		var filterData = function(l , f , t, r){
	// 			firstView = 5;
	// 			filteredData = window._.filter(data, function(i){
	// 				return parseFloat(window.accounting.unformat(i.price)) >= f && parseFloat(window.accounting.unformat(i.price)) <= t && l === i.location && parseFloat(i.rate) <= r;
	// 			}).sort(function(a, b) {
	// 				return b.rate - a.rate;
	// 			});
	// 		};

	// 		var firstRender = function(){
	// 			newData = filteredData.slice(0, firstView);
	// 			renderTemplate(newData);
	// 		};

	// 		var renderDisplay = function(f, t, r){
	// 			filterData(location.val(), f, t, r);
	// 			firstRender();
	// 			if(firstView > filteredData.length){
	// 				seaMoreBtn.hide();
	// 			}
	// 			else{
	// 				seaMoreBtn.show();
	// 			}
	// 		};

	// 		var starRating = function(){
	// 			startRating = ratingBlock.data('start-rate');
	// 			var chooseRate = function(){
	// 				rate.removeClass('rated');
	// 				rate.each(function(i){
	// 					var self = $(this);
	// 					if(i < startRating){
	// 						self.addClass('rated');
	// 					}
	// 				});
	// 				listHotel.children('.hotel-item').remove();
	// 				renderDisplay(slideFrom, slideTo, startRating);
	// 			};
	// 			rate.each(function(i){
	// 				$(this).off('click.rate').on('click.rate', function(e){
	// 					e.preventDefault();
	// 					startRating = i + 1;
	// 					chooseRate();
	// 				});
	// 			});
	// 			chooseRate();
	// 		};
	// 		starRating();

	// 		//init slide
	// 		slider.slider({
	// 			range: true,
	// 			min: 1000,
	// 			max: 4000,
	// 			values: initValue,
	// 			create: function() {
	// 				labelFrom.text(accounting.formatMoney(initValue[0], '', 0, ',', '.'));
	// 				labelTo.text(accounting.formatMoney(initValue[1], '', 0, ',', '.'));
	// 			},
	// 			slide: function(event, ui) {
	// 				slideFrom = ui.values[0];
	// 				slideTo = ui.values[1];
	// 				labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
	// 				labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
	// 				inputFrom.val(slideFrom);
	// 				inputTo.val(slideTo);
	// 			},
	// 			stop: function( event, ui ){
	// 				slideFrom = ui.values[0];
	// 				slideTo = ui.values[1];
	// 				holderUI = [slideFrom, slideTo];
	// 				listHotel.children('.hotel-item').remove();
	// 				renderDisplay(slideFrom, slideTo, startRating);
	// 			}
	// 		});

	// 		// render hotel
	// 		listHotel.children('.hotel-item').remove();
	// 		holderUI = [initValue[0], initValue[1]];
	// 		renderDisplay(initValue[0], initValue[1], startRating);

	// 		// see more
	// 		seaMoreBtn.off('click.seeMore').on('click.seeMore', function(e){
	// 			e.preventDefault();
	// 			newData = filteredData.slice(firstView, firstView + nextView);
	// 			firstView +=nextView;
	// 			renderTemplate(newData);
	// 			if(firstView > filteredData.length){
	// 				seaMoreBtn.hide();
	// 			}
	// 		});

	// 		// after select
	// 		// location
	// 		customLocation.off('afterSelect.filterLocation').on('afterSelect.filterLocation', function(){
	// 			listHotel.children('.hotel-item').remove();
	// 			renderDisplay(holderUI[0], holderUI[1], startRating);
	// 		});
	// 		// sort
	// 		customSort.off('afterSelect.filterSort').on('afterSelect.filterSort', function(){
	// 			listHotel.children('.hotel-item').remove();
	// 			if(sort.val() === 'price-asc'){
	// 				filteredData = filteredData.sort(function(a, b){
	// 					var aPrice = parseInt(a.price.replace(/\,/g, ''), 10),
	// 							bPrice = parseInt(b.price.replace(/\,/g, ''), 10);
	// 					return aPrice - bPrice;
	// 				});
	// 				firstRender();
	// 			}
	// 			else if(sort.val() === 'star-rating-asc'){
	// 				filteredData = filteredData.sort(function(a, b){
	// 					return a.rate - b.rate;
	// 				});
	// 				firstRender();
	// 			}
	// 			else if(sort.val() === 'star-rating-desc'){
	// 				filteredData = filteredData.sort(function(a, b){
	// 					return b.rate - a.rate;
	// 				});
	// 				firstRender();
	// 			}
	// 			else{
	// 				// renderDisplay(holderUI[0], holderUI[1]);
	// 				filteredData = filteredData.sort(function(a, b){
	// 					if(a.package > b.package) {
	// 						return 1;
	// 					}
	// 					if(a.package < b.package) {
	// 						return -1;
	// 					}
	// 					return 0;
	// 				});
	// 				firstRender();
	// 			}
	// 		}).trigger('afterSelect.filterSort');
	// 	};

	// 	getData();
	// };

	var listHotel = $('.list-hotel');
	var hotelItems = listHotel.find('.hotel-item');
	var sortPriceAsc = $('[data-value = "price-asc"]');
	var sortRateAsc = $('[data-value = "star-rating-asc"]');
	var sortRateDesc = $('[data-value = "star-rating-desc"]');
	var hotelItemBtn = $('.hotel-item-btn');
	var seeMoreBtn = $('.see-more-btn');
	var slider = $('#slider-range');
	var labelFrom = slider.find('.ui-slider_from');
	var labelTo = slider.find('.ui-slider_to');
	var inputFrom = slider.find('[name="from-price"]');
	var inputTo = slider.find('[name="to-price"]');
	var initValue = [slider.data('current-min'), slider.data('current-max')];
	var ratingBlock = $('.rating-block');
	var startRating = ratingBlock.data('start-rate');
	var slideFrom = initValue[0];
	var slideTo = initValue[1];
	var holderUI = [];
	var filteredItems;
	var win = $(window);

	var removeAccordion = function(){
		$('.hotel-item__info').removeAttr('data-accordion-content');
		$('.hotel-item__info').removeAttr('style');
	};

	win.on('resize.sshHotel', function(){
		if(window.innerWidth < config.mobile) {
			SIA.accordion.initAccordion();
		}else{
			$('.hotel-item__info').removeAttr('style');
		}
	});
	removeAccordion();

	slider.slider({
		range: true,
		min: 1000,
		max: 4000,
		values: initValue,
		create: function() {
			labelFrom.text(accounting.formatMoney(initValue[0], '', 0, ',', '.'));
			labelTo.text(accounting.formatMoney(initValue[1], '', 0, ',', '.'));
		},
		slide: function(event, ui) {
			slideFrom = ui.values[0];
			slideTo = ui.values[1];
			labelFrom.text(accounting.formatMoney(slideFrom, '', 0, ',', '.'));
			labelTo.text(accounting.formatMoney(slideTo, '', 0, ',', '.'));
			inputFrom.val(slideFrom);
			inputTo.val(slideTo);
		},
		stop: function(event, ui) {
			slideFrom = ui.values[0];
			slideTo = ui.values[1];
			holderUI = [slideFrom, slideTo];
			filterHotelByPrice();
		}
	});

	var starRating = function() {
		var rate = $('[data-start-rate]').children();
		startRating = ratingBlock.data('start-rate');
		var chooseRate = function() {
			rate.removeClass('rated');
			rate.each(function(i) {
				var self = $(this);
				if (i < startRating) {
					self.addClass('rated');
				}
			});
		};
		rate.each(function(i) {
			$(this).off('click.rate').on('click.rate', function(e) {
				e.preventDefault();
				startRating = i + 1;
				chooseRate();
				filterHotelByPrice();
			});
		});
		chooseRate();
	};

	var handleSeeMoreBtn = function(){
		if(filteredItems){
			if(filteredItems.length > 5){
				seeMoreBtn.removeClass('hidden');
				filteredItems.addClass('hidden');
				filteredItems.slice(0,5).removeClass('hidden');
			}else{
				seeMoreBtn.addClass('hidden');
			}
			seeMoreBtn.on('click.sshHotel', function(e){
				e.preventDefault();
				var hiddenItems = filteredItems.filter(':hidden');
				var numberOfHiddenItems = hiddenItems.length;
				hiddenItems.slice(0,(numberOfHiddenItems > 5) ? 5 : numberOfHiddenItems).removeClass('hidden');
				hiddenItems = filteredItems.filter(':hidden');
				if(hiddenItems.length === 0){
					seeMoreBtn.addClass('hidden');
				}
			});
		}
	};

	var filterHotelByPrice = function() {
		hotelItems = listHotel.find('.hotel-item');
		var minPrice = parseInt(labelFrom.text().replace(',', ''));
		var maxPrice = parseInt(labelTo.text().replace(',', ''));
		var hotelItemFiltered = hotelItems.filter(function() {
			var val = parseInt($(this).data('price').replace(',', ''));
			return val >= minPrice && val <= maxPrice;
		});
		if(hotelItemFiltered.length > 0){
			$('.list-value-sort').removeClass('hidden');
			$('.list-hotel').removeClass('hidden');
		}
		hotelItems.addClass('hidden');
		hotelItemFiltered.removeClass('hidden');
		filterHotelByRating();
	};

	var filterHotelByRating = function() {
		hotelItems = listHotel.find('.hotel-item').not(':hidden');
		var rateFilter = $('[data-start-rate]').children().filter('.rated').length;
		var attrName = 'rate';
		var hotelItemFiltered = hotelItems.filter(function() {
			var rate = $(this).data('rate');
			return rate <= rateFilter;
		});
		hotelItems.addClass('hidden');
		hotelItemFiltered.removeClass('hidden');
		filteredItems = hotelItemFiltered;
		handleSeeMoreBtn();
		sortData(attrName, true);
	};

	sortPriceAsc.off('click.sshHotel').on('click.sshHotel', function() {
		var attrName = 'price';
		sortData(attrName);
	});

	sortRateAsc.off('click.sshHotel').on('click.sshHotel', function() {
		var attrName = 'rate';
		sortData(attrName);
	});

	sortRateDesc.off('click.sshHotel').on('click.sshHotel', function() {
		var attrName = 'rate';
		sortData(attrName, true);
	});

	function sortData(attrName, isDesc) {
		if(!filteredItems){
			return;
		}

		isDesc = isDesc || false;
		var currentLength = filteredItems.not(':hidden').length;
		filteredItems.removeClass('hidden');
		var elArr = [];
		var dataVals = getValues.call(filteredItems, attrName);
		var dataValsSorted = dataVals.sort(function(a, b) {
			return a - b;
		});
		var len = dataValsSorted.length;
		filteredItems.detach();

		for (var i = 0; i < len; i++) {
			for (var j = 0; j < len; j++) {
				var item = filteredItems.eq(j).not('.sorted');
				if(parseInt(item.length > 0 && item.attr('data-' + attrName).replace(',', '')) === dataValsSorted[i]){
					item.addClass('sorted');
					elArr.push(item);
				}
			}
		}

		listHotel.append(isDesc ? elArr.reverse() : elArr);
		$('.sorted').removeClass('sorted');
		hotelItemBtn.detach();
		listHotel.append(hotelItemBtn);
		filteredItems.addClass('hidden');
		filteredItems.slice(0,currentLength).removeClass('hidden');
		if(filteredItems.slice(0,currentLength).length === 0){
			$('.list-value-sort').addClass('hidden');
			$('.list-hotel').addClass('hidden');
		}else{
			$('.list-value-sort').removeClass('hidden');
			$('.list-hotel').removeClass('hidden');
		}
	}

	var getValues = function(attrName) {
		var len = this.length;
		var arr = [];
		for (var i = 0; i < len; i++) {
			arr.push(parseInt(this.eq(i).attr('data-' + attrName).replace(',', '')));
		}
		return arr;
	};

	var initModule = function(){
		starRating();
		filterHotelByPrice();
		// if(true){
		// 	return;
		// }
		// hotelSortFilter();
	};

	initModule();
};
