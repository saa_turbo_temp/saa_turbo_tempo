'use strict';
/**
 * @name SIA
 * @description Define global variables and functions
 * @version 1.0
 */
var SIA = SIA || {};

SIA.global = (function() {

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	var paramUrl = sURLVariables[1];
	var sName = paramUrl ? paramUrl.split('=')[1] : '';
	var getURLParams = function(sParam) {
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1];
			}
		}
	};

	// configuration
	var config = {
		width: {
			docScroll: window.Modernizr.touch || window.navigator.msMaxTouchPoints ? 0 : 20,
			datepicker: 14
		},
		mobile: 768,
		tablet: 988,
		formatDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
		template: {
			overlay: '<div class="overlay"></div>',
			error: '<em class="ico-error">error</em>',
			// labelError: '<span class="text-error">Lorum ipsum lorum ipsum</span>',
			labelError: '<p class="text-error"><span>Lorum ipsum lorum ipsum</span></p>',
			success: '<em class="ico-success">success</em>',
			flags: '<img src="images/transparent.png" alt="" class="flags {0}">',
			addEmail: '<div data-validate-row="true" class="table-row"><div data-validate-col="true" class="table-col table-col-1"><span>{0}.</span></div><div data-validate-col="true" class="table-col table-col-2"><div class="table-inner"><label for="email-address-{0}" class="hidden">&nbsp;</label><span class="input-1"><input type="email" name="email-address-{0}" id="email-address-{0}" placeholder="Email address" value="" data-rule-email="true" data-msg-email="{1}"></span></div></div></div>',
			addEmailConfirm: '<div class="table-row"><div class="table-col table-col-1"><span>{0}.</span></div><div class="table-col table-col-2"><div class="table-inner"><label for="email-confirm-input-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"><input type="email" name="email-confirm-input-01" value="{1}" id="email-confirm-input-{0}" readonly="readonly"></span></div></div></div>',
			AddSMSSuccessfullySent: '<div class="table-row"> <div class="table-col table-col-1"><span>{0}.</span></div> <div class="table-col table-col-2"> <div class="table-inner"> <label for="country-name-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="text" name="country-name-{0}" value="{1}" id="country-name-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-3"> <div class="table-inner"> <label for="area-code-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="tel" name="area-code-{0}" value="{2}" id="area-code-{0}" readonly="readonly"></span> </div> </div> <div class="table-col table-col-4"> <div class="table-inner"> <label for="phone-number-{0}" class="hidden">&nbsp;</label><span class="input-1 disabled"> <input type="text" name="phone-number-{0}" value="{3}" id="phone-number-{0}" readonly="readonly"></span> </div> </div> </div>',
			loadingStatus: '<div class="loading-block"><img src="images/ajax-loader-ie.gif" alt="loading" longdesc="img-desc.html"></div>',
			loadingMedium: '<div class="loading loading--medium">Loading</div>',
			loadingSmall: '<div class="loading loading--small">Loading</div>'
		},
		zIndex: {
			datepicker: 1004,
			ppLanguage: 100,
			tabContentOverlay: 14,
			overlayPopup: 14
		},
		duration: {
			overlay: 400,
			menu: 400,
			languagePopup: 400,
			searchPopup: 400,
			newsTicker: {
				auto: 2000,
				animate: 1000
			},
			clearTimeout: 100,
			bookingWidget: 400,
			popupSearch: 400,
			flightStatus: 400,
			popupGesture: 400
		},
		imgSrc: {
			transparent: 'images/transparent.png'
		},
		url: {
			yourVoucherTpl: 'template/your-vouchers.tpl',
			aboutVoucher: 'template/vouchers-about.tpl',
			voucherAccountSummary: 'template/vouchers-account-summary.tpl',
			voucherFlightSegmentType: 'template/vouchers-flight-segment-type.tpl',
			voucherFlightSegmentTypeItems: 'template/vouchers-flight-segment-item-selection.tpl',
			voucherFlightSegmentTemplate: 'template/vouchers-flight-segment-item.tpl',
			checkVoucherAvailUrlFailue: 'ajax/voucher-selected-segment-list-json-response-failue.json',
			checkVoucherAvailUrl: 'ajax/voucher-selected-segment-list-json-response.json',
			paxFakeData: 'ajax/pax-fake-data.json',
			simpleStickyTemplate: 'template/simple-sticky.tpl',
			stickyTemplate: 'template/sticky.tpl',
			barTemplate: 'template/bar.tpl',
			flightHistoryTemplate: 'template/flight-history.tpl',
			addOnMbTemplate: 'template/add-on-mb.tpl',
			addOnMbTemplateSales: 'template/add-on-mb-sales.tpl',
			bookingDetailCarResponse : 'template/booking-detail-car-response.tpl',
			addOnCarSelected : 'template/add-on-car-selected.tpl',
			addOnCarTermAndonditions : 'template/add-on-car-term-and-conditions.tpl',
			addOnMbHotelTemplateSales: 'template/add-on-mb-hotel-sales.tpl',
			addOnAddedTemplateSales: 'template/add-on-added-sales.tpl',
			jsonAutocomplete: 'ajax/destinations-data.json',
			fareDealsJSON: 'ajax/fare-deals.json',
			templateFareDeal: 'template/fare-deals.tpl',
			cabinSelectJSON: 'ajax/cabin-data.json',
			flightStatusJSON: 'ajax/ice-flight-status.json',
			flightStatusTemplate: 'template/flight-status.tpl',
			flightScheduleJSON: 'ajax/flight-schedule.json',
			flightScheduleTemplate: 'template/flight-schedule.tpl',
			promotionPageKrisflyerTemplate: 'template/promotion-krisflyer-list.tpl',
			promotionPageKrisflyerJSON: 'ajax/promotion-krisflyer-list.json',
			promotionPageTemplate: 'template/promotion-list.tpl',
			promotionEnhancePageTemplate: 'template/promotion-enhance-list.tpl',
			promotionPageJSON: 'ajax/promotion-fare-list.json',
			promotionPageJSONSeaMore: 'ajax/promotion-fare-list-1.json',
			promotionPageJSONSearch: 'ajax/promotion-fare-list-search.json',
			languageJSON: 'ajax/autocomplete-language.json',
			social: {
				facebookSharing: 'https://www.facebook.com/sharer.php?u=',
				twitterSharing: 'https://twitter.com/share?url=',
				gplusSharing: 'https://plus.google.com/share?url='
			},
			CIBFlightSearch1Template: 'template/cib-flight-search.tpl',
			CIBFlightSearch1UpgradeTemplate: 'template/flight-upgrade.tpl',
			bookingDetailResponse: 'ajax/booking-details-response.json',
			addOnCarSelected : 'template/add-on-car-selected.tpl',
			flightSearchFare1JSON: 'ajax/cib-flight-search-fare-1.json',
			flightSearchFare2JSON: 'ajax/cib-flight-search-fare-2.json',
			yourVoucher: 'ajax/AccountSummarySampleJson_updated.json',
			flightSearchFareFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-search-fare-flight-info.json',
			flightSearchFare1UpgradeJSON: 'ajax/cib-flight-search-fare-1-upgrade.json',
			flightSearchFare2UpgradeJSON: 'ajax/cib-flight-search-fare-2-upgrade.json',
			kfTableTemplate: 'template/how-to-earn-kf-table.tpl',
			kfTableJSON: 'ajax/how-to-earn-kf-table.json',
			kfUseTableTemplate: 'template/how-to-use-kf-table.tpl',
			kfUseTableJSON: 'ajax/how-to-use-kf-table.json',
			kfAtAGlanceTemplate: 'template/kf-at-a-glance.tpl',
			kfAtAGlanceTemplate01: 'template/kf-at-a-glance-01.tpl',
			kfAtAGlanceTemplateButton: 'template/kf-at-a-glance-button.tpl',
			kfAtAGlanceTemplateSf: 'template/kf-at-a-glance-sf.tpl',
			// kfAtAGlanceJSON: 'ajax/kf-at-a-glance.json',
			kfMessageMarkJSON: 'ajax/kf-message-mark.json',
			kfMessageDeleteJSON: 'ajax/kf-message-delete.json',
			kfMessageItemsJSON: 'ajax/kf-message-items.json',
			kfMessageItemTemplate: 'template/kf-message-item.tpl',
			kfBookingUpcomingJSON: 'ajax/kf-booking-upcoming-flights.json',
			kfBookingUpcomingTemplate: 'template/kf-booking-upcoming-flights.tpl',
			kfFlightHistoryJSON: 'ajax/kf-booking-flight-history.json',
			kfFlightHistoryTemplate: 'template/kf-booking-flight-history.tpl',
			kfPastFlightHistoryTemplate: 'template/kf-booking-past-flight.tpl',
			kfCheckInJSON: 'ajax/kf-check-in.json',
			kfCheckInTemplate: 'template/kf-check-in.tpl',
			langToolbarTemplate: 'template/language-toolbar-old-browser.tpl',
			orbFlightResultTemplate: 'template/orb-flight-result.tpl',
			orbFlightResultJSON: 'ajax/orb-flight-result.json',
			orbFlightInfoJSON: getURLParams('fly') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-search-flight-info.json',
			orbWaitlistedFlightJSON: 'ajax/orb-waitlisted-flight.json',
			cibFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select.json',
			cibFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/cib-flight-select-on-change.json',
			orbFlightSelect: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select.json',
			orbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/orb-flight-select-on-change.json',
			mbFlightSelect: $('body').hasClass('mb-payments-excess-baggage-page') ? 'ajax/mb-flight-select-excess-baggage.json' : getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select.json',
			mbFlightSelectOnChange: getURLParams('bs') ? ('ajax/JSONS/' + sName) : 'ajax/mb-flight-select-on-change.json',
			kfNumberNomineeJSON: getURLParams('nominne') ? ('ajax/JSONS/' + sName) : 'ajax/kf-number-nominne.json',
			mb: {
				selectMeal: 'template/mb-select-meals.tpl',
				selectMealSidebar: 'template/mb-select-meals-sidebar.tpl',
				selectFlightMeal: 'template/mb-flight-select-meals.tpl',
				selectFlightMealSidebar: 'template/mb-flight-select-meals-sidebar.tpl',
				selectFlightMealPopupMenu: 'template/mb-flight-select-meals-popup.tpl'
			},
			cibBookingSummaryDetailsPopupTemplate: 'template/booking-summary-details-popup.tpl',
			cibBookingSummarySfDetailsPopupTemplate: 'template/booking-summary-details-popup-sf.tpl',
			orbBookingSummaryDetailsPopupTemplate: 'template/orb-booking-summary-details-popup.tpl',
			cibBookingSummaryDetailsPopupTemplateAddOn: 'template/booking-summary-details-popup-addon.tpl',
			bookingFlightConfirm: 'ajax/booking-flight-confirm.json',
			kfMilesTemplate: 'template/kf-miles.tpl',
			kfFavorite: 'template/kf-favourite.tpl',
			kfStatement: 'template/kf-statement.tpl',
			preferSeatContent: 'template/prefer-seat-content.tpl',
			kfSubBookingUpcomingFlights: 'template/kf-booking-upcoming-flights-01.tpl',
			kfExtendMileJSON: 'ajax/kf-extend-miles.json',
			orbFlightSchedule: 'template/orb-flight-schedule.tpl',
			sqcUpcomingFlightTemplate: 'template/sqc-upcoming-flight.tpl',
			sqcUpcomingFlightDetailTemplate: 'template/sqc-upcoming-flight-detail.tpl',
			sqcUserDelete: 'ajax/sqc-user-delete.json',
			sqcSalePointDelete: 'ajax/sqc-sale-point-delete.json',
			sqcCheckPIN: 'ajax/sqc-check-PIN.json',
			sqcSavedTripsTemplate: 'template/sqc-saved-trips.tpl',
			sqc: {
				atGlanceTemplate: 'template/sqc-at-a-glance.tpl',
				atGlanceDetailTemplate: 'template/sqc-at-a-glance-detail.tpl',
				atGlanceJSON: 'ajax/sqc-at-a-glance.json'
			},
			ssh: {
				hotel: {
					json: 'ajax/ssh-hotel.json',
					template: 'template/ssh-hotel.tpl'
				}
			},
			success: 'ajax/success.json',
			bookingDetailTemplate: 'template/booking-detail.tpl',
			bookingDetailMealTemplate: 'template/booking-detail-meal.tpl',
			bookingDetailBagTemplate: 'template/booking-detail-baggage.tpl',
			excessBaggageTemplate: 'template/booking-detail-excess-baggage.tpl',
			mealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-meal.json',
			mealFlightJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-meal.json',
			mealFlightPopupJSON: getURLParams('mealMenu') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-flight-popup-meal.json',
			manageBookingMealJSON: getURLParams('meal') ? ('ajax/JSONS/' + sName) : 'ajax/manage-booking-meal.json',
			excessMealJSON: 'ajax/booking-detail-excess-meal.json',
			baggageJSON: getURLParams('baggage') ? ('ajax/JSONS/' + sName) : 'ajax/booking-detail-baggage.json',
			excessBaggageJSON: 'ajax/booking-detail-excess-baggage.json',
			desEntryTemplate: 'template/des-entry.tpl',
			desEntryJSON: 'ajax/des-entry.json',
			staticMoviesTemplate: 'template/static-content-movies.tpl',
			staticMoviesJSON: 'ajax/static-content-movies.json',
			staticMusicTemplate: 'template/static-content-music.tpl',
			staticMusicJSON: 'ajax/static-content-music.json',
			kfVoucherRedemptionJSON: 'ajax/kf-voucher-redemption.json',
			kfVoucherSummaryJSON: 'ajax/how-to-use-kf-voucher.json',
			kfVoucherRedemptionTemplate: 'template/kf-voucher-redemption.tpl',
			kfVoucherRedemptionReviewTemplate: 'template/kf-voucher-redemption-review.tpl',
			videoLightbox: {
				youtube: 'template/youtube-template-lightbox.tpl',
				flowplayer: 'template/flowplayer-template-lightbox-old.tpl'
			},
			addBaggagePopup: {
				template: 'template/add-baggage-popup.tpl',
				json: 'ajax/add-baggage-popup.json'
			},
			promotionsPackages: {
				json: 'ajax/pormotions-packages.json',
				template: 'template/pormotions-packages.tpl'
			},
			faredealPromotion: {
				// faredealMultiPriceJson: 'ajax/faredeal-multiple-price.json',
				// faredealMultiPriceSeemoreJson: 'ajax/faredeal-multiple-price-seemore.json',
				// pricePointsJson: 'ajax/pormotions-price-points.json',
				bookingWidgetJson: 'ajax/booking-widget.json'
			},
			countryCityAutocomplete: 'ajax/country-city.json',
			addons: {
				hotel: {
					json: 'ajax/Hotel_Availability_Response.json',
                    jsonmb: 'ajax/Hotel_Availability_Response.json',
					template: 'template/add-on-hotel-list.tpl'
				},
				car: {
					templateSlider: 'template/add-on-car-slider.tpl',
					templateCar: 'template/add-on-car-list.tpl',
					templateCarAdded: 'template/add-on-car-added.tpl'
				}
			}
		},
		highlightSlider: {
			desktop: 4,
			tablet: 3,
			mobile: 1
		},
		datepicker: {
			numberOfMonths: ((window.Modernizr.touch || window.navigator.msMaxTouchPoints) && ($(window).width() < 768)) ? 1 : 2
		},
		seatMap: {
			template: {
				cabin: '<div class="seatmap-cabin"></div>',
				space: '<div class="seatmap-cabin-separate"></div>',
				sLabelWrapper: '<div class="seatmap-cabin-row seatmap-toprow"></div>',
				seatWrapper: '<div class="seatmap-cabin-wrapper"></div>',
				sLabel: '<div data-sia-rowblock="{0}" class="seatmap-columnletter">{1}</div>',
				blk: '<div class="seatmap-row-block"></div>',
				aisle: '<div class="seat-aisle"></div>',
				seatRow: '<div class="seatmap-cabin-row"><span class="seatmap-rownum left">{0}</span><span class="seatmap-rownum right">{0}</span></div>',
				seatColumn: '<div data-sia-rowblock="{0}" class="seatmap-row-block"></div>',
				inforSeat: '<span class="passenger-info__seat"></span>'
			},
			flightClass: {
				economy: 'Economy Class',
				business: 'Business Class'
			},
			seat: {
				available: 'ico-seat-available',
				empty: 'ico-seat-empty',
				occupied: 'ico-seat-occupied',
				bassinet: 'ico-1-sm-bassinet',
				blocked: 'ico-seat-blocked',
				emBassinet: 'ico-bassinet'
			},
			url: {
				seatMap: 'template/seatmap-foundation.html',
				seatMapJSON: 'ajax/seatmap_sample.json',
				seatMapJSON1: 'ajax/seatmap_sample-1.json'
			},
			passenger: 2
		},
		ajaxMethod: 'get',
		map: {
			googlemap: {
				apiKey: 'AIzaSyA8UdAEjReJYw6GTXdTqBsIxHwRA738xTo',
				lib: 'places',
				zoom: 14,
				radius: 1000
			},
			baidumap: {
				apiKey: '11ipY4UjrfPkwsHPhpqWzeHvaWkzGyAL',
				zoom: 18,
				radius: 1000
			}
		}
	};

	if (!String.prototype.format) {
		String.prototype.format = function() {
			var args = arguments;
			return this.replace(/{(\d+)}/g, function(match, number) {
				return typeof args[number] !== 'undefined' ? args[number] : match;
			});
		};
	}
	if (typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
			return this.replace(/^\s+|\s+$/g, '');
		};
	}

	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function(obj, start) {
			for (var i = (start || 0), j = this.length; i < j; i++) {
				if (this[i] === obj) {
					return i;
				}
			}
			return -1;
		};
	}


	// global variable
	var vars = {};
	var doc = $(document);
	var win = $(window);
	var body = $(document.body);
	var html = $('html');
	var menuHolder = $();
	var container = $('#container');
	var header = $('.header');
	var menuT = $('.menu');
	var menuBar = $('.menu-bar');
	var mainMenu = $('.menu-main');
	var popupSearch = $('.popup--search');
	var popupPromo = $('.popup--promo');
	var popupLogin = $('.popup--login');
	var ppLanguage = $('.popup--language');
	var popupLoggedProfile = $('.popup--logged-profile').removeClass('menu-sub');
	//var isIpad = /iPad/i.test(window.navigator.userAgent);

	var languageJSON = {
		'data': [{
			'value': 'Global',
			'flag': 'global',
			'language': 'en_UK, zh_CN, fr_FR, pt_BR, de_DE, zh_TW, ja_JP, ko_KR, ru_RU, es_ES',
			'order': 1
		}, {
			'value': 'Australia',
			'flag': 'australia',
			'language': 'en_UK',
			'order': 2
		}, {
			'value': 'Austria',
			'flag': 'austria',
			'language': 'en_UK',
			'order': 3
		}, {
			'value': 'Bangladesh',
			'flag': 'bangladesh',
			'language': 'en_UK',
			'order': 4
		}, {
			'value': 'Belgium',
			'flag': 'belgium',
			'language': 'en_UK, fr_FR',
			'order': 5
		}, {
			'value': 'Brazil',
			'flag': 'brazil',
			'language': 'pt_BR, en_UK',
			'order': 6
		}, {
			'value': 'Brunei',
			'flag': 'brunei',
			'language': 'en_UK',
			'order': 7
		}, {
			'value': 'Cambodia',
			'flag': 'cambodia',
			'language': 'en_UK',
			'order': 8
		}, {
			'value': 'Canada',
			'flag': 'canada',
			'language': 'en_UK',
			'order': 9
		}, {
			'value': 'People\'s Republic Of China',
			'flag': 'people_republic_of_china',
			'language': 'zh_CN, en_UK',
			'order': 10
		}, {
			'value': 'Denmark',
			'flag': 'denmark',
			'language': 'en_UK',
			'order': 11
		}, {
			'value': 'Egypt',
			'flag': 'egypt',
			'language': 'en_UK',
			'order': 12
		}, {
			'value': 'France',
			'flag': 'france',
			'language': 'fr_FR, en_UK',
			'order': 13
		}, {
			'value': 'Germany',
			'flag': 'germany',
			'language': 'de_DE, en_UK',
			'order': 14
		}, {
			'value': 'Greece',
			'flag': 'greece',
			'language': 'en_UK',
			'order': 15
		}, {
			'value': 'Hong Kong',
			'flag': 'hong_kong',
			'language': 'en_UK, zh_TW',
			'order': 16
		}, {
			'value': 'India',
			'flag': 'india',
			'language': 'en_UK',
			'order': 17
		}, {
			'value': 'Indonesia',
			'flag': 'indonesia',
			'language': 'en_UK',
			'order': 18
		}, {
			'value': 'Ireland',
			'flag': 'ireland',
			'language': 'en_UK',
			'order': 19
		}, {
			'value': 'Italy',
			'flag': 'italy',
			'language': 'en_UK',
			'order': 20
		}, {
			'value': 'Japan',
			'flag': 'japan',
			'language': 'ja_JP, en_UK',
			'order': 21
		}, {
			'value': 'Kuwait',
			'flag': 'kuwait',
			'language': 'en_UK',
			'order': 22
		}, {
			'value': 'Laos',
			'flag': 'laos',
			'language': 'en_UK',
			'order': 23
		},{
			'value': 'Luxembourg',
			'flag': 'luxembourg',
			'language': 'en_UK',
			'order': 24
		}, {
			'value': 'Malaysia',
			'flag': 'malaysia',
			'language': 'en_UK',
			'order': 25
		}, {
			'value': 'Maldives',
			'flag': 'maldives',
			'language': 'en_UK',
			'order': 26
		}, {
			'value': 'Nepal',
			'flag': 'nepal',
			'language': 'en_UK',
			'order': 27
		}, {
			'value': 'Netherlands',
			'flag': 'netherlands',
			'language': 'en_UK',
			'order': 28
		}, {
			'value': 'New Zealand',
			'flag': 'new_zealand',
			'language': 'en_UK',
			'order': 29
		}, {
			'value': 'Norway',
			'flag': 'norway',
			'language': 'en_UK',
			'order': 30
		}, {
			'value': 'Philippines',
			'flag': 'philippines',
			'language': 'en_UK',
			'order': 31
		}, {
			'value': 'Republic of Korea',
			'flag': 'south_korea',
			'language': 'ko_KR, en_UK',
			'order': 32
		}, {
			'value': 'Russia',
			'flag': 'russia',
			'language': 'ru_RU, en_UK',
			'order': 33
		}, {
			'value': 'Saudi Arabia',
			'flag': 'saudia_arabia',
			'language': 'en_UK',
			'order': 34
		}, {
			'value': 'Singapore',
			'flag': 'singapore',
			'language': 'en_UK, zh_CN',
			'order': 35
		}, {
			'value': 'South Africa',
			'flag': 'south_africa',
			'language': 'en_UK',
			'order': 36
		}, {
			'value': 'Spain',
			'flag': 'spain',
			'language': 'es_ES, en_UK',
			'order': 37
		}, {
			'value': 'Sri Lanka',
			'flag': 'sri_lanka',
			'language': 'en_UK',
			'order': 38
		}, {
			'value': 'Sweden',
			'flag': 'sweden',
			'language': 'en_UK',
			'order': 39
		}, {
			'value': 'Switzerland',
			'flag': 'switzerland',
			'language': 'en_UK, de_DE',
			'order': 40
		}, {
			'value': 'Taiwan',
			'flag': 'taiwan',
			'language': 'zh_TW, en_UK',
			'order': 41
		}, {
			'value': 'Thailand',
			'flag': 'thailand',
			'language': 'en_UK',
			'order': 42
		}, {
			'value': 'Turkey',
			'flag': 'turkey',
			'language': 'en_UK',
			'order': 43
		}, {
			'value': 'United Arab Emirates',
			'flag': 'united_arab_emirates',
			'language': 'en_UK',
			'order': 44
		}, {
			'value': 'United Kingdom',
			'flag': 'united_kingdom',
			'language': 'en_UK',
			'order': 45
		}, {
			'value': 'United States',
			'flag': 'united_states',
			'language': 'en_UK',
			'order': 46
		}, {
			'value': 'Vietnam',
			'flag': 'vietnam',
			'language': 'en_UK',
			'order': 47
		}]
	};

	vars = {
		doc: doc,
		win: win,
		body: body,
		html: html,
		container: container,
		header: header,
		popupPromo: popupPromo,
		ppLanguage: ppLanguage,
		languageJSON: languageJSON,
		validationRuleByGroup: [
			'validateDate',
			'required_issue_place',
			'checkofadult',
			'checkofchild',
			'checkofinfant',
			'checkCurrentDate',
			'checkpassport',
			'validateAtLeastOne',
			'notEqualTo'
		]
	};

	if (body.is('#template')) {
		return;
	}

	vars.isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};

	/*vars.detectDevice = {
		isMobile: (function(){return function(){ return ($(window).width() < 768);};})(),
		isTablet: (function(){return function(){ return ((win.width() <= config.tablet && win.width() >= config.mobile));};})()
	};*/

	vars.isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;

	vars.addClassOnIE = function(els, con, n) {
		switch (con) {
			case 'both':
				els.each(function(idx, el) {
					el = $(el);
					if (idx) {
						el.addClass('nth-child-' + (idx + 1));
					}
					if (el.is(':last')) {
						el.addClass('last-child');
					}
				});
				break;
			case 'last':
				els.each(function(idx, el) {
					el = $(el);
					if (el.is(':last')) {
						el.addClass('last-child');
					}
				});
				break;
			case '2':
				els.each(function(idx) {
					if (idx === (con - 1)) {
						$(this).addClass('nth-child-' + con);
					}
				});
				break;
			case 'option':
				els.each(function(idx) {
					if (idx < n) {
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
				break;
			case 'all':
				els.each(function(idx) {
					$(this).addClass('nth-child-' + (idx + 1));
				});
				break;
			default:
				els.each(function(idx) {
					if (idx) {
						$(this).addClass('nth-child-' + (idx + 1));
					}
				});
		}
	};

	// var addClassForSafari = function(){
	// 	// if(isSafari && (!window.Modernizr.touch && !window.navigator.msMaxTouchPoints)){
	// 	if(vars.isSafari){
	// 		body.addClass('safari');
	// 	}
	// };

	/*var centerPopup = function(el, visible){
		el.css({
			top: Math.max(0, (win.height() - el.innerHeight()) / 2) + win.scrollTop(),
			display: (visible ? 'block' : 'none'),
			//left: detectDevice.isMobile ? 0 : Math.max(0, (win.width() - el.innerWidth()) / 2)
			left: Math.max(0, (win.width() - el.innerWidth()) / 2)
		});
	};*/

	var detectHidePopup = function(nothide) {
		if (menuHolder.length) {
			if (menuHolder.is('.popup--language')) {
				/*if(!vars.isIE()){
					menuHolder.find('.custom-select input').blur();
				}
				else{
					// doc.trigger('click.hideAutocompleteCity');
					menuHolder.find('.custom-select input').closest('.custom-select').removeClass('focus');
					menuHolder.find('.custom-select input').autocomplete('close');
				}*/
				menuHolder.find('.custom-select input').closest('.custom-select').removeClass('focus');
				menuHolder.find('.custom-select input').autocomplete('close');

				menuHolder.data('triggerLanguage').trigger('click.showLanguage');
				win.off('resize.popupLanguage');
			} else if (!menuHolder.is('.popup--language')) {
				if (!nothide) {
					menuHolder.data('menu').children('a').trigger('click.showSubMenu');
				}
			}
		}
	};

	doc.off('scroll.hideOverlay mousewheel.hideOverlay').on('scroll.hideOverlay mousewheel.hideOverlay', function() {
		detectHidePopup(true);
		hidePopupSearch();
	});

	/*if(!vars.detectDevice.isMobile()){
		if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
			doc.off('scroll.hideOverlay mousewheel.hideOverlay').on('scroll.hideOverlay mousewheel.hideOverlay', function(){
				detectHidePopup();
				hidePopupSearch();
			});
		}
		else{
			doc.off('touchmove.hideOverlay').on('touchmove.hideOverlay', function(){
				if(vars.detectDevice.isTablet()){
					detectHidePopup();
					hidePopupSearch();
				}
			});
		}
	}*/

	var hideLoggedProfile = function() {
		if (popupLoggedProfile.length) {
			if (!popupLoggedProfile.hasClass('hidden')) {
				popupLoggedProfile.triggerPopup.removeClass('active');
				/*if(window.Modernizr.cssanimations){
					popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
					popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
						popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
						popupLoggedProfile.overlay.remove();
						popupLoggedProfile.overlay = $();
						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				}
				else{
					popupLoggedProfile.fadeOut(config.duration.popupSearch, function(){
						popupLoggedProfile.addClass('hidden');
					});
					popupLoggedProfile.overlay.fadeOut(config.duration.overlay, function(){
						popupLoggedProfile.overlay.remove();
						popupLoggedProfile.overlay = $();
					});
				}*/
				popupLoggedProfile.addClass('hidden');
				popupLoggedProfile.overlay.remove();
				popupLoggedProfile.overlay = $();
				win.off('resize.LoggedProfile');
			}
		}
	};
	var hidePopupSearch = function(disableHideProfile) {
		if (!popupSearch.length) {
			return;
		}
		if (!popupSearch.hasClass('hidden')) {
			popupSearch.triggerPopup.removeClass('active');
			popupSearch.find('input:text').data('ui-autocomplete').menu.element.hide();
			/*if(window.Modernizr.cssanimations){
				popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
				popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
				popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					popupSearch.removeClass('animated fadeOut').addClass('hidden');
					popupSearch.overlay.remove();
					popupSearch.overlay = $();
					popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
			}
			else{
				popupSearch.fadeOut(config.duration.popupSearch, function(){
					popupSearch.addClass('hidden');
				});
				popupSearch.overlay.fadeOut(config.duration.overlay, function(){
					popupSearch.overlay.remove();
					popupSearch.overlay = $();
				});
			}*/
			popupSearch.addClass('hidden');
			popupSearch.overlay.remove();
			popupSearch.overlay = $();
			win.off('resize.inputSearch');
		}
		if (!disableHideProfile) {
			hideLoggedProfile();
		}
	};

	// create a gesture for popup on M and T
	var popupGesture = function(popup, trigger, pClose, pContent, notTablet, callback) {
		var scrollTopHolder = 0;
		var overlayPopup = null;
		//var openOnTablet = false;
		//var openOnMobile = false;
		var timerPopupGesture = null;
		var winW = win.width();
		/*var popupOnTablet = function(){
			centerPopup(popup, true, 0);
			popup.addClass('animated fadeIn');
			container.css({
				'marginTop': - scrollTopHolder,
				'height': win.height() + scrollTopHolder,
				'overflow': 'hidden'
			}).data('setHeight', true);
			popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				if(popup.outerHeight(true) > win.height()){
					popup.find(pContent).removeAttr('style').css({
						height: win.height() - parseInt(popup.find(pContent).css('padding-top')),
						'overflow-y': 'auto'
					});
					// win.scrollTop(0);
				}
				openOnTablet = true;
				popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		};
		var popupOnMobile = function(){
			popup.show();
			if(popup.outerHeight(true) > win.height()){
				// popup.find(pContent).css({
				// 	height: win.height() - parseInt(popup.find(pContent).css('padding')),
				// 	'overflow-y': 'auto'
				// });
				container.css('height', popup.outerHeight(true));
			}
			else{
				container.css('height', win.height());
				popup.height(win.height());
			}
			// centerPopup(popup, true, 0);
			popup.addClass('animated slideInRight');
			popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
				container.css('overflow', 'hidden');
				popup.css('top', '');
				openOnMobile = true;
				win.scrollTop(0);
				popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		};*/

		var onResize = function() {
			if (winW === win.width() && (window.navigator.msMaxTouchPoints || window.Modernizr.touch)) {
				return;
			}
			winW = win.width();
			popup.css('height', '');
			popup.find(pContent).css('height', '');
			// container.css('height', '');
			/*if(popup.outerHeight(true) > win.height()){
				if(openOnMobile && win.width() < config.mobile){
					container.css('height', popup.outerHeight(true));
					container.css('overflow', 'hidden');
				}
				if(openOnTablet && win.width() > config.mobile){
					popup.find(pContent).removeAttr('style').css({
						height: win.height() - parseInt(popup.find(pContent).css('padding-top')),
						'overflow-y': 'auto'
					});
				}
			}
			else{
				if(openOnMobile && win.width() < config.mobile){
					container.css('height', win.height());
					popup.height(win.height());
					container.css('overflow', 'hidden');
				}
				if(openOnTablet && win.width() > config.mobile){
					popup.find(pContent).removeAttr('style');
				}
			}*/
			if (win.width() >= config.tablet - config.width.docScroll) {
				overlayPopup.remove();
				popup.removeClass('animated fadeIn').removeAttr('style');
				container.removeAttr('style');
				win.scrollTop(scrollTopHolder);
				scrollTopHolder = 0;
				win.off('resize.popupGesture');
				return;
			}
			popup.show();
			/*if(openOnMobile && win.width() > config.mobile){
				popup.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
				popup.find(pContent).css('height', '');
				container.removeAttr('style').data('setHeight', false);
				win.scrollTop(scrollTopHolder);
				if(notTablet){
					overlayPopup.remove();
					popup.hide();
					win.off('resize.popupGesture');
				}
				else{
					//popupOnTablet();
				}
				//openOnMobile = false;
				//openOnTablet = true;
			}*/
			/*if(openOnTablet && win.width() < config.mobile){
				popup.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
				popup.find(pContent).css('height', '');
				container.removeAttr('style').data('setHeight', false);
				win.scrollTop(scrollTopHolder);
				popup.css('left', '');
				popup.css('top', '');
				//popupOnMobile();
				openOnTablet = false;
				openOnMobile = true;
			}*/
		};
		trigger.off('click.showPopup').on('click.showPopup', function(e) {
			e.preventDefault();
			winW = win.width();
			if (callback) {
				callback($(this));
			}
			if (win.width() < config.tablet - config.width.docScroll) {
				/*if(window.Modernizr.csstransitions){
					scrollTopHolder = win.scrollTop();
					popup.show();
					overlayPopup = $(config.template.overlay).appendTo(body).show().css({'opacity': 0.5, 'zIndex': config.zIndex.overlayPopup}).addClass('animated fadeInOverlay');
					if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
						//popupOnMobile();
					}
					else{ // tablet
						//popupOnTablet();
						if(isIpad){
							overlayPopup.css('height', win.height());
						}
					}
				}
				else{

				}*/
				win.off('resize.popupGesture').on('resize.popupGesture', function() {
					clearTimeout(timerPopupGesture);
					timerPopupGesture = setTimeout(function() {
						onResize();
					}, 150);
				});
				overlayPopup.off('click.closePopup').on('click.closePopup', function(e) {
					e.preventDefault();
					popup.find(pClose).trigger('click.closePopup');
				});
			}
		});
		popup.find(pClose).off('click.closePopup').on('click.closePopup', function(e) {
			e.preventDefault();
			win.off('resize.popupGesture');
			/*if(win.width() < config.tablet - config.width.docScroll){
				if(window.Modernizr.csstransitions){
					if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
						overlayPopup.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						popup.addClass('animated slideOutRight');
						popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
							container.removeAttr('style').data('setHeight', false);
							win.scrollTop(scrollTopHolder);
							popup.removeClass('animated slideOutRight slideInRight');
							overlayPopup.off('click.closePopup').remove();
							scrollTopHolder = 0;
							popup.removeAttr('style');
							popup.find(pContent).removeAttr('style');
							popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{
						overlayPopup.addClass('animated fadeOutOverlay');
						popup.addClass('animated fadeOut');
						win.scrollTop(scrollTopHolder);
						scrollTopHolder = 0;
						popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
							container.removeAttr('style').data('setHeight', false);
							overlayPopup.off('click.closePopup').remove();
							popup.removeClass('animated fadeOut fadeIn');
							popup.removeAttr('style');
							popup.find(pContent).removeAttr('style');
							popup.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
				}
				else{
				}
			}*/
		});
	};
	//public popupGesture
	vars.popupGesture = popupGesture;

	// click document to hide popup
	container.off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
		if (!$(e.target).closest('.menu').length) {
			if (typeof window.resetMenuT !== 'undefined') {
				window.resetMenuT();
			}
		}
		if (!$(e.target).closest('.popup--search').length) {
			hidePopupSearch();
		}
	});

	var animationNavigationOnDesktop = function() {
		var menu = mainMenu.find('.menu-item');
		var winW = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

		var getPosTopArrow = function(el) {
			var elW = el.width(),
				elPos = el.children('a').offset();
			return Math.max(elPos.left + elW / 2, 0);
		};

		mainMenu.preventClick = false;
		menu.each(function() {
			var self = $(this);
			var realSub = self.children('.menu-sub'); // real sub
			var sub = self.children('.menu-sub'); // fake sub
			var timerResetMenu = null;
			if (sub.length) {

				if(!realSub.find('.sub-item').length && !realSub.children('.menu-sub__join').length) {
					self.children('a').find('.ico-point-r').addClass('hidden');
					return;
				}

				sub = sub.clone().addClass('menu-clone').removeClass('hidden').appendTo(body); // fake sub is created
				sub.data('menu', self);
				/*if(!window.Modernizr.cssanimations){
					sub.hide();
				}
				else{
					sub.addClass('hidden');
				}*/
				sub.addClass('hidden');

				var openNav = function() {
					self.overlay = $(config.template.overlay).height(doc.height()).appendTo(body);
					if(vars.isIE() && vars.isIE() < 8){
						self.overlay.css({
							top: header.offset().top + header.height()
						});
					}
					self.overlay.off('click.hideOverlay').on('click.hideOverlay', function() {
						closeNav();
					});

					/*if(window.Modernizr.cssanimations){
						self.overlay.hide();
						sub.removeClass('hidden').addClass('animated fadeIn');
						self.overlay.show().addClass('animated fadeInOverlay');
						sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							menuHolder = sub;
							sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{
						sub.fadeIn(config.duration.menu).addClass('fadeIn');
						if(vars.isIE() && vars.isIE() < 8){
							self.overlay.css({
								top: header.height()
							});
						}
						self.overlay.fadeIn(config.duration.overlay, function(){
							menuHolder = sub;
						});
					}*/

					sub.removeClass('hidden').addClass('fadeIn');
					// sub.removeClass('hidden').addClass('fadeIn').hide().fadeIn(config.duration.menu);
					/*if(vars.isIE() && vars.isIE() < 8){
						self.overlay.css({
							top: header.height()
						});
						win.off('scroll.setopNav').on('scroll.setopNav', function(){
							if(win.scrollTop() > header.height() + 100){
								self.overlay.css('top','');
							}
							else{
								self.overlay.css({
									top: header.height()
								});
							}
						});
					}*/
					// self.overlay.fadeIn(config.duration.overlay, function(){
					// 	menuHolder = sub;
					// });
					self.overlay.show();
					menuHolder = sub;
					self.addClass('active');
					header.addClass('active');
					win.off('resize.resetSub').on('resize.resetSub', function() {
						clearTimeout(timerResetMenu);
						timerResetMenu = setTimeout(function() {
							if (win.width() < config.tablet - config.width.docScroll) {
								self.overlay.remove();
								self.removeClass('active');
								header.removeClass('active');
								sub.addClass('hidden').removeClass('animated fadeOut fadeIn');
								menuHolder = $();
								win.off('resize.resetSub');
							}
							else {
								sub.find('.icon-top-arrow').css('left', getPosTopArrow(self));
							}
						}, config.duration.clearTimeout);
					});
				};

				var closeNav = function() {
					/*if(window.Modernizr.cssanimations){
						sub.removeClass('fadeIn').addClass('fadeOut');
						mainMenu.preventClick = true;
						sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							sub.addClass('hidden').removeClass('animated fadeOut');
							self.overlay.remove();
							sub.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
						self.overlay.addClass('fadeOutOverlay');
						mainMenu.preventClick = false;
					}
					else{
						sub.fadeOut(config.duration.menu).removeClass('fadeIn');
						self.overlay.fadeOut(config.duration.overlay, function(){
							$(this).remove();
						});
					}*/
					// sub.fadeOut(config.duration.menu).removeClass('fadeIn');
					sub.removeClass('fadeIn').addClass('hidden');
					if (self.overlay) {
						self.overlay.remove();
					}
					self.removeClass('active');
					menuHolder = $();
					header.removeClass('active');
					win.off('resize.resetSub');
					win.off('scroll.setopNav');
				};

				var beforeShowHideMenu = function() {
					if (menuHolder.length) {
						if (menuHolder.is('.popup--language')) {
							menuHolder.data('triggerLanguage').trigger('click.showLanguage');
						}
					}

					if (vars.popupPromo && vars.popupPromo.length && vars.popupPromo.data('Popup') && vars.popupPromo.data('Popup').element && vars.popupPromo.data('Popup').element.isShow) {
						vars.popupPromo.Popup('hide');
					}
					if (vars.popupPromo && vars.popupPromo.length && popupLogin.length && popupLogin.data('Popup') && popupLogin.data('Popup').element && popupLogin.data('Popup').element.isShow) {
						popupLogin.Popup('hide');
					}

					// hide menu if there is a its siblings opened
					if ($('.menu-sub.menu-clone.fadeIn').length && !$('.menu-sub.menu-clone.fadeIn').is(sub)) {
						$('.menu-sub.menu-clone.fadeIn').addClass('hidden').removeClass('fadeIn').data('menu').removeClass('active').overlay.remove();
					}

					// open and close sub
					//if (!sub.hasClass('fadeIn')) {
						//openNav();
					//} else {
						//closeNav();
					//}

					var topArrow = sub.find('.icon-top-arrow');
					if (!topArrow.length) {
						topArrow = $('<span class="icon-top-arrow"></span>').appendTo(sub);
						topArrow.css('left', getPosTopArrow(self));
					}
					else {
						topArrow.css('left', getPosTopArrow(self));
					}
				};

				var callCloseTimer;

				self.off('mouseover.showSubMenu').on('mouseover.showSubMenu', function() {
					if (callCloseTimer) {
						clearTimeout(callCloseTimer);
					}
					sub.css('top', mainMenu.offset().top + self.height());
					beforeShowHideMenu();
					if (!sub.hasClass('fadeIn')) {
						openNav();
					}
				}).off('mouseout.showSubMenu').on('mouseout.showSubMenu', function() {
					beforeShowHideMenu();
					callCloseTimer = setTimeout(function(){
						closeNav();
					}, 100);
				});

				sub.off('mouseover.showSubMenu').on('mouseover.showSubMenu', function() {
					clearTimeout(callCloseTimer);
				}).off('mouseout.showSubMenu').on('mouseout.showSubMenu', function() {
					// var x = e.pageX;
					// var y = e.pageY;
					// var off = $(this).offset();
					// if (!(off.left < x && off.left + $(this).width() > x && off.top < y && off.top + $(this).height() > y)) {
					beforeShowHideMenu();
					callCloseTimer = setTimeout(function(){
						closeNav();
					}, 100);
					// }
				});


				/*
				var menuAnimation = function() {

					if (menuHolder.length) {
						if (menuHolder.is('.popup--language')) {
							menuHolder.data('triggerLanguage').trigger('click.showLanguage');
						}
					}
					if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
						vars.popupPromo.Popup('hide');
					}
					if (vars.popupPromo.length && popupLogin.length && popupLogin.data('Popup').element.isShow) {
						popupLogin.Popup('hide');
					}

					// hide menu if there is a its siblings opened
					if ($('.menu-sub.menu-clone.fadeIn').length && !$('.menu-sub.menu-clone.fadeIn').is(sub)) {
						$('.menu-sub.menu-clone.fadeIn').addClass('hidden').removeClass('fadeIn').data('menu').removeClass('active').overlay.remove();
					}

					// open and close sub
					if (!sub.hasClass('fadeIn')) {
						openNav();
					} else {
						closeNav();
					}

					var topArrow = sub.find('.icon-top-arrow');
					if (!topArrow.length) {
						topArrow = $('<span class="icon-top-arrow"></span>').appendTo(sub);
						topArrow.css('left', getPosTopArrow(self));
					}
					else {
						topArrow.css('left', getPosTopArrow(self));
					}
				};

				self.children('a').off('click.showSubMenu').on('click.showSubMenu', function(e) {
					e.preventDefault();
					var popupSeatSelect = $('[data-infomations-1]'),
						popupSeatChange = $('[data-infomations-2]');
					if (popupSeatSelect.length) {
						popupSeatSelect.find('.tooltip__close').trigger('click');
					}
					if (popupSeatChange.length) {
						popupSeatChange.find('.tooltip__close').trigger('click');
					}
					if (mainMenu.preventClick) {
						return;
					}
					sub.css('top', mainMenu.offset().top + self.height());
					menuAnimation();
				});

				*/

				/*sub.find('.ico-close-rounded').off('click.hideSubMenu').on('click.hideSubMenu', function(e){
					e.preventDefault();
					if(window.Modernizr.touch){
						self.children('a').trigger('click.showSubMenu');
					}
					else{
						closeNav();
					}
				});*/
				var closeBtn = sub.find('.menu-sub__close').detach();
				closeBtn.off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
					e.preventDefault();
					if (window.Modernizr.touch) {
						self.children('a').trigger('click.showSubMenu');
					} else {
						closeNav();
					}
				});
				sub.append(closeBtn);
				realSub.find('.back-to-menu').off('click.hideSubMenu').on('click.hideSubMenu', function(e) {
					e.preventDefault();
					/*if (window.Modernizr.cssanimations) {
						setTimeout(function(){
							realSub.addClass('hidden');
							realSub.closest('.menu-item').removeClass('active');
						},500);
						mainMenu.css('height', '').removeClass('active');
						menuBar.removeClass('active');
					}
					else {

					}*/

					var currentX = -109; //use percent as unit
					var targetX = 0;
					var spaceToMove = 10;
					var interval = 20;
					var animateInterval = window.setInterval(function() {
						if (currentX >= targetX) {
							window.clearInterval(animateInterval);
							//
							mainMenu.children('ul').add(menuBar).css({
								'transform': '',
								'-moz-transform': '',
								'-webkite-transform': '',
								'-ms-transform': '',
								'-o-transform': ''
							});
							//setTimeout(function(){
							realSub.addClass('hidden');
							realSub.closest('.menu-item').removeClass('active');
							//},500);
							mainMenu.removeClass('active');
							menuBar.removeClass('active');
						} else {
							currentX += spaceToMove;
							mainMenu.children('ul').add(menuBar).css({
								'transform': 'translateX(' + currentX + '%)',
								'-moz-transform': 'translateX(' + currentX + '%)',
								'-webkite-transform': 'translateX(' + currentX + '%)',
								'-ms-transform': 'translateX(' + currentX + '%)',
								'-o-transform': 'translateX(' + currentX + '%)'
							});
						}
					}, interval);
				});
			}
		});

		win.off('resize.changeTopArrow').on('resize.changeTopArrow', function() {
			var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			if (w !== winW) {
				winW = w;

			}
		});
	};

	var selectLanguage = function() {
		var triggerLanguage = menuBar.find('ul > li:first > a');
		var timeoutppLanguage = null;
		//var openOnMobile = false;
		//var openOnTablet = false;
		var freezeBody = false;
		//var wW = win.width();
		ppLanguage = ppLanguage.appendTo(body).css('zIndex', config.zIndex.ppLanguage);
		var innerPopup = ppLanguage.find('.popup__inner');

		ppLanguage.data('triggerLanguage', triggerLanguage);
		triggerLanguage.off('click.showLanguage').on('click.showLanguage', function(e) {
			e.preventDefault();
			// hide menu
			if (menuHolder.length) {
				if (!menuHolder.is('.popup--language')) {
					menuHolder.data('menu').children('a').trigger('click.showSubMenu');
				}
			}
			// hide promotion popup
			if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
				vars.popupPromo.Popup('hide');
			}
			// hide login popup
			if (popupLogin.length && popupLogin.data('Popup').element.isShow) {
				popupLogin.Popup('hide');
			}


			// open and close popup language

			if (ppLanguage.hasClass('hidden')) { //open
				/*var popupOnTablet = function(){
					ppLanguage.removeClass('hidden').css({
						left: triggerLanguage.offset().left - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
						top: triggerLanguage.offset().top + triggerLanguage.height() + 25
					}).addClass('animated fadeIn');
					ppLanguage.off('touchmove.preventDefault').on('touchmove.preventDefault', function(e){
						e.stopPropagation();
					});
					if(win.width() < config.tablet - config.width.docScroll){
						if(!container.data('setHeight')){
							freezeBody = true;
							container.css({
								'height': win.height(),
								'overflow': 'hidden'
							});
						}
					}
					menuHolder = ppLanguage;
					triggerLanguage.addClass('active');
					triggerLanguage.overlay.show().addClass('animated fadeInOverlay').css('zIndex', (win.width() < config.tablet - config.width.docScroll) ? (config.zIndex.ppLanguage - 1) : '');
					triggerLanguage.overlay.off('touchmove.preventDefault').on('touchmove.preventDefault', function(){
						// e.stopPropagation();
					});
					if(isIpad){
						triggerLanguage.overlay.css({
							'position': 'absolute',
							'top': 0,
							'left': 0,
							height: body.height()
						});
					}
					openOnTablet = true;
				};
				var popupOnMobile = function(){
					ppLanguage.removeClass('hidden');
					centerPopup(ppLanguage, true);
					ppLanguage.css('top', 0);
					if(vars.detectDevice.isMobile()){
						ppLanguage.addClass('animated slideInRight');
						if(ppLanguage.outerHeight(true) > win.height()){
							container.css('height', ppLanguage.outerHeight(true));
						}
						else{
							container.css('height', win.height());
							ppLanguage.height(win.height());
						}
						container.css('overflow', 'hidden');
						ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							openOnMobile = true;
							ppLanguage.css('right', 0);
							ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
						ppLanguage.off('click.doNothing').on('click.doNothing', function(){
							// fix bug for lumina automatically close modal
						});
					}
					menuHolder = ppLanguage;
					triggerLanguage.addClass('active');
					triggerLanguage.overlay.show().addClass('animated fadeInOverlay').css('zIndex', (win.width() < config.tablet - config.width.docScroll) ? (config.zIndex.ppLanguage - 1) : '');
				};*/
				var setPosition = function() {
					ppLanguage.removeClass('hidden');
					/*var leftPpLanguage = Math.max(0,triggerLanguage.offset().left) - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2;
					if(leftPpLanguage + ppLanguage.outerWidth() > document.body.clientWidth){
						leftPpLanguage = leftPpLanguage - (leftPpLanguage + ppLanguage.outerWidth() - win.width());
					}
					ppLanguage.css({
						left: document.body.clientWidth > triggerLanguage.offset().left ? leftPpLanguage : triggerLanguage.offset().left - ppLanguage.outerWidth() + triggerLanguage.outerWidth(true)/2,
						top: Math.max(0,triggerLanguage.offset().top + triggerLanguage.height() + 40)
					});
					ppLanguage.find('.popup__arrow').css({
						left : Math.min(triggerLanguage.offset().left - leftPpLanguage + 10, ppLanguage.outerWidth() - 20)
					});*/

					var widthInnerPopup = innerPopup.outerWidth(),
						leftPpLanguage = Math.max(0, triggerLanguage.offset().left) - widthInnerPopup / 2 + triggerLanguage.outerWidth(true) / 2;

					if (leftPpLanguage + widthInnerPopup > document.body.clientWidth) {
						leftPpLanguage = win.width() - innerPopup.outerWidth();
					}
					ppLanguage.css('position', 'fixed');
					innerPopup.css({
						position: 'absolute',
						left: document.body.clientWidth > triggerLanguage.offset().left ? leftPpLanguage : triggerLanguage.offset().left - widthInnerPopup + triggerLanguage.outerWidth(true) / 2,
						top: Math.max(0, triggerLanguage.parent().offset().top + triggerLanguage.parent().height() + 30 - win.scrollTop())
					});
					ppLanguage.find('.popup__arrow').css({
						left: Math.min(triggerLanguage.offset().left - leftPpLanguage + 21, widthInnerPopup - 20)
					});

					// if(window.innerWidth >= config.tablet){
					// 	ppLanguage.removeClass('hidden').css({
					// 		left: Math.max(0,triggerLanguage.offset().left) - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
					// 		top: Math.max(0,triggerLanguage.offset().top + triggerLanguage.height() + 40)
					// 	});
					// }
					// else{
					// 	ppLanguage.removeClass('hidden').css({
					// 		left: Math.max(0,triggerLanguage.offset().left) - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
					// 		top: Math.max(0,triggerLanguage.offset().top + triggerLanguage.height() + 25)
					// 	});
					// }
					triggerLanguage.overlay.css({
						'zIndex': (window.innerWidth < config.tablet) ? (config.zIndex.ppLanguage - 1) : ''
					});
				};
				triggerLanguage.overlay = $(config.template.overlay).appendTo(body);
				triggerLanguage.overlay.hide();
				triggerLanguage.overlay.off('click.hideOverlay').on('click.hideOverlay', function() {
					triggerLanguage.trigger('click.showLanguage');
				});

				/*// Desktop and Tablet
				if(window.Modernizr.cssanimations){
					//popupOnTablet();
				}
				else{

				}*/
				/*ppLanguage.removeClass('hidden').css({
					left: triggerLanguage.offset().left - ppLanguage.outerWidth(true)/2 + triggerLanguage.outerWidth(true)/2,
					display: 'block',
					top: triggerLanguage.offset().top + triggerLanguage.height() + 25
				});*/
				ppLanguage.css('position', 'fixed').removeClass('hidden');
				menuHolder = ppLanguage;
				triggerLanguage.addClass('active');
				/*if(vars.isIE() && vars.isIE() < 8){
					triggerLanguage.overlay.css({
						top: header.height()
					});
					doc.off('scroll.setopNavtriggerLanguage').on('scroll.setopNavtriggerLanguage', function(){
						if(win.scrollTop() >= header.height()){
							triggerLanguage.overlay.css('top','');
						}
						else{
							triggerLanguage.overlay.css({
								top: header.height()
							});
						}
					});
				}*/
				triggerLanguage.overlay.show().css('zIndex', (window.innerWidth < config.tablet) ? (config.zIndex.ppLanguage - 1) : '');
				setPosition();
				win.off('resize.popupLanguage').on('resize.popupLanguage', function() {
					clearTimeout(timeoutppLanguage);
					timeoutppLanguage = setTimeout(function() {
						/*if(openOnMobile){
							if(wW !== win.width()){
								if(ppLanguage.find('.ui-autocomplete-input').autocomplete('widget').is(':visible')){
									ppLanguage.find('.ui-autocomplete-input').blur();
								}
								wW = win.width();
							}
						}*/
						ppLanguage.css('height', '');
						ppLanguage.find('.popup__content').css('height', '');
						//container.css('height','');
						if (window.innerWidth < config.tablet) {
							freezeBody = true;
							container.css({
								'height': win.height(),
								'overflow': 'hidden'
							});
						} else if (window.innerWidth >= config.tablet) {
							freezeBody = false;
							container.removeAttr('style');
						}
						if (ppLanguage.outerHeight(true) > win.height()) {
							if (window.innerWidth > config.mobile) {
								ppLanguage.find('.popup__content').css({
									height: win.height() - parseInt(ppLanguage.find('.popup__content').css('padding-top')),
									'overflow-y': 'auto'
								});
								/*if(vars.detectDevice.isMobile()){
									container.css('height', ppLanguage.outerHeight(true));
								}*/
							}
							/*else if(win.width() < config.mobile){
								if(vars.detectDevice.isMobile()){
									container.css('height', '');
									container.css('height', ppLanguage.height());
									ppLanguage.height(ppLanguage.height());
								}
							}*/
						}
						/*else{
							if(vars.detectDevice.isMobile()){
								container.css('height', '');
								container.css('height', win.height());
								ppLanguage.height(win.height());
							}
						}*/
						setPosition();
						/*if(openOnMobile && win.width() > config.mobile){
							ppLanguage.css('height', '').removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
							ppLanguage.find('.popup__content').css('height', '');
							//popupOnTablet();
							setPosition();
							openOnMobile = false;
							openOnTablet = true;
						}
						if(openOnTablet && win.width() < config.mobile){
							ppLanguage.css('height', '').css('top', '').removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
							ppLanguage.find('.popup__content').css('height', '');
							//popupOnMobile();
							setPosition();
							openOnTablet = false;
							openOnMobile = true;
						}*/
					}, 100);
				});
			} else { // close
				win.off('resize.popupLanguage');
				/*if(window.Modernizr.cssanimations){
					if(vars.detectDevice.isMobile()){
						ppLanguage.addClass('animated slideOutRight');
						triggerLanguage.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay').css('zIndex', '');
						ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							triggerLanguage.removeClass('active');
							menuHolder = $();
							triggerLanguage.overlay.remove();
							ppLanguage.removeClass('animated slideOutRight slideInRight').addClass('hidden');
							ppLanguage.css('height', '');
							ppLanguage.find('.popup__content').removeAttr('style');
							triggerLanguage.overlay.remove();
							win.trigger('resize.openMenuT');
							ppLanguage.css('right', '');
							ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{
						ppLanguage.addClass('fadeOut');
						triggerLanguage.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay').css('zIndex', '');
						triggerLanguage.removeClass('active');
						ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(e) {
							e.preventDefault();
							menuHolder = $();
							triggerLanguage.overlay.remove();
							ppLanguage.removeClass('animated fadeOut').addClass('hidden');
							if(freezeBody){
								container.removeAttr('style');
								freezeBody = false;
							}
							ppLanguage.css('right', '');
							ppLanguage.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
				}
				else{

				}*/
				ppLanguage.addClass('hidden');
				triggerLanguage.removeClass('active');
				menuHolder = $();
				triggerLanguage.overlay.css('zIndex', '').remove();
				doc.off('scroll.setopNavtriggerLanguage');
			}
		});

		// close popup language
		ppLanguage.find('.popup__close').off('click.hideLanguage').on('click.hideLanguage', function(e) {
			e.preventDefault();
			//e.stopPropagation()
			triggerLanguage.trigger('click.showLanguage');
		});
		ppLanguage.off('click.hideLanguage').on('click.hideLanguage', function() {
			triggerLanguage.trigger('click.showLanguage');
		});
		innerPopup.off('click.hideLanguage').on('click.hideLanguage', function(e) {
			if (!e) {
				e = window.event;
			}
			//IE9 & Other Browsers
			if (e.stopPropagation) {
				e.stopPropagation();
			}
			//IE8 and Lower
			else {
				e.cancelBubble = true;
			}
		});
	};

	var loginPopup = function() {
		var triggerLoginPoup = menuBar.find('ul a.login');
		var flyingFocus = $('#flying-focus');

		// init login popup
		popupLogin.Popup({
			overlayBGTemplate: config.template.overlay,
			modalShowClass: '',
			triggerCloseModal: '.popup__close',
			afterShow: function() {
				/*if(isIpad && (win.width() < config.tablet)){
					menuT.css('position', 'absolute');
				}*/
				flyingFocus = $('#flying-focus');
				if (flyingFocus.length) {
					flyingFocus.remove();
				}
			},
			beforeHide: function() {
				if (popupLogin.find('[data-tooltip]').length) {
					popupLogin.find('[data-tooltip]').data('kTooltip').closeTooltip();
				}
			}
			// afterHide: function(){

			// }
		});
		triggerLoginPoup.off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			// hide sub
			if (menuHolder.length) {
				var subMenu = $('.menu-sub.menu-clone.fadeIn');
				if (subMenu.length) {
					subMenu.find('.menu-sub__close').trigger('click.hideSubMenu');
					// subMenu.hide().removeClass('fadeIn').data('menu').removeClass('active').overlay.remove();
				}

				/*if(!menuHolder.is('.popup--language')){
					menuHolder.css({
						top: - menuHolder.height() - header.height() + header.offset().top
					}).removeClass('fadeIn');
					menuHolder.data('menu').removeClass('active');
					menuHolder.data('menu').overlay.remove();
					menuHolder = $();
					header.removeClass('active');
				}
				if(menuHolder.is('.popup--language')){
					menuHolder.addClass('hidden');
					win.off('resize.popupLanguage');
					menuHolder.data('triggerLanguage').removeClass('active');
					menuHolder.data('triggerLanguage').overlay.remove();
					menuHolder = $();
				}*/
			}
			// hide promotion popup
			if (vars.popupPromo.length && vars.popupPromo.data('Popup').element.isShow) {
				vars.popupPromo.Popup('hide');
			}

			// hide search popup
			hidePopupSearch();

			popupLogin.Popup('show');
		});

		// highlight input
		var inputs = popupLogin.find('input:text, input:password');
		inputs.off('focus.highlight').on('focus.highlight', function(e) {
			e.stopPropagation();
			$(this).closest('.input-1').addClass('focus');
		});
		inputs.off('blur.highlight').on('blur.highlight', function() {
			$(this).closest('.input-1').removeClass('focus');
		});
	};

	var menuTablet = function() {
		var navbar = $('.ico-nav');
		menuT = $('.menu');
		var menuBarItems = menuBar.find('li');
		var login = menuBarItems.eq(2).clone().addClass('login-item').prependTo(mainMenu.children('ul'));
		var search = menuBarItems.eq(1).clone(true).addClass('search-item').prependTo(mainMenu.children('ul'));
		var resetMenuTimer = null;
		var isAnimate = false;
		var timerPopupSearchResize = null;
		if (vars.isIE() && vars.isIE() < 8) {
			popupSearch = popupSearch.appendTo(body).css('zIndex', 100);
			// popupSearch = $('.popup--search').clone(true).appendTo(body).css('zIndex', 100);
		}

		search.find('.popup--search').removeClass('hidden');
		login.hide();
		search.hide();
		// container.off('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd').on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd', function() {
		// 	if(container.hasClass('show-menu')){
		// 		resetMenuT();
		// 	}
		// });
		var resetMenuT = function() {
			if (container.hasClass('show-menu')) {
				container.removeClass('show-menu');
				// reset menu
				// do not use event transitionEnd because this event always trigger when container's children do css animation
				/*if(window.Modernizr.cssanimations){
					menuT.addClass('fadeOutLeft animated');
					menuT.menuOverlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
						container.removeAttr('style').data('setHeight', false);
						win.off('resize.openMenuT');
						menuBarItems.eq(1).show();
						menuBarItems.eq(2).show();
						login.hide();
						search.hide();
						menuT.removeClass('fadeOutLeft animated active').css('display', '').css('height', '');
						isAnimate = false;
						menuT.menuOverlay.remove();
						mainMenu.removeClass('active');
						menuBar.removeClass('active');
						mainMenu.find('.menu-item').removeClass('active');
						mainMenu.find('.menu-sub').addClass('hidden');
						menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				}
				else{

				}*/
				menuT.menuOverlay.remove();
				menuT.find('.menu-inner').removeAttr('style');
				// if(detectDevice.isTablet()){
				// 	html.removeAttr('style');
				// }
				html.removeAttr('style');
				body.removeAttr('style');
				win.off('resize.openMenuT');
				menuBarItems.eq(1).show();
				menuBarItems.eq(2).show();
				login.hide();
				search.hide();
				menuT.css('display', '').css('height', '');
				//isAnimate = false;
				menuT.menuOverlay.remove();
				mainMenu.removeClass('active');
				menuBar.removeClass('active');
				mainMenu.find('.menu-item').removeClass('active');
				mainMenu.find('.menu-sub').addClass('hidden');
			}
		};
		// freeze body to slide
		var freezeBody = function() {
			/*if(window.Modernizr.cssanimations){
				var getMaxHeight = win.height();
				if(menuT.hasClass('active')){
					menuT.find('.menu-inner').height(getMaxHeight - parseInt($('.menu-inner').css('padding-top')));
					container.css('height', '');
					container.css('height', win.height());
					return;
				}
				menuT.menuOverlay = $(config.template.overlay).appendTo(body).css('opacity', 0.5).show().addClass('animated fadeInOverlay');
				if(isIpad){
					menuT.menuOverlay.css({
						'position': 'absolute',
						'top': 0,
						'left': 0
					});
				}
				menuT.css('height', '');
				menuT.addClass('fadeInLeft animated active').show();
				menuT.find('.menu-inner').height(getMaxHeight - parseInt($('.menu-inner').css('padding-top')));

				menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
					menuT.removeClass('fadeInLeft animated');
					isAnimate = false;
					container.css({
						'overflow': 'hidden',
						'height': win.height()
					}).data('setHeight', true);
					menuT.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
				});
			}
			else{

			}*/
			menuT.menuOverlay = $(config.template.overlay).appendTo(body).show();
		};

		var openMenuT = function() {
			//container.width(win.width());
			container.addClass('show-menu');
			freezeBody();

			menuT.menuOverlay.off('click.hideMenu').on('click.hideMenu', function(e) {
				e.preventDefault();
				resetMenuT();
			});
			menuBarItems.eq(1).hide();
			menuBarItems.eq(2).hide();
			login.show();
			search.show();

			win.off('resize.openMenuT').on('resize.openMenuT', function() {
				if (win.width() >= config.tablet - config.width.docScroll) {
					mainMenu.removeClass('active');
					menuBar.removeClass('active');
				}

				if (win.width() >= config.tablet - config.width.docScroll) {
					if (menuT.find('.menu-sub').length) {
						menuT.removeAttr('style').find('.menu-sub').addClass('hidden').parent().removeClass('active');
						menuT.find('.menu-inner').removeAttr('style');
						menuT.find('.menu-main').removeAttr('style');
						container.removeAttr('style');
					}
					container.removeClass('show-menu');
					menuT.removeClass('fadeInLeft animated active').css('display', '').css('height', '');
					menuT.menuOverlay.remove();
					/*if(!vars.detectDevice.isMobile()){
						win.off('resize.openMenuT');
					}*/
					menuBarItems.eq(1).show();
					menuBarItems.eq(2).show();
					login.hide();
					search.hide();
					return;
				}
				clearTimeout(resetMenuTimer);
				resetMenuTimer = setTimeout(function() {
					container.css('height', win.height());
					menuT.show().height(win.height());
					responsiveMenubar();
				}, config.duration.clearTimeout);

				/*if(!vars.detectDevice.isMobile()){
					if(win.width() >= config.tablet - config.width.docScroll){
						if(menuT.find('.menu-sub').length){
							menuT.removeAttr('style').find('.menu-sub').addClass('hidden').parent().removeClass('active');
							menuT.find('.menu-inner').removeAttr('style');
							menuT.find('.menu-main').removeAttr('style');
							container.removeAttr('style');
						}
						container.removeClass('show-menu');
						menuT.removeClass('fadeInLeft animated active').css('display', '').css('height', '');
						menuT.menuOverlay.remove();
						if(!vars.detectDevice.isMobile()){
							win.off('resize.openMenuT');
						}
						menuBarItems.eq(1).show();
						menuBarItems.eq(2).show();
						login.hide();
						search.hide();
						return;
					}
					clearTimeout(resetMenuTimer);
					resetMenuTimer = setTimeout(function(){
						container.css('height', win.height());
						menuT.show().height(win.height());
						responsiveMenubar();
					}, config.duration.clearTimeout);
				}else{
					clearTimeout(resetMenuTimer);
					resetMenuTimer = setTimeout(function(){
						freezeBody();
					}, config.duration.clearTimeout);
				}*/
			});

		};
		window.resetMenuT = resetMenuT;
		var responsiveMenubar = function() {
			if (win.width() < config.tablet - config.width.docScroll) {
				menuBarItems.eq(1).hide();
				menuBarItems.eq(2).hide();
				login.show();
				search.show();
			} else {
				menuBarItems.eq(1).show();
				menuBarItems.eq(2).show();
				login.hide();
				search.hide();
			}
		};

		menuBarItems.eq(1).children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			e.stopPropagation();
			detectHidePopup();
			hideLoggedProfile();
			/*if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
				$('[data-customselect]').customSelect('hide');
			}*/
			$('[data-customselect]').customSelect('hide');
			var self = $(this);
			if (popupSearch.hasClass('hidden')) {
				self.addClass('active');

				// var popupSearchPosition = function() {
				// 	popupSearch.removeClass('hidden');
				// 	/*var leftPopupSearch = menuBarItems.eq(1).offset().left - popupSearch.outerWidth()/2 + menuBarItems.eq(1).outerWidth()/2 - 30;
				// 	if(leftPopupSearch + popupSearch.outerWidth() > win.width()){
				// 		leftPopupSearch = leftPopupSearch - (leftPopupSearch + popupSearch.outerWidth() - win.width());
				// 	}

				// 	popupSearch.css({
				// 		left: document.body.clientWidth > menuBarItems.eq(1).offset().left ? leftPopupSearch : menuBarItems.eq(1).offset().left - popupSearch.outerWidth() + menuBarItems.eq(1).outerWidth(true)/2,
				// 		top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + (vars.isIE() ? 26 : 21)
				// 	});
				// 	popupSearch.find('.popup__arrow').css({
				// 		left : Math.min(menuBarItems.eq(1).offset().left - leftPopupSearch + 10, popupSearch.outerWidth() - 20)
				// 	});*/
				// 	var innerPopup = popupSearch.find('.popup__inner'),
				// 		widthInnerPopup = innerPopup.outerWidth(),
				// 		leftPopupSearch = menuBarItems.eq(1).offset().left - widthInnerPopup / 2 + menuBarItems.eq(1).outerWidth() / 2 - 30;
				// 	if (leftPopupSearch + widthInnerPopup > document.body.clientWidth) {
				// 		leftPopupSearch = win.width() - innerPopup.outerWidth();
				// 	}
				// 	popupSearch.css('position', 'fixed');
				// 	innerPopup.css({
				// 		position: 'absolute',
				// 		left: document.body.clientWidth > menuBarItems.eq(1).offset().left ? leftPopupSearch : menuBarItems.eq(1).offset().left - widthInnerPopup + menuBarItems.eq(1).outerWidth(true) / 2,
				// 		top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + 30 - win.scrollTop()
				// 	});
				// 	popupSearch.find('.popup__arrow').css({
				// 		left: menuBarItems.eq(1).offset().left - leftPopupSearch + 12
				// 	});
				// };

				var isFixedPopup = false;

				var popupSearchPosition = function() {
					popupSearch.removeClass('hidden');

					var innerPopup = popupSearch.find('.popup__inner'),
						widthInnerPopup = innerPopup.outerWidth(),
						leftPopupSearch = menuBarItems.eq(1).offset().left - widthInnerPopup / 2 + menuBarItems.eq(1).outerWidth() / 2 - 30;
					var headerInner = $('.header-inner');
					var winW = window.innerWidth || document.documentElement.clientWidth ||
						document.body.clientWidth;

					if (leftPopupSearch + widthInnerPopup > document.body.clientWidth) {
						leftPopupSearch = win.width() - innerPopup.outerWidth();
					}

					if (winW < 988) {
						if (!isFixedPopup) {
							isFixedPopup = true;

							var toolBarHeight = $('.toolbar--language').length ? $('.toolbar--language').outerHeight(true) : 0;
							var innerPopupTop = 0;
							var popupTop = 0;

							if (vars.isIE() && vars.isIE() === 7) {
								popupSearch.css('width', '987px');
								innerPopupTop = (toolBarHeight + headerInner.offset().top + headerInner.outerHeight(true) + popupSearch.find('.popup__arrow').outerHeight(true) - 5) + 'px';
								popupTop = - (headerInner.offset().top) + 'px';
							}
							else {
								popupTop = - (headerInner.offset().top + toolBarHeight) + 'px';
								if (vars.isIE() && vars.isIE() === 9) {
									innerPopupTop = (toolBarHeight + headerInner.offset().top + headerInner.outerHeight(true) + popupSearch.find('.popup__arrow').outerHeight(true) + 4) + 'px';
								}
								else {
									innerPopupTop = (toolBarHeight + headerInner.offset().top + headerInner.outerHeight(true) + popupSearch.find('.popup__arrow').outerHeight(true) - 5) + 'px';
								}
							}

							popupSearch.css({
								position: 'absolute',
								top: popupTop,
								height: win.outerHeight(true) + toolBarHeight + headerInner.offset().top + 'px'
							});

							innerPopup.css({
								position: 'absolute',
								// left: (headerInner.width() - innerPopup.width()) + 'px',
								top: innerPopupTop,
								right: 0
							});

							popupSearch.find('.popup__arrow').css({
								left: 302 + 'px'
							});
						}
					}
					else {
						isFixedPopup = false;
						popupSearch.css('width', '');
						popupSearch.css({'position': 'fixed', 'top': '', 'height': ''});

						innerPopup.css({
							position: 'absolute',
							left: document.body.clientWidth > menuBarItems.eq(1).offset().left ? leftPopupSearch : menuBarItems.eq(1).offset().left - widthInnerPopup + menuBarItems.eq(1).outerWidth(true) / 2,
							top: menuBarItems.eq(1).offset().top + menuBarItems.eq(1).innerHeight() + 28 - win.scrollTop()
						});

						popupSearch.find('.popup__arrow').css({
							left: menuBarItems.eq(1).offset().left - leftPopupSearch + 12
						});
					}
				};

				popupSearchPosition();

				/*if(window.Modernizr.cssanimations){
					popupSearch.addClass('animated fadeIn');
					popupSearch.overlay = $(config.template.overlay).appendTo(body).css('opacity', 0.5).addClass('animated fadeInOverlay').show();
					popupSearch.triggerPopup = self;
				}
				else{
					popupSearch.overlay = $(config.template.overlay).appendTo(body);
					if(vars.isIE() && vars.isIE() < 8){
						popupSearch.overlay.css({
							top: header.height()
						});
					}
					popupSearch.overlay.css('opacity', 0.5).fadeIn(config.duration.overlay);
					popupSearch.triggerPopup = self;
					popupSearch.hide().fadeIn(config.duration.popupSearch);
				}*/

				popupSearch.overlay = $(config.template.overlay).appendTo(body);

				/*if(vars.isIE() && vars.isIE() < 8){
					popupSearch.overlay.css({
						top: header.height()
					});
					doc.off('scroll.setopNavpopupSearch').on('scroll.setopNavpopupSearch', function(){
						if(win.scrollTop() >= header.height()){
							popupSearch.overlay.css('top','');
						}
						else{
							popupSearch.overlay.css({
								top: header.height()
							});
						}
					});
				}*/

				popupSearch.overlay.show();
				popupSearch.triggerPopup = self;
				popupSearch.show();

				win.off('resize.inputSearch').on('resize.inputSearch', function() {
					clearTimeout(timerPopupSearchResize);
					timerPopupSearchResize = setTimeout(function() {
						popupSearchPosition();
						// if(win.width() < config.tablet - config.width.docScroll){
						// 	self.removeClass('active');
						// 	popupSearch.addClass('hidden');
						// 	popupSearch.overlay.remove();
						// 	win.off('resize.inputSearch');
						// }
					}, 100);
				});
				popupSearch.find('.popup__close').off('click.closeSearch').on('click.closeSearch', function(e) {
					e.preventDefault();
					e.stopPropagation();
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.overlay.off('click.closeSearch').on('click.closeSearch', function() {
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.off('click.closeSearch').on('click.closeSearch', function() {
					menuBarItems.eq(1).children('a').trigger('click.showLoginPopup');
				});
				popupSearch.find('.popup__inner').off('click.closeSearch').on('click.closeSearch', function(e) {
					if (!e) {
						e = window.event;
					}
					//IE9 & Other Browsers
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					//IE8 and Lower
					else {
						e.cancelBubble = true;
					}
				});
			} else {
				self.removeClass('active');
				/*if(window.Modernizr.cssanimations){
					popupSearch.removeClass('fadeIn').addClass('animated fadeOut');
					popupSearch.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
					popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
						popupSearch.removeClass('animated fadeOut').addClass('hidden');
						popupSearch.overlay.remove();
						popupSearch.overlay = $();
						popupSearch.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				}
				else{

				}*/
				popupSearch.addClass('hidden');
				popupSearch.overlay.remove();
				popupSearch.overlay = $();
				win.off('resize.inputSearch');
				doc.off('scroll.setopNavpopupSearch');
			}
		});

		login.children('a').off('click.showLoginPopup').on('click.showLoginPopup', function(e) {
			e.preventDefault();
			menuBarItems.eq(2).children('a').trigger('click.showLoginPopup');
		});
		navbar.off('click.showMenuT').on('click.showMenuT', function(e) {
			e.preventDefault();
			e.stopPropagation();
			if (win.width() < config.tablet - config.width.docScroll) {
				if (isAnimate) {
					return;
				}
				isAnimate = true;
				if (menuT.hasClass('active')) {
					resetMenuT();
				} else {
					openMenuT();
				}
			}
		});
	};


	var loggedProfile = function() {
		if (popupLoggedProfile.length) {
			var triggerLoggedProfile = menuBar.find('ul a.status');
			var timerLoggedProfileResize = null;
			if (vars.isIE() && vars.isIE() < 8) {
				popupLoggedProfile = popupLoggedProfile.appendTo(body).css('zIndex', 100);
				// popupSearch = $('.popup--search').clone(true).appendTo(body).css('zIndex', 100);
			}

			triggerLoggedProfile.off('click.showLoggedProfile').on('click.showLoggedProfile', function(e) {
				e.preventDefault();
				e.stopPropagation();
				//hide all custom-select
				$('[data-customselect]').customSelect('hide');
				window.setTimeout(function() {
					$('input').blur();
				}, 1000);
				detectHidePopup();
				hidePopupSearch(true);
				var self = $(this);
				if (popupLoggedProfile.hasClass('hidden')) {
					self.addClass('active');
					var popupLoggedProfilePosition = function() {
						popupLoggedProfile.removeClass('hidden');
						var leftPopupLoggedProfile = triggerLoggedProfile.offset().left - popupLoggedProfile.width() / 2 + triggerLoggedProfile.children().width() / 2 /* - 30*/ ;
						if (leftPopupLoggedProfile + popupLoggedProfile.outerWidth() > win.width()) {
							leftPopupLoggedProfile = leftPopupLoggedProfile - (leftPopupLoggedProfile + popupLoggedProfile.outerWidth() - win.width());
						}

						popupLoggedProfile.css({
							left: document.body.clientWidth - triggerLoggedProfile.children().width() > triggerLoggedProfile.offset().left ? leftPopupLoggedProfile : $('.main-container').width() - popupLoggedProfile.width(),
							top: triggerLoggedProfile.children('.status-heading').offset().top - header.offset().top + triggerLoggedProfile.children('.status-heading').innerHeight() + (vars.isIE() ? 26 : 21)
						});

						if (vars.isIE() && vars.isIE() < 8) {
							popupLoggedProfile.css({
								top: triggerLoggedProfile.children('.status-heading').offset().top + triggerLoggedProfile.children('.status-heading').innerHeight() + (vars.isIE() ? 26 : 21)
							});
						}

						popupLoggedProfile.find('.popup__arrow').css({
							// left : Math.min(triggerLoggedProfile.offset().left - leftPopupLoggedProfile + 10, popupLoggedProfile.outerWidth() - 20)
							left: win.width() >= SIA.global.config.tablet + popupLoggedProfile.width() / 2 ? popupLoggedProfile.width() / 2 : (win.width() < SIA.global.config.tablet ? popupLoggedProfile.width() - triggerLoggedProfile.children().width() / 2 - 12 : popupLoggedProfile.width() / 2 + (popupLoggedProfile.width() / 2 - (win.width() - triggerLoggedProfile.offset().left - triggerLoggedProfile.children().width() / 2)))
						});
					};
					popupLoggedProfilePosition();
					/*if(window.Modernizr.cssanimations){
						popupLoggedProfile.addClass('animated fadeIn');
						popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body).css('opacity', 0.5).addClass('animated fadeInOverlay').show();
						popupLoggedProfile.triggerPopup = self;
					}
					else{

					}*/
					popupLoggedProfile.overlay = $(config.template.overlay).appendTo(body);
					/*if(vars.isIE() && vars.isIE() < 8){
						popupLoggedProfile.overlay.css({
							top: header.height()
						});
						doc.off('scroll.setopNavpopupLoggedProfile').on('scroll.setopNavpopupLoggedProfile', function(){
							if(win.scrollTop() >= header.height()){
								popupLoggedProfile.overlay.css('top','');
							}
							else{
								popupLoggedProfile.overlay.css({
									top: header.height()
								});
							}
						});
					}*/
					popupLoggedProfile.overlay.show();
					popupLoggedProfile.triggerPopup = self;
					popupLoggedProfile.show();

					win.off('resize.LoggedProfile').on('resize.LoggedProfile', function() {
						clearTimeout(timerLoggedProfileResize);
						timerLoggedProfileResize = setTimeout(function() {
							popupLoggedProfilePosition();
							// if(win.width() < config.tablet - config.width.docScroll){
							// 	self.removeClass('active');
							// 	popupLoggedProfile.addClass('hidden');
							// 	popupLoggedProfile.overlay.remove();
							// 	win.off('resize.LoggedProfile');
							// }
						}, 100);
					});
					popupLoggedProfile.overlay.off('click.closeLoggedProfile').on('click.closeLoggedProfile', function(e) {
						e.preventDefault();
						e.stopPropagation();
						triggerLoggedProfile.trigger('click.showLoggedProfile');
					});
					popupLoggedProfile.off('click.stopHide').on('click.stopHide', function(e) {
						e.stopPropagation();
					});
				} else {
					self.removeClass('active');
					/*if(window.Modernizr.cssanimations){
						popupLoggedProfile.removeClass('fadeIn').addClass('animated fadeOut');
						popupLoggedProfile.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function(){
							popupLoggedProfile.removeClass('animated fadeOut').addClass('hidden');
							popupLoggedProfile.overlay.remove();
							popupLoggedProfile.overlay = $();
							popupLoggedProfile.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{

					}*/
					popupLoggedProfile.addClass('hidden');
					popupLoggedProfile.overlay.remove();
					popupLoggedProfile.overlay = $();

					win.off('resize.LoggedProfile');
					doc.off('scroll.setopNavpopupLoggedProfile');
				}
			});
		}
	};

	// checkAllList
	var checkAllList = function(trigger, wrapper, callback) {
		var checkAll = function() {
			var checkall = true;
			if (callback) {
				callback(wrapper.find(':checkbox:checked').length);
			}
			wrapper.find(':checkbox').not('[disabled]').each(function() {
				if (!$(this).is(':checked')) {
					checkall = false;
					return;
				}
			});
			if (!wrapper.find(':checkbox').not('[disabled]').length) {
				checkall = false;
			}
			return checkall;
		};

		trigger.off('change.checkAllList').on('change.checkAllList', function() {
			if ($(this).is(':checked')) {
				wrapper.find(':checkbox').not('[disabled]').each(function() {
					$(this).prop('checked', true);
				});
			} else {
				wrapper.find(':checkbox').not('[disabled]').each(function() {
					$(this).prop('checked', false);
				});
			}
			if (callback) {
				callback($(this).is(':checked'));
			}
		});

		// wrapper.find(':checkbox').each(function() {
		// 	var self = $(this);
		// 	self.off('change.checkAllList').on('change.checkAllList', function() {
		// 		if (checkAll()) {
		// 			trigger.prop('checked', true);
		// 		} else {
		// 			trigger.prop('checked', false);
		// 		}
		// 	});
		// });
		wrapper.on('change.checkAllList', ':checkbox', function() {
			if (checkAll()) {
				trigger.prop('checked', true);
			} else {
				trigger.prop('checked', false);
			}
		});
	};
	//publick checkAllList function
	vars.checkAllList = checkAllList;

	// var loader = function(){
	// 	var imgs = $('body img').not('.img-loading'),
	// 		totalImage = imgs.length,
	// 		count = 0;
	// 	imgs.each(function(){
	// 		var img = new Image();
	// 		img.onload = function(){
	// 			count++;
	// 			if(count >= totalImage){
	// 				SIA.preloader.hide();
	// 			}
	// 		};
	// 		img.src = this.src;
	// 	});
	// };

	var initTooltip = function() {
		var tooltips = $('[data-tooltip]');
		if (tooltips.length) {
			tooltips.each(function() {
				var el = $(this);
				if (!el.data('kTooltip')) {
					el.kTooltip();
				}
			});
		}
	};

	// var addClassForWindowPhone = function(){
	// 	if(window.navigator.msMaxTouchPoints){
	// 		body.addClass('windows-phone');
	// 	}
	// };

	var initClearText = function() {
		$('input:text, input[type="tel"]').not('[data-start-date], [data-return-date], [data-oneway], [readonly]').addClear();
	};

	var triggerValidateByGroup = function() {
		// $('[data-rule-require_from_group]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group');
		// 	var elements = $(data[1]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });
		// $('[data-rule-require_from_group_if_check]').each(function() {
		// 	var self = $(this);
		// 	var data = self.data('rule-require_from_group_if_check');
		// 	var elements = $(data[3]);
		// 	elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
		// 		var validator = $(this).closest('form').data('validator');
		// 		var validatedOnce = $(this).closest('form').data('validatedOnce');
		// 		if(validator && validatedOnce) {
		// 			validator.element(self);
		// 		}
		// 	});
		// });

		// $('form').on('submit.validatedOnce', function() {
		// 	$(this).data('validatedOnce', true);
		// });

		$('[data-rule-validatedate]').each(function() {
			var self = $(this);
			var data = self.data('rule-validatedate');
			var elements = $(data.join(','));
			elements.off('change.validate blur.validate').on('change.validate blur.validate', function() {
				var el = $(this);
				var validator = el.closest('form').data('validator');
				// var validatedOnce = el.closest('form').data('validatedOnce');
				if (validator) {
					// validator.element(self);
					if (!el.is(':hidden') && !el.hasClass('ignore-validate')) {
						validator.element(el);
					}
				}
			});
		});
		$('[data-rule-requiredignorable]').each(function(i, it) {
			var self = $(it);
			var form = self.closest('form');
			var dataRule = self.data('rule-requiredignorable');
			var ignoreCheckbox = dataRule[1].replace(':checked', '');
			$(ignoreCheckbox).off('change.ignore-' + i).on('change.ignore-' + i, function() {
				// if (form.data('validator') && form.data('validatedOnce')) {
				if (form.data('validator')) {
					form.data('validator').element(self);
				}
			});
		});

		var vldDate = $('[data-rule-validatedate]');
		if (vldDate.length) {
			vldDate.each(function() {
				var self = $(this);
				var ar = self.data('rule-validatedate');
				// var formValidated = self.closest('form');
				$(ar[0]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[1]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[1]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[1]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
			});
		}
		var vldPp = $('[data-rule-checkpassport]');
		if (vldPp.length) {
			vldPp.each(function() {
				var self = $(this);
				var ar = self.data('rule-checkpassport')[1].split(',');
				// var formValidated = self.closest('form');
				$(ar[1]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[2]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
				$(ar[2]).off('blur.triggerByGroupValidate').on('blur.triggerByGroupValidate', function() {
					$(ar[0]).valid();
					$(ar[1]).valid();
					// if (formValidated.data('validatedOnce')) {
					// }
				});
			});
		}
	};

	var triggerAutocomplete = function() {
		$('[data-autocomplete] input').not(':submit').off('click.autocomplete').on('click.autocomplete', function() {
			if ($(this).is(':focus')) {
				$(this).autocomplete('search', '');
			}
		});
	};

	var initTriggerPopup = function() {
		var triggerPopup = $('[data-trigger-popup]');
		var flyingFocus = $('#flying-focus');

		triggerPopup.each(function() {
			var self = $(this);
			var passengerPage = $('.passenger-details-page');

			if (typeof self.data('trigger-popup') === 'boolean') {
				return;
			}

			var popup = $(self.data('trigger-popup'));
			var measureScrollbar = (function(){
				var a = document.createElement('div');
				a.className = 'modal-scrollbar-measure';
				body.append(a);
				var b = a.offsetWidth - a.clientWidth;
				return $(a).remove(), b;
			})();

			if (!popup.data('Popup')) {
				var browsehappy = $('.browsehappy').clone();
				popup.Popup({
					overlayBGTemplate: config.template.overlay,
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]',
					beforeShow: function(){
						if(passengerPage.length){
							$('.booking-summary.active').addClass('reset-zindex');
						}
						if (vars.isIE() < 8){
							browsehappy.appendTo($('.main-container'));
						}
					},
					afterShow: function(that) {
						flyingFocus = $('#flying-focus');
						if (flyingFocus.length) {
							flyingFocus.remove();
						}

						that.modal.attr({
							'open' : 'true',
							'role' : 'dialog'
						});

						var help = 'Beginning of dialog window. Press escape to cancel and close this window.'; // TODO l10n.prop
						that.modal.find('#popup--help').remove();
						that.modal.prepend('<p id="popup--help" class="says">' + help + '</p>');

						that.modal.find('.popup__content').attr({
							'role' : 'document', // NVDA doesn't read dialogs see https://github.com/nvaccess/nvda/issues/4493
							'aria-describedby' : 'popup--help'
						});
					},
					afterHide: function(){
						if(passengerPage.length){
							$('.booking-summary.active').removeClass('reset-zindex');
						}
						container.css('padding-right', '');
						if (vars.isIE() < 8){
							$('.main-container').find('.browsehappy').remove();
						}

						if(self.parent('span.link-4').is('[tabindex]')) {
							self.parent('span.link-4').focus();
						} else {
							self.focus();
						}
					}
				});
			}

			self.off('click.showPopup').on('click.showPopup', function(e) {
				e.preventDefault();
				if (!self.hasClass('disabled')) {
					container.css('padding-right', measureScrollbar);
					popup.Popup('show');
				}
			});
		});
	};

	var loginFormValidation = function() {
		$('.form--login').each(function(){
			$(this).validate({
				focusInvalid: true,
				errorPlacement: vars.validateErrorPlacement,
				success: vars.validateSuccess
			});
		});
	};

	var globalFormValidation = function() {
		$('[data-global-form-validate]').validate({
			focusInvalid: true,
			errorPlacement: vars.validateErrorPlacement,
			success: vars.validateSuccess,
			onfocusout: vars.validateOnfocusout,
			invalidHandler: vars.invalidHandler
		});
	};

	var initDisabledLink = function() {
		var disabledLink = $('[data-disabled-link]');
		if (disabledLink.length) {
			disabledLink.off().on('click', function(e) {
				if ($(this).hasClass('disabled')) {
					e.preventDefault();
					e.stopImmediatePropagation();
				}
			});
		}
	};

	// var initAutocompleteSearch = function() {
	// 	var term = '';
	// 	var timerAutocompleteLangOpen = null;
	// 	$('.form-search').each(function() {
	// 		var field = $(this).find('input:text');
	// 		var searchAutoComplete = field.autocomplete({
	// 			open: function() {
	// 				var self = $(this);
	// 				self.autocomplete('widget').hide();
	// 				clearTimeout(timerAutocompleteLangOpen);
	// 				timerAutocompleteLangOpen = setTimeout(function(){
	// 					self.autocomplete('widget').show().css({
	// 						'left': self.offset().left,
	// 						'top': self.offset().top + self.outerHeight(true)
	// 					});
	// 					self.autocomplete('widget')
	// 					.jScrollPane({
	// 						scrollPagePercent				: 10
	// 					}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
	// 						e.preventDefault();
	// 						e.stopPropagation();
	// 					});
	// 				}, 100);
	// 			},
	// 			source: function(request, response) {
	// 				term = request.term;
	// 				$.get('ajax/autocomplete-search.json', function(data) {
	// 					var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
	// 					var source = $.grep(data.searchSuggests, function(item){
	// 						return matcher.test(item);
	// 					});
	// 					response(source);
	// 				});
	// 			},
	// 			search: function() {
	// 				var self = $(this);
	// 				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
	// 			},
	// 			close: function() {
	// 				var self = $(this);
	// 				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
	// 			}
	// 		}).data('ui-autocomplete');

	// 		searchAutoComplete._renderItem = function(ul, item) {
	// 			if (!ul.hasClass('auto-suggest')) {
	// 				ul.addClass('auto-suggest');
	// 			}
	// 			var termLength = term.length;
	// 			var label = item.label;
	// 			var liContent = '<span class="text-suggest">' + label.substr(0, termLength) +
	// 				'</span>' + label.substr(termLength, label.length - 1);
	// 			return $('<li class="autocomplete-item">')
	// 				.attr('data-value', item.value)
	// 				.append('<a href="javascript:void(0);">' + liContent + '</a>')
	// 				.appendTo(ul);
	// 		};

	// 		searchAutoComplete._move = function( direction ) {
	// 			var item, previousItem,
	// 				last = false,
	// 				api = this.menu.element.data('jsp'),
	// 				li = $(),
	// 				minus = null,
	// 				currentPosition = api.getContentPositionY();

	// 			switch(direction){
	// 				case 'next':
	// 					if(this.element.val() === ''){
	// 						api.scrollToY(0);
	// 						li = this.menu.element.find('li:first');
	// 						item = li.addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					else{
	// 						previousItem = this.menu.element.find('li.active').removeClass('active');
	// 						li = previousItem.next();
	// 						item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					if(!item){
	// 						last = true;
	// 						li = this.menu.element.find('li').removeClass('active').first();
	// 						item = li.addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					this.term = item.value;
	// 					this.element.val(this.term);
	// 					if(last){
	// 						api.scrollToY(0);
	// 						last = false;
	// 					}
	// 					else{
	// 						currentPosition = api.getContentPositionY();
	// 						minus = li.position().top + li.innerHeight();
	// 						if(minus - this.menu.element.height() > currentPosition){
	// 							api.scrollToY(Math.max(0, minus - this.menu.element.height()));
	// 						}
	// 					}
	// 					break;
	// 				case 'previous':
	// 					if(this.element.val() === ''){
	// 						last = true;
	// 						item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					else{
	// 						previousItem = this.menu.element.find('li.active').removeClass('active');
	// 						li = previousItem.prev();
	// 						item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					if(!item){
	// 						last = true;
	// 						item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
	// 					}
	// 					this.term = item.value;
	// 					this.element.val(this.term);
	// 					if(last){
	// 						api.scrollToY(this.menu.element.find('.jspPane').height());
	// 						last = false;
	// 					}
	// 					else{
	// 						currentPosition = api.getContentPositionY();
	// 						if(li.position().top <= currentPosition){
	// 							api.scrollToY(li.position().top);
	// 						}
	// 					}
	// 					break;
	// 			}
	// 		};

	// 		field.autocomplete('widget').addClass('autocomplete-menu');

	// 		field.off('blur.autocomplete');

	// 		field.off('focus.highlight').on('focus.highlight', function (e) {
	// 			e.stopPropagation();
	// 			var self = $(this);
	// 			self.closest('.custom-select').addClass('focus');

	// 			doc.off('mousedown.hideAutocompleteLanguage').on('mousedown.hideAutocompleteLanguage', function(e){
	// 				if(!$(e.target).closest('.ui-autocomplete').length){
	// 					field.closest('.custom-select').removeClass('focus');
	// 					field.autocomplete('close');
	// 				}
	// 			});
	// 		});

	// 		field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll').on('scroll.preventScroll mousewheel.preventScroll', function(e){
	// 			e.stopPropagation();
	// 		});

	// 		field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
	// 			e.stopPropagation();
	// 		});

	// 		field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
	// 			if(e.which === 13){
	// 				e.preventDefault();
	// 				if(field.autocomplete('widget').find('li').length === 1){
	// 					field.autocomplete('widget').find('li').trigger('click');
	// 					return;
	// 				}
	// 				field.autocomplete('widget').find('li.active').trigger('click');
	// 			}
	// 		});
	// 	});
	// };

	var globalFun = function() {
		animationNavigationOnDesktop();
		selectLanguage();
		loginPopup();
		loggedProfile();
		menuTablet();
		// loader();
		initTooltip();
		// addClassForSafari();
		initClearText();
		// addClassForWindowPhone();
		triggerValidateByGroup();
		triggerAutocomplete();
		initTriggerPopup();
		loginFormValidation();
		globalFormValidation();
		initDisabledLink();

		// initAutocompleteSearch();
	};

	var IEBrowser = function() {
		var homePage = $('.home-page');
		if (homePage.length) {
			var travelWidgetTabItems = $('.travel-widget-inner > .tab > li', homePage);
			if (travelWidgetTabItems.length) {
				vars.addClassOnIE(travelWidgetTabItems, 'all');
			}
		}
		vars.addClassOnIE($('.footer-bottom').children(), 'last');

		// $('.booking-info').each(function(){
		// 	addClassOnIE($(this).children(), 'all');
		// });
		if ($('.booking-item').length) {
			$('.booking-item').each(function() {
				vars.addClassOnIE($(this).find('.booking-info .booking-info-item'), 'all');
			});
		} else {
			$('.booking-info').each(function() {
				vars.addClassOnIE($(this).find('.booking-info-item'), 'all');
			});
		}

		if (vars.isIE() < 8) {
			$('<span class="booking-nav__arrow"></span>').prependTo($('.booking-nav > a'));

			$('.sia-breadcrumb-1').find('.breadcrumb-item').each(function() {
				$('<span class="breadcrumb-item__arrow"></span>').prependTo($(this));
			});
		}
	};

	// it is used on mobile
	var backgroundImage = function() {
		$('.full-banner--img, #banner-slider .slide-item').each(function(idx) {
			var prImg = $(this);
			var img = prImg.find('img');

			if (!body.hasClass('home-page')) {
				var nImage = new Image();
				nImage.onload = function() {
					var nWidth = nImage.width;
					var nHeight = nImage.height;
					var rate = nHeight/nWidth;

					img.data('img-rate', rate);

					if (win.width() > 1280) {
						var settedHeightEls = img.closest('.flexslider').length ?
							img.closest('.flexslider').add(img.closest('.slide-item')) : img;
						settedHeightEls.height(win.width() * rate);
					}
				};

				nImage.src = img.attr('src');
			}

			prImg.css({
				'background': 'url(' + img.attr('src') + ') 50% 50% no-repeat',
				'backgroundSize': 'cover',
				'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + img.attr('src') + '", sizingMethod="scale")',
				'-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' + img.attr('src') + '", sizingMethod="scale")'
			});

			img.attr('src', config.imgSrc.transparent);

			var resizeFullBanner = function(){
				var winW = win.width();
				if (!body.hasClass('home-page')) {
					var settedHeightEls = img.closest('.flexslider').length ?
						img.closest('.flexslider').add(img.closest('.slide-item')) : img;

					if (winW > 1280) {
						var rate = parseFloat(img.data('img-rate'));
						settedHeightEls.height(win.width() * rate);
					}
					else {
						settedHeightEls.css('height', '');
					}
				}
			};

			win
				.off('resize.fullBannerImg' + idx)
				.on('resize.fullBannerImg' + idx, resizeFullBanner);

			if (prImg.is('.full-banner--img')) {
				prImg.removeClass('visibility-hidden');
			}
		});
	};

	$.fn.resetForm = function() {
		return this.each(function() {
			// $(this).data('validatedOnce', null);
			$(this).find('.error:input').parents('.error').removeClass('error');
			$(this).find('.text-error').remove();
		});
	};
	// vars.validationRuleByGroup = [];
	vars.validateErrorPlacement = function(error, element) {
		// if (!$(element).closest('form').data('validatedOnce')) {
		// 	return;
		// }
		if (element.is(':disabled')) { return; }
		// if (!$(element).closest('form').data('validatedOnce')) {
		// 	return;
		// }
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		var inputsInGroup = group.find(':input:visible').filter(function() {
			return (jQuery.isEmptyObject($(this).rules()) === false);
		});
		var validator = $(element).closest('form').data('validator');
		var validationRuleByGroup = vars.validationRuleByGroup;
		var errorList = validator.errorList;

		var appendError = function(container) {
			container.addClass('error');

			// fix issue: elements are not required validation in form-group
			var ignoreEl = container.find('[data-ignore]');
			if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
				ignoreEl.closest('.grid-col').addClass('ignore');
			}

			if (!container.children('.text-error').length && error.text()) {
				$(config.template.labelError)
					.appendTo(container)
					.find('span')
					.text(error.text());

				$(element).data('errorMessage', container.children('.text-error'));
			}
			else {
				if (error.text()) {
					container.children('.text-error').find('span').text(error.text());
				}
				else {
					container.children('.text-error').remove();
				}
			}
		};

		var theRestIsOk = function() {
			return inputsInGroup.not($(element)).filter(function() {
				return (validator.check($(this)) === false);
			}).length === 0;
		};

		//If it's not validating by group
		if (!isValidateByGroup) {
			appendError(gridInner);

			//check if the current rule is a rule-by-group
			var isRuleByGroup = false;
			if (validationRuleByGroup) {
				for (var i = errorList.length - 1; i >= 0; i--) {
					if ($(element).is(errorList[i].element) && validationRuleByGroup.indexOf(errorList[i].method) >= 0) {
						isRuleByGroup = true;
						break;
					}
				}
			}

			//if current rule is a rule-by-group and it's fail
			if (isRuleByGroup) {
				//and the rest input in group are ok
				if (theRestIsOk()) {
					//remove old error message in group if exists
					if (group.children('.text-error').length) {
						group.children('.text-error').remove();
					}
					gridInner.removeClass('error');
					//append new error message to group
					group.append($(element).data('errorMessage')).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}
				} else {
					//if the rest input are not ok
					//just show separate error and temporarily hide group error
					gridInner.removeClass('error').children('.text-error').remove();
				}
			} else {
				//if it's another separate error
				if (error.text() !== '') {
					//remove group error
					if (inputsInGroup.length > 1) {
						group.removeClass('error').children('.text-error').remove();

						// fix issue: elements are not required validation in form-group
						var ignoreEl = group.find('[data-ignore]');
						if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
							ignoreEl.closest('.grid-col').removeClass('ignore');
						}
					}
				} else {
					//if it's ok, need to detect it's ok for rule-group or rule-element
					for (var z = validationRuleByGroup.length - 1; z >= 0; z--) {
						if ($(element).rules()[validationRuleByGroup[z]]) {
							group.removeClass('error').children('.text-error').remove();

							// fix issue: elements are not required validation in form-group
							var ignoreEl = group.find('[data-ignore]');
							if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
								ignoreEl.closest('.grid-col').removeClass('ignore');
							}

							break;
						}
					}
				}
			}
		} else {
			//validating by group
			appendError(gridInner);
			if (inputsInGroup.index($(element)) === 0) {
				if (!$(element).data('elementValidator')) {
					$(element).data('elementValidator', $.extend({}, validator));
				}
			}
			if (!$(element).data('being-validated')) {
				inputsInGroup.data('being-validated', true);
				inputsInGroup.each(function() {
					inputsInGroup.eq(0).data('elementValidator') && inputsInGroup.eq(0).data('elementValidator').element($(this));
				});
				inputsInGroup.data('being-validated', false);
				group.children('.text-error').remove();
				var firstError = group.find('.text-error').filter(function() {
					return ($(this).find('span').text() !== '');
				}).first();

				if (firstError.length) {
					group.append(firstError).addClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && !ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').addClass('ignore');
					}

					group.find('.text-error').not(firstError).remove();
				} else {
					group.removeClass('error');

					// fix issue: elements are not required validation in form-group
					var ignoreEl = group.find('[data-ignore]');
					if (ignoreEl.length && ignoreEl.closest('.grid-col').hasClass('ignore')) {
						ignoreEl.closest('.grid-col').removeClass('ignore');
					}

					group.find('.error:not(:input)').removeClass('error');
				}
			}
		}
	};

	vars.validateSuccess = function(label, element) {
		var group = $(element).closest('.form-group, [data-validate-row]');
		var isValidateByGroup = group.data('validate-by-group');
		var isGroupGrid = group.is('.grid-row') || group.is('[data-validate-row]');
		var gridInner = isGroupGrid ? $(element).closest('.grid-col, [data-validate-col]') : group;
		// var inputsInGroup = group.find(':input');
		// var validator = $(element).closest('form').data('validator');

		var removeMessage = function(container) {
			container.removeClass('error');
		};

		//If it's not validating by group
		if (!isValidateByGroup) {
			removeMessage(gridInner);
		}
	};

	vars.validateOnfocusout = function(element) {
		var formEl = $(element).closest('form'),
			validator = formEl.data('validator');
		var container = $(element).closest('.error');
		if (container.data('validate-by-group')) {
			if (container.find('.error:input').not($(element)).length) {
				return;
			}
		}
		// if (formEl.data('validatedOnce')) {
		// }

		if (vars.validationRuleByGroup) {
			var group = $(element).closest('.form-group, [data-validate-row]'),
				strRules = '';
			for (var i = vars.validationRuleByGroup.length - 1; i >= 0; i--) {
				strRules += '[data-rule-' + vars.validationRuleByGroup[i] + ']' + (i !== 0 ? ',' : '');
			}
			if (strRules) {
				var ruleEl = group.find(strRules);
				if (ruleEl.length) {
					// ruleEl.trigger('change');
					validator.element(element);
					validator.element(ruleEl);
					return;
				}
			}
		}

		if (this.element(element) && !this.checkable(element)) {
			$(element)
				.closest('.error').removeClass('error').addClass('success')
				.children('.text-error').remove();
		}
		if (this.element(element) && !this.checkable(element)) {
			$(element)
				.closest('.error').removeClass('error').addClass('success')
				.children('.text-error').remove();
		}
	};

	vars.invalidHandler = function(form, validator) {
		var errors = validator.numberOfInvalids();
		if (errors) {
			var errorColEl = $(validator.errorList[0].element).closest('.grid-col');
			if (errorColEl.length) {
				win.scrollTop(errorColEl.offset().top - 40);
			}
		}
	};

	return {
		vars: vars,
		config: config,
		globalFun: globalFun,
		IEBrowser: IEBrowser,
		backgroundImage: backgroundImage
	};
})();

SIA.preloader = (function() {
	var preload = $('.overlay-loading');
	var show = function(callback) {
		preload.removeClass('hidden');
		if (typeof(callback) === 'function') {
			callback();
		}
	};

	var hide = function(callback) {
		if (!preload.data('never-stop')) {
			// if(preload.data('planloading') === undefined) {
				preload.addClass('hidden');
			// }
			preload.trigger('afterHide.ready');
		}
		if (typeof(callback) === 'function') {
			callback();
		}
	};

	var isVisible = function() {
		return preload.is(':visible');
	};

	return {
		show: show,
		hide: hide,
		isVisible: isVisible
	};
})();

SIA.planLoading = (function() {

  var interBlock = $('.interstitial-block'),
      win = $(window);

  var init = function(callback) {

    $('.progress-flying').css({
      'background': 'url(images/Older_browser.gif) 0 0 no-repeat'
    });

    $('.interstitial-1, .interstitial-2, .circle').hide();

  }

  var rePosition = function(callback) {

    if (typeof(callback) === 'function') {
      callback();
    };

  }

  return {
    rePosition: rePosition,
    init: init
  };

})();

(function($) {

  SIA.planLoading.init();

	$(document).ready(function() {
		// SIA.global = global();
		// SIA.preloader = preloader();
		var body = $(document.body);

		var loadScript = function(url, callback, location) {
			if (!url || (typeof url !== 'string')) {
				return;
			}
			var script = document.createElement('script');
			//if this is IE8 and below, handle onload differently
			if (typeof document.attachEvent === 'object') {
				script.onreadystatechange = function() {
					//once the script is loaded, run the callback
					if (script.readyState === 'loaded' || script.readyState === 'complete') {
						if (callback) {
							callback();
						}
					}
				};
			} else {
				//this is not IE8 and below, so we can actually use onload
				script.onload = function() {
					//once the script is loaded, run the callback
					if (callback) {
						callback();
					}
				};
			}
			//create the script and add it to the DOM
			script.src = url;
			document.getElementsByTagName(location ? location : 'head')[0].appendChild(script);
		};

		var urlScript = {
			scripts: {
				'underscore': 'scripts/underscore.js',
				'JSONglobal': 'scripts/JSON-global.js',
				'jqueryui': 'scripts/jquery-ui-1.10.4.custom.js',
				'validate': 'scripts/jquery.validate.js',
				'slick': 'scripts/slick.js',
				'mousewheel': 'scripts/jquery.mousewheel.js',
				'jscrollpane': 'scripts/jquery.jscrollpane.js',
				'inputmask': 'scripts/jquery.inputmask.js',
				'accounting': 'scripts/accounting.min.js',
				'placeholder': 'scripts/jquery.placeholder.js',
				'cleartext': 'scripts/clear-text.js',
				'highcharts': 'scripts/highcharts.js',
				'isotope': 'scripts/isotope.js',
				'shuffle': 'scripts/jquery.shuffle.js',
				'youtubeAPI': 'https://www.youtube.com/iframe_api',
				'flowplayerAPI': '//releases.flowplayer.org/js/flowplayer-3.2.13.min.js',
				'tiff': 'scripts/tiff.min.js',
				'recaptcha': 'https://www.google.com/recaptcha/api.js',
				'allyjs': 'https://cdn.jsdelivr.net/ally.js/1.3.0/ally.min.js'
			},
			modules: {
				'flightTableBorder': 'scripts/flight-table-border.js',
				'forceInput': 'scripts/force-input.js',
				'multipleSubmit': 'scripts/multiple-submit.js',
				'contact': 'scripts/contact.js',
				'simpleSticky': 'scripts/simple-sticky.js',
				'coachBeta': 'scripts/coach-beta.js',
				'sticky': 'scripts/sticky.js',
				'bar': 'scripts/bar.js',
				'filterEntertainment': 'scripts/filter-entertainment.js',
				'tab': 'scripts/tab.js',
				'blockScroll': 'scripts/block-scroll.js',
				'seatmap': 'scripts/seat-map.js',
				'accordion': 'scripts/accordion.js',
				'countCharsLeft': 'scripts/count-chars-left.js',
				'addon': 'scripts/add-on.js',
				'atAGlance': 'scripts/at-a-glance.js',
				'initAutocompleteCity': 'scripts/autocomplete-city.js',
			  'autocompleteAirport': 'scripts/autocomplete-airport.js',
			  'autocompleteCar': 'scripts/autocomplete-car.js',
				'bookingSummnary': 'scripts/booking-summary.js',
				'CIBBookingSummary': 'scripts/cib-booking-summary.js',
				'cibConfirmation': 'scripts/cib-confirmation.js',
				'KFClaimMissingMiles': 'scripts/claim-missing-miles.js',
				'KFMileExpiring': 'scripts/expiring-miles.js',
				'KFFavourite': 'scripts/favourites.js',
				'flightCalendar': 'scripts/flight-calendar.js',
				'flightSchedule': 'scripts/flight-schedule.js',
				'flightSearchCalendar': 'scripts/flight-search-calendar.js',
				'flightSelect': 'scripts/flight-select.js',
				'flightStatus': 'scripts/flight-status.js',
				'home': 'scripts/home.js',
				'darkSiteHome': 'scripts/dark-site-home.js',
				'KFMileHowToEarn': 'scripts/how-to-earn.js',
				'KFMileHowToUse': 'scripts/how-to-use.js',
				'flightScheduleTrip': 'scripts/init-flight-schedule-trip.js',
				'initPersonTitles': 'scripts/init-personTitles.js',
				'initTabMenu': 'scripts/init-tabMenu.js',
				'KFCheckIns': 'scripts/kf-check-ins.js',
				'KFFlightHistory': 'scripts/kf-flight-history.js',
				'KFMessageForward': 'scripts/kf-message-forward.js',
				'KFMessage': 'scripts/kf-message.js',
				'KFPartnerProgramme': 'scripts/kf-partner-programme.js',
				'KFPersonalDetail': 'scripts/kf-personal-detail.js',
				'KFRedemptionNominee': 'scripts/kf-redemption-nominee-add.js',
				'KFTicketReceipt': 'scripts/kf-tickets-and-receipts.js',
				'KFUpcomingFlights': 'scripts/kf-upcoming-flights.js',
				'multicity': 'scripts/multicity.js',
				'newsTickerContent': 'scripts/newsTickerContent.js',
				'orbConfirmation': 'scripts/orb-confirmation.js',
				'ORBFlightSelect': 'scripts/orb-flight-search.js',
				'passengerDetail': 'scripts/passenger-detail.js',
				'payment': 'scripts/payment.js',
				'promotionKrisflyer': 'scripts/promotion-krisflyer.js',
				'promotion': 'scripts/promotion.js',
				'KFRedeemMiles': 'scripts/redeem-miles.js',
				'roundTrip': 'scripts/round-trip.js',
				'selectCabin': 'scripts/select-cabin.js',
				'selectReturnDate': 'scripts/select-return-date.js',
				'KFStatement': 'scripts/statements.js',
				'stickySidebar': 'scripts/sticky-sidebar.js',
				// 'FormCheckChange' : 'scripts/form-check-change.js',
				'ORBBookingSummary': 'scripts/orb-booking-summary.js',
				'initAutoFilledPassenger': 'scripts/init-auto-filled-passenger.js',
				'initCorrectDate': 'scripts/init-corect-date.js',
				'kfRegistration': 'scripts/kf-registration.js',
				'sqcRegistration': 'scripts/sqc-registration.js',
				'specialAssistance': 'scripts/special-assistance.js',
				'manageBooking': 'scripts/manage-booking.js',
				'excessBaggage': 'scripts/excess-baggage.js',
				'mbConfirmation': 'scripts/mb-confirmation.js',
				'mbSelectMeal': 'scripts/mb-select-meals.js',
				'MBBookingSummary': 'scripts/mb-booking-summary.js',
				'SQCExpenditure': 'scripts/sqc-expenditure.js',
				'sqcUpcomingFlight': 'scripts/sqc-upcoming-flight.js',
				'SQCUser': 'scripts/sqc-user.js',
				'SQCAddUser': 'scripts/sqc-add-user.js',
				'sqcAtAGlance': 'scripts/sqc-at-a-glance.js',
				'SQCFlightHistory': 'scripts/sqc-flight-history.js',
				'sqcSavedTrips': 'scripts/sqc-saved-trips.js',
				'desWhereTo': 'scripts/des-where-to-stay.js',
				'desEntry': 'scripts/des-entry.js',
				'milestonesRewards': 'scripts/milestones-rewards.js',
				'DESCityGuide': 'scripts/des-city-guide.js',
				'staticContentKrisflyer': 'scripts/static-content-krisflyer.js',
				'sshHotel': 'scripts/ssh-hotel.js',
				'sshSelection': 'scripts/ssh-selection.js',
				'sshAdditional': 'scripts/ssh-additional.js',
				'mbChangeFlight': 'scripts/mb-change-flight.js',
				'multiTabsWithLongText': 'scripts/multitabs-with-long-text.js',
				'staticContentGeneric': 'scripts/static-content-generic.js',
				'staticContentHeritage': 'scripts/static-content-heritage.js',
				'kfUnsubscribe': 'scripts/kf-unsubscribe.js',
				'feedBack': 'scripts/feed-back.js',
				'kfVoucherRedemption': 'scripts/kf-voucher-redemption.js',
				'youtubeApp': 'scripts/youtube-app.js',
				'darkSiteStatements': 'scripts/dark-site-statements.js',
				'landingSearchFlight': 'scripts/landing-search-flight.js',
				'grayscaleModule': 'scripts/grayscale-module.js',
				'autocompleteSearch': 'scripts/autocomplete-search.js',
				'chatWidget': 'scripts/chat-widget.js',
				'faqs': 'scripts/faqs.js',
				'plusOrMinusNumber': 'scripts/plus-or-minus-number.js',
				'radioTabsChange': 'scripts/radio-tabs-change.js',
				'addBaggage': 'scripts/add-baggage.js',
				'promotionsPackages': 'scripts/promotions-packages.js',
				'carouselFade': 'scripts/carousel-fade.js',
				'carouselSlide': 'scripts/carousel-slide.js',
				'pricePoints': 'scripts/price-points.js',
				'checkImage': 'scripts/check-image.js',
				'hotelListing': 'scripts/hotel-listing.js',
				'disableValue': 'scripts/disable-value.js',
				'togglePrivacy': 'scripts/toggle-privacy.js',
				'promotionComponent': 'scripts/promotion-component.js',
				'bookingWidget': 'scripts/booking-widget.js',
				'sortTableContent': 'scripts/sort-table-content.js',
				'autocompleteCountryCity': 'scripts/autocomplete-country-city.js',
				'flowPlayerApp': 'scripts/flowplayer-app.js',
				'redemptionNominee': 'scripts/redemption-nominee.js',
				'bookingMileClaim': 'scripts/kf-retroactive-mile-claim.js',
				'googleMap': 'scripts/googlemap.js',
				'baiduMap': 'scripts/baidumap.js',
				'selectMeals': 'scripts/mb-flight-select-meal.js',
				'donateMiles': 'scripts/donate-miles.js',
				'voucherRewards': 'scripts/voucher-rewards.js',
				'voucherStored': 'scripts/voucher-cookies.js'
			},
			getScript: function(script) {
				return this.scripts[script];
			}
		};
		var comment = [
			SIA.global.globalFun,
			// SIA.footer,
			SIA.cookiesUse,
			SIA.autoCompleteLanguage,
			SIA.highlightInput,
			SIA.fixPlaceholder,
			SIA.initCustomSelect,

			// IE
			SIA.global.IEBrowser,
			// set backgroundImage
			SIA.global.backgroundImage,
			// accessibility
			SIA.initSkipTabIndex,
			// social
			SIA.socialNetworkAction,
			SIA.initLangToolbar,
			// checkbox confirm
			SIA.initConfirmCheckbox,
			SIA.initToggleButtonValue
		];

		var initFunction = comment;
		var requireScript = ['jqueryui', 'mousewheel', 'validate', 'jscrollpane', 'cleartext', 'placeholder'];
		var concatInitFunctionDone = false;
		var totalLoadedModules = 0;
		var loadedModules = 0;
		var modulesToLoad = [];
		var dashboard = {
			page: {
				'flightTableBorder': [
					'flightTableBorder'
				],
				'forceInput': [
					'forceInput'
				],
				'multipleSubmit': [
					'multipleSubmit'
				],
				'contact': [
					'contact',
					'initAutocompleteCity'
				],
				'simpleSticky': [
					'simpleSticky'
				],
				'coachBeta': [
					'coachBeta'
				],
				'sticky': [
					'sticky'
				],
				'bar': [
					'bar'
				],
				'grayscaleModule': [
					'grayscaleModule'
				],
				'darkSiteStatements': [
					'darkSiteStatements'
				],
				'home': [
					'home',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					'autocompleteAirport',
					'autocompleteCar',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip'
				],
				'darkSiteHome': [
					'darkSiteHome',
					'roundTrip',
					'newsTickerContent',
					'initAutocompleteCity',
					// 'initTabMenu',
					'selectReturnDate',
					'selectCabin',
					'flightScheduleTrip'
				],
				'passengerDetail': [
					'initPersonTitles',
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initAutocompleteCity',
					'initAutoFilledPassenger',
					'CIBBookingSummary'
				],
				'orbPassengerDetail': [
					'passengerDetail',
					'stickySidebar',
					// 'initTabMenu',
					'initPersonTitles',
					'initAutocompleteCity',
					'ORBBookingSummary',
					'initAutoFilledPassenger'
				],
				'seatMap': [
					'stickySidebar',
					// 'initTabMenu',
					'CIBBookingSummary',
					'seatmap'
				],
				'flightStatus': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'flightStatus'
					// 'initTabMenu'
				],
				'flightSchedule': [
					'flightSchedule',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'initAutocompleteCity',
					'selectCabin'
					// 'initTabMenu'
				],
				'flightCalendar': [
					'flightCalendar',
					'CIBBookingSummary'
				],
				'promotion': [
					'promotion'
				],
				'faresDetailsPage': [
					'roundTrip',
					'selectCabin',
					'promotion'
				],
				'promotionKrisflyer': [
					'promotionKrisflyer'
				],
				'bookingSummnary': [
					'bookingSummnary',
					'initAutocompleteCity'
					// 'initTabMenu'
				],
				'flightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'flightSelect',
					'CIBBookingSummary'
				],
				'orbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'ORBBookingSummary'
				],
				'mbFlightSelect': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'MBBookingSummary'
				],
				'mbFlightSelectSf': [
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'ORBFlightSelect',
					'CIBBookingSummary'
				],
				'flightSearchCalendar': [
					'flightSearchCalendar'
				],
				'multicity': [
					'initAutocompleteCity',
					'selectReturnDate',
					'selectCabin',
					'multicity'
				],
				'payment': [
					'payment',
					'initAutocompleteCity',
					'CIBBookingSummary'
				],
				'orbPayment': [
					'payment',
					'initAutocompleteCity',
					'ORBBookingSummary'
				],
				'mbPayment': [
					'payment'
				],
				'addon': [
					'addon',
					'CIBBookingSummary',
					'googleMap',
					'baiduMap',
					'multiTabsWithLongText',
					'initTabMenu',
					'plusOrMinusNumber',
					'roundTrip',
					'autocompleteCar'
				],
				'orbAddOns': [
					'addon',
					'ORBBookingSummary'
				],
				'cibConfirmation': [
					'cibConfirmation',
					'CIBBookingSummary'
				],
				'atAGlance': [
					'atAGlance'
				],
				'kfRedemptionNominee': [
					'KFRedemptionNominee',
					'initPersonTitles',
					'initAutocompleteCity'
					// 'initCorectDate'
				],
				'kfPartnerProgramme': [
					'KFPartnerProgramme'
				],
				'accordion': [
					'accordion'
				],
				'countCharsLeft': [
					'countCharsLeft'
				],
				'howToEarn': [
					'initAutocompleteCity',
					'KFMileHowToEarn'
				],
				'howToUse': [
					'initAutocompleteCity',
					'KFMileHowToUse',
					'kfVoucherRedemption'
				],
				'redeemMiles': [
					'initAutocompleteCity',
					'KFRedeemMiles'
				],
				'kfExpiringMiles': [
					'KFMileExpiring'
				],
				'kfStatement': [
					'KFStatement'
				],
				'KFFavourite': [
					'initAutocompleteCity',
					'KFFavourite'
				],
				// 'milesPage': [
				// 	'initAutocompleteCity'
				// ],
				'kfMessage': [
					'KFMessage'
				],
				'kfMessageForward': [
					'KFMessageForward'
				],
				'kfPersonalDetail': [
					'KFPersonalDetail',
					'initAutocompleteCity',
					'initPersonTitles'
				],
				'kfClaimMissingMiles': [
					'KFClaimMissingMiles',
					'initAutocompleteCity'
				],
				'kfUpcomingFlights': [
					'KFUpcomingFlights'
				],
				'kfFlightHistory': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'KFFlightHistory'
				],
				'KFTicketReceipt': [
					'KFTicketReceipt'
				],
				'KFCheckIns': [
					'KFCheckIns'
				],
				'ORBConfirmation': [
					'orbConfirmation',
					'ORBBookingSummary'
				],
				// 'FormCheckChange': [
				// 	'FormCheckChange'
				// ],
				'ORBFlightSchedule': [
					'flightSearchCalendar'
				],
				// 'KFCreateNewPin': [
				// 	'initCorectDate'
				// ],
				'KFRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'kfRegistration'
				],
				'SQCRegistration': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'sqcRegistration'
				],
				'specialAssistance': [
					'initAutocompleteCity',
					'initPersonTitles',
					// 'initCorectDate',
					'specialAssistance'
				],
				'cancelWaitlistBooking': [
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'ManageBooking': [
					'manageBooking'
				],
				'ExcessBaggage': [
					'excessBaggage'
				],
				'MBConfirmation': [
					'mbConfirmation',
					'manageBooking'
				],
				'MBReview': [
					'manageBooking',
					'initAutocompleteCity',
					'roundTrip',
					'flightScheduleTrip',
					'selectReturnDate',
					'selectCabin',
					'MBBookingSummary'
				],
				'MBSelectMeal': [
					'mbSelectMeal',
					'stickySidebar'
				],
				'MBSelectMealLand': [
					'stickySidebar',
					'accordion',
					'selectMeals'
				],
				'SQCExpenditure': [
					'SQCExpenditure'
				],
				'sqcUpcomingFlight': [
					'sqcUpcomingFlight'
				],
				'SQCUser': [
					'SQCUser'
				],
				'SQCAddUser': [
					'SQCAddUser',
					'initAutocompleteCity',
					'initPersonTitles'
					// 'initCorectDate'
				],
				'SQCAtAGlance': [
					'sqcAtAGlance'
				],
				'SQCFlightHistory': [
					'initAutocompleteCity',
					'SQCFlightHistory'
				],
				'SQCSavedTrips': [
					'sqcSavedTrips'
				],
				'initCorrectDate': [
					'initCorrectDate'
				],
				'desWhereTo': [
					'desWhereTo',
					'roundTrip',
					'initAutocompleteCity',
					'selectReturnDate'
				],
				'desEntry': [
					'desEntry',
					'initAutocompleteCity'
				],
				'milestonesRewards': [
					'milestonesRewards'
				],
				'DESCityGuide': [
					'DESCityGuide',
					'roundTrip',
					'initAutocompleteCity'
				],
				'staticContentKrisflyer': [
					'staticContentKrisflyer'
				],
				'sshSelection': [
					'CIBBookingSummary',
					'roundTrip',
					'sshSelection'
				],
				'sshHotel': [
					'sshHotel',
					'CIBBookingSummary',
					'roundTrip',
					'sshSelection'
				],
				'sshAdditional': [
					'CIBBookingSummary',
					'sshAdditional'
				],
				'mbChangeFlight': [
					'initAutocompleteCity',
					'selectReturnDate',
					'roundTrip',
					'mbChangeFlight'
				],
				'staticContentComponents': [
					// 'initTabMenu'
				],
				'multiTabsWithLongText': [
					'multiTabsWithLongText'
				],
				'staticContentMeals': [
					// 'initTabMenu'
				],
				'staticContentSpecMeals': [
					// 'initTabMenu'
				],
				'staticContentGeneric': [
					'staticContentGeneric'
				],
				'staticContentHeritage': [
					'staticContentHeritage'
				],
				'initTabMenu': [
					'initTabMenu'
				],
				'kfUnsubscribe': [
					'kfUnsubscribe'
				],
				'feedBack': [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack'
				],
				'bestFare' : [
					'initAutocompleteCity',
					'initPersonTitles',
					'feedBack',
					'selectReturnDate',
					'roundTrip'
				],
				'sQupgrade' : [
          // 'roundTrip'
        ],
				'checkRoom': [
					'sshSelection'
				],
				'youtubeApp': [
					'youtubeApp'
				],
				'flowPlayerApp': [
					'flowPlayerApp'
				],
				'landingSearchFlight': [
					'initAutocompleteCity',
					'landingSearchFlight',
					'roundTrip',
					'selectCabin',
					'selectReturnDate',
					'flightScheduleTrip'
				],
				'tab': [
					'tab'
				],
				'blockScroll': [
					'blockScroll'
				],
				'filterEntertainment': [
					'filterEntertainment'
				],
				'autocompleteSearch': [
					'autocompleteSearch'
				],
				'chatWidget': [
					'chatWidget'
				],
				'faqs': [
					'faqs'
				],
				'enewsSubscribe': [
					'initPersonTitles'
				],
				'contactUs': [
					'initAutocompleteCity'
				],
				'plusOrMinusNumber': [
					'plusOrMinusNumber'
				],
				'addBaggage': [
					'addBaggage'
				],
				'promotionsPackages': [
					'promotionsPackages',
					'initAutocompleteCity'
				],
				'radioTabsChange': [
					'radioTabsChange'
				],
				'hotelListing': [
					'hotelListing'
				],
				'disableValue': [
					'disableValue'
				],
				'togglePrivacy': [
					'togglePrivacy'
				],
				'carouselFade': [
					'carouselFade'
				],
				'carouselSlide': [
					'carouselSlide'
				],
				'promotionComponent': [
					'promotionComponent'
				],
				'pricePoints': [
					'pricePoints'
				],
				'checkImage': [
					'checkImage'
				],
				'bookingWidget': [
					'bookingWidget',
					'initAutocompleteCity',
					'roundTrip',
					'selectReturnDate',
					'selectCabin'
				],
				'sortTableContent': [
					'sortTableContent'
				],
				'autocompleteCountryCity': [
					'autocompleteCountryCity'
				],
				'redemptionNominee': [
					'initAutocompleteCity',
					'redemptionNominee'
				],
				'bookingMileClaim': [
					'bookingMileClaim'
				],
				'donateMiles': [
					'donateMiles'
				],
				'voucher': [
					'voucherRewards',
					'manageBooking',
					'voucherStored'
				]
			},
			getPage: function(page) {
				//return this.page[page];
				var functions = this.page[page];
				totalLoadedModules += functions.length;

				$.each(functions, function(index) {
					modulesToLoad.push(functions[index]);
					loadScript(urlScript.modules[functions[index]], function() {
						var moduleIndex = modulesToLoad.indexOf(functions[index]);
						modulesToLoad[moduleIndex] = SIA[functions[index]];
						loadedModules++;
						if (totalLoadedModules === loadedModules) {
							initFunction = initFunction.concat(modulesToLoad);
							concatInitFunctionDone = true;
						}
					}, 'body');
				});
			}
		};

		if($('[data-flight]').length){
			dashboard.getPage('flightTableBorder');
		}

		if($('[data-rule-onlycharacter]').length || $('[data-rule-digits]').length){
			dashboard.getPage('forceInput');
		}

		if($('[data-multiple-submit]').length){
			dashboard.getPage('multipleSubmit');
		}

		if ($('[data-social]').length) {
			dashboard.getPage('contact');
		}

		if (window.simpleSticky) {
			dashboard.getPage('simpleSticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-sticky]').length) {
			dashboard.getPage('sticky');
			requireScript = requireScript.concat(['underscore']);
		}

		if ($('[data-bar]').length) {
			dashboard.getPage('bar');
			requireScript = requireScript.concat(['underscore']);
		}

		if($('[data-coach-beta]').length){
			dashboard.getPage('coachBeta');
		}

		if ($('[data-autocomplete-search]')) {
			dashboard.getPage('autocompleteSearch');
		}

		if ($('[data-tab]').length) {
			dashboard.getPage('initTabMenu');
		}

		if ($('[data-check-room]').length) {
			dashboard.getPage('checkRoom');
		}

		if ($('[data-carousel-fade]').length) {
			dashboard.getPage('carouselFade');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-carousel-slide]').length) {
			dashboard.getPage('carouselSlide');
			requireScript = requireScript.concat(['slick']);
		}

		if ($('[data-price-points]').length) {
			dashboard.getPage('pricePoints');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-check-image]').length) {
			dashboard.getPage('checkImage');
		}

		if (body.hasClass('landing-search-flights-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('landing-fl-status-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-manage-booking-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-checkin-page')) {
			dashboard.getPage('landingSearchFlight');
		}

		if (body.hasClass('landing-flight-schedules-page')) {
			dashboard.getPage('landingSearchFlight');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('home-page') && !window.darkSite) {
			dashboard.getPage('home');
			requireScript = requireScript.concat(['underscore', 'slick', 'inputmask', 'accounting']);
		}

		if(window.darkSite){
			dashboard.getPage('grayscaleModule');
		}

		if (body.hasClass('statement-dark-site-page')) {
			dashboard.getPage('darkSiteStatements');
			requireScript = requireScript.concat(['slick', 'youtubeAPI']);
		}
		if (body.hasClass('home-page') && window.darkSite) {
			dashboard.getPage('darkSiteHome');
			requireScript = requireScript.concat(['underscore', 'slick', 'inputmask', 'accounting', 'allyjs']);
		}
		if (body.hasClass('passenger-details-page')) {
			if (body.hasClass('orb-passenger-details-page')) {
				dashboard.getPage('orbPassengerDetail');
			} else {
				dashboard.getPage('passengerDetail');
			}
			requireScript = requireScript.concat(['accounting', 'underscore', 'JSONglobal']);
		}
		if (body.hasClass('seatsmap-page')) {
			dashboard.getPage('seatMap');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('flight-status-page')) {
			dashboard.getPage('flightStatus');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('flight-schedules-page')) {
			dashboard.getPage('flightSchedule');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}
		if (body.hasClass('fare-calendar-page')) {
			dashboard.getPage('flightCalendar');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if (body.hasClass('fares-list-page')) {
			dashboard.getPage('promotion');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('fares-details-page')) {
			dashboard.getPage('faresDetailsPage');
			requireScript = requireScript.concat(['inputmask']);
		}
		if (body.hasClass('krisflyer-list-page')) {
			dashboard.getPage('promotionKrisflyer');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('booking-sumary-page') || body.hasClass('checkin-complete') || body.hasClass('relaunch-page')) {
			dashboard.getPage('bookingSummnary');
			requireScript = requireScript.concat(['inputmask']);
		}
		if (body.hasClass('flight-select-page')) {
			if (body.hasClass('orb-flight-select-page')) {
				dashboard.getPage('orbFlightSelect');
			} else if (body.hasClass('mb-flight-select-page')) {
				if (body.hasClass('mb-flight-select-page-sf')) {
					dashboard.getPage('mbFlightSelectSf');
				} else {
					dashboard.getPage('mbFlightSelect');
				}
			} else {
				dashboard.getPage('flightSelect');
			}
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}
		if (body.hasClass('flight-search-calendar-page')) {
			dashboard.getPage('flightSearchCalendar');
		}
		if (body.hasClass('milestones-at-a-glance-page')) {
			dashboard.getPage('milestonesRewards');
		}
		if (body.hasClass('multi-city-page')) {
			dashboard.getPage('multicity');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('payments-page')) {
			if (body.hasClass('orb-payments-page')) {
				dashboard.getPage('orbPayment');
			} else if (body.hasClass('mb-payments-page')) {
				dashboard.getPage('mbPayment');
			} else {
				dashboard.getPage('payment');
			}
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}

		// if (body.hasClass('mb-payments-page')) {
		// 	dashboard.getPage('mbPayment');
		// }

		if (body.hasClass('add-ons-page')) {
			if (body.hasClass('orb-add-ons')) {
				dashboard.getPage('orbAddOns');
			} else {
				dashboard.getPage('addon');
			}
			requireScript = requireScript.concat(['inputmask', 'slick', 'accounting', 'underscore', 'allyjs']);
		}
		if (body.hasClass('cib-confirmation-page')) {
			dashboard.getPage('cibConfirmation');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if(body.hasClass('at-a-glance-page')){
			dashboard.getPage('atAGlance');
			requireScript = requireScript.concat(['accounting', 'underscore']);
		}
		if (body.hasClass('add-redemption-nominee-page')) {
			dashboard.getPage('kfRedemptionNominee');
		}
		if (body.hasClass('partner-programme-page')) {
			dashboard.getPage('kfPartnerProgramme');
		}
		if (body.hasClass('miles-page')) {
			dashboard.getPage('milesPage');
		}
		if (body.hasClass('messages-inbox-page')) {
			dashboard.getPage('kfMessage');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('message-forward-page')) {
			dashboard.getPage('kfMessageForward');
		}
		if (body.hasClass('personal-details-page') || body.hasClass('kf-profile-security') || body.hasClass('kf-preferences') /*|| body.hasClass('redemption-nominee-page')*/) {
			dashboard.getPage('kfPersonalDetail');
		}
		if (body.hasClass('redemption-nominee-page')) {
			dashboard.getPage('redemptionNominee');
			requireScript = requireScript.concat(['JSONglobal']);
		}
		if (body.hasClass('claim-missing-miles-page')) {
			dashboard.getPage('kfClaimMissingMiles');
		}
		if (body.hasClass('booking-upcoming-flights-page')) {
			dashboard.getPage('kfUpcomingFlights');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('booking-page')) {
			dashboard.getPage('kfFlightHistory');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}
		if (body.hasClass('ticket-receipt-page')) {
			dashboard.getPage('KFTicketReceipt');
		}
		if (body.hasClass('booking-check-ins-page')) {
			dashboard.getPage('KFCheckIns');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('how-to-earn-page')) {
			dashboard.getPage('howToEarn');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('how-to-use-page')) {
			dashboard.getPage('howToUse');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if (body.hasClass('redeem-miles')) {
			dashboard.getPage('redeemMiles');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('expiring-miles-page')) {
			dashboard.getPage('kfExpiringMiles');
		}
		if (body.hasClass('favourites-page')) {
			dashboard.getPage('KFFavourite');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('statements-page')) {
			dashboard.getPage('kfStatement');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('orb-confirmation-page')) {
			dashboard.getPage('ORBConfirmation');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}
		if (body.hasClass('orb-flight-schedule')) {
			dashboard.getPage('ORBFlightSchedule');
			requireScript = requireScript.concat(['underscore']);
		}
		// if(body.hasClass('create-new-pin-page')) {
		// 	dashboard.getPage('KFCreateNewPin');
		// }

		if (body.hasClass('registration-page')) {
			dashboard.getPage('KFRegistration');
		}

		if (body.hasClass('sqc-registration-page')) {
			dashboard.getPage('SQCRegistration');
		}

		if (body.hasClass('special-assistance-page')) {
			dashboard.getPage('specialAssistance');
		}

		if (body.hasClass('cancel-waitlist-booking-page')) {
			dashboard.getPage('cancelWaitlistBooking');
			requireScript = requireScript.concat(['inputmask']);
		}
		if ($('[data-accordion-wrapper]').length) {
			dashboard.getPage('accordion');
		}

		if ($('[data-count-chars-left]').length) {
			dashboard.getPage('countCharsLeft');
			requireScript = requireScript.concat(['accounting']);
		}

		if ($('[data-rule-validatedate]').length) {
			dashboard.getPage('initCorrectDate');
		}
		if (body.hasClass('mb-main-page')) {
			dashboard.getPage('ManageBooking');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('excess-baggage-page')) {
			dashboard.getPage('ExcessBaggage');
			requireScript = requireScript.concat(['underscore']);
		}

		if (body.hasClass('mb-confirmation-page') || body.hasClass('mb-change-booking-confirmation-page')) {
			dashboard.getPage('MBConfirmation');
		}
		if (body.hasClass('sqc-saved-trips-page')) {
			dashboard.getPage('SQCSavedTrips');
			requireScript = requireScript.concat(['underscore']);
		}

		// if($('[data-check-change]').length){
		// dashboard.getPage('FormCheckChange');
		// }
		if (body.hasClass('review-cancellation-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('mb-waitlisted-flight-orc-reserved-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('mb-select-flights-page')) {
			dashboard.getPage('ManageBooking');
		}
		if (body.hasClass('review-page') || body.hasClass('review-booking-insuff-page')) {
			dashboard.getPage('MBReview');
			requireScript = requireScript.concat(['underscore', 'inputmask', 'accounting']);
		}
		if (body.hasClass('select-meals-page')) {
			dashboard.getPage('MBSelectMeal');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('select-meals-land-page')) {
			dashboard.getPage('MBSelectMealLand');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('expenditure-page')) {
			dashboard.getPage('SQCExpenditure');
		}
		if (body.hasClass('sqc-upcoming-flights-page')) {
			dashboard.getPage('sqcUpcomingFlight');
			requireScript = requireScript.concat(['underscore']);
		}
		if (body.hasClass('sqc-user')) {
			dashboard.getPage('SQCUser');
		}
		if (body.hasClass('sqc-add-user')) {
			dashboard.getPage('SQCAddUser');
		}
		if (body.hasClass('sqc-at-a-glance-page')) {
			dashboard.getPage('SQCAtAGlance');
			requireScript = requireScript.concat(['highcharts', 'underscore', 'accounting']);
		}
		if (body.hasClass('sqc-bookings-flight-history-page')) {
			dashboard.getPage('SQCFlightHistory');
			requireScript = requireScript.concat(['jqueryui', 'validate', 'underscore']);
		}
		if (body.hasClass('change-flight-page')) {
			dashboard.getPage('mbChangeFlight');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}
		if (body.hasClass('utilities-page')) {
			dashboard.getPage('desWhereTo');
			// requireScript = requireScript.concat(['isotope']);
			requireScript = requireScript.concat(['shuffle', 'underscore', 'inputmask']);
		}
		if (body.hasClass('destination-list-page')) {
			dashboard.getPage('desEntry');
			requireScript = requireScript.concat(['shuffle']);
		}
		if (body.hasClass('city-guide-page')) {
			dashboard.getPage('DESCityGuide');
			requireScript = requireScript.concat(['slick', 'inputmask']);
		}
		if (body.hasClass('des-krisFlyer-1-page')) {
			dashboard.getPage('staticContentKrisflyer');
			requireScript = requireScript.concat(['slick']);
		}

		if (body.hasClass('ssh-selection-page')) {
			dashboard.getPage('sshSelection');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if (body.hasClass('hotel-page')) {
			dashboard.getPage('sshHotel');
			requireScript = requireScript.concat(['underscore', 'accounting', 'inputmask']);
		}

		if (body.hasClass('ssh-additional-page')) {
			dashboard.getPage('sshAdditional');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('static-content-components')) {
			dashboard.getPage('staticContentComponents');
		}

		if ($('[data-multi-tab]').length) {
			dashboard.getPage('multiTabsWithLongText');
		}

		if (body.hasClass('static-content-meals-page')) {
			dashboard.getPage('staticContentMeals');
		}

		if (body.hasClass('static-content-spec-meal-page')) {
			dashboard.getPage('staticContentSpecMeals');
		}

		if (body.hasClass('static-content-generic-page')) {
			dashboard.getPage('staticContentGeneric');
			requireScript = requireScript.concat(['slick']);
		}

		if (body.hasClass('static-content-heritage')) {
			dashboard.getPage('staticContentHeritage');
		}

		if (body.hasClass('contact-us-page')) {
			dashboard.getPage('contactUs');
		}

		if ($('[data-youtube-url]').length) {
			dashboard.getPage('youtubeApp');
			if (SIA.global.vars.isIE() && SIA.global.vars.isIE() > 7) {
				requireScript = requireScript.concat(['youtubeAPI']);
			}
		}
		if ($('[data-flow-url]').length) {
			dashboard.getPage('flowPlayerApp');
			requireScript = requireScript.concat(['flowplayerAPI']);
		}

		if ($('[data-entertaiment]').length) {
			dashboard.getPage('filterEntertainment');
		}

		if ($('[data-block-scroll]').length) {
			dashboard.getPage('blockScroll');
		}
		if ($('[data-radio-tab]').length) {
			dashboard.getPage('tab');
		}
		if (body.hasClass('unsubscribe-page')) {
			dashboard.getPage('kfUnsubscribe');
		}

		if ($('[data-chat-now]').length) {
			dashboard.getPage('chatWidget');
		}

		if ($('[data-disable-value]').length) {
			dashboard.getPage('disableValue');
		}

		if ($('[data-toggle-privacy]').length) {
			dashboard.getPage('togglePrivacy');
		}

		if (body.hasClass('feedback-concern-baggage-delayed-page')) {
			dashboard.getPage('feedBack');
			requireScript = requireScript.concat(['accounting', 'recaptcha']);
		}

		if (body.hasClass('sq-upgrade-marketing-page')) {
      dashboard.getPage('sQupgrade');
      requireScript = requireScript.concat(['inputmask']);
    }

		if (body.hasClass('static-bestfare')) {
			dashboard.getPage('bestFare');
			requireScript = requireScript.concat(['accounting', 'inputmask', 'tiff', 'recaptcha']);
		}

		if ($('[data-open-accordion]').length) {
			dashboard.getPage('faqs');
		}

		// if($('[data-tablet-slider]').length){
		// 	dashboard.getPage('moreInTheSection');
		// 	if(requireScript.indexOf('slick') === - 1){
		// 		requireScript = requireScript.concat(['slick']);
		// 	}
		// }

		if ($('[data-plus-or-minus-number]').length) {
			dashboard.getPage('plusOrMinusNumber');
		}

		if ($('[data-radio-tabs-change]').length) {
			dashboard.getPage('radioTabsChange');
		}

		if (body.hasClass('add-baggage-page')) {
			dashboard.getPage('addBaggage');
		}

		if (body.hasClass('enews-subscribe-page')) {
			dashboard.getPage('enewsSubscribe');
		}

		if (body.hasClass('promotions-packages-page')) {
			dashboard.getPage('promotionsPackages');
			requireScript = requireScript.concat(['underscore', 'accounting']);
		}

		if (body.hasClass('hotel-listing-page')) {
			dashboard.getPage('hotelListing');
			requireScript = requireScript.concat(['shuffle']);
		}

		if (body.hasClass('booking-mile-claim-page')) {
			dashboard.getPage('bookingMileClaim');
			requireScript = requireScript.concat(['underscore', 'inputmask']);
		}

		if ($('[data-promotion-component]').length) {
			dashboard.getPage('promotionComponent');
			requireScript = requireScript.concat(['shuffle']);
		}

		if ($('[data-booking-widget]').length) {
			dashboard.getPage('bookingWidget');
			requireScript = requireScript.concat(['inputmask']);
		}

		if ($('[data-sort-content]').length) {
			dashboard.getPage('sortTableContent');
		}

		if ($('[data-component-country-city]').length) {
			dashboard.getPage('autocompleteCountryCity');
			requireScript = requireScript.concat(['inputmask']);
		}

		if (body.hasClass('donate-miles-page')) {
			dashboard.getPage('donateMiles');
		}

		if (body.hasClass('voucher-page')) {
			dashboard.getPage('voucher');
			requireScript = requireScript.concat(['underscore']);
		}

		if (totalLoadedModules === 0) {
			concatInitFunctionDone = true;
		}

		var countLoadScript = 0;
		var loadSIAScript = function() {
			var loadFunctionDuration = 0;
			if (SIA.global.vars.isIE() && SIA.global.vars.isIE() < 9) {
				loadFunctionDuration = 300;
				if (SIA.global.vars.isIE() < 8 && typeof SIA.setEmptyAltForImage !== 'undefined') {
					SIA.setEmptyAltForImage();
				}
			}
			var count = 0;
			var loadFunction = function(c) {
				setTimeout(function() {
					if (initFunction[c]) {
						initFunction[c]();
						count++;
						if (count !== initFunction.length) {
							loadFunction(count);
						} else {
							// SIA.preloader.hide();
							var unloadTimer = window.noJsonHandler ? 700 : 0;
							setTimeout(function() {
								SIA.preloader.hide();
							}, unloadTimer);

						}
					} else {
						SIA.preloader.hide();
					}
				}, loadFunctionDuration);
			};
			var waitInitFunction = setInterval(function() {
				if (concatInitFunctionDone) {
					loadFunction(count);
					clearInterval(waitInitFunction);
				}
			}, 100);
			if (typeof triggerModal !== 'undefined') {
				var setCookie = function(cname, cvalue, exdays) {
					var d = new Date();
					d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
					var expires = 'expires=' + d.toGMTString();
					document.cookie = cname + '=' + cvalue + ';' + expires;
				};

				var getCookie = function(cname) {
					var name = cname + '=';
					var ca = document.cookie.split(';');
					for (var i = 0; i < ca.length; i++) {
						var c = ca[i].trim();
						if (c.indexOf(name) === 0) {
							return c.substring(name.length, c.length);
						}
					}
					return '';
				};

				var initPopupTriggerModal = function(triggerModal, introLightboxEl) {
					// var trM = $(triggerModal);
					var trM = $();

					if(typeof introLightboxEl !== 'undefined'){
						trM = introLightboxEl;
					}else{
						trM = $(triggerModal);
					}

					var openLogin = trM.find('[data-open-popup-login]');
					var isOpenLogin = false;
					trM.Popup({
						overlayBGTemplate: SIA.global.config.template.overlay,
						modalShowClass: '',
						triggerCloseModal: '.popup__close, [data-close]',
						afterHide: function(popup) {
							if ($(popup).find('[data-remember-cookie] input').is(':checked')) {
								setCookie(triggerModal, true, 7);
							}
							if(openLogin.length && isOpenLogin){
								$(openLogin.data('open-popup-login')).Popup('show');
								isOpenLogin = false;
							}
						}
					});

					if(openLogin.length){
						openLogin.off('click.openLogin').on('click.openLogin', function(){
							isOpenLogin = true;
							trM.Popup('hide');
						});
					}

					if (!getCookie(triggerModal)) {
						trM.Popup('show');
					}
				};

				if (/\//.test(triggerModal)) {
					var getLbTemplate = function() {
						return $.ajax({
							url: triggerModal
						});
					};
					getLbTemplate().done(function(data) {
						var introLightboxEl = $(data);
						var totalImg = introLightboxEl.find('img');
						var countImage = 0;
						body.append(introLightboxEl);
						if (totalImg.length) {
							totalImg.each(function() {
								var that = $(this);
								var img = new Image();
								img.onload = function() {
									countImage++;
									if (countImage === totalImg.length) {
										// initPopupTriggerModal(introLightboxEl);
										initPopupTriggerModal(triggerModal, introLightboxEl);
									}
								};
								img.src = that.attr('src');
							});
						} else {
							// initPopupTriggerModal(introLightboxEl);
							initPopupTriggerModal(triggerModal, introLightboxEl);
						}
					});
				} else {
					initPopupTriggerModal(triggerModal);
				}

			}
		};
		var excuteScript = function(url) {
			loadScript(url, function() {
				countLoadScript++;
				if (countLoadScript === requireScript.length) {
					loadSIAScript();
				}
			});
		};
		// if(requireScript.length){
		// 	for(var i = 0; i < requireScript.length; i++ ){
		// 		excuteScript(urlScript.getScript(requireScript[i]));
		// 	}
		// }
		// else{
		// 	loadSIAScript();
		// }
		var initLoadPage = function() {
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			var paramUrl = sURLVariables[1];
			var getURLParams = function(sParam) {
				for (var i = 0; i < sURLVariables.length; i++) {
					var sParameterName = sURLVariables[i].split('=');
					if (sParameterName[0] === sParam) {
						return sParameterName[1];
					}
				}
			};
			var run = function() {
				if (requireScript.length) {
					for (var i = 0; i < requireScript.length; i++) {
						excuteScript(urlScript.getScript(requireScript[i]));
					}
				} else {
					loadSIAScript();
				}
			};
			if (getURLParams('debug') && $.parseJSON(getURLParams('debug'))) {
				var sName = paramUrl.split('=');
				$.get('./ajax/JSONS/' + sName[1], function(res) {
					globalJson[sName[0]] = res;
					run();
				});
			} else {
				run();
			}
		};

		initLoadPage();

		var preloadImages = function() {
			if (typeof imageSrcs !== 'undefined' && imageSrcs.oldBrowser.length) {
				$.each(imageSrcs.oldBrowser, function(i, src){
					var img = new Image();
					img.src = src;
				});
			}
		};

		preloadImages();

		$(window).off('resize.updatePosLoading').on('resize.updatePosLoading', function(){
      SIA.planLoading.rePosition();
    });

		var externalLinks = function(){
			var linkCheck = $('a.external');

			linkCheck.each(function(){
				var self = $(this);
				var linkText = self.text();
				self.attr({
					'aria-label': 'External Link. ' + linkText
				});
			});
		};
	  $('#choose-file').focusin(function() {
	  	/* Act on the event */
			$('.custom-choose-file').addClass('focus');
	  });
	  $('#choose-file').focusout(function() {
	  	/* Act on the event */
			$('.custom-choose-file').removeClass('focus');
	  });
		externalLinks();

	});
})(jQuery);

/**
 *  @name accordion
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'accordion';
	var win = $(window);

	// var isIE = function() {
	// 	var myNav = navigator.userAgent.toLowerCase();
	// 	return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	// };

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}


	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3 && window.Modernizr.touch;
			that.isOpenAll = false;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);

			that.wrapper.each(function(){
				var self = $(this);
				var triggerAccordion = self.find(that.options.triggerAccordion);
				var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');
				self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;

				triggerAccordion.each(function(idx){
					var trigger = $(this);
					var preventDefault = false;
					if(supportCss){
						trigger.data('height', contentAccordion.eq(idx).height());
					}
					if(!trigger.hasClass(that.options.activeClass)){
						if(supportCss){
							contentAccordion.eq(idx).css('height', 0);
						}else{
							contentAccordion.eq(idx).hide();
						}
					}
					trigger.off('click.accordion').on('click.accordion', function(e) {
						if($(e.target).is('label') || $(e.target).is(':checkbox')){
							return;
						}
						e.preventDefault();
						if(trigger.closest('.disable').length || preventDefault){
							return;
						}
						if(that.isOpenAll){
							that.isOpenAll = false;
							triggerAccordion.not(trigger).trigger('click.accordion');
							trigger.trigger('stayAcc');
							return;
						}
						preventDefault = true;
						if($.isFunction(that.options.beforeAccordion)){
							that.options.beforeAccordion.call(self, trigger, contentAccordion.eq(idx));
						}
						trigger.trigger('beforeAccordion');
						if(trigger.hasClass(that.options.activeClass)){
							if(supportCss){
								contentAccordion.eq(idx).css('height', 0);
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideUp(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.removeClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
							self.activeAccordion = $();
						}
						else{

							// if(self.activeAccordion.length){
							// 	self.activeAccordion = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							// 	self.activeAccordion.trigger('click.accordion');
							// 	self.activeAccordion = $();
							// }

							var curActiveAccor = triggerAccordion.filter('.' + that.options.activeClass).length ? triggerAccordion.filter('.' + that.options.activeClass) : $() ;
							if (curActiveAccor && curActiveAccor.length) {
								curActiveAccor.trigger('click.accordion');
							}

							self.activeAccordion = trigger;
							if(supportCss){
								contentAccordion.eq(idx).css('height', trigger.data('height'));
								preventDefault = false;
							}else{
								contentAccordion.eq(idx).slideDown(that.options.duration, function(){
									if($.isFunction(that.options.afterAccordion)){
										that.options.afterAccordion.call(self, trigger, contentAccordion.eq(idx));
									}
									preventDefault = false;
								});
							}
							trigger.addClass(that.options.activeClass);
							trigger.trigger('afterAccordion');
						}
					});
					// if(isIE() && isIE() < 8){
						// trigger.append('<span class="'+ trigger.find('em').attr('class') +'"></span>');
						// trigger.find('em').remove();
					// }
				});

				// this resize to support on mobile
				win.off('resize.accordion').on('resize.accordion', function(){
					if(supportCss && $(this).width() < 768){
						contentAccordion.css('height', 0);
						triggerAccordion.removeClass('active');
					}else if(supportCss && $(this).width() >= 768){
						contentAccordion.css('height', '');
						triggerAccordion.removeClass('active');
					}
				}).trigger('resize.accordion');
			});
		},
		refresh: function(){
			this.init();
		},
		openAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(!that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', triggerAccordion.data('height'));
					}else{
						contentAccordion.slideDown(that.options.duration);
					}
					triggerAccordion.addClass(that.options.activeClass);
					that.trigger('openAll');
					that.isOpenAll = true;
				});
			}
		},
		closeAll: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			var supportCss = that.options.css3;
			// this  wrapper is used if we would like to close the opening accordion and open the currect clicking accordion
			that.wrapper = that.find(that.options.wrapper);
			if(that.isOpenAll){
				that.wrapper.each(function(){
					var self = $(this);
					var triggerAccordion = self.find(that.options.triggerAccordion);
					var contentAccordion = self.find(that.options.contentAccordion).removeClass('hidden');

					if(supportCss){
						contentAccordion.css('height', 0);
					}else{
						contentAccordion.slideUp(that.options.duration);
					}
					triggerAccordion.removeClass(that.options.activeClass);
					that.trigger('closeAll');
					that.isOpenAll = false;
				});
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		wrapper: '',
		triggerAccordion: '',
		contentAccordion: '',
		activeClass: 'active',
		duration: 400,
		css3: false,
		afterAccordion: function() {},
		beforeAccordion: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global autoCompleteLanguage functions
 * @version 1.0
 */

SIA.autoCompleteLanguage = function(){
	var global = SIA.global;
	var doc = global.vars.doc;
	// var win = global.vars.win;
	// var	config = global.config;
	var body = global.vars.body;
	var languageJSON = global.vars.languageJSON;
	// var timerTriggerSearch;

	var autoCompleteCustom = function (opt) {
		// create a variable for management
		var that = {};

		// declare booking widget defaults
		var defaults = {
			containerAutocomplete : '',
			autocompleteFields : '',
			autoCompleteAppendTo: '',
			airportData : [],
			open: function(){},
			change: function(){},
			select: function(){},
			close: function(){},
			search: function(){},
			response: function(){},
			itemShow: 6,
			setWidth: 30
		};

		// extend passed options and defaults
		that.options = $.extend({}, defaults, opt);

		that.autocompleteFields = that.options.containerAutocomplete.find(that.options.autocompleteFields);
		that.airportData = that.options.airportData;
		that.timer = null;
		that.timerResize = null;

		that.autocompleteFields.each(function (index, value) {
			var field = $(value);
			var wp = field.closest('div.custom-select');
			var bookingAutoComplete = field.autocomplete({
					minLength : 0,
					open: that.options.open,
					change: that.options.change,
					select: that.options.select,
					close: that.options.close,
					response: that.options.response,
					search: that.options.search,
					// source: that.airportData,
					source: function(request, response) {
						// create a regex from ui.autocomplete for 'safe returns'
						var matcher = new RegExp('^' + $.ui.autocomplete.escapeRegex(request.term) + '|\\s' + $.ui.autocomplete.escapeRegex(request.term), 'ig');
						// match the user's request against each destination's keys,

						var match = $.grep(that.airportData, function(airport) {
							var flag = airport.flag;
							var value = airport.value;

							return (matcher.test(value) || matcher.test(flag)) && (/[a-zA-Z0-9]+$/.test(request.term) || !request.term);
						});

						// ... return if ANY of the keys are matched
						response(match);
					},
					appendTo : that.options.autoCompleteAppendTo
				}).data('ui-autocomplete');

			bookingAutoComplete._renderItem = function (ul, item) {
				// customising our suggestion dropdowns here
				return $('<li class="autocomplete-item">')
				.attr('data-value', item.order)
				.attr('data-language', item.language)
				.attr('data-flag', item.flag)
				.append('<a class="autocomplete-link" href="javascript:void(0);"><img src="images/transparent.png" alt="" class="flags '+ item.flag +'">'+ item.value + '</a>')
				.appendTo(ul);
			};

			bookingAutoComplete._resizeMenu = function () {
				this.menu.element.outerWidth(wp.outerWidth() + that.options.setWidth);
			};

			bookingAutoComplete._move = function( direction ) {
				var item, previousItem,
				last = false,
				li = $(),
				minus = null,
				api = this.menu.element.data('jsp');

				if (!api) {
					if (!this.menu.element.is(':visible')) {
						this.search(null, event);
						return;
					}
					if (this.menu.isFirstItem() && /^previous/.test(direction) ||
							this.menu.isLastItem() && /^next/.test(direction) ) {
						this._value( this.term );
						this.menu.blur();
						return;
					}
					this.menu[direction](event);
				}
				else {
					var currentPosition = api.getContentPositionY();
					switch(direction){
						case 'next':
							if(this.element.val() === ''){
								api.scrollToY(0);
								li = this.menu.element.find('li:first');
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.next();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
								// console.log(currentPosition, previousItem.position().top);
							}
							if(!item){
								last = true;
								li = this.menu.element.find('li').removeClass('active').first();
								item = li.addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(0);
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								minus = li.position().top + li.innerHeight();
								if(minus - this.menu.element.height() > currentPosition){
									api.scrollToY(Math.max(0, minus - this.menu.element.height()));
								}
							}
							break;
						case 'previous':
							if(this.element.val() === ''){
								last = true;
								item = this.menu.element.find('li:last').addClass('active').data( 'ui-autocomplete-item' );
							}
							else{
								previousItem = this.menu.element.find('li.active').removeClass('active');
								li = previousItem.prev();
								item = li.removeClass('active').addClass('active').data( 'ui-autocomplete-item' );
							}
							if(!item){
								last = true;
								item = this.menu.element.find('li').removeClass('active').last().addClass('active').data( 'ui-autocomplete-item' );
							}
							this.term = item.value;
							this.element.val(this.term);
							if(last){
								api.scrollToY(this.menu.element.find('.jspPane').height());
								last = false;
							}
							else{
								currentPosition = api.getContentPositionY();
								if(li.position().top <= currentPosition){
									api.scrollToY(li.position().top);
								}
							}
							break;
					}
				}
			};

			field.autocomplete('widget').addClass('autocomplete-menu');

			field.off('blur.autocomplete');
			field.off('focus.highlight').on('focus.highlight', function (e) {
				e.stopPropagation();
				var self = $(this);
				self.closest('.custom-select').addClass('focus');

				doc.off('mousedown.hideAutocompleteLanguage').on('mousedown.hideAutocompleteLanguage', function(e){
					if(!$(e.target).closest('.ui-autocomplete').length){
						field.closest('.custom-select').removeClass('focus');
						field.autocomplete('close');
					}
				});
			});

			field.autocomplete('widget').off('scroll.preventScroll mousewheel.preventScroll').on('scroll.preventScroll mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
				e.stopPropagation();
			});
			field.off('keypress.preventDefault').on('keypress.preventDefault', function(e){
				if(e.which === 13){
					e.preventDefault();
					if(field.autocomplete('widget').find('li').length === 1){
						field.autocomplete('widget').find('li').trigger('click');
						return;
					}
					field.autocomplete('widget').find('li.active').trigger('click');
				}
			});
			wp.children('.ico-dropdown').off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
			// wp.off('click.triggerAutocomplete').on('click.triggerAutocomplete', function(e){
				e.preventDefault();
				clearTimeout(that.timer);
				if(field.closest('.custom-select').hasClass('focus')){
					field.trigger('blur.highlight');
				}
				else{
					field.trigger('focus.highlight');
				}
			});
		});
	};


	var autoCompleteLanguage = function(){
		var ppLanguage = global.vars.ppLanguage;
		if (!ppLanguage.length) {
			return;
		}
		var optionLanguage = ppLanguage.find('.custom-radio');
		var parentOptionLanguage = optionLanguage.parent();
		var defaultFlag = ppLanguage.find('.custom-select img').attr('class').split(' ')[1];
		parentOptionLanguage.children().not(':eq(0)').remove();
		// optionLanguage.not(':eq(0)').hide();
		var dataLanguage = ['en_UK', 'zh_CN', 'fr_FR', 'pt_BR', 'de_DE', 'zh_TW', 'ja_JP', 'ko_KR', 'ru_RU', 'es_ES'];
		var detectLanguge = function(lng){
			var lngs = lng.split(',');
			var isLng = [];

			for(var i = 0; i < lngs.length; i ++){
				for(var ii = 0; ii < dataLanguage.length; ii ++){
					if($.trim(lngs[i]) === $.trim(dataLanguage[ii])){
						isLng.push(ii);
					}
				}
			}

			return isLng;
		};

		var _selectLanguage = function(arr){
			parentOptionLanguage.empty();
			optionLanguage.find(':radio').removeAttr('checked');
			for(var i = 0; i< arr.length; i ++){
				var t = optionLanguage.eq(arr[i]).clone().appendTo(parentOptionLanguage);
				if(i === 0){
					t.find(':radio').prop('checked', true);
				}
				else{
					t.find(':radio').prop('checked', false);
				}
			}
		};
		var timerAutocompleteLang = null;
		var timerAutocompleteLangOpen = null;

		var getFlags = function(value){
			var a = {
				idx : 0,
				flag : false
			};
			for(var i = 0; i < languageJSON.data.length; i ++){
				if(languageJSON.data[i].value === value){
					a = {
						idx : i,
						flag : true
					};
				}
			}
			return a;
		};

		autoCompleteCustom({
			containerAutocomplete : ppLanguage,
			autocompleteFields : 'input#text-country',
			autoCompleteAppendTo: body,
			airportData : languageJSON.data,
			open: function(){
				var self = $(this);
				self.autocomplete('widget').hide();
				clearTimeout(timerAutocompleteLangOpen);
				timerAutocompleteLangOpen = setTimeout(function(){
					self.autocomplete('widget').show().css({
						'left': self.closest('.custom-select--2').offset().left,
						'top': self.closest('.custom-select--2').offset().top + self.closest('.custom-select--2').outerHeight(true)
					});
					self.autocomplete('widget')
					.jScrollPane({
						scrollPagePercent				: 10
					}).off('mousewheel.preventScroll').on('mousewheel.preventScroll', function(e){
						e.preventDefault();
						e.stopPropagation();
					});
				}, 100);
			},
			select: function(event, ui){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				flag.removeClass().addClass('flags ' + ui.item.flag);
				_selectLanguage(detectLanguge(ui.item.language));
				/*if(win.width() < config.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){
					win.trigger('resize.popupLanguage');
				}*/
				setTimeout(function(){
					self.blur();
				}, 100);
			},
			response: function(event, ui){
				if(ui.content.length ===1){
					// $(this).val(ui.content[0].value);
					// $(this).select();
				}
			},
			search: function(){
				var self = $(this);
				self.autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				clearTimeout(timerAutocompleteLang);
				timerAutocompleteLang = setTimeout(function(){
					// if(self.autocomplete('widget').find('li').length === 1){
					// 	self.autocomplete('widget').find('li').addClass('active');
					// }
				}, 100);
			},
			close: function(){
				var self = $(this);
				var flag = self.closest('.select__text').siblings('img');
				var item = getFlags(self.val());
				if(item.flag){
					flag.removeClass().addClass('flags ' + languageJSON.data[item.idx].flag);
					_selectLanguage(detectLanguge(languageJSON.data[item.idx].language));
				}
				if(!$.trim(self.val())){
					flag.removeClass().addClass('flags ' + defaultFlag);
					var defaultLanguage = 'en_UK, zh_CN';
					_selectLanguage(detectLanguge($.trim(defaultLanguage)));
				}
				$(this).autocomplete('widget').removeData('jsp').off('mousewheel.preventScroll');
				doc.off('mousedown.hideAutocompleteLanguage');
			},
			setWidth: 0
		});
	};
	// init
	autoCompleteLanguage();
};

/**
 *  @name plugin
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'checkboxAll';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var that = this;
			var chbMaster = that.element.find('[data-checkbox-master]').first();
			var chbItems = that.element.find('[data-checkbox-item]');

			chbMaster.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checked = $(this).is(':checked');
				chbItems.filter(':visible').prop('checked', checked);
			});


			chbItems.off('change.' + pluginName).on('change.' + pluginName, function() {
				var checkedAll = (chbItems.filter(':visible').length === chbItems.filter(':checked').length);
				chbMaster.prop('checked', checkedAll);
			});
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				window.alert(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
	};

	// $(function() {
	// 	$('[data-checkbox-all]')[pluginName]();
	// });

}(jQuery, window));

/**
 * @name SIA
 * @description Define global cookiesUse functions
 * @version 1.0
 */
SIA.cookiesUse = function(){
	var popupCookies = $('.popup--cookie').appendTo(SIA.global.vars.container);
	// cookies

	var setCookie = function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+d.toGMTString();
		document.cookie = cname + '=' + cvalue + ';' + expires;
	};

	var getCookie = function(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i].trim();
			if (c.indexOf(name) === 0){
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	var checkCookie = function () {
		var user = getCookie('seen');
		if (user !== '') {
			popupCookies.addClass('hidden');
		} else {
			popupCookies.removeClass('hidden');
		}
	};
	// end cookies
	checkCookie();
	popupCookies.find('.popup__close').off('click.closeCookie').on('click.closeCookie', function(e){
		e.preventDefault();
		setCookie('seen', true, 7);
		popupCookies.addClass('hidden');
	});

	SIA.global.vars.setCookie = setCookie;
	SIA.global.vars.getCookie = getCookie;
};

/**
 *  @name customSelect
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'customSelect';
	var openedSelect = null;
	var getUID = (function(){
		var id = 0;
		return function(){
			return pluginName + '-' + id++;
		};
	})();
	var win = $(window);
	var doc = $(document);
	var isIE = function() {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
	};
	var key = {
		DOWN: 40,
		UP: 38,
		ENTER: 13,
		ESC: 27
	};

	function scrollWithADistance(that, top) {
		that.scrollBarBtn.css({
			'top' : Math.min(top, that.scrollContainer.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : Math.max(-top * that.ratioOfWrapperToScroll + top * that.ratioOfContenToScroll, that.scrollContainer.height() - that.wrapperOption.height())
		});
	}

	function scrollWithY(that, Y) {
		var y = Y;
		that.scrollBarBtn.css({
			'top' : Math.min(y/(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll), that.scrollBarWrapper.height() - that.scrollBarBtn.height())
		});
		that.wrapperOption.css({
			'margin-top' : -Math.min(that.itemsLength*that.options.heightItem - that.scrollContainer.height(), y)
		});
	}

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.idPlugin = getUID();
			that.options = plugin.options;
			that.curItem = $();
			// that.hasDefaultClass = that.hasClass('default');
			plugin.inPopup = that.closest('.popup');
			that.selectEl = that.find(plugin.options.selectEl);
			if(!that.selectEl.is('select')){
				return;
			}
			that.customText = that.find(plugin.options.customText);
			that.arrow = that.find(plugin.options.arrow);
			that.scroll = $('<div class="custom-scroll custom-dropdown">' +
				'<div class="scroll-container">' +
				'<ul></ul>' +
				'</div>' +
				'<div class="scroll-bar"><span>&nbsp;</span></div>' +
				'</div>');

			that.scrollContainer = that.scroll.find('.scroll-container');
			that.wrapperOption = that.scrollContainer.find('ul');
			that.scrollBarWrapper = that.scroll.find('.scroll-bar');
			that.scrollBarBtn = that.scrollBarWrapper.children();

			plugin._createTemplate();
			var closeSelect = function(){
				if(openedSelect){
					openedSelect.hide();
					// openedSelect = null;
				}
			};

			if(that.data('filter')){
				that.find('[data-trigger]').off('click.show').on('click.show', function(e){
					if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
					that.trigger('beforeSelect');
					//IE9 & Other Browsers
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					//IE8 and Lower
					else {
						e.cancelBubble = true;
					}
					if(!that.hasClass('active')){
						plugin.show();
						closeSelect();
						openedSelect = plugin;
					}
					else{
						// openedSelect = null;
						plugin.hide();
					}
				});
			} else {
				that.off('click.show').on('click.show', function(e){
					clearTimeout(that.simulatorTimer);
					if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
					that.trigger('beforeSelect');
					//IE9 & Other Browsers
					if (e.stopPropagation) {
						e.stopPropagation();
					}
					//IE8 and Lower
					else {
						e.cancelBubble = true;
					}
					if(that.hasClass('active')){
						// openedSelect = null;
						plugin.hide();
					}
					else{
						$(document).trigger('click');
						plugin.show();
						closeSelect();
						openedSelect = plugin;
					}
				});
				that.find('.select__tips').on('click', function(e){
					e.stopPropagation();
				});

				// that.simulator = $('<input style="width:110%; opacity:0; background-color: transparent; border: none; outline: 0" type="text" readonly="readonly"  placeholder="' + $.trim(that.find('.select__label').text()) + '" />').insertAfter(that.selectEl);
				that.simulator = $('<input class="input-overlay" type="text" readonly="readonly" placeholder="' + $.trim(that.find('.select__label').text()) + '" />').insertAfter(that.selectEl);

				that.simulator.off('click.preventDefault').on('click.preventDefault', function(e){
					if(e.preventDefault){
						e.preventDefault();
					}
					else {
						e.returnValue = false;
					}
				});

				// that.css({
				// 	'overflow': 'hidden'
				// });

				// that.simulator.css({
				// 	position: 'absolute',
				// 	top: 0,
				// 	left: '-10%',
				// 	bottom: 0,
				// 	right: 0
				// });

				that.simulator.off('focusin.show').on('focusin.show', function() {
					if(that.selectEl.prop('disabled') || that.hasClass('disabled')){ return; }
					that.simulatorTimer = setTimeout(function(){
						if(!that.hasClass('active')){
							plugin.show();
							closeSelect();
							openedSelect = plugin;
						}
					}, 200);
				});

				that.simulator.off('focusout.show').on('focusout.show', function() {
					clearTimeout(that.focusoutTimer);
					that.focusoutTimer = setTimeout(function(){
						if (!plugin.options.preventClose) {
							plugin.hide();
						}
					}, 200);
				});

				that.simulator
					.off('keydown.preventSubmit')
					.on('keydown.preventSubmit', function(e) {
						var key = e.keyCode || e.which;
						if (key === 13) {
							e.preventDefault();
						}
					});
			}

			that.selectEl.css('visibility', 'hidden');

			that.arrow.off('click.show').on('click.show', function(){
				that.trigger('focusin.show');
				return false;
			});

			that.selectEl.off('keypress.select').on('keypress.select', function(event) {
				var code = event.keycode || event.which;
				switch(code) {
					case key.ENTER:
						event.preventDefault();
						plugin.show();
						break;
					case key.ESC:
						event.preventDefault();
						plugin.hide();
						break;
				}
			});

			if(plugin.inPopup.length) {
				plugin.inPopup.off('scroll.inPopup' + that.idPlugin).on('scroll.inPopup' + that.idPlugin, function() {
					plugin.hide();
				});
			}
		},
		_createTemplate: function() {
			var plugin = this,
				that = plugin.element;
			that.items = that.selectEl.children();
			that.itemsLength = that.items.length;
			that.wrapperOption.empty().css('margin-top', '');
			that.scrollContainer.css('height', 0);
			that.scrollBarWrapper.css('height', 0);
			that.scrollBarBtn.css('top', '');
			var items  = [];
			if(!that.items.length){
				that.customText.text('');
			}
			that.items.each(function() {
				var self = $(this),
						optionClass = self.attr('class') || '';
				var item = '<li data-value="' + self.val() + '" class="' + (self.is(':selected') ? 'active' + optionClass : optionClass) + '">' + self.text() + '</li>';
				items.push(item);
				if(self.is(':selected')){
					that.customText.text(self.text());
				}
				// item.outerHeight(plugin.options.heightItem);
				// item.off('click.select').on('click.select', function(){
				// 	that.curItem = item;
				// 	that.customText.text(self.text());
				// 	that.selectEl.prop('selectedIndex', idx);
				// 	item.siblings('.active').removeClass('active');
				// 	item.addClass('active');
				// 	if(plugin.options.afterSelect){
				// 		plugin.options.afterSelect.call(that, that.selectEl, self, idx);
				// 	}
				// 	that.trigger('afterSelect');
				// });
			});
			if(that.items.length <= plugin.options.itemsShow){
				that.scrollContainer.height(that.items.length*plugin.options.heightItem);
				that.scrollBarWrapper.height(that.items.length*plugin.options.heightItem);
			}
			else{
				that.scrollContainer.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
				that.scrollBarWrapper.height(plugin.options.itemsShow*plugin.options.heightItem + plugin.options.heightItem/2);
			}
			that.wrapperOption.html(items.join(''));
			that.wrapperOption.undelegate('.select')
				.delegate('[data-value]','click.select', function(){
					that.curItem = $(this);
					if (!that.curItem.closest('[data-keep-limit]')) {
						if(that.curIndex === that.curItem.index()){
							return;
						}
					}
					that.curIndex = that.curItem.index();
					that.customText.text(that.items.eq(that.curIndex).text());
					that.selectEl.prop('selectedIndex', that.curIndex);
					that.curItem.siblings('.active').removeClass('active');
					that.curItem.addClass('active');
					if(plugin.options.afterSelect){
						plugin.options.afterSelect.call(that, that.selectEl, that.items.eq(that.curIndex), that.curIndex);
						if(void 0 !== that.selectEl.closest('form').data('validator')){
							that.selectEl.valid();
						}
					}
					that.trigger('afterSelect', that.curItem.data('value'));
					that.selectEl.trigger('change');
					that.selectEl.trigger('blur');
				})
				.delegate('[data-value]', 'mousedown.select', function() {
					setTimeout(function() {
						clearTimeout(that.focusoutTimer);
					}, 200);
				});
			that.scroll = that.scroll.appendTo(document.body);
			if(that.selectEl.closest('.popup').length){
				that.scroll.css({
					'z-index': '1004'
				});
			}
		},
		refresh: function () {
			var plugin = this,
				that = plugin.element;
			if(that.selectEl.find(':selected').index() === -1 ){
				that.customText.text(that.selectEl.find('option:eq(0)').text());
			}
			else{
				that.customText.text(that.selectEl.find(':selected').text());
			}
			that.wrapperOption.find('[data-value]').removeClass('active').eq(that.selectEl.prop('selectedIndex')).addClass('active');
			that.curIndex = that.selectEl.prop('selectedIndex');
			if($.trim(that.selectEl.find(':selected').val())){
				that.removeClass('default');
			}
			else{
				if(that.selectEl.find(':selected').index() === -1 && (that.selectEl.find('option:eq(0)').val() === 0 || that.selectEl.find('option:eq(0)').val() === '')){
					that.addClass('default');
				}
				else if(that.selectEl.find(':selected').index() !== -1){
					that.addClass('default');
				}
			}
		},
		update: function () {
			var plugin = this,
				that = plugin.element;
			var text = '';
			var sel = that.selectEl.find(':selected');
			text = sel.text();
			that.customText.text(text);
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(sel.index()).addClass('active').attr('aria-selected', true);
			that.find('input').val(sel.text());
			that.curIndex = sel.index();
			if($.trim(sel.val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		updateSync: function () {
			var plugin = this,
				that = plugin.element;
			var text = '';
			var sel = that.selectEl.find('[selected="selected"]');
			text = sel.text();
			that.customText.text(text);
			that.wrapperOption.find('[data-value]').removeClass('active').attr('aria-selected', false).eq(sel.index()).addClass('active').attr('aria-selected', true);
			that.find('input').val(sel.text());
			that.curIndex = sel.index();
			if($.trim(sel.val())){
				that.removeClass('default');
			}
			else{
				that.addClass('default');
			}
		},
		show: function(){
			var plugin = this,
				that = plugin.element;
			if(that.is(':hidden')){
				return;
			}
			var setDimension = function(){
				var top,
						maxW = 0;

				that.scroll.show();
				maxW = that.outerWidth();

				// fix issue page mb-select-meals: dropdown width > select width

				// that.scroll.find('li').each(function() {
				// 	maxW = Math.max(maxW, this.offsetWidth);
				// });

				that.scroll.width(maxW - plugin.options.padding);

				if(that.items.length <= plugin.options.itemsShow){
					that.scrollContainer.width(maxW - 1);
				}
				else{
					that.scrollContainer.width(maxW - plugin.options.padding - that.scrollBarWrapper.outerWidth(true) - 1);
				}

				/*that.scroll.css({
					//'position': 'absolute',
					'z-index': '1000',
					top: that.offset().top + that.outerHeight(),
					left: that.offset().left
				});*/

				// setTimeout(function(){
				// 	if(!isIE() && plugin.inPopup.length && plugin.inPopup.css('position') === 'fixed'){
				// 		top = that.offset().top - win.scrollTop() + that.outerHeight();
				// 		if(that.offset().top + that.scroll.outerHeight(true) > win.scrollTop() + win.height()){
				// 			top = that.offset().top - that.scroll.outerHeight(true) - win.scrollTop();
				// 		}
				// 		that.scroll.css('position', 'fixed');
				// 	}
				// 	else{
				// 		top = that.offset().top + that.outerHeight();
				// 		if(top + that.scroll.outerHeight(true) > win.scrollTop() + win.height()) {
				// 			top = that.offset().top - that.scroll.outerHeight(true);
				// 		}
				// 		that.scroll.css('position', 'absolute');
				// 	}

				// 	that.scroll.css({
				// 		'z-index': '1000',
				// 		top: top,
				// 		left: that.offset().left
				// 	});

				// 	if(plugin.options.onOpen){
				// 		plugin.options.onOpen(that.selectEl);
				// 	}
				// }, 100);

				that.scrollBarWrapper.hide();
				if(!isIE() && plugin.inPopup.length && plugin.inPopup.css('position') === 'fixed'){
					top = that.offset().top - win.scrollTop() + that.outerHeight();
					if(that.offset().top + that.outerHeight() + that.scroll.outerHeight(true) > win.scrollTop() + win.height()){
						top = that.offset().top - that.scroll.outerHeight(true) - win.scrollTop();
					}
					that.scroll.css('position', 'fixed');
				}
				else{
					top = that.offset().top + that.outerHeight();
					if(top + that.scroll.outerHeight(true) > win.scrollTop() + win.height()) {
						top = that.offset().top - that.scroll.outerHeight(true);
					}
					that.scroll.css('position', 'absolute');
				}
				that.scrollBarWrapper.show();

				that.scroll.css({
					'z-index': '1000',
					top: top,
					left: that.offset().left
				});

				if(that.selectEl.closest('.popup').length){
					that.scroll.css({
						'z-index': '1004'
					});
				}

				if(plugin.options.onOpen){
					plugin.options.onOpen(that.selectEl);
				}
			};
			setDimension();
			that.addClass('active focus');
			win.off('resize.kCustomSelect').on('resize.kCustomSelect', function(){
				setDimension();
			});
			plugin._initCustomScroll();
			that.wrapperOptionHeight = that.wrapperOption.outerHeight(true);
			$(document).off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
				if (!plugin.options.preventClose) {
					plugin.hide();
				}
			});

			if(that.closest('.popup__inner').length) {
				that.closest('.popup__inner').off('click.hide' + that.idPlugin).on('click.hide' + that.idPlugin, function(){
					plugin.hide();
				});
			}
		},
		hide: function(){
			var plugin = this,
				that = plugin.element;
			if(plugin.options.onClose){
				plugin.options.onClose(that.selectEl);
			}
			that.scroll.hide();
			that.removeClass('active focus');
			if(that.curItem.length){
				if(that.items.eq(that.curItem.index()).val()){
					that.removeClass('default');
				}
				else{
					that.addClass('default');
				}
			}
			win.off('resize.kCustomSelect');
			openedSelect = null;
			doc.off('mousemove.scrollBar'+ that.idPlugin);
			doc.off('mouseup.scrollBar'+ that.idPlugin);
			doc.off('keydown.scrollBar'+ that.idPlugin);
			doc.off('click.hide' + that.idPlugin);
			that.off('mousewheel.scrollBar');
			that.scroll.off('mousewheel.scrollBar');
		},
		_initCustomScroll: function(){
			var plugin = this,
				that = plugin.element;
			that.draggable = false;
			that.enableScrollBar = true;
			that.ratioOfContenToWrapper = that.scrollContainer.height()/that.wrapperOption.height();
			that.scrollBarBtn.height(that.scrollBarWrapper.height()*that.ratioOfContenToWrapper);
			that.ratioOfContenToScroll = that.scrollContainer.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.ratioOfWrapperToScroll = that.wrapperOption.height() / (that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight());
			that.firstY = 0;
			that.scrollBarBtnPos = 0;
			that.hiddenScroll = false;
			// that.scrollBarBtn.css('top', '');
			if (that.scrollContainer.height() >= that.wrapperOption.height() - 3) {
				that.scrollBarWrapper.hide();
				that.hiddenScroll = true;
			}
			else{
				if(that.scrollBarWrapper.is(':hidden')){
					that.scrollBarWrapper.show();
					that.hiddenScroll = false;
				}
			}

			var scroll = function (e, a) {
				//if(!that.enableScrollBar) return;
				var top = that.scrollBarBtn.position().top,
				preventDefault = true;
				if(e.keyCode === 40 || a < 0) {
					top += that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else if (e.keyCode === 38 || a > 0) {
					top -= that.ratioOfContenToScroll + plugin.options.scrollWith;
					preventDefault = false;
				} else {
					preventDefault = true;
				}
				if (top <= 0){
					top = 0;
				}
				if (top + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.height()){
					top = that.scrollBarWrapper.height() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, top);
				return preventDefault;
			};

			that.scrollBarBtn.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {

				// Fix issue closing custom select when holding mouse on scroll bar.

				//IE9 & Other Browsers
				if (e.stopPropagation) {
					e.stopPropagation();
				}
				//IE8 and Lower
				else {
					e.cancelBubble = true;
				}

				setTimeout(function() {
					clearTimeout(that.focusoutTimer);
				}, 150);

				that.draggable = true;
				that.firstY = e.pageY;
				that.scrollBarBtnPos = that.scrollBarBtn.position().top;

				return false;
			});
			that.scrollBarBtn.off('click.scrollBar').on('click.scrollBar', function (e) {
				//IE9 & Other Browsers
				if (e.stopPropagation) {
					e.stopPropagation();
				}
				//IE8 and Lower
				else {
					e.cancelBubble = true;
				}
			});
			that.scrollBarWrapper.off('click.scrollBar').on('click.scrollBar', function (e) {
				//IE9 & Other Browsers
				if (e.stopPropagation) {
					e.stopPropagation();
				}
				//IE8 and Lower
				else {
					e.cancelBubble = true;
				}

				setTimeout(function() {
					clearTimeout(that.focusoutTimer);
				}, 150);

				var a = (e.pageY - that.scrollBarWrapper.offset().top) - that.scrollBarBtn.outerHeight()/2;
				if (a <= 0){
					a = 0;
				}
				if (a + that.scrollBarBtn.outerHeight()/2 >= that.scrollBarWrapper.outerHeight()){
					a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
				}
				scrollWithADistance(that, a);
			});

			// Fix issue closing custom select when holding mouse on scroll bar.
			that.scrollBarWrapper.off('mousedown.scrollBar').on('mousedown.scrollBar', function (e) {
				//IE9 & Other Browsers
				if (e.stopPropagation) {
					e.stopPropagation();
				}
				//IE8 and Lower
				else {
					e.cancelBubble = true;
				}

				setTimeout(function() {
					clearTimeout(that.focusoutTimer);
				}, 150);

				if(e.preventDefault){
					e.preventDefault();
				} else {
					e.returnValue = false;
				}
			});

			that.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});
			that.scroll.off('mousewheel.scrollBar').on('mousewheel.scrollBar', function (e, a) {
				if(!that.hiddenScroll){
					scroll(e, a);
				}
				return false;
			});

			doc.off('mousemove.scrollBar'+ that.idPlugin).on('mousemove.scrollBar'+ that.idPlugin, function(e){
				if (that.draggable) {
					var a = that.scrollBarBtnPos + (e.pageY - that.firstY);
					if (a <= 0){
						a = 0;
					}
					if (a + that.scrollBarBtn.outerHeight() >= that.scrollBarWrapper.outerHeight()){
						a = that.scrollBarWrapper.outerHeight() - that.scrollBarBtn.outerHeight();
					}
					scrollWithADistance(that, a);
				}
				return false;
			});
			doc.off('mouseup.scrollBar'+ that.idPlugin).on('mouseup.scrollBar'+ that.idPlugin, function(){
				that.draggable = false;
				return false;
			});

			doc.off('keydown.scrollBar'+ that.idPlugin).on('keydown.scrollBar'+ that.idPlugin, function(e){
				var cur, next, curPos, minus, character, isStartedWithSelectedCharacter, collection;
				var returnFalse = false;
				if(!that.hiddenScroll){
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.active');
							next = cur.removeClass('active').next().addClass('active');
							if(next.length){
								curPos = that.scrollBarBtn.position().top*(that.ratioOfWrapperToScroll - that.ratioOfContenToScroll);
								minus = next.index()*next.outerHeight(true) + next.outerHeight(true) - that.scrollContainer.height() ;
								if(minus  > curPos){
									scrollWithY(that, Math.max(0, minus));
								}
							}
							else{
								next = that.scrollContainer.find('li:first').addClass('active');
								scrollWithY(that, next.index()*next.outerHeight(true), plugin.options.itemsShow);
							}
							returnFalse = true;

							that.simulator.val(next.data('value'));
							break;
						case key.UP:
							cur = that.scrollContainer.find('li.active');
							next = cur.removeClass('active').prev().addClass('active');
							if(next.length){
								curPos = that.scrollBarBtn.position().top * (that.ratioOfWrapperToScroll - that.ratioOfContenToScroll) - that.scrollContainer.height();
								minus = next.index()*next.outerHeight(true) - that.scrollContainer.height();
								if(minus <= curPos){
									scrollWithY(that, next.index()*next.outerHeight(true));
								}
							}
							else{
								next = that.scrollContainer.find('li:last').addClass('active');
								scrollWithY(that, next.index()*next.outerHeight(true), plugin.options.itemsShow);
							}
							returnFalse = true;

							that.simulator.val(next.data('value'));
							break;
						case key.ENTER:
							cur = that.scrollContainer.find('li.active');

							that.simulator.val(cur.data('value'));

							cur.trigger('click.select');
							plugin.hide();
							break;
						default:
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.active');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}
								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return ($(this).text()[0].toLowerCase() === character && $(this).data('value') !== '');
								});
								if(!collection.length) {
									break;
								}
								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('active');
									cur = collection.first().addClass('active');
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('active');
										cur = collection.eq(collection.index(cur) + 1).addClass('active');
									}
									else {
										cur.removeClass('active');
										cur = collection.first().addClass('active');
									}
								}
								scrollWithY(that, cur.index() * cur.outerHeight(true), plugin.options.itemsShow);

								that.simulator.val(cur.data('value'));
							}
							break;
					}
				}
				else{
					switch(e.which){
						case key.DOWN:
							cur = that.scrollContainer.find('li.active');
							next = cur.removeClass('active').next().addClass('active');
							if(!next.length){
								next = that.scrollContainer.find('li:first').addClass('active');
							}
							returnFalse = true;

							that.simulator.val(next.data('value'));
							break;
						case key.UP:
							cur = that.scrollContainer.find('li.active');
							next = cur.removeClass('active').prev().addClass('active');
							if(!next.length){
								next = that.scrollContainer.find('li:last').addClass('active');
							}
							returnFalse = true;

							that.simulator.val(next.data('value'));
							break;
						case key.ENTER:
							cur = that.scrollContainer.find('li.active');

							that.simulator.val(cur.data('value'));

							cur.trigger('click.select');
							plugin.hide();
							break;
						default:
							if(String.fromCharCode(e.which).length) {
								character = String.fromCharCode(e.which).toLowerCase();
								cur = that.scrollContainer.find('li.active');
								if(!cur.length) {
									cur = that.scrollContainer.find('li').first();
								}
								isStartedWithSelectedCharacter = (cur.text()[0].toLowerCase() === character && cur.data('value') !== '');
								collection = that.scrollContainer.find('li').filter(function() {
									return ($(this).text()[0].toLowerCase() === character && $(this).data('value') !== '');
								});
								if(!collection.length) {
									break;
								}
								if(!isStartedWithSelectedCharacter) {
									cur.removeClass('active');
									cur = collection.first().addClass('active');
								}
								else {
									if(collection.index(cur) !== collection.length - 1) {
										cur.removeClass('active');
										cur = collection.eq(collection.index(cur) + 1).addClass('active');
									}
									else {
										cur.removeClass('active');
										cur = collection.first().addClass('active');
									}
								}

								that.simulator.val(cur.data('value'));
							}
							break;
					}
				}
				if(returnFalse){
					return false;
				}
				//return scroll(e);
			});

			var curActive = that.scrollContainer.find('li.active');

			scrollWithY(that, Math.max(0, curActive.index()*curActive.outerHeight(true)));
			// doc.off('mousemove.scrollBar'+ that.idPlugin +' mouseup.scrollBar'+ that.idPlugin +' keydown.scrollBar'+ that.idPlugin).on({
			// 	'mousemove.scrollBar' : function (e) {
			// 	},
			// 	'mouseup.scrollBar' : function () {
			// 		that.draggable = false;
			// 		return false;
			// 	},
			// 	'keydown.scrollBar' : function (e) {
			// 	}
			// });
		},
		_reInitCustomScroll: function(){
			var plugin = this,
				that = plugin.element;

			var curActive = that.scrollContainer.find('li.active');

			scrollWithY(that, Math.max(0, curActive.index()*curActive.outerHeight(true)));
		},
		destroy: function() {
			$.removeData(this.element[0], pluginName);
		},
		enable: function() {
			var that = this.element;
			that.removeClass('disabled');
		},
		disable: function() {
			var that = this.element;
			that.addClass('disabled');
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				/* jshint -W030 */
				window.console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		selectEl: '> select',
		customText: '> span.select__text',
		arrow: '> a',
		itemsShow: 4,
		scrollWith : 2,
		heightItem: 43,
		padding: 3,
		preventClose: false,
		onOpen: function(){},
		afterSelect: function(){},
		onClose: function(){}
	};

}(jQuery, window));

/**
 * @name SIA
 * @description Define global highlightInput functions
 * @version 1.0
 */
SIA.highlightInput = function(){
	var doc = SIA.global.vars.doc;
	var el = null;

	// fix issue autocomplete when clear the text and focus other input, the autocomplete doesn't close
	doc.off('focus.highlightInput').on('focus.highlightInput', 'input, select', function(e) {
		if (el && el.length && el.closest('[data-autocomplete]').length && el.get(0) !== e.target) {
			setTimeout(function() {
				if(el.data('uiAutocomplete')){
					el.autocomplete('close');
					el = $(e.target);
				}
			}, 1200);
		}
		else {
			el = $(e.target);
		}
	});

	var ip = $('.input-3,.input-1,.textarea-1,.textarea-2').not('.disabled').filter(function(idx, el){
		if(!$(el).closest('[data-return-flight]').length){
			return el;
		}
	});
	ip.off('focusin.highlightInput').on('focusin.highlightInput', function(){
		var self = $(this);
		if (!self.hasClass('disabled')) {
			self.addClass('focus');
			self.removeClass('default');
		}
	}).off('focusout.highlightInput').on('focusout.highlightInput', function(){
		var self = $(this);
		var child = self.children();
		self.removeClass('focus');
		// fix issue for alphanumeric if there is no rule-required
		if(child.data('rule-alphanumeric') && !child.data('rule-required')){
			self.closest('.grid-col').removeClass('error').find('.text-error').remove();
		}
	}).find('input').off('change.removeClassDefault').on('change.removeClassDefault', function(){
		var self = $(this);
		if(!self.val() || self.val() === self.attr('placeholder')){
			self.addClass('default');
		}
		else{
			self.removeClass('default');
		}
	}).trigger('change.removeClassDefault');
};

'use strict';
/**
 * @name SIA
 * @description Define global initCustomSelect functions
 * @version 1.0
 */
SIA.initCustomSelect = function(){
	if($('.passenger-details-page').length){
		return;
	}
	if(!window.Modernizr.touch && !window.navigator.msMaxTouchPoints){
		// on Desktop
		$('[data-customselect]').customSelect({
			itemsShow: 5,
			heightItem: 43,
			scrollWith: 2,
			afterSelect: function(el, item, idx){
				if(el.is('#economy-5')){
					var outerWraper = el.closest('.form-group');
					var byRoute = outerWraper.siblings('.by-route');
					var byFlightNo = outerWraper.siblings('.by-flight-no');
					if(el.prop('selectedIndex')){
						byRoute.removeClass('hidden');
						byFlightNo.addClass('hidden');
					}
					else{
						byRoute.addClass('hidden');
						byFlightNo.removeClass('hidden');
					}
				}
				if(el.closest('[data-fare-deal]').length){
					$('#fare-deals').children('.manualFareDeals').hide().eq(idx).show();
				}
			}
		});
	}
	/*else{
		// on Tablet and Mobile
		$('select').each(function(){
			if(!$(this).data('defaultSelect')){
				$(this).defaultSelect({
					wrapper: '.custom-select',
					textCustom: '.select__text',
					afterSelect: function(){
						var self = $(this);
						if(self.is('#country-1')){
							var beforeFlyItem = self.closest('.before-fly-choose').next().children('.before-fly-item');
							beforeFlyItem.hide().eq(self.prop('selectedIndex')).show();
						}
						if(self.closest('[data-fare-deal]').length){
							$('#fare-deals').children('.manualFareDeals').hide().eq(self.prop('selectedIndex')).show();
						}
						if(self.is('#economy-5')){
							var outerWraper = self.closest('.form-group');
							var byRoute = outerWraper.siblings('.by-route');
							var byFlightNo = outerWraper.siblings('.by-flight-no');
							if(self.prop('selectedIndex')){
								byRoute.removeClass('hidden');
								byFlightNo.addClass('hidden');
							}
							else{
								byRoute.addClass('hidden');
								byFlightNo.removeClass('hidden');
							}
						}
					},
					isInput: false
				});
			}
		});
	}*/
};

/**
 * @name SIA
 * @description Define global initConfirmCheckbox functions
 * @version 1.0
 */
SIA.initConfirmCheckbox = function() {
	var wrapperFieldConfirm = $('[data-confirm-tc]');
	var btnSubmit = $('[data-button-submit]');

	if(wrapperFieldConfirm.length) {
		var confirmCheckbox = wrapperFieldConfirm.find(':checkbox');

		confirmCheckbox.off('change.confirm').on('change.confirm', function() {
			var btn = btnSubmit.length ? btnSubmit : confirmCheckbox.closest('.form-group-full').next().find(':submit');
			if(btn.length) {
				if(confirmCheckbox.is(':checked')) {
					btn.removeClass('disabled').prop('disabled', false);
				}
				else {
					btn.addClass('disabled').prop('disabled', true);
				}
			}
		}).trigger('change.confirm');
	}
};

/**
 * @name SIA
 * @description Define global initLangToolbar functions
 * @version 1.0
 */
SIA.initLangToolbar = function() {
	var global = SIA.global;
	var body = global.vars.body;

	if (!body.hasClass('popup-window-login-page') && !body.hasClass('popup-window-logout-page')) {
		var hideTbar = function(tpl) {
			var parentEl = tpl.parent();
			tpl.remove();
			if(!parentEl.children().length) {
				parentEl.remove();
			}
		};

		var showTBar = function(tpl) {
			// var delayTime = 200;
			body.prepend(tpl);
			// if($('.at-a-glance-page').length){
			//  delayTime += 800;
			// }
			// setTimeout(function() {
			//  tpl.removeAttr('style');
			// }, delayTime);
		};

		if(!body.is('.interstitial-page')) {
			$.get(global.config.url.langToolbarTemplate, function(templateStr) {
				var tpl = $(templateStr);

				tpl.find('.toolbar__close.language__close').off('click.closeToolbar').on('click.closeToolbar', function(e) {
					e.preventDefault();
					hideTbar(tpl.find('.toolbar__content.lang__content'));
				});

				tpl.find('.toolbar__close.alert__close').off('click.closeToolbar').on('click.closeToolbar', function(e) {
					e.preventDefault();
					hideTbar(tpl.find('.toolbar__content.alert__content'));
				});

				tpl.find('[data-lang-toolbar]').off('click.translateLang').on('click.translateLang', function(e) {
					e.stopPropagation();
					e.preventDefault();
					hideTbar(tpl.find('.toolbar__content.lang__content'));
					global.vars.setCookie('translateTo', true, 7);
				});

				if(global.vars.getCookie('translateTo')) {
					hideTbar(tpl.find('.toolbar__content.lang__content'));
				}

				showTBar(tpl);
			}, 'html');
		}
	}
};

/**
 * @name SIA
 * @description Define global fixPlaceholder functions
 * @version 1.0
 */
SIA.fixPlaceholder = function(){
	// creating a fake placeholder for not supporting placeholder
	var placeholderInput = $('[placeholder]');
	
	if(!window.Modernizr.input.placeholder){
		placeholderInput.each(function(){
			var self = $(this);
			if(!self.data('placeholder-enabled')){
				self.placeholder();
			}
		});
	}
};

/**
 * @name SIA
 * @description Define global initToggleButtonValue functions
 * @version 1.0
 */
SIA.initToggleButtonValue = function() {
	var buttonToggle = $('[data-toggle-value]');
	buttonToggle.off('click.toggleLabel').on('click.toggleLabel',function() {
		var toggleValue = $(this).data('toggle-value');
		var currentValue = $(this).is('input') ? $(this).val() : $(this).html();
		var slideItem = $(this).closest('.slide-item');
		var index = slideItem.attr('index');
		$(this).data('toggle-value', currentValue);
		if($(this).is('input')) {
			$(this).val(toggleValue);
		}
		else {
			$(this).html(toggleValue);
		}
		if(slideItem.length){
			slideItem.siblings('[index="'+ index +'"]').find('[data-toggle-value]').html(toggleValue);
		}
	});
};

/**
 *  @name Popup
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'Popup';
	var win = $(window);
	var body = $(document.body);
	var mobileDevice = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
	// var docScroll = window.Modernizr.touch || window.navigator.msMaxTouchPoints ? 0 : 20;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	function onIpad() {
		// var userAgent = navigator.userAgent.toLowerCase();
		// var isIOS = userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || ('ontouchstart' in window);
		var mobile = window.Modernizr.touch || window.navigator.msMaxTouchPoints;
		if (!mobile) {
			return false;
		} else {
			return true;
		}
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
					that = plugin.element;
			that.options = plugin.options;
			that.modal = that.attr('data-modal-popup') ? $(that.attr('data-modal-popup')) : that;
			that.modal = that.modal.appendTo(body);
			that.detectIpad = onIpad();
			that.isShow = false;
			that.isAnimate = false;
			that.timer = null;
			that.freezeBody = false;
			that.heightHolderContainer = 0;
			that.isTablet = false;
			that.isMobile = false;
			that.isIpad = /iPad/i.test(window.navigator.userAgent);
			that.scrollTopHolder = 0;
			that.winW = win.width();

			plugin.vars = {
				container: $(plugin.options.container)
			};

			that.modal.addClass('hidden');
		},
		freeze: function(){
			var plugin = this,
					that = plugin.element,
					langToolbarEl = $('.toolbar--language');

			if(langToolbarEl.length) {
				if(win.scrollTop() > langToolbarEl.height()) {
					langToolbarEl.css('visibility', 'hidden');
				}
			}

			plugin.vars.container.css({
				'marginTop': ''/*- that.scrollTopHolder*//* + langToolbarEl.outerHeight()*/,
				'height': win.height() + that.scrollTopHolder/*,
				'overflow': 'hidden'*/
			});
			$('html').addClass('no-flow');
		},
		showTablet: function() {
			var plugin = this,
					that = plugin.element;
			plugin.reposition();
			that.freezeBody = true;
			plugin.freeze();
			that.modal.removeClass('hidden').addClass('animated fadeIn').css('zIndex', (that.options.zIndex + 1));
			that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
				if(that.modal.outerHeight(true) > win.height()){
					that.modal.find('.popup__content').removeAttr('style').css({
						height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
						'overflow-y': 'auto'
					});
					win.scrollTop(0);
				}
				if(that.options.afterShow){
					that.options.afterShow(that);
				}
				plugin.freeze();
				that.trigger('afterShow');
				that.isAnimate = false;
				that.isTablet = true;
				plugin.addResize();
				that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
			});
		},
		// showMobile: function() {
		// 	var plugin = this,
		// 			that = plugin.element;
		// 	plugin.reposition();
		// 	that.modal.css('top', 0);
		// 	// freeze

		// 	if(that.modal.outerHeight(true) > win.height()){
		// 		plugin.vars.container.css('height', that.modal.outerHeight(true));
		// 	}
		// 	else{
		// 		plugin.vars.container.css('height', win.height());
		// 		that.modal.height(win.height());
		// 	}
		// 	plugin.vars.container.css('overflow', 'hidden');
		// 	that.modal.css('zIndex', (that.options.zIndex + 1));
		// 	that.modal.addClass('animated slideInRight');
		// 	that.isMobile = true;
		// 	that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
		// 		win.scrollTop(0);
		// 		if(that.options.afterShow){
		// 			that.options.afterShow(that);
		// 		}
		// 		// callback function
		// 		that.trigger('afterShow');
		// 		that.modal.css('right', 0);
		// 		that.isAnimate = false;
		// 		plugin.addResize();
		// 		that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
		// 	});
		// },
		reset: function(){
			var plugin = this,
					that = plugin.element;
			if(that.modal.outerHeight(true) > win.height()){
				if(that.isMobile){
					plugin.vars.container.css('height', that.modal.outerHeight(true));
					plugin.vars.container.css('overflow', 'hidden');
				}
			}
			else{
				that.modal.find('.popup__content').removeAttr('style');
				if(that.isMobile){
					plugin.vars.container.css('height', win.height());
					that.modal.height(win.height());
					plugin.vars.container.css('overflow', 'hidden');
				}
			}
			body.attr('scroll', 'no');
			if(that.isTablet){
				if(that.isShow){
					plugin.freeze();
				}
				if(win.width() > plugin.options.mobile ){
					if(that.modal.outerHeight(true) > win.height()){
						that.modal.find('.popup__content').removeAttr('style').css({
							height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
							'overflow-y': 'auto'
						});
					}
					else{
						that.modal.find('.popup__content').removeAttr('style');
					}
				}
			}
		},
		show: function() {
			var plugin = this,
					that = plugin.element;
			if(that.isShow){
				return;
			}
			that.winW = win.width();
			that.trigger('beforeShow');
			if(that.options.beforeShow){
				that.options.beforeShow(that);
			}

			that.isShow = true;
			that.isAnimate = true;
			that.overlay = $(that.options.overlayBGTemplate).appendTo(body);
			that.scrollTopHolder = win.scrollTop();
			/*if(window.Modernizr.csstransitions){
				that.overlay.show().css({
					'zIndex': that.options.zIndex,
					opacity: 0.5
				}).addClass('animated fadeInOverlay');
				that.modal.show();
				if(win.width() < plugin.options.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
					plugin.showMobile();
				}
				else{ //tablet
					plugin.showTablet();
					if(that.isIpad){
						that.overlay.css({
							'position': 'absolute',
							'top': 0,
							'left': 0
						});
					}
				}
			}
			else{

			}*/
			plugin.reposition();
			that.isTablet = true;
			that.freezeBody = true;
			plugin.freeze();
			if(that.modal.outerHeight(true) > win.height()){
				that.modal.find('.popup__content').removeAttr('style').css({
					height: win.height() - ( parseInt(that.modal.find('.popup__content').css('padding-bottom')) * 2 ),
					'overflow-y': 'auto'
				});
			}
			that.overlay.css({
				'zIndex': that.options.zIndex
			}).show();
			that.modal.addClass(that.options.modalShowClass).removeClass('hidden').css('zIndex', (that.options.zIndex + 1));

			plugin.freeze();
			if(that.options.afterShow){
				that.options.afterShow(that);
			}
			that.trigger('afterShow');
			that.isAnimate = false;
			plugin.addResize();

			/*else{
				that.overlay.css({
					'zIndex': that.options.zIndex,
					opacity: 0.5
				}).fadeIn(plugin.options.duration);
				that.modal.addClass(that.options.modalShowClass).hide().fadeIn(plugin.options.duration, function(){
					if(!plugin.vars.container.data('setHeight')){
						plugin.freeze();
					}
					// plugin.reposition();
					if(that.options.afterShow){
						that.options.afterShow(that);
					}
					that.trigger('afterShow');
					that.isAnimate = false;
					plugin.addResize();
					// if(win.width() >= plugin.options.mobile){
					// }
				}).css('zIndex', (that.options.zIndex + 1));
			}*/

			that.modal.find(that.options.triggerCloseModal).on({
				'click.hideModal': function(e) {
					//e.stopPropagation();
					e.preventDefault();
					if(!that.isAnimate){
						plugin.hide();
					}
					if(that.find('[data-customselect]').length){
						$(document).trigger('click');
					}
				}
			});
			that.modal.off('click.doNothing').on('click.doNothing', function(){
				// fix for lumina 820 and 1020
			});
			if(that.options.closeViaOverlay){
				that.overlay.on({
					'click.hideModal': function(e) {
						//e.stopPropagation();
						e.preventDefault();
						if(!that.isAnimate){
							plugin.hide();
						}
					}
				});

				that.modal.on({'click.hideModal': function(e) {
						if(!that.isAnimate && !$(e.target).closest('.popup__inner').length){
							// e.preventDefault();
							plugin.hide();
						}
					}
				});
				// that.modal.find('.popup__inner').on('click.hideModal', function(e) {
				// 	if(!e) {
				// 		e = window.event;
				// 	}

				// 	//IE9 & Other Browsers
				// 	if (e.stopPropagation) {
				// 		e.stopPropagation();
				// 	}
				// 	//IE8 and Lower
				// 	else {
				// 		e.cancelBubble = true;
				// 	}
				// });
			}

			that.isShow = true;
		},
		addResize: function(){
			var plugin = this,
				that = plugin.element;
			// resize
			win.on({
				'resize.reposition': function() {
					if(!that.isShow || (win.width() === that.winW && mobileDevice)){
						return;
					}
					that.winW = win.width();
					if(that.timer){
						clearTimeout(that.timer);
					}
					that.timer = setTimeout(function(){
						that.modal.css('height', '');
						// $(plugin.options.container).css('height', '');
						that.modal.find('.popup__content').css('height', '');

						plugin.reset();
						if(that.isShow){
							plugin.reposition();
						}
						/*if(that.isMobile && win.width() > plugin.options.mobile){
							that.modal.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
							plugin.vars.container.removeAttr('style');
							win.scrollTop(that.scrollTopHolder);
							plugin.showTablet();
							that.isMobile = false;
							that.isTablet = true;
						}
						else if(that.isTablet && win.width() < plugin.options.mobile){
							that.modal.css('height', '').hide().removeClass('animated slideOutRight fadeIn slideInRight fadeOut');
							win.scrollTop(that.scrollTopHolder);
							plugin.showMobile();
							that.isTablet = false;
							that.isMobile = true;
						}*/
					}, that.options.duration + 100);
				},
				'keyup.escape': function(e) {
					if (e.which === 27) {
						plugin.hide();
						if(that.find('[data-customselect]').length){
							$(document).trigger('click');
						}
					}
				}
			});
		},
		hide: function() {
			var plugin = this,
					that = plugin.element,
					langToolbarEl = $('.toolbar--language');

			if(that.overlay){
				that.isShow = false;
				that.trigger('beforeHide');
				that.modal.find(that.options.triggerCloseModal).off('click.hideModal');
				that.overlay.off('click.hideModal');
				if(langToolbarEl.length) {
					langToolbarEl.removeAttr('style');
				}
				if(that.options.beforeHide){
					that.options.beforeHide(that);
				}
				/*if(window.Modernizr.csstransitions){
					if(win.width() < plugin.options.mobile && (window.Modernizr.touch || window.navigator.msMaxTouchPoints)){ // mobile
						that.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						plugin.reposition();
						that.modal.addClass('animated slideOutRight');
						// unfreeze
						that.modal.css('height', '');

						if(!that.freezeBody){
							// $(plugin.options.container).css('height', that.heightHolderContainer);
							that.heightHolderContainer = 0;
						}
						else if(that.freezeBody){
							plugin.vars.container.removeAttr('style').data('popupSetHeight', false);
							win.off('resize.reposition keyup.escape');
							that.freezeBody = false;
						}
						that.modal.find('.popup__content').removeAttr('style');


						that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							win.scrollTop(that.scrollTopHolder);
							that.scrollTopHolder = 0;
							if(that.heightHolderContainer){
								// $(plugin.options.container).height(that.heightHolderContainer);
								that.heightHolderContainer = 0;
							}

							that.modal.removeAttr('style').hide().removeClass('animated slideOutRight fadeIn slideInRight');
							that.overlay.off('click.hideModal').remove();

							that.isAnimate = false;

							// callback function
							if(that.options.afterHide){
								that.options.afterHide(that);
							}
							that.trigger('afterHide');
							that.modal.css('right', '');
							that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
					else{ // tablet
						that.overlay.removeClass('fadeInOverlay').addClass('animated fadeOutOverlay');
						that.modal.addClass('animated fadeOut');
						if(that.freezeBody){
							plugin.vars.container.removeAttr('style').data('popupSetHeight', false);
							win.scrollTop(that.scrollTopHolder);
						}
						that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
							that.modal.removeAttr('style').hide().removeClass('animated fadeOut fadeIn slideInRight');
							if(that.freezeBody){
								plugin.vars.container.removeAttr('style').data('popupSetHeight', false);
								that.freezeBody = false;
								win.off('resize.reposition keyup.escape');
							}
							if(!plugin.vars.container.data('setHeight') && win.width() >= plugin.options.tablet - docScroll){
								plugin.vars.container.removeAttr('style');
								plugin.vars.container.data('setHeight', false);
							}
							// win.scrollTop(that.scrollTopHolder);
							that.scrollTopHolder = 0;
							that.overlay.off('click.hideModal').remove();
							that.isAnimate = false;
							that.modal.find('.popup__content').removeAttr('style');
							if(that.options.afterHide){
								that.options.afterHide(that);
							}
							that.trigger('afterHide');
							that.modal.css('right', '');
							that.modal.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
						});
					}
				}
				else{

				}*/
				that.freezeBody = false;
				plugin.vars.container.removeAttr('style');
				body.removeAttr('scroll');
				$('html').removeClass('no-flow');
				win.off('resize.reposition keyup.escape');
				win.scrollTop(that.scrollTopHolder);
				that.scrollTopHolder = 0;
				that.modal.removeClass(that.options.modalShowClass);
				that.modal.removeAttr('style').addClass('hidden');
				that.isAnimate = false;
				that.modal.find('.popup__content').removeAttr('style');
				if(that.options.afterHide){
					that.options.afterHide(that);
				}
				that.trigger('afterHide');
				that.overlay.hide().off('click.hideModal').remove();
				that.overlay = null;

				/*else{
					that.modal.removeClass(that.options.modalShowClass).fadeOut(plugin.options.duration, function(){
						that.modal.removeAttr('style').hide();
						that.isAnimate = false;
						that.modal.find('.popup__content').removeAttr('style');
						if(that.options.afterHide){
							that.options.afterHide(that);
						}
						that.trigger('afterHide');
					});
					that.overlay.fadeOut(plugin.options.duration, function(){
						that.overlay.off('click.hideModal').remove();
					});
				}*/
			}
		},
		reposition: function() {
			var plugin = this,
					that = plugin.element,
					popupInner = that.modal.find('.popup__inner');

			that.modal.removeClass('hidden').css({
				// top: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0, (win.height() - that.modal.height()) / 2),
				// top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2) :  0,
				// top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  Math.max(0, (win.height() - that.modal.height()) / 2) + win.scrollTop(),
				position : (!that.detectIpad) ? ((win.height() > popupInner.height()) ? 'fixed' : 'fixed') : (win.width() < plugin.options.mobile) ? 'absolute' : 'fixed'
				// left: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0 ,(win.width() - that.modal.width()) / 2)
				// left: Math.max(0 ,(win.width() - that.modal.width()) / 2)
			});

			var innerHeight = popupInner.height();
			// var contentHeight = popupInner.find('.popup__content').height();
			// if (innerHeight > contentHeight) {
			// 	innerHeight = contentHeight;
			// }

			popupInner.css({
				top: win.height() > innerHeight ? Math.max(0, (win.height() - innerHeight) / 2) : 0,
				left: Math.max(0, (win.width() - that.modal.width()) / 2)
			});

			// that.modal.show().css({
			// 	// top: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0, (win.height() - that.modal.height()) / 2),
			// 	top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  0,
			// 	// top: (win.height() > that.modal.height()) ? Math.max(0, (win.height() - that.modal.height()) / 2 + Math.abs(body.offset().top)) + (window.Modernizr.touch ? win.scrollTop() : 0) :  Math.max(0, (win.height() - that.modal.height()) / 2) + win.scrollTop(),
			// 	position : (!that.detectIpad) ? ((win.height() > that.modal.height()) ? 'fixed' : 'absolute') : 'absolute',
			// 	// left: (window.Modernizr.touch && (win.width() < 768)) ? 0 : Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// 	left: Math.max(0 ,(win.width() - that.modal.width()) / 2)
			// });
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		overlayBGTemplate: '<div class="bg-modal radial" id="overlay"></div>',
		modalShowClass: 'modal-show',
		triggerCloseModal: '.modal_close, a.btn-gray, [data-close]',
		duration: 500,
		container: '#container',
		mobile: 768,
		tablet: 988,
		zIndex: 20,
		closeViaOverlay: true,
		beforeShow: function() {},
		beforeHide: function() {}
	};
}(jQuery, window));

/**
 * @name SIA
 * @description Define global initSkipTabIndex functions
 * This function is created for accessibility
 * @version 1.0
 */

SIA.initSkipTabIndex = function(elm){
	if(elm){
		elm.find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
	}else{
		$('[data-skip-tabindex]').each(function(){
			$(this).find('a, area, button, input, object, select, textarea').attr('tabindex', '-1');
			$(this).removeAttr('data-skip-tabindex');
		});
	}
};

/**
 * @name SIA
 * @description Define social network functions
 * @version 1.0
 */
SIA.socialNetworkAction = function(){
	var global = SIA.global;
	var config = global.config;
	var social = $('[data-social]');
	var cocialSharing = function (element, link) {
		$(element).attr('href', link);
		$(element).attr('target', '_blank');
	};
	social.each(function(){
		var fb = $('[data-facebook]', this);
		var tw = $('[data-twitter]', this);
		var gplus = $('[data-gplus]', this);
		cocialSharing(fb, config.url.social.facebookSharing + window.location.href);
		cocialSharing(tw, config.url.social.twitterSharing + window.location.href);
		cocialSharing(gplus, config.url.social.gplusSharing + window.location.href);
	});
};

(function($, window, undefined) {
  var pluginName = 'stickyWidget',
      win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      var that = this,
          el = that.element,
          elPos = el.offset().top,
          openStickyEl = el.find('[data-sticky-open]'),
          stickyBlock = el.find('.booking-widget-block'),
          closeStickyEl = el.find('.sticky__close');
      win.off('scroll.' + pluginName).on('scroll.' + pluginName,
        function(){
        if(win.scrollTop() >= elPos) {
          el.addClass('sticky');
          if(stickyBlock.is('.hidden')) {
            openStickyEl.removeClass('hidden');
          }
          el.find('input').blur();
          $('.ui-datepicker').hide();
        } else {
          openStickyEl.addClass('hidden');
          stickyBlock.addClass('hidden');
          closeStickyEl.css('display','none');
          el.removeClass('sticky');
        }
      })
      openStickyEl.off('click.openSticky').on('click.openSticky', function(){
          stickyBlock.slideDown('fast').removeClass('hidden');
          openStickyEl.addClass('hidden');
          closeStickyEl.css('display', 'block');
      });
      closeStickyEl.off('click.closeSticky').on('click.closeSticky', function(){
          $.when(stickyBlock.slideUp('fast')).done(function(){
              openStickyEl.removeClass('hidden');
          });
          closeStickyEl.css('display', 'none');
      });
      closeStickyEl.off('keydown.closeSticky').on('keydown.closeSticky', function(e){
          e.preventDefault();
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 9 || keyCode === 13) {
          $(this).trigger('click.closeSticky');
        }

      });
      this.checkRadio();
    },

    checkRadio: function() {
      var that = this,
          el   = that.element,
          radioEl = el.find('input:radio'),
          target = el.find('[data-target]');
      radioEl.each(function(idx){
        var self = $(this);
        if(self.is(':checked')){
          if(idx){
            target.eq(1).removeClass('hidden');
            target.eq(0).addClass('hidden');
          }
          else{
            target.eq(0).removeClass('hidden');
            target.eq(1).addClass('hidden');
          }
        }
        self.off('change.stickyWidget').on('change.stickyWidget', function(){
          if(self.is(':checked')){
            if(idx){
              target.eq(1).removeClass('hidden');
              target.eq(0).addClass('hidden');
            }
            else{
              target.eq(0).removeClass('hidden');
              target.eq(1).addClass('hidden');
            }
          }
        });
      });
    },

    destroy: function() {
      $.removeData(this.element[0], pluginName);
    }
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
  };

  $(function() {
   $('[data-' + pluginName + ']')[pluginName]();
  });

}(jQuery, window));


(function($, window, undefined) {
	var pluginName = 'tabMenu';
	var win = $(window);
	var body = $(document.body);
	var html = $('html');
  var itemTab = $('.tab-nav-item');
  var customRadio = $('.custom-radio');
	var centerPopup = function(el, visible, scrollTopHolder){
		el.css({
			top: Math.max(0, (win.height() - el.innerHeight()) / 2) + scrollTopHolder,
			display: (visible ? 'block' : 'none'),
			left: Math.max(0, (win.width() - el.innerWidth()) / 2)
		});
	};

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			that.tab = that.find(that.options.tab);
			that.tabContent = that.find(that.options.tabContent);
			that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
			that.selectOnMobile = that.find(that.options.selectOnMobile);
			that.freezeBody = false;
			that.isIpad = /iPad/i.test(window.navigator.userAgent);
			that.scrollTopHolder = 0;

			var container = $(plugin.options.container);
			var tabContentOverlay = $();
			var tabMenuTimer = null;
			var isOpen = false;
			var isTablet = false;
			var onResize = null;
			var listTabs = that.find('.tab > .tab-item');
			// var isIE = function() {
			// 	var myNav = navigator.userAgent.toLowerCase();
			// 	return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
			// };

      itemTab.on('click', function() {
        var $this = $(this),
            itemClick = $(this).index(),
            itemActive = that.find('[data-tab-nav-item]'),
            widget = $('#travel-widget'),
            idx = $(this).index();
        if(!$(this).is('.active')) {
          itemTab.removeClass('active');
          $(this).addClass('active');
          var tabItem =  itemActive.eq(itemClick);
          var tabItemIndex = itemActive.eq(itemClick).index();
          var customSelectActive = $('#travel-widget [data-customselect].active');
          itemActive.removeClass('active');
          if(itemClick === tabItemIndex){
            tabItem.addClass('active');
          }
          $this.parent().find('.tab-nav-item').removeClass('active');
          $this.parent().find('.tab-nav-item').attr('aria-selected', false);
          $this.addClass('active');
          $this.attr('aria-selected', true);

          if(customSelectActive.length > 0) {
            customSelectActive.removeClass('focus');
            customSelectActive.customSelect('hide');
          }

          return false;
        }
      });
      var customRadioTab = function(){
        var radioBook = that.find('#travel-radio-1'),
          radioRedeem = that.find('#travel-radio-2'),
          bookFlight = that.find('#book-flight'),
          bookRedeem = that.find('#book-redem');
          if (radioBook.is(':checked')) {
            bookFlight.addClass('active');
          }else{
             bookFlight.removeClass('active');
          }if (radioRedeem.is(':checked')){
            bookRedeem.addClass('active');
          }else{
            bookRedeem.removeClass('active');
          }
      };

      customRadio.on('click',function() {
        if(that.data('widget-v1')){
          customRadioTab();
        }
      });

			if(that.options.isPopup){
				onResize = function(parent, self){
					centerPopup(parent, true, 0);
					parent.css('height', '');
					that.tabContent.eq(that.tab.index(self)).css('height', '');
					// container.css('height', '');
					if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) > win.height()){
						parent.find('.tab-content.active').css({
							height: win.height() - parseInt(that.tabContent.eq(that.tab.index(self)).css('padding-top')),
							'overflow-y': 'auto'
						});
						container.css({
							'marginTop': - that.scrollTopHolder,
							'height': win.height() + that.scrollTopHolder,
							'overflow': 'hidden'
						});
					}
					else{
						that.tabContent.eq(that.tab.index(self)).removeAttr('style');
						container.css({
							'marginTop': - that.scrollTopHolder,
							'height': win.height() + that.scrollTopHolder,
							'overflow': 'hidden'
						});
					}
					if(win.width() >= that.options.tablet - that.options.docScroll){
						tabContentOverlay.remove();
						parent.removeClass('animated fadeIn').removeAttr('style');
						that.tabContent.eq(that.tab.index(self)).css('height', '');
						if(that.freezeBody){
							body.removeAttr('style');
							html.removeAttr('style');
							that.freezeBody = false;
						}
						$(plugin.options.container).removeAttr('style');
						win.scrollTop(that.scrollTopHolder);
						that.scrollTopHolder = 0;
						isOpen = false;
						win.off('resize.resetTabMenu');
						return;
					}
					parent.show();
				};
			}

			//that.tabContent.removeClass('hidden').not(':eq(' + that.index + ')').hide();

			that.selectOnMobile.prop('selectedIndex', that.index);
			that.selectOnMobile.off('change.triggerTab').on('change.triggerTab', function(){
				if(that.data('tab')){
					that.tab.eq(that.selectOnMobile.prop('selectedIndex')).trigger('click.show');
				}
				else{
					if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).is('a')){
						window.location.href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).attr('href');
					}
					else{
						if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').length){
							var href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').attr('href');
							if(!href.match(':void')){
								window.location.href = href;
							}
						}
					}
				}
			}).triggerHandler('blur.resetValue');

			var changeTab = function(self){
				// if(!self.hasClass('hidden')) {
				that.tab/*.eq(that.index)*/.removeClass(that.options.activeClass);
				// }
				if(that.options.animation){
					that.tabContent.eq(that.index).fadeOut(that.options.duration);
				}
				else{
					that.tabContent.eq(that.index).removeClass(that.options.activeClass);
				}

				if(!self.hasClass('hidden')) {
					self.addClass(that.options.activeClass);
				}

				that.index = that.tab.index(self);

				if(that.options.animation){
					that.tabContent.eq(that.index).css({
						'position': 'absolute',
						top: 80
					}).fadeIn(that.options.duration, function(){
						$(this).css('position', '');
					});
				}
				else{
					that.tabContent
						.removeClass(that.options.activeClass)
						.eq(that.index).addClass(that.options.activeClass);
					that.selectOnMobile.prop('selectedIndex', that.index);
				}
			};

			if(!that.data('tab')){
				return;
			}
			that.tab.off('click.show').on('click.show', function(e) {
				e.preventDefault();
				if($.isFunction(that.options.beforeChange)){
					var resultBeforeChange = that.options.beforeChange.call(that, that.tabContent, $(this));
					if(false === resultBeforeChange) {
						return false;
					}
				}

				// show as popup for tabMenu

				if(that.options.isPopup){
					var parent = that.tabContent.parent();
					var self = $(this);
					isOpen = true;

					if(window.innerWidth < that.options.tablet){
						changeTab(self);
						if(window.Modernizr.csstransitions){ // support css3
							that.scrollTopHolder = win.scrollTop();
							parent.show();
							tabContentOverlay = $(that.options.templateOverlay).appendTo(body).show().css({'opacity': 0.5, 'zIndex': that.options.zIndex}).addClass('animated fadeInOverlay');
							parent.css('display', 'block');
							centerPopup(parent, true, 0);
							parent.css('position', 'fixed');
							parent.addClass('animated fadeIn');
							$(plugin.options.container).css({
								'marginTop': - that.scrollTopHolder,
								'height': win.height() + that.scrollTopHolder,
								'overflow': 'hidden'
							});
							parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
								if(parent.outerHeight(true) > win.height()){
									parent.find('.tab-content.active').removeAttr('style').css({
										height: win.height() - parseInt(parent.find('.tab-content.active').css('padding-top')),
										'overflow-y': 'auto'
									});
								}
								centerPopup(parent, true, 0);
								isOpen = false;
								isTablet = true;
								win.scrollTop(0);
								parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
								if($.isFunction(that.options.afterChange)){
									that.options.afterChange.call(that, that.tabContent, $(this));
								}
								that.trigger('afterChange');
							});
						}
						else{ // does not support css3
							if(window.innerWidth >= that.options.mobile){
								parent.css('display', 'block');
								centerPopup(parent, true, that.scrollTopHolder);
								parent.css('opacity', 0);
								that.scrollTopHolder = win.scrollTop();
								centerPopup(parent, true, that.scrollTopHolder);
								parent.animate({
									opacity: 1
								}, that.options.duration);
								isOpen = false;
								tabContentOverlay = $(that.options.templateOverlay).appendTo(body).css({'opacity': 0.5, 'zIndex': that.options.zIndex}).fadeIn(that.options.duration);
							}
						}
						tabContentOverlay.off('click.closeTabContent').on('click.closeTabContent', function(e){
							e.preventDefault();
							that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
						});
						win.off('resize.resetTabMenu').on('resize.resetTabMenu', function(){
							clearTimeout(tabMenuTimer);
							tabMenuTimer = setTimeout(function(){
								onResize(parent, self);
							}, 10);
						});
						return;
					}
				}
				// if($(this).hasClass(that.options.activeClass)){
				// 	return;
				// }
				if ($(this).is('[data-keep-limit]')) {
					return;
				}
				changeTab($(this));

				if($.isFunction(that.options.afterChange)){
					that.options.afterChange.call(that, that.tabContent, $(this));
				}

				var widget = $(this).closest('[data-widget-v2]');

				if(widget) {
				  var btnCollapse = widget.find('[data-bookatrip] [data-trigger-collapsed]'),
				      navWrapper = widget.find('[data-bookatrip] .tab-nav-wrapper');

				  if(navWrapper.is('.tab-form-expand')) {
				    btnCollapse.trigger('click.collapseFrom');
				  }

				};

				that.trigger('afterChange');
			});

			if(that.options.isPopup){
				var closeOnTablet = function(parent){
					// var langToolbarEl = $('.toolbar--language');
					tabContentOverlay.addClass('animated fadeOutOverlay');
					parent.removeClass('fadeIn').addClass('animated fadeOut');
					parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
						$(that.options.container).removeAttr('style');
						// if(langToolbarEl.length) {
						// 	$(that.options.container).css('padding-top', langToolbarEl.height());
						// }
						win.scrollTop(that.scrollTopHolder);
						tabContentOverlay.off('click.closeTabContent').remove();
						parent.removeClass('animated fadeOut');
						that.scrollTopHolder = 0;
						isOpen = false;
						parent.removeAttr('style');
						parent.find('.tab-content.active').removeAttr('style');
						parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
					});
				};
				that.find(that.options.triggerClosePopup).off('click.hideTabPopup').on('click.hideTabPopup', function(e){
					e.preventDefault();
					var parent = $(this).closest('.tab-wrapper');
					if(isOpen){
						return;
					}
					isOpen = true;
					if(window.innerWidth < that.options.tablet){
						if(window.Modernizr.csstransitions){ // support css3
							closeOnTablet(parent);
						}
						else{ // does not support css3
							parent.fadeOut(that.options.duration);
							tabContentOverlay.fadeOut(that.options.duration, function(){
								tabContentOverlay.remove();
								if(that.freezeBody){
									body.removeAttr('style');
									that.freezeBody = false;
								}
								win.scrollTop(that.scrollTopHolder);
								that.scrollTopHolder = 0;
								isOpen = false;
								parent.removeAttr('style');
							}).off('click.closeTabContent');
						}
						win.off('resize.resetTabMenu');
					}
					if($.isFunction(that.options.beforeClosePopup)){
						that.options.beforeClosePopup.call(that, that.tabContent, $(this));
					}
				});
			}
		},
		update: function(){
			var plugin = this,
				that = plugin.element;
			that.options = plugin.options;
			that.tab = that.find(that.options.tab);
			that.tabContent = that.find(that.options.tabContent);
			that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
			that.freezeBody = false;
		},
		option: function(options) {
			this.element.options = $.extend({}, this.options, options);
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				// console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		tab: '',
		tabContent: '',
		activeClass: 'active',
		animation: false,
		duration: 500,
		isPopup: false,
		tablet: 988,
		container: '#container',
		mobile: 768,
		zIndex: 14,
		templateOverlay: '<div class="overlay"></div>',
		triggerClosePopup: '.tab-wrapper .popup__close',
		selectOnMobile: '> select',
		docScroll: window.Modernizr.touch ? 0 : 20,
		beforeClosePopup: function(){},
		afterChange: function() {},
		beforeChange: function(){}
	};

}(jQuery, window));// (function($, window, undefined) {
// 	var pluginName = 'tabMenu';
// 	var win = $(window);
// 	var body = $(document.body);
// 	var html = $('html');

// 	var centerPopup = function(el, visible, scrollTopHolder){
// 		el.css({
// 			top: Math.max(0, (win.height() - el.innerHeight()) / 2) + scrollTopHolder,
// 			display: (visible ? 'block' : 'none'),
// 			left: Math.max(0, (win.width() - el.innerWidth()) / 2)
// 		});
// 	};

// 	function Plugin(element, options) {
// 		this.element = $(element);
// 		this.options = $.extend({}, $.fn[pluginName].defaults, options);
// 		this.init();
// 	}

// 	Plugin.prototype = {
// 		init: function() {
// 			var plugin = this,
// 				that = plugin.element;
// 			that.options = plugin.options;
// 			that.tab = that.find(that.options.tab);
// 			that.tabContent = that.find(that.options.tabContent);
// 			that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
// 			that.selectOnMobile = that.find(that.options.selectOnMobile);
// 			that.freezeBody = false;
// 			that.isIpad = /iPad/i.test(window.navigator.userAgent);
// 			that.scrollTopHolder = 0;

// 			var container = $(plugin.options.container);
// 			var tabContentOverlay = $();
// 			var tabMenuTimer = null;
// 			var isOpen = false;
// 			var isTablet = false;
// 			var onResize = null;
// 			var listTabs = that.find('.tab-item');
// 			// var isIE = function() {
// 			// 	var myNav = navigator.userAgent.toLowerCase();
// 			// 	return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1]) : false;
// 			// };

// 			if(that.options.isPopup){
// 				onResize = function(parent, self){
// 					centerPopup(parent, true, 0);
// 					parent.css('height', '');
// 					that.tabContent.eq(that.tab.index(self)).css('height', '');
// 					// container.css('height', '');
// 					if(that.tabContent.eq(that.tab.index(self)).outerHeight(true) > win.height()){
// 						parent.find('.tab-content.active').css({
// 							height: win.height() - parseInt(that.tabContent.eq(that.tab.index(self)).css('padding-top')),
// 							'overflow-y': 'auto'
// 						});
// 						container.css({
// 							'marginTop': - that.scrollTopHolder,
// 							'height': win.height() + that.scrollTopHolder,
// 							'overflow': 'hidden'
// 						});
// 					}
// 					else{
// 						that.tabContent.eq(that.tab.index(self)).removeAttr('style');
// 						container.css({
// 							'marginTop': - that.scrollTopHolder,
// 							'height': win.height() + that.scrollTopHolder,
// 							'overflow': 'hidden'
// 						});
// 					}
// 					if(win.width() >= that.options.tablet - that.options.docScroll){
// 						tabContentOverlay.remove();
// 						parent.removeClass('animated fadeIn').removeAttr('style');
// 						that.tabContent.eq(that.tab.index(self)).css('height', '');
// 						if(that.freezeBody){
// 							body.removeAttr('style');
// 							html.removeAttr('style');
// 							that.freezeBody = false;
// 						}
// 						$(plugin.options.container).removeAttr('style');
// 						win.scrollTop(that.scrollTopHolder);
// 						that.scrollTopHolder = 0;
// 						isOpen = false;
// 						win.off('resize.resetTabMenu');
// 						return;
// 					}
// 					parent.show();
// 				};
// 			}

// 			//that.tabContent.removeClass('hidden').not(':eq(' + that.index + ')').hide();

// 			that.selectOnMobile.prop('selectedIndex', that.index);
// 			that.selectOnMobile.off('change.triggerTab').on('change.triggerTab', function(){
// 				if(that.data('tab')){
// 					that.tab.eq(that.selectOnMobile.prop('selectedIndex')).trigger('click.show');
// 				}
// 				else{
// 					if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).is('a')){
// 						window.location.href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).attr('href');
// 					}
// 					else{
// 						if(that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').length){
// 							var href = that.tab.eq(that.selectOnMobile.prop('selectedIndex')).find('a').attr('href');
// 							if(!href.match(':void')){
// 								window.location.href = href;
// 							}
// 						}
// 					}
// 				}
// 			}).triggerHandler('blur.resetValue');

// 			var changeTab = function(self){
// 				// if(!self.hasClass('hidden')) {
// 				that.tab/*.eq(that.index)*/.removeClass(that.options.activeClass);
// 				// }
// 				if(that.options.animation){
// 					that.tabContent.eq(that.index).fadeOut(that.options.duration);
// 				}
// 				else{
// 					that.tabContent.eq(that.index).removeClass(that.options.activeClass);
// 				}

// 				if(!self.hasClass('hidden')) {
// 					self.addClass(that.options.activeClass);
// 				}

// 				that.index = that.tab.index(self);

// 				if(that.options.animation){
// 					that.tabContent.eq(that.index).css({
// 						'position': 'absolute',
// 						top: 80
// 					}).fadeIn(that.options.duration, function(){
// 						$(this).css('position', '');
// 					});
// 				}
// 				else{
// 					that.tabContent
// 						.removeClass(that.options.activeClass)
// 						.eq(that.index).addClass(that.options.activeClass);
// 					that.selectOnMobile.prop('selectedIndex', that.index);
// 				}
// 			};

// 			if(!that.data('tab')){
// 				return;
// 			}
// 			that.tab.off('click.show').on('click.show', function(e) {
// 				e.preventDefault();
// 				if($.isFunction(that.options.beforeChange)){
// 					var resultBeforeChange = that.options.beforeChange.call(that, that.tabContent, $(this));
// 					if(false === resultBeforeChange) {
// 						return false;
// 					}
// 				}

// 				// show as popup for tabMenu

// 				if(that.options.isPopup){
// 					var parent = that.tabContent.parent();
// 					var self = $(this);
// 					isOpen = true;

// 					if(window.innerWidth < that.options.tablet){
// 						changeTab(self);
// 						if(window.Modernizr.csstransitions){ // support css3
// 							that.scrollTopHolder = win.scrollTop();
// 							parent.show();
// 							tabContentOverlay = $(that.options.templateOverlay).appendTo(body).show().css({'opacity': 0.5, 'zIndex': that.options.zIndex}).addClass('animated fadeInOverlay');
// 							parent.css('display', 'block');
// 							centerPopup(parent, true, 0);
// 							parent.css('position', 'fixed');
// 							parent.addClass('animated fadeIn');
// 							$(plugin.options.container).css({
// 								'marginTop': - that.scrollTopHolder,
// 								'height': win.height() + that.scrollTopHolder,
// 								'overflow': 'hidden'
// 							});
// 							parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
// 								if(parent.outerHeight(true) > win.height()){
// 									parent.find('.tab-content.active').removeAttr('style').css({
// 										height: win.height() - parseInt(parent.find('.tab-content.active').css('padding-top')),
// 										'overflow-y': 'auto'
// 									});
// 								}
// 								centerPopup(parent, true, 0);
// 								isOpen = false;
// 								isTablet = true;
// 								win.scrollTop(0);
// 								parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
// 								if($.isFunction(that.options.afterChange)){
// 									that.options.afterChange.call(that, that.tabContent, $(this));
// 								}
// 								that.trigger('afterChange');
// 							});
// 						}
// 						else{ // does not support css3
// 							if(window.innerWidth >= that.options.mobile){
// 								parent.css('display', 'block');
// 								centerPopup(parent, true, that.scrollTopHolder);
// 								parent.css('opacity', 0);
// 								that.scrollTopHolder = win.scrollTop();
// 								centerPopup(parent, true, that.scrollTopHolder);
// 								parent.animate({
// 									opacity: 1
// 								}, that.options.duration);
// 								isOpen = false;
// 								tabContentOverlay = $(that.options.templateOverlay).appendTo(body).css({'opacity': 0.5, 'zIndex': that.options.zIndex}).fadeIn(that.options.duration);
// 							}
// 						}
// 						tabContentOverlay.off('click.closeTabContent').on('click.closeTabContent', function(e){
// 							e.preventDefault();
// 							that.find(that.options.triggerClosePopup).trigger('click.hideTabPopup');
// 						});
// 						win.off('resize.resetTabMenu').on('resize.resetTabMenu', function(){
// 							clearTimeout(tabMenuTimer);
// 							tabMenuTimer = setTimeout(function(){
// 								onResize(parent, self);
// 							}, 10);
// 						});
// 						return;
// 					}
// 				}
// 				// if($(this).hasClass(that.options.activeClass)){
// 				// 	return;
// 				// }

// 				changeTab($(this));

// 				if($.isFunction(that.options.afterChange)){
// 					that.options.afterChange.call(that, that.tabContent, $(this));
// 				}
// 				that.trigger('afterChange');
// 			});

// 			if(that.options.isPopup){
// 				var closeOnTablet = function(parent){
// 					// var langToolbarEl = $('.toolbar--language');
// 					tabContentOverlay.addClass('animated fadeOutOverlay');
// 					parent.removeClass('fadeIn').addClass('animated fadeOut');
// 					parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend').on('animationend webkitAnimationEnd MSAnimationEnd oanimationend', function() {
// 						$(that.options.container).removeAttr('style');
// 						// if(langToolbarEl.length) {
// 						// 	$(that.options.container).css('padding-top', langToolbarEl.height());
// 						// }
// 						win.scrollTop(that.scrollTopHolder);
// 						tabContentOverlay.off('click.closeTabContent').remove();
// 						parent.removeClass('animated fadeOut');
// 						that.scrollTopHolder = 0;
// 						isOpen = false;
// 						parent.removeAttr('style');
// 						parent.find('.tab-content.active').removeAttr('style');
// 						parent.off('animationend webkitAnimationEnd MSAnimationEnd oanimationend');
// 					});
// 				};
// 				that.find(that.options.triggerClosePopup).off('click.hideTabPopup').on('click.hideTabPopup', function(e){
// 					e.preventDefault();
// 					var parent = $(this).closest('.tab-wrapper');
// 					if(isOpen){
// 						return;
// 					}
// 					isOpen = true;
// 					if(window.innerWidth < that.options.tablet){
// 						if(window.Modernizr.csstransitions){ // support css3
// 							closeOnTablet(parent);
// 						}
// 						else{ // does not support css3
// 							parent.fadeOut(that.options.duration);
// 							tabContentOverlay.fadeOut(that.options.duration, function(){
// 								tabContentOverlay.remove();
// 								if(that.freezeBody){
// 									body.removeAttr('style');
// 									that.freezeBody = false;
// 								}
// 								win.scrollTop(that.scrollTopHolder);
// 								that.scrollTopHolder = 0;
// 								isOpen = false;
// 								parent.removeAttr('style');
// 							}).off('click.closeTabContent');
// 						}
// 						win.off('resize.resetTabMenu');
// 					}
// 					if($.isFunction(that.options.beforeClosePopup)){
// 						that.options.beforeClosePopup.call(that, that.tabContent, $(this));
// 					}
// 				});
// 			}

// 			that.on('keydown', function(e){
// 				var currActive = $(ally.get.activeElement());
// 				function moveTab(dir){
// 					var currTab = $(e.target).parent();
// 					var currTabInx;
// 					if(dir === 'next'){
// 				    	currTabInx = listTabs.index(currTab) + 1;
// 				    	if(currTabInx >= listTabs.length){
// 				    		currTabInx = 0;
// 				    	}
// 					}else if(dir === 'prev'){
// 				    	currTabInx = listTabs.index(currTab) - 1;
// 				    	if(currTabInx < 0 ){
// 				    		currTabInx = listTabs.length - 1;
// 				    	}
// 					}
// 					var currentActiveTab = listTabs.eq(currTabInx);
// 			        ally.element.focus(currentActiveTab.children()[0]);
// 			        currentActiveTab.children().attr('tabindex', 0);
// 			        currentActiveTab.siblings().children().attr('tabindex', -1);
// 			        currentActiveTab.attr({
// 			        	'aria-selected': true
// 			        });
// 			        currentActiveTab.siblings().attr({
// 			        	'aria-selected': false
// 			        });
// 			        changeTab(currentActiveTab);
// 			        currentActiveTab.trigger('click');

// 				}
// 				function moveContent(dir){
// 					var currContent = $(e.target);
// 					var listContent = that.find('.tab-content.active').find('[tabindex], button, a, input').not('.hidden');
// 					var currContentInx;

// 					if(dir === 'next'){
// 				    	currContentInx = listContent.index(currContent) + 1;
// 				    	if(currContentInx >= listContent.length){
// 				    		currContentInx = 0;
// 				    	}
// 					}else if(dir === 'prev'){
// 				    	currContentInx = listContent.index(currContent) - 1;
// 				    	if(currContentInx < 0 ){
// 				    		currContentInx = listContent.length - 1;
// 				    	}
// 					}
// 					var currentActiveContent = listContent.eq(currContentInx);
// 					ally.element.focus(currentActiveContent[0]);

// 				}
// 				switch(e.keyCode) {
// 				    case 39:
// 				    case 40:
// 				    	if(currActive.parents('.tab-content').length){
// 				    		moveContent('next');
// 				    	}else{
// 				    		moveTab('next');
// 				    	}
// 				        break;
// 				    case 37:
// 				    case 38:
// 				        if(currActive.parents('.tab-content').length){
// 				    		moveContent('prev');
// 				    	}else{
// 				    		moveTab('prev');
// 				    	}
// 				        break;

// 				    case 9:
// 				    	var fistTabindex = that.find('.tab-content.active').find('[tabindex]').eq(0);

// 				    	if(currActive.parents('.tab-content').length){
// 				    		if(currActive.is(fistTabindex) && e.shiftKey){
// 				    			ally.element.focus(that.find('.tab-item.active').children()[0]);
// 				    			return false;
// 				    		}
// 				    		return true;
// 				    	}else{
// 				    		if(e.shiftKey){
// 				    			return true;
// 				    		}
// 				    	}

// 				    	ally.element.focus(fistTabindex[0]);
// 				    	return false;

// 					case 36:
// 					  	if(currActive.parents('.tab-content').length){
// 					  		return true;
// 					  	}
// 					  	var targetEle = listTabs.eq(0).children();
// 					  	ally.element.focus(targetEle[0]);

// 					  	changeTab(targetEle);
// 			        	targetEle.trigger('click');
// 					  	return false;

// 					case 35:
// 					  	if(currActive.parents('.tab-content').length){
// 					  		return true;
// 					  	}
// 					  	var targetEle = listTabs.eq(listTabs.length - 1).children();
// 					  	ally.element.focus(targetEle[0]);

// 					  	changeTab(targetEle);
// 			        	targetEle.trigger('click');
// 					  	return false;
// 				}
// 			});
// 		},
// 		update: function(){
// 			var plugin = this,
// 				that = plugin.element;
// 			that.options = plugin.options;
// 			that.tab = that.find(that.options.tab);
// 			that.tabContent = that.find(that.options.tabContent);
// 			that.index = that.tab.filter('.' + that.options.activeClass).length ? that.tab.index(that.tab.filter('.' + that.options.activeClass)) : 0;
// 			that.freezeBody = false;
// 		},
// 		option: function(options) {
// 			this.element.options = $.extend({}, this.options, options);
// 		}
// 	};

// 	$.fn[pluginName] = function(options, params) {
// 		return this.each(function() {
// 			var instance = $.data(this, pluginName);
// 			if (!instance) {
// 				$.data(this, pluginName, new Plugin(this, options));
// 			} else if (instance[options]) {
// 				instance[options](params);
// 			} else {
// 				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
// 			}
// 		});
// 	};

// 	$.fn[pluginName].defaults = {
// 		tab: '',
// 		tabContent: '',
// 		activeClass: 'active',
// 		animation: false,
// 		duration: 500,
// 		isPopup: false,
// 		tablet: 988,
// 		container: '#container',
// 		mobile: 768,
// 		zIndex: 14,
// 		templateOverlay: '<div class="overlay"></div>',
// 		triggerClosePopup: '.tab-wrapper .popup__close',
// 		selectOnMobile: '> select',
// 		docScroll: window.Modernizr.touch ? 0 : 20,
// 		beforeClosePopup: function(){},
// 		afterChange: function() {},
// 		beforeChange: function(){}
// 	};

// }(jQuery, window));

/**
 *  @name tooltip
 *  @description description
 *  @version 1.0
 *  @options
 *    option
 *  @events
 *    event
 *  @methods
 *    init
 *    publicMethod
 *    destroy
 */
(function($, window, undefined) {
	var pluginName = 'kTooltip';
	var win = $(window);
	var tooltipOnDevice = null;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, options);
		this.init();
	}

	Plugin.prototype = {
		init: function() {
			var plugin = this,
			that = plugin.element;
			that.options = plugin.options;
			that.isShow = false;
			that.timer = 0;
			that.tooltip = $();
			that.maxWidth = plugin.options.maxWidth;
			that.winW = win.width();
			if(true){
				that.bind('click.showTooltip', function(e){
					e.preventDefault();
					if(that.hasClass('disabled') && !that.data('open-tooltip')){
						return;
					}
					that.winW = win.width();
					e.stopPropagation();
					if(tooltipOnDevice){
						tooltipOnDevice.tooltip.remove();
						tooltipOnDevice.tooltip = $();
						tooltipOnDevice.isShow = false;
						win.off('resize.hideTooltip');
						$(document).off('click.hideTooltip');
						$(document).off('keyup.escapeTooltip');
						$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
						tooltipOnDevice = null;
					}
					if(that.isShow){
						return;
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
			}
			else{
				that.off('click.showTooltip').on('click.showTooltip', function(e){
					e.preventDefault();
					e.stopPropagation();
					if(that.isShow){
						return;
					}
					if(that.timer){
						clearTimeout(that.timer);
					}
					plugin.generateTooltip();
					that.isShow = true;
				});
				that.off('mouseleave.hideTooltip').on('mouseleave.hideTooltip', function(){
					that.timer = setTimeout(function(){
							that.tooltip.remove();
							that.tooltip = $();
							that.isShow = false;
							win.off('resize.hideTooltip');
						}, 200);
				});
				// that.hover(
				// 	function(){
				// 		if(that.timer){
				// 			clearTimeout(that.timer);
				// 		}
				// 		plugin.generateTooltip();
				// 	},
				// 	function(){
				// 		that.timer = setTimeout(function(){
				// 			that.tooltip.remove();
				// 			that.tooltip = $();
				// 		}, 200);
				// 	}
				// );
			}
		},
		generateTooltip: function(){
			var plugin = this,
			that = plugin.element;
			tooltipOnDevice = that;
			if(that.tooltip.length){
				return;
			}
			// if(that.attr('data-type') === 2){
			// 	that.tooltip = $(that.options.template2).appendTo(document.body);
			// }
			// else if(that.attr('data-type') === 3){
			// 	that.tooltip = $(that.options.template3).appendTo(document.body);
			// }
			// else{
			// 	that.tooltip = $(that.options.template1).appendTo(document.body);
			// 	that.tooltip.find('a.popup_close').unbind('click.close').bind('click.close', function(e){
			// 		e.preventDefault();
			// 		e.stopPropagation();
			// 		that.tooltip.remove();
			// 		that.tooltip = $();
			// 	});
			// }
			if(that.data('type') === 2){
				that.tooltip = $(that.options.template2).appendTo(document.body);
				that.tooltip.find('a.popup__close').off('click.close').on('click.close', function(e){
					e.preventDefault();
					e.stopPropagation();
					that.tooltip.remove();
					that.tooltip = $();
					that.isShow = false;
					win.off('resize.hideTooltip');
					$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
					$(document).off('keyup.escapeTooltip');
				});
			}
			else if(that.data('type') === 3){
				that.tooltip = $(that.options.template3).appendTo(document.body);
			}
			else{
				that.tooltip = $(that.options.template1).appendTo(document.body);
			}
			// that.tooltip = $(that.options.template1).appendTo(document.body);
			that.tooltip.show().find('.tooltip__content').html(that.attr('data-content'));
			that.tooltip.off('click.preventDefault').on('click.preventDefault', function(e){
				e.stopPropagation();
			}).find('a.tooltip__close').unbind('click.close').bind('click.close', function(e){
				e.preventDefault();
				/*e.stopPropagation();
				that.tooltip.remove();
				that.tooltip = $();*/
				plugin.closeTooltip();
			});

			var actualWidth = that.tooltip.outerWidth(true);
			if(that.data('max-width')){
				that.maxWidth = that.data('max-width');
			}
			if(actualWidth > that.maxWidth){
				actualWidth = that.maxWidth;
			}
			that.tooltip.innerWidth(that.maxWidth);
			that.tooltip.css({
				'max-width': that.tooltip.width()
			}).css('width', '');

			var left = Math.max(0, that.offset().left - that.tooltip.outerWidth(true)/2 + that.outerWidth(true)/2 + 5);

			var top = (that.data('type') === 3) ? that.offset().top  + that.innerHeight() : that.offset().top - that.tooltip.outerHeight() - 10;

			if((left + that.tooltip.outerWidth()) > $(window).width()){
				left = $(window).width() - that.tooltip.outerWidth();
			}

			that.tooltip.css({
				position: 'absolute',
				'z-index': 999999,
				// top: that.offset().top - that.tooltip.outerHeight() - that.height()/2,
				top: top,
				left: left
			});
			that.tooltip.find('.tooltip__arrow').css({
				left: that.offset().left - left + that.width()/2
			});

			$(document).off('click.hideTooltip').on('click.hideTooltip', function(){
				plugin.closeTooltip();
			});
			win.off('resize.hideTooltip').on('resize.hideTooltip', function(){
				if(that.winW !== win.width()){
					plugin.closeTooltip();
				}
			});
			$(document).off('keyup.escapeTooltip').on('keyup.escapeTooltip', function(e){
				if (e.which === 27) {
					plugin.closeTooltip();
				}
			});
			$(document).off('scroll.hideTooltip mousewheel.hideTooltip').on('scroll.hideTooltip mousewheel.hideTooltip', function(){
				plugin.closeTooltip();
			});

			if($.isFunction(that.options.afterShow)){
				that.options.afterShow.call(that, that.tooltip);
			}
		},
		closeTooltip: function(){
			var plugin = this,
				that = plugin.element;
			if(that.tooltip.length){
				that.isShow = false;
				that.tooltip.remove();
				that.tooltip = $();
				$(document).unbind('click.hideTooltip');
				$(document).unbind('keyup.escapeTooltip');
				$(document).off('scroll.hideTooltip mousewheel.hideTooltip');
				tooltipOnDevice = null;
			}
			win.off('resize.hideTooltip');

			if($.isFunction(that.options.afterClose)){
				that.options.afterClose.call(that, that.tooltip);
			}
		}
	};

	$.fn[pluginName] = function(options, params) {
		return this.each(function() {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			} else {
				console.warn(options ? options + ' method is not exists in ' + pluginName : pluginName + ' plugin has been initialized');
			}
		});
	};

	$.fn[pluginName].defaults = {
		afterShow: function(){},
		afterClose: function(){},
		maxWidth: 240,
		template1: '<div class="question-tooltips"><span class="tooltip-cont"></span><em class="ico-question-tooltips"></em></div>',
		template2: '<aside class="tooltip tooltip--info"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close">&#xe60d;</a><div class="tooltip__content"></div></aside>',
		template3: '<aside class="tooltip tooltip--conditions-1"><em class="tooltip__arrow type-top"></em><a href="#" class="tooltip__close">&#xe60d;</a><div class="tooltip__content"></div></aside>'
	};

}(jQuery, window));
