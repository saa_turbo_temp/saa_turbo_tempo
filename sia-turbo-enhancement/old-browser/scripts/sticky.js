/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.sticky = function() {
	var global = SIA.global;
	var body = $('body');
	var sticky = null;
	var toggleBtn = null;
	var minibarTpl = '<em class="arrow-up"></em>BETA';
	var nonMinibarTpl = '<em class="arrow-right"></em>BETA.Singaporeair.com';

	var appendTemplate = function() {
		$.get(global.config.url.stickyTemplate, function(data) {
			var template = window._.template(data, {
				data: {}
			});
			var templateEl = $(template);
			body.append(templateEl);
			sticky = $('.sticky');
			toggleBtn = sticky.find('.mini-beta-link');
			sticky.addClass('minibar');
			toggleBtn.html(minibarTpl);
			sticky
				.off('mouseenter.additionBeta')
				.on('mouseenter.additionBeta', function() {
					if (!isOpen() && !sticky.data('prevent-mouseevent')) {
						showSticky();
					}
				})
				.off('mouseleave.additionBeta')
				.on('mouseleave.additionBeta', function() {
					if (isOpen() && !sticky.data('prevent-mouseevent')) {
						hideSticky();
					}
				});

			toggleBtn.off('click.additionBeta').on('click.additionBeta', function(e){
				e.preventDefault();
			});
		});
	};

	var showSticky = function() {
		sticky.removeClass('minibar');
		toggleBtn.html(nonMinibarTpl);
	};

	var hideSticky = function() {
		sticky.addClass('minibar');
		toggleBtn.html(minibarTpl);
	};

	var isOpen = function() {
		if (!sticky.length) {
			return false;
		}
		return !sticky.hasClass('minibar');
	};

	var initModule = function() {
		appendTemplate();
	};

	initModule();
};
