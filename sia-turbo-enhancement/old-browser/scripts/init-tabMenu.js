/**
 * @name SIA
 * @description Define global initTabMenu functions
 * @version 1.0
 */
SIA.initTabMenu = function(){
	// init tab plugin
	$('.tabs--1').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active'
	});

	$('.tabs--2').tabMenu({
		tab: '> ul.tab .tab-item',
		tabContent: '> div.tab-wrapper > div.tab-content',
		activeClass: 'active'
	});

	$('.tabs--4').tabMenu({
		tab: '> ul.main--tabs .tab-item',
		tabContent: '> div.tab-wrapper-1 > div.tab-content-1',
		activeClass: 'active'
	});
};
