/**
 * @name SIA
 * @description Define global setEmptyAltForImage functions
 * @version 1.0
 */
SIA.setEmptyAltForImage = function(){
	$('img').filter(function(){
		return $(this).closest('a').length;
	}).attr('alt', '');
};
