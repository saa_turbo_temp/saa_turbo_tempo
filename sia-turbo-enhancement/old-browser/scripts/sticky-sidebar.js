/**
 * @name SIA
 * @description Define global stickySidebar functions
 * @version 1.0
 */
SIA.stickySidebar = function(){
	/*var dropdownsOnMobile = function(wrapper, ch, sl){
		var chs = wrapper.find(ch);
		var sls = wrapper.find(sl);
		sls.off('change.direct').on('change.direct', function(){
			window.location.href = chs.eq(sls.prop('selectedIndex')).attr('href');
		});
	};*/
	var bookingNav = $('[data-fixed].sidebar');
	var innerBookingNav = bookingNav.children('.inner');
	var navBookingNav = bookingNav.find('.booking-nav');
	var listTabs = navBookingNav.find('.booking-nav__item');
	if(bookingNav.length){
		// reset scroll Top
		SIA.global.vars.win.scrollTop(0);

		var wrapperBookingNav = bookingNav.parent();
		var maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;
		var offsetBookingNavInWrapper = parseFloat(bookingNav.css('top'));
		var bookingNavOffsetTop = wrapperBookingNav.offset().top;

		SIA.global.vars.win.off('scroll.sticky mousewheel.sticky').on('scroll.sticky mousewheel.sticky', function(){
			var topForIE7 = 0;
			if(SIA.global.vars.html.hasClass('ie7') && $('.seatsmap-page').length){
				topForIE7 = 40;
			}
			maxScroll = wrapperBookingNav.height() - bookingNav.find('.inner').height() - 180;

			bookingNavOffsetTop = wrapperBookingNav.offset().top + topForIE7;
			if((bookingNavOffsetTop +offsetBookingNavInWrapper) <= SIA.global.vars.win.scrollTop()){
				bookingNav.css({
					top: Math.min(maxScroll, SIA.global.vars.win.scrollTop() - bookingNavOffsetTop)
				});
			}
			else{
				bookingNav.css('top', '');
			}
		});
		/*if(SIA.global.vars.win.width() <= SIA.global.config.tablet){
			// seat map and passenger detail
			dropdownsOnMobile(bookingNav, 'a', 'select');
		}*/

		innerBookingNav.attr({role: 'application'});
		navBookingNav.attr({role: 'tablist'});
		listTabs.attr({role: 'tab'});
		bookingNav.on('keydown', function(e){
				var currActive = $(ally.get.activeElement());
				function moveTab(dir){
					var currTab = $(e.target);
					var currTabInx;
					if(dir === 'next'){
				    	currTabInx = listTabs.index(currTab) + 1;
				    	if(currTabInx >= listTabs.length){
				    		currTabInx = 0;
				    	}
					}else if(dir === 'prev'){
				    	currTabInx = listTabs.index(currTab) - 1;
				    	if(currTabInx < 0 ){
				    		currTabInx = listTabs.length - 1;
				    	}
					}
					var currentActiveTab = listTabs.eq(currTabInx);
			        ally.element.focus(currentActiveTab);
			        currentActiveTab.attr('tabindex', 0);
			        currentActiveTab.siblings().attr('tabindex', -1);
			        currentActiveTab.trigger('click');

				}
				function moveContent(dir){
					var currContent = $(e.target);
					var listContent = navBookingNav.find('.booking-nav__item.active').find('[tabindex], button, a, input').not('.hidden');
					var currContentInx;

					if(dir === 'next'){
				    	currContentInx = listContent.index(currContent) + 1;
				    	if(currContentInx >= listContent.length){
				    		currContentInx = 0;
				    	}
					}else if(dir === 'prev'){
				    	currContentInx = listContent.index(currContent) - 1;
				    	if(currContentInx < 0 ){
				    		currContentInx = listContent.length - 1;
				    	}
					}
					var currentActiveContent = listContent.eq(currContentInx);
					ally.element.focus(currentActiveContent[0]);

				}
				switch(e.keyCode) {
			    case 39:
			    case 40:
			    		moveTab('next');
			        break;
			    case 37:
			    case 38:
			    		moveTab('prev');
			        break;

			    case 9:
			    	var fistTabindex = bookingNav.parents('.wrap-select-meals').find('.form--select-meals').find('[tabindex]').eq(0);

			    	if(currActive.parents('.form--select-meals').length){
			    		if(currActive.is(fistTabindex) && e.shiftKey){
			    			ally.element.focus(navBookingNav.find('.booking-nav__item.active')[0]);
			    			return false;
			    		}
			    		return true;
			    	}else{
			    		if(e.shiftKey){
			    			return true;
			    		}
			    	}
		    		ally.element.focus(fistTabindex[0]);
			    	return false;

					case 36:
					  	if(currActive.parents('.form--select-meals').length){
					  		return true;
					  	}
					  	var targetEle = listTabs.eq(0);
					  	ally.element.focus(targetEle[0]);
			        targetEle.trigger('click');
					  	return false;

					case 35:
					  	if(currActive.parents('.form--select-meals').length){
					  		return true;
					  	}
					  	var targetEle = listTabs.eq(listTabs.length - 1);
					  	ally.element.focus(targetEle[0]);
		        	targetEle.trigger('click');
					  	return false;
					}
			});
	}
};
