/**
 * @name SIA
 * @description Define global initAutoFilledPassenger functions
 * @version 1.0
 */
SIA.initAutoFilledPassenger = function(){
	var global = SIA.global,
			config = global.config;

	var hiddenKfNumber = $('[data-field-kfnumber]'),
			nomineeListEl = $('[data-nominee-list]'),
			checkboxPassenger = $('[data-under-18-years-old] input'),
			dateWrapEl = $('[data-field-dob] [data-day]'),
			dateDOBEl = dateWrapEl.data('autocomplete') ? dateWrapEl.find('input') : dateWrapEl.find('select');

	if(dateDOBEl.length) {
		var ruleCheckofchild = dateDOBEl.data('altRuleCheckofchild'),
				msgCheckofchild = dateDOBEl.data('altMsgCheckofchild');
		dateDOBEl.data({
			'oldRuleCheckofadult': dateDOBEl.data('ruleCheckofadult'),
			'oldMsgCheckofchild': dateDOBEl.data('msgCheckofadult')
		});
	}

	var setFieldValue = function(element, value) {
		var autocompleteEl = element.closest('.autocomplete');
		if(autocompleteEl.data('autocomplete')) {
			var selectEl = autocompleteEl.find('select');
			selectEl.val(value);
			autocompleteEl.removeClass('default').find('[autocomplete]').data('uiAutocomplete')._value(
				selectEl.find(':selected').data('text')
			);
			autocompleteEl.find('input').valid();
		} else {
			element.val(value);
			if(element.is('select')) {
				element.parent().customSelect('refresh');
			}
		}
	};

	var setFieldValue = function(element, value) {
		var autocompleteEl = element.closest('.autocomplete');
		if(autocompleteEl.data('autocomplete')) {
			var selectEl = autocompleteEl.find('select');
			selectEl.val(value);
			autocompleteEl.removeClass('default').find('[autocomplete]').data('uiAutocomplete')._value(
				selectEl.find(':selected').data('text')
			);
			autocompleteEl.find('input').valid();
		} else {
			element.val(value);
			if(element.is('select')) {
				element.parent().customSelect('refresh');
			}
		}
	};

	var filledData = function(data) {
		var monthString = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		$.each(data, function(key, value) {
			if(!/^(nomineeList|offpList)$/.test(key)) {
				var el = $('[data-field-' + key + ']');
				if(el.length) {
					if(/^(dob|passportExpiryDate)$/.test(key)) {
						var datetime = new Date(value),
								date = ('0' + datetime.getDate()).slice(-2),
								month = monthString[datetime.getMonth()],
								year = datetime.getFullYear();

						if(el.find('[data-autocomplete]').length) {
							setFieldValue($('[data-day]', el), parseInt(date));
							setFieldValue($('[data-month]', el), month);
							setFieldValue($('[data-year]', el), parseInt(year));
						} else {
							$('[data-day] select', el).val(date).parent().customSelect('refresh');
							$('[data-month] select', el).val(month).parent().customSelect('refresh');
							$('[data-year] select', el).val(year).parent().customSelect('refresh');
						}
					}
					else if(key === 'gender') {
						if(value === 'M') {
							el.filter(function() { return this.value === 'male'; }).trigger('click');
						} else {
							el.filter(function() { return this.value === 'female'; }).trigger('click');
						}
					}
					else if(key === 'phoneNumber') {
						if(value.countryCode) {
							setFieldValue(el.find('[data-field-phoneNumber-countryCode]'), value.countryCode);
						}
						if(value.phoneNumber) {
							setFieldValue(el.find('[data-field-phoneNumber-phoneNumber]'), value.phoneNumber);
						}
					}
					else {
						if(key === 'title' && value) {
							addSelectTitle(el, value);
						}
						setFieldValue(el, value);
						if(key === 'firstName' && el.is(':disabled')) {
							enableCheckbox(el);
						}
					}
				}
			}
		});
	};

	var addSelectTitle = function(el, val) {
		var optionsEl = el.find('option');
		if(optionsEl.filter(function() { return this.value === val; }).length) {
			return;
		}

		if(!$.grep(globalJson.titles, function(value) { return value === val; }).length) {
			if(!$.grep(globalJson.otherTitles, function(value) { return value === val; }).length) {
				globalJson.otherTitles.push(val);
			}

			if(optionsEl.filter(function() { return this.value === L10n.travellingPassenger.other; }).length) {
				el.val(L10n.travellingPassenger.other).trigger('change.title');
			} else {
				var parentEl = el.parent();
				el.append('<option value="' + val + '">' + val + '</option>');
				if(parentEl.is('[data-customselect]')) {
					parentEl.customSelect('_createTemplate');
				}
			}
		}
	};

	var enableCheckbox = function(el) {
		var checkboxEl = $(el.data('ruleRequired').split(':')[0]);
		if(checkboxEl.length) {
			checkboxEl.prop('checked', false).trigger('change.disabledInputSibling');
		}
	};

	var changeValidateRule = function(data) {
		if(data.paxType) {
			if(ruleCheckofchild) {
				if(data.paxType.toLowerCase() === L10n.travellingPassenger.child.toLowerCase()) {
					dateDOBEl
					.removeAttr('data-rule-checkofadult')
					.removeAttr('data-msg-checkofadult')
					.removeData('ruleCheckofadult msgCheckofadult')
					.data({
						'ruleCheckofchild': ruleCheckofchild,
						'msgCheckofchild': msgCheckofchild
					});
				} else {
					dateDOBEl
					.removeData('ruleCheckofchild msgCheckofchild')
					.data({
						'ruleCheckofadult': dateDOBEl.data('oldRuleCheckofadult'),
						'msgCheckofadult': dateDOBEl.data('oldMsgCheckofadult')
					});
				}
				dateDOBEl.trigger('change.validate');
			}
		}
	};

	var detectCheckboxPassenger = function(data) {
		if(data.paxType) {
			if(data.paxType.toLowerCase() === L10n.travellingPassenger.child.toLowerCase()) {
				checkboxPassenger.prop('checked', true);
			} else {
				checkboxPassenger.prop('checked', false);
			}
		}
	};

	var removeValidateRule = function(el, rules) {
		var ruleLength = rules.length;
		if(el.length && ruleLength) {
			for(var i = 0; i < ruleLength; i++) {
				el.removeAttr('data-rule-' + rules[i]).removeData('rule-' + rules[i]);
			}
		}
	};

	var generateOption = function(data, isSelected) {
		var options = '';
		isSelected = isSelected || false;
		for(var i = 0, length = data.length; i < length; i++) {
			options += '<option ' + ((isSelected && i === 0) ? 'selected="selected"' : '') + ' value="' + i + '">' + data[i].firstName + ' ' + data[i].lastName + ' (' + data[i].paxType + ')' + '</option>';
		}
		if(global.vars.body.hasClass('orb-passenger-details-page')) {
			options += '<option value="addnew">' + L10n.travellingPassenger.addRedemtionNominee + '</option>';
		}
		return options;
	};

	var resetFieldData = function(el) {
		var parentEl = el.closest('.autocomplete');
		if(parentEl.data('autocomplete')) {
			parentEl.find('[autocomplete]').data('uiAutocomplete')._value(
				el.find('[selected]').length ? el.find(':selected').data('text') : ''
			);
		} else {
			el.closest('.custom-select').customSelect('refresh');
		}
	};

	var checkSelectedNominee = function(nomineeList) {
		var navItemNonSelected = $('.booking-nav').find('.booking-nav__item.active').prevAll(),
				list = nomineeList,
				arrText = [];

		if(navItemNonSelected.length) {
			navItemNonSelected.each(function() {
				var that = $(this);
				arrText.push(that.find('.passenger-info__text').text().toLowerCase());
			});

			return $.grep(list, function(obj) {
				var name = obj.firstName + ' ' + obj.lastName;
				return $.inArray(name.toLowerCase(), arrText) === -1;
			});
		}
		return list;
	};

	var ajaxSuccess = function(res) {
		if(res) {
			if(nomineeListEl.length) {
				if(res.nomineeList.length) {
					var programmeEl = $('[data-field-programcode]'),
							membershipNumberEl = $('[data-field-number]'),
							nomineeListParentEl = nomineeListEl.parent(),
							listNominee = checkSelectedNominee(res.nomineeList),
							dobEl = dateWrapEl.data('autocomplete') ? dateWrapEl.find('input') : dateWrapEl.find('select');

					nomineeListEl.html(generateOption(listNominee));
					if(nomineeListEl.parent().is('[data-customselect]')) {
						nomineeListEl.parent().customSelect('_createTemplate');
						nomineeListParentEl.find(nomineeListParentEl.data('customSelect').options.customText).text(L10n.travellingPassenger.choose);
					}

					nomineeListEl.off('change.travellingPassenger').on('change.travellingPassenger', function() {
						var selectedVal = this.value,
								passengerForm = nomineeListEl.closest('form');

						if(selectedVal !== 'addnew') {
							if(res.nomineeList[selectedVal]) {
								filledData(res.nomineeList[selectedVal]);
								changeValidateRule(res.nomineeList[selectedVal]);
								detectCheckboxPassenger(res.nomineeList[selectedVal]);
							}
						} else {
							passengerForm.get(0).reset();
							nomineeListEl.val('addnew');
							passengerForm.find('select').each(function() {
								resetFieldData($(this));
							});
							passengerForm.find(':input').trigger('change.disabledInputSibling');
							removeValidateRule(dobEl, ['checkofchild', 'checkofadult']);
						}

						if(passengerForm.data('validatedOnce')) {
							var currentPos = global.vars.win.scrollTop();
							passengerForm.valid();
							global.vars.win.scrollTop(currentPos + 1);
						}
					});

					programmeEl.off('change.programCode').on('change.programCode', function() {
						var selectedVal = this.value;
						var programObj = res.offpList.filter(function(index) {
							return index.programCode === selectedVal;
						});
						if(programObj.length) {
							membershipNumberEl.val(programObj[0].number).trigger('blur');
						}
					});
				}
			}

			if(globalJson.loggedUser) {
				filledData(res);
			}
		}
	};

	var ajaxFail = function(jqXHR, textStatus) {
		console.log(textStatus);
	};

	var initCallAjax = function(url, type) {
		var dataAjax = hiddenKfNumber.serialize();
		type = type || 'json';

		$.ajax({
			url: url,
			type: global.config.ajaxMethod,
			dataType: type,
			data: dataAjax,
			success: ajaxSuccess,
			error: ajaxFail
		});
	};

	if (!Array.prototype.filter) {
		Array.prototype.filter = function(fun /*, thisp*/) {
			var len = this.length;
			if (typeof fun !== 'function') {
				throw new TypeError();
			}

			var res = [];
			var thisp = arguments[1];
			for (var i = 0; i < len; i++) {
				if (i in this) {
					var val = this[i];
					if (fun.call(thisp, val, i, this)) {
						res.push(val);
					}
				}
			}
			return res;
		};
	}

	if(globalJson.loggedUser) {
		initCallAjax(config.url.kfNumberNomineeJSON);
	}
};
