SIA.filterEntertainment = function() {
	var count = 0;
	var container = $('[data-entertaiment]');
	var items = container.find('.ent-item');
	var filterSelect = container.find('[data-entertaiment-filter]');
	var btnSeemore = container.find('.see-more-btn');
	var additionalItemsDisplay = container.data('item') * 1;
	var minItemsDisplay = (container.data('item') * 1) * (container.data('set') * 1);
	var filteredItems = null;

	var filterByCategory = function() {
		filterSelect.off('change.staticContentMusic').on('change.staticContentMusic', function() {
			items.addClass('hidden');
			filteredItems = getFilteredItem();
			var len = (filteredItems.length > minItemsDisplay) ? minItemsDisplay : filteredItems.length;
			for (var i = 0; i < len; i++) {
				filteredItems.eq(i).removeClass('hidden');
			}
			toggleBtnSeeMore();
			count = 0;
		});
	};

	var getFilteredItem = function(){
		var val = filterSelect.find('option').filter(':selected').attr('value');
		if (val === '') {
			return items;
		}
		return items.filter(function() {
			return $(this).data('category') === val;
		});
	};

	var toggleBtnSeeMore = function(){
		var len = filteredItems.length;
		var visibleElLen = filteredItems.not('.hidden').length;
		btnSeemore[(len === visibleElLen) ? 'addClass' : 'removeClass']('hidden');
	};

	var handleSeeMore = function() {
		btnSeemore.off('click.staticContentMusic').on('click.staticContentMusic', function(e) {
			e.preventDefault();
			filteredItems = getFilteredItem();
			var len = (filteredItems.length > minItemsDisplay) ? minItemsDisplay : filteredItems.length;
			count++;
			len = len + (count * additionalItemsDisplay);
			for (var i = 0; i < len; i++) {
				if (filteredItems.eq(i).length) {
					filteredItems.eq(i).removeClass('hidden');
				}
			}
			toggleBtnSeeMore();
		});
	};

	var initModule = function() {
		filterByCategory();
		handleSeeMore();
	};

	initModule();
};
