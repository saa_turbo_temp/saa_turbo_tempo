/**
 * @name SIA
 * @description Define global bookingWidget functions
 * @version 1.0
 */
SIA.bookingWidget = function(){
	var global = SIA.global;
	var bookingWidgetComponent = $('[data-booking-widget]');
	var bookingWidgetForm = bookingWidgetComponent.find('form');
	var bookingWidget = bookingWidgetComponent.find('.booking-widget.sticky');
	var bookingWidgetBlock = bookingWidget.find('.booking-widget-block');
	var stickyOpen = bookingWidgetComponent.find('[data-sticky-open]');
	var stickyReopen = bookingWidgetComponent.find('[data-sticky-reopen]');
	var stickyCloseEl = bookingWidget.find('.sticky__close');
	var isExpand = false;
	var isExpandAll = false;

	stickyOpen.off('click.openStciky').on('click.openStciky', function(){
		if(!isExpand){
			stickyOpen.addClass('hidden');
			stickyCloseEl.removeClass('hidden');
			bookingWidgetBlock.removeClass('hidden');
			isExpand = true;
		}
	});

	stickyReopen.off('click.reopen').on('click.reopen', function(e){
		e.preventDefault();
		if(!isExpandAll){
			bookingWidget.removeClass('hidden');
			isExpandAll = true;
			isExpand = true;
		}
		else{
			stickyOpen.trigger('click.openStciky');
		}
	});

	stickyCloseEl.off('click.collapseSticky').on('click.collapseSticky', function(){
		if(isExpand){
			bookingWidgetBlock.addClass('hidden');
			stickyCloseEl.addClass('hidden');
			stickyOpen.removeClass('hidden');
			isExpand = false;
		}
	});

	bookingWidgetForm.validate({
		focusInvalid: true,
		errorPlacement: global.vars.validateErrorPlacement,
		success: global.vars.validateSuccess
	});
	var wcagRadio = function(){

		var customRadio = bookingWidgetForm.find('.custom-radio'),
				focusPos = 0;
		customRadio.each(function(){

			var child = $(this);

			child.attr({
        'role': 'radio',
        'tabindex': -1,
        'aria-checked':false,
        'aria-label': child.find('label').text()
      });

			child.find('input').attr({
				'tabindex': -1
			});

			child.find('input').next().attr({
        'aria-hidden':true,
        'tabindex':-1
      });

      if(child.index() === 0){
        child.attr({
          'tabindex': 0,
          'aria-checked':true
        });
      }
		});

		customRadio.off('keyup').on('keyup', function(e){
        var keyCode = e.keyCode || e.which || e.charCode;

        if(keyCode === 39){
          if(focusPos !== customRadio.length-1){
            focusPos++;
            customRadio.eq(focusPos).focus();
          }else{
            customRadio.eq(0).focus();
            focusPos = 0;
          }
        }

        if(keyCode === 37){
          if(focusPos !== 0){
            focusPos--;
            customRadio.eq(focusPos).focus();
          }else{
            customRadio.eq(customRadio.length-1).focus();
            focusPos = customRadio.length-1;
          }
        }

      });

      customRadio.off('focus').on('focus', function(e){
        var selfFocus = $(this);
        selfFocus.find('input').trigger('click');
        selfFocus.attr({
          'aria-checked':true,
          'tabindex': 0
        });
        selfFocus.siblings('.custom-radio').attr({
          'aria-checked':false,
          'tabindex': -1
        });
      });
	};
	wcagRadio();
};
