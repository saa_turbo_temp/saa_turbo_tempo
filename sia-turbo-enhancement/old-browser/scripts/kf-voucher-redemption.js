/**
 * @name SIA
 * @description Define global kfVoucherRedemption functions
 * @version 1.0
 */
SIA.kfVoucherRedemption = function() {
	var global = SIA.global;

	var initVoucherRedemption = function() {
		var btnRedeem = $('[data-btn-redeem]'),
				popupVoucher = $(btnRedeem.data('triggerPopup')),
				popupReviewVoucher = $('.popup--review-voucher-redemption'),
				popupComplete = $('.popup--redeem-completed'),
				voucherJSON, reviewTpl;

		var ajaxSuccess = function(res) {
			if(res.status) {
				var voucherSelect = $('[data-field-voucher]'),
						partnerSelect = $('[data-field-partner]'),
						listVoucher = $('[data-list-voucher]'),
						reviewVoucherWrap = $('[data-review-voucher]'),
						wrapBtnProcess = $('[data-wrap-process]');

				var getListVoucher = function(jsonData, voucherSelected) {
					return jQuery.grep(jsonData, function(voucher){ return voucher.name === voucherSelected; });
				};

				var initDataVoucher = function(data, template) {
					var totalMilesTransaction = $('[data-total-miles-transaction]');

					var calcTotalMiles = function(el) {
						try {
							var val = el.val();
							return accounting.formatNumber(accounting.unformat(el.data('voucherMilesRequired')) * val);
						} catch(e) {
							if(window.console) { console.log(e); }
							return 0;
						}
					};

					var calcTotalMilesTransaction = function() {
						try {
							var total = 0;
							$('[data-total-miles]:not(:hidden)').each(function() {
								total += accounting.unformat($(this).text());
							});
							return accounting.formatNumber(total);
						} catch(e) {
							if(window.console) { console.log(e); }
							return 0;
						}
					};

					var generateReview = function(reviewData) {
						reviewVoucherWrap.empty();
						if(!reviewTpl) {
							$.get(global.config.url.kfVoucherRedemptionReviewTemplate, function(templateStr) {
								reviewTpl = templateStr;
								reviewVoucherWrap.append(window._.template(reviewTpl, {'data' : reviewData}));
							}, 'html');
						} else {
							reviewVoucherWrap.append(window._.template(reviewTpl, {'data' : reviewData}));
						}
					};

					var controlRowReview = function(index, qty, total, name) {
						$('[data-voucher-name]').text(name);
						$('[data-voucher-qty="' + index + '"]').text(qty);
						$('[data-voucher-total="' + index + '"]').text(total);
						$('[data-row-confirm], [data-block-confirm]').addClass('hidden');
						listVoucher.find('[data-partner]').not(':hidden').each(function(index) {
							var self = $(this),
									currentIndex = self.find('[data-field-quantity]').data('fieldQuantity');
							$('[data-voucher-qty="' + currentIndex + '"]').parents('[data-row-confirm]').removeAttr('class').addClass(index % 2 === 0 ? 'odd' : 'even');
							$('[data-voucher-total="' + currentIndex + '"]').parents('[data-block-confirm]').removeClass('hidden');
						});
					};

					var resetClassTableRow = function(index, el) {
						var self = $(el);
						if(!self.is(':hidden')) {
							if(index % 2 === 0) {
								self.addClass('odd');
							} else {
								self.addClass('even');
							}
						}
					};

					voucherSelect.parent().off('afterSelect.voucher').on('afterSelect.voucher', function() {
						var voucherVal = voucherSelect.val(),
								partnerData = getListVoucher(data, voucherVal);

						listVoucher.find('tbody').empty();
						if(partnerData.length && voucherVal) {
							listVoucher.removeClass('hidden').find('tbody').append(window._.template(template, {'data' : partnerData[0]})).find('[data-field-quantity]').each(function() {
								var self = $(this),
										rowEl = self.parents('tr'),
										totalMiles = rowEl.find('[data-total-miles]');

								initCustomSelect(self);
								self.parent().off('afterSelect.calcTotalMiles').on('afterSelect.calcTotalMiles', function() {
									var totalMilesVal = calcTotalMiles(self),
											currentIndex = self.data('fieldQuantity');
									totalMiles.text(totalMilesVal);
									totalMilesTransaction.text(calcTotalMilesTransaction());
									controlRowReview(currentIndex, self.val(), totalMilesVal, voucherVal);
								});
							});
							generateReview(partnerData[0]);
							initCustomSelect(partnerSelect, generateOption(partnerData[0].info, 'partner'));
							wrapBtnProcess.removeClass('hidden');
						} else {
							listVoucher.addClass('hidden');
							initCustomSelect(partnerSelect, generateOption([], 'partner'));
							wrapBtnProcess.addClass('hidden');
						}
						popupVoucher.Popup('reposition');
						totalMilesTransaction.text(0);
					});

					partnerSelect.parent().off('afterSelect.partner').on('afterSelect.partner', function() {
						var partnerVal = partnerSelect.val();
						if(partnerVal) {
							listVoucher.find('[data-partner]').addClass('hidden');
							listVoucher.find('[data-partner="' + partnerVal + '"]').removeAttr('class').each(resetClassTableRow).find('[data-field-quantity]').each(function() {
								$(this).parent().trigger('afterSelect.calcTotalMiles');
							});
						} else {
							listVoucher.find('[data-partner]').removeAttr('class').each(resetClassTableRow).find('[data-customselect]').trigger('afterSelect.calcTotalMiles');
						}
						popupVoucher.Popup('reposition');
					});
				};

				var generateOption = function(data, objTitle) {
					var options = '<option value="" selected="selected">' + L10n.kfVoucher.selectLabel + '</option>';
					for(var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i][objTitle] + '">' + data[i][objTitle] + '</option>';
					}
					return options;
				};

				var initCustomSelect = function(el, options) {
					var selectEl = el.parent();

					if(options) {
						el.html(options);
					}

					if(selectEl.is('[data-customselect]')) {
						selectEl.customSelect('_createTemplate');
						selectEl.customSelect('refresh');
					}
				};

				voucherJSON = res.voucherList;
				initCustomSelect(voucherSelect, generateOption(voucherJSON, 'name'));
				initCustomSelect(partnerSelect, generateOption([], 'partner'));
				popupVoucher.find('form').validate({
					focusInvalid: true,
					errorPlacement: global.vars.validateErrorPlacement,
					success: global.vars.validateSuccess,
					submitHandler: function() {
						popupVoucher.Popup('hide');
						popupReviewVoucher.Popup('show');
						return false;
					}
				});

				popupReviewVoucher.Popup({
					overlayBGTemplate: '<div class="overlay"></div>',
					modalShowClass: '',
					triggerCloseModal: '.popup__close, [data-close]'
				});

				popupComplete.data('Popup').options.beforeShow = function() {
					popupReviewVoucher.Popup('hide');
				};

				popupVoucher.data('Popup').options.beforeHide = function() {
					popupVoucher.find('[data-customselect]').customSelect('hide');
				};

				$.get(global.config.url.kfVoucherRedemptionTemplate, function(templateStr) {
					initDataVoucher(voucherJSON, templateStr);
				}, 'html');
			}
		};

		var ajaxFail = function(jqXHR, textStatus) {
			console.log(textStatus);
		};

		var initAjax = function(url, data, type, successFunc, errorFunc) {
			type = type || 'json';
			successFunc = successFunc || ajaxSuccess;
			errorFunc = errorFunc || ajaxFail;
			$.ajax({
				url: url,
				type: global.config.ajaxMethod,
				dataType: type,
				data: data,
				success: successFunc,
				error: errorFunc
			});
		};

		if(popupVoucher.length) {
			popupVoucher.data('Popup').options.beforeShow = function() {
				if(!voucherJSON) {
					initAjax(global.config.url.kfVoucherRedemptionJSON);
				}
			};
		}
	};

	var initModule = function() {
		initVoucherRedemption();
	};

	initModule();
};
