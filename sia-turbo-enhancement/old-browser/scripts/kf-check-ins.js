SIA.KFCheckIns = function() {
	var config = SIA.global.config;
	var loadPlaceholder = $('[data-load-placeholder]');

	var loadGlobalJson = function() {
		SIA.preloader.show();
		loadPlaceholder.children('[data-booking-item]').remove();
		$.get(config.url.kfCheckInTemplate, function(html) {
			html = html.replace(/td>\s+<td/g,'td><td');
			var template = window._.template(html, {
				data: globalJson.kfCheckIns
			});
			loadPlaceholder.find('.check-ins-content').append($(template));
			$('.booking-info').each(function(){
				SIA.global.vars.addClassOnIE($(this).find('.booking-info-item'), 'all');
			});
			SIA.preloader.hide();

			$('.checkin-alert').hide().removeClass('hidden').delay(1000).fadeIn(400);
		});
	};

	loadGlobalJson();
};