/**
 * @name SIA
 * @description Define function to add a bar on live site
 * @version 1.0
 */
SIA.plusOrMinusNumber = function() {
	var plusOrMunisModule = $('[data-plus-or-minus-number]');

	var changeValueInput = function(){
		plusOrMunisModule.each(function(){
			var self = $(this);
			var limitInputVL = self.data('plus-or-minus-number');
			var plusBtn = self.find('.btn-plus');
			var minusBtn = self.find('.btn-minus');
			var input = self.find('[data-updated-value]');

			input.off('change.value').on('change.value', function(){
				if(parseInt(input.val()) > limitInputVL){
					input.val(limitInputVL);
				}
			});

			plusBtn.off('click.plus').on('click.plus', function(){
				var inputVL = parseInt(input.val());
				var inputVLFinal = isNaN(inputVL) ? 0 : inputVL;
				if(inputVLFinal < limitInputVL){
					input.val(inputVLFinal + 1);
				}
			});

			minusBtn.off('click.minus').on('click.minus', function(){
				var inputVL = parseInt(input.val());
				if(inputVL > 0){
					input.val(inputVL - 1);
				}
			});
		});
	};

	var initModule = function() {
		changeValueInput();
	};

	initModule();
};
