/**
 * @name SIA
 * @description
 * @version 1.0
 */
SIA.DESCityGuide = function() {
	var global = SIA.global,
			win = global.vars.win,
			blockSlider = $('#where-to-stay-slider'),
			wrapperSlider = blockSlider.parent(),
			imgSliderLength = blockSlider.find('img').length - 1,
			totalDesktopSlide = 2,
			slideMarginRight = 20;

	var loadBackgroundSlider = function(self, parentSelf, idx) {
		if(idx === imgSliderLength) {
			blockSlider.width(wrapperSlider.width() + slideMarginRight);
			blockSlider.css('visibility', 'visible');
			blockSlider.find('.slides')
				.slick({
					siaCustomisations: true,
					dots: true,
					speed: 300,
					draggable: true,
					slidesToShow: totalDesktopSlide,
					slidesToScroll: totalDesktopSlide,
					accessibility: false
				});

			win.off('resize.blockSlider').on('resize.blockSlider',function() {
				blockSlider.width(wrapperSlider.width() + slideMarginRight);
			}).trigger('resize.blockSlider');
		}
	};

	blockSlider.find('img').each(function(idx) {
		var self = $(this),
				parentSelf = self.parent(),
				newImg = new Image();

		newImg.onload = function() {
			loadBackgroundSlider(self, parentSelf, idx);
		};
		newImg.src = self.attr('src');
	});
};
