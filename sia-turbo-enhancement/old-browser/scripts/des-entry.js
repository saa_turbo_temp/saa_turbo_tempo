/**
 * @name SIA
 * @description Define global desEntry functions
 * @version 1.0
 */
SIA.desEntry = function(){
	var btnSeeMore = $('.country-button [data-see-more]'),
		config = SIA.global.config,
		container = $('.static-block-2 .static-block--item'),
		regionBlock = $('[data-country]'),
		regionSelect = regionBlock.find('select'),
		regionInput = regionBlock.find('input:text'),
		cityBlock = $('[data-city]'),
		citySelect = cityBlock.find('select'),
		cityInput = cityBlock.find('input:text'),
		itemSltor = '.static-item',
		staticItems = $(),
		desEntryForm = $('.dest-city-form'),
		regionValue = '',
		cityValue = '',
		preventClick = false,
		res = {},
		startDeskNum = 0,
		defaultDeskNum = 15,
		seeMoreDeskNum = 5,
		seeMoreCount = 0,
		isShuffleActive = false;

	var randomize = function(container, selector) {
		var elems = selector ? container.find(selector) : container.children();
		var	parents = elems.parent();
		parents.each(function(){
			$(this).children(selector).sort(function(){
				return Math.round(Math.random()) - 0.5;
			}).detach().appendTo(this);
		});
		return container.find(selector);
	};

	var initShuffle = function() {
		var countryItem = container.find('.static-item.col-mb-3:not(.hidden)').eq(0);
		container.shuffle({
			speed: 0,
			itemSelector: '.static-item:not(.hidden)',
			sizer: countryItem
		});
	};

	var getRegionsAndCities = function() {
		var regions = [];
		var cities = [];
		staticItems.each(function() {
			var self = $(this);
			var region = self.data('region');
			var city = self.data('city');
			if (!regions.length || regions.indexOf(region) === -1) {
				regions.push(region);
			}
			cities.push({region: region, city: city});
		});
		return {
			regions: regions,
			cities: cities
		};
	};

	var initRegion = function(regions) {
		var i = 0;
		var regionHtml = '';
		for (i = 0; i < regions.length; i++) {
			regionHtml += '<option value="' + (i + 1) + '" data-text="' +
				regions[i] + '">' + regions[i] + '</option>';
		}
		regionSelect.empty().append(regionHtml);
		regionInput.val('');
		regionInput.autocomplete('destroy');
		regionSelect
			.closest('[data-autocomplete]')
			.removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		regionInput
			.on('autocompleteselect', function() {
				setTimeout(function() {
					regionValue = regionInput.val();
					initCity(res.cities, regionValue);
				}, 400);
			})
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!regionInput.val() && regionValue) {
						regionValue = regionInput.val();
						initCity(res.cities, regionValue);
					}
				}, 400);
			});
	};

	var initCity = function(cities, region) {
		var i = 0;
		var cityHtml = '';
		for (i = 0; i < cities.length; i++) {
			var city = cities[i].city;
			var reg = cities[i].region;
			if (!region || reg === region) {
				cityHtml += '<option value="' + (i + 1) + '" data-text="' +
				city + '">' + city + '</option>';
			}
		}
		citySelect.empty().append(cityHtml);
		cityInput.val('');
		cityInput.autocomplete('destroy');
		citySelect.closest('[data-autocomplete]').removeData('init-automcomplete');
		SIA.initAutocompleteCity();
		cityInput
			.off('autocompleteselect')
			.on('autocompleteselect', function() {
				setTimeout(function() {
					cityValue = cityInput.val();
					renderTemplate(regionInput.val(), cityValue);
				}, 400);
			})
			.off('autocompleteclose')
			.on('autocompleteclose', function() {
				setTimeout(function() {
					if (!cityInput.val() && cityValue) {
						cityValue = cityInput.val();
						renderTemplate(regionInput.val(), cityValue);
					}
				}, 400);
			});
	};

	var searchItems = function(region, city) {
		return container
			.find('.static-item')
			.removeClass('static-item--large col-mb-6')
			.addClass('hidden')
			.filter(function() {
				var self = $(this);
				return (!region || self.data('region') === region) &&
					(!city || self.data('city') === city);
			});
	};

	var renderTemplate = function(region, city) {
		staticItems = searchItems(region, city);
		generateClass(staticItems);
		startDeskNum = 0;
		fillContent();
	};

	var generateTemplate = function(tpl) {
		if (!isShuffleActive) {
			initShuffle();
			isShuffleActive = true;
		}
		else {
			container.shuffle('appended', tpl);
		}
		preventClick = false;
		changeSeemoreText();
	};

	var changeSeemoreText = function() {
		if (!staticItems.filter('.hidden').length) {
			btnSeeMore.addClass('hidden');
			btnSeeMore.text(L10n.kfSeemore.seeMore);
		}
		else {
			btnSeeMore.text(seeMoreCount === 2 ?
				L10n.kfSeemore.seeAll : L10n.kfSeemore.seeMore);
			btnSeeMore.removeClass('hidden');
		}
	};

	var getTemplate = function(res, endIdx) {
		var tpl = res.filter(':lt(' + endIdx + ')').filter('.hidden');
		if (tpl.length) {
			tpl.each(function() {
				var self = $(this);
				var img = self.find('img');
				// if (!img.data('loaded')) {
				// 	img.one('load.loadImg', function() {
				// 		self.removeClass('hidden');
				// 		img.data('loaded', true);
				// 		if (!tpl.filter('.hidden').length) {
				// 			generateTemplate(tpl);
				// 		}
				// 	}).attr('src', img.data('img-src'));
				// }
				if (!img.data('loaded')) {
					var newImg = new Image();

					newImg.onload = function() {
						self.removeClass('hidden');
						img.data('loaded', true);

						if (!tpl.filter('.hidden').length) {
							generateTemplate(tpl);
						}
						img.parent().css({
							'background-image': 'url(' + this.src + ')'
						});
					};

					newImg.src = img.data('img-src');
					img.attr('src', config.imgSrc.transparent);
				}
				else {
					self.removeClass('hidden');
					if (!tpl.filter('.hidden').length) {
						generateTemplate(tpl);
					}
				}
			});
		}
	};

	var fillContent = function() {
		if (staticItems.length) {
			var endIdx = 0;
			var resLen = staticItems.length;
			endIdx = !startDeskNum ?
				(seeMoreCount > 2 ? resLen : startDeskNum + defaultDeskNum) :
				(seeMoreCount > 2 ? resLen : startDeskNum + seeMoreDeskNum);
			startDeskNum = endIdx;
			getTemplate(staticItems, endIdx, resLen);
		}
	};

	var resetClass = function(elm, rClass, aClass) {
		elm
			.removeClass(rClass)
			.addClass(aClass);
	};

	var generateClass = function(res) {
		var i = -6, temp = 0;
		resetClass(res.removeAttr('style'), 'static-item--large col-mb-6', 'hidden col-mb-3');
		do {
			if (temp <= 2) {
				i += 6;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp++;
			}
			else {
				i += 3;
				resetClass(res.eq(i), 'col-mb-3', 'static-item--large col-mb-6');
				temp = 1;
			}
		}
		while (i < res.length);
	};

	desEntryForm.off('submit.template').on('submit.template', function(e) {
		e.preventDefault();
		if (!preventClick) {
			preventClick = true;
			seeMoreCount = 0;
			var region = regionInput.val();
			cityValue = cityInput.val();
			renderTemplate(region, cityValue);
		}
	});

	btnSeeMore.off('click.template').on('click.template', function(e) {
		e.preventDefault();
		if (!preventClick) {
			preventClick = true;
			seeMoreCount++;
			fillContent(false);
		}
	});

	var initModule = function(){
		SIA.initAutocompleteCity();
		staticItems = randomize(container, itemSltor);
		generateClass(staticItems);
		res = getRegionsAndCities();
		initRegion(res.regions);
		initCity(res.cities);
		fillContent(false);
	};

	initModule();
};
