/**
 * @name SIA
 * @description sqc-expenditure
 * @version 1.0
 */
SIA.SQCExpenditure = function() {
	var tableEl = $('[data-table-report]'),
			btnSeemoreEl = $('[data-see-more]'),
			linkSort = $('[data-sortable]');

	var sortItems = function(listItemEl, colIndex, sortType) {
		return listItemEl.sort(function(a, b) {
			var valA = $(a).find('td:nth-child(' + colIndex + ') [data-sort-value]').data('sortValue'),
					valB = $(b).find('td:nth-child(' + colIndex + ') [data-sort-value]').data('sortValue');
			if(sortType === 'DESC') {
				return valA < valB ? 1 : -1;
			}
			return valA >= valB ? 1 : -1;
		});
	};

	btnSeemoreEl.off('click.messageSeeMore').on('click.messageSeeMore', function(e) {
		e.preventDefault();
		if(!$(this).hasClass('hidden')) {
			tableEl.find('tr.hidden').removeClass('hidden');
			btnSeemoreEl.addClass('hidden');
		}
	});

	linkSort.off('click.sortable').on('click.sortable', function(e) {
		e.preventDefault();
		var el = $(this),
				bodyTable = tableEl.find('tbody'),
				listItemEl = bodyTable.find('tr').not(':hidden'),
				colIndex = el.closest('th').index() + 1,
				sortType = el.data('sortType') || 'DESC';

		if(listItemEl.length && colIndex !== -1) {
			listItemEl = sortItems(listItemEl.detach(), colIndex, sortType);
			bodyTable.prepend(listItemEl);
			if(sortType === 'DESC') {
				el.removeClass('active').data('sortType', 'ASC');
			} else {
				el.addClass('active').data('sortType', 'DESC');
			}
		}
	});
};
