/**
 * @name SIA
 * @description Define function to carousel slide
 * @version 1.0
 */
SIA.carouselSlide = function() {
	var global = SIA.global;
	var win = global.vars.win;
	var carouselSlide = $('[data-carousel-slide]');
	var paddingRight = 22;

	if(carouselSlide.length){
		carouselSlide.each(function(){
			var carousel = $(this);
			var numberSlide = carousel.data('carousel-slide');
			var wrapperHLS = carousel.parent();
			var imgsHl = carousel.find('img');

			var loadBackgroundHighlight = function(self, parentSelt, idx){
				if(idx === imgsHl.length - 1){
					carousel.width(wrapperHLS.width() + paddingRight);
					carousel.css('visibility', 'visible');
					carousel.find('.slides')
						.slick({
							siaCustomisations: true,
							dots: true,
							speed: 300,
							draggable: true,
							slidesToShow: numberSlide,
							slidesToScroll: numberSlide,
							accessibility: false,
							autoplay: false,
							pauseOnHover: false
						});
				}
			};

			imgsHl.each(function(idx) {
				var self = $(this);
				var parentSelt = self.parent();
				var nI = new Image();

				nI.onload = function(){
					loadBackgroundHighlight(self, parentSelt, idx);
				};

				nI.src = self.attr('src');
			});
		});

		win.off('resize.carouselSlide').on('resize.carouselSlide',function() {
			carouselSlide.each(function(){
				var carousel = $(this);

				carousel.width(carousel.parent().width() + paddingRight);
			});
		}).trigger('resize.carouselSlide');
	}
};
