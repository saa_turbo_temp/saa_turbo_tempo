/**
 * @name SIA
 * @description Define global multiTabsWithLongText functions
 * @version 1.0
 */
SIA.multiTabsWithLongText = function(){
	var global = SIA.global;

	var buildSelectForLongText = function(){
		var tabWrapper = $('[data-multi-tab]');
		var tabs = tabWrapper.find('ul.tab');
		// var select = multiTab.find('[data-select-tab]');
		// var tabWrapper = tabs.parent();
		var indexLimitTab = 0;
		var totalwid = 0;
		var tabItem = tabs.children();

		var multiTab1 = $();
		var selectTabs = $();
		var selectTabsText = $();
		var totalwidOneTab = 0;

		tabItem.each(function(i){
			var self = $(this);
			// var ctselect = self.find('select');
			// ctselect.empty().append(select.children(':eq('+i+'),:gt('+i+')').clone());
			totalwidOneTab+=self.outerWidth();
			if(totalwid + self.outerWidth() <= global.config.tablet && !tabItem.eq(indexLimitTab).hasClass('limit-item')){
				totalwid +=(self.outerWidth() - (self.find('em.ico-dropdown').is(':hidden') ? 0 : self.find('em.ico-dropdown').outerWidth(true)));
				indexLimitTab = i;
			}
			else{
				if(!tabItem.eq(indexLimitTab).hasClass('limit-item')){
					// tabWrapper.addClass('multi-tabs');
					tabItem.eq(indexLimitTab).addClass('limit-item').children('[data-customselect]').removeClass('hidden');
				}
			}
		});

		tabWrapper.addClass('multi-tabs');
		tabItem.filter(':gt('+indexLimitTab+')').addClass('hidden');

		if(totalwidOneTab < tabWrapper.outerWidth()){
			tabWrapper.removeClass('multi-tabs--1');
		}

		multiTab1 = $('.multi-tabs--1');
		selectTabs = multiTab1.find('[data-select-tab]');
		selectTabsText = selectTabs.find('.select__text');

		if(multiTab1.length){
			multiTab1.find('li').removeClass('hidden limit-item');
		}

		if(tabItem.eq(indexLimitTab).hasClass('limit-item')){
			// tabItem.eq(indexLimitTab).addClass('limit-item').children('[data-customselect]').removeClass('hidden');
			var multiSelect = tabItem.eq(indexLimitTab).find('select');
			var customSelectEl = multiSelect.closest('[data-customselect]');
			var limitItem = customSelectEl.closest('.limit-item');
			var indexOfFakeTab = limitItem.index();
			var indexTab = limitItem.is('.active') ? indexOfFakeTab : limitItem.siblings('li.active').index();
			var indexHolder = 0;

			// customSelectEl.customSelect({
			// 	itemsShow: 5,
			// 	heightItem: 43,
			// 	scrollWith: 2
			// });

			// var changeIcon = function(){
			// 	var displayTxtEl = customSelectEl.find(customSelectEl.data('customSelect').options.customText),
			// 		txt = multiSelect.find('option:selected').text(),
			// 		txtReplace = customSelectEl.data('replaceTextByPlane'),
			// 		regx = new RegExp(txtReplace, 'gi');
			// 	displayTxtEl.html(txt.replace(regx, '<em class="ico-plane"></em>'));
			// 	customSelectEl.siblings('.mark-desktop').html(txt.replace(regx, '<em class="ico-plane"></em>') + '<em class="ico-dropdown"></em>');
			// };
			// customSelectEl.off('beforeSelect.triggerTab').on('beforeSelect.triggerTab', function(){
			// 	changeIcon();
			// 	// if(tabs.data('click-through')){
			// 	// 	customSelectEl.closest('li').find('> a').trigger('click.switch-flight');
			// 	// }
			// });
			if(tabs.data('click-through')){
				customSelectEl.addClass('click-through');
				customSelectEl.off('click.triggerTab').on('click.triggerTab', function(){
					customSelectEl.closest('li').find('> a').trigger('click.show');
				});
			}
			customSelectEl.off('afterSelect.triggerTab').on('afterSelect.triggerTab', function(){
				// changeIcon();
				var curIndex = customSelectEl.data('customSelect').element.curIndex;
				tabs.find('> li > a').eq(curIndex + indexOfFakeTab).trigger('click.show');
				if(indexTab >= indexLimitTab){
					multiSelect.prop('selectedIndex', indexHolder);
					customSelectEl.customSelect('refresh');
					// changeIcon();
				}
			});
			if(indexTab > indexLimitTab){
				multiSelect.prop('selectedIndex', indexTab - limitItem.index());
				indexHolder = indexTab - limitItem.index();
				customSelectEl.customSelect('refresh');
				limitItem.siblings('li.active').removeClass('active').end().addClass('active');
			}
			// changeIcon();
		}

		var setWidthSelect = function(){
			if(selectTabs.length){
				var li = tabWrapper.find('li');
				var txt;
				var idx;

				selectTabs.width(tabWrapper.find('li.active').width());
				selectTabs.off('afterSelect.triggerTab').on('afterSelect.triggerTab', function(){
					txt = selectTabs.find('option:selected').text();
					idx = selectTabs.data('customSelect').element.curIndex;

					li.eq(idx).trigger('click.show');
					li.removeClass('active');
					li.eq(idx).addClass('active');

					selectTabsText.html(txt);
					tabWrapper.find('li.active').children('a').html(txt + '<em class="ico-dropdown"></em>');
					selectTabs.width(tabWrapper.find('li.active').width());

				});
			}
		};


		setWidthSelect();
	};

	var initModule = function(){
		buildSelectForLongText();
	};

	initModule();
};
