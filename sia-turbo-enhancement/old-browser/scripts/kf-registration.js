/**
 * @name SIA
 * @description Define global KFRegistration functions
 * @version 1.0
 */
SIA.kfRegistration = function() {
	var provideMyOwnEl = $('[data-provide-my-own]'),
			dataFieldProvide = $('[data-field-provide-my-own]');

	// var convertMonth = function(n, isGetIndex){
	// 	var m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	// 	return isGetIndex ? m.indexOf(n) : m[n];
	// };

	var detectGender = function(resourceElement, referenceElement) {
		if(!resourceElement.length || !referenceElement.length) {
			return;
		}
		var arrFemale = referenceElement.data('groupFemale').split(',');
		var triggerGender = function(element) {
			if($.inArray(element.val(), arrFemale) >= 0) {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'female';
				}).trigger('click');
			} else {
				resourceElement.find('input[type="radio"]').filter(function() {
					return this.value === 'male';
				}).trigger('click');
			}
		};

		referenceElement.off('change.referenceElement').on('change.referenceElement', function(){
			triggerGender($(this));
		});
		triggerGender(referenceElement);
	};

	var checkPostcode = function(){
		var countryVal = $('#country').val();
		var postCode = $('#postcode');
		if(countryVal === 'Singapore (+65)'){
			postCode.attr('data-rule-required', 'true');
			postCode.attr('data-msg-required', 'Singapore must have a postcode!');
		}else{
			postCode.removeAttr('data-rule-required');
			postCode.removeAttr('data-msg-required');
		}
	};

	// $.validator.addMethod('checkofadult', function(value, el, param) {
	// 	var day = $(param[1]).val();
	// 	var month = convertMonth($(param[2]).val(), true);
	// 	var year = $(param[3]).val();
	// 	var age = param[0];
	// 	var mydate = new Date();
	// 	mydate.setFullYear(year, month, day);

	// 	var setDate = new Date();
	// 	setDate.setFullYear(mydate.getFullYear() + age,month, day);
	// 	var currdate = new Date();

	// 	return currdate - setDate >= 0;
	// }, L10n.validator.checkofadult);

	provideMyOwnEl.off('change.provideMyOwn').on('change.provideMyOwn', function() {
		var that = $(this);
		if($.parseJSON(that.data('provideMyOwn'))) {
			dataFieldProvide.removeAttr('readonly').parent().removeClass('disabled');
		} else {
			dataFieldProvide.removeClass('error').attr('readonly', 'readonly').val('').parent().addClass('disabled');
			that.closest('.grid-col').removeClass('error').find('.text-error').remove();
		}
	});

	$('#country').off('blur.registerForm').on('blur.registerForm', function(){
		checkPostcode();
	});

	var initModule = function() {
		detectGender($('[data-gender-resource]'), $('[data-gender-reference]'));

		$('[data-disable-value]').each(function(){
			var wrap = $(this);
			var checkBox = $('input:checkbox', wrap);
			var inputSibling = $('input:text', wrap);
			if(inputSibling.data('ruleRequired')) {
				checkBox.each(function(i) {
					var self = $(this);
					var sibl = inputSibling.eq(i);
					self.off('change.disabledInputSibling').on('change.disabledInputSibling', function(){
						if(self.is(':checked')){
							sibl.val('').prop('disabled', true).siblings('a').hide().closest('span').addClass('disabled');
							wrap.find('.text-error').remove();
							wrap.find('.error').removeClass('error');
						}
						else{
							sibl.prop('disabled', false).closest('span').removeClass('disabled');
						}
					});
				});
			}
		});
	};

	initModule();
};
