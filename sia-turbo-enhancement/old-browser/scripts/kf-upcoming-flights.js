SIA.KFUpcomingFlights = function() {
	var config = SIA.global.config;
	var global = SIA.global;
	var loadPlaceholder = $('[data-load-placeholder]');
	var btnLoadMore = loadPlaceholder.find('[data-see-more]');

	var getBookingItemInfo = function() {
		$.get(config.url.kfSubBookingUpcomingFlights, function (html) {
			loadPlaceholder.find('[data-booking-item]').each(function() {
				var bookingItem = $(this);
				$.ajax({
					url: bookingItem.data('url'),
					type: global.config.ajaxMethod,
					data: {},
					dataType: 'json',
					success: function(res) {
						if(res){
							var template = window._.template(html, {
								data: res
							});
							$(template).appendTo(bookingItem.find('[data-booking-button]'));
							bookingItem.find('[data-booking-button] .loading').addClass('hidden');
							bookingItem.find('[data-booking-button] form').removeClass('hidden');

							bookingItem.find('[data-aircraft]').html(res.aircraft);
							bookingItem.find('[data-departure-time]').html(res.from.time);
							bookingItem.find('[data-date-port-departure]').html(res.from.date + ', ' + res.from.airport);
							bookingItem.find('[data-arrival-time]').html(res.to.time);
							bookingItem.find('[data-date-port-arrival]').html(res.to.date + ', ' + res.to.airport);
						}
					},
					error: function() {
						window.alert(L10n.upcomingFlights.errorGettingData);
					}
				});
			});
		});
	};

	var loadGlobalJson = function() {
		loadPlaceholder.children('[data-booking-item]').remove();
		$.get(config.url.kfBookingUpcomingTemplate, function(html) {
			html = html.replace(/td>\s+<td/g,'td><td');
			var template = window._.template(html, {
				data: globalJson.kfUpcomingFlights
			});
			loadPlaceholder.find('.main-heading').after($(template));

			if(globalJson.kfUpcomingFlights.length > 3) {
				btnLoadMore.removeClass('hidden');
			}
			else {
				btnLoadMore.addClass('hidden');
			}

			getBookingItemInfo();

			$('.checkin-alert').hide().removeClass('hidden').delay(2000).fadeIn(400);
		});
	};

	var loadMore = function() {
		var loadMoreCount = 0;

		var seeMore = function(btnSeemore, isSeeAll) {
			if (isSeeAll) {
				loadPlaceholder.find('.hidden[data-booking-item]').removeClass('hidden');
				loadMoreCount = 0;
			}
			else {
				loadPlaceholder.find('.hidden[data-booking-item]:lt(3)').removeClass('hidden');
			}

			if(loadPlaceholder.find('.hidden[data-booking-item]').length === 0) {
				btnSeemore.addClass('hidden');
				btnSeemore.text(L10n.kfSeemore.seeMore);
			}
		};

		btnLoadMore.off('click.load-more').on('click.load-more', function(e) {
			e.preventDefault();
			var self = $(this);
			loadMoreCount++;

			if (loadMoreCount < 2) {
				seeMore(self, false);
			}
			else if (loadMoreCount === 2) {
				seeMore(self, false);
				btnLoadMore.text(L10n.kfSeemore.seeAll);
			}
			else {
				seeMore(self, true);
			}
		});
	};

	var initModule = function() {
		loadGlobalJson();
		loadMore();
	};

	initModule();
};
