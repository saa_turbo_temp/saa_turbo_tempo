<% _.each(data.segmentMealVO.sectorMealListInformationVO, function(flight, flightIdx) { %>
  <div class="block-2 accordion" data-accordion="1"  data-select-meal="true">
    <div class="accordion__control active" data-tabindex="true" tabindex="0"><h3><%= (flightIdx + 1)%>. <%= flight.departureCityCode%> to <%= flight.arrivalCityCode%></h3></div>

    <div data-accordion-content="1" class="accordion__content" data-rule-selectCategory="true" data-msg-selectCategory="<%= L10n.selectMeal.msgSelectCategory %>" data-select-meal-trigger="true">
      <% var selectedPax = data.MealVO.passengerAndMealAssociationVO[data.MealVO.passengerStartingPoint];

      _.each(flight.mealService, function(trip, tripIdx) { %>
        <%
          var categoryName = '';
          var mealName = '';
          var categoryCode = '';
          var selectedCategory = [];
          var selectedInfo = selectedPax.flightDateInformationVO[flightIdx].SectorMealSelectedInfo[tripIdx];
          var mealProvided = trip.type.split('/');


          if (selectedInfo) {
            categoryName = selectedInfo.categoryName;
            categoryCode = selectedInfo.mealCategoryCode;
            mealName = selectedInfo.Name;
            selectedCategory = _.filter(trip.mealList, function(meal) {
              return meal.mealCategoryCode === categoryCode;
            });
          }
        %>
        <div class="accordion__content-info" >
          <h4 class="text-dark text-dark--left"><%= trip.origin%> TO <%= trip.destination%></h4><a href="#" class="link-4 link-4--right" data-trigger-popup=".popup--meal-maincourse" data-flight-json-url="ajax/<%- trip.popupJson %>"><em class="ico-point-r"></em>View Inflight Menu</a>
          <ul class="meal-list">
            <% _.each(mealProvided, function(provide, provideIdx) {  %>
              <%
                var mealType = provide.charAt(0).toUpperCase() + provide.substring(1).toLowerCase();
              %>
              <li ><%= mealType %> provided: &nbsp;<strong>Inflight Meal</strong></li>
            <% }); %>
          </ul>
          <button type="button" aria-expanded="false" class="select-meals btn-1" >Select Meal</button>
          <div tabindex="-1" class="select-meal-item" aria-hidden="true">
            <!-- <span aria-hidden="true">Choose Meals</span> -->
            <% _.each(mealProvided, function(provide, provideIdx) { %>
               <%
                 var mealType = provide.charAt(0).toUpperCase() + provide.substring(1).toLowerCase();
              %>
              <p class="text-dark" aria-label="<%- mealType%>"><%= mealType%></p>
              <div class="form-group grid-row" data-meal-portion="true">
                <div class="grid-col one-half">
                  <div class="grid-inner">
                    <div data-customselect="true" class="custom-select custom-select--2 default">
                      <label for="category-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" class="select__label">Category</label><span class="select__text" aria-hidden="true">Select</span><span class="ico-dropdown" aria-hidden="true">Select</span>
                      <select id="category-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" name="category-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" data-rule-required="true" data-msg-required="Select Category." data-select-category="true">
                      <%if (selectedPax.passengerType !=='INF') {%><option<%= !categoryName ? ' selected="selected" ' : ''%> value="<%= L10n.selectMeal.ifMealOption %>" data-populate="true"><%= L10n.selectMeal.ifMealOption %></option><%}%>
                        <% _.each(trip.mealList, function(category, categoryIdx) { %>
                          <% if ((selectedPax.passengerType.toUpperCase() === 'ADT' && category.mealCategoryCode.toUpperCase() !== 'IF' && category.mealCategoryCode.toUpperCase() !== 'CH' && category.mealCategoryCode.toUpperCase() !== 'BZ') || (selectedPax.passengerType.toUpperCase() === 'CHD' && category.mealCategoryCode.toUpperCase() !== 'IF') || (selectedPax.passengerType.toUpperCase() === 'INF' && category.mealCategoryCode.toUpperCase() === 'IF')) { %>
                            <%
                              var listMeal = _.map(category.mealServiceList, function(num, key) {return num.mealName}).toString();
                              var listCode = _.map(category.mealServiceList, function(num, key) {return num.mealCode}).toString();
                            %>
                            <option<%= categoryCode && category.mealCategoryCode === categoryCode ? ' selected="selected" ' : ''%> value="<%= category.mealCategoryName %>" data-code="<%= category.mealCategoryCode %>" data-service-list-meal='<%= listMeal %>' data-service-list-code='<%= listCode %>' data-undo="<%=(category.mealCategoryCode !== 'BC' && category.mealCategoryCode !== 'JP') ? true : false %>" data-populate="<%= (category.mealCategoryCode !== 'BC' && category.mealCategoryCode !== 'JP') ? true : false %>"><%= category.mealCategoryName %></option>
                          <% } %>
                        <%});%>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="grid-col one-half">
                  <div class="grid-inner">
                    <div data-customselect="true" class="custom-select custom-select--2 meal-type default">
                      <label for="meal-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" class="select__label">Selection</label><span class="select__text" aria-hidden="true">Select</span><span class="ico-dropdown" aria-hidden="true">Select</span>
                      <select  id="meal-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" name="meal-<%= flightIdx%>-<%= tripIdx%>-<%= provideIdx%>" data-rule-required="true" data-msg-required="Select Meal." data-option-meal="true">
                        <%if(categoryName === L10n.selectMeal.ifMealOption){%>
                          <option <%= !mealName ? ' selected="selected" ' : ''%> value="<%= L10n.selectMeal.ifMealOption %>"><%= L10n.selectMeal.ifMealOption %></option>
                        <%}%>
                        <% if (selectedInfo && selectedCategory && selectedCategory.length) { %>
                          <% _.each(selectedCategory[0].mealServiceList, function(meal, mealIdx) { %>
                            <option <%= mealName && meal.mealName.toUpperCase() === mealName.toUpperCase() ? ' selected="selected" ' : ''%> value="<%= meal.mealName%>" data-code="<%= meal.mealCode%>"><%= meal.mealName %></option>
                          <%});%>
                        <% }else{ %>
                          <%if (selectedPax.passengerType.toUpperCase() ==='INF'){%>
                            <%
                              var infantMeal = _.filter(trip.mealList, function(meal) {
                                return meal.mealCategoryCode.toUpperCase() === 'IF';
                              });
                            %>
                            <% _.each(infantMeal[0].mealServiceList, function(meal, mealIdx) { %>
                              <option <%= mealName && meal.mealName.toUpperCase() === mealName.toUpperCase() ? ' selected="selected" ' : ''%> value="<%= meal.mealName%>" data-code="<%= meal.mealCode%>"><%= meal.mealName %></option>
                            <%});%>
                          <% }else{%>
                            <option selected value="<%= L10n.selectMeal.ifMealOption %>"><%= L10n.selectMeal.ifMealOption %></option>
                          <% }%>
                        <% } %>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="alert-block checkin-alert" style="display: none;">
                <div class="inner">
                  <div class="alert__icon"><em class="ico-alert"></em></div>
                  <div class="alert__message"><span>By selecting a {0}, all your {1} selections from <%= flight.departureCityCode%> to <%= flight.arrivalCityCode%> have been changed to {0} selections.</span> <a href="#" data-undo="true">Undo</a></div>
                </div>
              </div>
            <% }); %>
            <button type="button" class="cancel-select btn-2" aria-expanded="true" data-cancel-meals="true">Cancel</button>
          </div>
        </div>
      <%});%>
    </div>

  </div>
<%});%>
