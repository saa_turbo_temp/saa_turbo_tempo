<% _.each(data, function(hotel, hotelIndex) { %>
  <div data-accordion="2" class="hotel-list" data-price="<%-hotel.rooms[0].rate.totalRate%>" data-rate="<%=hotel.starRating%>" data-map-id="<%=hotel.hotelID%>", data-latitude="<%=hotel.latitude%>", data-longtitude="<%=hotel.longitude%>" data-hotel-mainimg="<%=hotel.pictures[0].imageUrl%>" data-hotel-id="<%=hotel.hotelID%>" data-hotel-name="<%=hotel.hotelName%>" data-hotel-rooms="<%=hotel.rooms[0].remainingRooms%>" data-hotel-adults="<%=hotel.rooms[0].maximumAdultOccupancy%>" data-hotel-children="<%=hotel.rooms[0].maximumChildrenOccupancy%>" data-hotel-address="<%=hotel.address.addressLine1%>, <%=hotel.address.city%>, <%=hotel.address.country%>" data-hotel-kfmiles="<%=hotel.kfMiles%>">
    <div class="head-hotel accordion__control" data-accordion-trigger="2" aria-expanded="false">
      <div class="hotel-infor" tabindex="-1">
        <figure tabindex="-1">
          <img tabindex="-1" src="<%-hotel.pictures[0].imageUrl%>" alt="hotel.pictures[0].caption" longdesc="">
        </figure>
        <div class="slider-hotel slideshow-wrapper">
          <div data-skip-tabindex="true" class="flexslider flexslider--5">
            <a href="javascript:void(0);" class="slick-prev" tabindex="-1"></a>
            <a href="javascript:void(0);" class="slick-next" tabindex="-1"></a>
            <div class="slides" >
              <% _.each(hotel.pictures, function(picture, pictureIndex) { %>
                <div class="slide-item">
                  <img src="<%-picture.imageUrl%>" alt="<%=picture.caption%>" longdesc="">
                </div>
              <% }) %>
            </div>
          </div>
        </div>
        <div class="hotel-infor__content">
          <div class="hotel-infor__title">
            <h3 class="sub-heading-2--blue"><%=hotel.hotelName%></h3>
            <div class="desc">
              <p><%=hotel.address.addressLine1%>, <%=hotel.address.city%>, <%=hotel.address.country%> </p>
            </div>
            <ul class="rating-block">
              <% if (hotel.starRating === "0.5" || hotel.starRating === 0.5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "1" || hotel.starRating === 1) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "1.5" || hotel.starRating === 1.5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "2" || hotel.starRating === 2) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "2.5" || hotel.starRating === 2.5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "3" || hotel.starRating === 3) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "3.5" || hotel.starRating === 3.5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "4" || hotel.starRating === 4) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "4.5" || hotel.starRating === 4.5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } else if (hotel.starRating === "5" || hotel.starRating === 5) { %>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
                <li class="rated"><em class="ico-star"><span class="ui-helper-hidden-accessible">Click to mark as favourite</span></em>
                </li>
              <% } %>
            </ul>
          </div>
          <div class="price-room price-room--1">
            <div class="total-title">Price per night</div>
            <div class="total-cost"><%=hotel.ratesCurrency%> <%=hotel.rooms[0].rate.ratePerNight%></div>
            <div class="btn-6" >Earn <%=hotel.kfMiles%> miles</div>
          </div>
        </div>
        <div class="price-room price-room--2">
          <div class="total-title" >Price per night</div>
          <div class="total-cost"><%=hotel.ratesCurrency%> <%=hotel.rooms[0].rate.ratePerNight%></div>
          <div class="btn-6">Earn <%=hotel.kfMiles%> miles</div>
        </div>
        <em class="ico-point-r" data-trigger-arrow="true" aria-label="expand accordion"></em>
      </div>
    </div>
    <div data-accordion-content="2" class="detail-hotel accordion__content">
      <div data-wrapper-tab="true" data-tab="true" data-multi-tab="true" class="tabs--1 multi-tabs multi-tabs--1 addons-tab" role="tablist">
        <ul class="tab" role="tablist">
          <li class="tab-item active" aria-selected="true" role="tab"><a href="#">Rooms and rates</a>
          </li>
          <li class="tab-item" role="tab"><a href="#">Amenities</a>
          </li>
          <li data-hotel-map-trigger="true" class="tab-item" role="tab"><a href="#">Map</a>
          </li>
          <li class="more-item"><a href="#">More<em class="ico-dropdown"></em></a>
          </li>
        </ul>
        <div data-customselect="true" class="custom-select custom-select--2 multi-select tab-select" data-select-tab="true">
          <label for="multi-select_<%=hotel.hotelID%>" class="select__label">&nbsp;</label><span class="select__text">Rooms and rates</span><span class="ico-dropdown"></span>
          <select id="multi-select_<%=hotel.hotelID%>" name="multi-select_<%=hotel.hotelID%>">
            <option value="0" selected="selected">Rooms and rates</option>
            <option value="1">Amenities</option>
            <option value="2">Map</option>
          </select>
        </div>
        <div class="tab-wrapper">
          <div class="tab-content active", role="tabpanel">
            <div class="hotel-room--details">
              <div class="editor">
                <div class="component-fare-table"><span class="check-in">Check-in from <%=hotel.checkInTime%></span><span class="check-out">Check-out until <%=hotel.checkOutTime%></span>
                  <table data-sort-content="true" class="fare-basic-table" data-room-table="true">
                    <caption class="hidden">Room descriptions</caption>
                    <thead class="hidden-mb">
                      <tr>
                        <th class="th-5">Room type</th>
                        <th class="th-5">Capacity</th>
                        <th class="th-5">Price/night</th>
                        <th class="th-5">Total price*</th>
                        <th class="th-5"><span class="ui-helper-hidden-accessible">Table Heading</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <% _.each(hotel.rooms, function(room, roomIndex) { %>
                        <tr class="odd">
                          <td data-th="Room type"><%=room.roomName%></td>
                          <td data-th="Capacity"><em class="ico-user"></em><%=parseInt(room.maximumAdultOccupancy) + parseInt(room.maximumChildrenOccupancy) + parseInt(room.maximumInfantOccupancy) %></td>
                          <td data-th="Price/night"><%-room.currency%> <%=room.rate.ratePerNight%></td>
                          <td data-th="Total price*"><%-room.currency%> <%=room.rate.totalRate%></td>
                          <td>
                            <div class="btn-group">
                              <input type="submit" name="btn-add-room-<%-hotel.hotelID%>" id="btn-add-room-<%-hotel.hotelID%>" value="Select" data-add-room="true" data-roomid="<%- room.roomID %>" data-selfHotelID="<%- hotel.hotelID %>" class="btn-1">
                              <input type="submit" name="btn-remove-room-<%-hotel.hotelID%>" id="btn-remove-room-<%-hotel.hotelID%>" value="Remove" data-remove-room="true" class="btn-1 hidden">
                            </div>
                            <!-- <ul data-plus-or-minus-number="<%=room.remainingRooms%>" class="add-baggage-list">
                              <li>
                                <button type="button" class="btn-minus">-</button>
                              </li>
                              <li>
                                <input type="tel" name="number-baggage" value="1" placeholder="0" data-no-clear-text="true" data-updated-value="true" id="number-baggage-1-1-<%-room.roomID%>" data-rule-digits="true" maxlength="1" class="number-baggage">
                              </li>
                              <li>
                                <button type="button" class="btn-plus">+</button>
                              </li>
                            </ul> -->
                          </td>
                        </tr>
                        <tr class="even">
                          <td colspan="5">
                            <div class="free-cancel">
                              <span class="include">Included <%-room.rate.includes%></span>
                              <span class="cancel">Free cancellation before <%-room.cancellation.freeCancellationBefore%>
                                <em data-tooltip="true" data-content="<p class='tooltip__text-2'>Any cancellation received within 5 days prior to arrival date will incur the first night charge. <br/> Failure to arrive at your hotel will be treated as a No-Show and no refund will be given (Hotel policy).</p>" data-type="2" data-open-tooltip="data-open-tooltip" class="ico-info-round-fill"></em>
                              </span>
                            </div>
                          </td>
                        </tr>
                      <% }) %>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="note">
                <p>* Total price includes taxes</p>
              </div>
              <div class="terms-conditions added-room hidden">
              </div>
              <div class="terms-conditions">
                <p>By adding this room, you agree to the<a href="#">&nbsp;terms and conditions&nbsp;</a>of your hotel booking.</p>
              </div>
            </div>
            <div class="policy-hotel-room">
              <div class="head-policy">
                <h4 class="policy-title">Policies</h4>
                <div class="desc" >
                  <p>There is a fee of USD 25 for the use of health club and swimming pool. Guests staying in the Imperial floor rooms and suites are exempt from this fee</p>
                </div>
              </div>
              <div class="content-policy">
                <h4 class="title">Child and extra bed policy</h4>
                <ul class="list-policy">
                  <li><span >Infant 0-2 year(s)</span>Stay for free if using existing bedding.
                    <br>
                    Note, if you need a cot there may be an extra charge.
                  </li>
                  <li><span>Children 3- 17 year(s)</span>Stay for free if using existing bedding.</li>
                </ul>
                <ul class="list-policy-1">
                  <li>Guests over 17 years old are considered as adults.</li>
                  <li>Extra beds are dependent on the room you choose, please check the individual room capacity for more details.</li>
                  <li>When booking more than 5 rooms, diffrent policites and addition supplements may apply.</li>
                </ul>
              </div>
              <div class="head-policy">
                <h4 class="policy-title">Announcements</h4>
                <div class="desc">
                  <p>The property will be closed for renavations from 19 Oct 2016 to 1 Jan 2017</p>
                </div>
              </div>
              <div class="button-group">
                <input type="button" name="btn-poli-close-<%-hotel.hotelID%>" id="btn-poli-close-<%-hotel.hotelID%>" value="close" class="btn-4" data-trigger-poli-close="true">
              </div>
            </div>
          </div>
          <div class="tab-content" role="tabpanel">
            <div class="hotel-amenities">
              <p class="desc">The Fairmont San Francisco Hotel presents an awe-inspiring picture of historic San Francisco. The grandeur of the fully-restored hotel, coupled with its reputation for impeccable service, promises a truly memorable experience. Each of the hotel's spacious guestrooms and suites were also redecorated in elegant style. Central to the Financial District, Union Square, and Fisherman's Wharf, this property is located at the only spot in San Francisco where each of the city's cable car lines meet.</p>
              <p class="service-amenities">Amenities</p>
              <div class="amenities-content">
                <ul class="list-amenities">
                  <li><em class="ico-swim-36"></em><span class="text-icon">Swimming pool</span></li>
                  <li><em class="ico-info-3"></em><span class="text-icon">Concierge</span></li>
                  <li><em class="ico-2-hotel"></em><span class="text-icon">24 hour services</span></li>
                </ul>
                <ul class="list-amenities">
                  <li><em class="ico-2-lounge"></em><span class="text-icon">Lounge</span></li>
                  <li><em class="ico-2-fitness"></em><span class="text-icon">Fitness centre</span></li>
                  <li><em class="ico-wifi"></em><span class="text-icon">Wifi in public area</span></li>
                </ul>
                <ul class="list-amenities">
                  <li><em class="ico-cup"></em><span class="text-icon">Breakfast</span></li>
                  <li class="spa"><em class="ico-2-spa"></em><span class="text-icon">Spa and wellness</span></li>
                  <li><em class="ico-assistance"></em><span class="text-icon">Disability support</span></li>
                </ul>
              </div>
              <div class="button-group">
                <input type="submit" name="btn-amen-close-<%-hotel.hotelID%>" id="btn-amen-close-<%-hotel.hotelID%>" value="close" class="btn-4" data-trigger-amen-close="true">
              </div>
            </div>
          </div>
          <div class="tab-content" role="tabpanel">
            <div id="map_canvas_<%=hotel.hotelID%>" class="map_canvas">
            </div>
            <div class="button-group">
              <input type="submit" name="btn-map-close-<%-hotel.hotelID%>" id="btn-map-close-<%-hotel.hotelID%>" value="close" class="btn-4" data-trigger-map-close="true">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<% }) %>
