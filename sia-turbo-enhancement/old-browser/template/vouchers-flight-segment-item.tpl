<% if (index && data) {%>
  <% if (data.itinerary) {%>
    <div class="wrap-bg">
      <p class="title-6--dark"><%- index %>. <%- data.segmentDropDownValue%></p>
      <div class="block-white booking-info-group">
        <% if (data.itinerary) {%>
          <div class="flights__info--group">
            <div class="flights--detail" data-flight-number="O6600"><span class="flight-sq" tabindex="0">Flight <%- data.itinerary.flight %><em class="ico-point-d"><span class="ui-helper-hidden-accessible">See aircraft type</span></em><span class="loading loading--small hidden">Loading...</span></span>
              <div class="details hidden">
                <p>Aircraft type: Boeing 777-300ER</p>
                <p>Flying time: 18hrs 50mins</p>
              </div>
            </div><span aria-label="<%- data.itinerary.cabin %>" class="flights-type"><%- data.itinerary.cabin %></span>
          </div>
          <% _.each(data.itinerary.listSegment, function(segment, segmentIdx) { %>
            <div class="booking-info <%- segmentIdx === 0 ? 'no-border' : ''%>">
              <div class="booking-info-item ">
                <div class="booking-desc"><span class="hour"><%- segment.fromCity.cityCode + ' ' + segment.fromCity.departureTime %></span><span class="country-name"><%- segment.fromCity.cityName %></span>
                  <div class="booking-content"><span><%- segment.fromCity.departuteDate %>, <br><%- segment.fromCity.departureAirport %></span>
                  </div><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                </div>
              </div>
              <div class="booking-info-item ">
                <div class="booking-desc"><span class="hour"><%- segment.toCity.cityCode + ' ' + segment.toCity.departureTime %></span><span class="country-name"><%- segment.toCity.cityName %></span>
                  <div class="booking-content"><span><%- segment.toCity.departuteDate %>, <br><%- segment.toCity.departureAirport %></span>
                  </div>
                </div>
              </div>
            </div>
            <% if(segmentIdx >= data.itinerary.listSegment.length - 1) {%>
              <div class="booking-info booking-info-row">
                <div class="booking-content"><span>Total travel time <%- data.itinerary.totalTravelTime %></span></div>
              </div>
            <% } else { %>
              <div class="booking-info booking-info-row">
                <div class="booking-content"><span>Layover time <%- data.itinerary.layoverTime %></span></div>
              </div>
            <% } %>
          <% }); %>
        <% } %>
      </div>
      <div class="block-white">
        <p class="title-6--dark">Select passenger</p>
        <% if(data.itinerary && data.itinerary.passenger) {%>
          <ul class="voucher-passenger-list" data-voucherType="<%- voucherType %>">
            <% _.each(data.itinerary.passenger, function(passenger, idx) { %>
            <li>
                <% if(passenger.selected && passenger.disbled) { %>
                <div class="custom-checkbox custom-checkbox--1 disbled ">
                  <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" checked="true" disabled="true" data-checkedVoucher="<%- passenger.selected %>" data-tooltipVoucher="true" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                  <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>"  tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                  <% if(voucherType === 'Airport_Upgrade_Voucher') { %>
                    <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                       <div class="tooltip__content">
                         <p class="tooltip__text-2" tabindex="0">Airport upgrade vouchers can only be used by the principal PPS Club member or their redemption nominee</p>
                       </div>
                    </aside>
                  <% } else if(voucherType === 'Double_Miles_Accrual_Voucher') { %>
                    <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                       <div class="tooltip__content">
                         <p class="tooltip__text-2" tabindex="0">Double miles accrual vouchers can only be used by the principal PPS Club member</p>
                       </div>
                    </aside>
                  <% } else if(voucherType === 'Bookable_Upgrade_Voucher') { %>
                    <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                       <div class="tooltip__content">
                         <p class="tooltip__text-2" tabindex="0">Bookable upgrade vouchers are automatically applied to all passengers in one flight segment</p>
                       </div>
                    </aside>
                  <% } %>
                </div>
                <% } else { %>
                  <% if(voucherType === 'Airport_Upgrade_Voucher') { %>
                    <% if(!passenger.notANominee) { %>
                    <div class="custom-checkbox custom-checkbox--1">
                      <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" data-notANominee="<%- passenger.notANominee %>" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1" />
                      <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                    </div>
                    <% } else { %>
                    <div class="custom-checkbox custom-checkbox--1 disbled">
                      <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" data-notANominee="<%- passenger.notANominee %>" disabled="true" data-tooltipVoucher="true" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                      <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                      <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                         <div class="tooltip__content">
                           <p class="tooltip__text-2" tabindex="0">Airport upgrade vouchers can only be used by the principal PPS Club member or their redemption nominee</p>
                         </div>
                      </aside>
                    </div>
                    <% } %>
                  <% } else if(voucherType === 'Double_Miles_Accrual_Voucher') {%>
                    <% if(!passenger.principleKF) { %>
                      <div class="custom-checkbox custom-checkbox--1 disbled">
                        <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" disabled="true" data-tooltipVoucher="true" data-principleKF="<%- passenger.principleKF %>" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                        <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                        <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                           <div class="tooltip__content">
                             <p class="tooltip__text-2" tabindex="0">Double miles accrual vouchers can only be used by the principal PPS Club member</p>
                           </div>
                        </aside>
                      </div>
                    <% } else { %>
                    <div class="custom-checkbox custom-checkbox--1 disbled">
                      <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" checked="true" disabled="true" data-principleKF="<%- passenger.principleKF %>" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                      <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                    </div>
                    <% } %>
                  <% } else if(voucherType === 'Bookable_Upgrade_Voucher') { %>
                    <% if(!passenger.notANominee) { %>
                      <div class="custom-checkbox custom-checkbox--1 disbled">
                        <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" checked="true" disabled="true" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                        <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                      </div>
                    <% } else { %>
                      <div class="custom-checkbox custom-checkbox--1 disbled">
                        <input name="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" disabled="true" data-tooltipVoucher="true" id="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" aria-labelledby="booking-checkbox-<%- index %>-<%- passenger.passengerId %>-error" type="checkbox" tabindex="-1"/>
                        <label for="booking-checkbox-<%- index %>-<%- passenger.passengerId %>" tabindex="0"><%- passenger.firstName + ' ' + passenger.lastName %></label>
                        <aside tabindex="0" class="tooltip tooltip-3 tooltip--use-voucher hidden"><em class="tooltip__arrow"></em><a href="#" class="tooltip__close"><span class="ui-helper-hidden-accessible">Close</span><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a>
                           <div class="tooltip__content">
                             <p class="tooltip__text-2" tabindex="0">Bookable upgrade vouchers are automatically applied to all passengers in one flight segment</p>
                           </div>
                        </aside>
                      </div>
                    <% } %>
                  <% } %>
                <% } %>
            </li>
            <% }); %>
          </ul>
        <% } %>
      </div>
      <div class="block-white">
        <p class="title-6--dark">Upgrade to Premium Economy</p>
        <% if(voucherType !== 'Bookable_Upgrade_Voucher') { %>
          <p>Your upgrade will only be confirmed when you check in at the airport. Your voucher will be fully refunded if we are unable to upgrade you.</p>
        <% } %>
      </div>
<!--       <div class="add-more-cta">
        <p>Would you like to apply more vouchers on other flights segment?</p>
        <input type="button" name="more-flight" id="more-flight" value="Add more flight segment" class="btn-1 hidden"/>
      </div> -->
    </div>
  <% } else { %>
    <p class="text-error">There are no cabins available for an upgrade for the flight segment you have selected. Select another flight segment, or contact our<a href="#">
        <text>PPS Club Coordinators</text><span class="ui-helper-hidden-accessible"></span><span class="text"></span></a> for assistance.
    </p>
  <% } %>
<% } %>
