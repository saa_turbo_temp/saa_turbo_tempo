<% _.each(data, function(flight, index) { %>
  <section class="booking booking--style-1 <%- index > 2 ? 'hidden' : '' %> <%- index == data.length - 1 ? 'block--shadow' : '' %>" data-checkin-item="true">
    <div class="blk-heading">
      <h3 class="sub-heading-1--dark">
        Booking Reference <%- flight.referenceNumber %></h3>
      <a href="#" class="link-5"><em class="ico-gear"></em>Manage booking</a>
    </div>
    <h4 class="sub-heading-2--dark">
      <%- flight.from.name %> to <%- flight.to.name %></h4>
    <% if(flight.delay) { %>
      <div class="alert-block checkin-alert">
        <div class="inner">
          <div class="alert__icon"><em class="ico-alert">&nbsp;</em></div>
          <div class="alert__message"><%- flight.delay %></div>
        </div>
      </div>
    <% } %>
    <div class="block-2 booking-item">
      <div class="booking-info">
        <div class="booking-info-item booking-info--width-1">
          <div class="booking-desc">
            <span class="hour"><%- flight.flightNumber %></span>
            <div class="booking-content">
              <span data-aircraft="true"><%- flight.aircraft %></span>
            </div>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-2">
          <div class="booking-desc">
            <span class="hour">
              <%- flight.from.code %> <%- flight.from.time %>
            </span>
            <div class="booking-content">
              <span data-date-port="true">
                <%- flight.from.date %>, <%- flight.from.airport %>
              </span>
            </div><em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-2">
          <div class="booking-desc">
            <span class="hour">
            <%- flight.to.code %> <%- flight.to.time %>
            </span>
            <div class="booking-content">
              <span data-date-port="true">
                <%- flight.from.date %>, <%- flight.from.airport %>
              </span>
            </div>
          </div>
        </div>
        <div class="booking-info-item booking-info--width-3 booking-info__button">
          <div class="booking-desc">
            <div class="booking-content">
              <% if(flight.checkingStatus == 1) { %>
                <a href="#" class="btn-1 btn--block" data-checkin="1">Manage check-in</a>
              <% } else { %>
                <a href="#" class="btn-1 btn--block" data-checkin="2">Check-in</a>
              <% } %>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<% }); %>
