<aside class="popup popup--intro-lightbox-comingsoon hidden" >
  <div class="popup__inner">
    <a href="#" class="popup__close" data-close="true"></a>
    <figure>
      <div class="img-wrapper"><img src="images/lightbox-comingsoon.jpg" alt="" longdesc="img-desc.html">
      </div>
      <div class="intro-block">
        <h2 class="popup__heading"><span>Experience our newly enhanced singaporeair.com</span></h2>
        <p>We’ve improved singaporeair.com to give you a better experience when you’re on our website.</p>
        <p>The latest enhancements have been made to <strong>Manage booking</strong> – so it’s easier for you to change bookings, select seats and meals, upgrade your flights with KrisFlyer miles, and more.</p>
        <p>Previous revamps were made to online check-in, flight booking/redemption and KrisFlyer services. With all these enhancements in place, it’s now faster and easier to look for any information you may need.</p>
        <div class="button-group"><a href="#" class="btn-1" data-close="true">Continue</a>
        </div>
        <div class="custom-checkbox custom-checkbox--1" data-remember-cookie="true">
          <input name="remember-cb" id="remember-cb" type="checkbox">
          <label for="remember-cb">Don't show this to me again</label>
        </div>
      </div>
    </figure>
  </div>
</aside>
