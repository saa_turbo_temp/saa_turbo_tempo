<aside id="youtube-lightbox" class="popup popup--video-lightbox hidden">
  <div class="popup__inner">
    <div class="popup__content">
      <a href="#" class="popup__close" data-close="true"></a>
      <div class="wrap-video" id="flowplayer" style="display:block;width:640px;height:390px;">
        <a
            href="<%- data.old.url %>"
            style="display:block;"
            id="flowplayer">
        </a>
      </div>
      <ul class="watch-list">
        <% _.each(data.old.playlist, function(album, idx) { %>
          <li>
            <a href="<%- album.url %>" class="info-watch">
              <img src="<%- album.image %>" alt="" longdesc="img-desc.html">
              <div class="desc-thumb">
                <p><%- album.description %></p>
              </div>
            </a>
          </li>
        <% }); %>
      </ul>
  </div>
</aside>
