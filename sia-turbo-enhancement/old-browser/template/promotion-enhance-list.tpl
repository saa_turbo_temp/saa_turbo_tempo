<%
  var minPriceList = data.minPriceList;
%>
<% _.each( minPriceList, function( list, idx ){ %>
  <article data-accordion="1" class="promotion-item promotion-item--2 fadeIn animated">
    <div class="promotion-item__inner"><a href="#" aria-expanded="false" class="flight-item"><img src="<%- list.imgUrl %>" alt="<%- list.destinationCityName %>">
        <div class="flight-item__vignette">&nbsp;</div>
        <p class="info-promotion--enhance"><%- list.destinationCityName %></p></a>
      <a href="#" data-accordion-trigger="1" class="promotion-item__content" aria-expanded="false">
        <div class="promotion-item__desc">
          <p class="promotion_text-dark"><%- list.cabin %> <%- list.fromLabel %></p>
          <p class="promotion_text-dark"><%- list.currency %> <%- list.priceFormat %><sup>*</sup></p>
          <em class="ico-point-r">&nbsp;</em>
        </div>
      </a>
    </div>
    <div data-accordion-content="1" class="promotion-item__detail">
      <h3 class="title"><%- list.originCityName %> <%- list.toLabel %> <%- list.destinationCityName %></h3>
      <div class="editor">
        <%
          var promotionList = _.filter(data.list, function(item){
            return list.destinationCityName === item.destinationCityName;
          });

          var promotionListSorted = _.sortBy(promotionList, function(item){
            return item.priceFormat;
          });
        %>
        <table data-sort-content="true" class="fare-basic-table" data-room-table="true">
          <caption class="hidden">Promotion item descriptions</caption>
          <thead class="hidden-mb">
            <tr>
              <th class="th-5" scope="col">Prices from</th>
              <th class="th-5 advance-purchase" scope="col">Advanced purchase</th>
              <th class="th-5" scope="col"><%- list.bookByLabel %></th>
              <th class="th-5" scope="col"><%- list.travelPeriodLabel %></th>
              <th class="th-5" scope="col">Min. pax to go</th>
              <th class="th-5" scope="col"><span class="ui-helper-hidden-accessible">Table Heading</span></th>
            </tr>
          </thead>
          <tbody>
            <% _.each(promotionListSorted, function(promotionItem, promotionItemIdx) { %>
              <tr>
                <td data-th="Prices from" aria-describedby="wcag-currency-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.currency %> <%- promotionItem.priceFormat %><sup>*</sup><span class="info-trip-type">&nbsp;<%- promotionItem.tripType %></span><span class="ui-helper-hidden-accessible" id="wcag-currency-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.currency %> <%- promotionItem.priceFormat %>*</span></td>
               <td data-th="Advanced purchase" aria-describedby="wcag-advance-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.advancePurchase %> days<span class="ui-helper-hidden-accessible" id="wcag-advance-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.advancePurchase %> days</span></td>
                <td data-th="<%- promotionItem.bookByLabel %>" class="book-by" aria-describedby="wcag-bookby-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.bookBy %><span class="ui-helper-hidden-accessible" id="wcag-bookby-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.bookBy %></span></td>
                <td data-th="<%- promotionItem.travelPeriodLabel %>" aria-describedby="wcag-duration-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.durationStart %> - <%- promotionItem.durationEnd %><span class="ui-helper-hidden-accessible" id="wcag-duration-<%-promotionItemIdx%>-<%-idx%>"><%- promotionItem.durationStart %> - <%- promotionItem.durationEnd %></span></td>
                <td data-th="Min pax to go" class="min-pax" aria-describedby="wcag-minpaxtogo-<%-promotionItemIdx%>-<%-idx%>">
                  <span class="min-pax-hide"><%= promotionItem.minPaxToGo %></span>
                  <ul class="min-to-go">
                    <%
                      var minPax = parseInt(promotionItem.minPaxToGo);
                    %>
                    <% for(var i = 0; i < minPax; i++) { %>
                      <li><em class="ico-user"></em></li>
                    <% } %>
                  </ul>
                  <span class="ui-helper-hidden-accessible" id="wcag-minpaxtogo-<%-promotionItemIdx%>-<%-idx%>"><%= promotionItem.minPaxToGo %></span>
                </td>
                <td>
                  <a href="<%- promotionItem.shareurl %>" target="_blank" class="btn-1">Book Now</a>
                </td>
              </tr>
            <% }); %>
          </tbody>
        </table>
      </div><a href="#" class="close-btn">Close<em class="ico-close"></em></a>
    </div>
  </article>
<% }); %>
