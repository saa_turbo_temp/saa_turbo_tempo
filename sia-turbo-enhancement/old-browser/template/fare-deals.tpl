<div class="manualFareDeals">
  <% _.each(data.listFares, function( listFare ){ %>
    <div class="fare-deals-list">
      <ul>
        <% _.each( listFare.listDeals, function( listDeal ){ %>
          <li>
            <a href="<%- listDeal.href %>">
              <div class="info info--1">
                <span class="link"><%- listDeal.destinationCityName %></span><span><%- listDeal.cabin %></span>
              </div>
              <div class="info info--2"><span>from <%- listDeal.currency %></span><span class="fare-deal-num"><%- listDeal.price %>*</span>
              </div>
            </a>
          </li>
        <% }); %>
      </ul>
    </div>
  <% }); %>
  <% if (data.listMobiles && data.listMobiles.length) { %>
    <div class="fare-deals-list fare-deals-list--tablet">
      <ul>
        <% _.each( data.listMobiles, function( listDeal ){ %>
          <li>
            <a href="<%- listDeal.href %>">
              <div class="info info--1">
                <span class="link"><%- listDeal.destinationCityName %></span><span><%- listDeal.cabin %></span>
              </div>
              <div class="info info--2"><span>from <%- listDeal.currency %></span><span class="fare-deal-num"><%- listDeal.price %>*</span>
              </div>
            </a>
          </li>
        <% }); %>
      </ul>
    </div>
  <% } %>
</div>
<div class="promotion-wrap">
  <p class="fare-deals-note">* Click on each fare for the terms and conditions.</p>
  <a class="icon-guarantee" href="static-bestfare-generic.html">
    <img src="images/guarantee-large-icon.png" alt="Best Fare Guarantee">
  </a>
</div>
