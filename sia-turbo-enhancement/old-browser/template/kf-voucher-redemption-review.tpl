<table class="table-1 table-responsive review-voucher-redemption-table">
  <thead class="hidden-mb">
    <tr>
      <th class="table-width-0">Voucher type</th>
      <th class="table-width-1">Quantity</th>
      <th class="table-width-2">Miles required</th>
    </tr>
  </thead>
  <tbody>
  <% var i = 0 %>
  <% _.each( data.info, function( vouchers ){ %>
    <% _.each( vouchers.type, function( voucher ){ %>
      <tr class="<%= i % 2 === 0 ? 'odd' : 'even'  %> hidden" data-row-confirm="true">
        <td>
          <div class="data-title">Voucher type</div>
          <%= voucher.name %>
        </td>
        <td>
          <div class="data-title">Quantity</div>
          <span data-voucher-qty="<%= i %>"></span>
        </td>
        <td>
          <div class="data-title">Miles required</div>
          <span data-voucher-total="<%= i %>"></span>
        </td>
      </tr>
      <% i = i + 1 %>
    <% }) %>
  <% }) %>
  </tbody>
</table>

<% var j = 0 %>
<% _.each( data.info, function( vouchers ){ %>
  <% _.each( vouchers.type, function( voucher ){ %>
    <div class="cofirm-block cofirm-block-1 hidden" data-block-confirm="true">
      <dl>
        <dt>Name on voucher</dt>
        <dd data-voucher-name="true"></dd>
      </dl>
      <dl>
        <dt>Partner</dt>
        <dd><%= vouchers.partner %></dd>
      </dl>
      <dl>
        <dt>Total number of vouchers</dt>
        <dd data-voucher-qty="<%= j %>"></dd>
      </dl>
      <dl>
        <dt>Total miles redeemed</dt>
        <dd data-voucher-total="<%= j %>"></dd>
      </dl>
    </div>
    <% j = j + 1 %>
  <% }) %>
<% }) %>
