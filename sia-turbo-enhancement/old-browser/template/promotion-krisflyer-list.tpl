<% _.each( data.list, function( list, idx ){ %>
  <article class="promotion-item promotion-item--1">
    <a href="<%- list.shareurl %>" class="fadeIn animated promotion-item__inner">
      <figure>
        <div class="flight-item"><img src="<%- list.imgUrl %>" alt="<%- list.title %>" longdesc="img-desc.html">
        </div>
        <figcaption class="promotion-item__content">
          <h3 class="sub-heading-2--blue promotion__title"><%- list.title %></h3>
          <p class="promotion-item__desc"><%- list.copy %></p>
        </figcaption>
      </figure>
    </a>
  </article>
<% }); %>
