<% _.each(data.results, function(res, resIdx) { %>
	<tr class="<%- resIdx == 0 ? 'active' : '' %> <%- resIdx % 2 == 0 ? 'even-item' : '' %> <%- resIdx > 4 ? 'hidden' : '' %>">
		<td colspan="4" class="flight-part">
			<table class="flights__table--1__inner">
				<tbody>
					<% if(resIdx === 0) { %>
						<tr class="waitlisted">
							<td colspan="4" class="">
								<p class="message-waitlisted">
									<em class="ico-checkbox"></em>
									<% print(res.waitlisted) %>
								</p>
							</td>
						</tr>
					<% } %>
					<tr>
						<td class="first flights__info--group">
							<% _.each(res.stops, function(stop, stopIdx) { %>
								<% if(stop.flightName) { %>
									<div class="flights__info none-border">
										<div class="flights--detail left">
											<span><%- stop.flightName %>
												<em class="ico-point-d"></em>
												<span class="loading loading--small hidden">Loading...</span>
											</span>
											<div class="details hidden">
												<p>Aircraft type: <%- stop.aircraftType %></p>
												<p>Flying time: <%- stop.flyingTime %></p>
											</div>
										</div><span class="class-flight">
											<!--<%- stop.flightClass %>-->
										</span>
									</div>
								<% } %>
								<div class="flights__info">
									<div class="flights__info--detail">
										<em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
										<span class="hour"><%- stop.from.country %> <%- stop.from.time %></span>
										<span class="date"><%- stop.from.date %>, <br /><%- stop.from.port %></span>
									</div>
									<div class="flights__info--detail">
										<span class="hour"><%- stop.to.country %> <%- stop.to.time %></span>
										<span class="date"><%- stop.to.date %>, <br><%- stop.to.port %></span>
									</div>
								</div>
								<div class="flights__info <%- (stopIdx == res.stops.length - 1) ? 'last' : '' %>">
									<span><%- stop.totalTime %></span>
									<% if(stopIdx == res.stops.length - 1) { %>
										<a href="javascript:;" class="link-5 right">Fares<em class="ico-point-d"></em></a>
									<% } %>
								</div>
							<% }); %>
							<div class="flights__info flights__info--mb <%- resIdx == 0 ? 'visible-mb' : '' %>">
								<% _.each(res.packages, function(pack, packIdx) { %>
									<div class="flights__info--price">
										<% var flightClassName = '', colorClass = ''; %>
										<% if(packIdx == 0) { %>
											<% flightClassName = 'Economy Saver'; %>
											<% colorClass = 'bgd-green-2'; %>
										<% } else if (packIdx == 1) { %>
											<% flightClassName = 'Economy Standard'; %>
											<% colorClass = 'bgd-green-3'; %>
										<% } else { %>
											<% flightClassName = 'Premium Economy Standard'; %>
											<% colorClass = 'bgd-green-gray'; %>
										<% } %>
										<div class="package--name <%- colorClass %>">
											<%- flightClassName %>
										</div>
										<div class="package--price">
											<% if(pack.miles) { %>
												<% if(pack.waitlistText) { %>
													<p class="waitlist-text">Waitlist</p>
												<% } %>
												<div class="custom-radio custom-radio--1">
													<input name="flight-select-mb-<%- data.flight %>" id="flight-select-mb-<%- data.flight %>-<%- resIdx %>-<%- packIdx %>" type="radio" value="<%- pack.miles %>" data-related="<%- packIdx %>" data-fare="<%- pack.surcharges %>" data-taxes="<%- pack.taxes %>" data-carrier="<%- pack.carrier %>" data-flight-class="<%- pack.flightClass %>" data-datetime="<%- res.stops[0].from.date %> - <%- res.stops[0].from.time %>" data-waitlisted="<%- pack.waitlistText ? true : false %>">
													<label for="flight-select-mb-<%- data.flight %>-<%- resIdx %>-<%- packIdx %>"><%- pack.miles %><br> miles</label>
												</div>
												<a href="javascript: void(0)" data-type="3" data-tooltip="true" data-max-width="310" data-content="<div class=&quot;summary-fare&quot;> <h3 class=&quot;title-conditions&quot;>Summary of fare conditions</h3> <ul class=&quot;summary-fare__conditions&quot;> <li><em class=&quot;ico-check-thick&quot;></em>Booking change (USD 50)</li> <li><em class=&quot;ico-check-thick&quot;></em>Earn KrisFlyer miles (50%)</li> <li><em class=&quot;ico-close&quot;></em>Refund/Cancellation</li> <li><em class=&quot;ico-close&quot;></em>Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;<a href=&quot;cib-blank-page.html&quot;>terms and conditions</a>) </li> </ul> </div>" class="ico-info-round-fill">&nbsp;</a>
											<% } else { %>
												<span>Not available</span>
											<% } %>
										</div>
									</div>
								<% }); %>
							</div>
						</td>
						<% _.each(res.packages, function(pack, packIdx) { %>
							<% var tdClassName = ''; %>
							<% if(packIdx == 0) { %>
								<% tdClassName = 'package-1'; %>
							<% } else if(packIdx == 1) { %>
								<% tdClassName = 'package-2'; %>
							<% } else { %>
								<% tdClassName = 'last'; %>
							<% } %>
							<td class="hidden-mb <%- tdClassName %>">
								<% if(pack.miles) { %>
									<% if(pack.waitlistText) { %>
										<p class="waitlist-text">Waitlist</p>
									<% } %>
									<div class="custom-radio custom-radio--1">
										<input name="flight-select-<%- data.flight %>" id="flight-select-<%- data.flight %>-<%- resIdx %>-<%- packIdx %>" type="radio" value="<%- pack.miles %>" data-related="<%- packIdx %>" data-fare="<%- pack.surcharges %>" data-flight-class="<%- pack.flightClass %>" data-taxes="<%- pack.taxes %>" data-carrier="<%- pack.carrier %>" data-datetime="<%- res.stops[0].from.date %> - <%- res.stops[0].from.time %>" data-waitlisted="true">
										<label for="flight-select-<%- data.flight %>-<%- resIdx %>-<%- packIdx %>"><%- pack.miles %> miles</label>
									</div>
									<a href="javascript: void(0)" data-type="3" data-tooltip="true" data-max-width="310" data-content="<div class=&quot;summary-fare&quot;> <h3 class=&quot;title-conditions&quot;>Summary of fare conditions</h3> <ul class=&quot;summary-fare__conditions&quot;> <li><em class=&quot;ico-check-thick&quot;></em>Booking change (USD 50)</li> <li><em class=&quot;ico-check-thick&quot;></em>Earn KrisFlyer miles (50%)</li> <li><em class=&quot;ico-close&quot;></em>Refund/Cancellation</li> <li><em class=&quot;ico-close&quot;></em>Upgrade with KrisFlyer miles on flights operated by Singapore Airlines, SilkAir and selected Star Alliance airlines (see the KrisFlyer&amp;nbsp;<a href=&quot;cib-blank-page.html&quot;>terms and conditions</a>) </li> </ul> </div>" class="ico-info-round-fill">&nbsp;</a>
								<% } else { %>
									<span>Not available</span>
								<% } %>
							</td>
						<% }); %>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
<% }); %>
