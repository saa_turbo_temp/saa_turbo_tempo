<% if(!confirmationPage) { %>
  <h2 data-anchor="flights" class="popup__heading">Flight details</h2>
  <div class="flights-target flights-target--2">
    <% _.each(data.bookingSummary.flight, function(flight, flightIdx) { %>
      <h3 class="title-1">
        <%= (flightIdx + 1) %>. <%= flight.origin %> to <%= flight.destination %>
      </h3>
      <div class="flights-target__content">
        <div class="flights__info--group">
          <% _.each(flight.flightSegments, function(segment, segmentIdx) { %>
            <% if(segmentIdx == 0) { %>
              <div class="flights__info none-border">
                <div class="flights--detail left"
                  data-flight-number="<%= segment.flightNumber %>"
                  data-carrier-code="<%= segment.carrierCode %>"
                  data-date="<%= segment.deparure.date %>"
                  data-origin="<%= segment.deparure.airportCode %>">
                  <span>Flight <%= segment.carrierCode %> <%= segment.flightNumber %>
                    <em class="ico-point-d"></em>
                    <span class="loading loading--small hidden">Loading...</span>
                  </span>
                  <div class="details hidden">
                    <p>Aircraft type: <%= segment.airCraftType %></p>
                    <p>Flying time: <%= flight.totalTravelTime %></p>
                  </div>
                </div><span class="class-flight">Premium Economy</span>
              </div>
            <% } %>
            <div class="flights__info">
              <div class="flights__info--detail">
                <em class="ico-airplane-lift"><span class="ui-helper-hidden-accessible">Flying</span></em>
                <span class="hour">
                  <%= segment.deparure.airportCode %> <%= segment.deparure.time %>
                </span>
                <span class="country-name">
                  <%= segment.deparure.cityName %>
                </span>
                <span class="date">
                  <%= segment.deparure.date %>,<br> <%= segment.deparure.airportName %>
                </span>
              </div>
              <div class="flights__info--detail">
                <span class="hour">
                  <%= segment.arrival.airportCode %> <%= segment.arrival.time %>
                </span>
                <span class="country-name">
                  <%= segment.arrival.cityName %>
                </span>
                <span class="date">
                  <%= segment.arrival.date %>,<br> <%= segment.arrival.airportName %>
                </span>
              </div>
            </div>
            <% if(segment.layoverTime) { %>
              <div class="flights__info"><span>Layover time: 1hr 5mins</span></div>
            <% } %>
          <% }); %>
          <div class="flights__info none-border"><span>Total travel time: <%= flight.totalTravelTime %></span></div>
        </div>
      </div>
    <% }); %>
  </div>


  <h2 class="popup__heading">Passengers</h2>
  <div class="flights-target flights-target--3">
    <% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
      <% if(pax.detailsPerFlight) { %>
        <h3 class="title-1"><%= (paxIdx + 1) %>. <%= pax.paxName %></h3>
        <div class="info-passengers-group info-passengers--style-1">
          <% _.each(pax.detailsPerFlight, function(detail, detailIndex) { %>
            <div class="info-passengers <%= (detailIndex > 0) ? 'no-border' : '' %>">
              <div class="info-passengers__flight">
                <p class="title">
                  <%
                    _.each(data.bookingSummary.flight, function(f, fIdx) {
                      _.each(f.flightSegments, function(s, sIdx) {
                        if(s.deparure.airportCode == detail.from && s.arrival.airportCode == detail.to) {
                          print(s.carrierCode + ' ' + s.flightNumber);
                        }
                      });
                    });
                  %>
                </p>
                <span><%= detail.from %> – <%= detail.to %></span>
              </div>
              <div class="info-passengers__detail">
                <% var excessBaggage = false; %>
                <% _.each(detail.addonPerPax, function(addPax, addPaxIdx) { %>
                  <% if(addPax.type == 'Seat') { %>
                    <div class="pref-seat">
                      <span class="number-seat">44h</span>
                      <span class="text-seat">Preferred Seat</span>
                    </div>
                  <% } %>
                  <% if(addPax.type == 'Excess baggage') { excessBaggage = true; } %>
                <% }); %>
                <% if(excessBaggage == true) { %>
                  <div class="pref-bag">
                    <span class="number-pref"><em class="ico-business-1"></em></span>
                    <div class="weight">
                      <% _.each(detail.addonPerPax, function(addPax, addPaxIdx) { %>
                        <% if(addPax.type == 'Excess baggage') { %>
                          <span><%= addPax.description %></span>
                        <% } %>
                      <% }); %>
                    </div>
                  </div>
                <% } %>
              </div>
            </div>
          <% }); %>
        </div>
      <% } %>
    <% }); %>
  </div>
<% } %>

<h2 data-anchor="cost" class="popup__heading">Cost breakdown</h2>
<% _.each(data.bookingSummary.paxDetails, function(pax, paxIdx) { %>
  <div class="cost-breakdown">
    <h3 class="title-1">Passenger <%= (paxIdx + 1) %> - <%= pax.paxName %></h3>
    <table class="table-allocation table-allocation--cost">
      <thead>
        <tr>
          <th class="first"><span class="text-bold">Fare</span></th>
          <th class="second">Cost in KrisFlyer miles</th>
          <th class="third">Cost in SGD</th>
        </tr>
      </thead>
      <tbody>
        <% if (pax.fareDetails) { %>
          <% _.each(pax.fareDetails.milesAllocation, function(ma, maIdx) { %>
            <tr class="<%= (maIdx == pax.fareDetails.milesAllocation.length - 1) ? 'type-1' : '' %>">
              <td>
                <span><%= ma.category %></span>
                <ul class="list-allocation visible-mb">
                  <li>
                    <span>KrisFlyer miles</span>
                    <% if(ma.milesAllocated) { %>
                      <span data-need-format="0">
                        <%= ma.milesAllocated %>
                      <span>
                    <% } else { %>
                      <span>-</span>
                    <% } %>
                  </li>
                  <li>
                    <span>SGD</span>
                    <% if(ma.totalAmount) { %>
                      <span data-need-format="2">
                        <%= ma.totalAmount %>
                      </span>
                    <% } else { %>
                      <span>-</span>
                    <% } %>
                  </li>
                </ul>
              </td>
              <% if(ma.milesAllocated) { %>
                <td data-need-format="0">
                  <%= ma.milesAllocated %>
                </td>
              <% } else { %>
                <td>-</td>
              <% } %>
              </td>
              <% if(ma.totalAmount) { %>
                <td data-need-format="2">
                  <%= ma.totalAmount %>
                </td>
              <% } else { %>
                <td>-</td>
              <% } %>
              </td>
            </tr>
          <% }); %>
          <tr>
            <td><span class="text-bold">Airport / Government taxes:</span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <% _.each(pax.fareDetails.taxes, function(tax, taxIdx) { %>
            <tr class="state-1 <%= (taxIdx == pax.fareDetails.taxes.length - 1) ? 'type-1' : '' %>">
              <td>
                <span><%= tax.description %> (<%= tax.code %>)</span>
                <span class="visible-mb"><%= tax.amount %></span>
              </td>
              <td>-</td>
              <td data-need-format="2">
                <%= tax.amount %>
              </td>
            </tr>
          <% }); %>
          <tr>
            <td><span class="text-bold">Carrier Surcharges:</span></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <% _.each(pax.fareDetails.surcharge, function(charge, chargeIdx) { %>
            <tr class="state-1 <%= (chargeIdx == pax.fareDetails.taxes.length - 1) ? 'type-1' : '' %>">
              <td>
                <span><%= charge.description %> (<%= charge.code %>)</span>
                <span class="visible-mb"><%= charge.amount %></span></td>
              <td>-</td>
              <td data-need-format="2"><%= charge.amount %></td>
            </tr>
          <% }); %>
          <tr class="subtotal">
            <td><span>Subtotal</span>
              <ul class="list-allocation visible-mb">
                <li>
                  <span>KrisFlyer miles</span>
                  <span data-need-format="0"><%= pax.fareDetails.totalMiles %></span>
                </li>
                <li>
                  <span>SGD</span>
                  <span data-need-format="2"><%= pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount %></span>
                </li>
              </ul>
            </td>
            <td data-need-format="0"><%= pax.fareDetails.totalMiles %></td>
            <td data-need-format="2"><%= pax.fareDetails.fareAmount + pax.fareDetails.surchargeAmount + pax.fareDetails.taxAmount %></td>
          </tr>
        <% } %>
      </tbody>
    </table>
  </div>
<% }); %>
<div class="grand-total">
  <span class="total-title">Grand total payable</span>
  <span class="total-info">
    <% if(data.bookingSummary.costPayableByMiles) { %>
      <span data-need-format="0"><%= data.bookingSummary.costPayableByMiles %></span> miles +&nbsp;
    <% } %>
    SGD <span data-need-format="2"><%= data.bookingSummary.costPayableByCash %></span>
  </span>
</div>
