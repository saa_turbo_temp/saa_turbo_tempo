<% if (data && data.mealInfo && data.mealInfo.SectorMealSelectedInfo && data.mealInfo.SectorMealSelectedInfo.length) { %>
  <div class="booking-details booking-details--2">
    <div class="booking-col col-1">
      <em class="ico-food"></em>
    </div>
    <div class="booking-col col-2">
      Meals
    </div>
    <div class="booking-col col-3">
      <div class="align-wrapper">
        <div class="align-inner">
          <% _.each(data.mealInfo.SectorMealSelectedInfo, function(meal, mealIdx) { %>
            <div class="has-cols">
              <p class="target-info"><%- meal.origin %> TO <%- meal.destination %></p>
              <p class="seat-info">
                <strong><%- meal.type ? meal.type + ': ' : ''%> </strong>
                <%- meal.Name %>
              </p>
            </div>
          <% }); %>
      </div>
      </div>
    </div>
    <div class="booking-col col-4">
      <% if(data.paxId && data.mealInfo && data.mealInfo.departureCityCode && data.mealInfo.departureTime && data.mealInfo.flightNumber) { %>
        <% if (data.mealInfo.eligible && data.mealInfo.eligible === 'true') { %>
          <a href="/meals/?paxId=<%- data.paxId %>&departureCityCode=<%- data.mealInfo.departureCityCode %>&departureDate=<%- data.mealInfo.departureDate %>&flightNumber=<%- data.mealInfo.flightNumber %>">
            <em class="ico-point-r"></em>Select / Change
          </a>
        <% } else { %>
          <a class="disabled" href="javascript:void(0);">
            <em class="ico-point-r"></em>Select / Change
          </a>
        <% } %>
      <% } else { %>
        <a href="#">
          <em class="ico-point-r"></em>Select / Change
        </a>
      <% } %>
    </div>
  </div>
<% } %>
