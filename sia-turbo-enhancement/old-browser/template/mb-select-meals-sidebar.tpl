<% var optionHtml = ''; %>
<% _.each(data.passengerAndMealAssociationVO, function(pax, idx) { %>
  <% if (idx === data.passengerStartingPoint - 1) { %>
    <a href="#" class="booking-nav__item active">
    <% optionHtml += '<option selected="selected" '; %>
  <% } else { %>
    <a href="mb-select-meals.html" class="booking-nav__item">
    <% optionHtml += '<option '; %>
  <% } %>
    <span class="passenger-info">
      <span class="passenger-info__number"><%= (idx + 1) + '.' %></span>
      <span class="passenger-info__text"><%= pax.passengerName %><span>&nbsp;(<%= pax.passengerType %>)</span></span>
      <em class="ico-point-r"></em>
      <% if(pax.mealSelected && pax.mealSelected === 'true') { %>
        <em class="ico-check-thick"></em>
      <% } %>
      <input type="hidden" name="passenger-info-<%= (idx + 1) %>">
      <% optionHtml += 'value="' + (idx + 1) + '">' + pax.passengerName + '</option>' %>
    </span>
  </a>
<% }); %>
<select name="sidebar-tab-select" id="sidebar-tab-select" class="tab-select">
  <%= optionHtml %>
</select>
